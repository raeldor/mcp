//
//  FlashConvEnglishFromJapanese.m
//  Mcp
//
//  Created by Ray Price on 1/27/13.
//
//

#import "FlashConvEnglishFromJapanese.h"
#import "McpAppDelegate.h"
#import "HtmlFunctions.h"
#import "FlashConvBlock.h"
#import "FlashConvAnswer.h"
#import "FlashConvLine.h"

@implementation FlashConvEnglishFromJapanese

-(FlashConvBlock*)getBlockForName:(NSString*)inBlockName usingGame:(FlashGame*)inGame isMuted:(BOOL)inIsMuted {
    // return correct block
    if ([inBlockName isEqualToString:@"ClassIntro"])
        return [self getClassIntroConversationBlockFor:inGame];
    if ([inBlockName isEqualToString:@"GameIntro"])
        return [self getGameIntroConversationBlockFor:inGame];
    if ([inBlockName isEqualToString:@"Question"])
        return [self getQuestionConversationBlockFor:inGame];
    if ([inBlockName isEqualToString:@"Answer"])
        return [self getAnswerConversationBlockFor:inGame];
    if ([inBlockName isEqualToString:@"Congrats"])
        return [self getCongratsConversationBlockFor:inGame];
    if ([inBlockName isEqualToString:@"Regrets"])
        return [self getRegretsConversationBlockFor:inGame];
    
    // no block found
    return nil;
}

-(FlashConvBlock*)getClassIntroConversationBlockFor:(FlashGame*)inGame {
    FlashConvBlock *returnBlock = [[FlashConvBlock alloc] initWithBlockName:@"ClassIntro" nextBlockName:@"GameIntro" goToNextCard:NO];
    
    // format a date string
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"yyyy年MM月dd日"];
    NSDate *now = [[NSDate alloc] init];
    NSString *dateString = [format stringFromDate:now];
    [now release];
    [format release];
    
    // create first line
    FlashConvLine *firstLine = [[FlashConvLine alloc] init];
    [firstLine.segmentArray addObject:CONVSTRING_WELCOME];
    [returnBlock.lines addObject:firstLine];
    [firstLine release];
    
    return [returnBlock autorelease];
}

-(FlashConvBlock*)getGameIntroConversationBlockFor:(FlashGame*)inGame {
    FlashConvBlock *returnBlock = [[FlashConvBlock alloc] initWithBlockName:@"GameIntro" nextBlockName:@"Question" goToNextCard:NO];
    
    // create first line
    FlashConvLine *firstLine = [[FlashConvLine alloc] init];
    [firstLine.segmentArray addObject:CONVSTRING_GUESSENGLISH];
    [returnBlock.lines addObject:firstLine];
    [firstLine release];
    
    // let's go!
    FlashConvLine *secondLine = [[FlashConvLine alloc] init];
    [secondLine.segmentArray addObject:CONVSTRING_LETSGO];
    [returnBlock.lines addObject:secondLine];
    [secondLine release];
    
    return [returnBlock autorelease];
}

-(FlashConvBlock*)getQuestionConversationBlockFor:(FlashGame*)inGame {
    FlashConvBlock *returnBlock = [[FlashConvBlock alloc] initWithBlockName:@"Question" nextBlockName:nil goToNextCard:NO];
    
    // add continue button
    [returnBlock.answers addObject:[[[FlashConvAnswer alloc] initWithButtonText:@"Continue" jumpToBlockName:@"Answer" isRightAnswer:NO isWrongAnswer:NO] autorelease]];
    
	// get application delegate
    McpAppDelegate *appDelegate = (McpAppDelegate*)[[UIApplication sharedApplication] delegate];
    
    // create first line
    FlashConvLine *firstLine = [[FlashConvLine alloc] init];
    
    // say word in japanese
    if ([[inGame getNotes] isEqualToString:@"word usually written using kana alone"])
        [firstLine.segmentArray addObject:[NSString stringWithFormat:@"「%@」。", [inGame getKanaUsingSeparator:@", "]]];
    else {
        NSString *rubyText = [HtmlFunctions getRubyForKanji:[inGame getKanjiUsingSeparator:@", "] andKana:[inGame getKanaUsingSeparator:@", "]];
        [firstLine.segmentArray addObject:[NSString stringWithFormat:@"「<vtml_sub alias=\"%@\">%@</vtml_sub>」。", [inGame getKanaUsingSeparator:@", "], rubyText]];
    }
    
    // say word type
    [firstLine.segmentArray addObject:[NSString stringWithFormat:@"%@。", [inGame getWordType]]];
    
    // add example sentence pronunciation
    SampleEntry *thisSample = [inGame getSampleEntryForJlptLevel:appDelegate.localOptions.jlptLevel pickType:appDelegate.localOptions.pickSentenceType];
    if (thisSample == nil) {
        // say couldn't find line
        [firstLine.segmentArray addObject:@"例文を見つけませんでした。"];
    }
    else {
        // show sample sentence with ruby
        [firstLine.segmentArray addObject:@"For example。"];
        [firstLine.segmentArray addObject:[NSString stringWithFormat:@"”%@”。", [thisSample getKanjiWithRuby:YES includeLinks:NO]]];
    }
    
    // ask question
    [firstLine.segmentArray addObject:@"英語で何と言いますか。"];
    
    // add first line to block
    [returnBlock.lines addObject:firstLine];
    [firstLine release];
    
    return [returnBlock autorelease];
}

-(FlashConvBlock*)getAnswerConversationBlockFor:(FlashGame*)inGame {
    FlashConvBlock *returnBlock = [[FlashConvBlock alloc] initWithBlockName:@"Answer" nextBlockName:nil goToNextCard:NO];
    
    // add answers
    [returnBlock.answers addObject:[[[FlashConvAnswer alloc] initWithButtonText:@"はい" jumpToBlockName:@"Congrats" isRightAnswer:YES isWrongAnswer:NO] autorelease]];
    [returnBlock.answers addObject:[[[FlashConvAnswer alloc] initWithButtonText:@"いいえ" jumpToBlockName:@"Regrets" isRightAnswer:NO isWrongAnswer:YES] autorelease]];
    
	// get application delegate
    McpAppDelegate *appDelegate = (McpAppDelegate*)[[UIApplication sharedApplication] delegate];
    
    // create first line
    FlashConvLine *firstLine = [[FlashConvLine alloc] init];
    
    // say question again for confirmation
    if ([[inGame getNotes] isEqualToString:@"word usually written using kana alone"])
        [firstLine.segmentArray addObject:[NSString stringWithFormat:@"「%@」。", [inGame getKanaUsingSeparator:@", "]]];
    else {
        NSString *rubyText = [HtmlFunctions getRubyForKanji:[inGame getKanjiUsingSeparator:@", "] andKana:[inGame getKanaUsingSeparator:@", "]];
        [firstLine.segmentArray addObject:[NSString stringWithFormat:@"「<vtml_sub alias=\"%@\">%@</vtml_sub>」。", [inGame getKanaUsingSeparator:@", "], rubyText]];
    }
    
    // say answer in english
    [firstLine.segmentArray addObject:[NSString stringWithFormat:@"英語では"]];
    [firstLine.segmentArray addObject:[NSString stringWithFormat:@"「%@」。", [inGame getAnswerUsingSeparator:@", "]]];
    
    // add first line to block
    [returnBlock.lines addObject:firstLine];
    [firstLine release];
    
    /* taken out for quicker game by request
     
    // create second line
    FlashConvLine *secondLine = [[FlashConvLine alloc] init];
    
    // add example sentence pronunciation
    SampleEntry *thisSample = [inGame getSampleEntryForJlptLevel:appDelegate.localOptions.jlptLevel pickType:appDelegate.localOptions.pickSentenceType];
    if (thisSample == nil) {
        // say couldn't find line
        [secondLine.segmentArray addObject:@"例文を見つけませんでした。"];
        [returnBlock.lines addObject:secondLine];
        [secondLine release];
    }
    else {
        // add sentence meaning
        [secondLine.segmentArray addObject:[NSString stringWithFormat:@"「%@」", thisSample.meaning]];
        [returnBlock.lines addObject:secondLine];
        [secondLine release];
        
        // show sample sentence with ruby
        FlashConvLine *thirdLine = [[FlashConvLine alloc] init];
        [thirdLine.segmentArray addObject:[NSString stringWithFormat:@"「%@」", [thisSample getKanjiWithRuby:YES includeLinks:NO]]];
        [returnBlock.lines addObject:thirdLine];
        [thirdLine release];
        
        // show meaning again
        FlashConvLine *fourthLine = [[FlashConvLine alloc] init];
        [fourthLine.segmentArray addObject:[NSString stringWithFormat:@"「%@」", thisSample.meaning]];
        [returnBlock.lines addObject:fourthLine];
        [fourthLine release];
    }
     */
    
    // add did u get it right?
    FlashConvLine *fifthLine = [[FlashConvLine alloc] init];
    [fifthLine.segmentArray addObject:CONVSTRING_WEREYOURIGHT];
    [returnBlock.lines addObject:fifthLine];
    [fifthLine release];
    
    return [returnBlock autorelease];
}

-(FlashConvBlock*)getCongratsConversationBlockFor:(FlashGame*)inGame {
    FlashConvBlock *returnBlock = [[FlashConvBlock alloc] initWithBlockName:@"Congrats" nextBlockName:@"Question" goToNextCard:YES];
        
    // create first line
    FlashConvLine *firstLine = [[FlashConvLine alloc] init];
    [firstLine.segmentArray addObject:CONVSTRING_CONGRATULATIONS];
    [returnBlock.lines addObject:firstLine];
    [firstLine release];
    
    return [returnBlock autorelease];
}

-(FlashConvBlock*)getRegretsConversationBlockFor:(FlashGame*)inGame {
    FlashConvBlock *returnBlock = [[FlashConvBlock alloc] initWithBlockName:@"Regrets" nextBlockName:@"Question" goToNextCard:YES];
    
    // create first line
    FlashConvLine *firstLine = [[FlashConvLine alloc] init];
    [firstLine.segmentArray addObject:CONVSTRING_ITSOK];
    [returnBlock.lines addObject:firstLine];
    [firstLine release];
    
    return [returnBlock autorelease];
}


@end
