//
//  BufferPhoneme.h
//  Mcp
//
//  Created by Ray Price on 4/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BufferPhoneme : NSObject {
    int length;
    NSString *phoneme;
}

-(id)initWithLength:(int)inLength andPhoneme:(NSString*)inPhoneme;

@property (nonatomic, assign) int length;
@property (nonatomic, retain) NSString *phoneme;

@end
