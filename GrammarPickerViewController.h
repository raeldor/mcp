//
//  GrammarPickerViewController.h
//  Mcp
//
//  Created by Ray Price on 10/19/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Chapter.h"
#import "SelectControllerDelegate.h"

@interface GrammarPickerViewController : UIViewController <UIPickerViewDelegate, UIPickerViewDataSource> {
    IBOutlet UIToolbar *gpToolbar;
    IBOutlet UIPickerView *gpPicker;
    
    id<SelectControllerDelegate> selectDelegate;
    
    UIActionSheet *actionSheet;
    Chapter *chapter;
}

@property (nonatomic, retain) UIToolbar *gpToolbar;
@property (nonatomic, retain) UIPickerView *gpPicker;
@property (nonatomic, retain) id<SelectControllerDelegate> selectDelegate;

-(void)setActionSheet:(UIActionSheet*)inActionSheet;
-(void)setChapterAs:(Chapter*)inChapter;
-(IBAction)cancelButtonPressed:(id)sender;
-(IBAction)selectButtonPressed:(id)sender;

@end
