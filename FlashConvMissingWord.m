//
//  FlashConvMissingWord.m
//  Mcp
//
//  Created by Ray Price on 1/27/13.
//
//

#import "FlashConvMissingWord.h"
#import "HtmlFunctions.h"
#import "McpAppDelegate.h"
#import "FlashConvAnswer.h"
#import "FlashConvLine.h"

@implementation FlashConvMissingWord

-(FlashConvBlock*)getBlockForName:(NSString*)inBlockName usingGame:(FlashGame*)inGame isMuted:(BOOL)inIsMuted {
    // return correct block
    if ([inBlockName isEqualToString:@"ClassIntro"])
        return [self getClassIntroConversationBlockFor:inGame];
    if ([inBlockName isEqualToString:@"GameIntro"])
        return [self getGameIntroConversationBlockFor:inGame];
    if ([inBlockName isEqualToString:@"Question"])
        return [self getQuestionConversationBlockFor:inGame isMuted:inIsMuted];
    if ([inBlockName isEqualToString:@"Answer"])
        return [self getAnswerConversationBlockFor:inGame];
    if ([inBlockName isEqualToString:@"Congrats"])
        return [self getCongratsConversationBlockFor:inGame];
    if ([inBlockName isEqualToString:@"Regrets"])
        return [self getRegretsConversationBlockFor:inGame isMuted:inIsMuted];
    if ([inBlockName isEqualToString:@"Repeat1"])
        return [self getRepeat1ConversationBlockFor:inGame isMuted:inIsMuted];
    if ([inBlockName isEqualToString:@"Repeat2"])
        return [self getRepeat2ConversationBlockFor:inGame isMuted:inIsMuted];
    if ([inBlockName isEqualToString:@"Repeat3"])
        return [self getRepeat3ConversationBlockFor:inGame isMuted:inIsMuted];
    
    // no block found
    return nil;
}

-(FlashConvBlock*)getClassIntroConversationBlockFor:(FlashGame*)inGame {
    FlashConvBlock *returnBlock = [[FlashConvBlock alloc] initWithBlockName:@"ClassIntro" nextBlockName:@"GameIntro" goToNextCard:NO];
    
    // format a date string
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"yyyy年MM月dd日"];
    NSDate *now = [[NSDate alloc] init];
    NSString *dateString = [format stringFromDate:now];
    [now release];
    [format release];
    
    // create first line
    FlashConvLine *firstLine = [[FlashConvLine alloc] init];
    [firstLine.segmentArray addObject:CONVSTRING_WELCOME];
    [returnBlock.lines addObject:firstLine];
    [firstLine release];
    
    return [returnBlock autorelease];
}

-(FlashConvBlock*)getGameIntroConversationBlockFor:(FlashGame*)inGame {
    FlashConvBlock *returnBlock = [[FlashConvBlock alloc] initWithBlockName:@"GameIntro" nextBlockName:@"Question" goToNextCard:NO];
    
    // create first line
    FlashConvLine *firstLine = [[FlashConvLine alloc] init];
    [firstLine.segmentArray addObject:CONVSTRING_GUESSMISSINGWORD];
    [returnBlock.lines addObject:firstLine];
    [firstLine release];
    
    // say % completion
    FlashConvLine *secondLine = [[FlashConvLine alloc] init];
    [secondLine.segmentArray addObject:[NSString stringWithFormat:@"今まで、%3.0f%%完成しました。", [inGame getPercentComplete]]];
    
    // let's go!
    [secondLine.segmentArray addObject:CONVSTRING_LETSGO];
    [returnBlock.lines addObject:secondLine];
    [secondLine release];
    
    return [returnBlock autorelease];
}

-(FlashConvBlock*)getQuestionConversationBlockFor:(FlashGame*)inGame isMuted:(BOOL)inIsMuted {
    FlashConvBlock *returnBlock = [[FlashConvBlock alloc] initWithBlockName:@"Question" nextBlockName:nil goToNextCard:NO];
    // add continue answer
    [returnBlock.answers addObject:[[[FlashConvAnswer alloc] initWithButtonText:@"Continue" jumpToBlockName:@"Answer" isRightAnswer:NO isWrongAnswer:NO] autorelease]];
    
	// get application delegate
    McpAppDelegate *appDelegate = (McpAppDelegate*)[[UIApplication sharedApplication] delegate];
    
    // create first line
    FlashConvLine *firstLine = [[FlashConvLine alloc] init];
    
    // say word in english
    [firstLine.segmentArray addObject:[NSString stringWithFormat:@"「%@」。", [inGame getQuestionUsingSeparator:@", "]]];
    
    // say word type
    [firstLine.segmentArray addObject:[NSString stringWithFormat:@"%@。", [inGame getWordType]]];
    
    // add notes
    if (![[inGame getNotes] isEqualToString:@""] && ![[inGame getNotes] isEqualToString:@"word usually written using kana alone"])
        [firstLine.segmentArray addObject:[NSString stringWithFormat:@"%@。", [inGame getNotes]]];
    
    // add first line to block
    [returnBlock.lines addObject:firstLine];
    [firstLine release];
    
    // create second line
    FlashConvLine *secondLine = [[FlashConvLine alloc] init];
    
    // say sample sentence in english
    SampleEntry *thisSample = [inGame getSampleEntryForJlptLevel:appDelegate.localOptions.jlptLevel pickType:appDelegate.localOptions.pickSentenceType];
    if (thisSample == nil) {
        [secondLine.segmentArray addObject:@"No example found。"];
    }
    else
        [secondLine.segmentArray addObject:[NSString stringWithFormat:@"「%@」。", thisSample.meaning] ];
    
    // add second line to block
    [returnBlock.lines addObject:secondLine];
    [secondLine release];

    /*
    // create instruction
    FlashConvLine *newLine = [[FlashConvLine alloc] init];
    [newLine.segmentArray addObject:@"後について言って、抜けている言葉を埋めてください"];
    [returnBlock.lines addObject:newLine];
    [newLine release];
    */
    
    // create second line
    FlashConvLine *thirdLine = [[FlashConvLine alloc] init];
    
    // say japanese sentence with blank!
    // add example sentence pronunciation
    if (thisSample == nil) {
        // instead of going to next card, have them guess in japanese
        [thirdLine.segmentArray addObject:@"例文を見つけませんでした。でも、日本語でなんと言いますか。"];
    }
    else {
        // get dictionary id and sense index
        int dictionaryId;
        int senseIndex;
        [inGame getDictionaryId:&dictionaryId andSenseIndex:&senseIndex];
        
        // find missing word index, this is global, so will stay until the next question
        missingWordIndex = [thisSample getWordIndexForDictionaryId:dictionaryId andSenseIndex:senseIndex];
        
        // show sample sentence with ruby
        [thirdLine.segmentArray addObject:[NSString stringWithFormat:@"「%@」", [thisSample getKanjiWithRuby:YES includeLinks:NO blankWordIndex:missingWordIndex]]];
        
        /*
        // have user repeat after segments
        [thirdLine.segmentArray addObjectsFromArray:[thisSample getKanjiArrayWithRuby:YES breakUp:YES includeLinks:NO blankDictionaryId:dictionaryId andSenseIndex:senseIndex   ]];
        if (inIsMuted)
            thirdLine.pauseAfterEachSegment = YES;
        else
            thirdLine.answerAfterEachSegment = YES;
         */
    }
    
    // add second line
    [returnBlock.lines addObject:thirdLine];
    [thirdLine release];
    
    return [returnBlock autorelease];
}

-(FlashConvBlock*)getAnswerConversationBlockFor:(FlashGame*)inGame {
    FlashConvBlock *returnBlock = [[FlashConvBlock alloc] initWithBlockName:@"Answer" nextBlockName:nil goToNextCard:NO];
    
    // add answers
    [returnBlock.answers addObject:[[[FlashConvAnswer alloc] initWithButtonText:@"はい" jumpToBlockName:@"Congrats" isRightAnswer:YES isWrongAnswer:NO] autorelease]];
    [returnBlock.answers addObject:[[[FlashConvAnswer alloc] initWithButtonText:@"いいえ" jumpToBlockName:@"Regrets" isRightAnswer:NO isWrongAnswer:YES] autorelease]];
    
	// get application delegate
    McpAppDelegate *appDelegate = (McpAppDelegate*)[[UIApplication sharedApplication] delegate];
    
    // add full sentence
    SampleEntry *thisSample = [inGame getSampleEntryForJlptLevel:appDelegate.localOptions.jlptLevel pickType:appDelegate.localOptions.pickSentenceType];
    if (thisSample == nil) {
        // instead of copping out, say what it is in japanese
        FlashConvLine *firstLine = [[FlashConvLine alloc] init];
        if ([[inGame getNotes] isEqualToString:@"word usually written using kana alone"])
            [firstLine.segmentArray addObject:[NSString stringWithFormat:@"日本語では「%@」です。", [inGame getKanaUsingSeparator:@", "]]];
        else {
            NSString *rubyText = [HtmlFunctions getRubyForKanji:[inGame getKanjiUsingSeparator:@", "] andKana:[inGame getKanaUsingSeparator:@", "]];
            [firstLine.segmentArray addObject:[NSString stringWithFormat:@"日本語では「<vtml_sub alias=\"%@\">%@</vtml_sub>」です。", [inGame getKanaUsingSeparator:@", "], rubyText]];
        }
        [returnBlock.lines addObject:firstLine];
        [firstLine release];
    }
    else {
        // show sample sentence with ruby
        FlashConvLine *firstLine = [[FlashConvLine alloc] init];
        [firstLine.segmentArray addObject:[NSString stringWithFormat:@"「%@」", [thisSample getKanjiWithRuby:YES includeLinks:NO]]];
        firstLine.pauseAfterEachSegment = YES;
        [returnBlock.lines addObject:firstLine];
        [firstLine release];
        
        // don't show meaning... force user to understand the japanese by showing the sentence again!
        /*
        FlashConvLine *thirdLine = [[FlashConvLine alloc] init];
        [thirdLine.segmentArray addObject:[NSString stringWithFormat:@"「%@」", [thisSample getKanjiWithRuby:YES includeLinks:NO]]];
        thirdLine.pauseAfterEachSegment = YES;
        [returnBlock.lines addObject:thirdLine];
        [thirdLine release];*/
        
        /*
        // add sentence meaning
        FlashConvLine *thirdLine = [[FlashConvLine alloc] init];
        [thirdLine.segmentArray addObject:[NSString stringWithFormat:@"「%@」", thisSample.meaning]];
        [returnBlock.lines addObject:thirdLine];
        [thirdLine release];*/
        
        // show sample sentence again
        FlashConvLine *fourthLine = [[FlashConvLine alloc] init];
        [fourthLine.segmentArray addObject:[NSString stringWithFormat:@"「%@」", [thisSample getKanjiWithRuby:YES includeLinks:NO]]];
        [returnBlock.lines addObject:fourthLine];
        [fourthLine release];
    }
    
    // add did u get it right?
    FlashConvLine *fifthLine = [[FlashConvLine alloc] init];
    [fifthLine.segmentArray addObject:CONVSTRING_WEREYOURIGHT];
    [returnBlock.lines addObject:fifthLine];
    [fifthLine release];
    
    return [returnBlock autorelease];
}

-(FlashConvBlock*)getCongratsConversationBlockFor:(FlashGame*)inGame {
    FlashConvBlock *returnBlock = [[FlashConvBlock alloc] initWithBlockName:@"Congrats" nextBlockName:@"Question" goToNextCard:YES];
    
    // create first line
    FlashConvLine *firstLine = [[FlashConvLine alloc] init];
    [firstLine.segmentArray addObject:CONVSTRING_CONGRATULATIONS];
    [returnBlock.lines addObject:firstLine];
    [firstLine release];
    
    return [returnBlock autorelease];
}

-(FlashConvBlock*)getRegretsConversationBlockFor:(FlashGame*)inGame isMuted:(BOOL)inIsMuted {
    // have user repeat sample
    FlashConvBlock *returnBlock = [[FlashConvBlock alloc] initWithBlockName:@"Regrets" nextBlockName:@"Repeat1" goToNextCard:NO];
    
    // create first line
    FlashConvLine *firstLine = [[FlashConvLine alloc] init];
    [firstLine.segmentArray addObject:CONVSTRING_ITSOK];
    if (inIsMuted)
        [firstLine.segmentArray addObject:CONVSTRING_PLEASELISTEN];
    else
        [firstLine.segmentArray addObject:CONVSTRING_PLEASEREPEAT];
    [returnBlock.lines addObject:firstLine];
    [firstLine release];
    
    return [returnBlock autorelease];
}

-(FlashConvBlock*)getRepeat1ConversationBlockFor:(FlashGame*)inGame isMuted:(BOOL)inIsMuted {
	// get application delegate
    McpAppDelegate *appDelegate = (McpAppDelegate*)[[UIApplication sharedApplication] delegate];
    
    // jump to block depending on how many repeats the user asked for
    NSString *jumpToBlock = @"Repeat2";
    if (appDelegate.localOptions.sentenceRepeats == 0)
        jumpToBlock = @"Repeat3";
    
    // create block
    FlashConvBlock *returnBlock = [[FlashConvBlock alloc] initWithBlockName:@"Repeat1" nextBlockName:jumpToBlock goToNextCard:NO];
    
    // add continue answer
    if (!inIsMuted)
        [returnBlock.answers addObject:[[[FlashConvAnswer alloc] initWithButtonText:@"Continue" jumpToBlockName:jumpToBlock isRightAnswer:NO isWrongAnswer:NO] autorelease]];
    
    // make the user repeat the phrase, but break it up by particles and punctuation if
    // it's too long
    
    // create second line
    FlashConvLine *secondLine = [[FlashConvLine alloc] init];
    
    // show sample sentence with ruby
    SampleEntry *thisSample = [inGame getSampleEntryForJlptLevel:appDelegate.localOptions.jlptLevel pickType:appDelegate.localOptions.pickSentenceType];
    if (thisSample != nil) {
        // say as segments in brackets
        NSMutableArray *segments = [NSMutableArray arrayWithArray:[thisSample getKanjiArrayWithRuby:YES breakUp:YES includeLinks:NO blankWordIndex:-1]];
        [segments setObject:[NSString stringWithFormat:@"「%@", [segments objectAtIndex:0]] atIndexedSubscript:0];
        [segments setObject:[NSString stringWithFormat:@"%@」", [segments objectAtIndex:segments.count-1]] atIndexedSubscript:segments.count-1];
        [secondLine.segmentArray addObjectsFromArray:segments];
    }
    else {
        // just say the word
        if ([[inGame getNotes] isEqualToString:@"word usually written using kana alone"])
            [secondLine.segmentArray addObject:[NSString stringWithFormat:@"「%@」。", [inGame getKanaUsingSeparator:@", "]]];
        else {
            NSString *rubyText = [HtmlFunctions getRubyForKanji:[inGame getKanjiUsingSeparator:@", "] andKana:[inGame getKanaUsingSeparator:@", "]];
            [secondLine.segmentArray addObject:[NSString stringWithFormat:@"「<vtml_sub alias=\"%@\">%@</vtml_sub>」。", [inGame getKanaUsingSeparator:@", "], rubyText]];
        }
    }
    [returnBlock.lines addObject:secondLine];
    
    // if not muted, have user repeat after eachs segment
    if (!inIsMuted)
        secondLine.answerAfterEachSegment = YES;
    else
        secondLine.pauseAfterEachSegment = YES;
    
    // add second line
    [secondLine release];
    
    return [returnBlock autorelease];
}


-(FlashConvBlock*)getRepeat2ConversationBlockFor:(FlashGame*)inGame isMuted:(BOOL)inIsMuted {
    // get block and change to save replicating
    FlashConvBlock *returnBlock = [[self getRepeat1ConversationBlockFor:inGame isMuted:inIsMuted] retain];
    returnBlock.blockName = @"Repeat2";
    returnBlock.nextBlockName = @"Repeat3";
    
    // add continue answer
    if (!inIsMuted) {
        [returnBlock.answers removeAllObjects];
        [returnBlock.answers addObject:[[[FlashConvAnswer alloc] initWithButtonText:@"Continue" jumpToBlockName:@"Repeat3" isRightAnswer:NO isWrongAnswer:NO] autorelease]];
    }
        
    return [returnBlock autorelease];
}

-(FlashConvBlock*)getRepeat3ConversationBlockFor:(FlashGame*)inGame isMuted:(BOOL)inIsMuted {
    // get block and change to save replicating
    FlashConvBlock *returnBlock = [[self getRepeat1ConversationBlockFor:inGame isMuted:inIsMuted] retain];
    returnBlock.blockName = @"Repeat3";
    returnBlock.nextBlockName = @"Question";
    returnBlock.goToNextCard = YES;
    
    // add continue answer
    if (!inIsMuted) {
        [returnBlock.answers removeAllObjects];
        [returnBlock.answers addObject:[[[FlashConvAnswer alloc] initWithButtonText:@"Continue" jumpToBlockName:@"Question" isRightAnswer:NO isWrongAnswer:NO] autorelease]];
    }
        
    return [returnBlock autorelease];
}

@end
