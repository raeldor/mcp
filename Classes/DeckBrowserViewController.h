//
//  DeckBrowserViewController.h
//  HF Sensei
//
//  Created by Ray Price on 2/22/13.
//
//

#import <UIKit/UIKit.h>
#import "EditDeckViewControllerDelegate.h"
#import "FlashDeck.h"
#import "SectionBrowserViewControllerDelegate.h"
#import "CloudFileQuery.h"

@interface DeckBrowserViewController : UITableViewController<EditDeckViewControllerDelegate> {
    NSMutableArray *deckList;
    CloudFileQuery *cloudQuery;
}

-(void)refreshDeckList;

@property (nonatomic, assign) id<SectionBrowserViewControllerDelegate> sectionBrowserDelegate;

@end
