//
//  ConverseViewController.h
//  Mcp
//
//  Created by Ray on 11/5/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Chapter.h"
#import "Conversation.h"
#include "vt_jpn_show.h"
#include "vt_jpn_misaki.h"
#include "vt_eng_paul.h"
#include "vt_eng_julie.h"
#import <AVFoundation/AVFoundation.h>
#include "AudioToolbox/AudioQueue.h"
#include "AudioToolbox/AudioToolbox.h"
#include "AudioStates.h"
#import "GrammarConvGame.h"
#import "ShowScoreViewController.h"
#import "GameMenuViewController.h"
#import "MultiSelectControllerDelegate.h"
#import "GrammarTipViewController.h"
#import "UserProfile.h"
#import "AudioRecorder.h"
#import "GameOptionsViewControllerDelegate.h"
#import "SelectViewControllerDelegate.h"
#import "GrammarNote.h"
#import "CalibratePopupController.h"
#import "CalibratePopupDelegate.h"
#import "ConvGameProtocol.h"
#import "DeckPickerView.h"
#import "DeckPickerViewDelegate.h"
#import "SectionPickerViewControllerDelegate.h"
#import "ThoughtMenuViewController.h"
#import "ThoughtMenuViewControllerDelegate.h"
#import "CorrectionViewControllerDelegate.h"
#import "TapToCloseDelegate.h"
#import "ISSpeechRecognition.h"
#import "SpeechKit/SpeechKit.h"
#import "McpAppOptions.h"

typedef enum {
    onResume_ResumeSpeaking = 0,
    onResume_ResumeListening = 1,
    onResume_DoNextAction = 2,
    onResume_DoCurrentAction = 3,
    onResume_DoNothing
} onResumeEnum;

//SKVocalizerDelegate

//SelectViewControllerDelegate, MultiSelectControllerDelegate, UIWebViewDelegate,
@interface ConverseViewController : UIViewController <AVAudioPlayerDelegate, UIWebViewDelegate, GameOptionsViewControllerDelegate, UIAlertViewDelegate, CalibratePopupDelegate, DeckPickerViewDelegate, SectionPickerViewControllerDelegate, ThoughtMenuViewControllerDelegate, UIGestureRecognizerDelegate, CorrectionViewControllerDelegate, TapToCloseDelegate, UIActionSheetDelegate,ISSpeechRecognitionDelegate,SKRecognizerDelegate> {
	IBOutlet UIImageView *backgroundImageView;
	IBOutlet UIButton *menuButton;
	IBOutlet UIImageView *actor1;
	IBOutlet UIImageView *actor2;
	IBOutlet UIImageView *actor3;
	IBOutlet UIImageView *actor4;
	IBOutlet UIWebView *dialogueWebView;
	IBOutlet UILabel *statusLabel;
    IBOutlet UIWebView *toSayWebView;
    IBOutlet UIWebView *saidWebView;
	IBOutlet UILabel *runningScoreLabel;
	IBOutlet UILabel *lastScoreLabel;
    IBOutlet UILabel *bigLabel;
    IBOutlet UIButton *grammarButton; // to change title
    IBOutlet UIBarButtonItem *pauseButton; // to change title
    IBOutlet UIButton *sourceButton; // speaker/headphones/bluetooth
    IBOutlet UIButton *muteButton;
    IBOutlet UIToolbar *menuToolbar;
    IBOutlet UIToolbar *mediaToolbar;
    IBOutlet UIBarButtonItem *correctButton;
    IBOutlet UIBarButtonItem *knownButton;
    IBOutlet UIActivityIndicatorView *activityIndicator;
    
    // voice recognition
	ISSpeechRecognition *isRecognition;
    
    
    // save link to app options
    McpAppOptions *appOptions;
    
    UIImage *playImage;
    UIImage *pauseImage;
    
    AudioRecorder *myRecorder;
	
    UserProfile *userProfile;
    UserProfile *cpuProfile;
    
    CFTimeInterval rewindLastPushedTime; // used to determine rewind or play same again
    
//    BOOL priorPauseState;
    onResumeEnum onResumeAction;
    BOOL gamePaused;
//    BOOL speakingWhenPaused;
//    BOOL listeningWhenPaused;
    BOOL resumeAfterUpdate;
    BOOL resumingGame;
    
    UILongPressGestureRecognizer *longRecognizer;
    ThoughtMenuViewController *thoughtMenu;
    
    UITapGestureRecognizer *tapRecognizer;
    
	AVAudioPlayer *failPlayer;
	AVAudioPlayer *beepPlayer;
	AVAudioPlayer *matchedPlayer;
	AVAudioPlayer *speechPlayer;
	
	UIView *faderView;
	
	CGRect lastScoreFrame;
	
	NSTimer *labelTimer;
	NSTimer *pauseTimer;
	NSTimer *passFailWaitTimer;
    
    NSTimer *blinkTimer;
    int blinkState;
    NSTimer *nextFrameTimer;
    
    NSTimer *lipSyncTimer;
    NSMutableArray *lipSyncArray;
    int lipSyncArrayPos;
	
	SystemSoundID failSoundId;
	SystemSoundID beepSoundId;
	SystemSoundID matchedSoundId;

	id<ConvGameProtocol,FlashConvProtocol> playGame;
    
    int lastCharacterIndex;
	
    NSTimer *hideControlsTimer;
    
	BOOL okToInterrupt; // flag to say ok to interrupt by pressing dialog window
	
	ShowScoreViewController *showScoreController;
	GameMenuViewController *gameMenuController;
	GrammarTipViewController *tipController;
    CalibratePopupController *calibrateController;
    
    NSMutableDictionary *grammarNotes;
    
    NeuralNet *backupNet;
    NeuralNet *oldNet;
    
    BOOL calibrationMode;
    NSString *currentAudioRoute;
    
    DeckPickerView *deckPicker;
    UIView *sectionPickerFader;
}

/*
-(void)checkCalibration;
-(void)startCalibration;
-(void)cancelCalibration;
-(void)calibrate;
-(void)restartGameWithBookNamed:(NSString*)inBookName;
*/

-(void)restartGameWithBookNamed:(NSString*)inBookName completion:(void(^)(BOOL success)) block;
-(void)viewTapped:(id)sender;
-(void)showMenuAndControls;
-(void)hideMenuAndControls;

-(void)viewLongPressed:(id)sender;
-(void)showSectionPicker;
-(void)showDeckPicker;
-(void)hideDeckPicker;
-(void)hidePickerStopped:(id)animationID finished:(BOOL)finished context:(id)context;
-(void)showMenu;
//-(void)updateGrammarNotes;
-(IBAction)menuButtonPressed:(id)sender;
-(IBAction)dialoguePressed:(id)sender;
-(IBAction)muteButtonPressed:(id)sender;
//-(void)showNextLine;
-(GrammarNote*)getGrammarNoteFromTitle:(NSString*)inTitle;
-(void)labelTimerFired:(NSTimer*)inTimer;
-(void)blinkTimerFired:(NSTimer*)inTimer;
-(void)pauseAfterSpeakingTimerFired:(NSTimer*)inTimer;
-(void)pauseAfterListeningTimerFired:(NSTimer*)inTimer;
-(void)pauseAfterActorChangeTimerFired:(NSTimer*)inTimer;
-(void)passWaitTimerFired:(NSTimer*)inTimer;
-(void)failWaitTimerFired:(NSTimer*)inTimer;
-(void)lipSyncTimerFired:(NSTimer*)inTimer;
-(void)gameChanged;
-(void)lineChanged;
-(void)updateWebview;
-(void)speakText:(NSString*)inText isPrompt:(bool)isPrompt usingLanguage:(NSString*)inLanguage;
-(void)startListening;
-(void)showPassAnimation:(float)inScore;
-(void)showFailAnimation:(float)inScore;
-(void)passAnimationStopped:(id)animationID finished:(BOOL)finished context:(id)context;
-(void)failAnimationStopped:(id)animationID finished:(BOOL)finished context:(id)context;
-(CGPoint)getCurrentSegmentCenter;
-(void)pauseAfterConversationFinishedTimerFired:(NSTimer*)inTimer;
-(void)pauseAfterBigLabelTimerFired:(NSTimer*)inTimer;
//-(void)continueButtonPressed:(id)sender;
-(void)optionsButtonPressed:(id)sender;
-(void)resumeButtonPressed:(id)sender;
-(IBAction)backButtonPressed:(id)sender;
-(IBAction)rewindButtonPressed:(id)sender;
-(IBAction)pauseButtonPressed:(id)sender;
-(IBAction)forwardButtonPressed:(id)sender;
-(IBAction)fastForwardButtonPressed:(id)sender;
-(IBAction)grammarButtonPressed:(id)sender;
-(IBAction)correctButtonPressed:(id)sender;
-(IBAction)knownButtonPressed:(id)sender;
-(void)quitButtonPressed:(id)sender;
-(void)calibrateButtonPressed:(id)sender;
-(IBAction)sourceButtonPressed:(id)sender;
-(IBAction)closeTipButtonPressed:(id)sender;
-(IBAction)statusPressed:(id)sender;
-(void)pauseGame;
-(void)resumeGame;
-(void)doCurrentAction;
-(void)doNextAction;
-(void)doPreviousAction;
-(void)updateAudioRouteIcon;
-(void)updateLipSyncArrayFromKana:(NSString*)inKana forSeconds:(float)inSeconds;
-(void)didTapToClose;
-(void)findSaidMatches;

void routeChangeListener (void *inClientData, AudioSessionPropertyID inID, UInt32 inDataSize, const void *inData);

@property (nonatomic, readonly) BOOL isMuted;
@property (nonatomic, assign) int blinkState;
@property (nonatomic, retain) UserProfile *userProfile;
@property (nonatomic, retain) UserProfile *cpuProfile;
@property (nonatomic, retain) UIImageView *backgroundImageView;
@property (nonatomic, retain) UIButton *menuButton;
@property (nonatomic, retain) UIImageView *actor1;
@property (nonatomic, retain) UIImageView *actor2;
@property (nonatomic, retain) UIImageView *actor3;
@property (nonatomic, retain) UIImageView *actor4;
@property (nonatomic, retain) UIWebView *dialogueWebView;
@property (nonatomic, retain) UILabel *statusLabel;
@property (nonatomic, retain) UILabel *runningScoreLabel;
@property (nonatomic, retain) UILabel *lastScoreLabel;
@property (nonatomic, retain) UIButton *grammarButton;
@property (nonatomic, retain) UIBarButtonItem *pauseButton;
@property (nonatomic, retain) UIButton *sourceButton;
@property (nonatomic, assign) id<ConvGameProtocol> playGame;
@property (nonatomic, retain) NSTimer *labelTimer;
@property (nonatomic, retain) NSTimer *pauseTimer;
@property (nonatomic, retain) NSTimer *passFailWaitTimer;
@property (nonatomic, retain) UIView *faderView;
@property (nonatomic, retain) ShowScoreViewController *showScoreController;
@property (nonatomic, retain) GameMenuViewController *gameMenuController;
@property (nonatomic, retain) GrammarTipViewController *tipController;
@property (nonatomic, retain) AVAudioPlayer *speechPlayer;
@property (nonatomic, retain) UILabel *bigLabel;
@property (nonatomic, retain) NSString *currentAudioRoute;
@property (nonatomic, assign) BOOL calibrationMode;

@end
