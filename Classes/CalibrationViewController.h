//
//  CalibrationViewController.h
//  Mcp
//
//  Created by Ray Price on 9/17/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface CalibrationViewController : UIViewController <UITabBarControllerDelegate> {
    IBOutlet UITabBarController *myTabBarController;
    IBOutlet UIViewController *trainingTabViewController;
    IBOutlet UIViewController *calibrationTabViewController;
    
    IBOutlet UIWebView *webView;
    IBOutlet UILabel *samplesLabel;
    
    IBOutlet UILabel *samplesLabel2;
    IBOutlet UILabel *iterationsLabel;
    IBOutlet UILabel *errorSumLabel;
    IBOutlet UILabel *startingRateLabel;
    IBOutlet UIProgressView *startingRateProgress;
    IBOutlet UILabel *newRateLabel;
    IBOutlet UIProgressView *newRateProgress;
}

@end
