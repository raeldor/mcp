//
//  ShowScoreViewController.h
//  Mcp
//
//  Created by Ray on 11/12/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface ShowScoreViewController : UIViewController {
	IBOutlet UIButton *continueButton;
	IBOutlet UILabel *scoreLabel;
}

@property (nonatomic, retain) UIButton *continueButton;
@property (nonatomic, retain) UILabel *scoreLabel;

@end
