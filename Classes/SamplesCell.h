//
//  SamplesCell.h
//  Mcp
//
//  Created by Ray Price on 9/17/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface SamplesCell : UITableViewCell {
    IBOutlet UILabel *samplesLabel;
    IBOutlet UIButton *clearButton;
    IBOutlet UIButton *calibrateButton;
}

@property (nonatomic, retain) UILabel *samplesLabel;
@property (nonatomic, retain) UIButton *clearButton;
@property (nonatomic, retain) UIButton *calibrateButton;

@end
