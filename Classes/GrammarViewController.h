//
//  GrammarViewController.h
//  Mcp
//
//  Created by Ray on 1/13/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GrammarNote.h"
#import "GrammarViewSelectDelegate.h"

@interface GrammarViewController : UIViewController {
	id<GrammarViewSelectDelegate> selectDelegate;

	IBOutlet UIWebView *noteWebView;
	
	GrammarNote *grammarNote;
    BOOL allowSelect;
}

@property (nonatomic, retain) UIWebView *noteWebView;
@property (nonatomic, retain) GrammarNote *grammarNote;
@property (nonatomic, assign) id<GrammarViewSelectDelegate> selectDelegate;

-(void)setViewNote:(GrammarNote*)inNote allowSelect:(BOOL)inAllowSelect;
-(void)selectButtonClicked:(id)sender;

@end
