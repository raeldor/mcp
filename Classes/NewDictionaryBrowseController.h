//
//  NewDictionaryBrowseController.h
//  HF Sensei
//
//  Created by Ray on 4/5/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JapaneseDictionary.h"

@interface NewDictionaryBrowseController : UITableViewController <UITableViewDelegate, UITableViewDataSource, UISearchResultsUpdating> {
    //UISearchBarDelegate, UISearchDisplayDelegate, 
//    UIActivityIndicatorView *activityIndicator;
//    UIActivityIndicatorView *activityIndicator2;
    JapaneseDictionary *japaneseDictionary;
	
	// search results
    NSTimer *searchTimer;
    BOOL performingSearch;
//    BOOL isSearchResultsLoaded; // loaded yet?
    BOOL filterIsActive; // when displaying filtered results without showing search bar
	NSMutableArray *searchResults; // array of indexes into the actual list

    // save keyboard height
//    int keyboardHeight;
}

@property (nonatomic, assign) UISearchController *searchController;

-(NSUInteger)numberOfEntriesInGroup:(NSUInteger)groupIndex;
//-(void)updateSearchResultsUsingSearchController:(UISearchDisplayController*)controller;
//-(void)keyboardWillShow:(NSNotification *)inNotification;

@end

