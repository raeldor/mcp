//
//  Background.h
//  Mcp
//
//  Created by Ray on 10/19/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DataFile.h"

@interface Background : DataFile <DataFileDelegate> {
	NSString *backgroundName;
	UIImage *image;
	BOOL allowDelete;
}

@property (nonatomic, retain) NSString *backgroundName;
@property (nonatomic, retain) UIImage *image;

-(id)init;
-(id)initFromArchive:(NSString*)inName;
-(void)archiveFieldsUsingArchiver:(NSKeyedArchiver*)inArchiver;
-(void)unarchiveFieldsUsingUnarchiver:(NSKeyedUnarchiver*)inUnarchiver;

@end
