//
//  SamplesCell.m
//  Mcp
//
//  Created by Ray Price on 9/17/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "SamplesCell.h"

@implementation SamplesCell

@synthesize samplesLabel;
@synthesize clearButton;
@synthesize calibrateButton;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(IBAction)clearButtonPressed:(id)sender {
    
}

- (void)dealloc
{
    [samplesLabel release];
    [clearButton release];
    [calibrateButton release];
    [super dealloc];
}

@end
