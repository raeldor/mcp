//
//  PlayPartCell.h
//  Mcp
//
//  Created by Ray on 5/15/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface PlayPartCell : UITableViewCell {
    IBOutlet UISegmentedControl *playSegment;
}

@property (nonatomic, retain) UISegmentedControl *playSegment;

@end
