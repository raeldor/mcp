//
//  EditSectionViewControllerDelegate.h
//  HF Sensei
//
//  Created by Ray Price on 2/24/13.
//
//

#ifndef HF_Sensei_EditSectionViewControllerDelegate_h
#define HF_Sensei_EditSectionViewControllerDelegate_h

@protocol EditSectionViewControllerDelegate

-(void)didUpdateObject:(id)updatedObject key:objectKey;
-(void)didAddNewObject:(id)newObject;

@end

#endif
