//
//  Book.m
//  Mcp
//
//  Created by Ray on 10/23/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "Book.h"
#import "McpAppDelegate.h"

@implementation Book

@synthesize uniqueKey;
@synthesize bookName;
@synthesize image;
@synthesize chapters;

-(id)init {
    // init super
    self = [super initWithExtension:@".book" delegate:self];
	if (self) {
        self.uniqueKey = [McpAppDelegate getUuid];
		chapters = [[NSMutableArray alloc] init];
	}
	return self;
}

-(id)initFromArchive:(NSString*)inName {
	if ((self = [super initFromArchive:inName extension:@".book" delegate:self])) {
	}
	return self;
}

-(void)archiveFieldsUsingArchiver:(NSKeyedArchiver*)inArchiver {
	// archive fields
	[inArchiver encodeObject:uniqueKey forKey:@"UniqueKey"];
	[inArchiver encodeObject:bookName forKey:@"BookName"];
	NSData *imageData = UIImagePNGRepresentation(image);
	[inArchiver encodeObject:imageData forKey:@"Image"];
	[inArchiver encodeObject:chapters forKey:@"Chapters"];
	if (allowDelete)
		[inArchiver encodeObject:@"Yes" forKey:@"AllowDelete"];
	else
		[inArchiver encodeObject:@"No" forKey:@"AllowDelete"];
}

-(void)unarchiveFieldsUsingUnarchiver:(NSKeyedUnarchiver*)inUnarchiver {	
	// unarchive fields
    self.uniqueKey = [inUnarchiver decodeObjectForKey:@"UniqueKey"];
    // remove later
    if (uniqueKey == nil)
        self.uniqueKey = [McpAppDelegate getUuid];
	self.bookName = [inUnarchiver decodeObjectForKey:@"BookName"];
	self.chapters = [inUnarchiver decodeObjectForKey:@"Chapters"];
	NSData *imageData = [inUnarchiver decodeObjectForKey:@"Image"];
	UIImage *thisImage = [[UIImage alloc] initWithData:imageData];
	self.image = thisImage;
	[thisImage release];
	NSString *strAllowDelete = [inUnarchiver decodeObjectForKey:@"AllowDelete"];
	if ([strAllowDelete isEqualToString:@"Yes"])
		allowDelete = YES;
	else
		allowDelete = NO;
}

-(void)dealloc {
    [uniqueKey release];
	[bookName release];
	[image release];
	[chapters release];
	[super dealloc];
}

@end
