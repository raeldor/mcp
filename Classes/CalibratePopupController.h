//
//  CalibratePopupController.h
//  Mcp
//
//  Created by Ray Price on 10/24/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CalibratePopupDelegate.h"

@interface CalibratePopupController : UIViewController {
    IBOutlet UILabel *rateLabel;
    IBOutlet UIProgressView *progressView;
    
    id<CalibratePopupDelegate> popupDelegate;

    BOOL isCalibrating;
    BOOL stopCalibrating;
    float currentLowestScore;
    NSTimer *updateTimer;
    NSTimer *finishTimer;
}

@property (nonatomic, retain) UILabel *rateLabel;
@property (nonatomic, retain) UIProgressView *progressView;
@property (nonatomic, retain) id<CalibratePopupDelegate> popupDelegate;

-(void)startCalibration;
-(void)doCalibrate;
-(float)getTrainingSamplesLowestScore;
-(int)getTotalSampleCount;
-(void)updateTimerFired:(NSTimer*)inTimer;
-(void)finishTimerFired:(NSTimer*)inTimer;
-(IBAction)cancelPushed:(id)sender;

@end
