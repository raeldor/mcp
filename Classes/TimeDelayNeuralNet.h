//
//  TimeDelayNeuralNet.h
//  Mcp
//
//  Created by Ray on 2/6/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TimeDelayNeuralNet : NSObject <NSCoding, NSCopying> {
	int inputCount;
	int outputCount;
	int hiddenLayerCount;
    int maxTimeDelay1;
    int maxTimeDelay2;
	int neuronsPerHiddenLayer1;
	int neuronsPerHiddenLayer2;
	double learningRate;
	double bias;
	double momentumDelta;
	double activationResponse;
	bool trained;
    
	double errorSum;
	int numEpochs;
    
	NSMutableArray *layers;
}

-(id)initWithInputCount:(int)inInputCount outputCount:(int)inOutputCount hiddenLayerCount:(int)inHiddenLayerCount neuronsPerHiddenLayer1:(int)inNeuronsPerHiddenLayer1 maxTimeDelay1:(int)inMaxTimeDelay1 neuronsPerHiddenLayer2:(int)inNeuronsPerHiddenLayer2 maxTimeDelay2:(int)inMaxTimeDelay2 learningRate:(double)inLearningRate;
-(void)resetWeights;
-(void)dealloc;
-(double*)updateWithInputs:(double*)inInputs;
-(double)sigmoidWithInput:(double)inInput response:(double)inResponse;
-(bool)trainNetworkWithInputSet:(double*)inInputSet ofSize:(int)inSetSize andOutputSet:(double*)inOutputSet;
-(bool)trainNetworkOnceWithInputSet:(double*)inInputSet ofSize:(int)inSetSize andOutputSet:(double*)inOutputSet;

@property (nonatomic, retain) NSMutableArray *layers;
@property (nonatomic, assign) int inputCount;
@property (nonatomic, assign) int outputCount;
@property (nonatomic, assign) int hiddenLayerCount;
@property (nonatomic, assign) int maxTimeDelay1;
@property (nonatomic, assign) int maxTimeDelay2;
@property (nonatomic, assign) int neuronsPerHiddenLayer1;
@property (nonatomic, assign) int neuronsPerHiddenLayer2;
@property (nonatomic, assign) double bias;
@property (nonatomic, assign) double activationResponse;
@property (nonatomic, assign) double learningRate;
@property (nonatomic, assign) double errorSum;
@property (nonatomic, assign) bool trained;
@property (nonatomic, assign) int numEpochs;

@end
