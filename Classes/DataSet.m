//
//  DataSet.m
//  Mcp
//
//  Created by Ray on 3/20/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "DataSet.h"


@implementation DataSet

@synthesize dataPoints;

-(id)init {
	if (self = [super init]) {
		self.dataPoints = [NSMutableSet set];
	}
	return self;
}

-(DataPoint*)getNextUnvisitedDataPoint {
	// loop through all objects in set
	NSEnumerator *enumerator = [dataPoints objectEnumerator];
	DataPoint *thisDataPoint;
	while ((thisDataPoint = [enumerator nextObject])) {
		if (!thisDataPoint.visited)
			return thisDataPoint;
	}
	return nil;
}

-(void)addDataPoint:(DataPoint*)inDataPoint {
	[dataPoints addObject:inDataPoint];
}

-(void)addPointsFromDataSet:(DataSet*)inDataSet {
	[dataPoints unionSet:inDataSet.dataPoints];
}

-(DataSet*)getNeighborsOfPoint:(DataPoint*)inDataPoint UsingEps:(float)inEps {
	// for now just loop throgh all objects, but later improve performance
	DataSet *newSet = [[DataSet alloc] init];
	NSEnumerator *enumerator = [dataPoints objectEnumerator];
	DataPoint *thisDataPoint;
	while (thisDataPoint = [enumerator nextObject]) {
		if (thisDataPoint != inDataPoint && [thisDataPoint distanceFromDataPoint:inDataPoint] < inEps)
			[newSet.dataPoints addObject:thisDataPoint];
	}
	
	return [newSet autorelease];
}

-(void)dealloc {
	[dataPoints release];
	[super dealloc];
}

@end
