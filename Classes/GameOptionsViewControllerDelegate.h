/*
 *  GameOptionsDelegate.h
 *  Mcp
 *
 *  Created by Ray on 1/18/11.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */

#import "DialogueLine.h"
#import "McpLocalOptions.h"

@protocol GameOptionsViewControllerDelegate

-(void)didUpdateOptions;
-(void)didCancelOptions;

@end