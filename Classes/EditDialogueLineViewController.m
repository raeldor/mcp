//
//  EditDialogueLineViewController.m
//  Mcp
//
//  Created by Ray Price on 10/20/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "EditDialogueLineViewController.h"
#import "McpAppDelegate.h"
#import "GrammarPickerViewController.h"

@implementation EditDialogueLineViewController

@synthesize webView;
@synthesize thisLine;
@synthesize editDialogueLineDelegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)setEditableObject:(DialogueLine*)inLine isNew:(BOOL)isNew {
	// save new flag
	newMode = isNew;
	
	// make a copy of this object
	thisLine = [inLine copy];
	
	// set title
	self.title = @"Edit Line";
	
	// this table is always in edit mode
	// we are not adding delete rows, so we don't need edit mode
	//	[self setEditing:YES animated:NO];
	
	// create cancel button
	UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(cancelButtonClick:)];
	self.navigationItem.leftBarButtonItem = cancelButton;
	[cancelButton release];
	
	// create save button
	UIBarButtonItem *saveButton = [[UIBarButtonItem alloc] initWithTitle:@"Save" style:UIBarButtonItemStyleBordered target:self action:@selector(saveButtonClick:)];
	self.navigationItem.rightBarButtonItem = saveButton;
	[saveButton release];
    
	// change edit menu to add link grammar
    UIMenuItem *linkMenu = [[UIMenuItem alloc] initWithTitle:@"Link" action:@selector(linkGrammar:)];
    UIMenuItem *unlinkMenu = [[UIMenuItem alloc] initWithTitle:@"Unlink" action:@selector(unlinkGrammar:)];
	NSArray *newItems = [NSArray arrayWithObjects:linkMenu, unlinkMenu, nil];
    [linkMenu release];
    [unlinkMenu release];
	[UIMenuController sharedMenuController].menuItems = newItems;
}

-(void)setChapterAs:(Chapter*)inChapter {
    chapter = [inChapter retain];
}

-(BOOL)isOkToSave {
	BOOL stillOk = YES;
	NSString *msg;
	
    // copy web content to dialogue line
    NSString *test = [webView stringByEvaluatingJavaScriptFromString: 
                      @"document.body.innerHTML"];
    test = [test stringByReplacingOccurrencesOfString:@"<div contenteditable=\"true\">" withString:@""];
    test = [test stringByReplacingOccurrencesOfString:@"</div>" withString:@""];
    thisLine.line = test;
    
	// check fields are filled
    if (thisLine.line == nil || [thisLine.line isEqualToString:@""]) {
        msg = @"Dialogue line text must be completed.";
        stillOk = NO;
    }
	
	// display error if not ok
	if (!stillOk) {
		UIAlertView *myAlert = [[UIAlertView alloc] initWithTitle:@"Oops" message:msg delegate:nil cancelButtonTitle:@"Understood" otherButtonTitles:nil];
		[myAlert show];
		[myAlert release];
	}
	
	return stillOk;
}

-(void)saveButtonClick:(id)sender {
	// check first for errors
	if ([self isOkToSave])
	{
		// notify parent of change
		if (newMode)
			[editDialogueLineDelegate didAddDialogueLine:thisLine];
		else
			[editDialogueLineDelegate didUpdateDialogueLine:thisLine];
		
		// pop controller
		if (newMode) {
			[self dismissModalViewControllerAnimated:YES];
		}
		else {
			[self.navigationController popViewControllerAnimated:TRUE];
		}
	}
}

-(void)cancelButtonClick:(id)sender {
	// pop controller
	if (newMode) {
		[self dismissModalViewControllerAnimated:YES];
	}
	else {
		[self.navigationController popViewControllerAnimated:TRUE];
	}
}

-(BOOL)canPerformAction:(SEL)action withSender:(id)sender {
    if (action == @selector(linkGrammar:)) {
//		if (webViewBeingEdited != nil) {
            return YES;
            /*
             if ([textViewBeingEdited selectedRange].length != 0)
             return YES;
             else
             return NO;
             */
//		}
//		else
//			return NO;
    }
	
    return [super canPerformAction:action withSender:sender];
}

-(void)linkGrammar:(UIMenuController*)sender {
    // use action sheet so webview keeps focus
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil 
                                                             delegate:nil
                                                    cancelButtonTitle:nil
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:nil];
    [actionSheet setActionSheetStyle:UIActionSheetStyleBlackTranslucent];
    
    // our grammar picker
    GrammarPickerViewController *myPicker = [[GrammarPickerViewController alloc] initWithNibName:@"GrammarPickerViewController" bundle:nil];
    myPicker.selectDelegate = self;
    [myPicker setActionSheet:actionSheet];
    [myPicker setChapterAs:chapter];
    
    // add our view and show action sheet, then set bounds
    [actionSheet addSubview:myPicker.view];
    [actionSheet showInView:self.view];
    [actionSheet setBounds:CGRectMake(0, 0, 320, 485)];
    
    // release picker
    //    [myPicker release];
    [actionSheet release];
    
    /*
     // save text view and selected range
     linkTextView = textViewBeingEdited;
     linkRange = textViewBeingEdited.selectedRange;
     
     // bring up grammar select window
     GrammarPickerController *picker = [[GrammarPickerController alloc] init];
     picker.selectDelegate = self;
     [self.navigationController pushViewController:picker animated:YES];
     [picker release];
     */
}

-(void)didCancelSelection {
}

-(void)unlinkGrammar:(UIMenuController*)sender {
    // unlink selection
    [webView stringByEvaluatingJavaScriptFromString:@"document.execCommand(\"Unlink\")"];
}

-(void)didMakeSelection:(int)inSelection {
	// find name of grammar note
	NSString *noteTitle = [[chapter.grammarNotes objectAtIndex:inSelection] title];
	
    // link the grammar
	NSString *href = [noteTitle stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *command = [NSString stringWithFormat:@"document.execCommand(\"CreateLink\", false, \"file://%@\")", href];
    [webView stringByEvaluatingJavaScriptFromString:command];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
    // set html
    NSString *htmlString = [NSString stringWithFormat:@"<html><div contenteditable=\"true\"><body>%@</body></div></html>", thisLine.line];
    [webView loadHTMLString:htmlString baseURL:nil];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)dealloc {
    [chapter release];
	[webView release];
    [thisLine release];
    [super dealloc];
}


@end
