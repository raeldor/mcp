//
//  EditDeckViewControllerDelegate.h
//  HF Sensei
//
//  Created by Ray Price on 2/23/13.
//
//

#ifndef HF_Sensei_EditDeckViewControllerDelegate_h
#define HF_Sensei_EditDeckViewControllerDelegate_h

@protocol EditDeckViewControllerDelegate

-(void)didUpdateObject:(id)updatedObject key:oldKey;
-(void)didAddNewObject:(id)newObject;

@end

#endif
