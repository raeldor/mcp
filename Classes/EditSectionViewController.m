//
//  EditSectionViewController.m
//  HF Sensei
//
//  Created by Ray Price on 2/23/13.
//
//

#import "EditSectionViewController.h"
#import "FlashCard.h"
#import "KanjiSelectController.h"
#import "DictionarySelectController.h"
#import "SentenceSelectController.h"
#import "KanjiListCell.h"
#import "DictionaryListCell.h"
#import "SectionNameCell.h"
#import "SectionTypeCell.h"
#import "TextFunctions.h"

@interface EditSectionViewController ()

@end

@implementation EditSectionViewController

@synthesize delegate;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // always in edit mode
    [self setEditing:YES animated:NO];
    
    // base height on autolayout content
    self.tableView.estimatedRowHeight = 89.0f;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.searchDisplayController.searchResultsTableView.estimatedRowHeight = 89.0f;
    self.searchDisplayController.searchResultsTableView.rowHeight = UITableViewAutomaticDimension;
    
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

-(void)setSectionTo:(DeckSection*)inSection {
    // save deck reference or create a new deck
    if (inSection != nil) {
        isNewSection = NO;
        originalSection = [inSection retain];
        editSection = [inSection copy]; // make a copy so we can cancel!
    }
    else {
        isNewSection = YES;
        editSection = [[DeckSection alloc] init];
        originalSection = [[editSection copy] retain];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)cancelPushed:(id)sender {
    // pop view controller
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)savePushed:(id)sender {
    // deck name must be filled
    if (editSection.sectionName == nil || [editSection.sectionName isEqualToString:@""]) {
        // print error  message
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Section Name Missing" message:@"Section name must be completed and cannot be empty." delegate:nil cancelButtonTitle:@"Understood" otherButtonTitles:nil];
        [alert show];
        [alert release];
        return;
    }
    
    // update section and save
    originalSection.sectionName = editSection.sectionName;
    originalSection.sectionType = editSection.sectionType;

    // copy changes to cards
    [originalSection.cards removeAllObjects];
    for (int i=0; i < editSection.cards.count; i++) {
        FlashCard *thisCard = [editSection.cards objectAtIndex:i];
        [originalSection.cards addObject:thisCard];
    }
    
    // call delegate
    if (isNewSection)
        [delegate didAddNewObject:originalSection];
    else
        [delegate didUpdateObject:originalSection key:editSection.sectionName];
    
    // pop view controller
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)sectionNameChanged:(UITextField*)sender {
    // update name
    editSection.sectionName = sender.text;
}

-(IBAction)sectionTypeChanged:(UISegmentedControl*)sender {
    // set section
    switch (sender.selectedSegmentIndex) {
        case 0: // kanji
            editSection.sectionType = @"Kanji";
            break;
        case 1: // word
            editSection.sectionType = @"Dictionary";
            break;
        default: // example
            editSection.sectionType = @"Phrase";
            break;
    }
}

#pragma mark - Table view data source

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 2;
}

/*
-(NSArray*)sectionIndexTitlesForTableView:(UITableView *)tableView {
    return [NSArray arrayWithObjects:@"General", "Cards", nil];
}*/

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if (section == 0)
        return 2;
    else
        return editSection.cards.count+1;
}

-(BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // pass static stuff on
    if (indexPath.section == 0)
        return NO;
    else
        return YES;
}

-(void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
    // pass static stuff on
    if (fromIndexPath.section == 0)
        return;
    else {
        // get from and to row
        NSUInteger fromRow = [fromIndexPath row];
        NSUInteger toRow = [toIndexPath row];
        
        // if row is before the 'add new' row
        if (toRow < editSection.cards.count) {
            // swap these rows in the array
            FlashCard *moveCard = [[editSection.cards objectAtIndex:fromRow] retain];
            [editSection.cards removeObjectAtIndex:fromRow];
            [editSection.cards insertObject:moveCard atIndex:toRow];
            [moveCard release];
        }
        
        // reload in case we didn't move it
        [tableView reloadData];
    }
}

-(UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    // pass static stuff on
    if (indexPath.section == 0)
        return UITableViewCellEditingStyleNone;
    else {
        if (indexPath.row < editSection.cards.count)
            return UITableViewCellEditingStyleDelete;
        else
            return UITableViewCellEditingStyleInsert;
    }
}

/*
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    // pass static stuff on
    if (indexPath.section == 0) {
        if (indexPath.row == 0)
            return 88;
        else
            return 93;
    }
    else
        return 44;
}*/

-(NSInteger)tableView:(UITableView *)tableView indentationLevelForRowAtIndexPath:(NSIndexPath *)indexPath {
    // pass static stuff on
    if (indexPath.section == 0)
        return 0;
    else
        return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // if this is the static section
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            // get cell
            SectionNameCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SectionNameCell"];
            
            // set section name
            cell.sectionNameTextField.text = editSection.sectionName;
            
            return cell;
        }
        else {
            // get cell
            SectionTypeCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SectionTypeCell"];
            
            // set section type
            if ([editSection.sectionType isEqualToString:@"Kanji"])
                cell.sectionTypeSegment.selectedSegmentIndex = 0;
            else {
                if ([editSection.sectionType isEqualToString:@"Dictionary"])
                    cell.sectionTypeSegment.selectedSegmentIndex = 1;
                else
                    cell.sectionTypeSegment.selectedSegmentIndex = 2;
            }
            
            return cell;
        }
    }
    
    // is this a card or add new line?
    if (indexPath.row < editSection.cards.count) {
        if ([editSection.sectionType isEqualToString:@"Kanji"]) {
            // get kanji cell
            static NSString *CellIdentifier = @"KanjiCell";
            KanjiListCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            
            // set up cell
            FlashCard *thisCard = [editSection.cards objectAtIndex:indexPath.row];
            cell.kanjiLabel.text = thisCard.kanji;
            cell.meaningLabel.text = [thisCard getMeaningsUsingSeperator:@", "];
            
            // try and get readings
            KanjiEntry *thisEntry = [[KanjiDictionary getSingleton] getEntryUsingKanji:thisCard.kanji];
            if (thisEntry != nil)
                cell.readingLabel.text = [thisEntry getAllReadingsUsingSeperator:@", "];
            
            return cell;
        }
        else {
            // get dictionary cell
            static NSString *CellIdentifier = @"DictionaryCell";
            DictionaryListCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            
            // generate markup from individual components
            // returns 0=kana prefix, 1 = kanji, 2= furigana, 3 = kana suffix
            FlashCard *thisCard = [editSection.cards objectAtIndex:indexPath.row];
            NSArray *components = [TextFunctions getFuriganaComponentsFromKanji:thisCard.kanji andKana:thisCard.kana];
            NSString *markup = [NSString stringWithFormat:@"%@[%@{%@}]%@", components[0], components[1], components[2], components[3]];
            
            // set font manually
            //    cell.topRightLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:24.0f];
            cell.topRightLabel.font = [UIFont boldSystemFontOfSize:24.0f];
            
            // set up cell
            cell.topLeftLabel.text = thisCard.kanji;
            cell.topRightLabel.markupText = markup;
//            cell.topRightLabel.text = thisCard.kana;
            cell.grayLabel.text = [thisCard getMeaningsUsingSeperator:@", "];
            
            return cell;
        }
    }
    else {
        // get standard cell
        static NSString *CellIdentifier = @"AddCell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        return cell;
    }
}

// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // if this is the static section
    if (indexPath.section == 0)
        return NO;
    else
        return YES;
}


// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    // if we are deleting rows
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
		[editSection.cards removeObjectAtIndex:[indexPath row]];
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:YES];
    }
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
		if ([editSection.sectionType isEqualToString:@"Kanji"]) {
            // show kanji browser
            KanjiSelectController *kanjiBrowser=[[self storyboard] instantiateViewControllerWithIdentifier:@"KanjiSelectController"];
            kanjiBrowser.selectDelegate = self;
			UINavigationController *secondNavigationController = [[UINavigationController alloc] initWithRootViewController:kanjiBrowser];
            [self.navigationController presentViewController:secondNavigationController animated:YES completion:nil];
//            [self.navigationController presentModalViewController:secondNavigationController animated:YES];
			[secondNavigationController release];
		}
		else {
            if ([editSection.sectionType isEqualToString:@"Dictionary"]) {
                // show dictionary browser
                DictionarySelectController *dictionaryBrowser=[[self storyboard] instantiateViewControllerWithIdentifier:@"DictionarySelectController"];
                dictionaryBrowser.selectDelegate = self;
                UINavigationController *secondNavigationController = [[UINavigationController alloc] initWithRootViewController:dictionaryBrowser];
                [self.navigationController presentViewController:secondNavigationController animated:YES completion:nil];
//                [self.navigationController presentModalViewController:secondNavigationController animated:YES];
                [secondNavigationController release];
			}
			else {
                // show sentence browser
                SentenceSelectController *sentenceBrowser=[[self storyboard] instantiateViewControllerWithIdentifier:@"SentenceSelectController"];
                sentenceBrowser.selectDelegate = self;
                UINavigationController *secondNavigationController = [[UINavigationController alloc] initWithRootViewController:sentenceBrowser];
                [self.navigationController presentViewController:secondNavigationController animated:YES completion:nil];
//                [self.navigationController presentModalViewController:secondNavigationController animated:YES];
                [secondNavigationController release];
            }
		}
	}
}

-(void)didMakeBrowserSelection:(NSMutableArray*)selection {
	// add to cards
    for (FlashCard *newCard in selection) {
        //	for (NSString* key in selection) {
        NSString *key = [NSString stringWithFormat:@"%@%@", newCard.kanji, newCard.kana];
        // make sure card does not already exist
        //		FlashCard *newCard = [selection objectForKey:key];
        NSMutableArray *cards = (NSMutableArray*)editSection.cards;
        BOOL alreadyExists = NO;
        for (int i=0; i < cards.count; i++) {
            NSString *cardKey = [NSString stringWithFormat:@"%@%@", [[cards objectAtIndex:i] kanji], [[cards objectAtIndex:i] kana]];
            if ([cardKey isEqualToString:key]) {
                // show warning
                UIAlertView *myAlert = [[UIAlertView alloc] initWithTitle:@"Oops" message:[NSString stringWithFormat:@"Card for '%@ (%@)' already exists in this section.  Card will not be added.", [[cards objectAtIndex:i] kanji], [[cards objectAtIndex:i] kana]] delegate:nil cancelButtonTitle:@"Understood" otherButtonTitles:nil];
                [myAlert show];
                [myAlert release];
                
                // set flag
                alreadyExists = YES;
                break;
            }
        }
        
        // add card
        if (!alreadyExists)
            [editSection.cards addObject:newCard];
	}
	
	// redisplay table
	[self.tableView reloadData];
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // pass static stuff on
    if (indexPath.section == 0)
        return [super tableView:tableView didSelectRowAtIndexPath:indexPath];
    
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     [detailViewController release];
     */
}

-(void)dealloc {
    // release our retains
    [originalSection release];
    [editSection release];
    
    // call super
    [super dealloc];
}

@end
