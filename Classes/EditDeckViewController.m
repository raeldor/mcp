//
//  EditDeckViewController.m
//  HF Sensei
//
//  Created by Ray Price on 2/23/13.
//
//

#import "EditDeckViewController.h"

@interface EditDeckViewController ()

@end

@implementation EditDeckViewController

@synthesize delegate;
@synthesize deckNameTextField;
@synthesize deckImageView;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)setDeckTo:(FlashDeck*)inDeck {
    // save deck reference or create a new deck
    if (inDeck != nil) {
        isNewDeck = NO;
        editDeck = [inDeck retain];
    }
    else {
        isNewDeck = YES;
        editDeck = [[FlashDeck alloc] init];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // base height on autolayout content
    self.tableView.estimatedRowHeight = 128;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    
    // set deck name and image
    deckNameTextField.text = editDeck.deckName;
    deckImageView.image = editDeck.image;
}

-(void)didTouchImage:(id)sender {
    // create controller and set title
    UIActionSheet *action = [[UIActionSheet alloc] init];
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary])
        [action addButtonWithTitle:@"Choose Existing Photo"];
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
        [action addButtonWithTitle:@"Take Photo"];
    if (deckImageView.image != nil)
        [action addButtonWithTitle:@"Delete Photo"];
    [action addButtonWithTitle:@"Cancel"];
    action.cancelButtonIndex = action.numberOfButtons-1;
    action.delegate = self;
    [action showInView:self.view];
    [action release];
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
	// use button title
	NSString *buttonTitle = [actionSheet buttonTitleAtIndex:buttonIndex];
    
	// ipad?
	BOOL isIpad = NO;
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 30200
	if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
		isIpad = YES;
#endif
	
	// if we're using picker
	if ([buttonTitle isEqualToString:@"Choose Existing Photo"] || [buttonTitle isEqualToString:@"Take Photo"]) {
		// use image picker
		UIImagePickerController	*picker = [[UIImagePickerController alloc] init];
		picker.delegate = self;
		picker.sourceType = [buttonTitle isEqualToString:@"Choose Existing Photo"] ? UIImagePickerControllerSourceTypePhotoLibrary:UIImagePickerControllerSourceTypeCamera;
        
		// embed in popover for iPad
		if (isIpad) {
			UIPopoverController *myPopOver = [[NSClassFromString(@"UIPopoverController") alloc] initWithContentViewController:picker];
			[myPopOver presentPopoverFromRect:CGRectMake(self.view.frame.size.width, self.view.frame.size.height, 0, 0) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
			popOver = [myPopOver retain];
			[myPopOver release];
		}
		else {
            [self presentViewController:picker animated:YES completion:nil];
//            [self presentModalViewController:picker animated:YES];
		}
		// release picker
		[picker release];
	}
	if ([buttonTitle isEqualToString:@"Delete Photo"]) {
        deckImageView.image = nil;
		[self.tableView reloadData];
	}
}

//-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
//#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 30200
//	if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
//		[popOver release];
//#endif
//}

-(void)imagePickerController:(UIImagePickerController*)picker didFinishPickingImage:(UIImage*)image editingInfo:(NSDictionary*)editingInfo {
	// ipad?
	BOOL isIpad = NO;
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 30200
	if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
		isIpad = YES;
#endif
	
	// save image, but resize to 57 x 76
	CGSize newSize = CGSizeMake(57,76);
	UIGraphicsBeginImageContext(newSize	);
	[image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
	UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
    deckImageView.image = newImage;
    
	// close picker and refresh view
	if (isIpad) {
        //		[popOver release];
	}
	else {
        [picker.presentingViewController dismissViewControllerAnimated:YES completion:nil];
//		[picker dismissModalViewControllerAnimated:YES];
	}
	[self.tableView reloadData];
}

-(IBAction)cancelPushed:(id)sender {
    // close deck before we exit
    if (editDeck.documentState != UIDocumentStateClosed) {
        [editDeck closeWithCompletionHandler:^(BOOL success) {
            // pop view controller
            [self.navigationController popViewControllerAnimated:YES];
        }];
    }
    else
        // pop view controller
        [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)savePushed:(id)sender {
    // deck name must be filled
    if (deckNameTextField.text == nil || [deckNameTextField.text isEqualToString:@""]) {
        // print error  message
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Deck Name Missing" message:@"Deck name must be completed and cannot be empty." delegate:nil cancelButtonTitle:@"Understood" otherButtonTitles:nil];
        [alert show];
        [alert release];
        return;
    }
    
    // update deck and save
    NSString *oldDeckName = [editDeck.deckName copy];
    editDeck.deckName = deckNameTextField.text;
    editDeck.image = deckImageView.image;
    
    // save deck
    [editDeck saveCheckingOldName:oldDeckName completion:^() {
        // close deck before we exit
        if (editDeck.documentState != UIDocumentStateClosed) {
            [editDeck closeWithCompletionHandler:^(BOOL success) {
                // call delegate when done
                if (isNewDeck) {
                    // inform delegate of new deck
                    [delegate didAddNewObject:editDeck];
                }
                else {
                    // inform delegate
                    [delegate didUpdateObject:editDeck key:oldDeckName];
                }
                
                // pop view controller
                [self.navigationController popViewControllerAnimated:YES];
            }];
        }
        else {
            // call delegate when done
            if (isNewDeck) {
                // inform delegate of new deck
                [delegate didAddNewObject:editDeck];
            }
            else {
                // inform delegate
                [delegate didUpdateObject:editDeck key:oldDeckName];
            }
            
            // pop view controller
            [self.navigationController popViewControllerAnimated:YES];
        }
    }];
        
    // release saved name
    [oldDeckName release];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

/*
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
#warning Incomplete method implementation.
    // Return the number of rows in the section.
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}*/

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     [detailViewController release];
     */
}

-(void)dealloc {
    // release retains
    [editDeck release];
    
    // call super
    [super dealloc];
}

@end
