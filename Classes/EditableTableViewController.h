//
//  EditableTableViewController.h
//  Mcp
//
//  Created by Ray on 10/21/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EditableTableViewControllerDelegate.h"

@interface EditableTableViewController : UITableViewController {
	id<EditableTableViewControllerDelegate> delegate;

}

@property (nonatomic, assign) id<DataFileDelegate> delegate;

@end
