//
//  CorrectionSelection.m
//  Mcp
//
//  Created by Ray Price on 1/17/13.
//
//

#import "CorrectionSelection.h"

@implementation CorrectionSelection

@synthesize senses;
@synthesize kanji;
@synthesize kana;
@synthesize dictionaryId;

-(id)initWithDictionaryEntry:(JapaneseWord*)inWord {
    self = [super init];
    if (self) {
        // save kanji and kana
        kanji = [inWord.kanji retain];
        kana = [inWord.kana retain];
        dictionaryId = inWord.dictionaryId;
        
        // create array for senses
        senses = [[NSMutableArray alloc] initWithCapacity:2];
        
        // add senses for word
        NSString *currentSenseIndex = @"1";
        NSString *currentSenseString = @"";
        for (int i=0; i < inWord.englishMeanings.count; i++) {
            // find sense index and meaning
            NSString *thisMeaning = [inWord.englishMeanings objectAtIndex:i];
            NSString *thisIndex = [inWord.meaningSense objectAtIndex:i];
            
            // if sense has changed
            if (![thisIndex isEqualToString:currentSenseIndex]) {
                // add sense and reset string
                [senses addObject:currentSenseString];
                currentSenseString = @"";
                currentSenseIndex = thisIndex;
            }

            // add this meaning to the sense string
            if ([currentSenseString isEqualToString:@""])
                currentSenseString = thisMeaning;
            else
                currentSenseString = [NSString stringWithFormat:@"%@; %@", currentSenseString, thisMeaning];
        }
        
        // add final sense
        if (![currentSenseString isEqualToString:@""]) {
            [senses addObject:currentSenseString];
        }
    }
    
    return self;
}

-(void)dealloc {
    // release retains
    [senses release];
    [kanji release];
    [kana release];
    
    // call super
    [super dealloc];
}

@end
