//
//  EditableListDelegate.h
//  Mcp
//
//  Created by Ray on 10/19/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

@protocol EditableListDelegate

-(void)didUpdateObject:(id)updatedObject;
-(void)didAddNewObject:(id)newObject;
//-(void)didDeleteObject:(id)deletedObject;

@end
