//
//  ProCheckerViewController.m
//  Mcp
//
//  Created by Ray on 10/15/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "ProCheckerViewController.h"
#include "vt_jpn_show.h"
#include "vt_jpn_misaki.h"
#import "AudioToolbox/AudioFile.h"
#include "math.h"
#include "limits.h"
#import "VoiceSample.h"
//#import "Accelerate/Accelerate.h"
#import "McpAppDelegate.h"
#import "TrainingData.h"
#import "PhonemeData.h"
#import "PhonemeSample.h"
#import "NeuralNet.h"
#import "AudioFile.h"
#import "Syllable.h"
#import "ConverseViewController.h"

@implementation ProCheckerViewController

@synthesize playButton;
@synthesize sentenceTextView;
@synthesize statusLabel;
@synthesize percentLabel;
@synthesize myRecorder;

/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
        // Custom initialization
    }
    return self;
}
*/

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
}
*/

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	
	// set title
	self.title = @"Pronunciation Checker";
	
	// set up cepstral averages tracking list
    // MOVE THIS INTO USERS PROFILE EVENTUALLY SO IT GETS SAVED!
	userCepstralHistory = [[CepstralHistory alloc] initWithMfccCount:[VoiceSample getMfccCount]];
	cpuCepstralHistory = [[CepstralHistory alloc] initWithMfccCount:[VoiceSample getMfccCount]];
	
    // create audio recorder and player
    myRecorder = [[AudioRecorder alloc] init];
    myPlayer = [[AudioPlayer alloc] init];
        
	// start timer for label update
	labelTimer = [[NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(labelTimerFired:) userInfo:nil repeats:YES] retain];
}


-(IBAction)playButtonClicked:(id)sender {
	// close keyboard
	[sentenceTextView resignFirstResponder];
    /*
     //
     // neural network test harness
     //
     // create the network and test sets
     NeuralNet *myNet = [[NeuralNet alloc] initWithInputCount:2 outputCount:1 hiddenLayerCount:1 neuronsPerHiddenLayer:2 learningRate:0.1f];
     double *inputSet = malloc(sizeof(double)*4*2);
     inputSet[0*2+0] = 1.0f;
     inputSet[0*2+1] = 1.0f;
     inputSet[1*2+0] = 1.0f;
     inputSet[1*2+1] = 0.0f;
     inputSet[2*2+0] = 0.0f;
     inputSet[2*2+1] = 1.0f;
     inputSet[3*2+0] = 0.0f;
     inputSet[3*2+1] = 0.0f;
     double *outputSet = malloc(sizeof(double)*4*1);
     outputSet[0] = 0.0f;
     outputSet[1] = 1.0f;
     outputSet[2] = 1.0f;
     outputSet[3] = 0.0f;
     
     // train the network
     [myNet trainNetworkWithInputSet:inputSet ofSize:4 andOutputSet:outputSet];
     free(inputSet);
     free(outputSet);
     
     // test the network
     double *testInputs = malloc(sizeof(double)*2);
     testInputs[0] = 1.0f;
     testInputs[1] = 0.0f;
     double *outputs = [myNet updateWithInputs:testInputs];
     free(outputs);
     */
	//
	//
	//
	
	// get documents directory
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	
	// create sample audio file
	NSString *testText = sentenceTextView.text;
	char* cc_testText = (char*)[testText cStringUsingEncoding:-2147481087];
    //	const char* cc_testText = [testText cStringUsingEncoding:NSShiftJISStringEncoding];
	NSString *filename = @"/test000.wav";
	NSString *file_path = [documentsDirectory stringByAppendingString:filename];
	char* cc_file_path = (char*)[file_path UTF8String];
	
	// male or female voice?
    McpAppDelegate *appDelegate = (McpAppDelegate*)[[UIApplication sharedApplication] delegate];
	int result;
	if (appDelegate.currentUserProfile.gender == 0)
		result = VT_TextToFile_JPN_Show(VT_FILE_API_FMT_S16PCM_WAVE, cc_testText, cc_file_path, -1, appDelegate.currentUserProfile.voicePitch, 100, 100, 0, -1, -1);
	else
		result = VT_TextToFile_JPN_Misaki(VT_FILE_API_FMT_S16PCM_WAVE, cc_testText, cc_file_path, -1, appDelegate.currentUserProfile.voicePitch, 100, 100, 0, -1, -1);
	if (result != 1) {
		NSException *exception = [NSException exceptionWithName:@"DatabaseError" reason:@"Cannot open speech database" userInfo:nil];
		@throw exception;
	}
	
	// play the file
	NSURL *fileURL = [[NSURL alloc] initFileURLWithPath:file_path];
	AVAudioPlayer *newPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:fileURL error:nil];
	[fileURL release];
	[newPlayer prepareToPlay];
	//	[newPlayer setDelegate:self];
	[newPlayer play];
    [newPlayer autorelease];
}

-(IBAction)checkProButtonClicked:(id)sender {
	// start recording
    [myRecorder startRecording];
}

- (BOOL)textFieldShouldReturn:(UITextField *)theTextField {
	[theTextField resignFirstResponder];
	return YES;
}

-(void)labelTimerFired:(NSTimer*)inTimer {
    // get application delegate
    McpAppDelegate *appDelegate = (McpAppDelegate*)[[UIApplication sharedApplication] delegate];
    
	// update label
	if (myRecorder.recordState.state == AUDIORECORDER_STATE_ANALYZING) {
		// no longer listening
//		isListening = NO;
		
		// check pronunciation here
		statusLabel.text = @"Analyzing...";
		
		// check sample against record buffer
		
		// get documents directory
		NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
		NSString *documentsDirectory = [paths objectAtIndex:0];
//		NSString *bundlePath = [[NSBundle mainBundle] resourcePath];
		
		// open wave file
#define _RECORDON
#ifdef _RECORDON
		NSString *file1Path = [documentsDirectory stringByAppendingString:@"/test000.wav"];
#else
        myCounter++;
        if (myCounter > 4)
            myCounter = 1;
//		NSString *file1Path = [documentsDirectory stringByAppendingString:@"/compare1_nocms_raw copy 29.wav"];
		NSString *file1Path = [documentsDirectory stringByAppendingFormat:@"/test000.wav", myCounter];
#endif
//		NSString *file2Path = [documentsDirectory stringByAppendingString:@"/compare2_nocms_raw copy 29.wav"];
		NSString *file2Path = [documentsDirectory stringByAppendingFormat:@"/compare2_nocms_raw_arigatou%d.wav", myCounter];
		
		// get data for first file
		int file1SampleCount = [AudioFile getSampleCountForFile:file1Path];
		float *file1Data = malloc(sizeof(float)*file1SampleCount);
		[AudioFile getDataIntoFloatBuffer:file1Data ofLength:file1SampleCount fromFile:file1Path];
		
		
		//
		// here's a test... build users speech from samples
		//
/*		
		// get documents and profile directory
		McpAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
		NSString *profileDirectory = [documentsDirectory stringByAppendingPathComponent:appDelegate.currentUserProfile.name];
		
		// find size of concatenated syllables
		// convert text to kana
		NSString *kanaText = sentenceTextView.text;
		free(file1Data);
		file1SampleCount = 0;
		for (int i=0; i < kanaText.length; i++) {
			// find filename from syllables and add size to buffer size
			Syllable *thisSyllable = [appDelegate.trainingData findSyllableFromKana:[kanaText substringWithRange:NSMakeRange(i, 1)]];
			if (thisSyllable != nil) {
				file1Path = [NSString stringWithFormat:@"%@/%@.wav", profileDirectory, thisSyllable.syllableName];
				file1SampleCount += [AudioFile getSampleCountForFile:file1Path]*0.5f;
			}
		}
		
		// concatenate together into file1data
		file1Data = malloc(sizeof(float)*file1SampleCount);
		int thisSampleCount = 0;
		float *currBufferPos = file1Data;
		for (int i=0; i < kanaText.length; i++) {
			// find filename from syllables and add to buffer
			Syllable *thisSyllable = [appDelegate.trainingData findSyllableFromKana:[kanaText substringWithRange:NSMakeRange(i, 1)]];
			if (thisSyllable != nil) {
				file1Path = [NSString stringWithFormat:@"%@/%@.wav", profileDirectory, thisSyllable.syllableName];
				thisSampleCount = [AudioFile getSampleCountForFile:file1Path]*0.5f;
				[AudioFile getDataIntoFloatBuffer:currBufferPos ofLength:thisSampleCount fromFile:file1Path];
				currBufferPos += thisSampleCount;
			}
		}			
		
		*/
		//
		//
		//
		
#ifdef _RECORDON
		// change from 16000 to 8000
//		int newPos = 0;
//		for (int i=0; i < file1SampleCount; i+=2)
//			file1Data[newPos++] = file1Data[i];
//		file1SampleCount = newPos;
#endif
		
		// get data for second file
		int file2SampleCount = [AudioFile getSampleCountForFile:file2Path];
		float *file2Data = malloc(sizeof(float)*file2SampleCount);
		[AudioFile getDataIntoFloatBuffer:file2Data ofLength:file2SampleCount fromFile:file2Path];
		
		// normalize file buffer
		[AudioFile normalizeFloatArray:file1Data ofSize:file1SampleCount];
		[AudioFile normalizeFloatArray:file2Data ofSize:file2SampleCount];
		
		// normalize record buffer
		[AudioFile normalizeFloatArray:myRecorder.recordState.recordBuffer ofSize:myRecorder.recordState.recordBufferPos/sizeof(float)];

/*		
		// ok, here's the test... build the sound 'iie' using the training data
		McpAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
		TrainingData *trainingData = appDelegate.trainingData;
		
		// first find total size for buffer
//		NSString *practiceWord = @"iie";
		PhonemeData *iData = [trainingData.phonemeData objectForKey:@"i"];
		PhonemeData *eData = [trainingData.phonemeData objectForKey:@"e"];
		PhonemeSample *iSample = [iData.phonemeSamples objectAtIndex:0];
		PhonemeSample *eSample = [eData.phonemeSamples objectAtIndex:0];
		int iieSize = (iSample.sampleSize*2)+eSample.sampleSize;
		float *catBuffer = malloc(iieSize*sizeof(float));
		
		for (int i=0; i < iieSize; i++)
			catBuffer[i] = 0.0f;
		
		// now copy in data to buffer
		float TWOPI = 2.0f * M_PI;
		int bufferPos = 0;
		
		// copy to buffer and apply hamming window at the end
		for (int i=0; i < iSample.sampleSize; i++)
			catBuffer[bufferPos+i] += iSample.sampleBuffer[i];
//		catBuffer[bufferPos+i] += iSample.sampleBuffer[i] * ((1.0f-0.46f) - 0.46f*cos(TWOPI*i/((float)iSample.sampleSize-1.0f)));
		bufferPos += (float)iSample.sampleSize*0.9f;
		
		// copy to buffer and apply hamming window at the end
		for (int i=0; i < iSample.sampleSize; i++)
			catBuffer[bufferPos+i] += iSample.sampleBuffer[i];
//			catBuffer[bufferPos+i] += iSample.sampleBuffer[i] * ((1.0f-0.46f) - 0.46f*cos(TWOPI*i/((float)iSample.sampleSize-1.0f)));
		bufferPos += (float)iSample.sampleSize*0.9f;
		
		// copy to buffer and apply hamming window at the end
		for (int i=0; i < eSample.sampleSize; i++)
			catBuffer[bufferPos+i] += eSample.sampleBuffer[i];
//			catBuffer[bufferPos+i] += eSample.sampleBuffer[i] * ((1.0f-0.46f) - 0.46f*cos(TWOPI*i/((float)eSample.sampleSize-1.0f)));
		bufferPos += (float)eSample.sampleSize*0.9f;
		
		// for now, just play and exit
*/
		
		// calculate quantize size using fft window size with a little overlap
		// then round up to make divisible my compare segment size
		int fftWindowSize = [VoiceSample getN];
		fftWindowSize = fftWindowSize/3;
		int quantizeCount = round((float)file1SampleCount/(float)fftWindowSize);
//		quantizeCount = quantizeCount - (quantizeCount % [VoiceSample getCompareSegmentSize]);
//		quantizeCount += [VoiceSample getCompareSegmentSize];
//		// must maintain minimum of 6 segments
//		if (quantizeCount < [VoiceSample getCompareSegmentSize])
//			quantizeCount = [VoiceSample getCompareSegmentSize];
		
		// create voice sample data structure and compare
//		VoiceSample *mySample = [[VoiceSample alloc] initWithBuffer:catBuffer ofLength:iieSize quantizeTo:quantizeCount];
        
#ifdef _RECORDON
        // load user sample first so we can match spectrum band power
		VoiceSample *noCmsSample2 = [[VoiceSample alloc] initWithBuffer:myRecorder.recordState.recordBuffer ofLength:myRecorder.recordState.recordBufferPos/sizeof(float) quantizeTo:quantizeCount fromUser:appDelegate.currentUserProfile noiseLevel:0.1f cepstralHistory:userCepstralHistory phonemeLengthArray:nil sampleRate:8000];
        
		VoiceSample *noCmsSample = [[VoiceSample alloc] initWithBuffer:file1Data ofLength:file1SampleCount quantizeTo:quantizeCount fromUser:appDelegate.currentCpuProfile noiseLevel:0.0f cepstralHistory:cpuCepstralHistory phonemeLengthArray:nil sampleRate:8000];
#else
        // load user sample first so we can match spectrum band power
		VoiceSample *noCmsSample2 = [[VoiceSample alloc] initWithBuffer:file2Data ofLength:file2SampleCount quantizeTo:quantizeCount fromUser:appDelegate.currentUserProfile noiseLevel:0.1f cepstralHistory:userCepstralHistory matchSpectrum:nil];
//		VoiceSample *noCmsSample2 = [[VoiceSample alloc] initWithBuffer:file2Data ofLength:file2SampleCount quantizeTo:quantizeCount noiseLevel:myRecorder.recordState.avgBackgroundNoise cepstralHistory:userCepstralHistory matchSpectrum:nil];
        
		VoiceSample *noCmsSample = [[VoiceSample alloc] initWithBuffer:file1Data ofLength:file1SampleCount quantizeTo:quantizeCount fromUser:appDelegate.currentCpuProfile noiseLevel:0.0f cepstralHistory:cpuCepstralHistory matchSpectrum:[noCmsSample2 getSpectrumBuffer]];
        
		
#endif
		// try with and without cms incase it's a short vowel utterance
//		float result = [mySample compareWithVoiceSample:mySample2 filenameSuffix:@"_cms"];
		float result = 0.0f;
		float noCmsResult = [noCmsSample compareWithVoiceSample:noCmsSample2 filenameSuffix:@"_nocms" forText:sentenceTextView.text calibrationMode:NO writeDebug:YES];
		printf("score=%f, no cms score=%f\r\n",result,noCmsResult);
		if (noCmsResult > result)
			result = noCmsResult;
		percentLabel.text = [NSString stringWithFormat:@"%1.0f%%", result*100];
//		[mySample release];
//		[mySample2 release];
		[noCmsSample release];
		[noCmsSample2 release];
		
		// free file buffers
		free(file1Data);
		free(file2Data);
		
		// now play back audio
		// do we have data?  if so, play back audio
		if (myRecorder.recordState.recordBufferPos > 0)
		{
            NSLog(@"Playing back buffer of size %d", myRecorder.recordState.recordBufferPos);
            
            // start playing record buffer
            [myPlayer startPlayingBuffer:myRecorder.recordState.recordBuffer ofSize:myRecorder.recordState.recordBufferPos];
		}
		
		// no longer analyzing
        [myRecorder pauseRecording];
//		isAnalyzing = NO;
	}
	else {
        switch (myRecorder.recordState.state) {
            case AUDIORECORDER_STATE_RECORDING:
                statusLabel.text = @"Status: Recording...";
                break;
            case AUDIORECORDER_STATE_LISTENING:
				statusLabel.text = @"Listening...";
                break;
            case AUDIORECORDER_STATE_PAUSED:
				statusLabel.text = @"Waiting...";
                break;
            default:
                break;
        }
	}

	// refresh display
//	[self setNeedsDisplay];
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillDisappear:(BOOL)animated {
	// invalidate timer here, because it contains a reference to us!
	[labelTimer invalidate];
}

- (void)dealloc {
    // release audio
    [myRecorder release];
    [myPlayer release];

	// invalidate timer
	[labelTimer invalidate];

	// free outlets
	[labelTimer release];
	[playButton release];
	[sentenceTextView release];
	[statusLabel release];
	[percentLabel release];
	[userCepstralHistory release];
	[cpuCepstralHistory release];
    [super dealloc];
}


@end
