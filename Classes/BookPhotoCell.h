//
//  BookPhotoCell.h
//  Mcp
//
//  Created by Ray on 10/25/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface BookPhotoCell : UITableViewCell {
	IBOutlet UIImageView *bookImageView;
}

@property (nonatomic, retain) UIImageView *bookImageView;

@end
