//
//  FloatIndex.m
//  Mcp
//
//  Created by Ray on 3/27/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "FloatIndex.h"

@implementation FloatIndex

@synthesize floatValue;
@synthesize indexValue;

-(id)initWithFloatValue:(float)inFloatValue indexValue:(int)inIndexValue {
	if (self = [super init]) {
		floatValue = inFloatValue;
		indexValue = inIndexValue;
	}
	return self;
}

-(NSComparisonResult)compareFloat:(FloatIndex*)inFloatIndex {
	if (floatValue == [inFloatIndex floatValue])
		return NSOrderedSame;
	else {
		if (floatValue > [inFloatIndex floatValue])
			return NSOrderedAscending;
		else
			return NSOrderedDescending;
	}
}

-(NSComparisonResult)compareIndex:(FloatIndex*)inFloatIndex {
	if (indexValue == [inFloatIndex indexValue])
		return NSOrderedSame;
	else {
		if (indexValue > [inFloatIndex indexValue])
			return NSOrderedDescending;
		else
			return NSOrderedAscending;
	}
}

-(void)dealloc {
	[super dealloc];
}

@end
