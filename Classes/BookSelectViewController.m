//
//  BookSelectViewController.m
//  Mcp
//
//  Created by Ray Price on 8/31/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//


#import "BookSelectViewController.h"
#import "Book.h"
#import "McpAppDelegate.h"
#import "BookListCell.h"

@implementation BookSelectViewController

@synthesize bookList;
@synthesize selectDelegate;

#pragma mark -
#pragma mark View lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
	
	// show navigation bar again
	[self.navigationController setNavigationBarHidden:NO animated:YES];
    
	// set title
	self.title = @"Books";
	
	// create cancel button
	UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(cancelButtonClick:)];
	self.navigationItem.leftBarButtonItem = cancelButton;
	[cancelButton release];
	
	// create select button
	UIBarButtonItem *selectButton = [[UIBarButtonItem alloc] initWithTitle:@"Select" style:UIBarButtonItemStyleBordered target:self action:@selector(selectButtonClick:)];
	self.navigationItem.rightBarButtonItem = selectButton;
	[selectButton release];
	
	// search for deck archive files
	[self populateBookList];
}

-(void)populateBookList {
	// create new list of backgrounds
	self.bookList = [Book getListOfFilesWithExtension:@"book"];
}

#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return bookList.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	return 100;
}

// Customize the appearance of table view cells.
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	// get regular table cell
    static NSString *CellIdentifier = @"BookListCell";
    BookListCell *cell = (BookListCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
		// load cell layout from nib
		NSArray *nib = [[NSBundle mainBundle] loadNibNamed:CellIdentifier owner:self options:nil];
		cell = [nib objectAtIndex:0];
    }
	
	// Set up the cell...
    NSUInteger row = [indexPath row];
    cell.nameLabel.text = [bookList objectAtIndex:row];
    Book *book = [[Book alloc] initFromArchive:[bookList objectAtIndex:row]];
    cell.bookImageView.image = book.image;
    cell.editingAccessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.selectionStyle = UITableViewCellSelectionStyleBlue;
    [book release];
    
    return cell;
}

-(IBAction)selectButtonClick:(id)sender {
	// call delegate to notify about selection
	[selectDelegate didMakeBrowserSelection:[bookList objectAtIndex:[lastIndexPath row]] fromController:self];
    
	// hide navigation bar again
	[self.navigationController setNavigationBarHidden:YES animated:YES];
    
	// hide keyboard and pop controller
	[self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)cancelButtonClick:(id)sender {	
    // call delegate to notify about selection
	[selectDelegate didCancelBrowserSelection];
    
	// hide navigation bar again
	[self.navigationController setNavigationBarHidden:YES animated:YES];
    
	// hide keyboard and pop controler
	[self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -
#pragma mark Table view delegate

-(void)tableView:(UITableView*)tableView didSelectRowAtIndexPath:(NSIndexPath*) indexPath {
	// save last index
    [lastIndexPath release];
	lastIndexPath = [indexPath retain];
}

#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    // Relinquish ownership of anything that can be recreated in viewDidLoad or on demand.
    // For example: self.myOutlet = nil;
}


- (void)dealloc {
	[bookList release];
	[lastIndexPath release];
    [super dealloc];
}

@end


