//
//  CepstralHistory.h
//  Mcp
//
//  Created by Ray on 3/30/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface CepstralHistory : NSObject {
	int mfccCount;
	int historyLength;
	int totalLength;
	float *historyData;
}

-(id)initWithMfccCount:(int)inMfccCount;
-(void)dealloc;
-(void)addNewMfccData:(float*)inData ofLength:(int)inLength;
-(float*)getCepstralMeanArray;

@end
