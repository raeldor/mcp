//
//  EditSectionViewController.h
//  HF Sensei
//
//  Created by Ray Price on 2/23/13.
//
//

#import <UIKit/UIKit.h>
#import "DeckSection.h"
#import "EditSectionViewControllerDelegate.h"
#import "BrowserSelectDelegate.h"

@interface EditSectionViewController : UITableViewController <BrowserSelectDelegate> {
    DeckSection *originalSection;
    DeckSection *editSection;
    BOOL isNewSection;
}

@property (nonatomic, assign) id<EditSectionViewControllerDelegate> delegate;

-(void)setSectionTo:(DeckSection*)inSection;
-(IBAction)cancelPushed:(id)sender;
-(IBAction)savePushed:(id)sender;
-(IBAction)sectionNameChanged:(UITextField*)sender;
-(IBAction)sectionTypeChanged:(UISegmentedControl*)sender;

@end
