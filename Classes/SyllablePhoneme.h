//
//  SyllablePhoneme.h
//  Mcp
//
//  Created by Ray on 3/3/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface SyllablePhoneme : NSObject {
	NSString *phonemeName;
	bool alreadyTrained;
	float startPercent;
	float endPercent;
}

-(id)initWithDictionary:(NSDictionary*)inDictionary;
-(void)dealloc;

@property (nonatomic, retain) NSString *phonemeName;
@property (nonatomic, assign) bool alreadyTrained;
@property (nonatomic, assign) float startPercent;
@property (nonatomic, assign) float endPercent;

@end
