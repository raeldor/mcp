//
//  TrainingWord.m
//  Mcp
//
//  Created by Ray Price on 11/20/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "TrainingWord.h"
#import "TrainingWordPhoneme.h"
#import "McpAppDelegate.h"
#import "Phoneme.h"

@implementation TrainingWord

@synthesize wordName;
@synthesize wordKana;
@synthesize phonemes;

-(id)initWithName:(NSString*)inName andKana:(NSString*)inKana {
	if (self = [super init]) {
		self.wordName = inName;
		self.wordKana = inKana;
		phonemes = [[NSMutableArray alloc] init];
	}
	return self;
}

-(id)initWithDictionaryEntry:(NSDictionary*)inDict {
	if (self = [super init]) {
        self.wordName = [inDict objectForKey:@"WordName"];
        self.wordKana = [inDict objectForKey:@"WordKana"];
        
        // load phonemes
		phonemes = [[NSMutableArray alloc] init];
        NSArray *phoneArray = [inDict objectForKey:@"Phonemes"];
        for (int i=0; i < phoneArray.count; i++) {
            TrainingWordPhoneme *newTwp = [[TrainingWordPhoneme alloc] initWithDictionaryEntry:[phoneArray objectAtIndex:i]];
            [phonemes addObject:newTwp];
            [newTwp release];
        }
	}
	return self;
}

-(int)getFeatureIndexAtPercentPos:(float)inPercentPos {
    // get application delegate
    McpAppDelegate *appDelegate = (McpAppDelegate*)[[UIApplication sharedApplication] delegate];
    
    // find phoneme at position
    float movingStartPercent = 0.0f;
    int featureIndex = -1;
    for (int p=0; p < self.phonemes.count; p++) {
        // are we inside here?
        // later we can enhance to give % chance of being in either phoneme
        TrainingWordPhoneme *thisWordPhoneme = [self.phonemes objectAtIndex:p];
        if (inPercentPos >= movingStartPercent && inPercentPos <= movingStartPercent+thisWordPhoneme.lengthPercent) {
            Phoneme *thisPhoneme = [appDelegate.phonemes objectForKey:thisWordPhoneme.phonemeName];
            featureIndex = thisPhoneme.featureIndex;
            break;
        }
        else
            movingStartPercent += thisWordPhoneme.lengthPercent;
    }
    return featureIndex;
}

- (void)dealloc {
	[wordName release];
	[wordKana release];
	[phonemes release];
	[super dealloc];
}

@end
