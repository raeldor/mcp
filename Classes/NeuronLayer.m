//
//  NeuronLayer.m
//  Mcp
//
//  Created by Ray on 2/6/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "NeuronLayer.h"

@implementation NeuronLayer

@synthesize neurons;
@synthesize neuronCount;
@synthesize inputsPerNeuron;
@synthesize inputs;

-(id)initWithNeuronCount:(int)inNeuronCount inputsPerNeuron:(int)inInputsPerNeuron {
	if (self = [super init]) {
		// save parameters
		neuronCount = inNeuronCount;
		inputsPerNeuron = inInputsPerNeuron;
		
        // allocate space to save inputs for tdnn
        inputs = malloc(sizeof(double)*inInputsPerNeuron);
        for (int i=0; i < inInputsPerNeuron; i++)
            inputs[i] = 0.5f;
        
		// create array
		neurons = [[NSMutableArray alloc] init];
		
		// add neurons to the layer
		for (int i=0; i < neuronCount; i++)
			[neurons addObject:[[[Neuron alloc] initWithInputCount:inputsPerNeuron] autorelease]];
	}
	return self;
}

-(id)initWithCoder:(NSCoder*)coder {
    self = [super init];
	if (self) {
        // get parameters
        neuronCount = [coder decodeInt32ForKey:@"neuronCount"];
        inputsPerNeuron = [coder decodeInt32ForKey:@"inputsPerNeuron"];
        
        // get neurons for this layer
        self.neurons = [coder decodeObjectForKey:@"neurons"];
	}
	return self;
}

-(void)encodeWithCoder:(NSCoder*)coder {
    // encode parameters
    [coder encodeInt32:neuronCount forKey:@"neuronCount"];
    [coder encodeInt32:inputsPerNeuron forKey:@"inputsPerNeuron"];
    
    // encode neurons
    [coder encodeObject:neurons forKey:@"neurons"];
}

-(void)dealloc {
    free(inputs);
	[neurons release];
	[super dealloc];
}

@end
