//
//  CalibratePopupController.m
//  Mcp
//
//  Created by Ray Price on 10/24/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "CalibratePopupController.h"
#import "McpAppDelegate.h"
#import "NewTrainingData.h"
#import "NewTrainingSample.h"
#import "VoiceSample.h"
#import "Phoneme.h"
#import "TrainingWord.h"
#import "TrainingWordPhoneme.h"

@implementation CalibratePopupController

@synthesize rateLabel;
@synthesize progressView;
@synthesize popupDelegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)startCalibration {
    // spin off calibrate to seperate thread
    [NSThread detachNewThreadSelector:@selector(doCalibrate) toTarget:self withObject:nil];
    
    // we are now calibrating
    isCalibrating = YES;
    stopCalibrating = NO;
    
    // start a timer to update the UI
    updateTimer = [[NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateTimerFired:) userInfo:nil repeats:YES] retain];
}

-(IBAction)cancelPushed:(id)sender {
    // user cancelled
    stopCalibrating = YES;
}


-(void)updateTimerFired:(NSTimer*)inTimer {
    // update labels
    rateLabel.text = [NSString stringWithFormat:@"Recognition Rate: %3.0f%%", currentLowestScore*100.0f];
    progressView.progress = currentLowestScore;
    
    // if we're not calibrating anymore, stop the timer
    if (!isCalibrating) {
        // stop timer
        [updateTimer invalidate];
        [updateTimer release];
        
        // call end delegate on timer so UI has chance to refresh
        finishTimer = [[NSTimer scheduledTimerWithTimeInterval:0.1f target:self selector:@selector(finishTimerFired:) userInfo:nil repeats:NO] retain];
    }
}

-(void)finishTimerFired:(NSTimer*)inTimer {
    // call delegate to finish (and close this dialog)
    [popupDelegate calibrationFinished:stopCalibrating];
}

-(void)doCalibrate {
    // create auto release pool because we're going to spin this off to a seperate thread
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    
    // get application delegate
    McpAppDelegate *appDelegate = (McpAppDelegate*)[[UIApplication sharedApplication] delegate];
    
    // get training data
    NewTrainingData *myTrainingData = [appDelegate.currentUserProfile getTrainingDataForAudioDevice];
    
    // get total sample count
    int totalSampleCount = [self getTotalSampleCount];
    
    // feature count should be same as network input count
    NeuralNet *myNet = [appDelegate.currentUserProfile getNetForAudioDevice];
    NeuralNet *cpuNet = [appDelegate.currentCpuProfile getNetForAudioDevice];
    int compareFeatureCount = myNet.inputCount;
    int outputFeatureCount = myNet.outputCount;
    
    // do a test... train neural network with this time-matched data
    // input is both images, output is difference of first half
    int inPos = 0;
    int outPos = 0;
    int inPos1 = 0;
    int outPos1 = 0;
    int cpuInPos = 0;
    int cpuOutPos = 0;
    int cpuInPos1 = 0;
    int cpuOutPos1 = 0;
    NSString *nextKey;
    double *inputData = malloc(sizeof(double)*totalSampleCount*myNet.inputCount);
    double *outputData = malloc(sizeof(double)*totalSampleCount*myNet.outputCount);
    double *cpuInputData = malloc(sizeof(double)*totalSampleCount*myNet.inputCount);
    double *cpuOutputData = malloc(sizeof(double)*totalSampleCount*myNet.outputCount);
    float *inputData1 = malloc(sizeof(float)*totalSampleCount*myNet.inputCount);
    float *outputData1 = malloc(sizeof(float)*totalSampleCount*myNet.outputCount);
    float *cpuInputData1 = malloc(sizeof(float)*totalSampleCount*myNet.inputCount);
    float *cpuOutputData1 = malloc(sizeof(float)*totalSampleCount*myNet.outputCount);
    NSEnumerator *enumerator = [myTrainingData.trainingSamples keyEnumerator];
    int newTotalSampleCount = 0;
    while ((nextKey = [enumerator nextObject])) {
        NewTrainingSample *thisSample = [myTrainingData.trainingSamples objectForKey:nextKey];
        
        // don't use for simple image translation
        
        // find text of word and remove punctuation for processing
        NSString *wordName = [thisSample.text stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@".,!?？！。、"]];
        
        /*
        // find our training word
        TrainingWord *word = [appDelegate.trainingWords objectForKey:wordName];
        
        // assert if word not found
        if (word == nil)
            NSAssert(0, @"Training word not found");
        */
        
        // write out training data
        for (int s=0; s < thisSample.size; s++) {
            // before we were taking output from features
            for (int i=0; i < myNet.outputCount; i++) {
                inputData[inPos++] = thisSample.userFeatures[s*thisSample.featureCount+i];
                inputData1[inPos1++] = thisSample.userFeatures[s*thisSample.featureCount+i];
                outputData[outPos++] = thisSample.cpuFeatures[s*thisSample.featureCount+i];
                outputData1[outPos1++] = thisSample.cpuFeatures[s*thisSample.featureCount+i];
//                outputData[outPos++] = (thisSample.cpuFeatures[s*thisSample.featureCount+i] - thisSample.userFeatures[s*thisSample.featureCount+i]+1.0f)/2.0f;
//                outputData1[outPos1++] = (thisSample.cpuFeatures[s*thisSample.featureCount+i] - thisSample.userFeatures[s*thisSample.featureCount+i]+1.0f)/2.0f;
            }
            
            newTotalSampleCount++;
            
            /*
            // now we need to figure out from the phonemes and the training words, what
            // the percentage chance of the piece we're looking at being a certain
            // phoneme feature is
            
            // for this position find out which phoneme we are in
            int featureIndex = [word getFeatureIndexAtPercentPos:(float)s/(float)thisSample.size];
            
            // assert if we didn't find feature index
// use this when we should ignore for training
//            if (featureIndex == -1)
//                NSAssert(0, @"Feature index not found");
            if (featureIndex != -1)
            {
                // input data
                for (int i=0; i < thisSample.featureCount; i++) {
                    inputData[inPos++] = thisSample.userFeatures[s*thisSample.featureCount+i];
                    inputData1[inPos1++] = thisSample.userFeatures[s*thisSample.featureCount+i];
                    cpuInputData[cpuInPos++] = thisSample.cpuFeatures[s*thisSample.featureCount+i];
                    cpuInputData1[cpuInPos1++] = thisSample.cpuFeatures[s*thisSample.featureCount+i];
                }
                
                // set output data to 1.0f for found feature
                for (int i=0; i < outputFeatureCount; i++) {
                    if (i == featureIndex) {
                        outputData[outPos++] = 1.0f;
                        outputData1[outPos1++] = 1.0f;
                        cpuOutputData[cpuOutPos++] = 1.0f;
                        cpuOutputData1[cpuOutPos1++] = 1.0f;
                    }
                    else {
                        outputData[outPos++] = 0.0f;
                        outputData1[outPos1++] = 0.0f;
                        cpuOutputData[cpuOutPos++] = 0.0f;
                        cpuOutputData1[cpuOutPos1++] = 0.0f;
                    }
                }
                
                // only if we're using feature increment count
                newTotalSampleCount++;
            }
             */
        }
        
        /*
        // write out image data
        for (int s=0; s < thisSample.size; s++) {
            for (int i=0; i < thisSample.featureCount; i++)
                inputData1[inPos1++] = thisSample.userFeatures[s*thisSample.featureCount+i];
            for (int i=0; i < compareFeatureCount; i++)
                outputData1[outPos1++] = (thisSample.cpuFeatures[s*thisSample.featureCount+i] - thisSample.userFeatures[s*thisSample.featureCount+i]+1.0f)/2.0f;
        }
         */
    }
    
    // write input and output to debug image
    [VoiceSample writeToImageWithFilename:@"compare2_nocms_netinput.png" alternateBuffer:inputData1 alternateFeatureCount:myNet.inputCount alternateQuantizeCount:newTotalSampleCount];
    [VoiceSample writeToImageWithFilename:@"compare2_nocms_netoutput.png" alternateBuffer:outputData1 alternateFeatureCount:myNet.outputCount alternateQuantizeCount:newTotalSampleCount];
    [VoiceSample writeToImageWithFilename:@"compare1_nocms_netinput.png" alternateBuffer:cpuInputData1 alternateFeatureCount:myNet.inputCount alternateQuantizeCount:newTotalSampleCount];
    [VoiceSample writeToImageWithFilename:@"compare1_nocms_netoutput.png" alternateBuffer:cpuOutputData1 alternateFeatureCount:myNet.outputCount alternateQuantizeCount:newTotalSampleCount];
    
    // reset weights
//    [myNet resetWeights];
    
    // do simple train here
    [myNet trainNetworkWithInputSet:inputData ofSize:newTotalSampleCount andOutputSet:outputData];
    
    /*
    
    // train neural network using these samples
    currentLowestScore = 0.0;
	int passCount = 0;
    do {
		// run network training epoch
        [myNet trainNetworkOnceWithInputSet:inputData ofSize:newTotalSampleCount andOutputSet:outputData];
        [cpuNet trainNetworkOnceWithInputSet:cpuInputData ofSize:newTotalSampleCount andOutputSet:cpuOutputData];
        
        // increment counter
		passCount++;
        
        // update stats
        
        // update stats every 10 epochs
		if ((passCount % 10) == 0.0f) {
            // set simple stuff
            float score = [self getTrainingSamplesLowestScore];
            score = [VoiceSample convertScoreToPercent:score];
            currentLowestScore = score;
            
            // update UI
            //            calibrateController.rateLabel = [NSString stringWithFormat:@"Recognition Rate:%3.0f%%", currentLowestScore];
            //            calibrateController.progressView.progress = currentLowestScore;
        }
    } while (currentLowestScore < 0.80f && passCount < 10000 && !stopCalibrating);
    */
    
    // debug... how many iterations did it take?
    // put up message
    /*
    UIAlertView *myAlert = [[UIAlertView alloc] initWithTitle:@"Calibration" message:[NSString stringWithFormat:@"It took %d iterations", passCount] delegate:self cancelButtonTitle:@"Okay" otherButtonTitles:nil];
    [myAlert show];
    [myAlert release];
    */
    
    // signal delegate that we are finished
    
    // allow phone to turn off again
    [[UIApplication sharedApplication] setIdleTimerDisabled:NO  ];
    
    // free data
    free(inputData);
    free(outputData);
    free(inputData1);
    free(outputData1);
    free(cpuInputData);
    free(cpuOutputData);
    free(cpuInputData1);
    free(cpuOutputData1);
    
    // no longer calibrating
    isCalibrating = NO;
    
    // release pool
    [pool release];
}

-(int)getTotalSampleCount {
    // get application delegate
    McpAppDelegate *appDelegate = (McpAppDelegate*)[[UIApplication sharedApplication] delegate];
    
    // get training data
    NewTrainingData *myTrainingData = [appDelegate.currentUserProfile getTrainingDataForAudioDevice];
    
    // find total number of samples
    NSEnumerator *enumerator = [myTrainingData.trainingSamples keyEnumerator];
    NSString *nextKey;
    int totalSampleCount = 0;
    while ((nextKey = [enumerator nextObject])) {
        NewTrainingSample *thisSample = [myTrainingData.trainingSamples objectForKey:nextKey];
        totalSampleCount += thisSample.size;
    }
    
    return totalSampleCount;
}

// lowest of correct scores
-(float)getTrainingSamplesLowestScore {
    // get application delegate
    McpAppDelegate *appDelegate = (McpAppDelegate*)[[UIApplication sharedApplication] delegate];
    
    // put here for now as test
    NewTrainingData *myTrainingData = [appDelegate.currentUserProfile getTrainingDataForAudioDevice];
    
    // turn off calibration while doing this or it'll affect the array we're using
//    BOOL oldCalibrationMode = appDelegate.currentUserProfile.gameOptions.calibrationMode;
//    appDelegate.currentUserProfile.gameOptions.calibrationMode = NO;
    
    // retest samples using current network and get lowest score
    NSString *nextKey;
    float lowestScore = 0.0f;
    int wrongCount = 0;
    int rightCount = 0;
    NSEnumerator *enumerator = [myTrainingData.trainingSamples keyEnumerator];
    while ((nextKey = [enumerator nextObject])) {
        NewTrainingSample *thisSample = [myTrainingData.trainingSamples objectForKey:nextKey];
        
        // do comparison - use same features for dtw because it's already been
        // stretched
        /*
        float score = [VoiceSample compareImage3:thisSample.cpuFeatures ofWidth:thisSample.size andHeight:thisSample.featureCount withImage:thisSample.userFeatures forText:thisSample.text dtwImage:thisSample.userFeatures withDtwImage:thisSample.userFeatures withHeight:thisSample.featureCount calibrationMode:NO writeDebug:NO compareWithCheckImage:YES];
         */
        
        // this was for speech recog version
//        float score = [VoiceSample compareImage3:thisSample.userFeatures ofWidth:thisSample.size andHeight:thisSample.featureCount withImage:thisSample.userFeatures forText:thisSample.text dtwImage:thisSample.userFeatures withDtwImage:thisSample.userFeatures withHeight:thisSample.featureCount calibrationMode:NO writeDebug:NO compareWithCheckImage:YES];
        
        // TRY CPU FEATURES!
        float score = [VoiceSample compareImage3:thisSample.cpuFeatures ofWidth:thisSample.size andHeight:thisSample.featureCount withImage:thisSample.cpuFeatures forText:thisSample.text dtwImage:thisSample.cpuFeatures withDtwImage:thisSample.cpuFeatures withHeight:thisSample.featureCount calibrationMode:NO writeDebug:NO compareWithCheckImage:YES phonemeLengthArray:nil];
        
        // TRY USER FEATURES!
        float score2 = [VoiceSample compareImage3:thisSample.cpuFeatures ofWidth:thisSample.size andHeight:thisSample.featureCount withImage:thisSample.userFeatures forText:thisSample.text dtwImage:thisSample.userFeatures withDtwImage:thisSample.userFeatures withHeight:thisSample.featureCount calibrationMode:NO writeDebug:NO compareWithCheckImage:YES phonemeLengthArray:nil];
        
        // get score
        if (score > lowestScore)
            lowestScore = score;
        if (score2 > lowestScore)
            lowestScore = score2;
        
        // convert to percent for checking
        float percentScore = [VoiceSample convertScoreToPercent:score2>score?score2:score];
//        float percentScore = [VoiceSample convertScoreToPercent:score];
        
        if (percentScore < 0.8f) {
            printf("%s is less that 80%%\r\n", [thisSample.text UTF8String]);
            wrongCount++;
        }
        else
            rightCount++;
    }
    
    // reset calibration mode
//    appDelegate.currentUserProfile.gameOptions.calibrationMode = oldCalibrationMode;
    
    return lowestScore;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

-(void)dealloc {
    self.popupDelegate = nil;
    [finishTimer invalidate];
    [finishTimer release];
    [rateLabel release];
    [progressView release];
    [super dealloc];
}

@end
