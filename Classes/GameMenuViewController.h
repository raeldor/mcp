//
//  GameMenuViewController.h
//  Mcp
//
//  Created by Ray on 12/22/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface GameMenuViewController : UIViewController {
	IBOutlet UIButton *selectConversationsButton;
	IBOutlet UIButton *optionsButton;
	IBOutlet UIButton *changeBookButton;
	IBOutlet UIButton *resumeButton;
	IBOutlet UIButton *quitButton;
	IBOutlet UIButton *calibrateButton;
}

@property (nonatomic, retain) UIButton *selectConversationsButton;
@property (nonatomic, retain) UIButton *optionsButton;
@property (nonatomic, retain) UIButton *changeBookButton;
@property (nonatomic, retain) UIButton *resumeButton;
@property (nonatomic, retain) UIButton *quitButton;
@property (nonatomic, retain) UIButton *calibrateButton;

@end
