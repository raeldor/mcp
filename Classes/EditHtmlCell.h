//
//  EditHtmlCell.h
//  Mcp
//
//  Created by Ray on 1/7/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface EditHtmlCell : UITableViewCell {
	IBOutlet UISegmentedControl *editPreviewControl;
	IBOutlet UITextView *editTextView;
	IBOutlet UIWebView *previewWebView;	
}

@property (nonatomic, retain) UISegmentedControl *editPreviewControl;
@property (nonatomic, retain) UITextView *editTextView;
@property (nonatomic, retain) UIWebView *previewWebView;

-(void)editPreviewChanged:(id)sender;

@end
