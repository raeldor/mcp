//
//  CalibrateViewController.m
//  Mcp
//
//  Created by Ray Price on 9/17/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "CalibrateViewController.h"
#import "McpAppDelegate.h"
#import "NewTrainingData.h"
#import "NewTrainingSample.h"
#import "VoiceSample.h"

@implementation CalibrateViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(IBAction)calibrateButtonPressed:(id)sender {
    // starting or stopping?
    if ([calibrateButton.titleLabel.text isEqualToString:@"Calibrate"]) {
        // spin off calibration to a seperate thread
        [NSThread detachNewThreadSelector:@selector(doCalibration) toTarget:self withObject:nil];
        
        // we are now calibrating
        isCalibrating = YES;
        
        // start timer for stat updates every second
        updateTimer = [[NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateTimerFired:) userInfo:nil repeats:YES] retain];
        
        // change button name to stop calibration
        [calibrateButton setTitle:@"Stop Calibration" forState:UIControlStateNormal];
    }
    else {
        // stop calibration
        stopCalibrating = YES;
        
        // wait until we are stopped
        while (stopCalibrating) {
        }
    }
}

-(void)updateTimerFired:(NSTimer*)inTimer {
    // update labels
    iterationsLabel.text = [NSString stringWithFormat:@"Iterations: %d", currentIteration];
    errorSumLabel.text = [NSString stringWithFormat:@"Error Sum: %4.2f", currentErrorSum];
    newRateLabel.text = [NSString stringWithFormat:@"New Recognition Rate: %3.0f%%", currentLowestScore*100.0f];
    newRateProgress.progress = currentLowestScore;
    
    // if we're not calibrating anymore, stop the timer and set the button name back
    if (!isCalibrating) {
        // stop timer
        [updateTimer invalidate];
        [updateTimer release];
        
        // change button name to stop calibration
        [calibrateButton setTitle:@"Calibrate" forState:UIControlStateNormal];
        
        // update good and bad score boundaries
        McpAppDelegate *appDelegate = (McpAppDelegate*)[[UIApplication sharedApplication] delegate];
        appDelegate.currentUserProfile.badScoreBoundary = currentHighestScore;
        appDelegate.currentUserProfile.goodScoreBoundary = currentLowestScore;
        
        // save user profile to save network
        [appDelegate.currentUserProfile saveAs:appDelegate.currentUserProfile.name];
        
        // put up message
        UIAlertView *myAlert = [[UIAlertView alloc] initWithTitle:@"Calibration" message:@"Calibration Complete" delegate:self cancelButtonTitle:@"Okay" otherButtonTitles:nil];
        [myAlert show];
        [myAlert release];
    }
}

-(void)doCalibration {
    // create auto release pool because we're going to spin this off to a seperate thread
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    
    // get application delegate
    McpAppDelegate *appDelegate = (McpAppDelegate*)[[UIApplication sharedApplication] delegate];
    
    // get training data
    NewTrainingData *myTrainingData = [appDelegate.currentUserProfile getTrainingDataForAudioDevice];
    
    // get total sample count
    int totalSampleCount = [self getTotalSampleCount];
    
    // do a test... train neural network with this time-matched data
    // input is both images, output is difference of first half
    int compareFeatureCount = [VoiceSample getCompareFeatureCount];
    int inPos = 0;
    int outPos = 0;
    int inPos1 = 0;
    int outPos1 = 0;
    NSString *nextKey;
    NeuralNet *myNet = [appDelegate.currentUserProfile getNetForAudioDevice];
    double *inputData = malloc(sizeof(double)*totalSampleCount*myNet.inputCount);
    double *outputData = malloc(sizeof(double)*totalSampleCount*myNet.outputCount);
    float *inputData1 = malloc(sizeof(float)*totalSampleCount*myNet.inputCount);
    float *outputData1 = malloc(sizeof(float)*totalSampleCount*myNet.outputCount);
    NSEnumerator *enumerator = [myTrainingData.trainingSamples keyEnumerator];
    int newTotalSampleCount = 0;
    while ((nextKey = [enumerator nextObject])) {
        NewTrainingSample *thisSample = [myTrainingData.trainingSamples objectForKey:nextKey];
        // write out training data
        for (int s=0; s < thisSample.size; s++) {
            newTotalSampleCount++;
            for (int i=0; i < thisSample.featureCount; i++)
                inputData[inPos++] = thisSample.userFeatures[s*thisSample.featureCount+i];
            for (int i=0; i < compareFeatureCount; i++)
                outputData[outPos++] = (thisSample.cpuFeatures[s*thisSample.featureCount+i] - thisSample.userFeatures[s*thisSample.featureCount+i]+1.0f)/2.0f;
        }
        // write out image data
        for (int s=0; s < thisSample.size; s++) {
            for (int i=0; i < thisSample.featureCount; i++)
                inputData1[inPos1++] = thisSample.userFeatures[s*thisSample.featureCount+i];
            for (int i=0; i < compareFeatureCount; i++)
                outputData1[outPos1++] = (thisSample.cpuFeatures[s*thisSample.featureCount+i] - thisSample.userFeatures[s*thisSample.featureCount+i]+1.0f)/2.0f;
        }
    }
    
    // write input and output to debug image
    [VoiceSample writeToImageWithFilename:@"compare2_nocms_netinput.png" alternateBuffer:inputData1 alternateFeatureCount:myNet.inputCount alternateQuantizeCount:totalSampleCount];
    [VoiceSample writeToImageWithFilename:@"compare2_nocms_netoutput.png" alternateBuffer:outputData1 alternateFeatureCount:myNet.outputCount alternateQuantizeCount:totalSampleCount];
    
    // reset weights
    [myNet resetWeights];
    
    // don't allow phone to turn off
    [[UIApplication sharedApplication] setIdleTimerDisabled:YES];
    
    // train neural network using these samples
    // do this in a loop here, so we can track stats
//	double errorThreshold = 0.0001f;
//    float spreadPercent = -9999.0f;
//    float lastSpreadPercent = -9999.0f;
	int passCount = 0;
    do {
		// run network training epoch
        [myNet trainNetworkOnceWithInputSet:inputData ofSize:newTotalSampleCount andOutputSet:outputData];
//        [myNet trainNetworkOnceWithInputSet2:inputData ofSize:totalSampleCount andOutputSet:outputData];
        
        // increment counter
		passCount++;
        
        // update stats
        
        // update stats every 10 epochs
		if ((passCount % 10) == 0.0f) {
            // set simple stuff
            currentErrorSum = myNet.errorSum*100;
            currentIteration = passCount;
            float score = [self getTrainingSamplesLowestScore];
            
            float good = 0.02f;
            float bad = 0.05f;
            score -= good;
            score *= (1.0f/(bad-good));
            score = 1.0f - score;
            if (score < 0.0f)
                score = 0.0f;
            if (score > 1.0f)
                score = 1.0f;
            currentLowestScore = score;
            
            
//            currentHighestScore = [self getTrainingSamplesHighestScore];
//            lastSpreadPercent = spreadPercent;
//            spreadPercent = (currentHighestScore-currentLowestScore)/currentHighestScore;
        }
//    } while (spreadPercent >= lastSpreadPercent && !stopCalibrating);
    } while (!stopCalibrating);
    
    
    // allow phone to turn off again
    [[UIApplication sharedApplication] setIdleTimerDisabled:NO  ];
    
    // free data
    free(inputData);
    free(outputData);
    free(inputData1);
    free(outputData1);
    
    // no longer calibrating
    stopCalibrating = NO;
    isCalibrating = NO;
    
    // release pool
    [pool release];
}

-(int)getTotalSampleCount {
    // get application delegate
    McpAppDelegate *appDelegate = (McpAppDelegate*)[[UIApplication sharedApplication] delegate];
    
    // get training data
    NewTrainingData *myTrainingData = [appDelegate.currentUserProfile getTrainingDataForAudioDevice];
    
    // find total number of samples
    NSEnumerator *enumerator = [myTrainingData.trainingSamples keyEnumerator];
    NSString *nextKey;
    int totalSampleCount = 0;
    while ((nextKey = [enumerator nextObject])) {
        NewTrainingSample *thisSample = [myTrainingData.trainingSamples objectForKey:nextKey];
        totalSampleCount += thisSample.size;
    }
    
    return totalSampleCount;
}

// highest of incorrect scores
-(float)getTrainingSamplesHighestScore {
    // get application delegate
    McpAppDelegate *appDelegate = (McpAppDelegate*)[[UIApplication sharedApplication] delegate];
    
    // put here for now as test
    NewTrainingData *myTrainingData = [appDelegate.currentUserProfile getTrainingDataForAudioDevice];
    
    // turn off calibration while doing this or it'll affect the array we're using
//    BOOL oldCalibrationMode = appDelegate.currentUserProfile.gameOptions.calibrationMode;
//    appDelegate.currentUserProfile.gameOptions.calibrationMode = NO;
    
    // retest samples using current network and get lowest score
    NSString *nextKey;
    float highestscore = 0.0f;
    NSEnumerator *enumerator = [myTrainingData.trainingSamples keyEnumerator];
    while ((nextKey = [enumerator nextObject])) {
        NewTrainingSample *thisSample = [myTrainingData.trainingSamples objectForKey:nextKey];
        
        // reverse cpu version to make it opposite
        float *cpuReversed = malloc(sizeof(float)*thisSample.size*thisSample.featureCount);
        for (int i=0; i < thisSample.size; i++) {
            for (int f=0; f < thisSample.featureCount; f++)
                cpuReversed[i*thisSample.featureCount+f] = thisSample.cpuFeatures[(thisSample.size-i-1)*thisSample.featureCount+f];
        }
        
        // do comparison - use same features for dtw because it's already been
        // stretched
        float score = [VoiceSample compareImage3:cpuReversed ofWidth:thisSample.size andHeight:thisSample.featureCount withImage:thisSample.userFeatures forText:thisSample.text dtwImage:thisSample.cpuFeatures withDtwImage:thisSample.cpuFeatures withHeight:thisSample.featureCount calibrationMode:NO writeDebug:NO compareWithCheckImage:YES phonemeLengthArray:nil];
        free(cpuReversed);
        
        // get score
        if (score > highestscore)
            highestscore = score;
    }
    
    // reset calibration mode
//    appDelegate.currentUserProfile.gameOptions.calibrationMode = oldCalibrationMode;
    
    return highestscore;
}

// lowest of correct scores
-(float)getTrainingSamplesLowestScore {
    // get application delegate
    McpAppDelegate *appDelegate = (McpAppDelegate*)[[UIApplication sharedApplication] delegate];
    
    // put here for now as test
    NewTrainingData *myTrainingData = [appDelegate.currentUserProfile getTrainingDataForAudioDevice];
    
    // turn off calibration while doing this or it'll affect the array we're using
//    BOOL oldCalibrationMode = appDelegate.currentUserProfile.gameOptions.calibrationMode;
//    appDelegate.currentUserProfile.gameOptions.calibrationMode = NO;
    
    // retest samples using current network and get lowest score
    NSString *nextKey;
    float lowestScore = 0.0f;
    int wrongCount = 0;
    int rightCount = 0;
    NSEnumerator *enumerator = [myTrainingData.trainingSamples keyEnumerator];
    while ((nextKey = [enumerator nextObject])) {
        NewTrainingSample *thisSample = [myTrainingData.trainingSamples objectForKey:nextKey];
        
        // do comparison - use same features for dtw because it's already been
        // stretched
        float score = [VoiceSample compareImage3:thisSample.cpuFeatures ofWidth:thisSample.size andHeight:thisSample.featureCount withImage:thisSample.userFeatures forText:thisSample.text dtwImage:thisSample.cpuFeatures withDtwImage:thisSample.cpuFeatures withHeight:thisSample.featureCount calibrationMode:NO writeDebug:NO compareWithCheckImage:YES phonemeLengthArray:nil];
        
        // get score
        if (score > lowestScore)
            lowestScore = score;
        
        if (score > 0.025f) {
            printf("%s is less that 80%%\r\n", [thisSample.text UTF8String]);
            wrongCount++;
        }
        else
            rightCount++;
    }
    
    // reset calibration mode
//    appDelegate.currentUserProfile.gameOptions.calibrationMode = oldCalibrationMode;
    
    return lowestScore;
}

- (void)dealloc
{
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // count samples get current lowest score etc.
    float lowestScore = [self getTrainingSamplesLowestScore];
    samplesLabel.text = [NSString stringWithFormat:@"Samples: %d", [self getTotalSampleCount]];
    startingRateLabel.text = [NSString stringWithFormat:@"Starting Recognition Rate: %3.0f%%", lowestScore*100];
    startingRateProgress.progress = lowestScore;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

@end
