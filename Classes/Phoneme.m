//
//  Phoneme.m
//  Mcp
//
//  Created by Ray on 1/24/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "Phoneme.h"

@implementation Phoneme

@synthesize phonemeName;
@synthesize featureIndex;

-(id)initWithName:(NSString*)name andFeatureIndex:(int)index {
	if (self = [super init]) {
		self.phonemeName = name;
		featureIndex = index;
	}
	return self;
}

-(id)initWithName:(NSString*)inName andDictionaryEntry:(NSDictionary*)inDict {
	if (self = [super init]) {
		self.phonemeName = inName;
		self.featureIndex = [[inDict objectForKey:@"FeatureIndex"] floatValue];
	}
	return self;
}

- (void)dealloc {
	[phonemeName release];
	[super dealloc];
}

@end
