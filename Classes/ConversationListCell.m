//
//  ConversationListCell.m
//  Mcp
//
//  Created by Ray on 1/29/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "ConversationListCell.h"


@implementation ConversationListCell

@synthesize grammarLabel;
@synthesize actorLabel1;
@synthesize actorLabel2;
@synthesize actorLabel3;
@synthesize actorLabel4;
@synthesize dialogueLabel1;
@synthesize dialogueLabel2;
@synthesize dialogueLabel3;
@synthesize dialogueLabel4;

//@synthesize dialogueWebView;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code.
    }
    return self;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state.
}


- (void)dealloc {
	[grammarLabel release];
	[actorLabel1 release];
	[actorLabel2 release];
	[actorLabel3 release];
	[actorLabel4 release];
	[dialogueLabel1 release];
	[dialogueLabel2 release];
	[dialogueLabel3 release];
	[dialogueLabel4 release];
//	[dialogueWebView release];
    [super dealloc];
}


@end
