//
//  EditGrammarController.h
//  Mcp
//
//  Created by Ray on 1/7/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GrammarNote.h"
#import "EditableListDelegate.h"

@interface EditGrammarController : UITableViewController <UITextFieldDelegate, UINavigationControllerDelegate, UIActionSheetDelegate, UIImagePickerControllerDelegate, UITableViewDelegate> {
	BOOL newMode;
	GrammarNote *grammar;
	id<EditableListDelegate> editableListDelegate;
	NSIndexPath *lastIndexPath;	
	BOOL keyboardPresent;
    NSString *oldTitle; // save old title to go through and replace grammar links
}

@property (nonatomic, retain) GrammarNote *grammar;
@property (nonatomic, assign) id<EditableListDelegate> editableListDelegate;

-(void)setEditableObject:(GrammarNote*)thisGrammar isNew:(BOOL)isNew ;
-(BOOL)isOkToSave;
-(void)cancelButtonClick:(id)sender;
-(void)saveButtonClick:(id)sender;
-(void)textFieldDidChange:(NSNotification*)note;
-(void)textViewDidChange:(NSNotification*)note;
-(void)textViewDidBeginEditing:(NSNotification*)note;
-(void)keyboardWillShow:(NSNotification*)note;

@end
