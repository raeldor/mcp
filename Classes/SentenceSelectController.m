//
//  SentenceSelectController.m
//  HF Sensei
//
//  Created by Ray on 4/5/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import "SentenceSelectController.h"
#import "DictionaryListCell.h"
#import "SampleSentences.h"
#import "FlashCard.h"
#import "MultiSelectController.h"

@implementation SentenceSelectController

@synthesize selectDelegate;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // create array for selected sentences
    selectedSentences = [[NSMutableArray alloc] initWithCapacity:10];
}

-(IBAction)selectButtonClick:(id)sender {
	// hide keyboard and pop controller
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
//    [self.navigationController dismissModalViewControllerAnimated:YES];
	
	// call delegate to notify about selection
	[selectDelegate didMakeBrowserSelection:selectedSentences];
}

-(IBAction)cancelButtonClick:(id)sender {
    // if we have cards, confirm first
    if (selectedSentences.count > 0) {
		UIAlertView *myAlert = [[UIAlertView alloc] initWithTitle:@"Cancel" message:@"Cancel adding these cards, are you sure?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
		[myAlert show];
		[myAlert release];
    }
    else {
        // hide keyboard and pop controler
        [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
//        [self.navigationController dismissModalViewControllerAnimated:YES];
    }
}

-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    // if we cancelled, pop controler
    if (buttonIndex == 1) {
        [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
//        [self.navigationController dismissModalViewControllerAnimated:YES];
    }
}

-(int)getSelectedIndexForSample:(SampleEntry*)inWord {
    // already in selected list?
    int selectedIndex = -1;
    for (int i=0; i < selectedSentences.count; i++) {
        FlashCard *thisCard = [selectedSentences objectAtIndex:i];
        if ([thisCard.kanji isEqualToString:inWord.kanji] && [thisCard.kana isEqualToString:inWord.kana]) {
            selectedIndex = i;
            break;
        }
    }
    return selectedIndex;
}

-(UITableViewCellAccessoryType)tableView:(UITableView*)tableView
		accessoryTypeForRowWithIndexPath:(NSIndexPath*)indexPath {
    // which row are we getting
    NSUInteger section = [indexPath section];
    NSUInteger row = [indexPath row];
    
    // get sentence entry
    SampleEntry *sentence;
    if ((self.searchController.isActive || filterIsActive) && ![self.searchController.searchBar.text isEqualToString:@""])
        sentence = [sampleSentences getEntryUsingGroupIndex:0 entryIndex:[[searchResults objectAtIndex:row] intValue]];
    else
        sentence = [sampleSentences getEntryUsingGroupIndex:section entryIndex:row];
    
    // if this is already in selected list return checkmark
    int selectedIndex = [self getSelectedIndexForSample:sentence];
    if (selectedIndex == -1)
        return UITableViewCellAccessoryNone;
    else
        return UITableViewCellAccessoryCheckmark;
}

-(void)tableView:(UITableView*)tableView
didSelectRowAtIndexPath:(NSIndexPath*) indexPath {
	// which row are we getting
	NSUInteger section = [indexPath section];
	NSUInteger row = [indexPath row];
	lastSelectedSection = section;
	lastSelectedRow = row;
	
    // get sentence entry
    SampleEntry *sentence;
    if ((self.searchController.isActive || filterIsActive) && ![self.searchController.searchBar.text isEqualToString:@""])
        sentence = [sampleSentences getEntryUsingGroupIndex:0 entryIndex:[[searchResults objectAtIndex:row] intValue]];
    else
        sentence = [sampleSentences getEntryUsingGroupIndex:section entryIndex:row];
	
    // select or deselect
    int selectedIndex = [self getSelectedIndexForSample:sentence];
    if (selectedIndex == -1) {
        // add new card to selection
        NSArray *myMeanings = [[NSArray alloc] initWithObjects:[sentence meaning], nil];
        FlashCard *newCard = [[FlashCard alloc] initWithKanji:[sentence kanji] kana:[sentence kana] meanings:myMeanings notes:nil];
        [selectedSentences addObject:newCard];
        [newCard release];
        [myMeanings release];
        
        // refresh table to display check mark
        [tableView reloadData];
    }
    else {
        [selectedSentences removeObjectAtIndex:selectedIndex];
        [tableView reloadData];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning]; // Releases the view if it doesn't have a superview
    // Release anything that's not essential, such as cached data
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
}

- (void)dealloc {
    [super dealloc];
}


@end
