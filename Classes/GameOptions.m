//
//  GameOptions.m
//  Mcp
//
//  Created by Ray on 3/8/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "GameOptions.h"


@implementation GameOptions

@synthesize talkingSpeed;
@synthesize playPart;
@synthesize promptMyPart;
@synthesize pauseAfterActor;
@synthesize showTextAs;
@synthesize passAccuracy;

-(id)init {
	if ((self = [super init])) {
		// create default options here
		talkingSpeed = 70;
        playPart = 0;
		promptMyPart = YES;
		pauseAfterActor = NO;
        showTextAs = 1;
		passAccuracy = 0.6f;
	}
	return self;
}

-(id)initWithCoder:(NSCoder*)coder {
	if ((self = [super init])) {
        self.talkingSpeed = [coder decodeInt32ForKey:@"TalkingSpeed"];
        self.playPart = [coder decodeInt32ForKey:@"PlayPart"];
        self.promptMyPart = [coder decodeBoolForKey:@"PromptMyPart"];
        self.showTextAs = [coder decodeInt32ForKey:@"ShowTextAs"];
        self.pauseAfterActor = [coder decodeBoolForKey:@"PauseAfterActor"];
        self.passAccuracy = [coder decodeFloatForKey:@"PassAccuracy"];
	}
	return self;
}

-(void)encodeWithCoder:(NSCoder*)coder {
    [coder encodeInt32:talkingSpeed forKey:@"TalkingSpeed"];
    [coder encodeInt32:playPart forKey:@"PlayPart"];
    [coder encodeBool:promptMyPart forKey:@"PromptMyPart"];
    [coder encodeInt32:showTextAs forKey:@"ShowTextAs"];
    [coder encodeBool:pauseAfterActor forKey:@"PauseAfterActor"];
    [coder encodeFloat:passAccuracy forKey:@"PassAccuracy"];
}

-(id)copyWithZone:(NSZone *)zone {
    GameOptions *newOptions = [[GameOptions alloc] init];
    newOptions.talkingSpeed = talkingSpeed;
    newOptions.playPart = playPart;
    newOptions.promptMyPart = promptMyPart;
    newOptions.pauseAfterActor = pauseAfterActor;
    newOptions.showTextAs = showTextAs;
    newOptions.passAccuracy = passAccuracy;
    return newOptions;
}

-(void)dealloc {
	[super dealloc];
}

@end
