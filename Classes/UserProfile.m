//
//  UserProfile.m
//  Mcp
//
//  Created by Ray on 2/14/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#include "AudioToolbox/AudioQueue.h"
#include "AudioToolbox/AudioToolbox.h"
#import "UserProfile.h"
#import "VoiceSample.h"
#import "PhonemeTrainingData.h"
#import "PhonemeTrainingSample.h"

@implementation UserProfile

#define HIDDEN_COUNT 1
#define LEARN_RATE 0.2f
#define HIDDEN_MULTIPLIER 2.0f

@synthesize name;
//@synthesize translationNet;
@synthesize gender;
@synthesize voicePitch;
@synthesize gameOptions;
@synthesize gameData;
@synthesize clusterSet;
@synthesize bufferHistory;
@synthesize goodScoreBoundary;
@synthesize badScoreBoundary;
//@synthesize featureHistory;

-(id)init {
	if ((self = [super initWithExtension:@".profile" delegate:self])) {
		// create neural network for translation between our voice and synth voice
        int featureCount = 64;
        /*
		micNet = [[NeuralNet alloc] initWithInputCount:featureCount outputCount:featureCount hiddenLayerCount:HIDDEN_COUNT neuronsPerHiddenLayer:featureCount*HIDDEN_MULTIPLIER learningRate:LEARN_RATE];
		btNet = [[NeuralNet alloc] initWithInputCount:featureCount outputCount:featureCount hiddenLayerCount:HIDDEN_COUNT neuronsPerHiddenLayer:featureCount*HIDDEN_MULTIPLIER learningRate:LEARN_RATE];
		headsetNet = [[NeuralNet alloc] initWithInputCount:featureCount outputCount:featureCount hiddenLayerCount:HIDDEN_COUNT neuronsPerHiddenLayer:featureCount*HIDDEN_MULTIPLIER learningRate:LEARN_RATE];
        */
        
		// gender is male by default (0=male, 1=female)
        self.name = nil;
		gender = 0;
		voicePitch = 100;
        goodScoreBoundary = 0.05f;
        badScoreBoundary = 0.1f;
        
        // create buffer history
        bufferHistory = [[BufferHistory alloc] initWithBufferHeight:[VoiceSample getN]/2];
//        featureHistory = [[BufferHistory alloc] initWithBufferHeight:[VoiceSample getMfccCount]];
        
        // create training data history for neural network
        micTrainingData = [[NewTrainingData alloc] init];
        headsetTrainingData = [[NewTrainingData alloc] init];
        btTrainingData = [[NewTrainingData alloc] init];
		
		// create a set of game options for the user
		gameOptions = [[GameOptions alloc] init];
		gameData = [[GameData alloc] init];
    }
    return self;
}

-(id)initWithName:(NSString*)inName {
    self = [self init];
	if (self) {
		// default name here, but check to make sure it doesn't exist and create new one eventually
		self.name = inName;
    
        // create neural network for translation between our voice and synth voice
        int featureCount = 64;
        /*
        micNet = [[NeuralNet alloc] initWithInputCount:featureCount outputCount:featureCount hiddenLayerCount:HIDDEN_COUNT neuronsPerHiddenLayer:featureCount*HIDDEN_MULTIPLIER learningRate:LEARN_RATE];
        btNet = [[NeuralNet alloc] initWithInputCount:featureCount outputCount:featureCount hiddenLayerCount:HIDDEN_COUNT neuronsPerHiddenLayer:featureCount*HIDDEN_MULTIPLIER learningRate:LEARN_RATE];
        headsetNet = [[NeuralNet alloc] initWithInputCount:featureCount outputCount:featureCount hiddenLayerCount:HIDDEN_COUNT neuronsPerHiddenLayer:featureCount*HIDDEN_MULTIPLIER learningRate:LEARN_RATE];
        */
        
        // create buffer history
        bufferHistory = [[BufferHistory alloc] initWithBufferHeight:[VoiceSample getN]/2];
        //    featureHistory = [[BufferHistory alloc] initWithBufferHeight:[VoiceSample getMfccCount]];
        
        // create training data history for neural network
        micTrainingData = [[NewTrainingData alloc] init];
        headsetTrainingData = [[NewTrainingData alloc] init];
        btTrainingData = [[NewTrainingData alloc] init];
    }
    
	return self;
}

-(id)initFromArchive:(NSString*)inName {
    self = [super initFromArchive:inName extension:@".profile" delegate:self];
	if (self) {
        // create buffer history
        // LATER LOAD FROM PROFILE
        bufferHistory = [[BufferHistory alloc] initWithBufferHeight:[VoiceSample getN]/2];
        //    featureHistory = [[BufferHistory alloc] initWithBufferHeight:[VoiceSample getMfccCount]];
	}
	return self;
}

-(NewTrainingData*)getTrainingDataForAudioDevice {
    // find training data depending on current audio device
    NewTrainingData *deviceData = micTrainingData;
    UInt32 rSize = sizeof(CFStringRef);
    CFStringRef route;
    AudioSessionGetProperty(kAudioSessionProperty_AudioRoute, &rSize, &route);
    NSString *audioRoute = (NSString*)route;
    if ([audioRoute isEqualToString:@"HeadsetInOut"])
        deviceData = headsetTrainingData;
    if ([audioRoute isEqualToString:@"HeadsetBT"])
        deviceData = btTrainingData;
    CFRelease(route);
    
    return deviceData;
}

-(NeuralNet*)getNetForAudioDevice {
    // find training data depending on current audio device
    NeuralNet *deviceNet = micNet;
    UInt32 rSize = sizeof(CFStringRef);
    CFStringRef route;
    AudioSessionGetProperty(kAudioSessionProperty_AudioRoute, &rSize, &route);
    NSString *audioRoute = (NSString*)route;
    if ([audioRoute isEqualToString:@"HeadsetInOut"])
        deviceNet = headsetNet;
    if ([audioRoute isEqualToString:@"HeadsetBT"])
        deviceNet = btNet;
    CFRelease(route);
    
    return deviceNet;
}

-(int)getSampleRateForAudioDevice {
    // find audio device
    UInt32 rSize = sizeof(CFStringRef);
    CFStringRef route;
    AudioSessionGetProperty(kAudioSessionProperty_AudioRoute, &rSize, &route);
    NSString *audioRoute = (NSString*)route;
    if ([audioRoute isEqualToString:@"HeadsetBT"]) {
        CFRelease(route);
        return 8000;
    }
    else {
        CFRelease(route);
        return 16000;
    }
}

-(void)replaceNetForAudioDeviceWith:(NeuralNet*)inNet {
    // find training data depending on current audio device
    UInt32 rSize = sizeof(CFStringRef);
    CFStringRef route;
    AudioSessionGetProperty(kAudioSessionProperty_AudioRoute, &rSize, &route);
    NSString *audioRoute = (NSString*)route;
    if ([audioRoute isEqualToString:@"HeadsetInOut"]) {
        [headsetNet release];
        headsetNet = [inNet retain];
    }
    else {
        if ([audioRoute isEqualToString:@"HeadsetBT"]) {
            [btNet release];
            btNet = [inNet retain];
        }
        else {
            [micNet release];
            micNet = [inNet retain];
        }
    }

    // release string
    CFRelease(route);
}

-(void)archiveFieldsUsingArchiver:(NSKeyedArchiver*)inArchiver {
	// archive fields
	[inArchiver encodeObject:name forKey:@"Name"];
	[inArchiver encodeInt32:gender forKey:@"Gender"];
	[inArchiver encodeInt32:voicePitch forKey:@"VoicePitch"];
	[inArchiver encodeObject:gameOptions forKey:@"GameOptions"];
	[inArchiver encodeObject:gameData forKey:@"GameData"];
    [inArchiver encodeObject:micNet forKey:@"MicNet"];
    [inArchiver encodeObject:btNet forKey:@"BtNet"];
    [inArchiver encodeObject:headsetNet forKey:@"HeadsetNet"];
//    [inArchiver encodeObject:micTrainingData forKey:@"micTrainingData"];
//    [inArchiver encodeObject:btTrainingData forKey:@"btTrainingData"];
//    [inArchiver encodeObject:headsetTrainingData forKey:@"headsetTrainingData"];
    [inArchiver encodeFloat:badScoreBoundary forKey:@"badScoreBoundary"];
    [inArchiver encodeFloat:goodScoreBoundary forKey:@"goodScoreBoundary"];
}

-(void)unarchiveFieldsUsingUnarchiver:(NSKeyedUnarchiver*)inUnarchiver {
	// unarchive fields
    self.name = [inUnarchiver decodeObjectForKey:@"Name"];
    self.gender = [inUnarchiver decodeInt32ForKey:@"Gender"];
    self.voicePitch = [inUnarchiver decodeInt32ForKey:@"VoicePitch"];
    self.gameOptions = [inUnarchiver decodeObjectForKey:@"GameOptions"];
    self.gameData = [inUnarchiver decodeObjectForKey:@"GameData"];
    micNet = [[inUnarchiver decodeObjectForKey:@"MicNet"] retain];
    btNet = [[inUnarchiver decodeObjectForKey:@"BtNet"] retain];
    headsetNet = [[inUnarchiver decodeObjectForKey:@"HeadsetNet"] retain];
//    micTrainingData = [[inUnarchiver decodeObjectForKey:@"micTrainingData"] retain];
//    btTrainingData = [[inUnarchiver decodeObjectForKey:@"btTrainingData"] retain];
//    headsetTrainingData = [[inUnarchiver decodeObjectForKey:@"headsetTrainingData"] retain];
    self.badScoreBoundary = [inUnarchiver decodeFloatForKey:@"badScoreBoundary"];
    self.goodScoreBoundary = [inUnarchiver decodeFloatForKey:@"goodScoreBoundary"];
    
    // create neural network for translation between our voice and synth voice
    /*
    if (micNet == nil) {
        // create net for each audio device
        micNet = [[NeuralNet alloc] initWithInputCount:[VoiceSample getFeatureCount] outputCount:[VoiceSample getOutputFeatureCount] hiddenLayerCount:HIDDEN_COUNT neuronsPerHiddenLayer:[VoiceSample getFeatureCount]*HIDDEN_MULTIPLIER learningRate:LEARN_RATE];
        btNet = [[NeuralNet alloc] initWithInputCount:[VoiceSample getFeatureCount] outputCount:[VoiceSample getOutputFeatureCount] hiddenLayerCount:HIDDEN_COUNT neuronsPerHiddenLayer:[VoiceSample getFeatureCount]*HIDDEN_MULTIPLIER learningRate:LEARN_RATE];
        headsetNet = [[NeuralNet alloc] initWithInputCount:[VoiceSample getFeatureCount] outputCount:[VoiceSample getOutputFeatureCount] hiddenLayerCount:HIDDEN_COUNT neuronsPerHiddenLayer:[VoiceSample getFeatureCount]*HIDDEN_MULTIPLIER learningRate:LEARN_RATE];
    }*/
    
    // create training data
    if (micTrainingData == nil) {
        micTrainingData = [[NewTrainingData alloc] init];
        headsetTrainingData = [[NewTrainingData alloc] init];
        btTrainingData = [[NewTrainingData alloc] init];
    }

    /*
    // remove all but first phoneme training samples
    NSEnumerator *enumerator = [micTrainingData.phonemeTrainingData objectEnumerator];
    PhonemeTrainingData *phonemeData;
    while ((phonemeData = [enumerator nextObject])) {
        // delete all but first training sample
//        while (phonemeData.trainingSamples.count > 1)
//            [phonemeData.trainingSamples removeObjectAtIndex:1];
        for (int i=0; i < phonemeData.trainingSamples.count; i++) {
            PhonemeTrainingSample *thisTrainingSample = [phonemeData.trainingSamples objectAtIndex:i];
            [thisTrainingSample.sampleBuffer writeToWavFile:[NSString stringWithFormat:@"training_sample_%@_%d.wav", phonemeData.phonemeName, i]];
            
        }
    }
     */
}

-(void)dealloc {
	[name release];
//	[translationNet release];
    [micNet release];
    [btNet release];
    [headsetNet release];
	[gameOptions release];
	[gameData release];
    [bufferHistory release];
//    [featureHistory release];
    [micTrainingData release];
    [headsetTrainingData release];
    [btTrainingData release];
	[super dealloc];
}

@end
