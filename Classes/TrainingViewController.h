//
//  TrainingViewController.h
//  Mcp
//
//  Created by Ray on 1/24/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#include "AudioToolbox/AudioQueue.h"
#include "AudioToolbox/AudioToolbox.h"
#include "AVFoundation/AVFoundation.h"
#include "AudioStates.h"
#include "TrainingData.h"
#include "Phoneme.h"
#include "Syllable.h"
#import "AudioRecorder.h"
#import "AudioPlayer.h"

@interface TrainingViewController : UIViewController {
	IBOutlet UIButton *beginButton;
	IBOutlet UIButton *endButton;
	IBOutlet UIButton *backButton;
	IBOutlet UIButton *clearButton;
	IBOutlet UILabel *sayLabel;
	IBOutlet UILabel *statusLabel;
	
    AudioRecorder *myRecorder;
    AudioPlayer *myPlayer;
    
	NSTimer *labelTimer;
	
	TrainingData *trainingData;
	Phoneme *recordedPhoneme;
	Syllable *recordedSyllable;
}

@property (nonatomic, retain) UIButton *beginButton;
@property (nonatomic, retain) UIButton *endButton;
@property (nonatomic, retain) UIButton *clearButton;
@property (nonatomic, retain) UIButton *backButton;
@property (nonatomic, retain) UILabel *sayLabel;
@property (nonatomic, retain) UILabel *statusLabel;

-(IBAction)beginButtonPressed:(id)sender;
-(IBAction)endButtonPressed:(id)sender;
-(IBAction)clearButtonPressed:(id)sender;
-(IBAction)backButtonPressed:(id)sender;

-(void)doNextAction;
-(void) normalizeFloatArray:(float*)inArray ofSize:(int)inSize;
-(void)writeBuffer:(float*)audioBuffer ofSize:(int)bufferSize ToWavFile:(NSString *)inFilename;
-(void)getStartEndForPhonemeIndex:(int)inPhonemeIndex inSyllable:(Syllable*)inSyllable usingBreaks:(int*)inPhonemeBreaks andQuantizeCount:(int)inQuantizeCount intoStart:(int*)inStart andEnd:(int*)inEnd;

//
// c only stuff
//

/*
void AudioInputCallback3(void *inUserData, AudioQueueRef inAQ, AudioQueueBufferRef inBuffer, const AudioTimeStamp *inStartTime, UInt32 inNumberPacketDescriptions, const AudioStreamPacketDescription *inPacketDescs);
void AudioOutputCallback3(void *inUserData, AudioQueueRef inAQ, AudioQueueBufferRef inBuffer);
void PlaybackFinishedCallback3(void *inUserData, AudioQueueRef inAQ, AudioQueuePropertyID inID);

RecordState recordState;
BOOL isRecording;
BOOL isAnalyzing;
BOOL isListening;
void* recordBuffer;
int recordBufferPos;

PlayState playState;
int playBufferPos;
BOOL isPlaying;
 
 */

@end
