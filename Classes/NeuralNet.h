//
//  NeuralNet.h
//  Mcp
//
//  Created by Ray on 2/6/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NeuralNet : NSObject <NSCoding, NSCopying> {
	int inputCount;
	int outputCount;
	int hiddenLayerCount;
	int neuronsPerHiddenLayer;
	double learningRate;
	double bias;
	double momentumDelta;
	double activationResponse;
	bool trained;
    
	double errorSum;
	int numEpochs;
    
	NSMutableArray *layers;
}

-(id)initWithInputCount:(int)inInputCount outputCount:(int)inOutputCount hiddenLayerCount:(int)inHiddenLayerCount neuronsPerHiddenLayer:(int)inNeuronsPerHiddenLayer learningRate:(double)inLearningRate;
-(void)resetWeights;
-(void)dealloc;
-(double*)updateWithInputs:(double*)inInputs;
-(double)sigmoidWithInput:(double)inInput response:(double)inResponse;
-(bool)trainNetworkOnceWithInputSet:(double*)inInputSet ofSize:(int)inSetSize andOutputSet:(double*)inOutputSet;
-(bool)trainNetworkWithInputSet:(double*)inInputSet ofSize:(int)inSetSize andOutputSet:(double*)inOutputSet;
-(bool)trainNetworkOnceWithInputSet2:(double*)inInputSet ofSize:(int)inSetSize andOutputSet:(double*)inOutputSet;

@property (nonatomic, retain) NSMutableArray *layers;
@property (nonatomic, assign) int inputCount;
@property (nonatomic, assign) int outputCount;
@property (nonatomic, assign) int hiddenLayerCount;
@property (nonatomic, assign) int neuronsPerHiddenLayer;
@property (nonatomic, assign) double bias;
@property (nonatomic, assign) double activationResponse;
@property (nonatomic, assign) double learningRate;
@property (nonatomic, assign) double errorSum;
@property (nonatomic, assign) bool trained;
@property (nonatomic, assign) int numEpochs;

@end
