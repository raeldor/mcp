//
//  CharacterCell.h
//  Mcp
//
//  Created by Ray on 11/3/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface CharacterCell : UITableViewCell {
	IBOutlet UIImageView *actorImageView;
	IBOutlet UITextField *characterNameTextField;
	IBOutlet UILabel *actorNameLabel;
}

@property (nonatomic, retain) UIImageView *actorImageView;
@property (nonatomic, retain) UITextField *characterNameTextField;
@property (nonatomic, retain) UILabel *actorNameLabel;

@end
