//
//  EditDialogueLineController.h
//  Mcp
//
//  Created by Ray on 1/18/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DialogueLine.h"
#import "EditDialogueLineDelegate.h"
#import "SelectControllerDelegate.h"

@interface EditDialogueLineController : UITableViewController <SelectControllerDelegate> {
	BOOL newMode;
	DialogueLine *thisLine;
	id<EditDialogueLineDelegate> editDialogueLineDelegate;
	UITextView *textViewBeingEdited;
    UIWebView *webViewBeingEdited;
	UITextView *linkTextView;
	NSRange linkRange;
}

@property (nonatomic, retain) DialogueLine *thisLine;
@property (nonatomic, assign) id<EditDialogueLineDelegate> editDialogueLineDelegate;

-(void)setEditableObject:(DialogueLine*)thisLine isNew:(BOOL)isNew ;
-(BOOL)isOkToSave;
-(void)cancelButtonClick:(id)sender;
-(void)saveButtonClick:(id)sender;
-(void)textViewDidChange:(NSNotification*)note;
-(void)textViewDidBeginEditing:(NSNotification*)note;
-(void)textViewDidEndEditing:(NSNotification*)note;
-(void)linkGrammar:(UIMenuController*)sender;

@end
