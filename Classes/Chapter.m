//
//  Chapter.m
//  Mcp
//
//  Created by Ray on 10/25/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "Chapter.h"
#import "McpAppDelegate.h"

@implementation Chapter

@synthesize uniqueKey;
@synthesize chapterName;
@synthesize conversations;
@synthesize grammarNotes;

-(id)init {
    self = [super init];
	if (self) {
        self.uniqueKey = [McpAppDelegate getUuid];
		conversations = [[NSMutableArray alloc] init];
		grammarNotes = [[NSMutableArray alloc] init];
	}
	return self;
}

-(id)initWithCoder:(NSCoder*)coder {
    self = [super init];
	if (self) {
		self.uniqueKey = [coder decodeObjectForKey:@"UniqueKey"];
        // remove later
        if (uniqueKey == nil)
            self.uniqueKey = [McpAppDelegate getUuid];
		self.chapterName = [coder decodeObjectForKey:@"ChapterName"];
		self.conversations = [coder decodeObjectForKey:@"Conversations"];
		self.grammarNotes = [coder decodeObjectForKey:@"GrammarNotes"];
        if (grammarNotes == nil) {
            grammarNotes = [[NSMutableArray alloc] init];
        }
	}
	return self;
}

-(void)encodeWithCoder:(NSCoder*)coder {
    [coder encodeObject:uniqueKey forKey:@"UniqueKey"];
	[coder encodeObject:chapterName forKey:@"ChapterName"];
	[coder encodeObject:conversations forKey:@"Conversations"];
	[coder encodeObject:grammarNotes forKey:@"GrammarNotes"];
}

-(void)dealloc {
    [uniqueKey release];
	[chapterName release];
	[conversations release];
    [grammarNotes release];
	[super dealloc];
}

@end
