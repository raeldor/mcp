//
//  TrainingData.m
//  Mcp
//
//  Created by Ray on 1/24/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "TrainingData.h"
#import "Phoneme.h"
#import "Syllable.h"
#import "TrainingSentence.h"

@implementation TrainingData

@synthesize trainingSentences;
@synthesize trainingSamples;
@synthesize phonemeIndexes;
@synthesize syllables;

@synthesize phonemeList;
@synthesize phonemeData;

-(id)init {
	if (self = [super init]) {
		// initialize arrays
		self.phonemeList = [NSMutableArray array];
		self.phonemeData = [NSMutableDictionary dictionary];
		
        /*
		// add japanese phonemes
		Phoneme *newPhoneme;
		newPhoneme = [[Phoneme alloc] initWithName:@"a" andKana:@"あ。"];
		newPhoneme.phonemeIndex = 0;
		[phonemeList addObject:newPhoneme];
		
		newPhoneme = [[Phoneme alloc] initWithName:@"i" andKana:@"い。"];
		newPhoneme.phonemeIndex = 1;
		[phonemeList addObject:newPhoneme];
		
		newPhoneme = [[Phoneme alloc] initWithName:@"u" andKana:@"う。"];
		newPhoneme.phonemeIndex = 2;
		[phonemeList addObject:newPhoneme];
		
		newPhoneme = [[Phoneme alloc] initWithName:@"e" andKana:@"え。"];
		newPhoneme.phonemeIndex = 3;
		[phonemeList addObject:newPhoneme];
		
		newPhoneme = [[Phoneme alloc] initWithName:@"o" andKana:@"お。"];
		newPhoneme.phonemeIndex = 4;
		[phonemeList addObject:newPhoneme];
		
		newPhoneme = [[Phoneme alloc] initWithName:@"imasu" andKana:@"います。"];
		newPhoneme.startSelection = 0.7f;
		newPhoneme.endSelection = 1.0f;
		newPhoneme.phonemeIndex = 5;
		[phonemeList addObject:newPhoneme];
		
		// ---------------
		
		newPhoneme = [[Phoneme alloc] initWithName:@"ma" andKana:@"ま。"];
		newPhoneme.startSelection = 0.6f;
		newPhoneme.endSelection = 1.0f;
		newPhoneme.phonemeIndex = 0;
		[phonemeList addObject:newPhoneme];
		
		newPhoneme = [[Phoneme alloc] initWithName:@"mi" andKana:@"み。"];
		newPhoneme.startSelection = 0.6f;
		newPhoneme.endSelection = 1.0f;
		newPhoneme.phonemeIndex = 1;
		[phonemeList addObject:newPhoneme];
		
		newPhoneme = [[Phoneme alloc] initWithName:@"mu" andKana:@"む。"];
		newPhoneme.startSelection = 0.6f;
		newPhoneme.endSelection = 1.0f;
		newPhoneme.phonemeIndex = 2;
		[phonemeList addObject:newPhoneme];
		
		newPhoneme = [[Phoneme alloc] initWithName:@"me" andKana:@"め。"];
		newPhoneme.startSelection = 0.6f;
		newPhoneme.endSelection = 1.0f;
		newPhoneme.phonemeIndex = 3;
		[phonemeList addObject:newPhoneme];
		
		newPhoneme = [[Phoneme alloc] initWithName:@"mo" andKana:@"も。"];
		newPhoneme.startSelection = 0.6f;
		newPhoneme.endSelection = 1.0f;
		newPhoneme.phonemeIndex = 4;
		[phonemeList addObject:newPhoneme];
		
		// ---------------
		
		newPhoneme = [[Phoneme alloc] initWithName:@"sa" andKana:@"さ。"];
		newPhoneme.startSelection = 0.6f;
		newPhoneme.endSelection = 1.0f;
		newPhoneme.phonemeIndex = 0;
		[phonemeList addObject:newPhoneme];
		
		newPhoneme = [[Phoneme alloc] initWithName:@"shi" andKana:@"し。"];
		newPhoneme.startSelection = 0.6f;
		newPhoneme.endSelection = 1.0f;
		newPhoneme.phonemeIndex = 1;
		[phonemeList addObject:newPhoneme];
		
		newPhoneme = [[Phoneme alloc] initWithName:@"su" andKana:@"す。"];
		newPhoneme.startSelection = 0.6f;
		newPhoneme.endSelection = 1.0f;
		newPhoneme.phonemeIndex = 2;
		[phonemeList addObject:newPhoneme];
		
		newPhoneme = [[Phoneme alloc] initWithName:@"se" andKana:@"せ。"];
		newPhoneme.startSelection = 0.6f;
		newPhoneme.endSelection = 1.0f;
		newPhoneme.phonemeIndex = 3;
		[phonemeList addObject:newPhoneme];
		
		newPhoneme = [[Phoneme alloc] initWithName:@"so" andKana:@"そ。"];
		newPhoneme.startSelection = 0.6f;
		newPhoneme.endSelection = 1.0f;
		newPhoneme.phonemeIndex = 4;
		[phonemeList addObject:newPhoneme];
		
		// -------------------
		
		[newPhoneme release];
		
		
		
		*/
        
		// create samples array
		self.trainingSamples = [NSMutableArray array];
		
		// load syllables from plist
		self.syllables = [NSMutableArray array];
		NSString *path = [[NSBundle mainBundle] pathForResource:@"Syllables" ofType:@"plist"];
		NSArray *importArray = [[NSArray alloc] initWithContentsOfFile:path];
		for (int i=0; i < importArray.count; i++) {
			// add new syllable
			Syllable *newSyllable = [[Syllable alloc] initWithDictionary:[importArray objectAtIndex:i]];
			[syllables addObject:newSyllable];
			[newSyllable release];
		}
		[importArray release];
		
/*		
		NSEnumerator *enumerator = [importDictionary keyEnumerator];
		id key;
		while ((key = [enumerator nextObject])) {
			// add new syllable
			NSDictionary *thisSyllable = [importDictionary objectForKey:key];
			Syllable *newSyllable = [[Syllable alloc] initWithName:key andPhonemes:[thisSyllable objectForKey:@"Phonemes"]];
			[syllables setObject:newSyllable forKey:key];
			[newSyllable release];
		}
		[importDictionary release];
		*/
		
		// load phoneme indexes
		self.phonemeIndexes = [NSMutableDictionary dictionary];
		path = [[NSBundle mainBundle] pathForResource:@"Phonemes" ofType:@"plist"];
		NSDictionary *importDictionary = [[NSMutableDictionary alloc] initWithContentsOfFile:path];
		NSEnumerator *enumerator = [importDictionary keyEnumerator];
		id key;
		while ((key = [enumerator nextObject])) {
			// add new phoneme
			NSDictionary *thisPhoneme = [importDictionary objectForKey:key];
			NSNumber *featureIndex = [thisPhoneme objectForKey:@"FeatureIndex"];
			Phoneme *newPhoneme = [[Phoneme alloc] initWithName:key andFeatureIndex:[featureIndex intValue]];
			[phonemeIndexes setObject:newPhoneme forKey:key];
			[newPhoneme release];
		}
		[importDictionary release];
		
		// load training sentences
		self.trainingSentences = [NSMutableArray array];
/*		path = [[NSBundle mainBundle] pathForResource:@"TrainingSentences" ofType:@"plist"];
		NSArray *importArray = [[NSArray alloc] initWithContentsOfFile:path];
		for (int i=0; i < importArray.count; i++) {
			// add new training sentence
			TrainingSentence *newSentence = [[TrainingSentence alloc] initWithDictionary:[importArray objectAtIndex:i]];
			[trainingSentences addObject:newSentence];
			[newSentence release];
		}
		[importArray release];*/
	}
	return self;
}

-(Syllable*)findSyllableFromKana:(NSString*)inKana {
	// find by looking through array
	for (int i=0; i < syllables.count; i++) {
		Syllable *thisSyllable = [syllables objectAtIndex:i];
		if ([thisSyllable.syllableKana isEqualToString:inKana])
			return thisSyllable;
	}
	return nil;
}

- (void)dealloc {
	[trainingSamples release];
	[trainingSentences release];
	[phonemeIndexes release];
	[syllables release];
	[super dealloc];
}

@end
