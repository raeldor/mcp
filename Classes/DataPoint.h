//
//  DataPoint.h
//  Mcp
//
//  Created by Ray on 3/20/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DataPoint : NSObject {
	int dataCount;
	float *data;
	bool visited;
	bool noise;
	int clusterIndex;
	int dataSourceIndex;
}

-(id)initWithData:(float*)inData count:(int)inCount dataSourceIndex:(int)inDataSourceIndex;
-(void)dealloc;
-(float)distanceFromDataPoint:(DataPoint*)inDataPoint;

@property (nonatomic, assign) int dataCount;
@property (nonatomic, assign) bool visited;
@property (nonatomic, assign) bool noise;
@property (nonatomic, assign) float *data;
@property (nonatomic, assign) int clusterIndex;
@property (nonatomic, assign) int dataSourceIndex;

@end
