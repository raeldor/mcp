//
//  NameCell.h
//  Mcp
//
//  Created by Ray on 10/19/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface NameCell : UITableViewCell <UITextFieldDelegate> {
	IBOutlet UILabel *nameLabel;
	IBOutlet UITextField *nameTextField;
}

@property (nonatomic, retain) UILabel *nameLabel;
@property (nonatomic, retain) UITextField *nameTextField;

@end
