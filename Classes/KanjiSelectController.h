//
//  KanjiSelectController.h
//  HF Sensei
//
//  Created by Ray Price on 2/25/13.
//
//

#import <UIKit/UIKit.h>
#import "MultiSelectControllerDelegate.h"
#import "BrowserSelectDelegate.h"
#import "NewKanjiBrowseController.h"

@interface KanjiSelectController : NewKanjiBrowseController <UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate, UISearchDisplayDelegate, MultiSelectControllerDelegate> {
    IBOutlet UILongPressGestureRecognizer *longPressRecognizer;
    
    UITableView *lastSelectedTableView;
    NSUInteger lastSelectedSection;
    NSUInteger lastSelectedRow;
    
	// selected kanji
	NSMutableArray *selectedKanjis;
}

@property (nonatomic, assign) id<BrowserSelectDelegate> selectDelegate;

-(IBAction)handleLongPressGesture:(UIGestureRecognizer *)gestureRecognizer;
-(void)didSelectRowAtIndexPath:(NSIndexPath*)indexPath forTableView:(UITableView*)tableView wasLongPress:(BOOL)wasLongPress;
-(int)getSelectedIndexForKanji:(NSString*)inKanji;
-(IBAction)selectButtonClick:(id)sender;
-(IBAction)cancelButtonClick:(id)sender;

@end
