//
//  ChapterListViewController.m
//  Mcp
//
//  Created by Ray on 10/25/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "ChapterListViewController.h"
#import "ConversationListViewController.h"
#import "GrammarListViewController.h"
#import "EditChapterController.h"
#import "Chapter.h"

@implementation ChapterListViewController

@synthesize book;

#pragma mark -
#pragma mark View lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
	
	// set title
	self.title = @"Chapters";
	
	// allow selecting during editor
	self.tableView.allowsSelection = YES;
	self.tableView.allowsSelectionDuringEditing = YES;
	
	// add edit button
	self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

-(void)setFinalEditTypeAs:(int)inEditType {
    finalEditType = inEditType;
}

-(void)setEditableObject:(Book *)inBook {
	self.book = inBook;
}

#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 1;
}

//-(NSString*)tableView:(UITableView*)tableView
//titleForHeaderInSection:(NSInteger)section {
//}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows
	if (self.editing)
		return book.chapters.count+1;
	else
		if (book.chapters.count == 0)
			return 1;
		else
			return book.chapters.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	return 44;
}

// Customize the appearance of table view cells.
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	// get regular table cell
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
		cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
	
	// default cell config
	cell.textLabel.text = NULL;
	cell.imageView.image = NULL;
	cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
	cell.editingAccessoryType = UITableViewCellAccessoryNone;
	cell.selectionStyle = UITableViewCellSelectionStyleNone;
	cell.textLabel.textColor = [UIColor blackColor];
	
	// if no cells, display 'press to add' message
	if (book.chapters.count == 0 && !self.editing) {
		cell.textLabel.text = @"Press Edit Button To Create New Chapter";
		cell.textLabel.textColor = [UIColor lightGrayColor];
		cell.textLabel.adjustsFontSizeToFitWidth = YES;
	}
	else {
		// Set up the cell...
		NSUInteger row = [indexPath row];
		if (row < book.chapters.count) {
			cell.textLabel.text = [[book.chapters objectAtIndex:[indexPath row]] chapterName];
			cell.editingAccessoryType = UITableViewCellAccessoryDisclosureIndicator;
			cell.selectionStyle = UITableViewCellSelectionStyleBlue;
		}
		else {
			cell.textLabel.text = @"Add New";
			cell.textLabel.textColor = [UIColor lightGrayColor];
			cell.selectionStyle = UITableViewCellSelectionStyleBlue;
		}
	}
    
	return cell;
}

//editing mode changed
- (void)setEditing:(BOOL)editing animated:(BOOL)animated {
	// add/remove placeholder items
	NSMutableArray *indexPaths = [NSMutableArray array];
	[indexPaths addObject:[NSIndexPath indexPathForRow:book.chapters.count inSection:0]];
	[self.tableView beginUpdates];
	[super setEditing:editing animated:animated];
	[self.tableView setEditing:editing animated:YES];
	if (editing) {
		// delete 'press edit' message
		if (book.chapters.count == 0)
			[self.tableView deleteRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationTop];
		
		// Show the placeholder rows and hide back button
		[self.tableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationTop];
		self.navigationItem.hidesBackButton = YES;
	}
	else
	{
		// Hide the placeholder rows and show back button again
		[self.tableView deleteRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationTop];
		self.navigationItem.hidesBackButton = NO;
		
		// show 'press edit' message
		if (book.chapters.count == 0)
			[self.tableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationTop];
	}
	[self.tableView endUpdates];
}

-(UITableViewCellEditingStyle)tableView:(UITableView*)tableView editingStyleForRowAtIndexPath:(NSIndexPath*)indexPath {
	// if we are in edit mode
	if (self.editing) {
		// show delete style and insert for 'add new'
		if (book.chapters.count > 0 && [indexPath row] != book.chapters.count)
			return UITableViewCellEditingStyleDelete;
		else
			return UITableViewCellEditingStyleInsert;
	}
	return UITableViewCellEditingStyleNone;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    // if we are deleting rows
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source and table
		[book.chapters removeObjectAtIndex:[indexPath row]];
        [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:YES];
		
		// save the book
		[book saveAs:book.bookName];
    }   
    else {
		if (editingStyle == UITableViewCellEditingStyleInsert) {
			// insert new chapter
			EditChapterController *editChapterController = [[EditChapterController alloc] initWithNibName:@"EditConversationController" bundle:nil];
			Chapter *newChapter = [[Chapter alloc] init];
			[editChapterController setEditableObject:newChapter isNew:YES];
			editChapterController.editableListDelegate = self;
			UINavigationController *secondNavigationController = [[UINavigationController alloc] initWithRootViewController:editChapterController];
			[self.navigationController presentModalViewController:secondNavigationController animated:YES];
			[newChapter release];
			[editChapterController release];
			[secondNavigationController release];
		}
	}
}

-(void)didUpdateObject:(id)updatedObject {
    // save book
	[book saveAs:book.bookName];
	
	// refresh table
	[self.tableView reloadData];
}

-(void)didAddNewObject:(id)newObject {
	// save new class
	Chapter *newChapter = (Chapter*)newObject;
	[book.chapters addObject:newChapter];
	[book saveAs:book.bookName];
	
	// refresh table
	[self.tableView reloadData];
}

#pragma mark -
#pragma mark Table view delegate

-(void)tableView:(UITableView*)tableView didSelectRowAtIndexPath:(NSIndexPath*) indexPath {
	// find row
	NSUInteger row = [indexPath row];
	
	// editing mode?
	if (self.editing) {
		// add new?
		if (row >= [book.chapters count]) {
			[self tableView:tableView commitEditingStyle:UITableViewCellEditingStyleInsert forRowAtIndexPath:indexPath];
		}
		else {
			// edit chapter using edit controller
			EditChapterController *editChapterController = [[EditChapterController alloc] initWithNibName:@"EditChapterController" bundle:nil];
			[editChapterController setEditableObject:[book.chapters objectAtIndex:row] isNew:NO];
			editChapterController.editableListDelegate = self;
			[self.navigationController pushViewController:editChapterController animated:YES];
			[editChapterController release];
		}
	}
	else {
		if (book.chapters.count > 0) {
            // show conversations or grammar
            if (finalEditType == 0) {
                // selected chapter in non edit mode, so show conversations
                ConversationListViewController *convListController = [[ConversationListViewController alloc] initWithNibName:@"ConversationListViewController" bundle:nil];
                [convListController setEditableObject:[book.chapters objectAtIndex:row] inBook:book];
                [self.navigationController pushViewController:convListController animated:YES];
                [convListController release];
            }
            else {
                // selected chapter in non edit mode, so show grammar notes
                GrammarListViewController *grammarListController = [[GrammarListViewController alloc] initWithNibName:@"GrammarListViewController" bundle:nil];
                [grammarListController setEditableObject:[book.chapters objectAtIndex:row] inBook:book];
                [self.navigationController pushViewController:grammarListController animated:YES];
                [grammarListController release];
            }
		}
	}
}

-(BOOL)tableView:(UITableView*)tableView canMoveRowAtIndexPath:(NSIndexPath*)indexPath {
	// can't re-order 'add new'
	if ([indexPath row] < book.chapters.count)
		return YES;
	else
		return NO;
}

-(void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
	// get from and to row
	int fromRow = [fromIndexPath row];
	int toRow = [toIndexPath row];
	
	// swap these rows in the array
	Chapter *moveChapter = [[book.chapters objectAtIndex:fromRow] retain];
	[book.chapters removeObjectAtIndex:fromRow];
	[book.chapters insertObject:moveChapter atIndex:toRow];
	[moveChapter release];
	
	// save changes
	[book saveAs:book.bookName];
}

#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    // Relinquish ownership of anything that can be recreated in viewDidLoad or on demand.
    // For example: self.myOutlet = nil;
}


- (void)dealloc {
	[book release];
    [super dealloc];
}


@end

