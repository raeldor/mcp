//
//  BackgroundListViewController.m
//  Mcp
//
//  Created by Ray on 10/19/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "BackgroundListViewController.h"
#import "EditBackgroundController.h"
#import "Background.h"
#import "BackgroundListCell.h"
#import "McpAppDelegate.h"

@implementation BackgroundListViewController

@synthesize backgroundList;

#pragma mark -
#pragma mark View lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];

	// set title
	self.title = @"Backgrounds";
 
	// allow selecting during editor
	self.tableView.allowsSelection = FALSE;
	self.tableView.allowsSelectionDuringEditing = TRUE;
	
	// add edit button
	self.navigationItem.rightBarButtonItem = self.editButtonItem;
	
	// search for deck archive files
	[self populateBackgroundList];
}

-(void)populateBackgroundList {
	// create new list of backgrounds
	self.backgroundList = [Background getListOfFilesWithExtension:@"background"];
}

#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	if (self.editing)
		return backgroundList.count+1;
	else
	{
		if (backgroundList.count == 0)
			return 1;
		else
			return backgroundList.count;
	}
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	return 120;
}

// Customize the appearance of table view cells.
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	// get application delegate
    McpAppDelegate *appDelegate = (McpAppDelegate*)[[UIApplication sharedApplication] delegate];
    
	// get regular table cell
    static NSString *CellIdentifier = @"BackgroundListCell";
    BackgroundListCell *cell = (BackgroundListCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
		// load cell layout from nib
		NSArray *nib = [[NSBundle mainBundle] loadNibNamed:CellIdentifier owner:self options:nil];
		cell = [nib objectAtIndex:0];
    }
	
	// default cell config
	cell.textLabel.text = NULL;
	cell.imageView.image = NULL;
	cell.accessoryType = UITableViewCellAccessoryNone;
	cell.editingAccessoryType = UITableViewCellAccessoryNone;
	cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
	// if no cells, display 'press to add' message
	if (backgroundList.count == 0 && !self.editing) {
		cell.textLabel.text = @"Press Edit Button To Create New Background";
		cell.textLabel.textColor = [UIColor lightGrayColor];
		cell.textLabel.adjustsFontSizeToFitWidth = YES;
	}
	else {
		// Set up the cell...
		NSUInteger row = [indexPath row];
		if (row < backgroundList.count) {
			cell.nameLabel.text = [backgroundList objectAtIndex:row];
//			Background *background = [[Background alloc] initFromArchive:[backgroundList objectAtIndex:row]];
            Background *background = [appDelegate getBackgroundUsingName:[backgroundList objectAtIndex:row]];
			cell.backgroundImageView.image = background.image;
			cell.editingAccessoryType = UITableViewCellAccessoryDisclosureIndicator;
			cell.selectionStyle = UITableViewCellSelectionStyleBlue;
		}
		else {
			cell.textLabel.text = @"Add New";
			cell.textLabel.textColor = [UIColor lightGrayColor];
			cell.selectionStyle = UITableViewCellSelectionStyleBlue;
		}
	}
	
    return cell;
}

//editing mode changed
- (void)setEditing:(BOOL)editing animated:(BOOL)animated {
	// add/remove placeholder items
	NSMutableArray *indexPaths = [NSMutableArray array];
	[indexPaths addObject:[NSIndexPath indexPathForRow:backgroundList.count inSection:0]];
	[self.tableView beginUpdates];
	[super setEditing:editing animated:animated];
	[self.tableView setEditing:editing animated:YES];
	if (editing) {
		// delete 'press edit' message
		if (backgroundList.count == 0)
			[self.tableView deleteRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationTop];
		
		// Show the placeholder rows and hide back button
		[self.tableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationTop];
		self.navigationItem.hidesBackButton = YES;
	}
	else
	{
		// Hide the placeholder rows and show back button again
		[self.tableView deleteRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationTop];
		self.navigationItem.hidesBackButton = NO;
		
		// show 'press edit' message
		if (backgroundList.count == 0)
			[self.tableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationTop];
	}
	[self.tableView endUpdates];
}

-(UITableViewCellEditingStyle)tableView:(UITableView*)tableView editingStyleForRowAtIndexPath:(NSIndexPath*)indexPath {
	// which row are we getting
	NSUInteger row = [indexPath row];
	
	// if we are in edit mode
	if (self.editing) {
		// show delete style and insert for 'add new'
		if (row != backgroundList.count)
			return UITableViewCellEditingStyleDelete;
		else
			return UITableViewCellEditingStyleInsert;
	}
	return UITableViewCellEditingStyleNone;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    // if we are deleting rows
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
		[Background	delete:[backgroundList objectAtIndex:[indexPath row]] extension:@".background"];
		[backgroundList removeObjectAtIndex:[indexPath row]];
        [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:YES];
    }   
    else
		if (editingStyle == UITableViewCellEditingStyleInsert) {
			// edit background using edit controller
			oldBackgroundName = nil;
			EditBackgroundController *editBackgroundController = [[EditBackgroundController alloc] initWithNibName:@"EditBackgroundController" bundle:nil];
			Background *newBackground = [[Background alloc] init];
			[editBackgroundController setEditableObject:newBackground isNew:YES];
			editBackgroundController.editableListDelegate = self;
			UINavigationController *secondNavigationController = [[UINavigationController alloc] initWithRootViewController:editBackgroundController];
			[self.navigationController presentModalViewController:secondNavigationController animated:YES];
			[newBackground release];
			[editBackgroundController release];
			[secondNavigationController release];
		}
}

-(void)didUpdateObject:(id)updatedObject {
	// if renamed, then must rename file too
	Background *updatedBackground = (Background*)updatedObject;
	if (![oldBackgroundName isEqualToString:[updatedBackground backgroundName]])
		[updatedBackground renameFrom:oldBackgroundName to:[updatedBackground backgroundName]];
	
	// update class to disk and refresh table
	[updatedBackground saveAs:[updatedBackground backgroundName]];
	[self populateBackgroundList];
	[self.tableView reloadData];
}

-(void)didAddNewObject:(id)newObject {
	// save new class
	Background *newBackground = (Background*)newObject;
	[newObject saveAs:[newBackground backgroundName]];
	[self populateBackgroundList];
	[self.tableView reloadData];
}

#pragma mark -
#pragma mark Table view delegate

-(void)tableView:(UITableView*)tableView didSelectRowAtIndexPath:(NSIndexPath*) indexPath {
	// get application delegate
    McpAppDelegate *appDelegate = (McpAppDelegate*)[[UIApplication sharedApplication] delegate];
    
	// find row
	NSUInteger row = [indexPath row];
	
	// save index path
    [lastIndexPath release];
	lastIndexPath = [indexPath retain];
	
	// editing mode?
	if (self.editing) {
		// add new?
		if (row >= [backgroundList count]) {
			[self tableView:tableView commitEditingStyle:UITableViewCellEditingStyleInsert forRowAtIndexPath:indexPath];
		}
		else {
			// edit background using edit controller
			oldBackgroundName = [backgroundList objectAtIndex:row];
			EditBackgroundController *editBackgroundController = [[EditBackgroundController alloc] initWithNibName:@"EditBackgroundController" bundle:nil];
//			Background *thisBackground = [[Background alloc] initFromArchive:[backgroundList objectAtIndex:row]];
            Background *thisBackground = [appDelegate getBackgroundUsingName:[backgroundList objectAtIndex:row]];
			[editBackgroundController setEditableObject:thisBackground isNew:NO];
			editBackgroundController.editableListDelegate = self;
			[self.navigationController pushViewController:editBackgroundController animated:YES];
//			[thisBackground release];
			[editBackgroundController release];
		}
	}
	else {
	}
}

#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    // Relinquish ownership of anything that can be recreated in viewDidLoad or on demand.
    // For example: self.myOutlet = nil;
}


- (void)dealloc {
    [lastIndexPath release];
	[backgroundList release];
    [super dealloc];
}


@end

