//
//  ActorVoiceCell.m
//  Mcp
//
//  Created by Ray on 10/21/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "ActorVoiceCell.h"


@implementation ActorVoiceCell

@synthesize voiceNameSegmentControl;
@synthesize testButton;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if ((self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])) {
        // Initialization code
    }
    return self;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {

    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)loadVoiceList {
	// remove all segments
	[voiceNameSegmentControl removeAllSegments];
	
	// load japanese voices
	[voiceNameSegmentControl insertSegmentWithTitle:@"Misaki" atIndex:0 animated:NO];
	[voiceNameSegmentControl insertSegmentWithTitle:@"Shou" atIndex:1 animated:NO];
}

-(BOOL)setVoiceAs:(NSString*)inVoiceName {
	BOOL foundIt = NO;
	for (int i=0; i < [voiceNameSegmentControl numberOfSegments]; i++) {
		if ([[voiceNameSegmentControl titleForSegmentAtIndex:i] isEqualToString:inVoiceName])
		{
			voiceNameSegmentControl.selectedSegmentIndex = i;
			foundIt = YES;
			break;
		}
	}
	return foundIt;
}

- (void)dealloc {
	[voiceNameSegmentControl release];
	[testButton release];
    [super dealloc];
}


@end
