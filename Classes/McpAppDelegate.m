//
//  McpAppDelegate.m
//  Mcp
//
//  Created by Ray on 10/14/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#include "AudioToolbox/AudioQueue.h"
#include "AudioToolbox/AudioToolbox.h"
#import "Accelerate/Accelerate.h"
#include "vt_jpn_misaki.h"
#include "vt_jpn_show.h"
#include "vt_eng_paul.h"
#include "vt_eng_julie.h"
#import <AVFoundation/AVFoundation.h>
#import "McpAppDelegate.h"
#import "RootViewController.h"
#import "AppOptions.h"
#import "TextFunctions.h"
#import "VoiceSample.h"
#import "Phoneme.h"
#import "TrainingWord.h"
#import "iSpeechSDK.h"
#import "Book.h"
#import "GrammarNote.h"
#import "GrammarDictionaryEntry.h"
#import "Conversation.h"
#import "GrammarConversation.h"
#import "Character.h"
#import "GrammarDialogueLine.h"
#import "HtmlFunctions.h"
#import "TextFunctions.h"
#import "McpAppOptions.h"

@implementation McpAppDelegate

@synthesize window = _window;

@synthesize localOptions;
@synthesize appOptions;
@synthesize oldAppOptions;
//@synthesize window;
@synthesize navigationController;
@synthesize trainingData;
@synthesize currentUserProfile;
@synthesize currentCpuProfile;
//@synthesize mecab;
//@synthesize mecabExceptions;
@synthesize romajiPath;
@synthesize availableAnimeNames;
@synthesize phonemes;
@synthesize trainingWords;
@synthesize isOptionsLoaded;
@synthesize isOldOptionsLoaded;
@synthesize isLocalOptionsLoaded;

#pragma mark -
#pragma mark Application lifecycle

-(McpAppOptions*)getAppOptions {
    return appOptions;
}

-(McpLocalOptions*)getLocalOptions {
    return localOptions;
}

+(NSString *)getUuid {
    CFUUIDRef uuid = CFUUIDCreate(NULL);
    CFStringRef uuidStr = CFUUIDCreateString(NULL, uuid);
    CFRelease(uuid);
    [(NSString *)uuidStr autorelease];
    return (NSString *)uuidStr;
}

-(float*)getMelMatrixForSampleRate:(int)inSampleRate {
    if (inSampleRate == 8000)
        return melMatrix8k;
    else
        return melMatrix16k;
}

-(float*)getMelWindowForSampleRate:(int)inSampleRate {
    if (inSampleRate == 8000)
        return melWindow8k;
    else
        return melWindow16k;
}

-(FFTSetup)getFftSetup {
    return setupReal;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // initialize ispeech
//    iSpeechSDK *sdk = [iSpeechSDK sharedSDK];
//    sdk.APIKey = @"60c53346a7b725e18a958f4b52dbd3f9";
    
    // set notification callback for options
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(localOptionsLoaded:) name:@"Local Options Loaded" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appOptionsLoaded:) name:@"AppOptions.plist Options Loaded" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(oldAppOptionsLoaded:) name:@"McpAppOptions.plist Options Loaded" object:nil];
    
    // first load local options - check for cloud uses this!
    localOptions = [[McpLocalOptions alloc] init];
    
    //
    // do any initialization that doesn't rely on app or local options
    //
    
    // set up fft parameters
//    UInt32 log2n = [VoiceSample getLog2N];
//    setupReal = vDSP_create_fftsetup(log2n, FFT_RADIX2);
    UInt32 log2n = log2f(512);
//    UInt32 log2n = log2f(8192);
    setupReal = vDSP_create_fftsetup(log2n, FFT_RADIX2);
    
    // pre-calc mel matrix
    UInt32 sampleWindow = [VoiceSample getN];
    melMatrix8k = [self buildFftMatrixWithSamplingRate:8000 windowLength:512 channels:64];
    melMatrix16k = [self buildFftMatrixWithSamplingRate:16000 windowLength:512 channels:64];
    melWindow8k = [self buildFftWindowSizeArrayWithSamplingRate:8000 windowLength:512 channels:64];
    melWindow16k = [self buildFftWindowSizeArrayWithSamplingRate:16000 windowLength:512 channels:64];
    
    
    
    /*
     ********************************************************************************
     * convert books to new grammar format
     ********************************************************************************
    
    
    
    // get docs dir
    NSArray *paths2 = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory2 = [paths2 objectAtIndex:0]; // Get documents directory

    
    // try and open a book
    Book *testBook = [[Book alloc] initFromArchive:@"Genki I"];
    
    // loop through chapters
    int grammarCount = 0;
    for (int c=0; c < testBook.chapters.count; c++) {
        // get chapter
        Chapter *thisChapter = [testBook.chapters objectAtIndex:c];
        
        // loop through grammars
        for (int g=0; g < thisChapter.grammarNotes.count; g++) {
            // get grammar note
            GrammarNote *thisNote = [thisChapter.grammarNotes objectAtIndex:g];
            NSString *htmlText = [NSString stringWithFormat:@"<!DOCTYPE html>\r\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">\r\n<html>\r\n%@\r\n</html>", thisNote.text];
            
            // convert grammar notes into HTML files
            NSError *error;
//            BOOL succeed = [htmlText writeToFile:[documentsDirectory2 stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.html", thisNote.title]] atomically:YES encoding:NSUTF8StringEncoding error:&error];
            
    
            // create grammar data file
            GrammarDictionaryEntry *newGrammar = [[GrammarDictionaryEntry alloc] init];
            newGrammar.language = @"Japanese";
            newGrammar.nativeLanguage = @"English";
            newGrammar.entryName = thisNote.title;
            newGrammar.nativeTitle = thisNote.title;
            if ([testBook.bookName isEqualToString:@"Genki I"])
                newGrammar.levelName = @"Beginner";
            else
                newGrammar.levelName = @"Low Intermediate";
            newGrammar.grammarId = ++grammarCount;
            newGrammar.indexOrder = newGrammar.grammarId;
            newGrammar.htmlNotesFilename = [NSString stringWithFormat:@"%@.html", thisNote.title];
            
            // create lesson
            GrammarLesson *newLesson = [[GrammarLesson alloc] init];
            
            // then loop though conversations
            for (int o=0; o < thisChapter.conversations.count; o++) {
                // get this conversation
                Conversation *thisConv = [thisChapter.conversations objectAtIndex:o];
    
                // if conversation uses this grammar
                BOOL usesGrammar = NO;
                BOOL usesCharacterIndex = -1;
                for (int l=0; l < thisConv.dialogueLines.count; l++) {
                    DialogueLine *thisLine = [thisConv.dialogueLines objectAtIndex:l];
                    NSString *linkText = [NSString stringWithFormat:@"<a href=\"file://%@\">", [newGrammar.entryName stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
                    if ([thisLine.line rangeOfString:linkText].location != NSNotFound) {
                        usesGrammar = YES;
                        usesCharacterIndex = thisLine.characterIndex;
                        break;
                    }
                }
                
                // does conversation use this grammar
                if (usesGrammar) {
                    // create new conversation
                    GrammarConversation *newConv = [[GrammarConversation alloc] initWithId:o+1];
                    
                    // add characters
                    for (int c=0; c < thisConv.characters.count; c++) {
                        // get character
                        Character *thisCharacter = [thisConv.characters objectAtIndex:c];
                        
                        // add new
                        GrammarCharacter *newCharacter = [[GrammarCharacter alloc] initWithCharacterName:thisCharacter.characterName andActorName:thisCharacter.actorName isUserPart:c==usesCharacterIndex];
                        [newConv.characters addObject:newCharacter];
                        [newCharacter release];
                    }
                    
                    // add lines
                    for (int l=0; l < thisConv.dialogueLines.count; l++) {
                        // get dialog line
                        DialogueLine *thisLine = [thisConv.dialogueLines objectAtIndex:l];
                        
                        // add new
                        GrammarDialogueLine *newLine = [[GrammarDialogueLine alloc] initWithLineString:[HtmlFunctions stripHtmlTagsFromHtml:thisLine.line] language:@"Japanese" nativeLanguage:@"English" characterIndex:thisLine.characterIndex];
                        newLine.nativeLine = newLine.line;
                        [newConv.dialogueLines addObject:newLine];
                        [newLine release];
                    }
                    
                    // add conversation to grammar object
                    [newLesson.conversations addObject:newConv];
                    [newConv release];
                }
            }
            
            // add lesson
            newGrammar.lesson = newLesson;
            [newLesson release];
            
            // save grammar data file
            [newGrammar saveToXmlFilename:[documentsDirectory2 stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.grammar", newGrammar.entryName]]];
            [newGrammar release];
        }
    }
    
    */
    
    
    
    
    
    
    
    
    // create mecab object for converting kanji to kana
//    mecab = [[Mecab alloc] init];
    
    // load romaji path data for conversion to kana
    romajiPath = [[RomajiPath alloc] initFromPlist];
    
    // create file cache
    fileCache = [[DataFileCache alloc] init];
	
	// create new training data, later load from profile
	trainingData = [[TrainingData alloc] init];
    
    // create default profile if it doesn't exit
    if (![UserProfile fileExists:@"Default" withExtension:@".profile"]) {
        currentUserProfile = [[UserProfile alloc] initWithName:@"Default"];
        [currentUserProfile saveAs:@"Default"];
        [currentUserProfile release];
    }
    
    // initialize audio session
//    [[AVAudioSession sharedInstance] setActive:YES];
    OSStatus status = AudioSessionInitialize(NULL, NULL, NULL, self);
    
	// set for play and record
	[[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayAndRecord error:nil];
	
    // allow bluetooth input
    UInt32 allowBluetoothInput = 1;
    status = AudioSessionSetProperty(kAudioSessionProperty_OverrideCategoryEnableBluetoothInput, sizeof(allowBluetoothInput), &allowBluetoothInput);

    UInt32 rSize = sizeof(CFStringRef);
    CFStringRef route;
    status = AudioSessionGetProperty(kAudioSessionProperty_AudioRoute, &rSize, &route);
    NSLog(@"%@\r\n", route);
    CFRelease(route);
    
    // override to bluetooth
//    AudioSessionPropertyID routeOverride = kAudioSessionOverrideAudioRoute_Speaker;
//    OSStatus status3 = AudioSessionSetProperty(kAudioSessionProperty_OverrideAudioRoute, sizeof(routeOverride), &routeOverride);
    
	// re-route audio to speaker instead of earpiece
	UInt32 doChangeDefaultRoute=1;
	AudioSessionSetProperty(kAudioSessionProperty_OverrideCategoryDefaultToSpeaker, sizeof(doChangeDefaultRoute), &doChangeDefaultRoute);
	
	// initialize speech engine
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
    /*
	NSString *dbPath = [documentsDirectory substringToIndex:[documentsDirectory length]-9];
#ifdef TARGET_FREE
	dbPath = [dbPath stringByAppendingString:@"HFS-Free.app"];
#else
	dbPath = [dbPath stringByAppendingString:@"HF-Sensei.app"];
#endif
	NSString *enginePath = [dbPath stringByAppendingString:@"/"];
     */
#ifdef TARGET_SIMULATOR
//	NSString *licencePath = [enginePath stringByAppendingString:@"prod_verification.txt"];
    NSString *licencePath = [[NSBundle mainBundle] pathForResource:@"prod_verification" ofType:@"txt"];
#else
//	NSString *licencePath = [enginePath stringByAppendingString:@"prod_verification.txt"];
    NSString *licencePath = [[NSBundle mainBundle] pathForResource:@"prod_verification" ofType:@"txt"];
#endif
#ifdef TARGET_SIMULATOR
//	NSString *licencePath2 = [enginePath stringByAppendingString:@"eng_prod_verification.txt"];
    NSString *licencePath2 = [[NSBundle mainBundle] pathForResource:@"eng_prod_verification" ofType:@"txt"];
#else
//	NSString *licencePath2 = [enginePath stringByAppendingString:@"eng_prod_verification.txt"];
    NSString *licencePath2 = [[NSBundle mainBundle] pathForResource:@"eng_prod_verification" ofType:@"txt"];
#endif
    
    NSString *dbPath = [[NSBundle mainBundle] pathForResource:@"tts_single_db_julie" ofType:@"vtdb"];
    dbPath = [dbPath stringByDeletingLastPathComponent];
    
	char *cDbPath = (char*)[dbPath UTF8String];
	char *cLicensePath = (char*)[licencePath UTF8String];
	char *cLicensePath2 = (char*)[licencePath2 UTF8String];
    
    
    
	// initialize misaki voice
	int result = VT_LOADTTS_JPN_Misaki(0, 2, cDbPath, cLicensePath);
	if (result != 0) {
		NSException *exception = [NSException exceptionWithName:@"DatabaseError" reason:@"Cannot open speech database" userInfo:nil];
		@throw exception;
	}
    
    // initialize show voice
	result = VT_LOADTTS_JPN_Show(0, 1, cDbPath, cLicensePath);
	if (result != 0) {
		NSException *exception = [NSException exceptionWithName:@"DatabaseError" reason:@"Cannot open speech database" userInfo:nil];
		@throw exception;
	}

    // initialize paul english voice
	result = VT_LOADTTS_ENG_Paul(0, 1, cDbPath, cLicensePath2);
	if (result != 0) {
		NSException *exception = [NSException exceptionWithName:@"DatabaseError" reason:@"Cannot open speech database" userInfo:nil];
		@throw exception;
	}
    
    // intiailize julie english voice
	result = VT_LOADTTS_ENG_Julie(0, 3, cDbPath, cLicensePath2);
	if (result != 0) {
		NSException *exception = [NSException exceptionWithName:@"DatabaseError" reason:@"Cannot open speech database" userInfo:nil];
		@throw exception;
	}

	// create/open sql database
	NSString *bundleDb = [[NSBundle mainBundle] pathForResource:@"jmw.sqlite3" ofType:@""];
	if (sqlite3_open([bundleDb UTF8String], &jmwDb) != SQLITE_OK) {
		NSException *exception = [NSException exceptionWithName:@"DatabaseError" reason:@"Cannot open dictionary database" userInfo:nil];
		@throw exception;
	}
	
	// set cache
	if (sqlite3_exec(jmwDb, "PRAGMA CACHE_SIZE=1024;", NULL, NULL, NULL) != SQLITE_OK) {
		char *errMessage;
		errMessage = (char*)sqlite3_errmsg(jmwDb);
		NSAssert1(0, @"Error: failed to set cache size with message '%s'.", errMessage);
	}
    
    // create list of available anime models
    availableAnimeNames = [[NSArray arrayWithObjects:@"Rachel", @"Carl", @"Alan", @"Alison", @"Emiko", @"Jamaal", @"Jimmy", @"SheeAnn", @"Steve", @"Ted", nil] retain];
    
    
    /*
    // load actor frames
    NSString *phonemePath = [[NSBundle mainBundle] pathForResource:@"PhonemeSync.plist" ofType:nil];
    phonemeSyncList = [[NSDictionary alloc] initWithContentsOfFile:phonemePath];
    */
    
    // load phonemes
    phonemes = [[NSMutableDictionary alloc] init];
    NSString *phonemePath = [[NSBundle mainBundle] pathForResource:@"Phonemes.plist" ofType:nil];
    NSDictionary *importPhonemes = [[NSDictionary alloc] initWithContentsOfFile:phonemePath];
    NSEnumerator *phonemeEnum = [importPhonemes keyEnumerator];
    id key;
    while (key = [phonemeEnum nextObject]) {
        Phoneme *newPhoneme = [[Phoneme alloc] initWithName:key andDictionaryEntry:[importPhonemes objectForKey:key]];
        [phonemes setObject:newPhoneme forKey:key];
        [newPhoneme release];
    }
    [importPhonemes release];
    
    // load training words
    trainingWords = [[NSMutableDictionary alloc] init];
    NSString *twPath = [[NSBundle mainBundle] pathForResource:@"TrainingWords.plist" ofType:nil];
    NSArray *twArray = [[NSArray alloc] initWithContentsOfFile:twPath];
    for (int i=0; i < twArray.count; i++) {
        NSDictionary *twEntry = [twArray objectAtIndex:i];
        TrainingWord *newWord = [[TrainingWord alloc] initWithDictionaryEntry:twEntry];
        [trainingWords setObject:newWord forKey:[twEntry objectForKey:@"WordKana"]];
        [newWord release];
    }
    [twArray release];
    
    // don't need this for storyboard
//    // Add the navigation controller's view to the window and display.
//    [window addSubview:navigationController.view];
//    [window makeKeyAndVisible];

    return YES;
}

// stuff dependent on local options
-(void)localOptionsLoaded:(NSNotification *) notification {
    // check for cloud access
    NSURL *ubiq = [[NSFileManager defaultManager]
                   URLForUbiquityContainerIdentifier:nil];
    if (ubiq) {
        NSLog(@"iCloud access at %@", ubiq);
        
        // if cloud is now found, show message to user
        if (!localOptions.isUsingCloud) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"iCloud Enabled" message:@"iCloud has become available.  Switching over to using iCloud.  If you would prefer to work locally, please go to control panel and disable iCloud for this device." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            [alert release];
        }
        
        // soon as cloud is found, consider we are using
        localOptions.isUsingCloud = YES;
        [localOptions save];
        
        // load app options and check for changes
        appOptions = [[McpAppOptions alloc] initWithFilename:@"AppOptions.plist" andLocalOptions:localOptions];
        [appOptions checkForCloudChanges]; // this sets up the query to get back the options
    } else {
        NSLog(@"No iCloud access");
        // if we were previously using cloud, error and exit
        if ([McpLocalOptions getSingleton].isUsingCloud) {
            // show error and give option to restart locally or exit
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Fatal Error" message:@"iCloud is not available.  Please try again later or, if you turned it off on purpose, you can choose to work local.  If problem persists, please contact support@rakudasoft.com." delegate:nil cancelButtonTitle:@"Understood" otherButtonTitles:@"Work Local", nil];
            alert.delegate = self;
            [alert show];
            [alert release];
        }
        else {
            // otherwise, just show warning that cloud is good :S
            if (![McpLocalOptions getSingleton].cloudWarningShown) {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No iCloud" message:@"iCloud is not available.  Consider enabling iCloud if you want to synchronize your progress between devices." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert show];
                [alert release];
                
                // set error shown in options
                [McpLocalOptions getSingleton].cloudWarningShown = YES;
                [[McpLocalOptions getSingleton] save];
            }
        }
        
        // load local options
        appOptions = [[McpAppOptions alloc] initWithFilename:@"AppOptions.plist" andLocalOptions:localOptions];
    }
    
    // local options are now loaded
    isLocalOptionsLoaded = YES;
    
    /*
    // load old app options
    oldAppOptions = [[McpAppOptions alloc] initWithFilename:@"McpAppOptions.plist" andLocalOptions:localOptions];
    [oldAppOptions checkForCloudChanges]; // this sets up the query to get back the options
    */
    
    // load current profile
    currentUserProfile = [[UserProfile alloc] initFromArchive:localOptions.currentProfileName];
    
    // create cpu profile, but don't save
    currentCpuProfile = [[UserProfile alloc] initWithName:@"Cpu"];
	
    // if version has changed
	// if database version has changed
	if (![localOptions.lastVersionNumber isEqualToString:[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"]]) {
        // copy over any bundled books/actors/backgrounds
        [self copyBundledDocuments];
        
        // copy decks manually because they change
#ifdef TARGET_FREE
        [self copyBundledDocumentOfName:@"Sample JLPT5 Vocab.flashDeck"];
#else
        [self copyBundledDocumentsOfType:@"flashDeck"];
#endif
        // update last version number
        localOptions.lastVersionNumber = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
        [localOptions save];
    }
    
    NSLog(@"Finished loading local options");
    
    // remove observer
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"Local Options Loaded" object:nil];
}


-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    // if we cancelled, pop controler
    if ([alertView.title isEqualToString:@"Fatal Error"]) {
        // exit with alert message
        [NSException raise:alertView.title format:@"%@", alertView.message];
    }
    
    // work local?
    if ([[alertView buttonTitleAtIndex:buttonIndex] isEqualToString:@"Work Local"]) {
        // now working local
        [McpLocalOptions getSingleton].isUsingCloud = NO;
        [McpLocalOptions getSingleton].cloudWarningShown = YES;
        [[McpLocalOptions getSingleton] save];
        
        // load app options locally
        appOptions = [[McpAppOptions alloc] initWithFilename:@"AppOptions.plist" andLocalOptions:localOptions];
    }
    
    // create new icloud document?
    if ([[alertView buttonTitleAtIndex:buttonIndex] isEqualToString:@"Create New"]) {
        // create new progress
        // don't check for changes straight away, as save operates in background
        [appOptions createNewCloudDocument];
    }
    
    // try again
    if ([[alertView buttonTitleAtIndex:buttonIndex] isEqualToString:@"Try Again"]) {
        // load app options and check for changes
        appOptions = [[McpAppOptions alloc] initWithFilename:@"AppOptions.plist" andLocalOptions:localOptions];
        [appOptions checkForCloudChanges];
    }
}

// stuff dependent on app optiokns
-(void)appOptionsLoaded:(NSNotification *) notification {
    // check for cloud access
    NSURL *ubiq = [[NSFileManager defaultManager]
                   URLForUbiquityContainerIdentifier:nil];
    
    // did they load sucessfully?
    AppOptions *theseOptions = (AppOptions*)notification.object;
    if (!theseOptions.loadedOk) {
        // give user option to create new progress
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"iCloud Error" message:@"iCloud progress failed to load.  If this is your first time using the cloud, select 'Create New', otherwise select 'Try Again'.  If error persists, please contact support@rakudasoft.com." delegate:nil cancelButtonTitle:@"Try Later" otherButtonTitles:@"Create New", @"Try Again", nil];
        alert.delegate = self;
        [alert show];
        [alert release];
        
        return;
    }
    
    // if cloud is available and it's not kana practice, copy up local decks to the cloud
    NSString *productName = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleName"] ;
    if (ubiq != nil && ![productName isEqualToString:@"MJCP-Free"]) {
        // enumerate files with extension
        NSFileManager *fileManager = [NSFileManager defaultManager];
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSArray *files = [fileManager contentsOfDirectoryAtPath:documentsDirectory error:nil];
        BOOL firstDeck = YES;
        for (int i=0; i < files.count; i++)
            if ([[[files objectAtIndex:i] pathExtension] isEqualToString:@"flashDeck"]) {
                // if first deck, warn user
                if (firstDeck) {
                    // show alert
                    UIAlertView *myAlert = [[UIAlertView alloc] initWithTitle:@"Found Local Flash Decks" message:@"Copying local flash decks up to the cloud so they can be shared between your devices and Rakudasoft apps." delegate:self cancelButtonTitle:@"Understood" otherButtonTitles:nil];
                    [myAlert show];
                    [myAlert release];
                    firstDeck = NO;
                }
                
                // copy to cloud
                NSString *thisFile = [files objectAtIndex:i];
                NSURL *sourceURL = [NSURL fileURLWithPathComponents:[NSArray arrayWithObjects:documentsDirectory, thisFile, nil]];
                NSURL *destURL = [ubiq URLByAppendingPathComponent:[@"Documents" stringByAppendingPathComponent:thisFile]];
                
                // if this is a bundled document, delete the cloud version first so
                // we don't get error that it already exists
                if ([self isBundledDocumentWithName:thisFile andType:nil])
                    [[NSFileManager defaultManager] removeItemAtURL:destURL error:nil];
                
                NSError *thisError;
                if (![[NSFileManager defaultManager] setUbiquitous:YES itemAtURL:sourceURL destinationURL:destURL error:&thisError]) {
                    // show warning
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"iCloud Conflict" message:[NSString stringWithFormat:@"Error moving flash deck %@ to the cloud.  Please delete the local document using iTunes or if you want to replace the cloud document with this one, delete the cloud document using iCloud in your phone's settings.", thisFile] delegate:nil cancelButtonTitle:@"Understood" otherButtonTitles:nil];
                    [alert show];
                    [alert release];
                }
                
            }
    }
    
    // mark as loaded
    isOptionsLoaded = YES;
    
    // if both old and new are loaded, then we can now merge!
    if (isOldOptionsLoaded && isOptionsLoaded)
        [self mergeOldOptions];
    
    // remove observer
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"AppOptions.plist Options Loaded" object:nil];
}

-(void)oldAppOptionsLoaded:(NSNotification *) notification {
    // do one-off update to version 2
    if ([oldAppOptions updateToVersion2Options])
        [oldAppOptions save];
    
    // mark as loaded
    isOldOptionsLoaded = YES;
    
    // if both old and new are loaded, then we can now merge!
    if (isOldOptionsLoaded && isOptionsLoaded)
        [self mergeOldOptions];
    
    // remove observer
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"JmwAppOptions.plist Options Loaded" object:nil];
}

-(void)mergeOldOptions {
    // if not merged, merge and show message box
    if (!localOptions.isOldOptionsMerged) {
        // merge old jmwappoptions into new
        // use existing merge routine
        [appOptions mergeChangesFromOptions:[oldAppOptions getOptionsDictionary] toOptions:[appOptions getOptionsDictionary]];
        [appOptions save];
        
        // mark as merged
        localOptions.isOldOptionsMerged = YES;
        [localOptions save];
    }
}

-(BOOL)isBundledDocumentWithName:(NSString*)inName andType:(NSString*)inType {
    // see if this file is in the bundle
    if ([[NSBundle mainBundle] pathForResource:inName ofType:inType] != nil)
        return YES;
    else
        return NO;
}

-(void)copyBundledDocuments {
    // books and actors only
    [self copyBundledDocumentsOfType:@"actor"];
//    [self copyBundledDocumentsOfType:@"background"];
    [self copyBundledDocumentsOfType:@"book"];
    
}

-(void)copyBundledDocumentsOfType:(NSString*)inType {
    // copy files from bundle to documents
	NSFileManager *fileManager = [NSFileManager defaultManager];
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
    NSArray *actorPaths = [[NSBundle mainBundle] pathsForResourcesOfType:inType inDirectory:nil];
    for (int i=0; i < actorPaths.count; i++) {
        NSString *bundlePath = [actorPaths objectAtIndex:i];
        NSString *docsPath = [documentsDirectory stringByAppendingPathComponent:[bundlePath lastPathComponent]];
        [fileManager removeItemAtPath:docsPath error:nil];
        [fileManager copyItemAtPath:bundlePath toPath:docsPath error:nil];
    }
}

-(void)copyBundledDocumentOfName:(NSString*)inName {
    // copy individual file
	// get documents directory
    NSFileManager *fileManager = [NSFileManager defaultManager];
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];

    // copy file
    NSString *bundlePath = [[NSBundle mainBundle] pathForResource:inName ofType:nil];
    NSString *docsPath = [documentsDirectory stringByAppendingPathComponent:[bundlePath lastPathComponent]];
    [fileManager removeItemAtPath:docsPath error:nil];
    [fileManager copyItemAtPath:bundlePath toPath:docsPath error:nil];
}

-(NSString*)getDictionaryHtmlForWord:(NSString*)inWord {
    NSMutableString *myHtml = [NSMutableString stringWithFormat:@"<p><h1>%@", inWord];
    
    // get as utf string
    NSString *utfWord = [inWord stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    // if no kanji, search using kana field
    NSString *searchField = @"";
    if ([TextFunctions containsKanji:utfWord])
        searchField = @"kanji_word";
    else
        searchField = @"kana_word";
    
	// get word from database
	NSString *query = [[NSString alloc] initWithFormat:@"select kana_word, kanji_word, word_type_name, word_subtype_name, custom_category_name, kana_index, dictionary_id from dictionary join word_type on word_type.word_type_id=dictionary.word_type_id join word_subtype on word_subtype.word_subtype_id=dictionary.word_subtype_id join custom_category on custom_category.custom_category_id=dictionary.custom_category_id where %@='%@'",searchField, utfWord];
	sqlite3_stmt *statement;
	if (sqlite3_prepare_v2(jmwDb, [query UTF8String], -1, &statement, nil) != SQLITE_OK) {
        // append not found message
        [myHtml appendString:@"<h2>Dictionary entry not found.</h2>"];
        
        char *errMessage;
        errMessage = (char*)sqlite3_errmsg(jmwDb);
        
		sqlite3_finalize(statement);
		[query release];
		return nil;
	}
	
	// build a word object
	int kanji_key = 0;
	if (sqlite3_step(statement) == SQLITE_ROW) {
		// create new word
		NSString *wordType = [NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 2)];
		NSString *wordSubType = [NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 3)];
		kanji_key = sqlite3_column_int(statement, 6);
        [myHtml appendFormat:@" (%@/%@)</h1>", wordType, wordSubType];
	}
	else {
        // append not found message
        [myHtml appendString:@"<h2>Dictionary entry not found.</h2>"];
	}
	
	// free statement data
	sqlite3_finalize(statement);
	[query release];
    
    // loop through meanings
	// get senses
	NSString *query2 = [[NSString alloc] initWithFormat:@"select s.sense_index, notes, meaning from dictionary_meaning dm, dictionary_meaning_fts dmfts, dictionary_sense s where s.dictionary_id=dm.dictionary_id and s.sense_index=dm.sense_index and dmfts.rowid=dm.rowid and dm.dictionary_id=%d order by dm.sense_index, meaning_id", kanji_key];
	sqlite3_stmt *statement2;
	if (sqlite3_prepare_v2(jmwDb, [query2 UTF8String], -1, &statement2, nil) != SQLITE_OK) {
		char *errMessage;
		errMessage = (char*)sqlite3_errmsg(jmwDb);
	}
	else {
		// loop through senses
        //		WordSense *newSense;
        //		int currentIndex=0;
        int lastSenseIndex = 0;
		while (sqlite3_step(statement2) == SQLITE_ROW) {
			// get next meaning
            int senseIndex = sqlite3_column_int(statement2, 0);
			char *notes = (char*)sqlite3_column_text(statement2, 1);
			char *meaning = (char*)sqlite3_column_text(statement2, 2);
            NSString *myNotes = [NSString stringWithUTF8String:notes];
            NSString *myMeaning = [NSString stringWithUTF8String:meaning];
            
            // start new header if new sense
            if (senseIndex != lastSenseIndex) {
                if (lastSenseIndex == 0) {
                    if (myNotes != nil && ![myNotes isEqualToString:@""])
                        [myHtml appendFormat:@"<h2><i>%@</i></h2>", myNotes];
                    [myHtml appendFormat:@"<h2>%d. ", senseIndex];
                }
                else
                    [myHtml appendFormat:@".</h2><h2>%d. ", senseIndex];
                lastSenseIndex = senseIndex;
            }
            else
                [myHtml appendString:@", "];
            
            // append word
            [myHtml appendFormat:@"%@", myMeaning];
		}
        if (lastSenseIndex > 0)
            [myHtml appendString:@".</h2>"];
	}
	
	// free statement data
	sqlite3_finalize(statement2);
	[query2 release];
    
    // loop though top 3 sample sentences (if found)
    
    // append final html close
    [myHtml appendString:@"</p>"];
    
    return myHtml;
}

-(Actor*)getActorUsingName:(NSString*)inName {
    return (Actor*)[fileCache getDataFileUsingName:inName andExtension:@"actor" andClassName:@"Actor"];
}

-(Background*)getBackgroundUsingName:(NSString*)inName {
    return (Background*)[fileCache getDataFileUsingName:inName andExtension:@"background" andClassName:@"Background"];
}

-(float)freq2mel:(float)inFreq {
	return 2595.0f*log10(1+inFreq/700.0f);
}

-(float)mel2freq:(float)inFreq {
	return 700.0f*((pow(10, inFreq/2595.0f))-1);
}

-(float*)buildFftWindowSizeArrayWithSamplingRate:(int)fs windowLength:(int)N channels:(int)nofChannels {
	// built from matlab code by plannerer
	// compute resolution etc.
	float df = fs/N; // frequency increment on linear scale
	int Nmax = N/2; // maximum fft index
    float startFrequency = nofChannels==40?130.0f:200.0f;
    float endFrequency = nofChannels==40?6800.0f:3500.0f;
    float fmax = endFrequency-startFrequency;
    //	float fmax = fs/2; // maximum frequency
	float startMel = [self freq2mel:startFrequency]; // maximum mel frequency
	float endMel = [self freq2mel:endFrequency]; // maximum mel frequency
	float melmax = endMel-startMel; // maximum mel frequency
    //	float melmax = [self freq2mel:fmax]; // maximum mel frequency
	float melinc = melmax / ((float)nofChannels+1.0f); // frequency increment on mel scale
	
	// center frequencies on mel scale
	float *melcenters = malloc(sizeof(float)*nofChannels);
	for (int i=0; i < nofChannels; i++)
		melcenters[i] = ((float)i+1)*melinc+startMel;
	
	// center frequencies in linear scale
	float *fcenters = malloc(sizeof(float)*nofChannels);
	for (int i=0; i < nofChannels; i++) {
		fcenters[i] = [self mel2freq:melcenters[i]];
        printf("mel channel %d center is %f\r\n", i, fcenters[i]);
    }    
	
	// compute filter bandwidths (linear scale)
	float *startfreq = malloc(sizeof(float)*nofChannels);
	float *endfreq = malloc(sizeof(float)*nofChannels);
    //	float *bandwidth = malloc(sizeof(float)*nofChannels);
	for (int i=0; i < nofChannels; i++) {
		if (i == 0)
			startfreq[i] = 0.0f;
		else
			startfreq[i] = fcenters[i-1];
		if (i == nofChannels-1)
			endfreq[i] = fmax;
		else
			endfreq[i] = fcenters[i+1];
	}
    
    // build window size array
    float *windowArray = malloc(sizeof(float)*nofChannels);
	for (int i=0; i < nofChannels; i++) {
        windowArray[i] = endfreq[i]-startfreq[i];
    }
    
    // release temp arrays
    free(fcenters);
    free(melcenters);
    free(startfreq);
    free(endfreq);
    
    return windowArray;
}

-(float*)buildFftMatrixWithSamplingRate:(int)fs windowLength:(int)N channels:(int)nofChannels {
	// built from matlab code by plannerer
	// compute resolution etc.
	float df = (float)fs/(float)N; // frequency increment on linear scale
	int Nmax = N/2; // maximum fft index
//    float startFrequency = fs==16000?130.0f:200.0f;
//    float endFrequency = fs==16000?6800.0f:3500.0f;
    float startFrequency = fs==16000?0.0f:0.0f;
    float endFrequency = fs==16000?8000.0f:4000.0f;
    float fmax = endFrequency-startFrequency;
//	float fmax = fs/2; // maximum frequency
	float startMel = [self freq2mel:startFrequency]; // maximum mel frequency
	float endMel = [self freq2mel:endFrequency]; // maximum mel frequency
	float melmax = endMel-startMel; // maximum mel frequency
//	float melmax = [self freq2mel:fmax]; // maximum mel frequency
	float melinc = melmax / ((float)nofChannels+1.0f); // frequency increment on mel scale
	
	// center frequencies on mel scale
	float *melcenters = malloc(sizeof(float)*nofChannels);
	for (int i=0; i < nofChannels; i++)
		melcenters[i] = ((float)i+1)*melinc+startMel;
	
	// center frequencies in linear scale
	float *fcenters = malloc(sizeof(float)*nofChannels);
	for (int i=0; i < nofChannels; i++) {
		fcenters[i] = [self mel2freq:melcenters[i]];
        printf("mel channel %d center is %f\r\n", i, fcenters[i]);
    }    
	
	// compute filter bandwidths (linear scale)
	float *startfreq = malloc(sizeof(float)*nofChannels);
	float *endfreq = malloc(sizeof(float)*nofChannels);
    //	float *bandwidth = malloc(sizeof(float)*nofChannels);
	for (int i=0; i < nofChannels; i++) {
		if (i == 0)
			startfreq[i] = 0.0f;
		else
			startfreq[i] = fcenters[i-1];
		if (i == nofChannels-1)
			endfreq[i] = fmax;
		else
			endfreq[i] = fcenters[i+1];
	}
	
	// quantize into fft indices
	float *indexcenter = malloc(sizeof(float)*nofChannels);
	float *fftfreq = malloc(sizeof(float)*nofChannels);
	for (int i=0; i < nofChannels; i++) {
		indexcenter[i] = round(fcenters[i] / df);
		fftfreq[i] = indexcenter[i] * df;
	}
	
	// compute start indices of windows
	float *indexstart = malloc(sizeof(float)*nofChannels);
	float *indexstop = malloc(sizeof(float)*nofChannels);
	float *idxbw = malloc(sizeof(float)*nofChannels);
	float *FFTbandwidth = malloc(sizeof(float)*nofChannels);
	for (int i=0; i < nofChannels; i++) {
		if (i == 0)
			indexstart[i] = 0;
		else
			indexstart[i] = indexcenter[i-1];
		if (i == nofChannels-1)
			indexstop[i] = Nmax-1;
		else
			indexstop[i] = indexcenter[i+1];
		idxbw[i] = indexstop[i]-indexstart[i]+1;
		FFTbandwidth[i] = idxbw[i] * df;
	}
	
	// compute resulting quantization error
	float *diff = malloc(sizeof(float)*nofChannels);
	for (int i=0; i < nofChannels; i++)
		diff[i] = fcenters[i]-fftfreq[i];
	
	// compute triangle shapes filter coefficients
	float *w = malloc(sizeof(float)*nofChannels*Nmax);
	for (int i=0; i < nofChannels*Nmax; i++)
		w[i] = 0.0f;
	for (int c=0; c < nofChannels; c++) {
        if (indexcenter[c] - indexstart[c] <= 0)
            w[c*Nmax+(int)indexstart[c]] = 1.0f;
        else {
            // left ramp
            float increment = 1.0f / (indexcenter[c] - indexstart[c]);
            for (int i=indexstart[c]; i < indexcenter[c]; i++) {
                w[c*Nmax+i] = (i - indexstart[c]) * increment;
            }
            // right ramp
            float decrement = 1.0f / (indexstop[c] - indexcenter[c]);
            for (int i=indexcenter[c]; i < indexstop[c]; i++) {
                w[c*Nmax+i] = 1.0 - ((i - indexcenter[c])*decrement);
            }
        }
	}
	
	// free memory
	free(melcenters);
	free(fcenters);
	free(startfreq);
	free(endfreq);
	free(indexcenter);
	free(fftfreq);
	free(indexstart);
	free(indexstop);
	free(diff);
    free(idxbw);
    free(FFTbandwidth);
    
	// return matrix
	return w;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    /*
     Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
     If your application supports background execution, called instead of applicationWillTerminate: when the user quits.
     */
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    /*
     Called as part of  transition from the background to the inactive state: here you can undo many of the changes made on entering the background.
     */
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    /*
     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */
    
    // don't do this if local options are not loaded
    if (!isLocalOptionsLoaded)
        NSLog(@"Local options not loaded in applicationDidBecomeActive");
    else {
        // check for cloud progress changes
        NSLog(@"Ask options to check for cloud changes in applicationDidBecomeActive");
        [appOptions checkForCloudChanges];
    }
}

- (void)applicationWillTerminate:(UIApplication *)application {
    /*
     Called when the application is about to terminate.
     See also applicationDidEnterBackground:.
     */
	
	// unload speech engine
	VT_UNLOADTTS_JPN_Show(-1);
	VT_UNLOADTTS_JPN_Misaki(-1);
}


#pragma mark -
#pragma mark Memory management

- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application {
    /*
     Free up as much memory as possible by purging cached data objects that can be recreated (or reloaded from disk) later.
     */
}

- (void)dealloc {
    // destroy dsp setup now we're done
    vDSP_destroy_fftsetup(setupReal);
    
    // free mel matrix
    free(melMatrix8k);
    free(melMatrix16k);
    
    [availableAnimeNames release];
//    [phonemeSyncList release];
//    [actorFrames release];
    [fileCache release];
//    [mecab release];
//    [mecabExceptions release];
	[navigationController release];
	[trainingData release];
	[currentUserProfile release];
	[currentCpuProfile release];
    [romajiPath release];
    [phonemes release];
    [trainingWords release];
    [localOptions release];
    [appOptions release];
    
    [_window release];
    
	[super dealloc];
}


@end

