//
//  UserProfile.h
//  Mcp
//
//  Created by Ray on 2/14/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NeuralNet.h"
#import "GameOptions.h"
#import "GameData.h"
#import "ClusterSet.h"
#import "DataFile.h"
#import "BufferHistory.h"
#import "NewTrainingData.h"

@interface UserProfile : DataFile <DataFileDelegate> {
	NSString *name;

	int gender; // 0=male, 1=female
	int voicePitch; // default 100
    
    float badScoreBoundary; // good and bad score boundaries
    float goodScoreBoundary;
	
	GameOptions *gameOptions;
	GameData *gameData;
	
//	NeuralNet *translationNet;
    
    BufferHistory *bufferHistory;
//    BufferHistory *featureHistory;
    
    NewTrainingData *micTrainingData;
    NewTrainingData *btTrainingData;
    NewTrainingData *headsetTrainingData;
    
    NeuralNet *micNet;
    NeuralNet *btNet;
    NeuralNet *headsetNet;
        
//	ClusterSet *clusterSet;
}

-(id)init;
-(id)initWithName:(NSString*)inName;
-(id)initFromArchive:(NSString*)inName;
-(void)dealloc;
-(NewTrainingData*)getTrainingDataForAudioDevice;
-(NeuralNet*)getNetForAudioDevice;
-(int)getSampleRateForAudioDevice;
-(void)replaceNetForAudioDeviceWith:(NeuralNet*)inNet;

@property (nonatomic, retain) NSString *name;
//@property (nonatomic, retain) NeuralNet *translationNet;
@property (nonatomic, retain) GameOptions *gameOptions;
@property (nonatomic, retain) GameData *gameData;
@property (nonatomic, assign) int gender;
@property (nonatomic, assign) int voicePitch;
@property (nonatomic, retain) ClusterSet *clusterSet;
@property (nonatomic, retain) BufferHistory *bufferHistory;
//@property (nonatomic, retain) BufferHistory *featureHistory;
@property (nonatomic, assign) float goodScoreBoundary;
@property (nonatomic, assign) float badScoreBoundary;

//@property (nonatomic, retain) NewTrainingData *micTrainingData;
//@property (nonatomic, retain) NewTrainingData *btTrainingData;
//@property (nonatomic, retain) NewTrainingData *headsetTrainingData;

@end
