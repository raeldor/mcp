//
//  TimeDelayNeuralNet.m
//  Mcp
//
//  Created by Ray on 2/6/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "TimeDelayNeuralNet.h"
#import "NeuronLayer.h"

@implementation TimeDelayNeuralNet

@synthesize layers;
@synthesize inputCount;
@synthesize outputCount;
@synthesize hiddenLayerCount;
@synthesize maxTimeDelay1;
@synthesize maxTimeDelay2;
@synthesize neuronsPerHiddenLayer1;
@synthesize neuronsPerHiddenLayer2;
@synthesize bias;
@synthesize activationResponse;
@synthesize learningRate;
@synthesize errorSum;
@synthesize trained;
@synthesize numEpochs;

-(id)initWithInputCount:(int)inInputCount outputCount:(int)inOutputCount hiddenLayerCount:(int)inHiddenLayerCount neuronsPerHiddenLayer1:(int)inNeuronsPerHiddenLayer1 maxTimeDelay1:(int)inMaxTimeDelay1 neuronsPerHiddenLayer2:(int)inNeuronsPerHiddenLayer2 maxTimeDelay2:(int)inMaxTimeDelay2 learningRate:(double)inLearningRate {
	if ((self = [super init])) {
		// setup bias and activation response once
		bias = -1.0f;
        activationResponse = 1.0f; // normal sigmoid
		momentumDelta = 0.9f;
		
		// save parameters
		inputCount = inInputCount;
		outputCount = inOutputCount;
		hiddenLayerCount = inHiddenLayerCount;
        maxTimeDelay1 = inMaxTimeDelay1;
        maxTimeDelay2 = inMaxTimeDelay2;
		neuronsPerHiddenLayer1 = inNeuronsPerHiddenLayer1;
		neuronsPerHiddenLayer2 = inNeuronsPerHiddenLayer2;
		learningRate = inLearningRate;
		
		// create array
		layers = [[NSMutableArray alloc] init];
		
		// create layers
		if (hiddenLayerCount > 0) {
			// create first hidden laer
			[layers addObject:[[[NeuronLayer alloc] initWithNeuronCount:neuronsPerHiddenLayer1 inputsPerNeuron:inputCount*maxTimeDelay1] autorelease]];
			
			// create other hidden layers
			for (int i=0; i < hiddenLayerCount-1; i++)
				[layers addObject:[[[NeuronLayer alloc] initWithNeuronCount:neuronsPerHiddenLayer2 inputsPerNeuron:neuronsPerHiddenLayer1*maxTimeDelay2] autorelease]];
			
			// create output layer
			[layers addObject:[[[NeuronLayer alloc] initWithNeuronCount:outputCount inputsPerNeuron:neuronsPerHiddenLayer2] autorelease]];
		}
		else {
			// create output layer
			[layers addObject:[[[NeuronLayer alloc] initWithNeuronCount:outputCount inputsPerNeuron:inputCount] autorelease]];
		}
	}
	return self;
}

-(void)resetWeights {
    // reset all weights
    for (int i=0; i < layers.count; i++) {
        NeuronLayer *thisLayer = [layers objectAtIndex:i];
        for (int n=0; n < thisLayer.neurons.count; n++) {
            Neuron *thisNeuron = [thisLayer.neurons objectAtIndex:n];
            for (int w=0; w < thisNeuron.inputCount; w++)
                thisNeuron.weights[w] = (rand()/(double)RAND_MAX)*2.0f-1.0f;
        }
    }
}

-(id)initWithCoder:(NSCoder*)coder {
	if ((self = [super init])) {
        // get parameters
        inputCount = [coder decodeInt32ForKey:@"inputCount"];
        outputCount = [coder decodeInt32ForKey:@"outputCount"];
        hiddenLayerCount = [coder decodeInt32ForKey:@"hiddenLayerCount"];
        maxTimeDelay1 = [coder decodeInt32ForKey:@"maxTimeDelay1"];
        maxTimeDelay2 = [coder decodeInt32ForKey:@"maxTimeDelay2"];
        neuronsPerHiddenLayer1 = [coder decodeInt32ForKey:@"neuronsPerHiddenLayer1"];
        neuronsPerHiddenLayer2 = [coder decodeInt32ForKey:@"neuronsPerHiddenLayer2"];
        learningRate = [coder decodeFloatForKey:@"learningRate"];
        bias = [coder decodeFloatForKey:@"bias"];
        momentumDelta = [coder decodeFloatForKey:@"momentumDelta"];
        activationResponse = [coder decodeFloatForKey:@"activationResponse"];
        trained = [coder decodeBoolForKey:@"trained"];
        
        // get network
        self.layers = [coder decodeObjectForKey:@"layers"];
	}
	return self;
}

-(void)encodeWithCoder:(NSCoder*)coder {
    // encode parameters
    [coder encodeInt32:inputCount forKey:@"inputCount"];
    [coder encodeInt32:outputCount forKey:@"outputCount"];
    [coder encodeInt32:hiddenLayerCount forKey:@"hiddenLayerCount"];
    [coder encodeInt32:maxTimeDelay1 forKey:@"maxTimeDelay1"];
    [coder encodeInt32:maxTimeDelay2 forKey:@"maxTimeDelay2"];
    [coder encodeInt32:neuronsPerHiddenLayer1 forKey:@"neuronsPerHiddenLayer1"];
    [coder encodeInt32:neuronsPerHiddenLayer2 forKey:@"neuronsPerHiddenLayer2"];
    [coder encodeFloat:learningRate forKey:@"learningRate"];
    [coder encodeFloat:bias forKey:@"bias"];
    [coder encodeFloat:momentumDelta forKey:@"momentumDelta"];
    [coder encodeFloat:activationResponse forKey:@"activationResponse"];
    [coder encodeBool:trained forKey:@"trained"];
    
    // encode network
    [coder encodeObject:layers forKey:@"layers"];
}

-(id)copyWithZone:(NSZone *)zone {
    // make new net
    TimeDelayNeuralNet *newNet = [[TimeDelayNeuralNet alloc] initWithInputCount:inputCount outputCount:outputCount hiddenLayerCount:hiddenLayerCount neuronsPerHiddenLayer1:(int)neuronsPerHiddenLayer1 maxTimeDelay1:(int)maxTimeDelay1 neuronsPerHiddenLayer2:(int)neuronsPerHiddenLayer2 maxTimeDelay2:(int)maxTimeDelay2 learningRate:learningRate];
    newNet.trained = trained;
    
    // now copy weights
    for (int l=0; l < layers.count; l++) {
        NeuronLayer *thisLayer = [layers objectAtIndex:l];
        NeuronLayer *newLayer = [newNet.layers objectAtIndex:l];
        for (int n=0; n < thisLayer.neurons.count; n++) {
            Neuron *thisNeuron = [thisLayer.neurons objectAtIndex:n];
            Neuron *newNeuron = [newLayer.neurons objectAtIndex:n];
            for (int w=0; w < thisNeuron.inputCount+1; w++) {
                newNeuron.weights[w] = thisNeuron.weights[w];
            }
        }
    }
    
    return newNet;
}

-(double*)updateWithInputs:(double*)inInputs {
	// create a memory area to store outputs, up to calling proc to free
	double *outputs = malloc(sizeof(double)*(MAX(MAX(outputCount,neuronsPerHiddenLayer1),neuronsPerHiddenLayer2)));
	double *myInputs = malloc(sizeof(double)*(MAX(MAX(inputCount,neuronsPerHiddenLayer1),neuronsPerHiddenLayer2)));
    
    // zero memory
    memset(outputs, 0, sizeof(double)*(MAX(MAX(outputCount,neuronsPerHiddenLayer1),neuronsPerHiddenLayer2)));
    memset(myInputs, 0, sizeof(double)*(MAX(MAX(inputCount,neuronsPerHiddenLayer1),neuronsPerHiddenLayer2)));
	
	// copy inputs to my inputs
	for (int i=0; i < inputCount; i++) {
		myInputs[i] = inInputs[i];
	}
	
	// loop through layers
	int weightPos = 0;
	for (int i=0; i < hiddenLayerCount+1; i++) {
        // get this layer
        NeuronLayer *thisLayer = [layers objectAtIndex:i];
        NeuronLayer *layerBelow = nil;
        if (i > 0)
             layerBelow = [layers objectAtIndex:i-1];
        
        // shuffle inputs along for time delay, then copy new inputs to beginning
        int realInputCount = i==0?inputCount:layerBelow.neuronCount;
        for (int c=thisLayer.inputsPerNeuron; c > 0; c++) {
            if (c > realInputCount)
                thisLayer.inputs[c-1] = thisLayer.inputs[c-realInputCount-1];
            else
                thisLayer.inputs[c-1] = inInputs[i];
        }
        
		// for each neuron sum the inputs * corresponding weights, use the
		// sigmoid function of this to get the output
		weightPos = 0;
		for (int j=0; j < [[layers objectAtIndex:i] neuronCount]; j++) {
			Neuron *thisNeuron = [thisLayer.neurons objectAtIndex:j];
			double netInput = 0.0f;
			
			// for each weight (except bias)
			for (int k=0; k < thisNeuron.inputCount; k++) {
				netInput += thisNeuron.weights[k] * myInputs[weightPos++];
			}
			
			// add in the bias
			netInput += thisNeuron.weights[thisNeuron.inputCount] * bias;
			
			// update the outputs as we generate them
			outputs[j] = [self sigmoidWithInput:netInput response:activationResponse];
			
			// save the output into the activiation property of the neuron
			thisNeuron.activation = outputs[j];
			
			// reset weight position
			weightPos = 0;
			
		}
		
		// after first layer, copy outputs to inputs for inputs of next layer
		for (int ip=0; ip < [[layers objectAtIndex:i] neuronCount]; ip++) {
			myInputs[ip] = outputs[ip];
		}
	}
    
	// free temporary inputs space
	free(myInputs);
    
	// return array of outputs
	return outputs;
}

// this has been adjusted to work with multiple layers
-(bool)trainNetworkOnceWithInputSet:(double*)inInputSet ofSize:(int)inSetSize andOutputSet:(double*)inOutputSet {
	// cumulative error for training set
	errorSum = 0.0f;
	
	// run each input pattern through the network, get outputs, calculate error and adjust weights
	for (int s=0; s < inSetSize; s++) {
		// first run input pattern and get outputs
		double *outputs = [self updateWithInputs:&inInputSet[s*inputCount]];
		
		// save layers for readability
		NeuronLayer *outputLayer = [layers objectAtIndex:hiddenLayerCount];
		
		// for each output neuron, calculate the error and adjust the weights
		for (int o=0; o < outputCount; o++) {
			// first calculate the error value
			double err = (inOutputSet[s*outputCount+o] - outputs[o]) * outputs[o] * (1.0f - outputs[o]);
			
			// update the error total (when this becomes lower than threshold, training is success)
			errorSum += (inOutputSet[s*outputCount+o] - outputs[o]) * (inOutputSet[s*outputCount+o] - outputs[o]);
			
			// keep a record of the error value
			Neuron *thisOutputNeuron = [outputLayer.neurons objectAtIndex:o];
			thisOutputNeuron.errorValue = err;
			
			// for each weight for this output neuron except the bias
			int hiddenNeuronIndex = 0;
			NeuronLayer *hiddenLayer = [layers objectAtIndex:hiddenLayerCount-1];
			for (int w=0; w < neuronsPerHiddenLayer2; w++) {
				// calculate the new weight based on the backprop rules
				Neuron *thisHiddenNeuron = [hiddenLayer.neurons objectAtIndex:hiddenNeuronIndex];
				thisOutputNeuron.weights[w] += err * learningRate * thisHiddenNeuron.activation;
				
				// go to next weight and next hidden neuron
				hiddenNeuronIndex++;
			}
			
			// also adust the bias
			thisOutputNeuron.weights[neuronsPerHiddenLayer2] += err * learningRate * bias;
		}
		
		// loop through hidden layers
		for (int h=hiddenLayerCount-1; h >= 0; h--) {
			// get this hidden layer
			NeuronLayer *hiddenLayer = [layers objectAtIndex:h];
			NeuronLayer *layerAbove = [layers objectAtIndex:h+1];
            int neuronsPerHiddenLayer = h==0?neuronsPerHiddenLayer1:neuronsPerHiddenLayer2;
            
			// for each neuron in the hidden layer, calc and adjust the weights
			for (int n=0; n < neuronsPerHiddenLayer; n++) {
				double err = 0;
				int myOutputCount = (h+1 == hiddenLayerCount ? outputCount : neuronsPerHiddenLayer);
				for (int o=0; o < myOutputCount; o++) {
					Neuron *thisOutputNeuron = [layerAbove.neurons objectAtIndex:o];
//					err += thisOutputNeuron.errorValue * thisOutputNeuron.weights[n];
					err += thisOutputNeuron.errorValue * hiddenLayer.inputs[n];
				}
				
				// now we can calculate the error
				Neuron *thisHiddenNeuron = [hiddenLayer.neurons objectAtIndex:n];
				err *= thisHiddenNeuron.activation * (1.0f - thisHiddenNeuron.activation);
				
				// save error value to this hidden neuron
				thisHiddenNeuron.errorValue = err;
				
				// for each weight in this neuron (hidden) calculate the new weight based on the error signal and learning rate
				int myInputCount = (h == 0 ? inputCount : neuronsPerHiddenLayer);
				for (int w=0; w < myInputCount; w++) {
					if (h == 0)
						thisHiddenNeuron.weights[w] += err * learningRate * inInputSet[s*inputCount+w];
					else {
						NeuronLayer *layerBelow = [layers objectAtIndex:h-1];
						Neuron *inputNeuron = [layerBelow.neurons objectAtIndex:w];
						thisHiddenNeuron.weights[w] += err * learningRate * inputNeuron.activation;
					}
				}
				
				// calculate the bias
				thisHiddenNeuron.weights[myInputCount] += err * learningRate * bias;
			}
		}
		
		// free outputs array
		free(outputs);
	}
	
	// make error sum not dependent on set size or output count
	errorSum /= (float)inSetSize*(float)outputCount;
	
	// all went ok
	return YES;
}

// train the network repeatedly until the SSE (sum squared errors) is less than a threshold value
-(bool)trainNetworkWithInputSet:(double*)inInputSet ofSize:(int)inSetSize andOutputSet:(double*)inOutputSet {
    //	double errorThreshold = 0.02f;
	int passCount = 0;
	[self trainNetworkOnceWithInputSet:inInputSet ofSize:inSetSize andOutputSet:inOutputSet];
	float lastErrorSum = errorSum;
	[self trainNetworkOnceWithInputSet:inInputSet ofSize:inSetSize andOutputSet:inOutputSet];
    //	float firstImprovement = lastErrorSum - errorSum;
	// train until less than 0.1% of first improvement
    //	while (errorSum > errorThreshold && passCount < 10000) {
    while (passCount < 10000) {
        //    while ((lastErrorSum-errorSum)/firstImprovement > 0.01f && passCount < 4000) {
		// run network training epoch
		lastErrorSum = errorSum;
		[self trainNetworkOnceWithInputSet:inInputSet ofSize:inSetSize andOutputSet:inOutputSet];
		passCount++;
		if ((passCount % 100) == 0.0f)
			printf("Error sum is now %8.8f, pass count is %d\r\n", errorSum, passCount);
        //		printf("Error sum is now %3.2f%% of first improvement\r\n", ((lastErrorSum-errorSum)/firstImprovement)*100);
	}
	
	// mark network as trained
	trained = YES;
    
	return YES;
}

-(double)sigmoidWithInput:(double)inInput response:(double)inResponse {
	return 1.0f / (1 + pow(2.7183, -inInput/inResponse));
}

-(void)dealloc {
	[layers release];
	[super dealloc];
}
@end
