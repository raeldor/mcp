//
//  ConversationListViewController.m
//  Mcp
//
//  Created by Ray on 10/25/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "ConversationListViewController.h"
#import "EditConversationController.h"
#import "Conversation.h"
#import "Chapter.h"
#import "ConversationListCell.h"
#import "Character.h"

@implementation ConversationListViewController

@synthesize chapter;
@synthesize book;
@synthesize myTableView;

#pragma mark -
#pragma mark View lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
	
	// set title
	self.title = @"Conversations";
	
	// allow selecting during editor
	self.myTableView.allowsSelection = NO;
	self.myTableView.allowsSelectionDuringEditing = TRUE;
	
	// add edit button
	self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

-(void)setEditableObject:(Chapter *)inChapter inBook:(Book*)inBook {
	self.chapter = inChapter;
	self.book = inBook;
}

#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
    // show grammars as section header, so walk through and count changes
    int grammarCount = 0;
    NSString *lastGrammar = @"";
    for (int i=0; i < chapter.conversations.count; i++) {
        Conversation *thisConversation = [chapter.conversations objectAtIndex:i];
        NSString *grammarList = [thisConversation getGrammarListAsString];
        if (![grammarList isEqualToString:lastGrammar]) {
            grammarCount++;
            lastGrammar = grammarList;
        }
    }

    return grammarCount;
//	return 1;
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return nil;
    // return title of grammar
    int grammarCount = 0;
    NSString *lastGrammar = @"";
    for (int i=0; i < chapter.conversations.count; i++) {
        Conversation *thisConversation = [chapter.conversations objectAtIndex:i];
        NSString *grammarList = [thisConversation getGrammarListAsString];
        if (![grammarList isEqualToString:lastGrammar]) {
            grammarCount++;
            lastGrammar = grammarList;
            
            // if this is our boy, return list
            if (grammarCount-1 == section)
                return lastGrammar;
        }
    }
    
    return @"Grammar Not Found";
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	// return conversations
	if (self.editing)
        return chapter.conversations.count+1;
	else
	{
		if (chapter.conversations.count == 0)
			return 1;
		else
            return chapter.conversations.count;
	}
    
    // get number of conversations in this section
    // return title of grammar
    int grammarCount = 0;
    int lastConvCount = 0;
    NSString *lastGrammar = @"";
    for (int i=0; i < chapter.conversations.count; i++) {
        Conversation *thisConversation = [chapter.conversations objectAtIndex:i];
        NSString *grammarList = [thisConversation getGrammarListAsString];
        if (![grammarList isEqualToString:lastGrammar]) {
            grammarCount++;
            lastGrammar = grammarList;
            lastConvCount = 1;
            
            // if this is our boy, return list
            if (grammarCount-1 == section)
                break;
        }
        else
            lastConvCount++;
    }
    
	// return conversations
	if (self.editing)
        return lastConvCount;
//		return chapter.conversations.count+1;
	else
	{
		if (chapter.conversations.count == 0)
			return 1;
		else
            return lastConvCount;
//			return chapter.conversations.count;
	}
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	return 100;
}

// Customize the appearance of table view cells.
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSUInteger row = [indexPath row];
    
	// get identifier and get cell
	NSString *CellIdentifier = @"ConversationListCell";
	if ((chapter.conversations.count == 0 && !self.editing) || (self.editing && row == chapter.conversations.count))
        CellIdentifier = @"Cell";
	UITableViewCell *cell = (UITableViewCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	if (cell == nil) {
        if ([CellIdentifier isEqualToString:@"Cell"])
            cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        else {
            // load cell layout from nib
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:CellIdentifier owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
	}
	
	// default cell config
	cell.textLabel.text = NULL;
	cell.imageView.image = NULL;
	cell.accessoryType = UITableViewCellAccessoryNone;
	cell.editingAccessoryType = UITableViewCellAccessoryNone;
	cell.selectionStyle = UITableViewCellSelectionStyleNone;
	cell.textLabel.textColor = [UIColor blackColor];
    
    
	// if no cells, display 'press to add' message
	if (chapter.conversations.count == 0 && !self.editing) {
		cell.textLabel.text = @"Press Edit Button To Create New Conversation";
		cell.textLabel.textColor = [UIColor lightGrayColor];
		cell.textLabel.adjustsFontSizeToFitWidth = YES;
	}
	else {
		// if this is a cell showing conversation
		if (row < chapter.conversations.count) {
            // clear existing labels
            ConversationListCell *convCell = (ConversationListCell*)cell;
            convCell.grammarLabel.text = nil;
            convCell.actorLabel1.text = nil;
            convCell.dialogueLabel1.text = nil;
            convCell.actorLabel2.text = nil;
            convCell.dialogueLabel2.text = nil;
            convCell.actorLabel3.text = nil;
            convCell.dialogueLabel3.text = nil;
            convCell.actorLabel4.text = nil;
            convCell.dialogueLabel4.text = nil;

            // setup the cell
			Conversation *thisConv = [chapter.conversations objectAtIndex:[indexPath row]];
			convCell.grammarLabel.text = [thisConv getGrammarListAsString];
			for (int i=0; i < thisConv.dialogueLines.count && i < 3; i++) {
				DialogueLine *thisLine = [thisConv.dialogueLines objectAtIndex:i];
				Character *thisCharacter = [thisConv.characters objectAtIndex:thisLine.characterIndex];
				switch (i) {
					case 0:
						convCell.actorLabel1.text = [NSString stringWithFormat:@"%@:", thisCharacter.characterName];
						convCell.dialogueLabel1.text = [thisLine getLineAsText];
						break;
					case 1:
						convCell.actorLabel2.text = [NSString stringWithFormat:@"%@:", thisCharacter.characterName];
						convCell.dialogueLabel2.text = [thisLine getLineAsText];
						break;
					default:
						convCell.actorLabel3.text = [NSString stringWithFormat:@"%@:", thisCharacter.characterName];
						convCell.dialogueLabel3.text = [thisLine getLineAsText];
						break;
				}
			}
//			[convCell.dialogueWebView loadHTMLString:[thisConv getDialogHtmlPreview] baseURL:nil];
//			convCell.dialogueWebView.opaque = NO;
//			convCell.dialogueWebView.backgroundColor = [UIColor clearColor];
			cell.textLabel.text = nil;
			cell.editingAccessoryType = UITableViewCellAccessoryDisclosureIndicator;
			cell.selectionStyle = UITableViewCellSelectionStyleBlue;
		}
		else {
			cell.textLabel.text = @"Add New";
			cell.textLabel.textColor = [UIColor lightGrayColor];
			cell.selectionStyle = UITableViewCellSelectionStyleBlue;
		}
	}
	
    return cell;
}

//editing mode changed
- (void)setEditing:(BOOL)editing animated:(BOOL)animated {
    // put table into edit mode manually because we're not using a table view controller
    [super setEditing:editing animated:animated];
    [self.myTableView setEditing:editing animated:animated];
    
	// add/remove placeholder items
	NSMutableArray *indexPaths = [NSMutableArray array];
	[indexPaths addObject:[NSIndexPath indexPathForRow:chapter.conversations.count inSection:0]];
	[self.myTableView beginUpdates];
	[super setEditing:editing animated:animated];
//	[self.myTableView setEditing:editing animated:YES];
	if (editing) {
		// delete 'press edit' message
		if (chapter.conversations.count == 0)
			[self.myTableView deleteRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationTop];
		
		// Show the placeholder rows and hide back button
		[self.myTableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationTop];
		self.navigationItem.hidesBackButton = YES;
	}
	else
	{
		// Hide the placeholder rows and show back button again
		[self.myTableView deleteRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationTop];
		self.navigationItem.hidesBackButton = NO;
		
		// show 'press edit' message
		if (chapter.conversations.count == 0)
			[self.myTableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationTop];
	}
	[self.myTableView endUpdates];
}

-(UITableViewCellEditingStyle)tableView:(UITableView*)tableView editingStyleForRowAtIndexPath:(NSIndexPath*)indexPath {
	// which row are we getting
	NSUInteger row = [indexPath row];
	
	// if we are in edit mode
	if (self.editing) {
		// show delete style and insert for 'add new'
		if (chapter.conversations.count > 0 && row != chapter.conversations.count)
			return UITableViewCellEditingStyleDelete;
		else
			return UITableViewCellEditingStyleInsert;
	}
	return UITableViewCellEditingStyleNone;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    // if we are deleting rows
    if (editingStyle == UITableViewCellEditingStyleDelete) {
		[chapter.conversations removeObjectAtIndex:[indexPath row]];
        [self.myTableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:YES];

		// save the book
		[book saveAs:book.bookName];
    }   
    else
		if (editingStyle == UITableViewCellEditingStyleInsert) {
			// edit conversation using edit controller
			EditConversationController *editConversationController = [[EditConversationController alloc] initWithNibName:@"EditConversationController" bundle:nil];
            
            // create new conversation
			Conversation *newConversation = [[Conversation alloc] init];
            
            // fill with random background
            NSArray *backgrounds = [DataFile getListOfFilesWithExtension:@"background"];
            if (backgrounds.count > 0) {
                int backgroundIndex = arc4random() % (backgrounds.count-1);
                newConversation.backgroundName = [backgrounds objectAtIndex:backgroundIndex];
            }
            
            // fill with 2 random characters
            NSArray *actors = [DataFile getListOfFilesWithExtension:@"actor"];
            int previousActorIndex = -1;
            int actorIndex = -1;
            if (actors.count > 0) {
                for (int a=0; a < 2; a++) {
                    while (actorIndex == previousActorIndex) {
                        actorIndex = arc4random() % (actors.count-1);
                    }
                    previousActorIndex = actorIndex;
                    Character *newCharacter = [[Character alloc] init];
                    newCharacter.actorName = [actors objectAtIndex:actorIndex];
                    newCharacter.characterName = newCharacter.actorName;
                    [newConversation.characters addObject:newCharacter];
                    [newCharacter release];
                }
            }
            
			[editConversationController setEditableObject:newConversation inBook:book andChapter:chapter isNew:YES];
			editConversationController.editableListDelegate = self;
			UINavigationController *secondNavigationController = [[UINavigationController alloc] initWithRootViewController:editConversationController];
			[self.navigationController presentModalViewController:secondNavigationController animated:YES];
			[newConversation release];
			[editConversationController release];
			[secondNavigationController release];
		}
}

-(BOOL)tableView:(UITableView*)tableView canMoveRowAtIndexPath:(NSIndexPath*)indexPath {
	// can't re-order 'add new'
	if ([indexPath row] < chapter.conversations.count)
		return YES;
	else
		return NO;
}

-(void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
	// get from and to row
	int fromRow = [fromIndexPath row];
	int toRow = [toIndexPath row];
	
	// swap these rows in the array
	Conversation *moveConv = [[chapter.conversations objectAtIndex:fromRow] retain];
	[chapter.conversations removeObjectAtIndex:fromRow];
	[chapter.conversations insertObject:moveConv atIndex:toRow];
	[moveConv release];
	
	// save changes
	[book saveAs:book.bookName];
}

-(void)didUpdateObject:(id)updatedObject {
    // update the conversation
    [chapter.conversations replaceObjectAtIndex:[lastIndexPath row] withObject:updatedObject];
    
	// save the book
	[book saveAs:book.bookName];
	
	// refresh table
	[self.myTableView reloadData];
}

-(void)didAddNewObject:(id)newObject {
	// save new class
	Conversation *newConversation = (Conversation*)newObject;
	
	// add new conversation and save
	[chapter.conversations addObject:newConversation];
	[book saveAs:book.bookName];
	
	// refresh table
	[self.myTableView reloadData];
}

#pragma mark -
#pragma mark Table view delegate

-(void)tableView:(UITableView*)inTableView didSelectRowAtIndexPath:(NSIndexPath*)indexPath {
	// find row
	NSUInteger row = [indexPath row];
    
    // save index path for didupdate
    [lastIndexPath release];
    lastIndexPath = [indexPath retain];
	
	// editing mode?
	if (self.editing) {
        // are we asking to make a copy of the conversation?
		NSTimeInterval fingerDownTime = [NSDate timeIntervalSinceReferenceDate]-self.myTableView.touchesBeganTime;
		if (fingerDownTime > 0.5f) {
            // confirm first
            UIAlertView *myAlert = [[UIAlertView alloc] initWithTitle:@"Copy" message:@"Make a new copy of this conversation?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
            [myAlert show];
            [myAlert release];
            return;
        }
        
		// add new?
		if (row >= [chapter.conversations count]) {
			[self tableView:inTableView commitEditingStyle:UITableViewCellEditingStyleInsert forRowAtIndexPath:indexPath];
		}
		else {
			// edit conversation using edit controller
			EditConversationController *editConversationController = [[EditConversationController alloc] initWithNibName:@"EditConversationController" bundle:nil];
			[editConversationController setEditableObject:[chapter.conversations objectAtIndex:row] inBook:book andChapter:chapter isNew:NO];
			editConversationController.editableListDelegate = self;
			[self.navigationController pushViewController:editConversationController animated:YES];
			[editConversationController release];
		}
	}
	else {
		// selected conversation in non edit mode, so show conversations
	}
}

-(void)viewWillAppear:(BOOL)animated {
    // deselect currently selected row
    [self.myTableView deselectRowAtIndexPath:[self.myTableView indexPathForSelectedRow] animated:animated];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    // if yes was pressed
    if (buttonIndex == 1) {
        // make a copy after the current row, and refresh the table
        Conversation *conversationCopy = [[chapter.conversations objectAtIndex:[lastIndexPath row]] copy];
        [chapter.conversations insertObject:conversationCopy atIndex:[[self.myTableView indexPathForSelectedRow] row]+1];
        [conversationCopy release];
//        [chapter.conversations addObject:[[chapter.conversations objectAtIndex:[lastIndexPath row]] copy]];
        [self.myTableView reloadData];
    }
}

#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    // Relinquish ownership of anything that can be recreated in viewDidLoad or on demand.
    // For example: self.myOutlet = nil;
}


- (void)dealloc {
    [lastIndexPath release];
    [myTableView release];
	[chapter release];
	[book release];
    [super dealloc];
}


@end

