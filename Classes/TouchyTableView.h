//
//  TouchyTableView.h
//  Mcp
//
//  Created by Ray on 7/2/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface TouchyTableView : UITableView {
	NSTimeInterval touchesBeganTime;
}

@property (nonatomic, assign) NSTimeInterval touchesBeganTime;

@end
