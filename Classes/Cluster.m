//
//  Cluster.m
//  Mcp
//
//  Created by Ray on 3/20/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "Cluster.h"

@implementation Cluster

@synthesize clusterIndex;
@synthesize dataPoints;

-(id)init {
	if (self = [super init]) {
		self.dataPoints = [NSMutableArray arrayWithCapacity:10];
	}
	return self;
}

-(void)expandClusterUsingPoint:(DataPoint*)inDataPoint withNeighbors:(DataSet*)inNeighbors inDataSet:(DataSet*)inDataSet withEps:(float)inEps andMinPts:(int)inMinPts {
	// add point to cluster
	[self addDataPoint:inDataPoint];
	
	// for each of the neighbors
	DataPoint *pDash;
	while ((pDash = [inNeighbors getNextUnvisitedDataPoint])) {
		// mark as visited
		pDash.visited = YES;
		
		// get neighbors and join together if has more than minimum points
		DataSet *nDash = [inDataSet getNeighborsOfPoint:pDash UsingEps:inEps];
		if (nDash.dataPoints.count >= inMinPts) {
			[inNeighbors addPointsFromDataSet:nDash];
		}
		// add pDash to cluster if it's not already a member of another cluster
		if (pDash.clusterIndex == -1)
			[self addDataPoint:pDash];
	}
}

-(void)addDataPoint:(DataPoint*)inDataPoint {
	[dataPoints addObject:inDataPoint];
	inDataPoint.clusterIndex = clusterIndex;
}

-(float)getPercentChanceOfFeature:(int)inFeatureIndex {
	// count data points that have this feature index
	int featureCount = 0;
	for (int i=0; i < dataPoints.count; i++) {
		DataPoint *thisPoint = [dataPoints objectAtIndex:i];
		if (thisPoint.dataSourceIndex == inFeatureIndex) {
			featureCount++;
		}
	}
	return (float)featureCount/(float)dataPoints.count;
}

-(bool)hasNeighborsOfPoint:(DataPoint*)inDataPoint eps:(float)inEps {
	// see if any neighbors are close enough
	for (int i=0; i < dataPoints.count; i++) {
		DataPoint *thisPoint = [dataPoints objectAtIndex:i];
		if ([thisPoint distanceFromDataPoint:inDataPoint] < inEps)
			return YES;
	}	
	return NO;
}

-(float)getDistanceOfClosestPointToPoint:(DataPoint*)inDataPoint {
	float distance = 99999.0f;
	for (int i=0; i < dataPoints.count; i++) {
		DataPoint *thisPoint = [dataPoints objectAtIndex:i];
		if ([thisPoint distanceFromDataPoint:inDataPoint] < distance)
			distance = [thisPoint distanceFromDataPoint:inDataPoint];
	}	
	return distance;
}

/*
-(bool)containsDataPoint:(DataPoint*)inDataPoint {
	for (int i=0; i < dataPoints.count; i++) {
		if ([dataPoints objectAtIndex:i] == inDataPoint) {
			return YES;
		}
	}
	return NO;
}
*/

-(void)dealloc {
	[dataPoints release];
	[super dealloc];
}

@end
