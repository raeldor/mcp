//
//  FloatIndex.h
//  Mcp
//
//  Created by Ray on 3/27/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface FloatIndex : NSObject {
	float floatValue;
	int indexValue;
}

-(id)initWithFloatValue:(float)inFloatValue indexValue:(int)inIndexValue;
-(void)dealloc;
-(NSComparisonResult)compareFloat:(FloatIndex*)inFloatIndex;
-(NSComparisonResult)compareIndex:(FloatIndex*)inFloatIndex;

@property (nonatomic, assign) float floatValue;
@property (nonatomic, assign) int indexValue;

@end
