//
//  TrainingViewController.m
//  Mcp
//
//  Created by Ray on 1/24/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "TrainingViewController.h"
#include "vt_jpn_show.h"
#include "vt_jpn_misaki.h"
#import "AudioToolbox/AudioFile.h"
#include "math.h"
#include "limits.h"
#import "PhonemeData.h"
#import "McpAppDelegate.h"
#import "PhonemeSample.h"
#import "VoiceSample.h"
#import "NeuralNet.h"
#import "AudioFile.h"
#import "SyllablePhoneme.h"
#import "DataSet.h"
#import "DataPoint.h"
#import "ClusterSet.h"

@implementation TrainingViewController

@synthesize beginButton;
@synthesize endButton;
@synthesize backButton;
@synthesize clearButton;
@synthesize sayLabel;
@synthesize statusLabel;

// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
    }
    return self;
}
*/

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
}
*/

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	
	// set title
	self.title = @"Training";
	
	// save reference to training data for speed
	McpAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
	trainingData = appDelegate.trainingData;
	
	// set text to say next to blank
	self.sayLabel.text = @"";
    
    // create audio recorder and player
    myRecorder = [[AudioRecorder alloc] init];
    myPlayer = [[AudioPlayer alloc] init];

/*	
	// set up recording structure
	recordState.dataFormat.mSampleRate = 16000;
	recordState.dataFormat.mFormatID = kAudioFormatLinearPCM;
	recordState.dataFormat.mFramesPerPacket = 1;
	recordState.dataFormat.mChannelsPerFrame = 1;
	recordState.dataFormat.mBytesPerFrame = 4;
	recordState.dataFormat.mBytesPerPacket = 4;
	recordState.dataFormat.mBitsPerChannel = 32;
	recordState.dataFormat.mReserved = 0;
	recordState.dataFormat.mFormatFlags = kLinearPCMFormatFlagIsFloat|kLinearPCMFormatFlagIsPacked;
	
	// create input queue
	OSStatus status = AudioQueueNewInput(&recordState.dataFormat, AudioInputCallback3, &recordState, CFRunLoopGetCurrent(), kCFRunLoopCommonModes, 0, &recordState.queue);
	
	// allocate memory and queue buffers
	for (int i=0; i < NUM_BUFFERS; i++)
	{
		AudioQueueAllocateBuffer(recordState.queue, 4000*4, &recordState.buffers[i]);
		AudioQueueEnqueueBuffer(recordState.queue, recordState.buffers[i], 0, NULL);
	}
	
	// allocate buffer long enough for 10 seconds recording
	recordBuffer = malloc(10*16000*4);
	recordBufferPos = 0;
*/
    
	// start timer for label update
	labelTimer = [[NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(labelTimerFired:) userInfo:nil repeats:YES] retain];
    
/*	
	// setup play state information
	playState.dataFormat = recordState.dataFormat;
	
	// create output queue
	status = AudioQueueNewOutput(&playState.dataFormat, AudioOutputCallback3, &playState, CFRunLoopGetCurrent(), kCFRunLoopCommonModes, 0, &playState.queue);
	
	// allocate buffers, but don't queue yet
	for (int i=0; i < NUM_BUFFERS; i++)
	{
		OSStatus status = AudioQueueAllocateBuffer(playState.queue, 4000*4, &playState.buffers[i]);
		playBufferPos = 0;
	}	
 */
}

/*
void AudioOutputCallback3(void *inUserData, AudioQueueRef inAQ, AudioQueueBufferRef inBuffer)
{
	// fill data and requeue buffer
	PlayState *playState = (PlayState*)inUserData;
	if (playBufferPos < recordBufferPos)
	{
		[[NSString stringWithFormat:@"Queue next playback buffer from position %d\n", playBufferPos] writeToFile:@"/dev/stdout" atomically:NO];
		
		memcpy(inBuffer->mAudioData, recordBuffer+playBufferPos, 4000*4);
		playBufferPos += 4000*4;
		AudioQueueEnqueueBuffer(playState->queue, inBuffer, 0, NULL);
	}
	else {
		if (isPlaying)
		{
			UInt32 isRunning;
			UInt32 isRunningSize=sizeof(isRunning);
			OSStatus status = AudioQueueGetProperty(inAQ, kAudioQueueProperty_IsRunning, &isRunning, &isRunningSize);
			[[NSString stringWithFormat:@"Isrunning in output callback returned as %d\n", isRunning] writeToFile:@"/dev/stdout" atomically:NO];
			if (isRunning == 1)
			{
				// stop playback, but not until finished
				[[NSString stringWithFormat:@"Stop audio playback, but wait until finished\n"] writeToFile:@"/dev/stdout" atomically:NO];
				AudioQueueStop(playState->queue, false);
				
				// wait for queue to stop, then call my proc
				OSStatus status = AudioQueueAddPropertyListener(playState->queue, kAudioQueueProperty_IsRunning, PlaybackFinishedCallback3, NULL);
			}			
		}
	}
	
}

void PlaybackFinishedCallback3(void *inUserData, AudioQueueRef inAQ, AudioQueuePropertyID inID)
{
	[[NSString stringWithFormat:@"Inside playback finished\n"] writeToFile:@"/dev/stdout" atomically:NO];

	PlayState *playState = (PlayState*)inUserData;
	
	// if playback has finished
	
	
	UInt32 isRunning;
	UInt32 isRunningSize=sizeof(isRunning);
	OSStatus status = AudioQueueGetProperty(inAQ, kAudioQueueProperty_IsRunning, &isRunning, &isRunningSize);
	[[NSString stringWithFormat:@"Isrunning returned as %d\n", isRunning] writeToFile:@"/dev/stdout" atomically:NO];
	if (isRunning == 0)
	{
		// stop playing
		isPlaying = NO;
		
	}
}
*/

-(void)labelTimerFired:(NSTimer*)inTimer {
	// update label
	if (myRecorder.recordState.state == AUDIORECORDER_STATE_ANALYZING) {
		// no longer listening
//		isListening = NO;
		
		// clear say label
		self.sayLabel.text = @"";
		
		// check pronunciation here
		statusLabel.text = @"Analyzing...";
		
		// normalize sample
		[self normalizeFloatArray:myRecorder.recordState.recordBuffer ofSize:myRecorder.recordState.recordBufferPos/sizeof(float)];
		
		// trim sample
		float *inBuffer = (float*)myRecorder.recordState.recordBuffer;
		int inLength = myRecorder.recordState.recordBufferPos/sizeof(float);
		
		// find lead and tail length where < 10 % amplitude
		int leadLength = 0;
		for (int i=0; i < inLength; i++) {
			if (fabs(inBuffer[i]) > 0.01f) {
				leadLength = i;
				break;
			}
		}
		int tailLength = 0;
		for (int i=inLength-1; i >= 0; i--) {
			if (fabs(inBuffer[i]) > 0.01f) {
				tailLength = inLength-i;
				break;
			}
		}
		
		printf("lead is %d\n\r", leadLength);
		printf("tail is %d\n\r", tailLength);
		
		// now we have lead and tail, copy to our own buffer
		int sampleBufferSize = (inLength-leadLength-tailLength) * sizeof(float);
		float *sampleBuffer = malloc(sampleBufferSize);
		int copyOffset = leadLength;
		int i=0;
		while (copyOffset < inLength-tailLength)
			sampleBuffer[i++] = inBuffer[copyOffset++];
		
		// save recording as wave file to users profile directory
		McpAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
		NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
		NSString *documentsDirectory = [paths objectAtIndex:0];
		NSString *profileDirectory = [documentsDirectory stringByAppendingPathComponent:appDelegate.currentUserProfile.name];
		NSString *filename = [profileDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.wav", recordedSyllable.syllableName]];
		[self writeBuffer:sampleBuffer ofSize:sampleBufferSize ToWavFile:filename];
		
		// free sample
		free(sampleBuffer);
		
/*		
		// add new training data to phoneme if it doesn't exist
		PhonemeData *thisData = (PhonemeData*)[trainingData.phonemeData objectForKey:recordedPhoneme.phonemeName];
		if (thisData == nil) {
			PhonemeData *newData = [[PhonemeData alloc] init];
			[trainingData.phonemeData setObject:newData forKey:recordedPhoneme.phonemeName];
			thisData = newData;
			[newData release];
		}
		
		// add new sample to data
		PhonemeSample *newSample = [[PhonemeSample alloc] initWithBuffer:sampleBuffer ofSize:sampleBufferSize/sizeof(float)];
		[thisData.phonemeSamples addObject:newSample];
		[newSample release];
		*/
		
		// now play back audio
		// do we have data?  if so, play back audio
		if (myRecorder.recordState.recordBufferPos > 0)
		{
			[[NSString stringWithFormat:@"Playing back buffer of size %d\n", myRecorder.recordState.recordBufferPos] writeToFile:@"/dev/stdout" atomically:NO];
			
            // playback
            [myPlayer startPlayingBuffer:myRecorder.recordState.recordBuffer ofSize:myRecorder.recordState.recordBufferPos];
            
            /*
			isPlaying = YES;
			playBufferPos = 0;
			
			// fill the first three buffers and queue
			for (int i=0; i < NUM_BUFFERS; i++)
			{
				if (playBufferPos < recordBufferPos)
				{
					memcpy(playState.buffers[i]->mAudioData, recordBuffer+playBufferPos, 4000*4);
					playState.buffers[i]->mAudioDataByteSize = 4000*4;
					playBufferPos += 4000*4;
					OSStatus status2=AudioQueueEnqueueBuffer(playState.queue, playState.buffers[i], 0, NULL);
					[[NSString stringWithFormat:@"Play queue buffer result code %d\n", status2] writeToFile:@"/dev/stdout" atomically:NO];
					playBufferPos = playBufferPos;
				}
			}
			
			// start playback
			//			AudioQueueSetParameter(playState.queue, kAudioQueueParam_Volume, 1.0);
			//			[[NSString stringWithFormat:@"Start audio playback\n"] writeToFile:@"/dev/stdout" atomically:NO];
			
			// wait for queue to stop, then call my proc
			OSStatus status = AudioQueueStart(playState.queue, NULL);
             */
		}
		
		// no longer analyzing
        [myRecorder pauseRecording];
//		isAnalyzing = NO;
	}
	else {
        switch (myRecorder.recordState.state) {
            case AUDIORECORDER_STATE_RECORDING:
                statusLabel.text = @"Status: Recording...";
                break;
            case AUDIORECORDER_STATE_LISTENING:
				statusLabel.text = @"Listening...";
                break;
            case AUDIORECORDER_STATE_PAUSED:
				statusLabel.text = @"Waiting...";
                break;
            default:
                break;
        }
	}
}

-(void)writeBuffer:(float*)audioBuffer ofSize:(int)bufferSize ToWavFile:(NSString *)inFilename {
	// if file exists, delete old copy
	if ([[NSFileManager defaultManager] fileExistsAtPath:inFilename]) {
		NSError *myError = [NSError alloc];
		[[NSFileManager defaultManager] removeItemAtPath:inFilename error:&myError];
	}
	
	// set up format
	AudioStreamBasicDescription dataFormat;
	dataFormat.mSampleRate = 16000;
	dataFormat.mFormatID = kAudioFormatLinearPCM;
	dataFormat.mFramesPerPacket = 1;
	dataFormat.mChannelsPerFrame = 1;
	dataFormat.mBytesPerFrame = 4;
	dataFormat.mBytesPerPacket = 4;
	dataFormat.mBitsPerChannel = 32;
	dataFormat.mReserved = 0;
	dataFormat.mFormatFlags = kLinearPCMFormatFlagIsFloat|kLinearPCMFormatFlagIsPacked;
	
	// get c path names
	char *path1 = malloc(1024);
	[inFilename getCString:path1 maxLength:1024 encoding:NSUTF8StringEncoding];
	
	// write first audio file
	AudioFileID fileId1;
	CFURLRef fileURL1 = CFURLCreateFromFileSystemRepresentation(NULL, (UInt8*)path1, strlen(path1), false);
	UInt32 sizeWritten = bufferSize;
	AudioFileCreateWithURL(fileURL1, kAudioFileWAVEType, &dataFormat, kAudioFileFlags_EraseFile, &fileId1);
	AudioFileWriteBytes(fileId1, false, 0, &sizeWritten, (void*)audioBuffer);
	AudioFileClose(fileId1);
	
	// free buffers
	free(path1);
}

-(void) normalizeFloatArray:(float*)inArray ofSize:(int)inSize {
	float max = 0;
	float min = INT_MAX;
	for (int i=0; i < inSize; i++) {
		if (fabs(inArray[i]) > max)
			max = fabs(inArray[i]);
		if (fabs(inArray[i]) < min)
			min = fabs(inArray[i]);
	}
	for (int i=0; i < inSize; i++) {
		if (inArray[i] > 0)
			inArray[i] = fabs(inArray[i]) / max;
		else 
			inArray[i] = 0 - (fabs(inArray[i]) / max);
	}
}

/*
void AudioInputCallback3(void *inUserData, AudioQueueRef inAQ, AudioQueueBufferRef inBuffer, const AudioTimeStamp *inStartTime, UInt32 inNumberPacketDescriptions, const AudioStreamPacketDescription *inPacketDescs)
{
	// pointer to buffer
	float* test=(float*)inBuffer->mAudioData;
	int recordedBytes = inBuffer->mAudioDataByteSize;
	int recordedFloats = inBuffer->mAudioDataByteSize/4;
	
	// if any of buffer is about 10%, start recording
	if (isListening && !isRecording)
	{
		for (int i=0; i < recordedFloats; i++)
			if (test[i] > 0.1f)
			{
				isRecording = YES;
				recordBufferPos = 0;
				break;
			}
	}
	
	// check for silence
	BOOL isSilent=YES;
	if (isListening && isRecording)
	{
		for (int i=0; i < recordedFloats; i++)
			if (test[i] > 0.1f)
			{
				isSilent = NO;
				break;
			}
	}
	
	// if we are recording, copy buffer to our recording buffer
	if (isListening && isRecording) {
		memcpy(recordBuffer+recordBufferPos, test, recordedBytes);
		recordBufferPos += recordedBytes;
		printf("copied to record buffer from buffer %p, pos now %d\r\n", test, recordBufferPos);
	}
	
	// stop recording and compare
	if (isListening && isRecording && isSilent) {
		// stop recording
		isRecording = NO;
		isListening = NO;
		isAnalyzing = YES;
		OSStatus status1 = AudioQueuePause(recordState.queue);
		if (status1 != 0) {
			NSException *exception = [NSException exceptionWithName:@"Audio Error" reason:@"Cannot stop audio record queue" userInfo:nil];
			@throw exception;
		}
		
		// do we have data?
		if (recordBufferPos > 0)
		{
			// check pronunciation
		}
	}
	
	// re-queue buffer
	RecordState *recordState = (RecordState*)inUserData;
	AudioQueueEnqueueBuffer(recordState->queue, inBuffer, 0, NULL);
}
*/

-(IBAction)beginButtonPressed:(id)sender {
	[self doNextAction];
}

-(void)doNextAction {
	// get app delegate
	McpAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];

	// first, create profile directory if it doesn't exist
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *profileDirectory = [documentsDirectory stringByAppendingPathComponent:appDelegate.currentUserProfile.name];
	if (![[NSFileManager defaultManager] fileExistsAtPath:profileDirectory]) {
		[[NSFileManager defaultManager] createDirectoryAtPath:profileDirectory attributes:nil];
	}
	
	// find next syllable that needs recording
	for (int i=0; i < trainingData.syllables.count; i++) {
		// if wave file doesn't exist
		Syllable *thisSyllable = [trainingData.syllables objectAtIndex:i];
		NSString *filename = [profileDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.wav", thisSyllable.syllableName]];
		if (![[NSFileManager defaultManager] fileExistsAtPath:filename]) {
			// update name and start recording
			recordedSyllable = thisSyllable;
			self.sayLabel.text = thisSyllable.syllableName;
			
			// start recording
            [myRecorder startRecording];
//			isListening = YES;
//			OSStatus status = AudioQueueStart(recordState.queue, false);
			
			// exit loop
			break;
		}
	}
}

-(IBAction)endButtonPressed:(id)sender {
	// use training data to train neural network
	McpAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
	NeuralNet *myNet = [appDelegate.currentUserProfile getNetForAudioDevice];
	NeuralNet *cpuNet = [appDelegate.currentCpuProfile getNetForAudioDevice];
	
	// get documents and profile directory
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *profileDirectory = [documentsDirectory stringByAppendingPathComponent:appDelegate.currentUserProfile.name];
	NSString *cpuProfileDirectory = [documentsDirectory stringByAppendingPathComponent:appDelegate.currentCpuProfile.name];
	
	// create cpu profile directory if it doesn't exist
	if (![[NSFileManager defaultManager] fileExistsAtPath:cpuProfileDirectory]) {
		[[NSFileManager defaultManager] createDirectoryAtPath:cpuProfileDirectory attributes:nil];
	}
	
	// first work out how many quantize segments (training sets)
	int quantizeTotal = 0;
	int featureCount = 25;
	for (int i=0; i < trainingData.syllables.count; i++) {
		// if wave file exists
		Syllable *thisSyllable = [trainingData.syllables objectAtIndex:i];
		NSString *filename = [profileDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.wav", thisSyllable.syllableName]];
		if ([[NSFileManager defaultManager] fileExistsAtPath:filename]) {
			// create synthesized version of phoneme in temporary directory
			NSString *testText = thisSyllable.syllableKana;
			const char* cc_testText = [testText cStringUsingEncoding:-2147481087];
			NSString *filename = [NSString stringWithFormat:@"/%@.wav", thisSyllable.syllableName];
			NSString *file_path = [cpuProfileDirectory stringByAppendingString:filename];
			const char* cc_file_path=[file_path UTF8String];
			int result;
			if (appDelegate.currentUserProfile.gender == 0)
				result = VT_TextToFile_JPN_Show(VT_FILE_API_FMT_S16PCM_WAVE, (void*)cc_testText, (void*)cc_file_path, -1, 100, 100, 100, 0, -1, -1);
			else
				result = VT_TextToFile_JPN_Misaki(VT_FILE_API_FMT_S16PCM_WAVE, (void*)cc_testText, (void*)cc_file_path, -1, 100, 100, 100, 0, -1, -1);
			if (result != 1) {
				NSException *exception = [NSException exceptionWithName:@"DatabaseError" reason:@"Cannot open speech database" userInfo:nil];
				@throw exception;
			}				
			
			// find size of this sample to get quantize size divide by 2 for convert from 16k to 8k
			int file1SampleCount = [AudioFile getSampleCountForFile:file_path];
			
			// calculate quantize size using fft window size with a little overlap
			int fftWindowSize = [VoiceSample getN];
			fftWindowSize = fftWindowSize/3;
			int quantizeCount = round((float)file1SampleCount/(float)fftWindowSize);
			
			// increment counters
			quantizeTotal += quantizeCount;
		}			
	}

	// allocate memory for training set buffer
	float *cpuDataPointSet = malloc(sizeof(float)*myNet.inputCount);
	float *userDataPointSet = malloc(sizeof(float)*myNet.inputCount);
	double *inputSet = malloc(sizeof(double)*myNet.inputCount*quantizeTotal);
	double *outputSet = malloc(sizeof(double)*myNet.outputCount*quantizeTotal);
	double *cpuInputSet = malloc(sizeof(double)*myNet.inputCount*quantizeTotal);
	double *cpuOutputSet = malloc(sizeof(double)*myNet.outputCount*quantizeTotal);
	
	// create data set for clustering
	DataSet *cpuDataSet = [[DataSet alloc] init];
	DataSet *userDataSet = [[DataSet alloc] init];
	NSMutableArray *dataArray = [NSMutableArray arrayWithCapacity:10];
	
	// loop through syllables in training data
	int setCount = 0;
	int setCount2 = 0;
	int *trainList = malloc(sizeof(int)*4);
	trainList[0] = 6;
	trainList[1] = 7;
	trainList[2] = 1;
	trainList[3] = 2;
//	for (int tl=0; tl < 2; tl++) {
//		int i = trainList[tl];
//	for (int i=0; i < 20; i++) {
	for (int i=0; i < trainingData.syllables.count; i++) {
		// train network on first 6 syllables (vowels) first
//		if (i > 5) {
//			[myNet trainNetworkWithInputSet:inputSet ofSize:setCount andOutputSet:outputSet];
//			[cpuNet trainNetworkWithInputSet:cpuInputSet ofSize:setCount andOutputSet:cpuOutputSet];
//		}
		
		// if wave file exists
		Syllable *thisSyllable = [trainingData.syllables objectAtIndex:i];
		NSString *filename = [profileDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.wav", thisSyllable.syllableName]];
		if ([[NSFileManager defaultManager] fileExistsAtPath:filename]) {
			// get data for synthesized file
			NSString *file1Path = [NSString stringWithFormat:@"%@/%@.wav", cpuProfileDirectory, thisSyllable.syllableName];
			int file1SampleCount = [AudioFile getSampleCountForFile:file1Path];
			float *file1Data = malloc(sizeof(float)*file1SampleCount);
			[AudioFile getDataIntoFloatBuffer:file1Data ofLength:file1SampleCount fromFile:file1Path];
			
			// get data for user sample
			NSString *file2Path = [NSString stringWithFormat:@"%@/%@.wav", profileDirectory, thisSyllable.syllableName];
			int file2SampleCount = [AudioFile getSampleCountForFile:file2Path];
			float *file2Data = malloc(sizeof(float)*file2SampleCount);
			[AudioFile getDataIntoFloatBuffer:file2Data ofLength:file2SampleCount fromFile:file2Path];
			
			// normalize sample buffers
			[self normalizeFloatArray:file1Data ofSize:file1SampleCount];
			[self normalizeFloatArray:file2Data ofSize:file2SampleCount];
			
			// calculate quantize size using fft window size with a little overlap
			// then round up to make divisible my compare segment size
			int fftWindowSize = [VoiceSample getN];
			fftWindowSize = fftWindowSize/3;
			int quantizeCount = round((float)file1SampleCount/(float)fftWindowSize);
			
			// create voice samples
			VoiceSample *synthSample = [[VoiceSample alloc] initWithBuffer:file1Data ofLength:file1SampleCount quantizeTo:quantizeCount trim:YES useCms:NO useCepstralMeans:nil newCepstralMeans:nil];
			VoiceSample *mySample = [[VoiceSample alloc] initWithBuffer:file2Data ofLength:file2SampleCount quantizeTo:quantizeCount trim:YES useCms:NO useCepstralMeans:nil newCepstralMeans:nil];
			
			// write out synth sample to image
			[synthSample writeToImageWithFilename:[NSString stringWithFormat:@"%@.png", thisSyllable.syllableName] alternateBuffer:nil];
			[synthSample writeToWavFile:[NSString stringWithFormat:@"%@.wav", thisSyllable.syllableName]];
			[mySample writeToImageWithFilename:[NSString stringWithFormat:@"my_%@.png", thisSyllable.syllableName] alternateBuffer:nil];
			[mySample writeToWavFile:[NSString stringWithFormat:@"my_%@.wav", thisSyllable.syllableName]];
			
			// find positions of phonemes
			int *cpuPhonemeBreaks = [synthSample getPhonemeBreakPositionsForCount:thisSyllable.phonemes.count];
			int *myPhonemeBreaks = [mySample getPhonemeBreakPositionsForCount:thisSyllable.phonemes.count];
			
			
			if (i == 20) {
				int a=0;
			}
			
			// loop through phonemes here and add training data for each one
			// at this point, positions should be filled out
			for (int p=0; p < thisSyllable.phonemes.count; p++) {
				// get sample data
				float *mySampleData = [mySample getFeatureBuffer];
				float *synthSampleData = [synthSample getFeatureBuffer];
				
				// find start and end points
				int cpuPhonemeStart;
				int cpuPhonemeEnd;
				int myPhonemeStart;
				int myPhonemeEnd;
				[self getStartEndForPhonemeIndex:p inSyllable:thisSyllable usingBreaks:cpuPhonemeBreaks andQuantizeCount:quantizeCount intoStart:&cpuPhonemeStart andEnd:&cpuPhonemeEnd];
				[self getStartEndForPhonemeIndex:p inSyllable:thisSyllable usingBreaks:myPhonemeBreaks andQuantizeCount:quantizeCount intoStart:&myPhonemeStart andEnd:&myPhonemeEnd];
				
//				int quantizeStart = (float)quantizeCount*thisSyllablePhoneme.startPercent;
//				int quantizeEnd = (float)quantizeCount*thisSyllablePhoneme.endPercent;
				
				// make sure we don't start and end on same point
//				if (quantizeStart != 0) {
//					quantizeStart ++;
//				}
				
				// only train phonemes aiueo for now
//				if (thisPhoneme.featureIndex < 5) {
				
				// build input set for synth data
				SyllablePhoneme *thisSyllablePhoneme = [thisSyllable.phonemes objectAtIndex:p];
				Phoneme *thisPhoneme = [trainingData.phonemeIndexes objectForKey:thisSyllablePhoneme.phonemeName];
				printf("cpu start=%d, end=%d, user start=%d, end = %d\r\n", cpuPhonemeStart, cpuPhonemeEnd, myPhonemeStart, myPhonemeEnd);
				for (int q=cpuPhonemeStart; q < cpuPhonemeEnd; q++) {
					// input is mfcc's
					for (int m=0; m < myNet.inputCount; m++) {
						cpuDataPointSet[m] = synthSampleData[q*featureCount+m];
						cpuInputSet[setCount*myNet.inputCount+m] = synthSampleData[q*featureCount+m];
					}
					// output is phoneme feature index
					for (int m=0; m < myNet.outputCount; m++)
						cpuOutputSet[setCount*myNet.outputCount+m] = (m == thisPhoneme.featureIndex ? 1 : 0);
					
					// add this data point to data set
					DataPoint * newCpuDataPoint = [[DataPoint alloc] initWithData:cpuDataPointSet count:myNet.inputCount dataSourceIndex:thisPhoneme.featureIndex];
					[cpuDataSet addDataPoint:newCpuDataPoint];
					[dataArray addObject:newCpuDataPoint];
					[newCpuDataPoint release];
					
					// next training set!
					setCount++;
				}
				
				// build input/output set for user data
				for (int q=myPhonemeStart; q < myPhonemeEnd; q++) {
					// input is mfcc's
					for (int m=0; m < myNet.inputCount; m++) {
						userDataPointSet[m] = mySampleData[q*featureCount+m];
						inputSet[setCount2*myNet.inputCount+m] = mySampleData[q*featureCount+m];
					}
					// output is phoneme feature index
					for (int m=0; m < myNet.outputCount; m++)
						outputSet[setCount2*myNet.outputCount+m] = (m == thisPhoneme.featureIndex ? 1 : 0);
					
					// add this data point to data set
					DataPoint * newUserDataPoint = [[DataPoint alloc] initWithData:userDataPointSet count:myNet.inputCount dataSourceIndex:thisPhoneme.featureIndex];
					[userDataSet addDataPoint:newUserDataPoint];
					[newUserDataPoint release];
					
					// next training set!
					setCount2++;
				}
								
//				}
			}
			
			// free data
			free(file1Data);
			free(file2Data);
			free(cpuPhonemeBreaks);
			free(myPhonemeBreaks);
		}
	}
	
	//
	// implement clustering algorithm here to reduce number of features
	//
	
	// firstly cluster the mfcc features
	appDelegate.currentCpuProfile.clusterSet = [[ClusterSet alloc] initWithDataSet:cpuDataSet eps:0.1f minPts:1];
	appDelegate.currentUserProfile.clusterSet = [[ClusterSet alloc] initWithDataSet:userDataSet eps:0.1f minPts:1];
	
	// test it
	/*
	for (int a=0; a < dataArray.count; a++) {
		DataPoint *testPoint = [dataArray objectAtIndex:a];
		Cluster *thisCluster = [appDelegate.currentCpuProfile.clusterSet getClusterForNewDataPoint:testPoint];
		if (thisCluster != nil) {
			for (int i=0; i < 10; i++) {
				float chance = [thisCluster getPercentChanceOfFeature:i];
				printf("chance=%f\r\n",chance);
			}
		}
	}
	*/
	
	// release the data set
	free(cpuDataPointSet);
	[cpuDataSet release];
	free(userDataPointSet);
	[userDataSet release];
	
	
	// now train the networks
//	[myNet trainNetworkWithInputSet:inputSet ofSize:setCount andOutputSet:outputSet];
//	[cpuNet trainNetworkWithInputSet:cpuInputSet ofSize:setCount andOutputSet:cpuOutputSet];
	
	// free training data
	free(inputSet);
	free(outputSet);
	free(cpuInputSet);
	free(cpuOutputSet);
}

-(void)getStartEndForPhonemeIndex:(int)inPhonemeIndex inSyllable:(Syllable*)inSyllable usingBreaks:(int*)inPhonemeBreaks andQuantizeCount:(int)inQuantizeCount intoStart:(int*)inStart andEnd:(int*)inEnd {
	if (inPhonemeIndex == 0) {
		*inStart = 0;
		if (inSyllable.phonemes.count == 1)
			*inEnd = inQuantizeCount;
		else
			*inEnd = inPhonemeBreaks[inPhonemeIndex]+1;
	}
	else {
		if (inPhonemeIndex == inSyllable.phonemes.count-1) {
			*inEnd = inQuantizeCount;
			if (inSyllable.phonemes.count == 1)
				*inStart = 0;
			else
				*inStart = inPhonemeBreaks[inPhonemeIndex-1]+1;
		}
		else {
			*inStart = inPhonemeBreaks[inPhonemeIndex-1]+1;
			*inEnd = inPhonemeBreaks[inPhonemeIndex]+1;
		}
	}
}


-(IBAction)backButtonPressed:(id)sender {
	// find last recorded sample in users profile directory
		// delete sample
}

-(IBAction)clearButtonPressed:(id)sender {
	// delete all recorded samples from users profile directory
}

/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillDisappear:(BOOL)animated {
	// invalidate timer here, because it contains a reference to us!
	[labelTimer invalidate];
}

- (void)dealloc {
    // release audio
    [myPlayer release];
    [myRecorder release];
    
	// free buffers and queue
//	AudioQueueStop(recordState.queue, TRUE);
//	AudioQueueStop(playState.queue, TRUE);
//	AudioQueueDispose(recordState.queue, TRUE);
//	AudioQueueDispose(playState.queue, TRUE);
//	free(recordBuffer);
	
	[labelTimer release];
	[beginButton release];
	[endButton release];
	[backButton release];
	[clearButton release];
	[sayLabel release];
	[statusLabel release];
    [super dealloc];
}


@end
