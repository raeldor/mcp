//
//  EditDialogueLineCell.h
//  Mcp
//
//  Created by Ray Price on 10/20/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EditDialogueLineCell : UITableViewCell {
    IBOutlet UIWebView *webView;
}

@property (nonatomic, retain) UIWebView *webView;

@end
