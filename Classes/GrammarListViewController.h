//
//  GrammarListViewController.h
//  Mcp
//
//  Created by Ray on 1/7/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EditableListDelegate.h"
#import "Book.h"
#import "Chapter.h"
#import "SelectControllerDelegate.h"

@interface GrammarListViewController : UITableViewController <UITableViewDelegate, UITableViewDataSource, EditableListDelegate, SelectControllerDelegate> {
//    IBOutlet UITableView *browseTableView;
//	IBOutlet UISearchBar *searchBar;
//    IBOutlet UISearchDisplayController *searchController;
    
    /*
	UIButton *keyboardHideButton;
	UIToolbar *searchToolbar;
	UISwitch *romajiSwitch;
	UISwitch *ftsSwitch;*/
    
	NSIndexPath *lastIndexPath;
	NSMutableArray *searchResults;
    
	Chapter *chapter;
	Book *book;
}

/*
@property (nonatomic, retain) UITableView *browseTableView;
@property (nonatomic, retain) UISearchBar *searchBar;
@property (nonatomic, retain) UISearchDisplayController *searchController;
@property (nonatomic, retain) UIToolbar *searchToolbar;
@property (nonatomic, retain) UIButton *keyboardHideButton;
@property (nonatomic, retain) UISwitch *romajiSwitch;
@property (nonatomic, retain) UISwitch *ftsSwitch;
 */
@property (nonatomic, retain) Chapter *chapter;
@property (nonatomic, retain) Book *book;

-(void)setEditableObject:(Chapter *)inChapter inBook:(Book*)inBook;

@end
