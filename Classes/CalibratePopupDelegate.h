//
//  CalibratePopupDelegate.h
//  Mcp
//
//  Created by Ray Price on 10/24/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#ifndef Mcp_CalibratePopupDelegate_h
#define Mcp_CalibratePopupDelegate_h

@protocol CalibratePopupDelegate

-(void)calibrationFinished:(BOOL)didCancel;

@end


#endif
