//
//  NewTrainingData.h
//  Mcp
//
//  Created by Ray Price on 9/17/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface NewTrainingData : NSObject <NSCoding> {
    NSMutableDictionary *trainingSamples;
    NSMutableDictionary *phonemeTrainingData; // PhonemeTrainingData
}

@property (nonatomic, retain) NSMutableDictionary *trainingSamples;
@property (nonatomic, retain) NSMutableDictionary *phonemeTrainingData;

@end
