//
//  SectionBrowserViewController.h
//  HF Sensei
//
//  Created by Ray Price on 2/23/13.
//
//

#import <UIKit/UIKit.h>
#import "EditSectionViewControllerDelegate.h"
#import "FlashDeck.h"
#import "SectionBrowserViewControllerDelegate.h"

@interface SectionBrowserViewController : UITableViewController<EditSectionViewControllerDelegate> {
    FlashDeck *editDeck;
}

-(void)setDeckTo:(FlashDeck*)inDeck;
-(IBAction)readingSegmentValueChanged:(id)sender;
-(IBAction)writingSegmentValueChanged:(id)sender;

@property (nonatomic, assign) id<SectionBrowserViewControllerDelegate> sectionBrowserDelegate;

@end
