//
//  PhonemeData.m
//  Mcp
//
//  Created by Ray on 1/24/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "PhonemeData.h"

@implementation PhonemeData

@synthesize phonemeSamples;

-(id)init {
	if (self = [super init]) {
		// create array
		self.phonemeSamples = [NSMutableArray array];
	}
	return self;
}

- (void)dealloc {
	[phonemeSamples release];
	[super dealloc];
}

@end
