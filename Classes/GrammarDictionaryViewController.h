//
//  GrammarDictionaryViewController.h
//  Mcp
//
//  Created by Ray on 1/7/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EditableListDelegate.h"
//#import "GrammarDictionary.h"

@interface GrammarDictionaryViewController : UITableViewController <UITableViewDelegate, UITableViewDataSource> {
    /*
    IBOutlet UITableView *browseTableView;
	IBOutlet UISearchBar *mySearchBar;
    IBOutlet UISearchDisplayController *searchController;
    
	UIButton *keyboardHideButton;
	UIToolbar *searchToolbar;
	UISwitch *romajiSwitch;
	UISwitch *ftsSwitch;*/
    
	NSIndexPath *lastIndexPath;
    NSMutableArray *grammarNotes;
	NSMutableArray *searchResults;
}

/*
@property (nonatomic, retain) UITableView *browseTableView;
@property (nonatomic, retain) UISearchBar *mySearchBar;
@property (nonatomic, retain) UISearchDisplayController *searchController;
@property (nonatomic, retain) UIToolbar *searchToolbar;
@property (nonatomic, retain) UIButton *keyboardHideButton;
@property (nonatomic, retain) UISwitch *romajiSwitch;
@property (nonatomic, retain) UISwitch *ftsSwitch;
 */

@end
