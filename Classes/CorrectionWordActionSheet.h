//
//  CorrectionWordActionSheet.h
//  Mcp
//
//  Created by Ray Price on 1/17/13.
//
//

#import <UIKit/UIKit.h>
#import "JapaneseDictionary.h"
#import "SampleEntryWord.h"

@interface CorrectionWordActionSheet : UIActionSheet <UIPickerViewDelegate, UIPickerViewDataSource> {
    SampleEntryWord *sampleEntryWord;
    JapaneseWord *dictionaryWord;
    NSMutableArray *correctionSelections;
}

-(id)initWithTitle:(NSString*)inTitle delegate:(id<UIActionSheetDelegate>)inDelegate andSampleWord:(SampleEntryWord*)inSampleWord;
-(void)didPressPickerCancel:(id)sender;
-(void)didPressPickerDone:(id)sender;

@property (nonatomic, assign) JapaneseWord *selectedWord;

@end
