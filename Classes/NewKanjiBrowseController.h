//
//  NewKanjiBrowseController.h
//  HFSensei
//
//  Created By Ray on 3/15/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KanjiDictionary.h"

@interface NewKanjiBrowseController : UITableViewController <UITableViewDelegate, UITableViewDataSource, UISearchResultsUpdating> {
    KanjiDictionary *kanjiDictionary;
//    UIActivityIndicatorView *activityIndicator;
//    UIActivityIndicatorView *activityIndicator2;
	
	// search results
    NSTimer *searchTimer;
    BOOL performingSearch;
//    BOOL isSearchResultsLoaded; // loaded yet?
    BOOL filterIsActive; // when displaying filtered results without showing search bar
	NSMutableArray *searchResults; // array of indexes into the actual list
    
    // save keyboard height
//    int keyboardHeight;
}

@property (nonatomic, assign) UISearchController *searchController;

-(NSUInteger)numberOfEntriesInGroup:(NSUInteger)groupIndex;
//-(void)updateSearchResultsUsingSearchController:(UISearchDisplayController*)controller;

@end
