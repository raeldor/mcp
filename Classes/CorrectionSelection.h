//
//  CorrectionSelection.h
//  Mcp
//
//  Created by Ray Price on 1/17/13.
//
//

#import <Foundation/Foundation.h>
#import "JapaneseDictionary.h"

@interface CorrectionSelection : NSObject {
}

-(id)initWithDictionaryEntry:(JapaneseWord*)inWord;

@property (nonatomic, retain) NSString *kanji;
@property (nonatomic, retain) NSString *kana;
@property (nonatomic, assign) int dictionaryId;
@property (nonatomic, retain) NSMutableArray *senses; // array of string of senses separated by semi colons

@end
