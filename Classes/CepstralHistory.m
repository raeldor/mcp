//
//  CepstralHistory.m
//  Mcp
//
//  Created by Ray on 3/30/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "CepstralHistory.h"

@implementation CepstralHistory

-(id)initWithMfccCount:(int)inMfccCount {
	if (self = [super init]) {
		// initialize members
		mfccCount = inMfccCount;
		historyLength = 512;
		totalLength = 0;
		historyData = malloc(sizeof(float)*mfccCount*historyLength);
		
		// set data to zero
		for (int i=0; i < mfccCount*historyLength; i++)
			historyData[i] = 0.0f;
	}
	return self;
}

-(void)addNewMfccData:(float*)inData ofLength:(int)inLength {
	// add to total length
	totalLength += inLength;
	if (totalLength > historyLength)
		totalLength = historyLength;
	
	// shuffle data to the left
	for (int i=inLength; i < historyLength; i++) {
		for (int m=0; m < mfccCount; m++) {
			historyData[(i-inLength)*mfccCount+m] = historyData[i*mfccCount+m];
		}
	}
	
	// add new data
	for (int i=0; i < (inLength > historyLength ? historyLength:inLength); i++) {
		for (int m=0; m < mfccCount; m++) {
			historyData[(historyLength-inLength+i)*mfccCount+m] = inData[i*mfccCount+m];
		}
	}
}

-(float*)getCepstralMeanArray {
	// loop through and calculate cepstral mean array and return
	float *cepstralMeans = malloc(sizeof(float)*mfccCount);
    float total = 0.0f;
	for (int m=0; m < mfccCount; m++) {
		cepstralMeans[m] = 0.0f;
		for (int i=0; i < totalLength; i++) {
			cepstralMeans[m] += historyData[(historyLength-i)*mfccCount+m];
            total += historyData[(historyLength-i-1)*mfccCount+m];
		}
		cepstralMeans[m] /= totalLength;
	}
    
    // this is wrong!  see http://sourceforge.net/projects/cmusphinx/forums/forum/5470/topic/4076185
    // try calc as total across all channels
//	for (int m=0; m < mfccCount; m++)
//        cepstralMeans[m] = total / (mfccCount*totalLength);
	
	return cepstralMeans;
}

-(void)dealloc {
	free(historyData);
	[super dealloc];
}

@end
