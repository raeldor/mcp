//
//  ActorListViewController.h
//  Mcp
//
//  Created by Ray on 10/21/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EditableListDelegate.h"

@interface ActorListViewController : UITableViewController <UITableViewDelegate, UITableViewDataSource, EditableListDelegate> {
	NSMutableArray *actorList; // list of NSString*
	NSIndexPath *lastIndexPath;
	NSString *oldActorName;
}

@property (nonatomic, retain) NSMutableArray *actorList;

-(void)populateActorList;

@end
