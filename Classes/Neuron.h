//
//  Neuron.h
//  Mcp
//
//  Created by Ray on 2/6/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Neuron : NSObject <NSCoding> {
	int inputCount;
	double *weights;
	double *lastWeightUpdateValue;
	double activation;
	double errorValue;
}

-(id)initWithInputCount:(int)inCount;
-(void)dealloc;

@property (nonatomic, assign) int inputCount;
@property (nonatomic, assign) double *weights;
@property (nonatomic, assign) double *lastWeightUpdateValue;
@property (nonatomic, assign) double activation;
@property (nonatomic, assign) double errorValue;

@end
