//
//  AudioPlayer.h
//  Mcp
//
//  Created by Ray on 4/18/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#include "AudioToolbox/AudioQueue.h"
#include "AudioToolbox/AudioToolbox.h"
#include "AudioStates.h"

@interface AudioPlayer : NSObject {
    PlayState playState;
}

-(id)init;
-(void)dealloc;
-(int)getPlayBufferPos;
-(void*)getPlayBuffer;
-(BOOL)startPlayingBuffer:(void*)inBuffer ofSize:(int)inBufferSize;

@property (nonatomic, assign) PlayState playState;

@end

void AudioOutputCallback(void *inUserData, AudioQueueRef inAQ, AudioQueueBufferRef inBuffer);

