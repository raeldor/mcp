//
//  CorrectionViewController.h
//  Mcp
//
//  Created by Ray Price on 1/15/13.
//
//

#import <UIKit/UIKit.h>
#import "CorrectionViewControllerDelegate.h"
#import "SampleEntry.h"
#import "CorrectionWordActionSheet.h"
#import "CorrectionMeaningActionSheet.h"

@interface CorrectionViewController : UIViewController <UIWebViewDelegate, UIActionSheetDelegate, UIAlertViewDelegate> {
    IBOutlet UIWebView *sentenceWebView;
    SampleEntry *sampleSentence;
    SampleEntry *originalSentence;
    int selectedWordIndex;
    CorrectionWordActionSheet *wordActionSheet;
    CorrectionMeaningActionSheet *meaningActionSheet;
}

@property (nonatomic, assign) id<CorrectionViewControllerDelegate> delegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil sampleSentence:(SampleEntry*)inSampleSentence;
-(void)didPressCancel:(id)sender;
-(void)didPressSubmit:(id)sender;
-(void)updateHtml;

@end

