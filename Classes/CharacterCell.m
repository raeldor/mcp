//
//  CharacterCell.m
//  Mcp
//
//  Created by Ray on 11/3/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "CharacterCell.h"


@implementation CharacterCell

@synthesize actorImageView;
@synthesize characterNameTextField;
@synthesize actorNameLabel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if ((self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])) {
        // Initialization code
    }
    return self;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
	
    [super setSelected:selected animated:animated];
	
    // Configure the view for the selected state
}

- (BOOL)textFieldShouldReturn:(UITextField *)theTextField {
	[theTextField resignFirstResponder];
	return YES;
}

- (void)dealloc {
	[actorImageView release];
	[characterNameTextField release];
	[actorNameLabel release];
    [super dealloc];
}


@end
