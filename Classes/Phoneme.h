//
//  Phoneme.h
//  Mcp
//
//  Created by Ray on 1/24/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Phoneme : NSObject {
	NSString *phonemeName;
	int featureIndex;
}

-(id)initWithName:(NSString*)name andFeatureIndex:(int)index;
-(id)initWithName:(NSString*)inName andDictionaryEntry:(NSDictionary*)inDict;
-(void)dealloc;

@property (nonatomic, retain) NSString *phonemeName;
@property (nonatomic, assign) int featureIndex;

@end
