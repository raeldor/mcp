//
//  TrainingWordPhoneme.m
//  Mcp
//
//  Created by Ray Price on 11/20/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "TrainingWordPhoneme.h"

@implementation TrainingWordPhoneme

@synthesize phonemeName;
@synthesize lengthPercent;

-(id)initWithDictionaryEntry:(NSDictionary*)inDict {
	if (self = [super init]) {
        self.phonemeName = [inDict objectForKey:@"PhonemeName"];
        self.lengthPercent = [[inDict objectForKey:@"LengthPercent"] floatValue];
	}
	return self;
}

- (void)dealloc {
	[phonemeName release];
	[super dealloc];
}

@end
