//
//  BufferHistory.h
//  Mcp
//
//  Created by Ray Price on 9/8/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface BufferHistory : NSObject {
	int bufferHeight;
	int historyLength;
	int totalLength;
	float *historyData;
}

-(id)initWithBufferHeight:(int)inBufferHeight;
-(void)dealloc;
-(void)addNewData:(float*)inData ofLength:(int)inLength;
-(float*)getRowTotals;
-(float*)getDataStart;

@property (nonatomic, assign) int totalLength;

@end
