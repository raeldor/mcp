//
//  EditHtmlCell.m
//  Mcp
//
//  Created by Ray on 1/7/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "EditHtmlCell.h"
#import "TextFunctions.h"

@implementation EditHtmlCell

@synthesize editPreviewControl;
@synthesize editTextView;
@synthesize previewWebView;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if ((self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])) {
        // Initialization code
    }
    return self;
}

-(void)editPreviewChanged:(id)sender {
	// if we are switching to edit mode
	if (editPreviewControl.selectedSegmentIndex == 1) {
		// set web view text
//		NSString *htmlString = [NSString stringWithString:@"<html><style>span.first {color:white;text-shadow:1px 1px 0px #000;font-family='helvetica';font-size:17px;font-weight:bold;}</style><style>span.second {color:gray;text-shadow:1px 1px 0px #000;font-family='helvetica';font-size:17px;font-weight:bold;}</style><body><p>"];
		NSString *htmlString = [NSString stringWithFormat:@"<html><body><div contenteditable=\"true\">%@</div></body></html>", editTextView.text];
//		NSString *htmlString = [NSString stringWithFormat:@"<html><div id=\"content\" contenteditable=\"true\"><body><p>%@</p></body></div></html>", editTextView.text];
//        htmlString = [TextFunctions convertHtml:htmlString toHtmlAs:1];
		[previewWebView loadHTMLString:htmlString baseURL:nil];
		
		// hide text view, show web view
		previewWebView.hidden = NO;
		editTextView.hidden = YES;
	}
	else {
		// show text view again
		editTextView.hidden = NO;
		previewWebView.hidden = YES;
	}

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
	
    [super setSelected:selected animated:animated];
	
    // Configure the view for the selected state
}


- (void)dealloc {
	[editPreviewControl release];
	[editTextView release];
	[previewWebView release];
    [super dealloc];
}


@end
