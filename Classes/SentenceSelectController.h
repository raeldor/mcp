//
//  SentenceSelectController.h
//  HF Sensei
//
//  Created by Ray on 4/5/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewSentenceBrowseController.h"
#import "SampleEntry.h"
#import "BrowserSelectDelegate.h"

@interface SentenceSelectController : NewSentenceBrowseController <UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate, UISearchDisplayDelegate> {
    // last selected
    UITableView *lastSelectedTableView;
	NSUInteger lastSelectedSection;
	NSUInteger lastSelectedRow;
	
	// selected sentences
	NSMutableArray *selectedSentences;
}

-(int)getSelectedIndexForSample:(SampleEntry*)inWord;
-(IBAction)selectButtonClick:(id)sender;
-(IBAction)cancelButtonClick:(id)sender;

@property (nonatomic, assign) id<BrowserSelectDelegate> selectDelegate;

@end

