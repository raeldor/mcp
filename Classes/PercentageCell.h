//
//  PercentageCell.h
//  Mcp
//
//  Created by Ray on 4/22/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PercentageCell : UITableViewCell {
    IBOutlet UILabel *textLabel;
    IBOutlet UILabel *percentLabel;
	IBOutlet UISlider *percentSlider;
}

@property (nonatomic, retain) UILabel *textLabel;
@property (nonatomic, retain) UILabel *percentLabel;
@property (nonatomic, retain) UISlider *percentSlider;

-(IBAction)sliderChanged:(id)sender;


@end
