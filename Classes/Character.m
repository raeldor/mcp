//
//  Character.m
//  Mcp
//
//  Created by Ray on 10/23/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "Character.h"


@implementation Character

@synthesize characterName;
@synthesize actorName;

-(id)init {
	if ((self = [super init])) {
	}
	return self;
}

-(id)initWithCoder:(NSCoder*)coder {
	if ((self = [super init])) {
		self.characterName = [coder decodeObjectForKey:@"CharacterName"];
		self.actorName = [coder decodeObjectForKey:@"ActorName"];
	}
	return self;
}

-(void)encodeWithCoder:(NSCoder*)coder {
	[coder encodeObject:characterName forKey:@"CharacterName"];
	[coder encodeObject:actorName forKey:@"ActorName"];
}

-(id)copyWithZone:(NSZone *)zone {
    Character *newCharacter = [[Character alloc] init];
    newCharacter.characterName = [[characterName copy] autorelease];
    newCharacter.actorName = [[actorName copy] autorelease];
    return newCharacter;
}

-(void)dealloc {
	[characterName release];
	[actorName release];
	[super dealloc];
}

@end
