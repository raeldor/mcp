//
//  DataSet.h
//  Mcp
//
//  Created by Ray on 3/20/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DataPoint.h"

@interface DataSet : NSObject {
	NSMutableSet *dataPoints;
}

-(id)init;
-(void)dealloc;
-(void)addDataPoint:(DataPoint*)inDataPoint;
-(DataSet*)getNeighborsOfPoint:(DataPoint*)inDataPoint UsingEps:(float)inEps;
-(DataPoint*)getNextUnvisitedDataPoint;
-(void)addPointsFromDataSet:(DataSet*)inDataSet;

@property (nonatomic, retain) NSMutableSet *dataPoints;

@end
