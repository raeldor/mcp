//
//  GameOptions.h
//  Mcp
//
//  Created by Ray on 3/8/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GameOptions : NSObject <NSCoding, NSCopying> {
	int talkingSpeed; // 50 to 150, 100 = middle
    int playPart;
	bool promptMyPart;
	bool pauseAfterActor;
    int showTextAs; // 0=kanji, 1=furigana, 2=kana, 3=romaji
	float passAccuracy; // percentage
}

-(id)init;
-(void)dealloc;

@property (nonatomic, assign) int talkingSpeed;
@property (nonatomic, assign) int playPart;
@property (nonatomic, assign) bool promptMyPart;
@property (nonatomic, assign) bool pauseAfterActor;
@property (nonatomic, assign) int showTextAs;
@property (nonatomic, assign) float passAccuracy;

@end
