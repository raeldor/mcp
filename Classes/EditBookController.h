//
//  EditBookController.h
//  Mcp
//
//  Created by Ray on 10/25/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Book.h"
#import "EditableListDelegate.h"

@interface EditBookController : UITableViewController <UITextFieldDelegate, UINavigationControllerDelegate, UIActionSheetDelegate, UIImagePickerControllerDelegate, UITableViewDelegate> {
	BOOL newMode;
	Book *book;
	id<EditableListDelegate> editableListDelegate;
	NSIndexPath *lastIndexPath;	
	
	UIPopoverController *popOver; // for ipad compatibility
}

@property (nonatomic, retain) Book *book;
@property (nonatomic, assign) id<EditableListDelegate> editableListDelegate;
@property (nonatomic, retain) UIPopoverController *popOver;

-(void)setEditableObject:(Book*)thisBook isNew:(BOOL)isNew ;
-(BOOL)isOkToSave;
-(void)cancelButtonClick:(id)sender;
-(void)saveButtonClick:(id)sender;
-(void)textFieldDidChange:(NSNotification*)note;

@end
