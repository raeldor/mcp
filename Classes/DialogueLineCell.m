//
//  DialogueLineCell.m
//  Mcp
//
//  Created by Ray on 11/4/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "DialogueLineCell.h"

@implementation DialogueLineCell

@synthesize characterButton;
@synthesize characterLabel;
@synthesize lineWebView;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if ((self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
	
    // Configure the view for the selected state
}

- (void)dealloc {
	[characterButton release];
	[characterLabel release];
	[lineWebView release];
    [super dealloc];
}

@end