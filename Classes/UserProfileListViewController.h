//
//  UserProfileListViewController.h
//  Mcp
//
//  Created by Ray on 4/8/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EditableListDelegate.h"

@interface UserProfileListViewController : UITableViewController <UITableViewDelegate, UITableViewDataSource, EditableListDelegate> {
    NSMutableArray *profileList; // list of NSString*
    NSIndexPath *lastIndexPath;
    NSString *oldProfileName;
}

@property (nonatomic, retain) NSMutableArray *profileList;

-(void)populateProfileList;

@end
