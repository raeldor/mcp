//
//  ClusterSet.h
//  Mcp
//
//  Created by Ray on 3/20/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DataSet.h"
#import "Cluster.h"

@interface ClusterSet : NSObject {
	NSMutableArray *clusters;
	float eps;
}

-(id)initWithDataSet:(DataSet*)inDataSet eps:(float)inEps minPts:(int)inMinPts;
//-(bool)containsDataPoint:(DataPoint*)inDataPoint;
-(Cluster*)getClusterForNewDataPoint:(DataPoint*)inDataPoint;
-(Cluster*)getClosestClusterToDataPoint:(DataPoint*)inDataPoint;
-(Cluster*)getNewCluster;
-(void)dealloc;

@property (nonatomic, retain) NSMutableArray *clusters;
@property (nonatomic, assign) float eps;

@end
