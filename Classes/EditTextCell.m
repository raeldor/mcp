//
//  NameCell.m
//  Mcp
//
//  Created by Ray on 10/19/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "EditTextCell.h"


@implementation EditTextCell

@synthesize myTextField;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if ((self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {

    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (void)dealloc {
	[myTextField release];
    [super dealloc];
}


@end
