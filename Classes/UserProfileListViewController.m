//
//  UserProfileListViewController.m
//  Mcp
//
//  Created by Ray on 4/8/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "UserProfileListViewController.h"
#import "EditUserProfileController.h"
#import "UserProfile.h"
#import "McpAppDelegate.h"

@implementation UserProfileListViewController

@synthesize profileList;

#pragma mark -
#pragma mark View lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
	// set title
	self.title = @"User Profiles";
    
	// allow selecting during editing and not editing (to select profile)
	self.tableView.allowsSelection = TRUE;
	self.tableView.allowsSelectionDuringEditing = TRUE;
	
	// add edit button
	self.navigationItem.rightBarButtonItem = self.editButtonItem;
	
	// search for deck archive files
	[self populateProfileList];
}

-(void)populateProfileList {
	// create new list of backgrounds
	self.profileList = [UserProfile getListOfFilesWithExtension:@"profile"];
}

#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	if (self.editing)
		return profileList.count+1;
	else
	{
		if (profileList.count == 0)
			return 1;
		else
			return profileList.count;
	}
}

//-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
//	return 120;
//}

// Customize the appearance of table view cells.
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	// get regular table cell
    static NSString *CellIdentifier = @"UserProfileListCell";
    UITableViewCell *cell = (UITableViewCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
		cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
	
	// default cell config
	cell.accessoryType = UITableViewCellAccessoryNone;
	cell.editingAccessoryType = UITableViewCellAccessoryNone;
	cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
	// if no cells, display 'press to add' message
	if (profileList.count == 0 && !self.editing) {
		cell.textLabel.text = @"Press Edit Button To Create New User Profile";
		cell.textLabel.textColor = [UIColor lightGrayColor];
		cell.textLabel.adjustsFontSizeToFitWidth = YES;
	}
	else {
		// Set up the cell...
		NSUInteger row = [indexPath row];
		if (row < profileList.count) {
			cell.textLabel.text = [profileList objectAtIndex:row];
            cell.textLabel.textColor = [UIColor blackColor];
			cell.editingAccessoryType = UITableViewCellAccessoryDisclosureIndicator;
			cell.selectionStyle = UITableViewCellSelectionStyleBlue;
		}
		else {
			cell.textLabel.text = @"Add New";
			cell.textLabel.textColor = [UIColor lightGrayColor];
			cell.selectionStyle = UITableViewCellSelectionStyleBlue;
		}
	}
	
    return cell;
}

// set checkmarks here
- (UITableViewCellAccessoryType)tableView:(UITableView *)tableView accessoryTypeForRowWithIndexPath:(NSIndexPath *)indexPath {
	// get application delegate
    McpAppDelegate *appDelegate = (McpAppDelegate*)[[UIApplication sharedApplication] delegate];
	
	// should card be shown?
    if (self.editing)
        return UITableViewCellAccessoryNone;
    else {
        if (profileList.count == 0)
            return UITableViewCellAccessoryNone;
        else {
            NSString *thisProfileName = [profileList objectAtIndex:[indexPath row]];
            if ([thisProfileName isEqualToString:appDelegate.currentUserProfile.name])
                return UITableViewCellAccessoryCheckmark;
            else
                return UITableViewCellAccessoryNone;
        }
    }
}

//editing mode changed
- (void)setEditing:(BOOL)editing animated:(BOOL)animated {
	// add/remove placeholder items
	NSMutableArray *indexPaths = [NSMutableArray array];
	[indexPaths addObject:[NSIndexPath indexPathForRow:profileList.count inSection:0]];
	[self.tableView beginUpdates];
	[super setEditing:editing animated:animated];
	[self.tableView setEditing:editing animated:YES];
	if (editing) {
		// delete 'press edit' message
		if (profileList.count == 0)
			[self.tableView deleteRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationTop];
		
		// Show the placeholder rows and hide back button
		[self.tableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationTop];
		self.navigationItem.hidesBackButton = YES;
	}
	else
	{
		// Hide the placeholder rows and show back button again
		[self.tableView deleteRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationTop];
		self.navigationItem.hidesBackButton = NO;
		
		// show 'press edit' message
		if (profileList.count == 0)
			[self.tableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationTop];
	}
	[self.tableView endUpdates];
}

-(UITableViewCellEditingStyle)tableView:(UITableView*)tableView editingStyleForRowAtIndexPath:(NSIndexPath*)indexPath {
	// which row are we getting
	NSUInteger row = [indexPath row];
	
	// if we are in edit mode
	if (self.editing) {
		// show delete style and insert for 'add new'
		if (row != profileList.count)
			return UITableViewCellEditingStyleDelete;
		else
			return UITableViewCellEditingStyleInsert;
	}
	return UITableViewCellEditingStyleNone;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    // if we are deleting rows
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
		[UserProfile delete:[profileList objectAtIndex:[indexPath row]] extension:@".profile"];
		[profileList removeObjectAtIndex:[indexPath row]];
        [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:YES];
    }   
    else
		if (editingStyle == UITableViewCellEditingStyleInsert) {
			// edit profile using edit controller
			oldProfileName = nil;
			EditUserProfileController *editProfileController = [[EditUserProfileController alloc] initWithNibName:@"EditUserProfileController" bundle:nil];
			UserProfile *newProfile = [[UserProfile alloc] init];
			[editProfileController setEditableObject:newProfile isNew:YES];
			editProfileController.editableListDelegate = self;
			UINavigationController *secondNavigationController = [[UINavigationController alloc] initWithRootViewController:editProfileController];
			[self.navigationController presentModalViewController:secondNavigationController animated:YES];
			[newProfile release];
			[editProfileController release];
			[secondNavigationController release];
		}
}

-(void)didUpdateObject:(id)updatedObject {
	// get application delegate
    McpAppDelegate *appDelegate = (McpAppDelegate*)[[UIApplication sharedApplication] delegate];
    
	// if renamed, then must rename file too
	UserProfile *updatedProfile = (UserProfile*)updatedObject;
	if (![oldProfileName isEqualToString:[updatedProfile name]])
		[updatedProfile renameFrom:oldProfileName to:[updatedProfile name]];
	
	// update class to disk and refresh table
	[updatedProfile saveAs:[updatedProfile name]];
	[self populateProfileList];
	[self.tableView reloadData];
    
    // if this is our currently selected profile, assign to delegate
    if ([appDelegate.currentUserProfile.name isEqualToString:oldProfileName]) {
        appDelegate.currentUserProfile = updatedObject;
    }
}

-(void)didAddNewObject:(id)newObject {
	// save new class
	UserProfile *newProfile = (UserProfile*)newObject;
	[newObject saveAs:[newProfile name]];
	[self populateProfileList];
	[self.tableView reloadData];
}

#pragma mark -
#pragma mark Table view delegate

-(void)tableView:(UITableView*)tableView didSelectRowAtIndexPath:(NSIndexPath*) indexPath {
	// get application delegate
    McpAppDelegate *appDelegate = (McpAppDelegate*)[[UIApplication sharedApplication] delegate];
    
	// find row
	NSUInteger row = [indexPath row];
	
	// save index path
    [lastIndexPath release];
	lastIndexPath = [indexPath retain];
	
	// editing mode?
	if (self.editing) {
		// add new?
		if (row >= [profileList count]) {
			[self tableView:tableView commitEditingStyle:UITableViewCellEditingStyleInsert forRowAtIndexPath:indexPath];
		}
		else {
			// edit background using edit controller
			oldProfileName = [[profileList objectAtIndex:row] copy];
			EditUserProfileController *editProfileController = [[EditUserProfileController alloc] initWithNibName:@"EditUserProfileController" bundle:nil];
			UserProfile *thisProfile = [[UserProfile alloc] initFromArchive:[profileList objectAtIndex:row]];
			[editProfileController setEditableObject:thisProfile isNew:NO];
			editProfileController.editableListDelegate = self;
			[self.navigationController pushViewController:editProfileController animated:YES];
			[thisProfile release];
			[editProfileController release];
		}
	}
	else {
        // select profile and reload table
        UserProfile *thisProfile = [[UserProfile alloc] initFromArchive:[profileList objectAtIndex:row]];
        appDelegate.currentUserProfile = thisProfile;
        appDelegate.localOptions.currentProfileName = thisProfile.name;
        [appDelegate.localOptions save];
        [thisProfile release];
        [tableView reloadData];
	}
}

#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    // Relinquish ownership of anything that can be recreated in viewDidLoad or on demand.
    // For example: self.myOutlet = nil;
}


- (void)dealloc {
    [lastIndexPath release];
	[profileList release];
    [super dealloc];
}

@end
