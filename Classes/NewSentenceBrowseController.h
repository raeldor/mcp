//
//  NewSentenceBrowseController.h
//  HF Sensei
//
//  Created by Ray on 4/5/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SampleSentences.h"
#import "JapaneseDictionary.h"

@interface NewSentenceBrowseController : UITableViewController <UITableViewDelegate, UITableViewDataSource, UISearchResultsUpdating> {
   //UISearchBarDelegate, UISearchDisplayDelegate, 
    // save for speed
    SampleSentences *sampleSentences;
    JapaneseDictionary *japaneseDictionary;
    UIActivityIndicatorView *activityIndicator;
    UIActivityIndicatorView *activityIndicator2;
	
	// search results
    NSTimer *searchTimer;
    BOOL performingSearch;
//    BOOL isSearchResultsLoaded; // loaded yet?
    BOOL filterIsActive; // when displaying filtered results without showing search bar
	NSMutableArray *searchResults; // array of indexes into the actual list
    
    // save keyboard height
    int keyboardHeight;
}

@property (nonatomic, assign) UISearchController *searchController;

-(NSUInteger)numberOfEntriesInGroup:(NSUInteger)groupIndex;
//-(void)updateSearchResultsUsingSearchController:(UISearchDisplayController*)controller;

@end

