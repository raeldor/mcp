//
//  VoiceSample.h
//  Mcp
//
//  Created by Ray on 11/15/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CepstralHistory.h"
#import "UserProfile.h"

@interface StretchDetail : NSObject {
    float score;
    float stretchPercent;
    float processedSize;
}

-(id)initWithScore:(float)inScore andStretch:(float)inStretch andProcessedSize:(int)inProcessedSize;

@property (nonatomic, assign) float score;
@property (nonatomic, assign) float stretchPercent;
@property (nonatomic, assign) float processedSize;

@end

@interface VoiceSample : NSObject {
	int dataLength;
	float *featureBuffer;
	
	// save individual features for things like phoneme boundary checking
	// mfcc and delta buffer (number of mfcc's)
	float *mfccBuffer;
	float *deltaBuffer;
	float *energy;
	float *energyDelta;
	float *covBuffer;
	float *spectrumBuffer;
	float *melBuffer;
	
	float *sampleBuffer;
	int sampleBufferSize;
	
	int mfccCount;
    int melCount;
	int featureCount; // actual number used for analysis
	int quantizeCount;
    
    NSArray *phonemeLengthArray;
	
//	float *frequencySsma;
}

+(int)getMfccCount;
+(int)getFeatureCount;
+(int)getCompareFeatureCount;
+(int)getOutputFeatureCount;
+(int)getMelChannelCountForSampleRate:(int)inSampleRate;
+(int)getN;
+(int)getLog2N;
+(int)getCompareSegmentSize;
+(float)convertScoreToPercent:(float)inScore;

-(double)sigmoidWithInput:(double)inInput response:(double)inResponse;

-(id)initWithBuffer:(float*)inBuffer ofLength:(int)inLength quantizeTo:(int)inQuantizeCount fromUser:(UserProfile*)inUserProfile noiseLevel:(float)inNoiseLevel cepstralHistory:(CepstralHistory*)inCepstralHistory phonemeLengthArray:(NSArray*)inPhonemeLengthArray sampleRate:(int)inSampleRate;

-(void)increaseContrastOfBuffer:(float*)inBuffer andSize:(int)inSize byPercent:(float)inPercent usingCutoff:(float)inCutoff;
-(void)blurBuffer:(float*)inBuffer ofWidth:(int)inWidth andHeight:(int)inHeight;
+(void)applyContrast:(float*)inBuffer ofWidth:(int)inWidth andHeight:(int)inHeight notUsedPercentage:(float)inNotUsedPercentage;
+(void)changeBrightnessTo:(float)inBrightnessPercentChange contrastTo:(float)inContrastPercentChange forBuffer:(float*)inBuffer toBuffer:(float*)outBuffer ofWidth:(int)inWidth andHeight:(int)inHeight blackDistribution:(float*)blackDistribution whiteDistribution:(float*)whiteDistribution;

-(float)compareImage:(float*)inImage1 ofWidth:(int)inWidth andHeight:(int)inHeight withImage:(float*)inImage2;
-(float)compareImage2:(float*)inImage1 ofWidth:(int)inWidth andHeight:(int)inHeight withImage:(float*)inImage2;

-(float)compareSegmentOfImage:(float*)inImage1 withImage:(float*)inImage2 ofWidth:(int)inWidth andHeight:(int)inHeight image1StartPosition:(int)inImage1StartPosition image2StartPosition:(int)inImage2StartPosition segmentSize:(int)inSegmentSize stretchPercentage:(float)inStretchPercentage processBands:(BOOL*)processBand boost:(BOOL)inBoostFlag;


-(float)compareSegmentOfImage2:(float*)inImage1 withImage:(float*)inImage2 ofWidth:(int)inWidth andHeight:(int)inHeight image1StartPosition:(int)inImage1StartPosition image2StartPosition:(int)inImage2StartPosition segmentSize:(int)inSegmentSize stretchPercentage:(float)inStretchPercentage;
-(float)compareSegmentOfImage3:(float*)inImage1 withImage:(float*)inImage2 ofWidth:(int)inWidth andHeight:(int)inHeight image1StartPosition:(int)inImage1StartPosition image2StartPosition:(int)inImage2StartPosition segmentSize:(int)inSegmentSize stretchPercentage:(float)inStretchPercentage;
-(float)compareColumn:(int)sourcePos OfImage:(float*)inImage1 ofWidth:(int)inWidth andHeight:(int)inHeight withImage:(float*)inImage2 column:(int)destPos;

-(NSArray*)getLead:(float*)leadPercent andTail:(float*)tailPercent fromBuffer:(float*)hpBuffer ofLength:(int)inLength overrideNoiseLevel:(float)inNoiseLevel fromUser:(UserProfile*)inUserProfile;
+(void)getLead:(float*)leadPercent andTail:(float*)tailPercent fromBuffer:(float*)hpBuffer ofLength:(int)inLength overrideNoiseLevel:(float)inNoiseLevel;

-(float*)buildBufferUsingStretchList:(NSArray*)inStretchList andImage:(float*)inImage ofWidth:(int)inWidth andHeight:(int)inHeight;

-(float)compareImage4:(float*)inImage1 ofWidth:(int)inWidth andHeight:(int)inHeight withImage:(float*)inImage2;

-(float)compareImageSplit:(float*)inImage1 ofWidth:(int)inWidth andHeight:(int)inHeight startPosition:(int)inStartPosition withImage:(float*)inImage2 segmentSize:(int)inSegmentSize processBands:(BOOL*)processBand;

-(NSArray*)getSegmentScores:(float*)inImage1 ofWidth:(int)inWidth andHeight:(int)inHeight withImage:(float*)inImage2 processBands:(BOOL*)processBands test:(BOOL)inTest;

-(int*)getPhonemeBreakPositionsForCount:(int)phonemeCount;

-(float)correlationOf:(float*)inArray1 with:(float*)inArray2 length:(int)inLength;
-(float)covarianceOf:(float*)inArray1 with:(float*)inArray2 length:(int)inLength;

-(int)getSyllableCount;
-(bool)getNextSyllableEndPositionFromPosition:(int*)endPos;
-(char)getNextContigousNoiseTypeFromPosition:(int*)inPos;
-(char)getNoiseTypeForPosition:(int)inPos;

-(void)normalizeBufferAtZero:(float*)inBuffer ofSize:(int)inSize;
-(void)normalizeBufferAtZero:(float*)inBuffer ofSize:(int)inSize usingMax:(float)inMax;

-(void)normalizeBufferAroundZeroToPoint5:(float*)inBuffer ofSize:(int)inSize;
+(void)normalizeBufferAroundZeroToPoint5:(float*)inBuffer ofSize:(int)inSize usingMax:(float)inMax;

-(void)normalizePositiveBuffer:(float*)inBuffer ofSize:(int)inSize usingMax:(float)max;
+(void)normalizePositiveBuffer:(float*)inBuffer ofSize:(int)inSize;

-(void)normalizeAndScaleBuffer:(float*)inBuffer ofSize:(int)inSize;
-(void)normalizeBuffer:(float*)inBuffer ofSize:(int)inSize usingMax:(float)max;
-(void)createSsmaWithBuffer:(float*)inBuffer ofSize:(int)inSize usingPeriod:(int)inPeriod outputTo:(float*)outBuffer;
-(float)compareBuffer:(float*)inBuffer ofSize:(int)inSize withBuffer:(float*)inBuffer2;
-(float)compareImage:(float*)inImage ofWidth:(int)inWidth andHeight:(int)inHeight withImage:(float*)inImage2;
-(float)compareImage2:(float*)inImage ofWidth:(int)inWidth andHeight:(int)inHeight withImage:(float*)inImage2;

+(void)matchBrightnessOfBuffer:(float*)buffer1 toBuffer:(float*)buffer2 ofWidth:(int)inWidth andHeight:(int)inHeight;
+(float)compareImage3:(float*)inImage1 ofWidth:(int)inWidth andHeight:(int)inHeight withImage:(float*)inImage2 forText:(NSString*)inText dtwImage:(float*)inDtwImage1 withDtwImage:(float*)inDtwImage2 withHeight:(int)inDtwHeight calibrationMode:(BOOL)inCalibrationMode writeDebug:(BOOL)inWriteDebug compareWithCheckImage:(BOOL)inCompareWithCheck phonemeLengthArray:(NSArray*)inPhonemeLengthArray ;
+(float)compareImage3_1:(float*)inImage1 ofWidth:(int)inWidth withImage:(float*)inImage2 withLeeway:(int)inLeeway stretchImage2:(BOOL)inStretch;
+(float)compareImage3_horizontal:(float*)inImage1 ofWidth:(int)inWidth andHeight:(int)inHeight withImage:(float*)inImage2 withLeeway:(int)inLeeway stretchImage2:(BOOL)inStretch;
+(float)TukeyFuncWithWidth:(int)inWidth andPosition:(int)i;

-(float*)getSpectrogramFromBuffer:(float*)hpBuffer ofLength:(int)inWidth quantizeTo:(int)inQuantizeCount;

//-(float*)getFeatureBufferFromSpectrogram:(float*)inSpectrumBuffer ofWidth:(int)inWidth andHeight:(int)inHeight;


-(float)compareWithVoiceSample:(VoiceSample*)inSample filenameSuffix:(NSString*)filenameSuffix forText:(NSString*)inText calibrationMode:(BOOL)inCalibrationMode writeDebug:(BOOL)inWriteDebug;
-(int)getDataLength;
-(float*)getFeatureBuffer;
-(float*)getMelBuffer;
-(float*)getMfccBuffer;
-(int)getSampleBufferSize;
-(float*)getSampleBuffer;
-(float*)getSpectrumBuffer;
-(void)image:(UIImage*)image didFinishSavingWithError:(NSError*)error contextInfo:(void*)contextInfo;
-(void)writeToWavFile:(NSString *)inFilename;
//-(void)writeToImageWithFilename:(NSString*)inFilename alternateBuffer:(float*)inAltBuffer;
+(void)writeToImageWithFilename:(NSString*)inFilename alternateBuffer:(float*)inAltBuffer alternateFeatureCount:(int)inAltFeatureCount alternateQuantizeCount:(int)inAltQuantizeCount;
-(void)writeToCsvFileWithFilename:(NSString*)inFilename;
-(void)cropAtStartPercent:(float)startPercent endPercent:(float)endPercent quantizeCount:(int)newQuantizeCount;

CGContextRef MyCreateBitmapContext (int pixelsWide,
									int pixelsHigh);

@end
