    //
//  EditHtmlViewController.m
//  Mcp
//
//  Created by Ray on 1/18/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "EditHtmlViewController.h"
#import "TextFunctions.h"
#import "HtmlFunctions.h"

@implementation EditHtmlViewController

@synthesize editPreviewControl;
@synthesize editTextView;
@synthesize previewWebView;

// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
    }
    return self;
}
*/

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
}
*/

-(void)editPreviewChanged:(id)sender {
	// if we are switching to edit mode
	if (editPreviewControl.selectedSegmentIndex == 1) {
		// set web view text
		NSString *htmlString = @"<html><style>span.first {color:white;text-shadow:1px 1px 0px #000;font-family='helvetica';font-size:17px;font-weight:bold;}</style><style>span.second {color:gray;text-shadow:1px 1px 0px #000;font-family='helvetica';font-size:17px;font-weight:bold;}</style><body><div id=\"content\" contenteditable=\"true\"><p>";
		htmlString = [htmlString stringByAppendingFormat:@"<span class='first'>%@</span></p></div></body></html>", editTextView.text];
        htmlString = [HtmlFunctions convertHtml:htmlString toHtmlAs:1];
		[previewWebView loadHTMLString:editTextView.text baseURL:nil];
		
		// hide text view, show web view
		previewWebView.hidden = NO;
		editTextView.hidden = YES;
	}
	else {
		// show text view again
		editTextView.hidden = NO;
		previewWebView.hidden = YES;
	}
	
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
	[editPreviewControl release];
	[editTextView release];
	[previewWebView release];
    [super dealloc];
}


@end
