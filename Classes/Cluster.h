//
//  Cluster.h
//  Mcp
//
//  Created by Ray on 3/20/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DataSet.h"

@interface Cluster : NSObject {
	NSMutableArray *dataPoints;
	int clusterIndex;
}

-(id)init;
-(void)addDataPoint:(DataPoint*)inDataPoint;
-(void)expandClusterUsingPoint:(DataPoint*)inDataPoint withNeighbors:(DataSet*)inNeighbors inDataSet:(DataSet*)inDataSet withEps:(float)inEps andMinPts:(int)inMinPts;
//-(bool)containsDataPoint:(DataPoint*)inDataPoint;
-(bool)hasNeighborsOfPoint:(DataPoint*)inDataPoint eps:(float)inEps;
-(float)getDistanceOfClosestPointToPoint:(DataPoint*)inDataPoint;
-(float)getPercentChanceOfFeature:(int)inFeatureIndex;
-(void)dealloc;

@property (nonatomic, retain) NSMutableArray *dataPoints;
@property (nonatomic, assign) int clusterIndex;

@end
