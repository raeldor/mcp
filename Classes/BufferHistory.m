//
//  BufferHistory.m
//  Mcp
//
//  Created by Ray Price on 9/8/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "BufferHistory.h"

@implementation BufferHistory

@synthesize totalLength;

-(id)initWithBufferHeight:(int)inBufferHeight {
	if ((self = [super init])) {
		// initialize members
		bufferHeight = inBufferHeight;
		historyLength = 2048;
		totalLength = 0;
		historyData = malloc(sizeof(float)*bufferHeight*historyLength);
		
		// set data to zero
		for (int i=0; i < bufferHeight*historyLength; i++)
			historyData[i] = 0.0f;
	}
	return self;
}

-(void)addNewData:(float*)inData ofLength:(int)inLength {
	// add to total length
	totalLength += inLength;
	if (totalLength > historyLength)
		totalLength = historyLength;
	
	// shuffle data to the left
	for (int i=inLength; i < historyLength; i++) {
		for (int m=0; m < bufferHeight; m++) {
			historyData[(i-inLength)*bufferHeight+m] = historyData[i*bufferHeight+m];
//			historyData[i*bufferHeight+m] = historyData[(i-inLength)*bufferHeight+m];
		}
	}
	
	// add new data
	for (int i=0; i < (inLength > historyLength ? historyLength:inLength); i++) {
		for (int m=0; m < bufferHeight; m++) {
			historyData[(historyLength-inLength+i)*bufferHeight+m] = inData[i*bufferHeight+m];
		}
	}
}

-(float*)getDataStart {
    return &historyData[(historyLength-totalLength)*bufferHeight];
}

-(float*)getRowTotals {
	// loop through and calculate cepstral mean array and return
	float *rowTotals = malloc(sizeof(float)*bufferHeight);
	for (int m=0; m < bufferHeight; m++) {
		rowTotals[m] = 0.0f;
		for (int i=0; i < totalLength; i++) {
            if (historyData[(historyLength-i)*bufferHeight+m] > rowTotals[m])
                rowTotals[m] = historyData[(historyLength-i-1)*bufferHeight+m];
		}
	}
	
	return rowTotals;
}

-(void)dealloc {
	free(historyData);
	[super dealloc];
}

@end
