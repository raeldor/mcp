//
//  DictionarySelectController.h
//  HF Sensei
//
//  Created by Ray on 4/5/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewDictionaryBrowseController.h"
#import "BrowserSelectDelegate.h"
#import "MultiSelectControllerDelegate.h"
#import "JapaneseDictionary.h"

@interface DictionarySelectController : NewDictionaryBrowseController <UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate, UISearchDisplayDelegate, MultiSelectControllerDelegate> {
    IBOutlet UILongPressGestureRecognizer *longPressRecognizer;
    
    UITableView *lastSelectedTableView;
    NSUInteger lastSelectedSection;
    NSUInteger lastSelectedRow;
	
	// selected words list
	NSMutableArray *selectedWords;
}

@property (nonatomic, assign) id<BrowserSelectDelegate> selectDelegate;

-(IBAction)handleLongPressGesture:(UIGestureRecognizer *)gestureRecognizer;
-(int)getSelectedIndexForWord:(JapaneseWord*)inWord;
-(void)didSelectRowAtIndexPath:(NSIndexPath*)indexPath forTableView:(UITableView*)tableView wasLongPress:(BOOL)wasLongPress;
-(IBAction)selectButtonClick:(id)sender;
-(IBAction)cancelButtonClick:(id)sender;

@end

