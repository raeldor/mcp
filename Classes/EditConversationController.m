//
//  EditConversationController.m
//  Mcp
//
//  Created by Ray on 10/26/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "EditConversationController.h"
#import "NameCell.h"
#import "NotesCell.h"
#import "BackgroundListCell.h"
#import "Background.h"
#import "BackgroundSelectViewController.h"
#import "ActorSelectViewController.h"
#import "Character.h"
#import "Actor.h"
#import "CharacterCell.h"
#import "DialogueLineCell.h"
#import "DialogueLine.h"
#import "McpAppDelegate.h"
#import "EditDialogueLineController.h"
#import "TextFunctions.h"
#import "EditDialogueLineViewController.h"
#import "HtmlFunctions.h"

@implementation EditConversationController

@synthesize conversation;
@synthesize editableListDelegate;
@synthesize popOver;
@synthesize myTableView;

#pragma mark -
#pragma mark View lifecycle

-(void)setEditableObject:(id)editableObject inBook:(Book *)inBook andChapter:(Chapter *)inChapter isNew:(BOOL)isNew {
	// save new flag
	newMode = isNew;
    book = [inBook retain];
    chapter = [inChapter retain];
    
	// save copy of object
    Conversation *conversationCopy = [editableObject copy];
	self.conversation = conversationCopy;
    [conversationCopy release];
	
	// this table is always in edit mode
	// we are not adding delete rows, so we don't need edit mode
	//	[self setEditing:YES animated:NO];
	
	// create cancel button
	UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(cancelButtonClick:)];
	self.navigationItem.leftBarButtonItem = cancelButton;
	[cancelButton release];
	
	// create save button
	UIBarButtonItem *saveButton = [[UIBarButtonItem alloc] initWithTitle:@"Save" style:UIBarButtonItemStyleBordered target:self action:@selector(saveButtonClick:)];
	self.navigationItem.rightBarButtonItem = saveButton;
	[saveButton release];
}

-(void)saveButtonClick:(id)sender {
	// check first for errors
	if ([self isOkToSave])
	{
		// notify parent of change
		if (newMode)
			[editableListDelegate didAddNewObject:conversation];
		else
			[editableListDelegate didUpdateObject:conversation];
		
		// pop controller
		if (newMode) {
			[self dismissModalViewControllerAnimated:YES];
		}
		else {
			[self.navigationController popViewControllerAnimated:TRUE];
		}
	}
}

-(void)cancelButtonClick:(id)sender {
	// pop controller
	if (newMode) {
		[self dismissModalViewControllerAnimated:YES];
	}
	else {
		[self.navigationController popViewControllerAnimated:TRUE];
	}
}

-(BOOL)isOkToSave {
	BOOL stillOk = YES;
	NSString *msg;
	
	// check fields are filled
    if (conversation.characters.count < 1) {
        msg = @"At least one character must be added.";
        stillOk = NO;
    }
    else {
        if (conversation.dialogueLines.count < 1) {
            msg = @"At least one dialogue line must be added.";
            stillOk = NO;
        }
        else {
            // check character indexes of dialogue lines are less than character count
            for (int i=0; i < conversation.dialogueLines.count; i++) {
                DialogueLine *thisLine = [conversation.dialogueLines objectAtIndex:i];
                if (thisLine.characterIndex >= conversation.characters.count) {
                    msg = [NSString stringWithFormat:@"Character for dialogue line %d does not exist.", i+1];
                    stillOk = NO;
                    break;
                }
            }
        }
    }

	
	// display error if not ok
	if (!stillOk) {
		UIAlertView *myAlert = [[UIAlertView alloc] initWithTitle:@"Oops" message:msg delegate:nil cancelButtonTitle:@"Understood" otherButtonTitles:nil];
		[myAlert show];
		[myAlert release];
	}
	
	return stillOk;
}

-(void)viewDidLoad {
	[super viewDidLoad];
    
	// allow selecting during editor
	self.myTableView.allowsSelection = TRUE;
	self.myTableView.allowsSelectionDuringEditing = TRUE;
    
    // put into editing mode here now objects are established
	self.editing = YES;
}

-(void)viewWillAppear:(BOOL)animated {
    // deselect currently selected row
    [self.myTableView deselectRowAtIndexPath:[self.myTableView indexPathForSelectedRow] animated:animated];
}

//editing mode changed
-(void)setEditing:(BOOL)editing animated:(BOOL)animated {
    // put table into edit mode manually because we're not using a table view controller
    [super setEditing:editing animated:animated];
    [self.myTableView setEditing:editing animated:animated];
}

#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 2;  // characters, dialogue
}

-(NSString*)tableView:(UITableView*)tableView
titleForHeaderInSection:(NSInteger)section {
	switch (section) {
		case 0:
			return @"Characters";
		case 1:
			return @"Dialogue Lines";
		default:
			break;
	}
	return @"Unknown";
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
	switch (section) {
		case 0: // characters
			return conversation.characters.count+1;
		case 1: // dialogue
			return conversation.dialogueLines.count+1;
		default:
			break;
	}
    return 0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	switch ([indexPath section]) {
		case 0: // characters
			if ([indexPath row] == conversation.characters.count)
				return 44;
			else
				return 120;
		case 1: // dialogue lines
			if ([indexPath row] == conversation.dialogueLines.count)
				return 44;
			else {
				// calculate height of webview text
				DialogueLine *thisLine = [conversation.dialogueLines objectAtIndex:[indexPath row]];
				NSString *text = [thisLine getLineAsText];
                
				testHtmlString = [NSString stringWithFormat:@"<html id='test'><p>%@</p></html>", thisLine.line];
                testHtmlString = [HtmlFunctions convertHtml:testHtmlString toHtmlAs:1];
                
//                foundSize = -1;
//                [self performSelectorOnMainThread:@selector(findHtmlHeight:) withObject:self waitUntilDone:YES];
//                while (foundSize < 0);
                
				CGSize stringSize = [text sizeWithFont:[UIFont systemFontOfSize:18] constrainedToSize:CGSizeMake(200,500) lineBreakMode:UILineBreakModeCharacterWrap];
				return stringSize.height*1.0f+100;
			}
		default:
			break;
	}
	return 44;
}

-(void)findHtmlHeight:(id)inString {
    UIWebView *myWeb = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, 200, 10)];
    myWeb.delegate = self;
    [myWeb loadHTMLString:testHtmlString baseURL:nil];
}

-(void)webViewDidFinishLoad:(UIWebView *)webView {
    NSString *output = [webView stringByEvaluatingJavaScriptFromString:@"document.body.scrollHeight;"];
    foundSize = [output intValue];
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	// get application delegate
    McpAppDelegate *appDelegate = (McpAppDelegate*)[[UIApplication sharedApplication] delegate];
    
    NSString *CellIdentifier = @"Cell";
	
	// get identifier and get cell
	switch ([indexPath section]) {
		case 0:
			if ([indexPath row] == conversation.characters.count)
				CellIdentifier = @"SimpleCell";
			else
				CellIdentifier = @"CharacterCell";
			break;
		case 1:
			if ([indexPath row] == conversation.dialogueLines.count)
				CellIdentifier = @"SimpleCell";
			else
				CellIdentifier = @"DialogueLineCell";
			break;
		default:
			break;
	}
	UITableViewCell *cell = (UITableViewCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	if (cell == nil) {
		// load cell layout from nib
		if ([CellIdentifier isEqualToString:@"SimpleCell"])
			cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
		else {
			NSArray *nib = [[NSBundle mainBundle] loadNibNamed:CellIdentifier owner:self options:nil];
			cell = [nib objectAtIndex:0];
		}
	}
	cell.accessoryType = UITableViewCellAccessoryNone;
	cell.selectionStyle = UITableViewCellSelectionStyleNone;
	
	// which section?
	CharacterCell *myCharacterCell;
	DialogueLineCell *myDialogueCell;
	switch ([indexPath section]) {
		case 0: // characters
			if ([indexPath row] == conversation.characters.count) {
				cell.textLabel.text = @"Add New";
				cell.textLabel.textColor = [UIColor lightGrayColor];
			}
			else {
				cell.textLabel.text = nil;
				myCharacterCell = (CharacterCell*)cell;
				myCharacterCell.characterNameTextField.tag = [indexPath row];
				[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(characterNameDidChange:) name:@"UITextFieldTextDidChangeNotification" object:myCharacterCell.characterNameTextField];
				Character *thisChar = [conversation.characters objectAtIndex:[indexPath row]];
//				Actor *myActor = [[Actor alloc] initFromArchive:thisChar.actorName];
                Actor *myActor = [appDelegate getActorUsingName:thisChar.actorName];
                int restFrame = [myActor getActorFrameForName:@"rest"];
                NSString *filename = [NSString stringWithFormat:@"%@%05d.jpg", myActor.animeCharacter, restFrame+1];
                NSString *filePath = [[NSBundle mainBundle] pathForResource:filename ofType:nil];
                myCharacterCell.actorImageView.image = [UIImage imageWithContentsOfFile:filePath];
				myCharacterCell.characterNameTextField.text = thisChar.characterName;
				myCharacterCell.actorNameLabel.text = thisChar.actorName;
//				[myActor release];
			}
			break;
		case 1: // dialogue lines
			if ([indexPath row] == conversation.dialogueLines.count) {
				cell.textLabel.text = @"Add New";
				cell.textLabel.textColor = [UIColor lightGrayColor];
			}
			else {
				// allow selection for edit of line
				cell.editingAccessoryType = UITableViewCellAccessoryDisclosureIndicator;
				cell.selectionStyle = UITableViewCellSelectionStyleBlue;
				cell.textLabel.text = nil;
				
				// get dialog cell and configure
				myDialogueCell = (DialogueLineCell*)cell;
				myDialogueCell.characterButton.tag = [indexPath row];
				DialogueLine *thisLine = [conversation.dialogueLines objectAtIndex:[indexPath row]];
				NSString *htmlString = [NSString stringWithFormat:@"<html>%@</html>", thisLine.line];
//                htmlString = [TextFunctions convertHtml:htmlString toHtmlAs:1];
				[myDialogueCell.lineWebView loadHTMLString:htmlString baseURL:nil];
				myDialogueCell.lineWebView.opaque = NO;
				myDialogueCell.lineWebView.backgroundColor = [UIColor clearColor];
				
				// link character button
				[myDialogueCell.characterButton addTarget:self action:@selector(characterButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
				
				// if actor is still here
				if (conversation.characters.count > thisLine.characterIndex) {
					// configure actor button image and label
					myDialogueCell.characterLabel.text = [[conversation.characters objectAtIndex:thisLine.characterIndex] characterName];
					NSString *actorName = [[conversation.characters objectAtIndex:thisLine.characterIndex] actorName];
//					Actor *myActor = [[Actor alloc] initFromArchive:actorName];
                    Actor *myActor = [appDelegate getActorUsingName:actorName];
                    int restFrame = [myActor getActorFrameForName:@"rest"];
                    NSString *filename = [NSString stringWithFormat:@"%@%05d.jpg", myActor.animeCharacter, restFrame+1];
                    NSString *filePath = [[NSBundle mainBundle] pathForResource:filename ofType:nil];
					[myDialogueCell.characterButton setImage:[UIImage imageWithContentsOfFile:filePath] forState:UIControlStateNormal];
//					[myActor release];
				}
				else {
					myDialogueCell.characterLabel.text = @"Unknown Actor";
				}
			}
			break;
		default:
			break;
	}
    
    return cell;
}

-(UITableViewCellEditingStyle)tableView:(UITableView*)tableView editingStyleForRowAtIndexPath:(NSIndexPath*)indexPath {
	// which section?
	switch ([indexPath section]) {
		case 0: // characters
			if ([indexPath row] != conversation.characters.count)
				return UITableViewCellEditingStyleDelete;
			else
				return UITableViewCellEditingStyleInsert;
			break;
		case 1: // dialog lines
			if ([indexPath row] != conversation.dialogueLines.count)
				return UITableViewCellEditingStyleDelete;
			else
				return UITableViewCellEditingStyleInsert;
			break;
		default:
			break;
	}
	return UITableViewCellEditingStyleNone;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    // if we are deleting rows
    if (editingStyle == UITableViewCellEditingStyleDelete) {
		switch ([indexPath section]) {
			case 0: // characters
				[conversation.characters removeObjectAtIndex:[indexPath row]];
				[self.myTableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:YES];
				// reload data to update actor names in case one was deleted
				[self.myTableView reloadData];
				break;
			case 1: // dialogue lines
				[conversation.dialogueLines removeObjectAtIndex:[indexPath row]];
				[self.myTableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:YES];
				break;
		}
    }
    else
		if (editingStyle == UITableViewCellEditingStyleInsert) {
			// which section
			ActorSelectViewController *actorSelectController;
			DialogueLine *newLine;
			switch ([indexPath section]) {
				case 0: // characters
					// select actor
					actorSelectController = [[ActorSelectViewController alloc] initWithNibName:@"ActorSelectViewController" bundle:nil];
					actorSelectController.view.tag = 0;
					actorSelectController.selectDelegate = self;
					[self.navigationController pushViewController:actorSelectController animated:YES];
					[actorSelectController release];
					break;
				case 1: // dialog lines
					newLine = [[DialogueLine alloc] init];
					[conversation.dialogueLines addObject:newLine];
					[newLine release];
					[self.myTableView reloadData];
				default:
					break;
			}
		}
}

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

-(BOOL)tableView:(UITableView*)tableView canMoveRowAtIndexPath:(NSIndexPath*)indexPath {
	// can re-order dialogue lines
	if ([indexPath section] == 2) {
		// can't re-order 'add new'
		if ([indexPath row] < conversation.dialogueLines.count)
			return YES;
		else
			return NO;
	}
    return NO;
}

-(void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
	// get from and to row
	int fromRow = [fromIndexPath row];
	int toRow = [toIndexPath row];
	
	// swap these rows in the array
	DialogueLine *moveLine = [[conversation.dialogueLines objectAtIndex:fromRow] retain];
	[conversation.dialogueLines removeObjectAtIndex:fromRow];
	[conversation.dialogueLines insertObject:moveLine atIndex:toRow];
	[moveLine release];
}

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	// save last index path for de-highlighting and updating
    [lastIndexPath release];
	lastIndexPath = [indexPath retain];

	EditDialogueLineViewController *editController;
    NSTimeInterval fingerDownTime = [NSDate timeIntervalSinceReferenceDate]-self.myTableView.touchesBeganTime;
	switch ([indexPath section]) {
		case 1: // dialogue line
            // are we asking to make a copy of the line?
            if (fingerDownTime > 0.5f) {
                // confirm first
                UIAlertView *myAlert = [[UIAlertView alloc] initWithTitle:@"Copy" message:@"Make a new copy of this line?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
                [myAlert show];
                [myAlert release];
                return;
            }
            
			// if we are on an edit line
			if ([indexPath row] < conversation.dialogueLines.count) {
                // show edit controller
				editController = [[EditDialogueLineViewController alloc] initWithNibName:@"EditDialogueLineViewController" bundle:nil];
				editController.editDialogueLineDelegate = self;
				[editController setEditableObject:[conversation.dialogueLines objectAtIndex:[indexPath row]] isNew:NO];
                [editController setChapterAs:chapter];
				[self.navigationController pushViewController:editController animated:YES];
				[editController release];
			}
			break;
		default:
			break;
	}
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    // if yes was pressed
    if (buttonIndex == 1) {
        // make a copy after the current row, and refresh the table
        DialogueLine *lineCopy = [[conversation.dialogueLines objectAtIndex:[lastIndexPath row]] copy];
        [conversation.dialogueLines addObject:lineCopy];
        [lineCopy release];
        [self.myTableView reloadData];
    }
}

-(void)didUpdateDialogueLine:(DialogueLine*)inLine {
	// update line
    [conversation.dialogueLines replaceObjectAtIndex:[lastIndexPath row] withObject:inLine];
	
	// refresh tableview
	[self.myTableView reloadData];
}

-(void)didAddDialogueLine:(DialogueLine*)inLine {
    // not used as dialogline edit is not used to add lines
}

-(void)didMakeBrowserSelection:(NSString*)selection fromController:(UIViewController*)selectController {
	// which controller?
	Character *newChar;
	switch ([selectController.view tag]) {
		case 0: // characters
			// add new character
			newChar = [[Character alloc] init];
			newChar.characterName = selection;
			newChar.actorName = selection;
			[conversation.characters addObject:newChar];
			[newChar release];
			break;
		default:
			break;
	}
	
	[self.myTableView reloadData];
}

-(void)didCancelBrowserSelection {
}

-(void)textFieldDidChange:(NSNotification*)note {
	UITextField *thisTextField = (UITextField*)note.object;
	switch (thisTextField.tag) {
		default:
			break;
	}
}

-(void)characterNameDidChange:(NSNotification*)note {
	UITextField *thisTextField = (UITextField*)note.object;
	Character *thisChar = [conversation.characters objectAtIndex:thisTextField.tag];
	thisChar.characterName = [NSString stringWithString:thisTextField.text];
}

-(void)dialogueLineDidChange:(NSNotification*)note {
	UITextView *thisTextView = (UITextView*)note.object;
	DialogueLine *thisLine = [conversation.dialogueLines objectAtIndex:thisTextView.tag];
	thisLine.line = [NSString stringWithString:thisTextView.text];
}

-(void)dialogueLineBeginEdit:(NSNotification*)note {
	UITextView *thisTextView = (UITextView*)note.object;
	textViewBeingEdited = thisTextView;
}

-(void)dialogueLineEndEdit:(NSNotification*)note {
	textViewBeingEdited = nil;
}

-(void)actorSegmentChanged:(id)sender {
	UISegmentedControl *actorSegment = (UISegmentedControl*)sender;
	DialogueLine *thisLine = [conversation.dialogueLines objectAtIndex:actorSegment.tag];
	thisLine.characterIndex = actorSegment.selectedSegmentIndex;
}

-(void)characterButtonPressed:(id)sender {
	// get line
	UIButton *thisButton = (UIButton*)sender;
	DialogueLine *thisLine = [conversation.dialogueLines objectAtIndex:thisButton.tag];
	
	// update character index
	thisLine.characterIndex++;
	if (thisLine.characterIndex > conversation.characters.count-1)
		thisLine.characterIndex = 0;
	
	// reload
	[self.myTableView reloadData];
}

#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    // Relinquish ownership of anything that can be recreated in viewDidLoad or on demand.
    // For example: self.myOutlet = nil;
}

- (void)dealloc {
    [lastIndexPath release];
    [book release];
    [chapter release];
    [myTableView release];
	[conversation release];
	[popOver release];
    [super dealloc];
}


@end

