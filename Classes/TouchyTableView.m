//
//  TouchyTableView.m
//  Mcp
//
//  Created by Ray on 7/2/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "TouchyTableView.h"

@implementation TouchyTableView

@synthesize touchesBeganTime;

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
	// set time touches began
	touchesBeganTime = [NSDate timeIntervalSinceReferenceDate];
	
	// call super
	[super touchesBegan:touches withEvent:event];
}


@end
