//
//  ActorSelectViewController.h
//  Mcp
//
//  Created by Ray on 11/1/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SelectViewControllerDelegate.h"

@interface ActorSelectViewController : UITableViewController <UITableViewDelegate, UITableViewDataSource> {
	// delegate
	id<SelectViewControllerDelegate> selectDelegate;
	
	NSMutableArray *actorList; // list of NSString*
	NSIndexPath *lastIndexPath;
}

@property (nonatomic, retain) NSMutableArray *actorList;
@property (nonatomic, assign) id<SelectViewControllerDelegate> selectDelegate;

-(void)populateActorList;
-(IBAction)selectButtonClick:(id)sender;
-(IBAction)cancelButtonClick:(id)sender;

@end