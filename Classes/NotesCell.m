//
//  NotesCell.m
//  Mcp
//
//  Created by Ray on 10/29/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "NotesCell.h"


@implementation NotesCell

@synthesize notesLabel;
@synthesize notesTextView;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if ((self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
	
    [super setSelected:selected animated:animated];
	
    // Configure the view for the selected state
}

- (void)dealloc {
	[notesLabel release];
	[notesTextView release];
    [super dealloc];
}


@end
