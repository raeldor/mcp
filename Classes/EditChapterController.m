//
//  EditChapterController.m
//  Mcp
//
//  Created by Ray on 12/21/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "EditChapterController.h"
#import "NameCell.h"

@implementation EditChapterController

@synthesize chapter;
@synthesize editableListDelegate;

#pragma mark -
#pragma mark View lifecycle

-(void)setEditableObject:(Chapter*)inChapter isNew:(BOOL)isNew {
	// allow selecting during editor
	self.tableView.allowsSelectionDuringEditing = TRUE;
	
	// save new flag
	newMode = isNew;
	
	// save object
	self.chapter = inChapter;
	
	// this table is always in edit mode
	// we are not adding delete rows, so we don't need edit mode
	//	[self setEditing:YES animated:NO];
	
	// create cancel button
	UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(cancelButtonClick:)];
	self.navigationItem.leftBarButtonItem = cancelButton;
	[cancelButton release];
	
	// create save button
	UIBarButtonItem *saveButton = [[UIBarButtonItem alloc] initWithTitle:@"Save" style:UIBarButtonItemStyleBordered target:self action:@selector(saveButtonClick:)];
	self.navigationItem.rightBarButtonItem = saveButton;
	[saveButton release];
}

-(void)saveButtonClick:(id)sender {
	// check first for errors
	if ([self isOkToSave])
	{
		// notify parent of change
		if (newMode)
			[editableListDelegate didAddNewObject:chapter];
		else
			[editableListDelegate didUpdateObject:chapter];
		
		// pop controller
		if (newMode) {
			[self dismissModalViewControllerAnimated:YES];
		}
		else {
			[self.navigationController popViewControllerAnimated:TRUE];
		}
	}
}

-(void)cancelButtonClick:(id)sender {
	// pop controller
	if (newMode) {
		[self dismissModalViewControllerAnimated:YES];
	}
	else {
		[self.navigationController popViewControllerAnimated:TRUE];
	}
}

-(BOOL)isOkToSave {
	BOOL stillOk = YES;
	NSString *msg;
	
	// check fields are filled
    if (chapter.chapterName == nil || [chapter.chapterName isEqualToString:@""]) {
        msg = @"Chapter name must be completed.";
        stillOk = NO;
    }
	
	// display error if not ok
	if (!stillOk) {
		UIAlertView *myAlert = [[UIAlertView alloc] initWithTitle:@"Oops" message:msg delegate:nil cancelButtonTitle:@"Understood" otherButtonTitles:nil];
		[myAlert show];
		[myAlert release];
	}
	
	return stillOk;
}

#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	return 44;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *CellIdentifier = @"Cell";
	
	// get identifier and get cell
	NSArray *identifiers = [NSArray arrayWithObjects:@"NameCell", @"BookPhotoCell", nil];
	CellIdentifier = [identifiers objectAtIndex:[indexPath row]];
	UITableViewCell *cell = (NameCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	if (cell == nil) {
		// load cell layout from nib
		NSArray *nib = [[NSBundle mainBundle] loadNibNamed:CellIdentifier owner:self options:nil];
		cell = [nib objectAtIndex:0];
	}
	cell.accessoryType = UITableViewCellAccessoryNone;
	cell.selectionStyle = UITableViewCellSelectionStyleNone;
	
	// now configure new cell
	NameCell *myNameCell;
	switch ([indexPath row]) {
		case 0: // name
			myNameCell = (NameCell*)cell;
			[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textFieldDidChange:) name:@"UITextFieldTextDidChangeNotification" object:myNameCell.nameTextField];
			myNameCell.nameTextField.text = chapter.chapterName;
			break;
		default:
			break;
	}
    
    return cell;
}

-(UITableViewCellEditingStyle)tableView:(UITableView*)tableView editingStyleForRowAtIndexPath:(NSIndexPath*)indexPath {
	return UITableViewCellEditingStyleNone;
}

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */


/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
 
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }   
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }   
 }
 */


/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
 }
 */


/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

}

-(void)textFieldDidChange:(NSNotification*)note {
	chapter.chapterName = [note.object performSelector:@selector(text)];
}

#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    // Relinquish ownership of anything that can be recreated in viewDidLoad or on demand.
    // For example: self.myOutlet = nil;
}


- (void)dealloc {
	[chapter release];
    [super dealloc];
}


@end

