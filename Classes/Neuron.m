//
//  Neuron.m
//  Mcp
//
//  Created by Ray on 2/6/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "Neuron.h"


@implementation Neuron

@synthesize inputCount;
@synthesize weights;
@synthesize lastWeightUpdateValue;
@synthesize activation;
@synthesize errorValue;

-(id)initWithInputCount:(int)inCount {
	if (self = [super init]) {
		// allocate space for vector weights
		inputCount = inCount;
		weights = malloc(sizeof(double)*(inCount+1));
		lastWeightUpdateValue = malloc(sizeof(double)*(inCount+1));
		
		// assign random values to weights between -1 and 1
		for (int i=0; i < inputCount+1; i++) {
			double initialValue = (rand()/(double)RAND_MAX)*2.0f-1.0f;
			weights[i] = initialValue;
			lastWeightUpdateValue[i] = 0.0f;
		}
	}
	return self;
}

-(id)initWithCoder:(NSCoder*)coder {
	if ((self = [super init])) {
        // get parameters
        inputCount = [coder decodeInt32ForKey:@"inputCount"];
        
        // get weights
        NSUInteger decodeLength;
        double *myWeights = (double*)[coder decodeBytesForKey:@"weights" returnedLength:&decodeLength];
        weights = malloc(decodeLength);
        memcpy(weights, myWeights, decodeLength);
        
        // create last update array for momentum
		lastWeightUpdateValue = malloc(sizeof(double)*(inputCount+1));
		for (int i=0; i < inputCount+1; i++)
			lastWeightUpdateValue[i] = 0.0f;
	}
	return self;
}

-(void)encodeWithCoder:(NSCoder*)coder {
    // encode parameters
    [coder encodeInt32:inputCount forKey:@"inputCount"];
    
    // encode weights
    [coder encodeBytes:(void*)weights length:(inputCount+1)*sizeof(double) forKey:@"weights"];
}


-(void)dealloc {
	free(weights);
	free(lastWeightUpdateValue);
	[super dealloc];
}

@end
