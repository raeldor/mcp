//
//  EditActorController.h
//  Mcp
//
//  Created by Ray on 10/21/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Actor.h"
#import "EditableListDelegate.h"
#import "ActorAnimeCell.h"

@interface EditActorController : UITableViewController <UITextFieldDelegate, UINavigationControllerDelegate, UIActionSheetDelegate, UIImagePickerControllerDelegate, UITableViewDelegate> {
	BOOL newMode;
	Actor *actor;
	id<EditableListDelegate> editableListDelegate;
	NSIndexPath *lastIndexPath;	
    
	UIPopoverController *popOver; // for ipad compatibility
}

@property (nonatomic, retain) Actor *actor;
@property (nonatomic, assign) id<EditableListDelegate> editableListDelegate;
@property (nonatomic, retain) UIPopoverController *popOver;

-(void)setEditableObject:(Actor*)thisActor isNew:(BOOL)isNew ;
-(BOOL)isOkToSave;
-(void)cancelButtonClick:(id)sender;
-(void)saveButtonClick:(id)sender;
-(void)playButtonPressed:(id)sender;
-(void)pitchChanged:(id)sender;
-(void)voiceSegmentChanged:(id)sender;

-(IBAction)animeSliderChanged:(id)sender;
-(void)updateAnimeCellContent:(UIView*)inContentView;

-(void)textFieldDidChange:(NSNotification*)note;

@end
