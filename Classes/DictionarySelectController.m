//
//  DictionarySelectController.m
//  HF Sensei
//
//  Created by Ray on 4/5/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import "DictionarySelectController.h"
#import "DictionaryListCell.h"
#import "JapaneseDictionary.h"
#import "FlashCard.h"
#import "MultiSelectController.h"
#import "WordSense.h"

@implementation DictionarySelectController

@synthesize selectDelegate;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // create select list
    selectedWords = [[NSMutableArray alloc] initWithCapacity:10];
    
    // add long press recognizer to tableviews
    [self.tableView addGestureRecognizer:longPressRecognizer];
}

/*
-(void)updateSearchResultsUsingSearchController:(UISearchDisplayController*)controller {
    // call super
    [super updateSearchResultsUsingSearchController:controller];
    
    // not sure why we need to remove and re-add, but we do
	if ([controller.searchBar.text length] != 0) {
        // add long press to select individual definitions
        [controller.searchResultsTableView removeGestureRecognizer:longPressRecognizer];
        [controller.searchResultsTableView addGestureRecognizer:longPressRecognizer];
    }
    else {
        // disable long press
        [controller.searchResultsTableView removeGestureRecognizer:longPressRecognizer];
        [self.tableView removeGestureRecognizer:longPressRecognizer];
        [self.tableView addGestureRecognizer:longPressRecognizer];
    }
}*/

-(IBAction)selectButtonClick:(id)sender {
	// pop controller
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
//    [self.navigationController dismissModalViewControllerAnimated:YES];
	
	// call delegate to notify about selection
	[selectDelegate didMakeBrowserSelection:selectedWords];
}

-(IBAction)cancelButtonClick:(id)sender {
    // if we have cards, confirm first
    if (selectedWords.count > 0) {
		UIAlertView *myAlert = [[UIAlertView alloc] initWithTitle:@"Cancel" message:@"Cancel adding these cards, are you sure?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
		[myAlert show];
		[myAlert release];
    }
    else
        // pop controler
        [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
//        [self.navigationController dismissModalViewControllerAnimated:YES];
}

-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    // if we cancelled, pop controler
    if (buttonIndex == 1)
        [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
//        [self.navigationController dismissModalViewControllerAnimated:YES];
}

-(int)getSelectedIndexForWord:(JapaneseWord*)inWord {
    // already in selected list?
    int selectedIndex = -1;
    for (int i=0; i < selectedWords.count; i++) {
        FlashCard *thisCard = [selectedWords objectAtIndex:i];
        if ([thisCard.kanji isEqualToString:inWord.kanji] && [thisCard.kana isEqualToString:inWord.kana]) {
            selectedIndex = i;
            break;
        }
    }
    return selectedIndex;
}

-(UITableViewCellAccessoryType)tableView:(UITableView*)tableView
		accessoryTypeForRowWithIndexPath:(NSIndexPath*)indexPath {
    // which row are we getting
    NSUInteger section = [indexPath section];
    NSUInteger row = [indexPath row];
    
    // get dictionary entry
    JapaneseWord *word;
    if ((self.searchController.isActive || filterIsActive) && ![self.searchController.searchBar.text isEqualToString:@""])
        word = [japaneseDictionary getEntryUsingGroupIndex:0 entryIndex:[[searchResults objectAtIndex:row] intValue]];
    else
        word = [japaneseDictionary getEntryUsingGroupIndex:section entryIndex:row];
    
    // already in selected list?
    int selectedIndex = [self getSelectedIndexForWord:word];
    
    // if this is already in selected list return checkmark
    if (selectedIndex == -1)
        return UITableViewCellAccessoryNone;
    else
        return UITableViewCellAccessoryCheckmark;
}

-(IBAction)handleLongPressGesture:(UIGestureRecognizer *)gestureRecognizer {
    // gesture begining?
    if (gestureRecognizer.state == UIGestureRecognizerStateBegan) {
    /*
        // create decks using filter
        // get list of words and meanings for JLPT level and word type
        NSArray *senseList = [[JapaneseDictionary getSingleton] getWordSenseListFromJlptLevel:@"JLPTN1" andWordType:@"Expression"];
        // verb, noun, adjective, adverb, expression
        
        // loop through new senses
        for (int i=0; i < senseList.count; i++) {
            //        for (int i=0; i < senseList.count && i < 20; i++) {
            // get dictionary entry
            WordSense *sense = [senseList objectAtIndex:i];
            JapaneseWord *thisWord = [[JapaneseDictionary getSingleton] getWordFromDictionaryId:sense.dictionaryId];
            
            // get notes and array of meanings for this sense
            NSString *notes = nil;
            NSMutableArray *meanings = [NSMutableArray arrayWithCapacity:2];
            for (int s=0; s < thisWord.englishMeanings.count && meanings.count < 3; s++) {
                // if this is the right sense
                NSString *senseIndexString = [thisWord.meaningSense objectAtIndex:s];
                int thisSenseIndex = [senseIndexString intValue];
                if (thisSenseIndex == sense.senseIndex) {
                    // get notes
                    if (notes == nil)
                        notes = [thisWord.meaningNotes objectAtIndex:s];
                    
                    // add to meanings
                    [meanings addObject:[thisWord.englishMeanings objectAtIndex:s]];
                }
            }
            
            // see if this word is not already in the list
            BOOL alreadyFound = NO;
            for (int w=0; w < selectedWords.count; w++) {
                // already there?
                FlashCard *selected = [selectedWords objectAtIndex:w];
                if ([selected.kanji isEqualToString:thisWord.kanji] && [selected.kana isEqualToString:thisWord.kana]) {
                    alreadyFound = YES;
                    break;
                }
            }
            
            // if not already found, create word
            if (!alreadyFound) {
                // now create a card and add to selected words
                FlashCard *newCard = [[FlashCard alloc] initWithKanji:[thisWord kanji] kana:[thisWord kana] meanings:meanings notes:notes];
                [selectedWords addObject:newCard];
                [newCard release];
            }
        }
        
        return;
        */
        
        // did we long press on a row?
        UITableView *thisTableView = (UITableView*)gestureRecognizer.view;
        CGPoint p = [gestureRecognizer locationInView:thisTableView];
        NSIndexPath *indexPath = [thisTableView indexPathForRowAtPoint:p];
        if (indexPath != nil) {
            [self didSelectRowAtIndexPath:indexPath forTableView:thisTableView wasLongPress:YES];
        }
    }
}

-(void)didSelectRowAtIndexPath:(NSIndexPath*)indexPath forTableView:(UITableView*)tableView wasLongPress:(BOOL)wasLongPress {
	// which row are we getting
	NSUInteger section = [indexPath section];
	NSUInteger row = [indexPath row];
    lastSelectedTableView = tableView;
	lastSelectedSection = section;
	lastSelectedRow = row;
	
	// did the user want to multi-select?
	BOOL multiSelect=wasLongPress;
	
    // get dictionary entry
    JapaneseWord *word;
    if ((self.searchController.isActive || filterIsActive) && ![self.searchController.searchBar.text isEqualToString:@""])
        word = [japaneseDictionary getEntryUsingGroupIndex:0 entryIndex:[[searchResults objectAtIndex:row] intValue]];
    else
        word = [japaneseDictionary getEntryUsingGroupIndex:section entryIndex:row];
	
    // already in selected list?
    int selectedIndex = [self getSelectedIndexForWord:word];
    
    // select or deselect
    if (selectedIndex == -1) {
        // don't show meanings selector is there is only one
        if ([[word englishMeanings] count] == 1 || !multiSelect) {
            // add new card to selection
            // find number with sense #1
            NSMutableArray *myMeanings = [[NSMutableArray alloc] init];
            for (int i=0; i < [[word englishMeanings] count]; i++) {
                if (![[[word meaningSense] objectAtIndex:i] isEqualToString:@"1"]) {
                    break;
                }
                else {
                    [myMeanings addObject:[[word englishMeanings] objectAtIndex:i]];
                }
            }
            FlashCard *newCard = [[FlashCard alloc] initWithKanji:[word kanji] kana:[word kana] meanings:myMeanings notes:[[word meaningNotes] objectAtIndex:0]];
            [selectedWords addObject:newCard];
            [newCard release];
            [myMeanings release];
            
            // refresh table to display check mark
            [tableView reloadData];
            //				[browseTableView reloadData];
        }
        else {
            // go to a screen to select the wanted meanings
            MultiSelectController *selector = [[MultiSelectController alloc] initWithSelectedItems:nil title:@"Meanings" listSource:[word englishMeanings]];
            selector.delegate = self;
            UINavigationController *secondNavigationController = [[UINavigationController alloc] initWithRootViewController:selector];
            [self.navigationController presentViewController:secondNavigationController animated:YES completion:nil];
//            [self.navigationController presentModalViewController:secondNavigationController animated:YES];
            [secondNavigationController release];
            [selector release];
        }
    }
    else {
        [selectedWords removeObjectAtIndex:selectedIndex];
        [tableView reloadData];
    }
}

-(void)tableView:(UITableView*)tableView
didSelectRowAtIndexPath:(NSIndexPath*) indexPath {
    // call without long press
    [self didSelectRowAtIndexPath:indexPath forTableView:tableView wasLongPress:NO];
}

// called by multi selector of meanings
-(void)didMakeSelection:(NSArray*)selection notes:(NSArray*)inNotes {
    // get dictionary entry
    JapaneseWord *word;
//    if (lastSelectedTableView == self.searchDisplayController.searchResultsTableView)
    if ((self.searchController.isActive || filterIsActive) && ![self.searchController.searchBar.text isEqualToString:@""])
        word = [japaneseDictionary getEntryUsingGroupIndex:0 entryIndex:[[searchResults objectAtIndex:lastSelectedRow] intValue]];
    else
        word = [japaneseDictionary getEntryUsingGroupIndex:lastSelectedSection entryIndex:lastSelectedRow];
    
	// add new card to selection
	FlashCard *newCard = [[FlashCard alloc] initWithKanji:[word kanji] kana:[word kana] meanings:selection notes:[inNotes objectAtIndex:0]];
    [selectedWords addObject:newCard];
	[newCard release];
    
	// refresh table to display check mark
    [lastSelectedTableView reloadData];
}

-(void)didCancelSelection {
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning]; // Releases the view if it doesn't have a superview
    // Release anything that's not essential, such as cached data
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
}

- (void)dealloc {
	[selectedWords release];
    [super dealloc];
}


@end
