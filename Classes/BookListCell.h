//
//  BookListCell.h
//  Mcp
//
//  Created by Ray on 10/25/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface BookListCell : UITableViewCell {
	IBOutlet UIImageView *bookImageView;
	IBOutlet UILabel *nameLabel;

}

@property (nonatomic, retain) UIImageView *bookImageView;
@property (nonatomic, retain) UILabel *nameLabel;

@end
