//
//  SectionBrowserViewController.m
//  HF Sensei
//
//  Created by Ray Price on 2/23/13.
//
//

#import "SectionBrowserViewController.h"
#import "FlashDeck.h"
#import "EditSectionViewController.h"
#import "RenshuuSectionCell.h"

@interface SectionBrowserViewController ()

@end

@implementation SectionBrowserViewController

@synthesize sectionBrowserDelegate;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // base height on autolayout content
    self.tableView.estimatedRowHeight = 128;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    
    // add edit button
	self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    // if we are allowed to select sections to play
    if ([(NSObject*)sectionBrowserDelegate respondsToSelector:@selector(didSelectSection:inDeckNamed:)]) {
        // allow selection
        self.tableView.allowsSelection = YES;
    }
}

-(void)setDeckTo:(FlashDeck*)inDeck {
    // save deck reference or create a new deck
    editDeck = [inDeck retain];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if (self.tableView.editing)
        return editDeck.deckSections.count+1;
    else
        return (editDeck.deckSections.count==0?1:editDeck.deckSections.count);
}

-(IBAction)readingSegmentValueChanged:(id)sender {
    // which list does this belong to?
    UISegmentedControl *thisSegment = (UISegmentedControl*)sender;
    DeckSection *thisSection = [editDeck.deckSections objectAtIndex:thisSegment.tag];
    NSString *listName = thisSection.sectionName;
    
    /*
    // only allow change if kanji section
    if (!thisSegment.selectedSegmentIndex != 2 &&
        ![thisSection.sectionType isEqualToString:@"Kanji"]) {
        // show alert
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Not a Kanji Section" message:@"Only kanji-type sections can be marked as studying or studied for Kanji Renshuu." delegate:nil cancelButtonTitle:@"Understood" otherButtonTitles:nil];
        [alert show];
        [alert release];
        
        // set segment back
        thisSegment.selectedSegmentIndex = 2;
    }
    else {
        // call delegate
        [sectionBrowserDelegate didUpdateReadingStudySwitchForSectionNamed:listName inDeckNamed:editDeck.deckName toValue:thisSegment.selectedSegmentIndex];
    }*/
    
    // call delegate
    [sectionBrowserDelegate didUpdateReadingStudySwitchForSectionNamed:listName inDeckNamed:editDeck.deckName toValue:thisSegment.selectedSegmentIndex];
}

-(IBAction)writingSegmentValueChanged:(id)sender {
    // which list does this belong to?
    UISegmentedControl *thisSegment = (UISegmentedControl*)sender;
    DeckSection *thisSection = [editDeck.deckSections objectAtIndex:thisSegment.tag];
    NSString *listName = thisSection.sectionName;
    
    /*
    // only allow change if kanji section
    if (!thisSegment.selectedSegmentIndex != 2 &&
        ![thisSection.sectionType isEqualToString:@"Kanji"]) {
        // show alert
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Not Kanji Section" message:@"Only kanji-type sections can be marked as studying or studied for Kanji Renshuu." delegate:nil cancelButtonTitle:@"Understood" otherButtonTitles:nil];
        [alert show];
        [alert release];
        
        // set segment back
        thisSegment.selectedSegmentIndex = 2;
    }
    else {
        // call delegate
        [sectionBrowserDelegate didUpdateWritingStudySwitchForSectionNamed:listName inDeckNamed:editDeck.deckName toValue:thisSegment.selectedSegmentIndex];
    }*/
    
    // call delegate
    [sectionBrowserDelegate didUpdateWritingStudySwitchForSectionNamed:listName inDeckNamed:editDeck.deckName toValue:thisSegment.selectedSegmentIndex];
}

/*
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *productName = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleName"] ;
    if ([productName isEqualToString:@"Kanji Renshuu"]) {
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
            return 95;
        else
            return 186;
    }
    else
        return 44;
}*/

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // add new row, or deck row
    if (indexPath.row >= editDeck.deckSections.count) {
        UITableViewCell *thisCell;
        if (editDeck.deckSections.count == 0 && !self.tableView.isEditing)
            // don't use forindexpath as it's not on ios5
            thisCell = [tableView dequeueReusableCellWithIdentifier:@"PressAddCell"];
        else
            // don't use forindexpath as it's not on ios5
            thisCell = [tableView dequeueReusableCellWithIdentifier:@"AddCell"];
        return thisCell;
    }
    else {
        // if this is kanji renshuu use the studied/studying/not studied version
        // if we have icloud access
        NSString *productName = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleName"] ;
        if ([productName isEqualToString:@"Kanji Renshuu"]) {
            // don't use FORINDEXPATH version, as it's not on ios5
            RenshuuSectionCell *thisCell = [tableView dequeueReusableCellWithIdentifier:@"RenshuuCell"];
            DeckSection *thisSection = [editDeck.deckSections objectAtIndex:indexPath.row];
            thisCell.nameLabel.text = thisSection.sectionName;
            thisCell.readingSegment.tag = indexPath.row;
            thisCell.writingSegment.tag = indexPath.row;
            
            // set segment value
            thisCell.readingSegment.selectedSegmentIndex = [sectionBrowserDelegate getReadingStudySwitchValueForSectionNamed:thisSection.sectionName inDeckNamed:editDeck.deckName];
            thisCell.writingSegment.selectedSegmentIndex = [sectionBrowserDelegate getWritingStudySwitchValueForSectionNamed:thisSection.sectionName inDeckNamed:editDeck.deckName];
            
            // return cell
            return thisCell;
        }
        else {
            // don't use FORINDEXPATH version, as it's not on ios5
            UITableViewCell *thisCell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
            DeckSection *thisSection = [editDeck.deckSections objectAtIndex:indexPath.row];
            thisCell.textLabel.text = thisSection.sectionName;
            
            return thisCell;
        }
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // if editing a deck, pass deck reference
    if ([segue.identifier isEqualToString:@"EditSectionSegue"]) {
        EditSectionViewController *editController = (EditSectionViewController *)segue.destinationViewController;
        [editController setSectionTo:sender];
        editController.delegate = self;
    }
}

-(void)tableView:(UITableView*)tableView didSelectRowAtIndexPath:(NSIndexPath*) indexPath {
	// find row
	NSUInteger row = [indexPath row];
	
	// editing mode?
	if (self.editing) {
		// add new?
		if (row >= editDeck.deckSections.count) {
			[self tableView:tableView commitEditingStyle:UITableViewCellEditingStyleInsert forRowAtIndexPath:indexPath];
		}
		else {
            // perform segue to show section editor browser
            [self performSegueWithIdentifier:@"EditSectionSegue" sender:[editDeck.deckSections objectAtIndex:row]];
		}
	}
	else {
        // if we have select method
        if ([(NSObject*)sectionBrowserDelegate respondsToSelector:@selector(didSelectSection:inDeckNamed:)]) {
            // call delegate
            [sectionBrowserDelegate didSelectSection:[[editDeck.deckSections objectAtIndex:row] sectionName] inDeckNamed:editDeck.deckName];
        }
	}
}

- (void)setEditing:(BOOL)editing animated:(BOOL)animated {
    // on ios5, you can't re-open once it's closed!
    if (editing) {
        // open afresh
        NSString *deckName = [editDeck.deckName retain];
        [editDeck release];
        editDeck = [[FlashDeck alloc] initWithName:deckName];
        [deckName release];
        [editDeck openWithCompletionHandler:^(BOOL success) {
            // add/remove placeholder items
            NSMutableArray *indexPaths = [NSMutableArray array];
            [indexPaths addObject:[NSIndexPath indexPathForRow:editDeck.deckSections.count inSection:0]];
            [self.tableView beginUpdates];
            [super setEditing:editing animated:animated];
            [self.tableView setEditing:editing animated:YES];

            // delete 'press edit' message
            if (editDeck.deckSections.count == 0)
                [self.tableView deleteRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationTop];
            
            // Show the placeholder rows and hide back button
            [self.tableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationTop];
            self.navigationItem.hidesBackButton = YES;
            
            // finish updates
            [self.tableView endUpdates];
        }];
    }
    else {
        [editDeck closeWithCompletionHandler:^(BOOL success) {
            // add/remove placeholder items
            NSMutableArray *indexPaths = [NSMutableArray array];
            [indexPaths addObject:[NSIndexPath indexPathForRow:editDeck.deckSections.count inSection:0]];
            [self.tableView beginUpdates];
            [super setEditing:editing animated:animated];
            [self.tableView setEditing:editing animated:YES];

            // Hide the placeholder rows and show back button again
            [self.tableView deleteRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationTop];
            self.navigationItem.hidesBackButton = NO;
            
            // show 'press edit' message
            if (editDeck.deckSections.count == 0)
                [self.tableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationTop];
            
            // finish updates
            [self.tableView endUpdates];
        }];
    }
    
    /*
    // if we're going into editing mode, open deck, otherwise close it!
    if (editing) {
        [editDeck openWithCompletionHandler:^(BOOL success) {
            // do nothing
        }];
    }
    else {
        [editDeck closeWithCompletionHandler:^(BOOL success) {
            // do nothing
        }];
    }*/
}

-(UITableViewCellEditingStyle)tableView:(UITableView*)tableView editingStyleForRowAtIndexPath:(NSIndexPath*)indexPath {
	// if we are in edit mode
	if (self.editing) {
		// show delete style and insert for 'add new'
		if (indexPath.row != editDeck.deckSections.count)
			return UITableViewCellEditingStyleDelete;
		else
			return UITableViewCellEditingStyleInsert;
	}
	return UITableViewCellEditingStyleNone;
}

// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

-(BOOL)tableView:(UITableView*)tableView canMoveRowAtIndexPath:(NSIndexPath*)indexPath {
	// can't re-order 'add new'
	if (indexPath.row < editDeck.deckSections.count)
		return YES;
	else
		return NO;
}

-(void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
	// get from and to row
	NSUInteger fromRow = [fromIndexPath row];
	NSUInteger toRow = [toIndexPath row];
	
    // if row is before the 'add new' row
    if (toRow < editDeck.deckSections.count) {
        // swap these rows in the array
        DeckSection *moveSection = [[editDeck.deckSections objectAtIndex:fromRow] retain];
        [editDeck.deckSections removeObjectAtIndex:fromRow];
        [editDeck.deckSections insertObject:moveSection atIndex:toRow];
        [moveSection release];
        
        // save deck changes
        [editDeck saveCheckingOldName:editDeck.deckName completion:^{
            // do nothing
        }];
    }
    
    // reload in case we didn't move it
    [tableView reloadData];
}


// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    // if we are deleting rows
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [editDeck.deckSections removeObjectAtIndex:indexPath.row];
        [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:YES];
        
        // save deck changes
        [editDeck saveCheckingOldName:editDeck.deckName completion:^{
            // do nothing
        }];
    }
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // perform segue to edit deck, passing nil as deck
        [self performSegueWithIdentifier:@"EditSectionSegue" sender:nil];
	}
}

-(void)didUpdateObject:(id)updatedObject key:objectKey {
    // refresh list
	[self.tableView reloadData];
    
    // save deck
    [editDeck saveCheckingOldName:editDeck.deckName completion:^{
    }];
}

-(void)didAddNewObject:(id)newObject {
    // add new section to section list
    DeckSection *newSection = (DeckSection*)newObject;
    [editDeck.deckSections addObject:newSection];
    
    // refresh list
	[self.tableView reloadData];
    
    // save deck
    [editDeck saveCheckingOldName:editDeck.deckName completion:^{
    }];
}

-(void)dealloc {
    // release our retains
    [editDeck release];
    
    // call super
    [super dealloc];
}

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

@end
