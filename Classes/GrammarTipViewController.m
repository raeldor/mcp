//
//  GrammarTipViewController.m
//  Mcp
//
//  Created by Ray on 1/4/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "GrammarTipViewController.h"


@implementation GrammarTipViewController

@synthesize tipWebView;
@synthesize closeButton;

/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
 - (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
 if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
 // Custom initialization
 }
 return self;
 }
 */

/*
 // Implement loadView to create a view hierarchy programmatically, without using a nib.
 - (void)loadView {
 }
 */

 // Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
 - (void)viewDidLoad {
	 [super viewDidLoad];
	 
//	 tipWebView.opaque = NO;
	 tipWebView.backgroundColor = [UIColor clearColor];
 }

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
	[tipWebView release];
	[closeButton release];
    [super dealloc];
}


@end
