//
//  CorrectionMeaningActionSheet.h
//  Mcp
//
//  Created by Ray Price on 1/17/13.
//
//

#import <UIKit/UIKit.h>
#import "JapaneseDictionary.h"
#import "CorrectionSelection.h"

@interface CorrectionMeaningActionSheet : UIActionSheet <UIPickerViewDelegate, UIPickerViewDataSource> {
    JapaneseWord *wordEntry;
    NSMutableArray *correctionSelections;
    CorrectionSelection *selectedCorrection;
}

-(id)initWithTitle:(NSString*)inTitle delegate:(id<UIActionSheetDelegate>)inDelegate andWord:(JapaneseWord*)inWord;
-(void)didPressPickerCancel:(id)sender;
-(void)didPressPickerDone:(id)sender;

@end
