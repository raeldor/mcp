/*
 *  EditableTableViewControllerDelegate.h
 *  Mcp
 *
 *  Created by Ray on 10/21/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */


@protocol EditableTableViewControllerDelegate

-(int)getListCount;
-(NSString*)getListCellIdentifier;

@end
