//
//  TrainingSample.h
//  Mcp
//
//  Created by Ray on 3/4/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface TrainingSample : NSObject {
	float *sampleBuffer;
	int sampleSize;
}

-(id)initWithBuffer:(float*)inBuffer ofSize:(int)inSize;
-(void)dealloc;

@property (nonatomic, assign) float *sampleBuffer;
@property (nonatomic, assign) int sampleSize;

@end
