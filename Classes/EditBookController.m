//
//  EditBookController.m
//  Mcp
//
//  Created by Ray on 10/25/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "EditBookController.h"
#import "BookPhotoCell.h"
#import "NameCell.h"
#import "Conversation.h"
#import "DialogueLine.h"

@implementation EditBookController

@synthesize book;
@synthesize editableListDelegate;
@synthesize popOver;

#pragma mark -
#pragma mark View lifecycle

-(void)setEditableObject:(id)editableObject isNew:(BOOL)isNew {
	// allow selecting during editor
	self.tableView.allowsSelectionDuringEditing = TRUE;
	
	// save new flag
	newMode = isNew;
	
	// save object
	self.book = editableObject;
    
    /*
    // fix url's
    for (int c=0; c < book.chapters.count; c++) {
        Chapter *thisChapter = [book.chapters objectAtIndex:c];
        for (int i=0; i < thisChapter.conversations.count; i++) {
            Conversation *thisConv = [thisChapter.conversations objectAtIndex:i];
            for (int l=0; l < thisConv.dialogueLines.count; l++) {
                DialogueLine *thisLine = [thisConv.dialogueLines objectAtIndex:l];
                
                NSScanner *myScanner = [NSScanner scannerWithString:thisLine.line];
                NSString *newLine = @"";
                while (![myScanner isAtEnd]) {
                    // get first reference
                    NSString *nextBlock = @"";
                    [myScanner scanUpToString:@"<a href=file://" intoString:&nextBlock];
                    newLine = [newLine stringByAppendingString:nextBlock];
                    if (![myScanner isAtEnd]) {
                        [myScanner scanUpToString:@">" intoString:&nextBlock];
                        nextBlock = [nextBlock stringByReplacingOccurrencesOfString:@"file://" withString:@"\"file://"];
                        nextBlock = [nextBlock stringByAppendingString:@"\""];
                        newLine = [newLine stringByAppendingString:nextBlock];
                    }
                }
                thisLine.line = newLine;
            }
        }
    }
    [book saveAs:book.bookName];
	*/
    
	// this table is always in edit mode
	// we are not adding delete rows, so we don't need edit mode
	//	[self setEditing:YES animated:NO];
	
	// create cancel button
	UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(cancelButtonClick:)];
	self.navigationItem.leftBarButtonItem = cancelButton;
	[cancelButton release];
	
	// create save button
	UIBarButtonItem *saveButton = [[UIBarButtonItem alloc] initWithTitle:@"Save" style:UIBarButtonItemStyleBordered target:self action:@selector(saveButtonClick:)];
	self.navigationItem.rightBarButtonItem = saveButton;
	[saveButton release];
}

-(void)saveButtonClick:(id)sender {
	// check first for errors
	if ([self isOkToSave])
	{
		// notify parent of change
		if (newMode)
			[editableListDelegate didAddNewObject: book];
		else
			[editableListDelegate didUpdateObject: book];
		
		// pop controller
		if (newMode) {
			[self dismissModalViewControllerAnimated:YES];
		}
		else {
			[self.navigationController popViewControllerAnimated:TRUE];
		}
	}
}

-(void)cancelButtonClick:(id)sender {
	// pop controller
	if (newMode) {
		[self dismissModalViewControllerAnimated:YES];
	}
	else {
		[self.navigationController popViewControllerAnimated:TRUE];
	}
}

-(BOOL)isOkToSave {
	BOOL stillOk = YES;
	NSString *msg;
	
	// check fields are filled
    if (book.bookName == nil || [book.bookName isEqualToString:@""]) {
        msg = @"Book name must be completed.";
        stillOk = NO;
    }
	
	// display error if not ok
	if (!stillOk) {
		UIAlertView *myAlert = [[UIAlertView alloc] initWithTitle:@"Oops" message:msg delegate:nil cancelButtonTitle:@"Understood" otherButtonTitles:nil];
		[myAlert show];
		[myAlert release];
	}
	
	return stillOk;
}

#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return 2;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	switch ([indexPath row]) {
		case 1: // picture
			return 100;
			break;
		default:
			break;
	}
	return 44;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *CellIdentifier = @"Cell";
	
	// get identifier and get cell
	NSArray *identifiers = [NSArray arrayWithObjects:@"NameCell", @"BookPhotoCell", nil];
	CellIdentifier = [identifiers objectAtIndex:[indexPath row]];
	UITableViewCell *cell = (NameCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	if (cell == nil) {
		// load cell layout from nib
		NSArray *nib = [[NSBundle mainBundle] loadNibNamed:CellIdentifier owner:self options:nil];
		cell = [nib objectAtIndex:0];
	}
	cell.accessoryType = UITableViewCellAccessoryNone;
	cell.selectionStyle = UITableViewCellSelectionStyleNone;
	
	// now configure new cell
	NameCell *myNameCell;
	BookPhotoCell *myPhotoCell;
	switch ([indexPath row]) {
		case 0: // name
			myNameCell = (NameCell*)cell;
			[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textFieldDidChange:) name:@"UITextFieldTextDidChangeNotification" object:myNameCell.nameTextField];
			myNameCell.nameTextField.text = book.bookName;
			break;
		case 1: // picture
			myPhotoCell = (BookPhotoCell*)cell;
			myPhotoCell.bookImageView.image = book.image;
			myPhotoCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
			myPhotoCell.selectionStyle = UITableViewCellSelectionStyleBlue;
			break;
		default:
			break;
	}
    
    return cell;
}

-(UITableViewCellEditingStyle)tableView:(UITableView*)tableView editingStyleForRowAtIndexPath:(NSIndexPath*)indexPath {
	return UITableViewCellEditingStyleNone;
}

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */


/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
 
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }   
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }   
 }
 */


/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
 }
 */


/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */


#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	// save last index path for de-highlighting and updating
    [lastIndexPath release];
	lastIndexPath = [indexPath retain];
	
	switch ([indexPath row]) {
		case 1: // picture
		{
			// show action sheet
			UIActionSheet *action = [[UIActionSheet alloc] init];
			if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary])
				[action addButtonWithTitle:@"Choose Existing Photo"];
			if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
				[action addButtonWithTitle:@"Take Photo"];
			if ( book.image != nil)
				[action addButtonWithTitle:@"Delete Photo"];
			[action addButtonWithTitle:@"Cancel"];
			action.cancelButtonIndex = action.numberOfButtons-1;
			action.delegate = self;
			[action showInView:self.view];
			[action release];
		}
			break;
		default:
			break;
	}
}

-(void)textFieldDidChange:(NSNotification*)note {
	 book.bookName = [note.object performSelector:@selector(text)];
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
	// use button title
	NSString *buttonTitle = [actionSheet buttonTitleAtIndex:buttonIndex];
	
	// ipad?
	BOOL isIpad = NO;
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 30200
	if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
		isIpad = YES;
#endif
	
	// if we're using picker
	if ([buttonTitle isEqualToString:@"Choose Existing Photo"] || [buttonTitle isEqualToString:@"Take Photo"]) {
		// use image picker
		UIImagePickerController	*picker = [[UIImagePickerController alloc] init];
		picker.delegate = self;
		picker.sourceType = [buttonTitle isEqualToString:@"Choose Existing Photo"] ? UIImagePickerControllerSourceTypePhotoLibrary:UIImagePickerControllerSourceTypeCamera;
		
		// embed in popover for iPad
		if (isIpad) {
			UIPopoverController *myPopOver = [[NSClassFromString(@"UIPopoverController") alloc] initWithContentViewController:picker];
			[myPopOver presentPopoverFromRect:self.view.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
			self.popOver = myPopOver;
			[myPopOver release];
		}
		else {
			[self presentModalViewController:picker animated:YES];
		}
		// release picker
		[picker release];
	}
	if ([buttonTitle isEqualToString:@"Delete Photo"]) {
		 book.image = nil;
		[self.tableView reloadData];
	}
	if ([buttonTitle isEqualToString:@"Delete Photo"] || [buttonTitle isEqualToString:@"Cancel"]) {
		// deselect any currently selected row
		[self.tableView deselectRowAtIndexPath:lastIndexPath animated:YES];
	}
}

-(void)imagePickerController:(UIImagePickerController*)picker didFinishPickingImage:(UIImage*)image editingInfo:(NSDictionary*)editingInfo {
	// ipad?
	BOOL isIpad = NO;
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 30200
	if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
		isIpad = YES;
#endif
	
	// save image, but resize to 57 x 76
	CGSize newSize = CGSizeMake(57, 76);
	UIGraphicsBeginImageContext(newSize	);
	[image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
	UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	 book.image = newImage;
	
	// close picker and refresh view
	if (isIpad) {
		//		[popOver release];
	}
	else {
		[picker dismissModalViewControllerAnimated:YES];
	}
	[self.tableView reloadData];
}

#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    // Relinquish ownership of anything that can be recreated in viewDidLoad or on demand.
    // For example: self.myOutlet = nil;
}


- (void)dealloc {
    [lastIndexPath release];
	[book release];
	[popOver release];
    [super dealloc];
}


@end

