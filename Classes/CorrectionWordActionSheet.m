//
//  CorrectionWordActionSheet.m
//  Mcp
//
//  Created by Ray Price on 1/17/13.
//
//

#import "CorrectionWordActionSheet.h"
#import "CorrectionSelection.h"
#import "TextFunctions.h"

@implementation CorrectionWordActionSheet

@synthesize selectedWord;

-(id)initWithTitle:(NSString*)inTitle delegate:(id<UIActionSheetDelegate>)inDelegate andSampleWord:(SampleEntryWord*)inSampleWord {
    self = [super initWithTitle:inTitle delegate:inDelegate cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:nil, nil];
    if (self) {
        // save word entry
        sampleEntryWord = [inSampleWord retain];
        
        // get dictionary word
        dictionaryWord = [[[JapaneseDictionary getSingleton] getWordFromDictionaryId:inSampleWord.dictionaryId] retain];
        
        // create the picker
        UIPickerView *wordPicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0.0, 44.0, 0.0, 0.0)];
        wordPicker.showsSelectionIndicator = YES;
        wordPicker.delegate = self;
        wordPicker.dataSource = self;
        
        // create toolbar for cancel, done at top
        UIToolbar *pickerToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
        pickerToolbar.barStyle = UIBarStyleBlackOpaque;
        [pickerToolbar sizeToFit];
        
        // create bar buttons for bar
        NSMutableArray *barItems = [[NSMutableArray alloc] init];
        UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(didPressPickerCancel:)];
        [barItems addObject:cancelButton];
        UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
        [barItems addObject:flexSpace];
        UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(didPressPickerDone:)];
        [barItems addObject:doneButton];
        [flexSpace release];
        [cancelButton release];
        [doneButton release];
        
        // add buttons to toolbar and release
        [pickerToolbar setItems:barItems animated:YES];
        [barItems release];
        
        // add toolbar and picker to action sheet
        [self addSubview:pickerToolbar];
        [self addSubview:wordPicker];
        
        // release objects
        [pickerToolbar release];
        [wordPicker release];
        
        // which reading do we use?  if dictionary and sample kanji are different, expect
        // conjugation and use dictionary reading
        NSString *reading = sampleEntryWord.kanji;
        if (![reading isEqualToString:dictionaryWord.kanji]) {
            // if original was all kana, use kana for search so we can switch verbs
            if (![TextFunctions containsKanji:reading])
                reading = dictionaryWord.kana;
            else
                reading = dictionaryWord.kanji;
        }
        
        // get a list of words that have same reading
        NSArray *words = [[JapaneseDictionary getSingleton] getIndexListFromSearchString:reading usingSearchScope:2];
        
        // create new array of correction selections
        correctionSelections = [[NSMutableArray alloc] initWithCapacity:10];
        
        // create array of words, and meanings etc.
        int defaultIndex = 0;
        for (int w=0; w < words.count; w++) {
            // get word
            NSString *indexString = [words objectAtIndex:w];
            int kanaIndex = [indexString intValue];
            JapaneseWord *dictWord = [[JapaneseDictionary getSingleton] getEntryUsingGroupIndex:0 entryIndex:kanaIndex];
            
            // create new correction selection
            CorrectionSelection *selection = [[CorrectionSelection alloc] initWithDictionaryEntry:dictWord];
            
            // if this is our current word, make this the default selection
            if (dictWord.dictionaryId == inSampleWord.dictionaryId) {
                selectedWord = [dictWord retain];
                defaultIndex = w;
            }
            
            // add to list
            [correctionSelections addObject:selection];
            [selection release];
        }
        
        // set default index
        [wordPicker selectRow:defaultIndex inComponent:0 animated:NO];
    }
    
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void)didPressPickerCancel:(id)sender {
    // dismiss picker
    [self dismissWithClickedButtonIndex:0 animated:YES];
}

-(void)didPressPickerDone:(id)sender {
    // dismiss picker
    [self dismissWithClickedButtonIndex:1 animated:YES];
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView*)pickerView {
	return 2;
}

-(NSInteger)pickerView:(UIPickerView*)pickerView numberOfRowsInComponent:(NSInteger)component {
    // word?
    if (component == 0)
        return correctionSelections.count;
    else {
        return 1; // later return # pronunciations
    }
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    // get selection
    CorrectionSelection *selection;
    if (component == 0)
        selection = [correctionSelections objectAtIndex:row];
    else
        selection = [correctionSelections objectAtIndex:[pickerView selectedRowInComponent:0]];
    
    // word?
    if (component == 0)
        return selection.kanji;
    else
        return selection.kana;
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    // if we selected a kanji
    if (component == 0) {
        // update list of readings
        [pickerView reloadComponent:1];
        
        // save word
        [selectedWord release];
        selectedWord = [[correctionSelections objectAtIndex:[pickerView selectedRowInComponent:0]] retain];
    }
}

/*
-(UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view {
    // if we don't have a view already
    UILabel *thisLabel;
	if (view == nil) {
        // create a new one
		UILabel *newLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, (self.frame.size.width-20)/2, 20)];
		newLabel.font = [UIFont boldSystemFontOfSize:14];
		newLabel.lineBreakMode = UILineBreakModeWordWrap;
		newLabel.numberOfLines = 2;
		newLabel.backgroundColor = [UIColor clearColor];
		newLabel.opaque = NO;
		newLabel.textAlignment = UITextAlignmentCenter;
		
        // use this label
        thisLabel = newLabel;
		[newLabel autorelease];
	}
	else {
        // change text on existing label
		thisLabel = (UILabel*)view;
	}
    
    // get selection
    CorrectionSelection *selection;
    if (component == 0)
        selection = [correctionSelections objectAtIndex:row];
    else
        selection = [correctionSelections objectAtIndex:[pickerView selectedRowInComponent:0]];
    
    // word?
    if (component == 0) {
        thisLabel.text = selection.kanji;
    }
    else {
        thisLabel.text = selection.kana;
    }
    
    return thisLabel;
}
*/

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

-(void) dealloc {
    // free retains
    [sampleEntryWord release];
    [dictionaryWord release];
    [selectedWord release];
    
    // call super
    [super dealloc];
}

@end
