//
//  Grammar.m
//  Mcp
//
//  Created by Ray on 1/7/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "GrammarNote.h"


@implementation GrammarNote

@synthesize title;
@synthesize text;

-(id)init {
	if (self = [super init]) {
		self.title = @"";
		self.text = @"";
	}
	return self;
}

-(id)initWithCoder:(NSCoder*)coder {
	if (self = [super init]) {
		self.title = [coder decodeObjectForKey:@"Title"];
		self.text = [coder decodeObjectForKey:@"Text"];
	}
	return self;
}

-(void)encodeWithCoder:(NSCoder*)coder {
	[coder encodeObject:title forKey:@"Title"];
	[coder encodeObject:text forKey:@"Text"];
}

-(void)dealloc {
	[title release];
	[text release];
	[super dealloc];
}

@end
