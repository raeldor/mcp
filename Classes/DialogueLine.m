//
//  DialogueLine.m
//  Mcp
//
//  Created by Ray on 10/23/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "DialogueLine.h"


@implementation DialogueLine

@synthesize characterIndex;
@synthesize line;

-(id)init {
	if ((self = [super init])) {
		// not null by default
		self.line = @"";
	}
	return self;
}

-(id)initWithCoder:(NSCoder*)coder {
	if ((self = [super init])) {
		NSNumber *indexNum = [coder decodeObjectForKey:@"CharacterIndex"];
		self.characterIndex = [indexNum intValue];
		self.line = [coder decodeObjectForKey:@"Line"];
	}
	return self;
}

-(void)encodeWithCoder:(NSCoder*)coder {
	[coder encodeObject:[NSNumber numberWithInt:self.characterIndex] forKey:@"CharacterIndex"];
	[coder encodeObject:line forKey:@"Line"];
}

-(id)copyWithZone:(NSZone *)zone {
    // return a deep copy of this object
    DialogueLine *newLine = [[DialogueLine alloc] init];
    newLine.characterIndex = characterIndex;
    newLine.line = [[line copy] autorelease];
    return newLine;
}

-(NSString*)getLineAsText {
	
	// remove remove anything between <>
	NSString *newLine = @"";
	BOOL insideBrackets = NO;
	for (int i=0; i < [line length]; i++) {
		// check for being inside bracket
		if ([@"<" rangeOfString:[line substringWithRange:NSMakeRange(i, 1)]].location != NSNotFound) {
			// set flag
			insideBrackets = YES;
		}
		else {
			if ([@">" rangeOfString:[line substringWithRange:NSMakeRange(i, 1)]].location != NSNotFound) {
				// set flag
				insideBrackets = NO;
			}
			else {
				if (!insideBrackets) {
					if (![[line substringWithRange:NSMakeRange(i, 1)] isEqualToString:@"\n"])
						newLine = [newLine stringByAppendingString:[line substringWithRange:NSMakeRange(i, 1)]];
				}
			}
		}
	}		
	
	return newLine;
}

-(void)dealloc {
	[line release];
	[super dealloc];
}

@end
