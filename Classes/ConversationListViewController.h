//
//  ConversationListViewController.h
//  Mcp
//
//  Created by Ray on 10/25/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EditableListDelegate.h"
#import "Book.h"
#import "TouchyTableView.h"

@interface ConversationListViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, EditableListDelegate, UIAlertViewDelegate> {
    IBOutlet TouchyTableView *myTableView;
	NSIndexPath *lastIndexPath;
	Chapter *chapter;
	Book *book;
}

@property (nonatomic, retain) TouchyTableView *myTableView;
@property (nonatomic, retain) Chapter *chapter;
@property (nonatomic, retain) Book *book;

-(void)setEditableObject:(Chapter *)inChapter inBook:(Book*)inBook;

@end
