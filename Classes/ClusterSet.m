//
//  ClusterSet.m
//  Mcp
//
//  Created by Ray on 3/20/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "ClusterSet.h"

@implementation ClusterSet

@synthesize clusters;
@synthesize eps;

-(id)initWithDataSet:(DataSet*)inDataSet eps:(float)inEps minPts:(int)inMinPts {
	if (self = [super init]) {
		// create array for holding clusters
		self.clusters = [NSMutableArray arrayWithCapacity:10];
		eps = inEps;
		
		// loop through unvisited data points
		DataPoint *P;
		while ((P = [inDataSet getNextUnvisitedDataPoint])) {
			// mark as visited
			P.visited = YES;
			
			// get neighbors
			DataSet *N = [inDataSet getNeighborsOfPoint:P UsingEps:inEps];
			
			// mark as noise if no close points over min
//			if (N.dataPoints.count < inMinPts)
//				P.noise = NO;
//			else {
				Cluster *newCluster = [self getNewCluster];
				[newCluster expandClusterUsingPoint:P withNeighbors:N inDataSet:inDataSet withEps:inEps andMinPts:inMinPts];
//			}
		}
	}
	return self;
}

-(Cluster*)getNewCluster {
	// create new cluster and add to list
	Cluster *newCluster = [[Cluster alloc] init];
	newCluster.clusterIndex = clusters.count;
	[clusters addObject:newCluster];
	[newCluster release];
	
	return newCluster;
}

//-(bool)containsDataPoint:(DataPoint*)inDataPoint {
//}

-(Cluster*)getClosestClusterToDataPoint:(DataPoint*)inDataPoint {
	// loop through clusters
	float closestDistance = 99999.0f;
	Cluster *closestCluster = nil;
	for (int i=0; i < clusters.count; i++) {
		Cluster *thisCluster = [clusters objectAtIndex:i];
		float distance = [thisCluster getDistanceOfClosestPointToPoint:inDataPoint];
		if (distance < closestDistance) {
			closestDistance = distance;
			closestCluster = thisCluster;
		}
	}
	return closestCluster;
}

-(Cluster*)getClusterForNewDataPoint:(DataPoint*)inDataPoint {
	// loop through clusters
	for (int i=0; i < clusters.count; i++) {
		Cluster *thisCluster = [clusters objectAtIndex:i];
		if ([thisCluster hasNeighborsOfPoint:inDataPoint eps:eps])
			return thisCluster;
	}
	return nil;
}

-(void)dealloc {
	[clusters release];
	[super dealloc];
}

@end
