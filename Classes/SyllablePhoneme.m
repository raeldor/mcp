//
//  SyllablePhoneme.m
//  Mcp
//
//  Created by Ray on 3/3/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "SyllablePhoneme.h"


@implementation SyllablePhoneme

@synthesize phonemeName;
@synthesize alreadyTrained;
@synthesize startPercent;
@synthesize endPercent;

-(id)initWithDictionary:(NSDictionary*)inDictionary {
	if (self = [super init]) {
		self.phonemeName = [inDictionary objectForKey:@"PhonemeName"];
		alreadyTrained = [[inDictionary objectForKey:@"AlreadyTrained"] boolValue];;
		startPercent = [[inDictionary objectForKey:@"StartPercent"] floatValue];;
		endPercent = [[inDictionary objectForKey:@"EndPercent"] floatValue];;
	}
	return self;
}

- (void)dealloc {
	[phonemeName release];
	[super dealloc];
}

@end
