//
//  CalibrateViewController.h
//  Mcp
//
//  Created by Ray Price on 9/17/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface CalibrateViewController : UIViewController {
    IBOutlet UILabel *samplesLabel;
    IBOutlet UILabel *iterationsLabel;
    IBOutlet UILabel *errorSumLabel;
    IBOutlet UILabel *startingRateLabel;
    IBOutlet UILabel *newRateLabel;
    IBOutlet UIProgressView *startingRateProgress;
    IBOutlet UIProgressView *newRateProgress;
    IBOutlet UIButton *calibrateButton;
    
    NSTimer *updateTimer;
    
    BOOL isCalibrating;
    BOOL stopCalibrating;
    int currentIteration;
    float currentErrorSum;
    float currentLowestScore;
    float currentHighestScore;
}

-(IBAction)calibrateButtonPressed:(id)sender;
-(void)doCalibration;
-(void)updateTimerFired:(NSTimer*)inTimer;
-(float)getTrainingSamplesLowestScore; // lowest of correct ones
-(float)getTrainingSamplesHighestScore; //highest of incorrect ones
-(int)getTotalSampleCount;

@end
