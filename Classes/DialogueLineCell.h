//
//  DialogueLineCell.h
//  Mcp
//
//  Created by Ray on 11/4/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface DialogueLineCell : UITableViewCell {
	IBOutlet UIButton *characterButton;
	IBOutlet UILabel *characterLabel;
	IBOutlet UIWebView *lineWebView;
}

@property (nonatomic, retain) UIButton *characterButton;
@property (nonatomic, retain) UILabel *characterLabel;
@property (nonatomic, retain) UIWebView *lineWebView;

@end
