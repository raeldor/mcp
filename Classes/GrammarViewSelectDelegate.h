/*
 *  GrammarViewSelectDelegate.h
 *  Mcp
 *
 *  Created by Ray on 1/14/11.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */

#import <Foundation/Foundation.h>

@protocol GrammarViewSelectDelegate

@optional

-(void)didSelectGrammar;

@end
