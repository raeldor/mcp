//
//  BackgroundListCell.m
//  Mcp
//
//  Created by Ray on 10/20/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "BackgroundListCell.h"


@implementation BackgroundListCell

@synthesize backgroundImageView;
@synthesize nameLabel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if ((self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])) {
        // Initialization code
    }
    return self;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {

    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (void)dealloc {
	[backgroundImageView release];
	[nameLabel release];
    [super dealloc];
}


@end
