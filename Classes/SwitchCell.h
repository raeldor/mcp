//
//  SwitchCell.h
//  Mcp
//
//  Created by Ray on 4/22/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SwitchCell : UITableViewCell {
    IBOutlet UILabel *textLabel;
    IBOutlet UISwitch *mySwitch;
}

@property (nonatomic, retain) UILabel *textLabel;
@property (nonatomic, retain) UISwitch *mySwitch;

@end
