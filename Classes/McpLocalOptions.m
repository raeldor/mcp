
//
//  AppOptions.m
//  Mcp
//
//  Created by Ray on 4/13/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "McpLocalOptions.h"
#import "McpAppDelegate.h"

@implementation McpLocalOptions

+(McpLocalOptions*)getSingleton {
	// get application delegate
    McpAppDelegate *appDelegate = (McpAppDelegate*)[[UIApplication sharedApplication] delegate];
    
    // return options
    return appDelegate.localOptions;
}

-(id)init {
	if ((self = [super init])) {
    }
    return self;
}

// implement methods to add to dictionary instead of synthesizing
-(NSString*)currentProfileName {
    // get dictionary entry
	NSString *myCurrentProfileName = [localOptions objectForKey:@"CurrentProfileName"];
    if (myCurrentProfileName == nil)
        return @"Default";
    return myCurrentProfileName;
}

-(void)setCurrentProfileName:(NSString *)inCurrentProfileName {
    // set dictionary entry
    [localOptions setObject:inCurrentProfileName forKey:@"CurrentProfileName"];
}

// implement methods to add to dictionary instead of synthesizing
-(NSString*)lastVersionNumber {
    // get dictionary entry
	NSString *myLastVersionNumber = [localOptions objectForKey:@"LastVersionNumber"];
    if (myLastVersionNumber == nil)
        return @"0.0.0";
    return myLastVersionNumber;
}

-(void)setLastVersionNumber:(NSString *)inLastVersionNumber {
    // set dictionary entry
    [localOptions setObject:inLastVersionNumber forKey:@"LastVersionNumber"];
}

// implement methods to add to dictionary instead of synthesizing
-(BOOL)isMicMuted {
    // get dictionary entry
	NSString *myIsMicMuted = [localOptions objectForKey:@"IsMicMuted"];
    if (myIsMicMuted == nil)
        return NO;
    return [myIsMicMuted boolValue];
}

-(void)setIsMicMuted:(BOOL)inIsMicMuted {
    // set dictionary entry
    [localOptions setObject:[NSNumber numberWithBool:inIsMicMuted] forKey:@"IsMicMuted"];
}

// implement methods to add to dictionary instead of synthesizing
-(BOOL)showSubtitles {
    // get dictionary entry
	NSString *myShowSubtitles = [localOptions objectForKey:@"ShowSubtitles"];
    if (myShowSubtitles == nil)
        return YES;
    return [myShowSubtitles boolValue];
}

-(void)setShowSubtitles:(BOOL)inShowSubtitles {
    // set dictionary entry
    [localOptions setObject:[NSNumber numberWithBool:inShowSubtitles] forKey:@"ShowSubtitles"];
}

// implement methods to add to dictionary instead of synthesizing
-(int)pickSentenceType {
    // get dictionary entry
	NSString *myPickSentenceType = [localOptions objectForKey:@"PickSentenceType"];
    if (myPickSentenceType == nil)
        return 0;
    return [myPickSentenceType intValue];
}

-(void)setPickSentenceType:(int)pickSentenceType {
    // set dictionary entry
    [localOptions setObject:[NSNumber numberWithInt:pickSentenceType] forKey:@"PickSentenceType"];
}

// implement methods to add to dictionary instead of synthesizing
-(int)jlptLevel {
    // get dictionary entry
	NSString *myJlptLevel = [localOptions objectForKey:@"JlptLevel"];
    if (myJlptLevel == nil)
        return 0;
    return [myJlptLevel intValue];
}

-(void)setJlptLevel:(int)jlptLevel {
    // set dictionary entry
    [localOptions setObject:[NSNumber numberWithInt:jlptLevel] forKey:@"JlptLevel"];
}

// implement methods to add to dictionary instead of synthesizing
-(int)sentenceRepeats {
    // get dictionary entry
	NSString *sentenceRepeats = [localOptions objectForKey:@"SentenceRepeats"];
    if (sentenceRepeats == nil)
        return 0;
    return [sentenceRepeats intValue];
}

-(void)setSentenceRepeats:(int)sentenceRepeats {
    // set dictionary entry
    [localOptions setObject:[NSNumber numberWithInt:sentenceRepeats] forKey:@"SentenceRepeats"];
}

// implement methods to add to dictionary instead of synthesizing
-(NSString*)senseiName {
    // get dictionary entry
	NSString *mySenseiName = [localOptions objectForKey:@"SenseiName"];
    if (mySenseiName == nil)
        return @"Miss Jones";
    return mySenseiName;
}

-(void)setSenseiName:(NSString *)senseiName {
    // set dictionary entry
    [localOptions setObject:senseiName forKey:@"SenseiName"];
}

// implement methods to add to dictionary instead of synthesizing
-(int)englishSpeed {
    // get dictionary entry
	NSString *myEnglishSpeed = [localOptions objectForKey:@"EnglishSpeed"];
    if (myEnglishSpeed == nil)
        return 100;
    return [myEnglishSpeed intValue];
}

-(void)setEnglishSpeed:(int)englishSpeed {
    // set dictionary entry
    [localOptions setObject:[NSNumber numberWithInt:englishSpeed] forKey:@"EnglishSpeed"];
}

// implement methods to add to dictionary instead of synthesizing
-(int)japaneseSpeed {
    // get dictionary entry
	NSString *myJapaneseSpeed = [localOptions objectForKey:@"JapaneseSpeed"];
    if (myJapaneseSpeed == nil)
        return 80;
    return [myJapaneseSpeed intValue];
}

-(void)setJapaneseSpeed:(int)japaneseSpeed {
    // set dictionary entry
    [localOptions setObject:[NSNumber numberWithInt:japaneseSpeed] forKey:@"JapaneseSpeed"];
}

// implement methods to add to dictionary instead of synthesizing
-(float)passAccuracy {
    // get dictionary entry
	NSString *myPassAccuracy = [localOptions objectForKey:@"PassAccuracy"];
    if (myPassAccuracy == nil)
        return 0.7;
    return [myPassAccuracy floatValue];
}

-(void)setPassAccuracy:(float)passAccuracy {
    // set dictionary entry
    [localOptions setObject:[NSNumber numberWithFloat:passAccuracy] forKey:@"PassAccuracy"];
}

-(void)dealloc {
	[super dealloc];
}

@end
