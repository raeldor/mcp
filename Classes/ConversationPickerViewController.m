//
//  ConversationPickerViewController.m
//  Mcp
//
//  Created by Ray on 12/22/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "ConversationPickerViewController.h"
#import "Conversation.h"
#import "Chapter.h"
#import "ConversationListCell.h"
#import "DialogueLine.h"
#import "Character.h"
#import "GrammarConvGame.h"

@implementation ConversationPickerViewController

@synthesize playBook;
@synthesize selectedItems;
@synthesize delegate;
@synthesize tableData;

#pragma mark -
#pragma mark View lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
	
	// set title
	self.title = @"Conversations";
	
	// show navigation bar again
	[self.navigationController setNavigationBarHidden:NO animated:YES];
    
	// create cancel button
	UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(cancelButtonClick:)];
	self.navigationItem.leftBarButtonItem = cancelButton;
	[cancelButton release];
	
	// create select button
	UIBarButtonItem *selectButton = [[UIBarButtonItem alloc] initWithTitle:@"Select" style:UIBarButtonItemStyleBordered target:self action:@selector(selectButtonClick:)];
	self.navigationItem.rightBarButtonItem = selectButton;
	[selectButton release];
}

-(void)setBook:(Book*)inBook {
	self.playBook = inBook;
    
    // clear any existing table data
    self.tableData = [NSMutableArray arrayWithCapacity:5];
    
    // loop through chapters
    for (int c=0; c < playBook.chapters.count; c++) {
        Chapter *thisChapter = [playBook.chapters objectAtIndex:c];
        
        // array entry is a dictionary with keys for chapter name and grammar list
        NSMutableDictionary *newEntry = [[NSMutableDictionary alloc] init];
        [newEntry setObject:thisChapter.chapterName forKey:@"ChapterName"];
        [newEntry setObject:thisChapter.uniqueKey forKey:@"ChapterKey"];
        
        // get grammar points into unique list
        NSMutableDictionary *grammarList = [NSMutableDictionary dictionaryWithCapacity:5];
        for (int i=0; i < thisChapter.conversations.count; i++) {
            // get grammar points into array
            NSArray *conversationGrammar = [[thisChapter.conversations objectAtIndex:i] getGrammarListAsArray];
            for (int g=0; g < conversationGrammar.count; g++)
                [grammarList setObject:[conversationGrammar objectAtIndex:g] forKey:[conversationGrammar objectAtIndex:g]];
        }
        
        // add grammar list as array to dictionary entry
        [newEntry setObject:[grammarList allKeys] forKey:@"GrammarList"];
        
        // add new entry to array and release
        [tableData addObject:newEntry];
        [newEntry release];
    }    
}

-(void)setSelectedItems:(NSMutableArray *)inSelectedItems fromBook:(Book*)inBook {
	// create selected items list
	selectedItems = [[NSMutableArray alloc] init];
	
	// save book
    [self setBook:inBook];
	
	// copy items into our list for this book
	for (int i=0; i < inSelectedItems.count; i++) {
        NSString *thisItem = [inSelectedItems objectAtIndex:i];
        if ([thisItem hasPrefix:inBook.uniqueKey])
            [self.selectedItems addObject:[inSelectedItems objectAtIndex:i]];
    }
}

/*
-(IBAction)selectButtonClick:(id)sender {
	// must have at least one item selected
	if ([selectedItems count] == 0) {
		UIAlertView *myAlert = [[UIAlertView alloc] initWithTitle:@"Oops" message:@"You must have at least one item selected." delegate:nil cancelButtonTitle:@"Understood" otherButtonTitles:nil];
		[myAlert show];
		[myAlert release];
		return;
	}
    
    // make sure selection results in a conversation
    if ([GrammarConvGame getConversationPlayListFromGrammarList:selectedItems inBook:playBook].count == 0) {
		UIAlertView *myAlert = [[UIAlertView alloc] initWithTitle:@"Oops" message:@"No individual conversations found for the selected grammar.  Select all grammars for this chapter to practice." delegate:nil cancelButtonTitle:@"Understood" otherButtonTitles:nil];
		[myAlert show];
		[myAlert release];
		return;
    }
	
	// hide navigation bar again
	[self.navigationController setNavigationBarHidden:YES animated:YES];
	
	// pop controller
	[self.navigationController popViewControllerAnimated:YES];
	
	// call delegate to notify about selection
	[delegate didMakeSelection:selectedItems];
}

-(IBAction)cancelButtonClick:(id)sender {	
	// hide navigation bar again
	[self.navigationController setNavigationBarHidden:YES animated:YES];
	
	// pop controler
	[self.navigationController popViewControllerAnimated:YES];
	
	// call to delegate
	[delegate didCancelSelection];
}
*/

#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return tableData.count;
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
	return [[playBook.chapters objectAtIndex:section] chapterName];
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // return row count from table data
    NSDictionary *thisEntry = [tableData objectAtIndex:section];
    NSArray *grammarList = [thisEntry objectForKey:@"GrammarList"];
    return grammarList.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;
}

// Customize the appearance of table view cells.
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	// get regular table cell
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
		cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
    
    // set text to grammar list entry for this chapter
    NSDictionary *thisEntry = [tableData objectAtIndex:[indexPath section]];
    NSArray *grammarList = [thisEntry objectForKey:@"GrammarList"];
    cell.textLabel.text = [grammarList objectAtIndex:[indexPath row]];
    
	return cell;
}

#pragma mark -
#pragma mark Table view delegate

-(UITableViewCellAccessoryType)tableView:(UITableView*)tableView
		accessoryTypeForRowWithIndexPath:(NSIndexPath*)indexPath {
	// build key for this position in the table
    NSDictionary *thisEntry = [tableData objectAtIndex:[indexPath section]];
    NSString *chapterKey = [thisEntry objectForKey:@"ChapterKey"];
    NSArray *grammarList = [thisEntry objectForKey:@"GrammarList"];
    NSString *grammarName = [grammarList objectAtIndex:[indexPath row]];
	NSString *key = [NSString stringWithFormat:@"%@\n%@\n%@", playBook.uniqueKey, chapterKey, grammarName];
	
	// if this is already in selected list return checkmark
	BOOL found = false;
	for (int i=0; i < [selectedItems count]; i++) {
		if ([key isEqualToString:[selectedItems objectAtIndex:i]]) {
			found = YES;
			break;
		}
	}
	if (!found)
		return UITableViewCellAccessoryNone;
	else
		return UITableViewCellAccessoryCheckmark;
}

-(void)tableView:(UITableView*)tableView
didSelectRowAtIndexPath:(NSIndexPath*) indexPath {
	// build key for this position in the table
    NSDictionary *thisEntry = [tableData objectAtIndex:[indexPath section]];
    NSString *chapterKey = [thisEntry objectForKey:@"ChapterKey"];
    NSArray *grammarList = [thisEntry objectForKey:@"GrammarList"];
    NSString *grammarName = [grammarList objectAtIndex:[indexPath row]];
	NSString *key = [NSString stringWithFormat:@"%@\n%@\n%@", playBook.uniqueKey, chapterKey, grammarName];
	
	// add to selected list if not found
	int foundPos = -1;
	for (int i=0; i < [selectedItems count]; i++) {
		if ([key isEqualToString:[selectedItems objectAtIndex:i]]) {
			foundPos = i;
			break;
		}
	}
	if (foundPos == -1) {
		[selectedItems addObject:key];
	}
	else {
		[selectedItems removeObjectAtIndex:foundPos];
	}
	
	// refresh table
	[tableView reloadData];
}

#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    // Relinquish ownership of anything that can be recreated in viewDidLoad or on demand.
    // For example: self.myOutlet = nil;
}


- (void)dealloc {
    [tableData release];
	[playBook release];
	[selectedItems release];
    [super dealloc];
}

@end
