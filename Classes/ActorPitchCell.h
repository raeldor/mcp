//
//  ActorPitchCell.h
//  Mcp
//
//  Created by Ray on 10/21/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface ActorPitchCell : UITableViewCell {
    IBOutlet UILabel *textLabel;
    IBOutlet UILabel *percentLabel;
	IBOutlet UISlider *pitchSlider;
}

@property (nonatomic, retain) UILabel *textLabel;
@property (nonatomic, retain) UILabel *percentLabel;
@property (nonatomic, retain) UISlider *pitchSlider;

-(IBAction)pitchChanged:(id)sender;


@end
