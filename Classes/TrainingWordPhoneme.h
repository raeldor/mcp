//
//  TrainingWordPhoneme.h
//  Mcp
//
//  Created by Ray Price on 11/20/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TrainingWordPhoneme : NSObject {
    NSString *phonemeName;
    float lengthPercent;
}

-(id)initWithDictionaryEntry:(NSDictionary*)inDict;
-(void)dealloc;

@property (nonatomic, retain) NSString *phonemeName;
@property (nonatomic, assign) float lengthPercent;

@end
