//
//  McpLocalOptions.h
//  Mcp
//
//  Created by Ray on 4/13/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DataFile.h"
#import "DataFileDelegate.h"
#import "FlashLocalOptions.h"
#import "FlashConvGame.h"

@interface McpLocalOptions : FlashLocalOptions {
}

+(McpLocalOptions*)getSingleton;
-(id)init;
-(void)dealloc;

@property (nonatomic, assign) NSString *currentProfileName;
@property (nonatomic, assign) NSString *lastVersionNumber;
@property (nonatomic, assign) BOOL isMicMuted;
@property (nonatomic, assign) int pickSentenceType;
@property (nonatomic, assign) int jlptLevel;
@property (nonatomic, assign) int sentenceRepeats;
@property (nonatomic, assign) NSString *senseiName;
@property (nonatomic, assign) int englishSpeed;
@property (nonatomic, assign) int japaneseSpeed;
@property (nonatomic, assign) float passAccuracy;
@property (nonatomic, assign) BOOL showSubtitles;

@end
