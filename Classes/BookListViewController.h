//
//  BookListViewController.h
//  Mcp
//
//  Created by Ray on 10/25/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EditableListDelegate.h"

@interface BookListViewController : UITableViewController <UITableViewDelegate, UITableViewDataSource, EditableListDelegate> {
	NSMutableArray *bookList; // list of NSString*
	NSIndexPath *lastIndexPath;
	NSString *oldBookName;
    int finalEditType;
}

@property (nonatomic, retain) NSMutableArray *bookList;

-(void)setFinalEditTypeAs:(int)inEditType;
-(void)populateBookList;

@end
