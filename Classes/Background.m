//
//  Background.m
//  Mcp
//
//  Created by Ray on 10/19/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "Background.h"


@implementation Background

@synthesize backgroundName;
@synthesize image;

-(id)init {
	if ((self = [super initWithExtension:@".background" delegate:self])) {
	}
	return self;
}

-(id)initFromArchive:(NSString*)inName {
	if ((self = [super initFromArchive:inName extension:@".background" delegate:self])) {
	}
	return self;
}

-(void)archiveFieldsUsingArchiver:(NSKeyedArchiver*)inArchiver {
	// archive fields
	[inArchiver encodeObject:backgroundName forKey:@"BackgroundName"];
	NSData *imageData = UIImageJPEGRepresentation(image, 0.8f);
//	NSData *imageData = UIImagePNGRepresentation(image);
	[inArchiver encodeObject:imageData forKey:@"Image"];
	if (allowDelete)
		[inArchiver encodeObject:@"Yes" forKey:@"AllowDelete"];
	else
		[inArchiver encodeObject:@"No" forKey:@"AllowDelete"];
}

-(void)unarchiveFieldsUsingUnarchiver:(NSKeyedUnarchiver*)inUnarchiver {
	
	// unarchive fields
	self.backgroundName = [inUnarchiver decodeObjectForKey:@"BackgroundName"];
	NSData *imageData = [inUnarchiver decodeObjectForKey:@"Image"];
	UIImage *thisImage = [[UIImage alloc] initWithData:imageData];
	self.image = thisImage;
	[thisImage release];
	NSString *strAllowDelete = [inUnarchiver decodeObjectForKey:@"AllowDelete"];
	if ([strAllowDelete isEqualToString:@"Yes"])
		allowDelete = YES;
	else
		allowDelete = NO;
}

-(void)dealloc {
	[backgroundName release];
	[image release];
	[super dealloc];
}

@end
