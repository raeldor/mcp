//
//  RootViewController.m
//  Mcp
//
//  Created by Ray on 10/14/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "McpAppDelegate.h"
#import "RootViewController.h"
#import "ProCheckerViewController.h"
#import "BackgroundListViewController.h"
#import "ActorListViewController.h"
#import "BookListViewController.h"
#import "ConverseViewController.h"
#import "UserProfileListViewController.h"
#import "CalibrationViewController.h"
#import "GrammarDictionaryViewController.h"
#import "TutorialBrowseController.h"
#import "DeckBrowserViewController.h"

@implementation RootViewController

#pragma mark -
#pragma mark View lifecycle

-(void)viewDidLoad {
    // call super
    [super viewDidLoad];
    
    // have to disable play until we have app options
    playButton.enabled = NO;
    playLabel.textColor = [UIColor grayColor];
    
    // set timer to check for app options being loaded
    optionsCheckCount = 0;
    checkTimer = [[NSTimer scheduledTimerWithTimeInterval:0.5f target:self selector:@selector(checkOptionsTimerFired:) userInfo:nil repeats:YES] retain];
    
    // Do any additional setup after loading the view from its nib.
    self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:0 green:128.0f/255.0f blue:0.0f alpha:1.0f];
    
    // rename title
#ifdef TARGET_FREE
    self.title = @"Hands Free Sensei Free";
#endif
}

-(void)checkOptionsTimerFired:(NSTimer*)inTimer {
    // increment counter
    optionsCheckCount++;
    
    // get application delegate
    McpAppDelegate *appDelegate = (McpAppDelegate*)[[UIApplication sharedApplication] delegate];
    
    // have options been loaded
    if (appDelegate.isOptionsLoaded) {
        // re-enable button
        playButton.enabled = YES;
        playLabel.textColor = [UIColor whiteColor];
        
        // disable timer
        [inTimer invalidate];
    }
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    // if this is cloud warning
    if ([alertView.title isEqualToString:@"iCloud Issue"]) {
        // assert
        NSAssert(0, @"Progress would not load from cloud");
    }
}

-(IBAction)showFlashDecks:(id)sender {
    // get product name
    NSString *productName = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleName"];
    
    // if this is free version
    if ([productName isEqualToString:@"MJCP-Free"]) {
        // show alert instead
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Flash Decks" message:@"Sorry, the option to create your own flash decks is not available in the free version." delegate:nil cancelButtonTitle:@"Understood" otherButtonTitles:nil];
        [alert show];
        [alert release];
        
        return;
    }

    // launch flash decks manually, as it's in a different storyboard
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Storyboard" bundle:nil];
    DeckBrowserViewController *browser=[storyboard instantiateViewControllerWithIdentifier:@"DeckBrowserViewController"];
    [self.navigationController pushViewController:browser animated:YES];
}

// do this here, because prepare is executed after load which takes too long
-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    // if this is the play game
    if ([identifier isEqualToString:@"StartSegue"]) {
        // disable start label too to show we're transitioning
        playLabel.textColor = [UIColor grayColor];
    }
    return [super shouldPerformSegueWithIdentifier:identifier sender:sender];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // always use back
    self.navigationItem.backBarButtonItem.title = @"Back";
}

-(void)viewDidAppear:(BOOL)animated {
    // get application delegate
    McpAppDelegate *appDelegate = (McpAppDelegate*)[[UIApplication sharedApplication] delegate];
    
    // have options been loaded
    if (appDelegate.isOptionsLoaded) {
        // show play as enabled again
        playLabel.textColor = [UIColor whiteColor];
    }
}

#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    // Relinquish ownership of anything that can be recreated in viewDidLoad or on demand.
    // For example: self.myOutlet = nil;
}


- (void)dealloc {
    // release our retains
    [checkTimer release];
    
    // call super
    [super dealloc];
}


@end

