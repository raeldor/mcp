//
//  BackgroundPictureCell.h
//  Mcp
//
//  Created by Ray on 10/19/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface BackgroundPictureCell : UITableViewCell {
	IBOutlet UIImageView *backgroundImageView;
}

@property (nonatomic, retain) UIImageView *backgroundImageView;

@end
