//
//  ConversationPickerViewController.h
//  Mcp
//
//  Created by Ray on 12/22/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "EditableListDelegate.h"
#import "MultiSelectControllerDelegate.h"
#import "Book.h"

@interface ConversationPickerViewController : UITableViewController <UITableViewDelegate, UITableViewDataSource> {
	id<MultiSelectControllerDelegate> delegate;
	Book *playBook;
    NSMutableArray *tableData; // array of dictionary items with keys 'ChapterName', 'GrammarList'
	NSMutableArray *selectedItems; // array of conversationkey objects
}

@property (nonatomic, assign) id<MultiSelectControllerDelegate> delegate;
@property (nonatomic, retain) Book *playBook;
@property (nonatomic, retain) NSMutableArray *selectedItems;
@property (nonatomic, retain) NSMutableArray *tableData;

-(void)setBook:(Book*)inBook;
-(void)setSelectedItems:(NSMutableArray *)inSelectedItems fromBook:(Book*)inBook;

@end
