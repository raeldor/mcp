//
//  NewTrainingSample.h
//  Mcp
//
//  Created by Ray Price on 9/17/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VoiceSample.h"

@interface NewTrainingSample : NSObject <NSCoding> {
    NSString *text;
    int featureCount;
    int size;
    float *userFeatures;
    float *cpuFeatures;
}

-(id)initWithText:(NSString*)inText featureCount:(int)inFeatureCount size:(int)inSize userFeatures:(float*)inUserFeatures cpuFeatures:(float*)inCpuFeatures;

@property (nonatomic, retain) NSString *text;
@property (nonatomic, assign) int featureCount;
@property (nonatomic, assign) int size;
@property (nonatomic, assign) float *userFeatures;
@property (nonatomic, assign) float *cpuFeatures;

@end
