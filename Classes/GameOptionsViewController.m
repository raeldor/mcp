//
//  GameOptionsViewController.m
//  Mcp
//
//  Created by Ray on 4/22/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "GameOptionsViewController.h"
//#import "ActorPitchCell.h"
//#import "SwitchCell.h"
//#import "ShowTextAsCell.h"
//#import "PercentageCell.h"
//#import "PlayPartCell.h"
//#import "SamplesCell.h"
#import "McpAppDelegate.h"
#import "NewTrainingData.h"
#import "NewTrainingSample.h"
#import "CalibrateViewController.h"
#import "Actor.h"
#import "ActorSelectViewController.h"

@implementation GameOptionsViewController

@synthesize myOptions;
@synthesize delegate;

#pragma mark -
#pragma mark View lifecycle

- (void)viewDidLoad {
    // call super
    [super viewDidLoad];
    
    // get application delegate
    McpAppDelegate *appDelegate = (McpAppDelegate*)[[UIApplication sharedApplication] delegate];
    
    // update control values
    pickSentenceSegmentedControl.selectedSegmentIndex = appDelegate.localOptions.pickSentenceType;
    jlptLevelSegmentedControl.selectedSegmentIndex = appDelegate.localOptions.jlptLevel;
    batchSizeSlider.value = appDelegate.localOptions.batchSize;
    englishSpeedSlider.value = appDelegate.localOptions.englishSpeed;
    japaneseSpeedSlider.value = appDelegate.localOptions.japaneseSpeed;
    recognitionAccuracySlider.value = appDelegate.localOptions.passAccuracy*100;
    showSubtitlesSwitch.on = appDelegate.localOptions.showSubtitles;
    sentenceRepeatsSegmentedControl.selectedSegmentIndex = appDelegate.localOptions.sentenceRepeats;
    
    // set sensei name and image
    Actor *sensei = [appDelegate getActorUsingName:appDelegate.localOptions.senseiName];
    if (sensei != nil) {
        senseiNameLabel.text = appDelegate.localOptions.senseiName;
        senseiImageView.image = [sensei.animationFrames objectAtIndex:0];
    }
    else
        senseiNameLabel.text = [NSString stringWithFormat:@"Sensei %@ not found", appDelegate.localOptions.senseiName];
    
    // update slider labels
    [self batchSizeChanged:batchSizeSlider];
    [self accuracyChanged:recognitionAccuracySlider];
    [self englishSpeedChanged:englishSpeedSlider];
    [self japaneseSpeedChanged:japaneseSpeedSlider];
	
    /*
	// set title
	self.title = @"Game Options";
	
	// don't allow selection
	self.tableView.allowsSelection = FALSE;
    
	// show navigation bar again
	[self.navigationController setNavigationBarHidden:NO animated:YES];
    
	// create cancel button
	UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(cancelButtonClick:)];
	self.navigationItem.leftBarButtonItem = cancelButton;
	[cancelButton release];
	
	// create save button
	UIBarButtonItem *saveButton = [[UIBarButtonItem alloc] initWithTitle:@"Save" style:UIBarButtonItemStyleBordered target:self action:@selector(saveButtonClick:)];
	self.navigationItem.rightBarButtonItem = saveButton;
	[saveButton release];
     */
}

-(void)didMakeBrowserSelection:(NSString*)selection fromController:(UIViewController*)selectController {
    // get application delegate
    McpAppDelegate *appDelegate = (McpAppDelegate*)[[UIApplication sharedApplication] delegate];
    
    // update actor image and name
    Actor *selectedActor = [appDelegate getActorUsingName:selection];
    senseiNameLabel.text = selectedActor.actorName;
    senseiImageView.image = [selectedActor.animationFrames objectAtIndex:0];
}

-(void)didCancelBrowserSelection {
    // do nothing
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"SelectActor"]) {
        ActorSelectViewController *selectController = (ActorSelectViewController *)segue.destinationViewController;
        selectController.selectDelegate = self;
    }
}

-(void)saveButtonPressed:(id)sender {
	// check first for errors
	if ([self isOkToSave])
	{
        // get application delegate
        McpAppDelegate *appDelegate = (McpAppDelegate*)[[UIApplication sharedApplication] delegate];
        
        // update options and save
        appDelegate.localOptions.jlptLevel = jlptLevelSegmentedControl.selectedSegmentIndex;
        appDelegate.localOptions.batchSize = batchSizeSlider.value;
        appDelegate.localOptions.englishSpeed = englishSpeedSlider.value;
        appDelegate.localOptions.japaneseSpeed = japaneseSpeedSlider.value;
        appDelegate.localOptions.passAccuracy = recognitionAccuracySlider.value/100.0f;
        appDelegate.localOptions.senseiName = senseiNameLabel.text;
        appDelegate.localOptions.pickSentenceType = pickSentenceSegmentedControl.selectedSegmentIndex;
        appDelegate.localOptions.showSubtitles = showSubtitlesSwitch.on;
        appDelegate.localOptions.sentenceRepeats = sentenceRepeatsSegmentedControl.selectedSegmentIndex;
        [appDelegate.localOptions save];
        
		// notify delegate of change
        [delegate didUpdateOptions];
        
        // hide navigation bar again
//        [self.navigationController setNavigationBarHidden:YES animated:YES];
        
		// pop controller
        UINavigationController *parent = (UINavigationController*)self.parentViewController;
        [parent popViewControllerAnimated:TRUE];
	}
}

-(void)cancelButtonPressed:(id)sender {
    // notify delegate
    [delegate didCancelOptions];
    
	// hide navigation bar again
//	[self.navigationController setNavigationBarHidden:YES animated:YES];
    
	// pop controller
    UINavigationController *parent = (UINavigationController*)self.parentViewController;
    [parent popViewControllerAnimated:TRUE];
}

-(BOOL)isOkToSave {
	BOOL stillOk = YES;
	NSString *msg;
	
	// check fields are filled
	
	// display error if not ok
	if (!stillOk) {
		UIAlertView *myAlert = [[UIAlertView alloc] initWithTitle:@"Oops" message:msg delegate:nil cancelButtonTitle:@"Understood" otherButtonTitles:nil];
		[myAlert show];
		[myAlert release];
	}
	
	return stillOk;
}

#pragma mark -
#pragma mark Table view data source

/*
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    //	[self.gameMenuController.view removeFromSuperview];
    // calibration is now done via menu
    return 1;
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    switch (section) {
        case 0: // game options
            return @"Game Options";
            break;            
        default: // calibration
            return @"Calibration";
            break;
    }
    return nil;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    switch (section) {
        case 0:
            return 6; //make this 6 when we add back grading
            break;
        case 1:
            return 2;
            break;
        default:
            break;
    }
    return 0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch ([indexPath section]) {
        case 0: // game options
            switch ([indexPath row]) {
                case 0: // talking speed
                    return 81;
                    break;
                case 1: // play part
                case 4: // show text as
                    return 91;
                    break;
                case 5: // pass accuracy
                    return 81;
                    break;
                default:
                    break;
            }
            break;
        default: // calibration
            switch ([indexPath row]) {
                case 1: // samples cell
                    return 136;
                    break;
            }
            break;
    }
	return 44;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    // get application delegate
    McpAppDelegate *appDelegate = (McpAppDelegate*)[[UIApplication sharedApplication] delegate];
    
	// get identifier and get cell
    NSString *CellIdentifier = @"Cell";
	NSArray *identifiers;
    switch ([indexPath section]) {
        case 0: // game options
            identifiers = [NSArray arrayWithObjects:@"ActorPitchCell", @"PlayPartCell", @"SwitchCell", @"SwitchCell", @"ShowTextAsCell", @"PercentageCell", nil];
            break;
        default: // calibration
            identifiers = [NSArray arrayWithObjects:@"SwitchCell", @"SamplesCell", nil];
            break;
    }
	CellIdentifier = [identifiers objectAtIndex:[indexPath row]];
	UITableViewCell *cell = (UITableViewCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	if (cell == nil) {
        if ([CellIdentifier isEqualToString:@"Cell"])
            cell = [[UITableViewCell alloc] init];
        else {
            // load cell layout from nib
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:CellIdentifier owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
	}
	
	// get cell re-use name
    //    cell.accessoryType = UITableViewCellAccessoryNone;
    //    cell.selectionStyle = UITableViewCellSelectionStyleNone;
	ActorPitchCell *talkingSpeedCell;
    PlayPartCell *playPartCell;
    SwitchCell *promptPartCell;
    SwitchCell *pauseAfterCell;
    ShowTextAsCell *showTextAsCell;
    PercentageCell *passAccuracyCell;
    SwitchCell *calibrationModeCell;
    SamplesCell *samplesCell;
    switch ([indexPath section]) {
        case 0: // game options
            switch ([indexPath row]) {
                case 0: // talking speed
                    talkingSpeedCell = (ActorPitchCell*)cell;
                    talkingSpeedCell.textLabel.text = @"Talking Speed";
                    talkingSpeedCell.pitchSlider.value = gameOptions.talkingSpeed;
                    talkingSpeedCell.percentLabel.text = [NSString stringWithFormat:@"%d", gameOptions.talkingSpeed];
                    [talkingSpeedCell.pitchSlider addTarget:self action:@selector(speedChanged:) forControlEvents:UIControlEventValueChanged];
                    break;
                case 1: // play part
                    playPartCell = (PlayPartCell*)cell;
                    playPartCell.playSegment.selectedSegmentIndex = gameOptions.playPart;
                    [playPartCell.playSegment addTarget:self action:@selector(playPartChanged:) forControlEvents:UIControlEventValueChanged];
                    break;
                case 2: // prompt part?
                    promptPartCell = (SwitchCell*)cell;
                    promptPartCell.mySwitch.tag = 0;
                    promptPartCell.textLabel.text = @"Prompt My Part";
                    promptPartCell.mySwitch.on = gameOptions.promptMyPart;
                    [promptPartCell.mySwitch addTarget:self action:@selector(switchChanged:) forControlEvents:UIControlEventValueChanged];
                    break;
                case 3: // pause after actor?
                    pauseAfterCell = (SwitchCell*)cell;
                    pauseAfterCell.mySwitch.tag = 1;
                    pauseAfterCell.textLabel.text = @"Pause After Actor";
                    pauseAfterCell.mySwitch.on = gameOptions.pauseAfterActor;
                    [pauseAfterCell.mySwitch addTarget:self action:@selector(switchChanged:) forControlEvents:UIControlEventValueChanged];
                    break;
                case 4: // show text as
                    showTextAsCell = (ShowTextAsCell*)cell;
                    showTextAsCell.asSegment.selectedSegmentIndex = gameOptions.showTextAs;
                    [showTextAsCell.asSegment addTarget:self action:@selector(showTextAsChanged:) forControlEvents:UIControlEventValueChanged];
                    break;
                case 5: // pass accuracy
                    passAccuracyCell = (PercentageCell*)cell;
                    passAccuracyCell.textLabel.text = @"Pass Accuracy";
                    passAccuracyCell.percentSlider.value = gameOptions.passAccuracy;
                    passAccuracyCell.percentLabel.text = [NSString stringWithFormat:@"%3.0f%%", gameOptions.passAccuracy*100.0f];
                    [passAccuracyCell.percentSlider addTarget:self action:@selector(accuracyChanged:) forControlEvents:UIControlEventValueChanged];
                    break;
                default:
                    break;
            }
            break;
        default: // calibration
            switch ([indexPath row]) {
                case 0: // calibration mode
                    calibrationModeCell = (SwitchCell*)cell;
                    calibrationModeCell.mySwitch.tag = 2;
                    calibrationModeCell.textLabel.text = @"Calibration Mode";
                    calibrationModeCell.mySwitch.on = NO;
                    [calibrationModeCell.mySwitch addTarget:self action:@selector(switchChanged:) forControlEvents:UIControlEventValueChanged];
                    break;
                case 1: // clear samples
                {
                    // find total number of samples
                    NewTrainingData *myTrainingData = [appDelegate.currentUserProfile getTrainingDataForAudioDevice];
                    NSEnumerator *enumerator = [myTrainingData.trainingSamples keyEnumerator];
                    NSString *nextKey;
                    int totalSampleCount = 0;
                    while ((nextKey = [enumerator nextObject])) {
                        NewTrainingSample *thisSample = [myTrainingData.trainingSamples objectForKey:nextKey];
                        totalSampleCount += thisSample.size;
                    }

                    // setup the cell
                    samplesCell = (SamplesCell*)cell;
                    samplesCell.samplesLabel.text = [NSString stringWithFormat:@"Samples Collected: %d", totalSampleCount];
                    [samplesCell.calibrateButton addTarget:self action:@selector(calibrateButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
                    [samplesCell.clearButton addTarget:self action:@selector(clearButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
                }
                    break;
                default:
                    break;
            }
            break;
    }
    
    return cell;
}

-(void)clearButtonPressed:(id)sender {
    // confirm first
    UIAlertView *myAlert = [[UIAlertView alloc] initWithTitle:@"Clear History" message:@"Clear all training sample history, are you sure?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    [myAlert show];
    [myAlert release];
}

-(void)calibrateButtonPressed:(id)sender {
    // create view controller
    CalibrateViewController *calibrateController = [[CalibrateViewController alloc] initWithNibName:@"CalibrateViewController" bundle:nil];
    
    // Pass the selected object to the new view controller.
    [self.navigationController pushViewController:calibrateController animated:YES];
    [calibrateController release];
}


-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    // clear history?
    if ([alertView.title isEqualToString:@"Clear History"]) {
        // if we confirmed
        if (buttonIndex == 1) {
            // get application delegate
            McpAppDelegate *appDelegate = (McpAppDelegate*)[[UIApplication sharedApplication] delegate];
            
            // get training data
            NewTrainingData *myTrainingData = [appDelegate.currentUserProfile getTrainingDataForAudioDevice];
            
            // clear history
            [myTrainingData.trainingSamples removeAllObjects];
            
            // refresh tableview to show new samples count
            [self.tableView reloadData];
        }
    }
}

-(UITableViewCellEditingStyle)tableView:(UITableView*)tableView editingStyleForRowAtIndexPath:(NSIndexPath*)indexPath {
	return UITableViewCellEditingStyleNone;
}*/

-(IBAction)showSubtitlesChanged:(id)sender {
    
}

-(IBAction)jlptLevelChanged:(id)sender {
}

-(IBAction)sentenceRepeatsChanged:(id)sender {
    
}

-(IBAction)pickSentenceChanged:(id)sender {
}

-(IBAction)batchSizeChanged:(id)sender {
    // set to closest value
    [batchSizeSlider setValue:((int)((batchSizeSlider.value + 2.5) / 5) * 5) animated:NO];

    // update label
    batchSizeLabel.text = [NSString stringWithFormat:@"%2.0f", batchSizeSlider.value];
}

-(IBAction)englishSpeedChanged:(id)sender {
    // set to closest value
    [englishSpeedSlider setValue:((int)((englishSpeedSlider.value + 2.5) / 5) * 5) animated:NO];

    // update label
    englishSpeedLabel.text = [NSString stringWithFormat:@"%3.0f", englishSpeedSlider.value];
}

-(IBAction)japaneseSpeedChanged:(id)sender {
    // set to closest value
    [japaneseSpeedSlider setValue:((int)((japaneseSpeedSlider.value + 2.5) / 5) * 5) animated:NO];

    // update label
    japaneseSpeedLabel.text = [NSString stringWithFormat:@"%3.0f", japaneseSpeedSlider.value];
}

-(IBAction)accuracyChanged:(id)sender {
    // set to closest value
    [recognitionAccuracySlider setValue:((int)((recognitionAccuracySlider.value + 2.5) / 5) * 5) animated:NO];
    
    // update label
    recognitionAccuracyLabel.text = [NSString stringWithFormat:@"%3.0f%%", recognitionAccuracySlider.value];
}

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */


/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
 
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }   
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }   
 }
 */


/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
 }
 */


/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */


#pragma mark -
#pragma mark Table view delegate

/*
 -(void)viewDidAppear:(BOOL)animated
 {
 // deselect any currently selected row
 [self.tableView deselectRowAtIndexPath:lastIndexPath animated:YES];
 return [super viewDidAppear:animated];
 }
 */

#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    // Relinquish ownership of anything that can be recreated in viewDidLoad or on demand.
    // For example: self.myOutlet = nil;
}

- (void)dealloc {
    [super dealloc];
}


@end

