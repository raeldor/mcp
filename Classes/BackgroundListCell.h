//
//  BackgroundListCell.h
//  Mcp
//
//  Created by Ray on 10/20/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface BackgroundListCell : UITableViewCell {
	IBOutlet UIImageView *backgroundImageView;
	IBOutlet UILabel *nameLabel;
}

@property (nonatomic, retain) UIImageView *backgroundImageView;
@property (nonatomic, retain) UILabel *nameLabel;

@end
