//
//  Conversation.h
//  Mcp
//
//  Created by Ray on 10/23/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DialogueLine.h"

@interface Conversation : NSObject <NSCoding, NSCopying> {
    NSString *uniqueKey;
	NSString *backgroundName;
	NSMutableArray *characters;
	NSMutableArray *dialogueLines;
	BOOL isRomaji;
}

-(NSString*)getGrammarListAsString;
-(NSArray*)getGrammarListAsArray;
-(NSArray*)getGrammarListAsArrayForLine:(DialogueLine*)inLine;
-(NSString*)getDialogHtmlPreview;
-(int)getBestCharacterIndexFromSelectedPractice:(NSArray*)inSelectedPractice;

@property (nonatomic, retain) NSString *uniqueKey;
@property (nonatomic, retain) NSString *backgroundName;
@property (nonatomic, retain) NSMutableArray *characters;
@property (nonatomic, retain) NSMutableArray *dialogueLines;
@property (nonatomic, assign) BOOL isRomaji;

@end
