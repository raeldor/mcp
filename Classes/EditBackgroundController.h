//
//  EditBackgroundController.h
//  Mcp
//
//  Created by Ray on 10/19/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Background.h"
#import "EditableListDelegate.h"

@interface EditBackgroundController : UITableViewController <UITextFieldDelegate, UINavigationControllerDelegate, UIActionSheetDelegate, UIImagePickerControllerDelegate, UITableViewDelegate> {
	BOOL newMode;
	Background *background;
	id<EditableListDelegate> editableListDelegate;
	NSIndexPath *lastIndexPath;	
	
	UIPopoverController *popOver; // for ipad compatibility
}

@property (nonatomic, retain) Background *background;
@property (nonatomic, assign) id<EditableListDelegate> editableListDelegate;
@property (nonatomic, retain) UIPopoverController *popOver;

-(void)setEditableObject:(Background*)thisBackground isNew:(BOOL)isNew ;
-(BOOL)isOkToSave;
-(void)cancelButtonClick:(id)sender;
-(void)saveButtonClick:(id)sender;

-(void)textFieldDidChange:(NSNotification*)note;

@end
