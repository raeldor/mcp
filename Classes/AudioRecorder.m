//
//  AudioRecorder.cpp
//  Mcp
//
//  Created by Ray on 4/16/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#include "AudioRecorder.h"

@implementation AudioRecorder

@synthesize recordState;
@synthesize isMuted;

-(id)initWithSampleRate:(int)inSampleRate startMuted:(BOOL)inStartMuted {
    if (self = [super init]) {
        // set up recording structure
        int sampleRate = inSampleRate;
        recordState.dataFormat.mSampleRate = sampleRate;
        recordState.dataFormat.mFormatID = kAudioFormatLinearPCM;
        recordState.dataFormat.mFramesPerPacket = 1;
        recordState.dataFormat.mChannelsPerFrame = 1;
        recordState.dataFormat.mBytesPerFrame = 4;
        recordState.dataFormat.mBytesPerPacket = 4;
        recordState.dataFormat.mBitsPerChannel = 32;
        recordState.dataFormat.mReserved = 0;
        recordState.dataFormat.mFormatFlags = kLinearPCMFormatFlagIsFloat|kLinearPCMFormatFlagIsPacked;
        
        // not muted by default
        isMuted = inStartMuted;
        
        // create input queue
        AudioQueueNewInput(&recordState.dataFormat, AudioInputCallback5, &recordState, CFRunLoopGetCurrent(), kCFRunLoopCommonModes, 0, &recordState.queue);
        
        // allocate memory and queue buffers
        for (int i=0; i < NUM_BUFFERS; i++)
        {
            int BUFFER_SIZE = sampleRate;
            AudioQueueAllocateBuffer(recordState.queue, BUFFER_SIZE*4, &recordState.buffers[i]);
            printf("requeue buffer %p\r\n", recordState.buffers[i]->mAudioData);
            AudioQueueEnqueueBuffer(recordState.queue, recordState.buffers[i], 0, NULL);
        }
        
        // allocate buffer long enough for 10 seconds recording
        recordState.recordBufferSize = sizeof(float)*sampleRate*10;
        recordState.recordBuffer = (float*)malloc(recordState.recordBufferSize);
        recordState.recordBufferPos = 0;	
        
        // recording stopped to start
        recordState.state = AUDIORECORDER_STATE_STOPPED;
    }
    return self;
}

-(void)mute {
    // set flag
    recordState.isMuted = YES;
    isMuted = YES;
}

-(void)unMute {
    // set flag
    recordState.isMuted = NO;
    isMuted = NO;
}

-(int)getSampleRate {
    return recordState.dataFormat.mSampleRate;
}

-(int)getRecordBufferPos {
    return recordState.recordBufferPos;
}

-(void*)getRecordBuffer {
    return recordState.recordBuffer;
}

-(BOOL)startRecording {
    AudioQueueStart(recordState.queue, NULL);
    recordState.state = AUDIORECORDER_STATE_LISTENING;
    recordState.waitingForFirstBuffer = YES;
    recordState.recordBufferPos = 0;
    recordState.isMuted = isMuted;
    return true;
}

-(BOOL)pauseRecording {
    AudioQueuePause(recordState.queue);
    recordState.state = AUDIORECORDER_STATE_PAUSED;
    return true;
}

-(BOOL)resumeRecording {
    AudioQueueStart(recordState.queue, NULL);
    recordState.state = AUDIORECORDER_STATE_LISTENING;
    recordState.isMuted = isMuted;
    return true;
}

-(void)dealloc {
	AudioQueueDispose(recordState.queue, TRUE);
	free(recordState.recordBuffer);
	[super dealloc];
}

@end

void AudioInputCallback5(void *inUserData, AudioQueueRef inAQ, AudioQueueBufferRef inBuffer, const AudioTimeStamp *inStartTime, UInt32 inNumberPacketDescriptions, const AudioStreamPacketDescription *inPacketDescs) {
    enum AUDIORECORDER_STATE previousState;
    
    // get user data (record state)
	RecordState *myRecordState = (RecordState*)inUserData;
    previousState = myRecordState->state;
    
	// pointer to buffer
	float* test=(float*)inBuffer->mAudioData;
	int recordedBytes = inBuffer->mAudioDataByteSize;
	int recordedFloats = inBuffer->mAudioDataByteSize/4;
    
    // if it's muted, replace with zeros
    if (myRecordState->isMuted) {
        memset(test, 0, recordedBytes);
    }
    
    // if waiting for first buffer, use first buffer to calc background noise
    if (myRecordState->waitingForFirstBuffer) {
        float avgNoise = 0.0f;
        float maxNoise = 0.0f;
		for (int i=0; i < MIN(1000,recordedFloats); i++) {
            avgNoise += test[i];
            if (test[i] > maxNoise)
                maxNoise = test[i];
        }
        avgNoise /= (float)recordedFloats;
        myRecordState->avgBackgroundNoise = maxNoise;
        myRecordState->waitingForFirstBuffer = NO;
//        printf("average noise is %f\r\n", maxNoise);
    }
	
    //	printf("buffer %p returned with bytes %d\r\n", test, inBuffer->mAudioDataByteSize);
	
	// if any of buffer is about 10% or average noise, start recording
    if (myRecordState->state == AUDIORECORDER_STATE_LISTENING) {
		for (int i=0; i < recordedFloats; i++)
			if (test[i] > MAX(0.1f, myRecordState->avgBackgroundNoise*1.5f))
//			if (test[i] > 0.1f)
			{
                myRecordState->state = AUDIORECORDER_STATE_RECORDING;
				break;
			}
	}
	
	// check for silence
	bool isSilent = true;
	if (myRecordState->state == AUDIORECORDER_STATE_RECORDING) {
		for (int i=0; i < recordedFloats; i++)
			if (test[i] > MAX(0.1f, myRecordState->avgBackgroundNoise*1.5f))
//			if (test[i] > 0.1f)
			{
				isSilent = false;
				break;
			}
	}
	
    // always copy to record buffer because we will use the previous data
    // before the sound was found in case the sound starts exactly on the line
	// if we are recording, copy buffer to our recording buffer
    // if we were listening when we came in, and are still listening, reset the buffer
	if (previousState == AUDIORECORDER_STATE_LISTENING && myRecordState->state == AUDIORECORDER_STATE_LISTENING)
		myRecordState->recordBufferPos = 0;
//    printf("audio state is %d, was %d, record position is %d, recording %d floats\r\n", myRecordState->state, previousState, myRecordState->recordBufferPos, recordedFloats);
    memcpy(myRecordState->recordBuffer+myRecordState->recordBufferPos, test, recordedBytes);

    // add bytes recorded
    myRecordState->recordBufferPos += recordedBytes;
	
	// stop recording and compare
	if (myRecordState->state == AUDIORECORDER_STATE_RECORDING && isSilent) {
		// stop recording
		AudioQueuePause(myRecordState->queue);
        
        // now in analyzing state
        myRecordState->state = AUDIORECORDER_STATE_ANALYZING;
	}
	
	// re-queue buffer
	AudioQueueEnqueueBuffer(myRecordState->queue, inBuffer, 0, NULL);
    //	printf("requeue buffer %p\r\n", inBuffer->mAudioData);
}


