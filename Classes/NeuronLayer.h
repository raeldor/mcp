//
//  NeuronLayer.h
//  Mcp
//
//  Created by Ray on 2/6/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Neuron.h"

@interface NeuronLayer : NSObject <NSCoding> {
	int neuronCount;
	int inputsPerNeuron;
    double *inputs;
	NSMutableArray *neurons;
}

-(id)initWithNeuronCount:(int)inNeuronCount inputsPerNeuron:(int)inInputsPerNeuron;
-(void)dealloc;

@property (nonatomic, retain) NSMutableArray *neurons;
@property (nonatomic, assign) int neuronCount;
@property (nonatomic, assign) int inputsPerNeuron;
@property (nonatomic, assign) double *inputs;

@end
