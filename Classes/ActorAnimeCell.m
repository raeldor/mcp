//
//  ActorAnimeCell.m
//  Mcp
//
//  Created by Ray Price on 10/8/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "ActorAnimeCell.h"
#import "McpAppDelegate.h"

@implementation ActorAnimeCell

@synthesize animeSlider;
@synthesize nameLabel;
@synthesize animeImage;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)dealloc
{
    [super dealloc];
}

@end
