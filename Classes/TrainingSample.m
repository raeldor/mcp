//
//  TrainingSample.m
//  Mcp
//
//  Created by Ray on 3/4/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "TrainingSample.h"

@implementation TrainingSample

@synthesize sampleBuffer;
@synthesize sampleSize;

-(id)initWithBuffer:(float*)inBuffer ofSize:(int)inSize {
	if (self = [super init]) {
		sampleBuffer = inBuffer;
		sampleSize = inSize;
	}
	return self;
}

- (void)dealloc {
	[super dealloc];
}

@end
