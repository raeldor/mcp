//
//  ProCheckerViewController.h
//  Mcp
//
//  Created by Ray on 10/15/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#include "AudioToolbox/AudioQueue.h"
#include "AudioToolbox/AudioToolbox.h"
#include "AVFoundation/AVFoundation.h"
#include "AudioStates.h"
#include "CepstralHistory.h"
#import "AudioRecorder.h"
#import "AudioPlayer.h"

@interface ProCheckerViewController : UIViewController <UITextFieldDelegate> {
	IBOutlet UIButton *playButton;
	IBOutlet UITextView *sentenceTextView;
	IBOutlet UILabel *statusLabel;
	IBOutlet UILabel *percentLabel;
	
	NSTimer *labelTimer;
    
    AudioRecorder *myRecorder;
    AudioPlayer *myPlayer;
    
    int myCounter;

	CepstralHistory *userCepstralHistory;
	CepstralHistory *cpuCepstralHistory;
}

-(IBAction)playButtonClicked:(id)sender;
-(IBAction)checkProButtonClicked:(id)sender;
-(void)labelTimerFired:(NSTimer*)inTimer;

@property (nonatomic, retain) UIButton *playButton;
@property (nonatomic, retain) UITextView *sentenceTextView;
@property (nonatomic, retain) UILabel *statusLabel;
@property (nonatomic, retain) UILabel *percentLabel;

@property (nonatomic, retain) AudioRecorder *myRecorder;

@end

