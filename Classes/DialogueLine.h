//
//  DialogueLine.h
//  Mcp
//
//  Created by Ray on 10/23/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface DialogueLine : NSObject <NSCoding, NSCopying> {
	int characterIndex;
	NSString *line;
}

@property (nonatomic, assign) int characterIndex;
@property (nonatomic, retain) NSString *line;

-(NSString *)getLineAsText;

@end
