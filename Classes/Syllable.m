//
//  Syllable.m
//  Mcp
//
//  Created by Ray on 3/3/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "Syllable.h"
#import "SyllablePhoneme.h"

@implementation Syllable

@synthesize syllableName;
@synthesize syllableKana;
@synthesize phonemes;

-(id)initWithDictionary:(NSDictionary*)inDictionary {
	if (self = [super init]) {
		// initialize arrays
		self.syllableName = [inDictionary objectForKey:@"SyllableName"];
		self.syllableKana = [inDictionary objectForKey:@"SyllableKana"];
		NSArray *importPhonemes = [inDictionary objectForKey:@"Phonemes"];
		self.phonemes = [NSMutableArray array];
		for (int i=0; i < importPhonemes.count; i++) {
			// create new phoneme
			SyllablePhoneme *newPhoneme = [[SyllablePhoneme alloc] initWithDictionary:[importPhonemes objectAtIndex:i]];
			[phonemes addObject:newPhoneme];
			[newPhoneme release];
		}
		
	}
	return self;
}

- (void)dealloc {
	[syllableName release];
	[syllableKana release];
	[phonemes release];
	[super dealloc];
}

@end
