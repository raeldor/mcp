//
//  TrainingSentence.h
//  Mcp
//
//  Created by Ray on 3/3/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface TrainingSentence : NSObject {
	NSString *sentence;
	NSMutableArray *syllableNames;
}

-(id)initWithDictionary:(NSDictionary*)inDictionary;
-(void)dealloc;

@property (nonatomic, retain) NSString *sentence;
@property (nonatomic, retain) NSMutableArray *syllableNames;

@end
