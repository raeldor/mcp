//
//  Conversation.m
//  Mcp
//
//  Created by Ray on 10/23/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "Conversation.h"
#import "DialogueLine.h"
#import "Character.h"
#import "McpAppDelegate.h"

@implementation Conversation

@synthesize uniqueKey;
@synthesize backgroundName;
@synthesize characters;
@synthesize dialogueLines;
@synthesize isRomaji;


-(id)init {
    self = [super init];
	if (self) {
        self.uniqueKey = [McpAppDelegate getUuid];
		characters = [[NSMutableArray alloc] init];
		dialogueLines = [[NSMutableArray alloc] init];
	}
	return self;
}

-(id)initWithCoder:(NSCoder*)coder {
	if ((self = [super init])) {
		self.uniqueKey = [coder decodeObjectForKey:@"UniqueKey"];
        // remove later
        if (uniqueKey == nil)
            self.uniqueKey = [McpAppDelegate getUuid];
		self.backgroundName = [coder decodeObjectForKey:@"backgroundName"];
		self.characters = [coder decodeObjectForKey:@"Characters"];
		self.dialogueLines = [coder decodeObjectForKey:@"DialogueLines"];
	}
	return self;
}

-(void)encodeWithCoder:(NSCoder*)coder {
    [coder encodeObject:uniqueKey forKey:@"UniqueKey"];
	[coder encodeObject:backgroundName forKey:@"backgroundName"];
	[coder encodeObject:characters forKey:@"Characters"];
	[coder encodeObject:dialogueLines forKey:@"DialogueLines"];
}

-(id)copyWithZone:(NSZone *)zone {
    Conversation *newConversation = [[Conversation alloc] init];
    // new unique key!
    newConversation.uniqueKey = [McpAppDelegate getUuid];
    newConversation.backgroundName = [[backgroundName copy] autorelease];
    newConversation.characters = [[[NSMutableArray alloc] initWithArray:characters copyItems:YES] autorelease];
    newConversation.dialogueLines = [[[NSMutableArray alloc] initWithArray:dialogueLines copyItems:YES] autorelease];
    newConversation.isRomaji = isRomaji;
    return newConversation;
}

-(int)getBestCharacterIndexFromSelectedPractice:(NSArray*)inSelectedPractice {
    // keep count of grammar for actor that matches selection
    int characterMatchCount[4]; // allow for up to 4 actors
    for (int i=0; i < 4; i++)
        characterMatchCount[i] = 0;
    
	// loop through dialogue lines
	for (int i=0; i < dialogueLines.count; i++) {
        // get this dialogue line
        DialogueLine *thisLine = [dialogueLines objectAtIndex:i];
        
        // get grammar list for this line
        NSArray *lineGrammar = [self getGrammarListAsArrayForLine:thisLine];
        for (int g=0; g < lineGrammar.count; g++) {
            // if this grammar is in our selection list
            NSString *thisGrammar = [lineGrammar objectAtIndex:g];
            for (int s=0; s < inSelectedPractice.count; s++) {
                // get grammar name from key
                NSString *selectedKey = [inSelectedPractice objectAtIndex:s];
                NSString *selectedGrammarName = (NSString*)[[selectedKey componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString: @"\n"]] objectAtIndex:2];
                if ([thisGrammar isEqualToString:selectedGrammarName]) {
                    // add to count for this character
                    characterMatchCount[thisLine.characterIndex]++;
                    break;
                }
            }
        }
    }
    
    // return the best match
    for (int c=0; c < characters.count; c++) {
        // see if it's better than all the rest
        bool theBest = YES;
        for (int i=0; i < characters.count; i++) {
            if (i != c) {
                if (characterMatchCount[i] > characterMatchCount[c])
                    theBest = NO;
                break;
            }
        }
        
        // if this is the best, return it
        if (theBest)
            return c;
    }
    
    // return zero just in case
    return 0;
}

-(NSArray*)getGrammarListAsArray {
	// return as array
    NSMutableDictionary *grammarList = [NSMutableDictionary dictionaryWithCapacity:5];
	
	// loop through dialogue lines
	for (int i=0; i < dialogueLines.count; i++) {
		DialogueLine *thisLine = [dialogueLines objectAtIndex:i];
		NSScanner *myScanner = [NSScanner scannerWithString:thisLine.line];
		while (![myScanner isAtEnd]) {
			// find href
			NSString *urlName = nil;
			[myScanner scanUpToString:@"<a href=\"file://" intoString:NULL];
			[myScanner scanUpToString:@"\">" intoString:&urlName];
			
			// if we have a url
			if (urlName != nil) {
				// remove tag
				urlName = [urlName stringByReplacingOccurrencesOfString:@"<a href=\"file://" withString:@""];
				
				// translate from character breaks to real string
				urlName = [urlName stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
				
				// add to grammar list if doesn't already exist
                [grammarList setObject:urlName forKey:urlName];
			}
		}
	}
    
    // return '<none selected>' if no tags found
    return [grammarList allKeys];
}

-(NSArray*)getGrammarListAsArrayForLine:(DialogueLine*)inLine {
	// return as array
    NSMutableDictionary *grammarList = [NSMutableDictionary dictionaryWithCapacity:5];
    
    NSScanner *myScanner = [NSScanner scannerWithString:inLine.line];
    while (![myScanner isAtEnd]) {
        // find href
        NSString *urlName = nil;
        [myScanner scanUpToString:@"<a href=\"file://" intoString:NULL];
        [myScanner scanUpToString:@"\">" intoString:&urlName];
        
        // if we have a url
        if (urlName != nil) {
            // remove tag
            urlName = [urlName stringByReplacingOccurrencesOfString:@"<a href=\"file://" withString:@""];
            
            // translate from character breaks to real string
            urlName = [urlName stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            
            // add to grammar list if doesn't already exist
            [grammarList setObject:urlName forKey:urlName];
        }
    }

    return [grammarList allKeys];
}

-(NSString*)getGrammarListAsString {
	// return as string
	NSMutableString *grammarList = [NSMutableString stringWithString:@""];
	
	// loop through dialogue lines
	for (int i=0; i < dialogueLines.count; i++) {
		DialogueLine *thisLine = [dialogueLines objectAtIndex:i];
		NSScanner *myScanner = [NSScanner scannerWithString:thisLine.line];
		while (![myScanner isAtEnd]) {
			// find href
			NSString *urlName = nil;
			[myScanner scanUpToString:@"<a href=\"file://" intoString:NULL];
			[myScanner scanUpToString:@"\">" intoString:&urlName];
			
			// if we have a url
			if (urlName != nil) {
				// remove tag
				urlName = [urlName stringByReplacingOccurrencesOfString:@"<a href=\"file://" withString:@""];
				
				// translate from character breaks to real string
				urlName = [urlName stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
				
				// add to grammar list if doesn't already exist
				if ([grammarList rangeOfString:urlName].location == NSNotFound) {
					if ([grammarList isEqualToString:@""])
						[grammarList appendString:urlName];
					else
						[grammarList appendFormat:@", %@", urlName];
				}
			}
		}
	}
    
    // return '<none selected>' if no tags found
    if ([grammarList isEqualToString:@""])
        return @"<No Grammar Selected>";
    else
        return grammarList;
}

-(NSString*)getDialogHtmlPreview {
	// intial html
	NSMutableString *htmlString = [NSMutableString stringWithString:@"<html>"];
	[htmlString appendString:@"<style>span.first {color:black;font-family='helvetica';font-size:12px;font-weight:bold;}</style>"];
	[htmlString appendString:@"<style>span.second {color:black;font-family='helvetica';font-size:12px;}</style>"];
	[htmlString appendString:@"<style>a {color:inherit;font-family='helvetica';font-size:12px;}</style>"];
	[htmlString appendString:@"<body>"];
	
	// loop through dialogue lines
	for (int i=0; i < dialogueLines.count; i++) {
		DialogueLine *thisLine = [dialogueLines objectAtIndex:i];
		Character *thisCharacter = [characters objectAtIndex:[thisLine characterIndex]];
		[htmlString appendFormat:@"<span class='first'>%@</span><span class='second'>: %@</span><br>", thisCharacter.characterName, thisLine.line];
	}
	[htmlString appendString:@"</body></html>"];
	return htmlString;
}

-(void)dealloc {
    [uniqueKey release];
	[backgroundName release];
	[characters release];
	[dialogueLines release];
	[super dealloc];
}

@end
