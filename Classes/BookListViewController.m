//
//  BookListViewController.m
//  Mcp
//
//  Created by Ray on 10/25/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "BookListViewController.h"
#import "EditBookController.h"
#import "Book.h"
#import "BookListCell.h"
#import "ChapterListViewController.h"

@implementation BookListViewController

@synthesize bookList;

#pragma mark -
#pragma mark View lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
	
	// set title
	self.title = @"Books";
    
	// allow selecting during editor
	self.tableView.allowsSelection = TRUE;
	self.tableView.allowsSelectionDuringEditing = TRUE;
	
	// add edit button
	self.navigationItem.rightBarButtonItem = self.editButtonItem;
	
	// search for deck archive files
	[self populateBookList];
}

// 0 = conversation, 1 = grammar
-(void)setFinalEditTypeAs:(int)inEditType {
    finalEditType = inEditType;
}

-(void)populateBookList {
	// create new list of books
	self.bookList = [Book getListOfFilesWithExtension:@"book"];
}

#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	if (self.editing)
		return bookList.count+1;
	else
	{
		if (bookList.count == 0)
			return 1;
		else
			return bookList.count;
	}
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	return 100;
}

// Customize the appearance of table view cells.
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	// get regular table cell
    static NSString *CellIdentifier = @"BookListCell";
    BookListCell *cell = (BookListCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
		// load cell layout from nib
		NSArray *nib = [[NSBundle mainBundle] loadNibNamed:CellIdentifier owner:self options:nil];
		cell = [nib objectAtIndex:0];
    }
	
	// default cell config
	cell.textLabel.text = NULL;
	cell.imageView.image = NULL;
	cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
	cell.editingAccessoryType = UITableViewCellAccessoryNone;
	cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
	// if no cells, display 'press to add' message
	if (bookList.count == 0 && !self.editing) {
		cell.textLabel.text = @"Press Edit Button To Create New Book";
		cell.textLabel.textColor = [UIColor lightGrayColor];
		cell.textLabel.adjustsFontSizeToFitWidth = YES;
	}
	else {
		// Set up the cell...
		NSUInteger row = [indexPath row];
		if (row < bookList.count) {
			cell.nameLabel.text = [bookList objectAtIndex:row];
			Book *book = [[Book alloc] initFromArchive:[bookList objectAtIndex:row]];
			cell.bookImageView.image = book.image;
			cell.editingAccessoryType = UITableViewCellAccessoryDisclosureIndicator;
			cell.selectionStyle = UITableViewCellSelectionStyleBlue;
            [book release];
		}
		else {
			cell.textLabel.text = @"Add New";
			cell.textLabel.textColor = [UIColor lightGrayColor];
			cell.selectionStyle = UITableViewCellSelectionStyleBlue;
		}
	}
	
    return cell;
}

//editing mode changed
- (void)setEditing:(BOOL)editing animated:(BOOL)animated {
	// add/remove placeholder items
	NSMutableArray *indexPaths = [NSMutableArray array];
	[indexPaths addObject:[NSIndexPath indexPathForRow:bookList.count inSection:0]];
	[self.tableView beginUpdates];
	[super setEditing:editing animated:animated];
	[self.tableView setEditing:editing animated:YES];
	if (editing) {
		// delete 'press edit' message
		if (bookList.count == 0)
			[self.tableView deleteRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationTop];
		
		// Show the placeholder rows and hide back button
		[self.tableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationTop];
		self.navigationItem.hidesBackButton = YES;
	}
	else
	{
		// Hide the placeholder rows and show back button again
		[self.tableView deleteRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationTop];
		self.navigationItem.hidesBackButton = NO;
		
		// show 'press edit' message
		if (bookList.count == 0)
			[self.tableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationTop];
	}
	[self.tableView endUpdates];
}

-(UITableViewCellEditingStyle)tableView:(UITableView*)tableView editingStyleForRowAtIndexPath:(NSIndexPath*)indexPath {
	// which row are we getting
	NSUInteger row = [indexPath row];
	
	// if we are in edit mode
	if (self.editing) {
		// show delete style and insert for 'add new'
		if (row != bookList.count)
			return UITableViewCellEditingStyleDelete;
		else
			return UITableViewCellEditingStyleInsert;
	}
	return UITableViewCellEditingStyleNone;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    // if we are deleting rows
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
		[Book	delete:[bookList objectAtIndex:[indexPath row]] extension:@".book"];
		[bookList removeObjectAtIndex:[indexPath row]];
        [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:YES];
    }   
    else
		if (editingStyle == UITableViewCellEditingStyleInsert) {
			// edit book using edit controller
			oldBookName = nil;
			EditBookController *editBookController = [[EditBookController alloc] initWithNibName:@"EditBookController" bundle:nil];
			Book *newBook = [[Book alloc] init];
			[editBookController setEditableObject:newBook isNew:YES];
			editBookController.editableListDelegate = self;
			UINavigationController *secondNavigationController = [[UINavigationController alloc] initWithRootViewController:editBookController];
			[self.navigationController presentModalViewController:secondNavigationController animated:YES];
			[newBook release];
			[editBookController release];
			[secondNavigationController release];
		}
}

-(void)didUpdateObject:(id)updatedObject {
	// if renamed, then must rename file too
	Book *updatedBook = (Book*)updatedObject;
	if (![oldBookName isEqualToString:[updatedBook bookName]])
		[updatedBook renameFrom:oldBookName to:[updatedBook bookName]];
	
	// update class to disk and refresh table
	[updatedBook saveAs:[updatedBook bookName]];
	[self populateBookList];
	[self.tableView reloadData];
}

-(void)didAddNewObject:(id)newObject {
	// save new class
	Book *newBook = (Book*)newObject;
	[newObject saveAs:[newBook bookName]];
	[self populateBookList];
	[self.tableView reloadData];
}

#pragma mark -
#pragma mark Table view delegate

-(void)tableView:(UITableView*)tableView didSelectRowAtIndexPath:(NSIndexPath*) indexPath {
	// find row
	NSUInteger row = [indexPath row];
	
	// save index path
    [lastIndexPath release];
	lastIndexPath = [indexPath retain];
	
	// editing mode?
	if (self.editing) {
		// add new?
		if (row >= [bookList count]) {
			[self tableView:tableView commitEditingStyle:UITableViewCellEditingStyleInsert forRowAtIndexPath:indexPath];
		}
		else {
			// edit book using edit controller
			oldBookName = [bookList objectAtIndex:row];
			EditBookController *editBookController = [[EditBookController alloc] initWithNibName:@"EditBookController" bundle:nil];
			Book *thisBook = [[Book alloc] initFromArchive:[bookList objectAtIndex:row]];
			[editBookController setEditableObject:thisBook isNew:NO];
			editBookController.editableListDelegate = self;
			[self.navigationController pushViewController:editBookController animated:YES];
			[thisBook release];
			[editBookController release];
		}
	}
	else {
		if (bookList.count > 0) {
			// selected book in non edit mode, so show chapters
			ChapterListViewController *chapterListController = [[ChapterListViewController alloc] initWithNibName:@"ChapterListViewController" bundle:nil];
            [chapterListController setFinalEditTypeAs:finalEditType];
			Book *thisBook = [[Book alloc] initFromArchive:[bookList objectAtIndex:row]];
			[chapterListController setEditableObject:thisBook];
			[self.navigationController pushViewController:chapterListController animated:YES];
			[thisBook release];
			[chapterListController release];
		}
	}
}

#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    // Relinquish ownership of anything that can be recreated in viewDidLoad or on demand.
    // For example: self.myOutlet = nil;
}


- (void)dealloc {
    [lastIndexPath release];
	[bookList release];
    [super dealloc];
}


@end

