//
//  NameCell.m
//  Mcp
//
//  Created by Ray on 10/19/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "NameCell.h"


@implementation NameCell

@synthesize nameTextField;
@synthesize nameLabel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if ((self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {

    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (BOOL)textFieldShouldReturn:(UITextField *)theTextField {
	[theTextField resignFirstResponder];
	return YES;
}

- (void)dealloc {
	[nameLabel release];
	[nameTextField release];
    [super dealloc];
}


@end
