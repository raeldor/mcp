//
//  EditDeckViewController.h
//  HF Sensei
//
//  Created by Ray Price on 2/23/13.
//
//

#import <UIKit/UIKit.h>
#import "EditDeckViewControllerDelegate.h"
#import "FlashDeck.h"

@interface EditDeckViewController : UITableViewController <UIActionSheetDelegate ,UINavigationControllerDelegate, UIImagePickerControllerDelegate> {
    FlashDeck *editDeck;
	UIPopoverController *popOver;
    BOOL isNewDeck;
}

@property (nonatomic, assign) id<EditDeckViewControllerDelegate> delegate;
@property (nonatomic, assign) IBOutlet UITextField *deckNameTextField;
@property (nonatomic, assign) IBOutlet UIImageView *deckImageView;

-(void)setDeckTo:(FlashDeck*)inDeck;
-(IBAction)cancelPushed:(id)sender;
-(IBAction)savePushed:(id)sender;
-(IBAction)didTouchImage:(id)sender;

@end
