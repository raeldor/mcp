    //
//  GrammarViewController.m
//  Mcp
//
//  Created by Ray on 1/13/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "GrammarViewController.h"
#import "McpAppDelegate.h"
#import "TextFunctions.h"
#import "HtmlFunctions.h"

@implementation GrammarViewController

@synthesize grammarNote;
@synthesize noteWebView;
@synthesize selectDelegate;

-(void)setViewNote:(GrammarNote*)inNote allowSelect:(BOOL)inAllowSelect {
	// save grammar note to display in web view
	self.grammarNote = inNote;
    allowSelect = inAllowSelect;
}

// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
    }
    return self;
}
*/

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
}
*/

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];

	// get user profile
    McpAppDelegate *appDelegate = (McpAppDelegate*)[[UIApplication sharedApplication] delegate];
    UserProfile *userProfile = appDelegate.currentUserProfile;
    
    // if this is a selection style
    if (allowSelect) {
        // add select button
        UIBarButtonItem *selectButton = [[UIBarButtonItem alloc] initWithTitle:@"Select" style:UIBarButtonItemStyleBordered target:self action:@selector(selectButtonClicked:)];
        self.navigationItem.rightBarButtonItem = selectButton;
        [selectButton release];
	}
    
    // ipad?
	BOOL isIpad = NO;
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 30200
	if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
		isIpad = YES;
#endif
    
    // smaller font for iphone
    int fontSize = 17;
    int headerFontSize = 24;
    if (!isIpad) {
        fontSize = 10;
        headerFontSize = 14;
    }
    
	// update web view with html
    NSMutableString *htmlString = [NSMutableString stringWithFormat:@"<html><style>body {color:black;font-family='helvetica';font-size:%dpx;font-weight:normal;}", fontSize];
	[htmlString appendFormat:@"</style><style>h1 {color:black;font-family='helvetica';font-size:%dpx;font-weight:bold;}</style>", headerFontSize];
	[htmlString appendFormat:@"</style><style>blockquote {color:black;font-family='helvetica';font-size:%dpx;}</style><body>", fontSize];
	[htmlString appendFormat:@"</style><style>table {color:black;font-family='helvetica';font-size:%dpx;}</style><body>", fontSize];
	[htmlString appendFormat:@"<h1>%@</h1>%@</body></html>", grammarNote.title, grammarNote.text];
    
    // convert kanji to users requested format
    NSString *htmlString2 = [HtmlFunctions convertHtml:htmlString toHtmlAs:userProfile.gameOptions.showTextAs];
    
	[noteWebView loadHTMLString:htmlString2 baseURL:nil];
}

-(void)selectButtonClicked:(id)sender {
	// call delegate and pop controller
	[selectDelegate didSelectGrammar];
	[self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
	[grammarNote release];
	[noteWebView release];
    [super dealloc];
}


@end
