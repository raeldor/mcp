//
//  ConverseViewController.m
//  Mcp
//
//  Created by Ray on 11/5/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "ConverseViewController.h"
#import "Book.h"
#import "Background.h"
#import "Character.h"
#import "Actor.h"
#import "DialogueLine.h"
#import "ConversationPickerViewController.h"
#import "GameOptionsViewController.h"
#import "GrammarNote.h"
#import "McpAppDelegate.h"
#import "TextFunctions.h"
#import "BookSelectViewController.h"
#import "LipSyncFrame.h"
#import "AudioFile.h"
#import "CalibratePopupController.h"
#import "NewTrainingSample.h"
#import "FlashConvGame.h"
#import "InteractiveConvGame.h"
#import "HtmlFunctions.h"
#import "DeckPickerView.h"
#import "SectionPickerViewController.h"
#import "ThoughtMenuViewController.h"
#import "CorrectionViewController.h"
#import "ProgressViewController.h"
#import "HelpViewController.h"

const unsigned char SpeechKitApplicationKey[] = {0xdd, 0xb7, 0x1d, 0x86, 0x6a, 0x22, 0x81, 0xb1, 0xbc, 0x89, 0x49, 0xb1, 0xe7, 0x6e, 0x1d, 0x94, 0x0c, 0x30, 0x28, 0x78, 0x42, 0xa9, 0x8a, 0xc0, 0x1b, 0xf4, 0x4e, 0x50, 0xf2, 0x89, 0x3c, 0xcf, 0x40, 0x59, 0x55, 0xaa, 0x7b, 0xd7, 0x5b, 0x37, 0x60, 0xa5, 0xe9, 0x47, 0x13, 0x06, 0xdc, 0x1f, 0x36, 0x04, 0x98, 0x78, 0xad, 0x22, 0x38, 0xc8, 0x16, 0x87, 0x18, 0x08, 0x10, 0x54, 0xd7, 0x50};

@implementation ConverseViewController

@synthesize blinkState;
@synthesize backgroundImageView;
@synthesize menuButton;
@synthesize actor1;
@synthesize actor2;
@synthesize actor3;
@synthesize actor4;
@synthesize dialogueWebView;
@synthesize statusLabel;
@synthesize playGame;
@synthesize labelTimer;
@synthesize pauseTimer;
@synthesize passFailWaitTimer;
@synthesize runningScoreLabel;
@synthesize lastScoreLabel;
@synthesize faderView;
@synthesize showScoreController;
@synthesize gameMenuController;
@synthesize tipController;
@synthesize speechPlayer;
@synthesize userProfile;
@synthesize cpuProfile;
@synthesize grammarButton;
@synthesize pauseButton;
@synthesize sourceButton;
@synthesize bigLabel;
@synthesize currentAudioRoute;
@synthesize calibrationMode;

#define degreesToRadian(x) (M_PI * (x) / 180.0)

 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
    }
    return self;
}

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
    
    /*
    // setup speech kit
    [SpeechKit setupWithID:@"NMDPTRIAL_raeldor20110925085010"
                      host:@"sandbox.nmdp.nuancemobility.net"
                      port:443
                    useSSL:NO
                  delegate:nil];
    
    // Set earcons to play
    SKEarcon* earconStart   = [SKEarcon earconWithName:@"beep-24.wav"];
//    SKEarcon* earconStart   = [SKEarcon earconWithName:@"earcon_listening.wav"];
//    SKEarcon* earconStop    = [SKEarcon earconWithName:@"earcon_done_listening.wav"];
//    SKEarcon* earconCancel  = [SKEarcon earconWithName:@"earcon_cancel.wav"];
    [SpeechKit setEarcon:earconStart forType:SKStartRecordingEarconType];
    
//    [SpeechKit setEarcon:earconStop forType:SKStopRecordingEarconType];
//    [SpeechKit setEarcon:earconCancel forType:SKCancelRecordingEarconType];
    */
    
    // start activity indicator
    [activityIndicator startAnimating];
    
    /*
    // initialize ispeech
	isRecognition = [[ISSpeechRecognition alloc] init];
    isRecognition.silenceDetectionEnabled = YES;
	[isRecognition setDelegate:self];
    */
    
    // change actor sizes
    float actorRatio = actor1.frame.size.height/actor1.frame.size.width;
    actor1.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.width*actorRatio);
    
    // don't allow phone to turn off
    [[UIApplication sharedApplication] setIdleTimerDisabled:YES];
    
	// get application delegate
    McpAppDelegate *appDelegate = (McpAppDelegate*)[[UIApplication sharedApplication] delegate];
    
    // save reference to app options
    appOptions = appDelegate.appOptions;
    
    // get audio source and change icon accordingly
    [self updateAudioRouteIcon];
    
    // listen for changes to audio route
    AudioSessionAddPropertyListener(kAudioSessionProperty_AudioRouteChange, routeChangeListener, self);
    
    // save pointer to game options for speed
    self.userProfile = appDelegate.currentUserProfile;
    self.cpuProfile = appDelegate.currentCpuProfile;
    
    // get play and pause images
    playImage = [[UIImage imageNamed:@"button-play-mini.png"] retain];
    pauseImage = [[UIImage imageNamed:@"button-pause-mini.png"] retain];
    
	// get fail sound id
	NSString *path = [NSString stringWithFormat:@"%@/%@", [[NSBundle mainBundle] resourcePath], @"fail.wav"];
	NSURL *filePath = [NSURL fileURLWithPath:path isDirectory:NO];
	AudioServicesCreateSystemSoundID((CFURLRef)filePath, &failSoundId);
    
    // system sounds don't sound through bluetooth
	failPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:filePath error:nil];
	[failPlayer prepareToPlay];
    
	// get beep sound id
	path = [NSString stringWithFormat:@"%@/%@", [[NSBundle mainBundle] resourcePath], @"beep-24.wav"];
	filePath = [NSURL fileURLWithPath:path isDirectory:NO];
	AudioServicesCreateSystemSoundID((CFURLRef)filePath, &beepSoundId);
    
    // system sounds don't sound through bluetooth
	beepPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:filePath error:nil];
    beepPlayer.delegate = self;
	[beepPlayer prepareToPlay];
    
	// get matched word sound id
	path = [NSString stringWithFormat:@"%@/%@", [[NSBundle mainBundle] resourcePath], @"retro_affirm.wav"];
	filePath = [NSURL fileURLWithPath:path isDirectory:NO];
	AudioServicesCreateSystemSoundID((CFURLRef)filePath, &matchedSoundId);
    
    // system sounds don't sound through bluetooth
	matchedPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:filePath error:nil];
    matchedPlayer.delegate = self;
	[matchedPlayer prepareToPlay];
    
	// hide nav bar
	[self.navigationController setNavigationBarHidden:YES animated:NO];

	// save position and size of last score label to use as starting point for animation
	lastScoreFrame = lastScoreLabel.frame;
	
    // create audio recorder
    myRecorder = [[AudioRecorder alloc] initWithSampleRate:[userProfile getSampleRateForAudioDevice] startMuted:appDelegate.localOptions.isMicMuted];
    
    // hide all actors
    lastCharacterIndex = -1;
	actor1.alpha = 0;
	actor2.alpha = 0;
	actor3.alpha = 0;
	actor4.alpha = 0;
	
    // create array for lip sync
    lipSyncArray = [[NSMutableArray arrayWithCapacity:10] retain];
    
	// set uiwebview to clear background
	dialogueWebView.opaque = NO;
	dialogueWebView.backgroundColor = [UIColor clearColor];
	saidWebView.opaque = NO;
	saidWebView.backgroundColor = [UIColor clearColor];
	toSayWebView.opaque = NO;
	toSayWebView.backgroundColor = [UIColor clearColor];
	
	// start timer for label update
	labelTimer = [[NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(labelTimerFired:) userInfo:nil repeats:YES] retain];
    
    // create grammar notes array
    grammarNotes = [[NSMutableDictionary alloc] init];
    
    // create new game
    [self restartGameWithBookNamed:userProfile.gameData.selectedBookName completion:^(BOOL success) {
        // if not success, show alert
        if (!success) {
            UIAlertView *myAlert = [[UIAlertView alloc] initWithTitle:@"Can't Start Game" message:@"There was a problem starting this game.  Check to make sure you have a valid deck and sections selected." delegate:self cancelButtonTitle:@"Understood" otherButtonTitles:nil];
            [myAlert show];
            [myAlert release];
        }
        else {
            [activityIndicator stopAnimating];
        }
    }];
    
    // create a long press recognizer for the thought menu
    // create long press recognizer for thought menu
    longRecognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(viewLongPressed:)];
    longRecognizer.delegate = self;
    [self.view addGestureRecognizer:longRecognizer];
    
    // create tap recognizer to show controls
    tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewTapped:)];
    tapRecognizer.delegate = self;
    [self.view addGestureRecognizer:tapRecognizer];
    
    // update mute setting based on saved setting
    if (appDelegate.localOptions.isMicMuted) {
        // mute
        [myRecorder mute];
        
        // update icon
        [muteButton setImage:[UIImage imageNamed:@"mic_mute_24_32.png"] forState:UIControlStateNormal];
    }
    else {
        // unmute
        [myRecorder unMute];
        
        // update icon
        [muteButton setImage:[UIImage imageNamed:@"mic_24_32.png"] forState:UIControlStateNormal];
    }
    
    // create dragon vocalizer
    /*
    [SpeechKit setupWithID:@"NMDPTRIAL_raeldor20110925085010"
                      host:@"sandbox.nmdp.nuancemobility.net"
                      port:443
                    useSSL:NO
                  delegate:nil];
    
    dragonVocalizer = [[SKVocalizer alloc] initWithLanguage:@"ja_JP" delegate:self];
    */
    
    // check calibration
    // we don't need to do this because route change listener will do this
//    [self checkCalibration];
}

-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    if ([touch.view isKindOfClass:[UIButton class]] || [touch.view.superview isKindOfClass:[UIToolbar class]]) {
        return NO;
    }
    return YES;
}

-(void)viewTapped:(id)sender {
    // show or hide menu
    if (menuToolbar.hidden) {
        [self showMenuAndControls];
    }
    else {
        [self hideMenuAndControls];
        
    }
}

-(void)resetHideControlsTimer {
    // set timer to auto hide
    if (hideControlsTimer != nil) {
        [hideControlsTimer invalidate];
        [hideControlsTimer release];
        hideControlsTimer = [[NSTimer scheduledTimerWithTimeInterval:2.0f target:self selector:@selector(hideMenuAndControls) userInfo:nil repeats:NO] retain];
    }
}

-(void)showMenuAndControls {
    // hide correct button if no sentence
    if (playGame == nil || [playGame getCurrentSampleSentence] == nil)
        correctButton.enabled = NO;
    else
        correctButton.enabled = YES;
    
    // hide known button if no flash card
    if (playGame == nil || [playGame getCurrentFlashCard] == nil)
        knownButton.enabled = NO;
    else
        knownButton.enabled = YES;
    
    // use animation to smoothly move it onto screen
    // first move off screen
    menuToolbar.frame = CGRectMake(0.0f, -menuToolbar.frame.size.height, menuToolbar.frame.size.width, menuToolbar.frame.size.height);
    mediaToolbar.frame = CGRectMake(0.0f, self.view.frame.size.height, mediaToolbar.frame.size.width, mediaToolbar.frame.size.height);
    
    // now animate onto screen
    menuToolbar.hidden = NO;
    mediaToolbar.hidden = NO;
    [UIView animateWithDuration:0.5f animations:^{
        menuToolbar.frame = CGRectMake(0.0f, 0.0f, menuToolbar.frame.size.width, menuToolbar.frame.size.height);
        mediaToolbar.frame = CGRectMake(0.0f, self.view.frame.size.height-mediaToolbar.frame.size.height, mediaToolbar.frame.size.width, mediaToolbar.frame.size.height);
    }];
    
    // set timer to auto hide
    hideControlsTimer = [[NSTimer scheduledTimerWithTimeInterval:2.0f target:self selector:@selector(hideMenuAndControls) userInfo:nil repeats:NO] retain];
}

-(void)hideMenuAndControls {
    // use animation to smoothly move it off screen
    // now animate onto screen
    [UIView animateWithDuration:0.5f animations:^{
        menuToolbar.frame = CGRectMake(0.0f, -menuToolbar.frame.size.height, menuToolbar.frame.size.width, menuToolbar.frame.size.height);
        mediaToolbar.frame = CGRectMake(0.0f, self.view.frame.size.height, mediaToolbar.frame.size.width, mediaToolbar.frame.size.height);
    } completion:^(BOOL finished) {
        menuToolbar.hidden = YES;
        mediaToolbar.hidden = YES;
    }];
    
    // invalidate any timer
    [hideControlsTimer invalidate];
    [hideControlsTimer release];
    hideControlsTimer = nil;
}
/*
-(void)vocalizer:(SKVocalizer *)vocalizer didFinishSpeakingString:(NSString *)text withError:(NSError *)error {
    // pause for a half second, then continue
    self.pauseTimer = [NSTimer scheduledTimerWithTimeInterval:0.2 target:self selector:@selector(pauseAfterSpeakingTimerFired:) userInfo:nil repeats:NO];
}

-(void)recognizer:(SKRecognizer*)recognizer didFinishWithError:(NSError *)error suggestion:(NSString *)suggestion {
    NSString *expectedText = [playGame getCurrentSegmentForSpeech:NO];
}

-(void)recognizer:(SKRecognizer*)recognizer didFinishWithResults:(SKRecognition *)results {
    // no results!?
    float grade = 0.3f;
    if (results.results.count == 0)
        grade = 1.0f;
    else {
        for (int i=0; i < results.results.count; i++) {
            // get spoken and expected text
            NSString *expectedText = [playGame getCurrentSegmentForSpeech:NO];
            NSString *spokenText = (NSString*)[results.results objectAtIndex:i];
            
            
            // turn into hiragana
            expectedText = [HtmlFunctions convertHtml:expectedText toHtmlAs:2];
            expectedText = [TextFunctions katakanaToHiragana:expectedText];
            spokenText = [HtmlFunctions convertHtml:spokenText toHtmlAs:2];
            spokenText = [TextFunctions katakanaToHiragana:spokenText];
            
            // strip punctuation
            expectedText = [TextFunctions stripPunctuationFromString:expectedText];
            spokenText = [TextFunctions stripPunctuationFromString:spokenText];
            
            NSLog(@"expected:%@", expectedText);
            NSLog(@"spoken:%@", spokenText);
            
            // derive grade
            if ([spokenText isEqualToString:expectedText]) {
                grade = 0.9f;
                break;
            }
        }
    }
    
    // if we passed
    if (grade > userProfile.gameOptions.passAccuracy ||
        calibrationMode) {
        // do animation to show pass score and add to total
        [self showPassAnimation:grade];
        
        // do next action
        [self doNextAction];
    }
    else {
        // do animation to show and fade fail score
        [self showFailAnimation:grade];
    }
}
*/

-(void)checkCalibration {
    /*
    // if our neural net is not yet calibrated, tell user we will be calibrating
    // first before we can converse
    if (![userProfile getNetForAudioDevice].trained) {
        // pause game
        resumeAfterUpdate = NO;
        [self pauseGame];
        
        // show alert to tell user we will be calibrating
        UIAlertView *myAlert = [[UIAlertView alloc] initWithTitle:@"Calibrate" message:@"This audio device has not been calibrated for this user.  Calibration must be done before two-way conversation can take place.  Would you like to calibrate now?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
        [myAlert show];
        [myAlert release];
    }
     */
}

/*
-(void)updateGrammarNotes {
    // get all grammar notes into one dictionary
    [grammarNotes removeAllObjects];
    
    // if we are using a book
    if ([playGame getBook] != nil) {
        for (int c=0; c < [playGame getBook].chapters.count; c++) {
            Chapter *thisChapter = [[playGame getBook].chapters objectAtIndex:c];
            for (int g=0; g < thisChapter.grammarNotes.count; g++) {
                GrammarNote *thisNote = [thisChapter.grammarNotes objectAtIndex:g];
                [grammarNotes setObject:thisNote forKey:thisNote.title];
            }
        }
    }
}
*/

-(void)updateAudioRouteIcon {
    // get audio source and change icon accordingly
    UInt32 rSize = sizeof(CFStringRef);
    CFStringRef route;
    AudioSessionGetProperty(kAudioSessionProperty_AudioRoute, &rSize, &route);
    NSString *audioRoute = (NSString*)route;
    UIImage *routeImage = [UIImage imageNamed:@"sound_24_32.png"];;
    if ([audioRoute isEqualToString:@"HeadphonesAndMicrophone"] || 
        [audioRoute isEqualToString:@"HeadsetInOut"])
        routeImage = [UIImage imageNamed:@"headphones_24_32.png"];
    if ([audioRoute isEqualToString:@"HeadsetBT"])
        routeImage = [UIImage imageNamed:@"bluetooth_24_32.png"];
    [sourceButton setImage:routeImage forState:UIControlStateNormal];
    CFRelease(route);
}

void routeChangeListener (void *inClientData, AudioSessionPropertyID inID, UInt32 inDataSize, const void *inData) {
    // get reference to converse view controller
    ConverseViewController *controller = (ConverseViewController*)inClientData;
    
    // do nothing if it is still the same as the previous route
    UInt32 rSize = sizeof(CFStringRef);
    CFStringRef route;
    AudioSessionGetProperty(kAudioSessionProperty_AudioRoute, &rSize, &route);
    NSString *routeName = (NSString*)route;
    if ([routeName isEqualToString:controller.currentAudioRoute]) {
        CFRelease(route);
        return;
    }

    // save current route
    NSString *newRoute = [routeName copy];
    controller.currentAudioRoute = newRoute;
    [newRoute release];
    
    // release string
    CFRelease(route);
    
    // update icon
    [controller updateAudioRouteIcon];
    
    // if you changed the audio route while calibrating, put up warning message
    if (controller.calibrationMode) {
        // pause game
        [controller pauseGame];
        
        // bring up alert
        UIAlertView *myAlert = [[UIAlertView alloc] initWithTitle:@"Audio Device Changed" message:@"You have changed audio device during calibration.  This will result in a bad calibration.  Calibration has been canceled.  Please change your audio device back and try again." delegate:controller cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [myAlert show];
        [myAlert release];
    }
    else {
        // check once again for calibration
        [controller checkCalibration];
    }
}

-(IBAction)muteButtonPressed:(id)sender {
	// get application delegate
    McpAppDelegate *appDelegate = (McpAppDelegate*)[[UIApplication sharedApplication] delegate];
    
    // mute recorder?
    if (myRecorder.isMuted) {
        // unmute
        [myRecorder unMute];
        
        // update icon
        [muteButton setImage:[UIImage imageNamed:@"mic_24_32.png"] forState:UIControlStateNormal];
    }
    else {
        // mute
        [myRecorder mute];
        
        // update icon
        [muteButton setImage:[UIImage imageNamed:@"mic_mute_24_32.png"] forState:UIControlStateNormal];
    }
    
    // save new setting
    appDelegate.localOptions.isMicMuted = myRecorder.isMuted;
    [appDelegate.localOptions save];
}

-(IBAction)sourceButtonPressed:(id)sender {
    // get audio source and change icon accordingly
    UInt32 rSize = sizeof(CFStringRef);
    CFStringRef route;
    AudioSessionGetProperty(kAudioSessionProperty_AudioRoute, &rSize, &route);
    NSString *audioRoute = (NSString*)route;

    // if bluetooth, change back to speaker
    if ([audioRoute isEqualToString:@"HeadsetBT"]) {
        AudioSessionPropertyID routeOverride = kAudioSessionOverrideAudioRoute_Speaker;
        AudioSessionSetProperty(kAudioSessionProperty_OverrideAudioRoute, sizeof(routeOverride), &routeOverride);
    }
    else {
        // try and output to bluetooth again by turning it off then back on
        UInt32 allowBluetoothInput = 0;
        AudioSessionSetProperty(kAudioSessionProperty_OverrideCategoryEnableBluetoothInput, sizeof(allowBluetoothInput), &allowBluetoothInput);
        allowBluetoothInput = 1;
        AudioSessionSetProperty(kAudioSessionProperty_OverrideCategoryEnableBluetoothInput, sizeof(allowBluetoothInput), &allowBluetoothInput);
    }

    // release string
    CFRelease(route);
}

-(void)viewDidAppear:(BOOL)animated {   
    // don't do this here, or it gets called after options windows etc.
    // moved into didload event
	// update actors
//	[self updateActors];
	
	// pause for a half second for actors to change
//	self.pauseTimer = [[NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(pauseAfterActorChangeTimerFired:) userInfo:nil repeats:NO] retain];
}

-(void)doCurrentAction {
    // don't do current action if game is paused, otherwise saving options
    // will start playing again while menu is still up
    
    // if our part, start listening
    if ([playGame isGetUserInput]) {
        // we can start blinking here, since lips won't be moving
        // create a random timer to do blinking while interacting with user
        if (blinkTimer == nil) {
            blinkState = 0;
            blinkTimer = [[NSTimer scheduledTimerWithTimeInterval:1+rand()%4 target:self selector:@selector(blinkTimerFired:) userInfo:self repeats:NO] retain];
        }
    }
    
    // speak!
    if ([playGame isSpeakThisSegment]) {
        [self speakText:[playGame getCurrentSegmentForSpeech:YES] isPrompt:NO usingLanguage:[playGame getCurrentSegmentLanguageName]];
    }
    else {
        // start listening?
        if ([playGame isGetUserInput]) {
            // start listening
            [self startListening];
        }
    }
}

-(void)doPreviousAction {
}

-(void)doNextAction {
    NSLog(@"Do next action");
    
    // don't go to next action if paused.  the user may have hit pause after speech or
    // record but before the timer that calls this fired
    if (!gamePaused) {
        // firstly, let's try and just go to next segment
        if ([playGame playNext]) {
            // update line
            [self lineChanged];
            
            // just update webview
            [self updateWebview];
            
            // now do current action
// done by line changed
//            [self doCurrentAction];
        }
    }
}

-(void)pauseAfterConversationFinishedTimerFired:(NSTimer*)inTimer {
    // if we are calibrating
    if (calibrationMode) {
        // pause here
        [self pauseGame];
        
        // bring up calibration complete alert
        UIAlertView *myAlert = [[UIAlertView alloc] initWithTitle:@"Calibration Complete" message:@"Calibration complete.  You will now be returned to your previous conversation." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [myAlert show];
        [myAlert release];
    }
    else {
        // don't bring up score controller anymore, just show on big label and continue
        bigLabel.text = [NSString stringWithFormat:@"Final Score\r\n%@", runningScoreLabel.text];
        
        // pause for user to see score
        self.pauseTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(pauseAfterFinishedLabelTimerFired:) userInfo:nil repeats:NO];
    }
}

/*
-(void)pauseAfterFinishedLabelTimerFired:(NSTimer*)inTimer {
    // clear label
    bigLabel.text = nil;
    
    // go to next conversation and dismiss controller
    [playGame goToNextConversation];
	
	// conversation has changed
	[self gameChanged];
    [self lineChanged];
	
	// pause for a half second for actors to change
    // this is now done in linechanged event
//	self.pauseTimer = [[NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(pauseAfterActorChangeTimerFired:) userInfo:nil repeats:NO] retain];
}*/

-(void)pauseGame {
    // pause first so nothing else fires
    [pauseButton setImage:playImage];
    gamePaused = YES;
    
	// if we are already playing speech, stop and free the existing player
	if (speechPlayer != nil) {
        // pause player
        [speechPlayer pause];
        onResumeAction = onResume_ResumeSpeaking;
        
        // pause lip sync
        [lipSyncTimer invalidate];
        lipSyncTimer = nil;
	}
	else {
        // if we are listening or recording, stop
        if (myRecorder.recordState.state == AUDIORECORDER_STATE_LISTENING || myRecorder.recordState.state == AUDIORECORDER_STATE_RECORDING) {
            [myRecorder pauseRecording];
            onResumeAction = onResume_ResumeListening;
        }
        else {
            onResumeAction = onResume_DoCurrentAction;
        }
    }
}

-(void)resumeGame {
    // if we don't have a game, we can't resume!
    if (playGame == nil)
        return;
    
    // resume
    [pauseButton setImage:pauseImage];
    gamePaused = NO;
    
    // resuming game
    resumingGame = YES;
    
    // what were we doing when we paused?
    if (onResumeAction == onResume_ResumeSpeaking) {
        // play speech
        [speechPlayer play];
        
        // if this is not the users part
        // never user part anymore
        if (YES) {
//        if (![playGame isPlayerPart]) {
            // don't blink while talking, stop any existing lip sync
            [blinkTimer invalidate];
            blinkTimer = nil;
            
            // restart lip sync timer
            lipSyncTimer = [NSTimer scheduledTimerWithTimeInterval:0.0f target:self selector:@selector(lipSyncTimerFired:) userInfo:self repeats:NO];
        }
    }
    else {
//        if (listeningWhenPaused)
//            [myRecorder resumeRecording];
//        else
        if (onResumeAction == onResume_DoCurrentAction || onResumeAction == onResume_ResumeListening)
            [self doCurrentAction];
        else
            if (onResumeAction != onResume_DoNothing)
                [self doNextAction];
    }
    
    // not resuming game anymore
    resumingGame = NO;
}

/*
-(void)selectConversationsButtonPressed:(id)sender {
	// hide this controller
// come back to menu instead    
//	[self.gameMenuController.view removeFromSuperview];
	
	// show select game controller
	ConversationPickerViewController *pickerController = [[ConversationPickerViewController alloc] initWithNibName:@"ConversationPickerViewController" bundle:nil];
	[pickerController setSelectedItems:userProfile.gameData.selectedPractice fromBook:[playGame getBook]];
	pickerController.delegate = self;
	[self.navigationController pushViewController:pickerController animated:YES];
	[pickerController release];
}

-(void)changeBookButtonPressed:(id)sender {
	// hide this controller
// come back to menu instead
//	[self.gameMenuController.view removeFromSuperview];
	
	// show select book controller
    BookSelectViewController *bookController = [[BookSelectViewController alloc] initWithNibName:@"BookSelectViewController" bundle:nil];
	bookController.selectDelegate = self;
	[self.navigationController pushViewController:bookController animated:YES];
	[bookController release];
}

-(void)didMakeBrowserSelection:(NSString*)selection fromController:(UIViewController*)selectController {
    // hide menu controll because we're starting new game
	[self.gameMenuController.view removeFromSuperview];
    
    // user selected new book, so change and save options
    userProfile.gameData.selectedBookName = selection;
    [userProfile saveAs:userProfile.name];
    
	// start timer again because it turns off (think it's when another form comes on top)
	self.labelTimer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(labelTimerFired:) userInfo:nil repeats:YES];
	
    // create new game now we changed the book
    [self restartGameWithBookNamed:selection];
}

-(void)didCancelBrowserSelection {
	// start timer again because it turns off (think it's when another form comes on top)
	self.labelTimer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(labelTimerFired:) userInfo:nil repeats:YES];
}
*/

-(void)optionsButtonPressed:(id)sender {
	// get application delegate and game options
    GameOptions *myOptions = userProfile.gameOptions;
    
	// hide this controller
// come back to menu instead!    
//	[self.gameMenuController.view removeFromSuperview];
	
    // show navivation bar again
    self.navigationController.navigationBarHidden = NO;
    
    // show options
    GameOptionsViewController *optionsController=[[self storyboard] instantiateViewControllerWithIdentifier:@"OptionsVc"];
	optionsController.delegate = self;
	[self.navigationController pushViewController:optionsController animated:YES];
}

-(void)didUpdateOptions {
    // hide navivation bar again
    self.navigationController.navigationBarHidden = YES;
    
	// start timer again because it turns off (think it's when another form comes on top)
	self.labelTimer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(labelTimerFired:) userInfo:nil repeats:YES];
    
    // do current action again in case we've become the actor or vice versa
    [self lineChanged];
    [self updateWebview];
    
    // change to do current action on resume from closing menu
    onResumeAction = onResume_DoCurrentAction;
    [self resumeGame];
}

-(void)didTapToClose {
	// start timer again because it turns off (think it's when another form comes on top)
	self.labelTimer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(labelTimerFired:) userInfo:nil repeats:YES];
    
    // resume game
    [self resumeGame];
}

-(void)didCancelOptions {
    // hide navivation bar again
    self.navigationController.navigationBarHidden = YES;
    
	// start timer again because it turns off (think it's when another form comes on top)
	self.labelTimer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(labelTimerFired:) userInfo:nil repeats:YES];
    
    // change to do current action on resume from closing menu
    // why?
    //    onResumeAction = onResume_DoCurrentAction;
    [self resumeGame];
}

/*
-(void)didMakeSelection:(NSArray*)selection {
    // hide menu controller because we're starting again
	[self.gameMenuController.view removeFromSuperview];
    
	// start timer again because it turns off (think it's when another form comes on top)
	self.labelTimer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(labelTimerFired:) userInfo:nil repeats:YES];
    
	// get application delegate
    McpAppDelegate *appDelegate = (McpAppDelegate*)[[UIApplication sharedApplication] delegate];
    
	// change selected conversations for profile and then create new game which will use this
    [appDelegate.currentUserProfile.gameData removePracticeForBookUniqueKey:[playGame getBook].uniqueKey];
	for (int i=0; i < selection.count; i++)
        [appDelegate.currentUserProfile.gameData.selectedPractice addObject:[selection objectAtIndex:i]];
    [appDelegate.currentUserProfile saveAs:appDelegate.currentUserProfile.name];
    
    // restart game for this book
    [self restartGameWithBookNamed:appDelegate.currentUserProfile.gameData.selectedBookName];
}

-(void)didCancelSelection {
	// start timer again because it turns off (think it's when another form comes on top)
	self.labelTimer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(labelTimerFired:) userInfo:nil repeats:YES];
}
*/

-(void)resumeButtonPressed:(id)sender {
	// hide controller
	[self.gameMenuController.view removeFromSuperview];
	
	// resume game
    [self resumeGame];
}

-(void)quitButtonPressed:(id)sender {
	// hide controller
	[self.gameMenuController.view removeFromSuperview];
    
    // release any objects that have a pointer to us, otherwise dealloc wont
    // be called when controller is popped
    
	// invalidate timers
	[labelTimer invalidate];
    [blinkTimer invalidate];
    [pauseTimer invalidate];
	
	// stop and free audio players
	[speechPlayer stop];
	[speechPlayer release];
    [failPlayer stop];
    [failPlayer release];
    [beepPlayer stop];
    [beepPlayer release];
	
	// show navigation bar again
	[self.navigationController setNavigationBarHidden:NO animated:YES];
	
	// return to main menu for now
	[self.navigationController popViewControllerAnimated:YES];
}

-(void)calibrateButtonPressed:(id)sender {
    // if we are calibrating already
    if (calibrationMode) {
        // this is now not possible
        /*
        // turn off
        userProfile.gameOptions.calibrationMode = NO;
        
        // resume game
        if (!priorPauseState)
            [self resumeGame];
        */
    }
    else {
        // confirm calibration
        UIAlertView *myAlert = [[UIAlertView alloc] initWithTitle:@"Start Calibration" message:@"End current conversation and recalibrate?  Are you sure?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
        [myAlert show];
        [myAlert release];
    }
    
	// hide controller
	[self.gameMenuController.view removeFromSuperview];
}

-(void)pauseAfterActorChangeTimerFired:(NSTimer*)inTimer {
    // update webview
    [self updateWebview];
		
    // now do current action
    [self doCurrentAction];
}

//-(void)blinkAnimationStopped:(id)animationID finished:(BOOL)finished context:(id)context {
-(void)blinkTimerFired:(NSTimer*)inTimer {
    Actor *thisActor = [playGame getActorAtIndex:[playGame getCurrentCharacterIndex]];
    ConverseViewController *viewController = [inTimer userInfo];
    
    // get next image
    UIImage *nextImage;
    switch (viewController.blinkState) {
        case 0: // make half closed
            nextImage = [thisActor.animationFrames objectAtIndex:11];
            break;
        case 1: // make closed
            nextImage = [thisActor.animationFrames objectAtIndex:12];
            break;
        case 2: // make half open
            nextImage = [thisActor.animationFrames objectAtIndex:11];
            break;
        case 3: // make open
            nextImage = [thisActor.animationFrames objectAtIndex:10];
            break;
        default:
            nextImage = [thisActor.animationFrames objectAtIndex:10];
            break;
    }
    
    // always actor 1 now
    actor1.image = nextImage;
    
    /*
    // update to talking to actor
    switch ([playGame getTalkingToCharacterIndex]) {
        case 0:
            actor1.image = nextImage;
            break;
        case 1:
            actor2.image = nextImage;
            break;
        case 2:
            actor3.image = nextImage;
            break;
        case 3:
            actor4.image = nextImage;
            break;
        default:
            break;
   }*/
    
    // increment state, set next timer or invalidate
    blinkState++;
    if (blinkState > 3) {
        blinkState = 0;
        blinkTimer = [[NSTimer scheduledTimerWithTimeInterval:1+rand()%3 target:self selector:@selector(blinkTimerFired:) userInfo:self repeats:NO] retain];
    }
    else
        blinkTimer = [[NSTimer scheduledTimerWithTimeInterval:0.1f target:self selector:@selector(blinkTimerFired:) userInfo:self repeats:NO] retain];
}

-(void)gameChanged {
    // hide all the actors for now
    actor1.alpha = 0;
    actor2.alpha = 0;
    actor3.alpha = 0;
    actor4.alpha = 0;
    lastCharacterIndex = -1;
    
    // load new actor
    
    // use only one actor now
    actor1.image = [[playGame getActorAtIndex:[playGame getCurrentCharacterIndex]].animationFrames objectAtIndex:10];
    
    /*
	// set actor images
	for (int i=0; i < [playGame getCharacterCount]; i++) {
		switch (i) {
			case 0:
                actor1.image = [playGame getCharacterImageAtIndex:i];
				break;
			case 1:
				actor2.image = [playGame getCharacterImageAtIndex:i];
				break;
			case 2:
				actor3.image = [playGame getCharacterImageAtIndex:i];
				break;
			case 3:
				actor4.image = [playGame getCharacterImageAtIndex:i];
				break;
			default:
				break;
		}
	}*/
}

-(void)lineChanged {
    // always actor 1 now
    actor1.alpha = 1.0;
    
    // set actor!
    actor1.image = [[playGame getActorAtIndex:[playGame getCurrentCharacterIndex]].animationFrames objectAtIndex:10];
    
    // fire event to continue
    [self pauseAfterActorChangeTimerFired:nil];
    
    /*
    // if the person we're talking to has changed
    // don't change actors if we're still talking to the same person
    if ([playGame getTalkingToCharacterIndex] != lastCharacterIndex) {
        // show fade in/out
        [UIView beginAnimations:@"frame" context:nil];
        [UIView setAnimationDuration:0.5];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
        
        // show the correct actor
        actor1.alpha = 0;
        actor2.alpha = 0;
        actor3.alpha = 0;
        actor4.alpha = 0;
        switch ([playGame getTalkingToCharacterIndex]) {
            case 0:
                actor1.alpha = 1.0;
                break;
            case 1:
                actor2.alpha = 1.0;
                break;
            case 2:
                actor3.alpha = 1.0;
                break;
            case 3:
                actor4.alpha = 1.0;
                break;
            default:
                break;
        }
        
        // save last index
        lastCharacterIndex = [playGame getTalkingToCharacterIndex];
        
        // end animation
        [UIView commitAnimations];
        
        // pause for a half second for actors to change
        self.pauseTimer = [[NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(pauseAfterActorChangeTimerFired:) userInfo:nil repeats:NO] retain];
    }
    else
        [self pauseAfterActorChangeTimerFired:nil];
     */
}

-(void)updateWebview {
    // don't update web view if not showing subtitles
    if ([McpLocalOptions getSingleton].showSubtitles) {
        // update web view
        [dialogueWebView loadHTMLString:[playGame getCurrentLineAsHtmlWithTextAs:userProfile.gameOptions.showTextAs linkDictionary:[grammarButton.titleLabel.text isEqualToString:@"D"]] baseURL:nil];
        
        // if it is our turn to speak, show web view prompt
        if ([playGame isGetUserInput]) {
            // update hint webview too if we have one
//            [toSayWebView loadHTMLString:[playGame getHintAsHtmlWithTextAs:userProfile.gameOptions.showTextAs linkDictionary:[grammarButton.titleLabel.text isEqualToString:@"D"]] baseURL:nil];
        }
        else {
            // update hint webview too if we have one
//            [toSayWebView loadHTMLString:@"<html/>" baseURL:nil];
        }
        
        // update said text in white
//        [saidWebView loadHTMLString:[playGame getAnswerAsHtmlWithTextAs:userProfile.gameOptions.showTextAs linkDictionary:[grammarButton.titleLabel.text isEqualToString:@"D"]] baseURL:nil];
    }
    else
        [dialogueWebView loadHTMLString:@"" baseURL:nil];
}

-(void)speakText:(NSString*)inText isPrompt:(bool)isPrompt usingLanguage:(NSString *)inLanguage {
    // get application delegate
    McpAppDelegate *appDelegate = (McpAppDelegate*)[[UIApplication sharedApplication] delegate];
    // use dragon test
//    [dragonVocalizer speakString:inText];
    
	// find actor voice, speed and pitch
	int speechVoiceIndex;
    int speechPitch = [playGame getActorAtIndex:[playGame getCurrentCharacterIndex]].speechPitch;
//	int speechPitch = [playGame getCurrentCharacterVoicePitch];
    
    // get speed dependent on language
    int speechSpeed = appDelegate.localOptions.englishSpeed;
    if ([inLanguage isEqualToString:@"Japanese"])
        speechSpeed = appDelegate.localOptions.japaneseSpeed;
    
    if ([[playGame getActorAtIndex:[playGame getCurrentCharacterIndex]].speechVoice isEqualToString:@"Misaki"])
//	if ([[playGame getCurrentCharacterVoiceName] isEqualToString:@"Misaki"])
		speechVoiceIndex = 2;
	else
		speechVoiceIndex = 1;
	
    /*
	if (isPrompt) {
		if (appDelegate.currentUserProfile.gender == 0)
			speechVoiceIndex = 1;
		else
			speechVoiceIndex = 2;
		speechPitch = appDelegate.currentUserProfile.voicePitch;
	}*/
    
	// get documents directory
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	
	// create audio file
	NSString *testText = [playGame getCurrentSegmentForSpeech:YES];
    
    // strip certain characters from the english that cause speech engine crash
    if ([inLanguage isEqualToString:@"English"]) {
        testText = [testText stringByReplacingOccurrencesOfString:@"「" withString:@""];
        testText = [testText stringByReplacingOccurrencesOfString:@"」" withString:@""];
        testText = [testText stringByReplacingOccurrencesOfString:@"。" withString:@"."];
        testText = [testText stringByReplacingOccurrencesOfString:@"、" withString:@","];
        testText = [testText stringByReplacingOccurrencesOfString:@"—" withString:@";"];
        testText = [testText stringByReplacingOccurrencesOfString:@"usu." withString:@"usually"];
        testText = [testText stringByReplacingOccurrencesOfString:@"esp." withString:@"especially"];
        testText = [testText stringByReplacingOccurrencesOfString:@"orig." withString:@"originally"];
        testText = [testText stringByReplacingOccurrencesOfString:@"°" withString:@" degrees "];
    }
    
    // strip certain characters from the english that cause speech engine crash
    if ([inLanguage isEqualToString:@"Japanese"]) {
        testText = [testText stringByReplacingOccurrencesOfString:@"～" withString:@""];
    }
    
    // make intonation rise for ka and ne
//    testText = [testText stringByReplacingOccurrencesOfString:@"か。" withString:@"か？"];
//    testText = [testText stringByReplacingOccurrencesOfString:@"そうですか？" withString:@"そうですか。"];
//    testText = [testText stringByReplacingOccurrencesOfString:@"ね。" withString:@"ね？"];
//    testText = [testText stringByReplacingOccurrencesOfString:@"よ。" withString:@"よ！"];
    const char* cc_testText;
    if ([inLanguage isEqualToString:@"Japanese"])
        cc_testText = [testText cStringUsingEncoding:-2147481087];
    else
        cc_testText = [testText cStringUsingEncoding:NSASCIIStringEncoding];
    
	//	const char* cc_testText = [testText cStringUsingEncoding:NSShiftJISStringEncoding];
	NSString *filename = @"/test000.wav";
	NSString *file_path = [documentsDirectory stringByAppendingString:filename];
	const char* cc_file_path=[file_path UTF8String];
	int result;
    if ([inLanguage isEqualToString:@"Japanese"]) {
        if (speechVoiceIndex == 2)
            result = VT_TextToFile_JPN_Misaki(VT_FILE_API_FMT_S16PCM_WAVE, (char*)cc_testText, (char*)cc_file_path, -1, speechPitch, speechSpeed, 150, 0, -1, -1);
        else
            result = VT_TextToFile_JPN_Show(VT_FILE_API_FMT_S16PCM_WAVE, (char*)cc_testText, (char*)cc_file_path, -1, speechPitch, speechSpeed, 150, 0, -1, -1);
    }
    else {
        if (speechVoiceIndex == 2)
            result = VT_TextToFile_ENG_Julie(VT_FILE_API_FMT_S16PCM_WAVE, (char*)cc_testText, (char*)cc_file_path, -1, speechPitch+5, speechSpeed, 150, 0, -1, -1);
        else
            result = VT_TextToFile_ENG_Paul(VT_FILE_API_FMT_S16PCM_WAVE, (char*)cc_testText, (char*)cc_file_path, -1, speechPitch, speechSpeed, 150, 0, -1, -1);
    }
	if (result != 1) {
		NSException *exception = [NSException exceptionWithName:@"DatabaseError" reason:@"Cannot open speech database" userInfo:nil];
		@throw exception;
	}
	
	// if we are already playing speech, stop and free the existing player
	if (speechPlayer != nil) {
		[speechPlayer stop];
		[speechPlayer release];
		speechPlayer = nil;
	}
	
	// play the file
	NSURL *fileURL = [[NSURL alloc] initFileURLWithPath:file_path];
	AVAudioPlayer *newPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:fileURL error:nil];
	newPlayer.delegate = self;
	[fileURL release];
	[newPlayer prepareToPlay];
	self.speechPlayer = newPlayer;
	[newPlayer release];
    
    // find out length of speech in seconds
    int sampleCount = [AudioFile getSampleCountForFile:file_path];
    float seconds = sampleCount/16000.0f;
    
    // invalidate lip sync timer here in case we interrupted
    [lipSyncTimer invalidate];
    lipSyncTimer = nil;
    lipSyncArrayPos = 0;
    
    // CHANGE HERE.  LIP SYNC TO ALTERNATIVE PRONUNCIATIONS AND CHANGES (USU.=USUALLY)
    
    // setup lip sync array here because we need it even if game is paused
    NSString *kanjiText = [playGame getCurrentSegmentForSpeech:NO];
    NSString *kanaText = [kanjiText copy];
    if ([inLanguage isEqualToString:@"Japanese"])
        kanaText = [HtmlFunctions convertHtml:kanjiText toHtmlAs:2];
    kanaText = [HtmlFunctions stripHtmlTagsFromHtml:kanaText];
    [self updateLipSyncArrayFromKana2:kanaText forSeconds:seconds];
    
    // if resuming, don't pause anymore
    if (gamePaused && resumeAfterUpdate) {
        onResumeAction = onResume_DoNothing;
        [self resumeGame];
    }
    
    // only play if not paused
    if (!gamePaused) {
        // if this is not a prompt
//        if (!isPrompt) {
            // don't blink while talking, stop any existing lip sync
            [blinkTimer invalidate];
            blinkTimer = nil;
            
            // start lip sync timer
            lipSyncTimer = [NSTimer scheduledTimerWithTimeInterval:0.0f target:self selector:@selector(lipSyncTimerFired:) userInfo:self repeats:NO];
//        }
        
        // play speech
        [self.speechPlayer play];
    }
    
    // ok to interrupt (skip forward/back) now
    okToInterrupt = YES;
}

-(void)lipSyncTimerFired:(NSTimer*)inTimer {
    McpAppDelegate *appDelegate = (McpAppDelegate*)[[UIApplication sharedApplication] delegate];
    // set timer to nil just in case we try and use while inside here
    lipSyncTimer = nil;
    
    // get view controller
    ConverseViewController *viewController = [inTimer userInfo];
    
    // update image with next frame
    int frameNumber;
    if (lipSyncArrayPos < lipSyncArray.count) {
        // update image
        LipSyncFrame *thisFrame = [lipSyncArray objectAtIndex:lipSyncArrayPos];
        frameNumber = [[playGame getActorAtIndex:0] getActorFrameForName:thisFrame.frameName];
                   
        // increment frame and cue new timer
        lipSyncArrayPos++;
        lipSyncTimer = [NSTimer scheduledTimerWithTimeInterval:thisFrame.frameLength target:viewController selector:@selector(lipSyncTimerFired:) userInfo:viewController repeats:NO];
    }
    else {
        // set back to rest image
        frameNumber = [[playGame getActorAtIndex:0] getActorFrameForName:@"rest"];
    }
    
    //update image
    // try a blend
    /*
    CATransition *transition = [CATransition animation];
    transition.duration = 0.1f;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionFade;
     [viewController.actor1.layer addAnimation:transition forKey:nil];
    */
    
    Actor *thisActor = [playGame getActorAtIndex:[playGame getCurrentCharacterIndex]];
    switch (0) {
//            switch ([playGame getTalkingToCharacterIndex]) {
        case 0:
            /*
            [UIView transitionWithView:viewController.actor1
                              duration:0.1f
                               options:UIViewAnimationOptionTransitionCrossDissolve
                            animations:^{
                                viewController.actor1.image = [thisActor.animationFrames objectAtIndex:frameNumber];
                            } completion:NULL];*/
            viewController.actor1.image = [thisActor.animationFrames objectAtIndex:frameNumber];
            break;
        case 1:
            viewController.actor2.image = [thisActor.animationFrames objectAtIndex:frameNumber];
            break;
        case 2:
            viewController.actor3.image = [thisActor.animationFrames objectAtIndex:frameNumber];
            break;
        case 3:
            viewController.actor4.image = [thisActor.animationFrames objectAtIndex:frameNumber];
            break;
        default:
            break;
    }
}

-(void)updateLipSyncArrayFromKana2:(NSString*)inKana forSeconds:(float)inSeconds {
    // remove any existing syncs
    McpAppDelegate *appDelegate = (McpAppDelegate*)[[UIApplication sharedApplication] delegate];
    [lipSyncArray removeAllObjects];
    
    // convert katakana to hiragana
    inKana = [TextFunctions katakanaToHiragana:inKana];
    
    // instead of processing one character at a time, try and find longest
    // phonemes first down to smallest
    int charPos = 0;
    float totalLength = 0;
    while (charPos < inKana.length) {
        for (int s=4; s > 0; s--) {
            // see if it matches any phonemes of length 's'
            if (inKana.length-charPos >= s) {
                NSString *thisPhoneme = [inKana substringWithRange:NSMakeRange(charPos, s)];
                NSArray *syncList = [[playGame getActorAtIndex:0] getSyncListForPhoneme:thisPhoneme];
                if (syncList == nil) {
//                    NSLog(@"No lip sync for phoneme %@", thisPhoneme);
                    
                    // if we didn't find anything for length 1, move on
                    if (s == 1)
                        charPos++;
                }
                else {
//                    NSLog(@"found lip sync for phoneme %@", thisPhoneme);
                    // brief pause if no sync list items
                    if (syncList.count == 0) {
                        // get last frame
                        LipSyncFrame *lastFrame = [lipSyncArray objectAtIndex:lipSyncArray.count-1];
                        
                        // add last frame again
                        [lipSyncArray addObject:[[LipSyncFrame alloc] initWithName:lastFrame.frameName andLength:0.5f]];
                        totalLength += 0.5f;
                    }
                    else {
                        // loop through and add to full array
                        for (int s=0; s < syncList.count; s++) {
                            NSDictionary *thisEntry = [syncList objectAtIndex:s];
                            NSString *frameName = [thisEntry objectForKey:@"FrameName"];
                            NSNumber *frameLength = [thisEntry objectForKey:@"FrameLength"];
                            LipSyncFrame *newFrame = [[LipSyncFrame alloc] initWithName:frameName andLength:[frameLength floatValue]];
                            [lipSyncArray addObject:newFrame];
                            [newFrame release];
                            totalLength += [frameLength floatValue];
                        }
                    }
                    
                    // move on
                    charPos += s;
                }
            }
        }
    }
    
    // go through and adjust length based on length of sentence in seconds
    for (int i=0; i < lipSyncArray.count; i++) {
        // get this frame
        LipSyncFrame *thisFrame = [lipSyncArray objectAtIndex:i];
        
        // adjust length
        thisFrame.frameLength = (inSeconds*0.95f) * (thisFrame.frameLength/totalLength);
    }
}

-(void)updateLipSyncArrayFromKana:(NSString*)inKana forSeconds:(float)inSeconds {
    // remove any existing syncs
    McpAppDelegate *appDelegate = (McpAppDelegate*)[[UIApplication sharedApplication] delegate];
    [lipSyncArray removeAllObjects];
    
    // convert katakana to hiragana
    inKana = [TextFunctions katakanaToHiragana:inKana];
    
    // get count of voiced characters
    int voicedCount = 0;
    for (int i=0; i < inKana.length; i++) {
        NSString *thisChar = [inKana substringWithRange:NSMakeRange(i, 1)];
        NSRange found = [@" 　、。？！「」" rangeOfString:thisChar] ;
        if (found.location == NSNotFound)
            voicedCount++;
    }
    
    // loop through kana one at a time and product lip sync array
    // for now just to one character at a time
    float waitTime = 0.0f;
    for (int i=0; i < inKana.length; i++) {
        // ignore white spaces and punctuation
        NSString *thisChar = [inKana substringWithRange:NSMakeRange(i, 1)];
        NSRange found = [@" 　、。？！「」" rangeOfString:thisChar] ;
        if (found.location == NSNotFound) {
            NSArray *syncList = [[playGame getActorAtIndex:0] getSyncListForPhoneme:thisChar];
            if (syncList.count == 0) {
                waitTime += 1.0f;
                NSLog(@"No lip sync for phoneme %@", thisChar);
            }
            else {
                // loop through and add to full array
                for (int s=0; s < syncList.count; s++) {
                    NSDictionary *thisEntry = [syncList objectAtIndex:s];
                    NSString *frameName = [thisEntry objectForKey:@"FrameName"];
                    NSNumber *frameLength = [thisEntry objectForKey:@"FrameLength"];
                    float adjustedLength = ([frameLength floatValue]+waitTime)*(inSeconds/(float)voicedCount);
                    LipSyncFrame *newFrame = [[LipSyncFrame alloc] initWithName:frameName andLength:adjustedLength];
                    [lipSyncArray addObject:newFrame];
                    [newFrame release];
                    waitTime = 0.0f;
                }
            }
        }
    }
}

-(void)startListening {
    /*
    // which language is this conversation using?
    if ([[playGame getCurrentSegmentLanguageName] isEqualToString:@"English"]) {
        // use nuance
        SKRecognizer *recognizer = [[SKRecognizer alloc] initWithType:SKDictationRecognizerType
                                                            detection:SKShortEndOfSpeechDetection
                                                             language:@"eng-USA"
                                                             delegate:self];
    }
    else {
        // use nuance
        SKRecognizer *recognizer = [[SKRecognizer alloc] initWithType:SKDictationRecognizerType
                                                            detection:SKShortEndOfSpeechDetection
                                                             language:@"jpn-JPN"
                                                             delegate:self];
    }*/
    
    /*
    // start listener for ispeech
	NSError *err;
	if(![isRecognition listen:&err]) {
		NSLog(@"ERROR: %@", err);
	}
    
    // use animation instead of timer
	[UIView beginAnimations:@"blink" context:nil];
	[UIView setAnimationDuration:1+rand()%3];
	[UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
	[UIView setAnimationDelegate:self];
	[UIView setAnimationDidStopSelector:@selector(blinkAnimationStopped:finished:context:)];
	[UIView commitAnimations];
    
    return;
     */
    
    // sound beep so user knows to start speaking
    [beepPlayer play];
    
    // update label so user knows to speak
    bigLabel.text = @"GO!";
    
    // pause for a half second, then clear the label
    self.pauseTimer = [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(pauseAfterBigLabelTimerFired:) userInfo:nil repeats:NO];

    // use animation instead of timer
	[UIView beginAnimations:@"blink" context:nil];
	[UIView setAnimationDuration:1+rand()%3];
	[UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
	[UIView setAnimationDelegate:self];
	[UIView setAnimationDidStopSelector:@selector(blinkAnimationStopped:finished:context:)];
	[UIView commitAnimations];
}

- (void)recognizer:(SKRecognizer *)recognizer didFinishWithResults:(SKRecognition *)results {
    [recognizer autorelease];
	NSLog(@"Result: %@", results.firstResult);
    
    // process using text
//    [playGame processAnswerUsingText:@"hello"];
    [playGame processAnswerUsingText:results.firstResult];
    
    // first update 'said' text in white
    [saidWebView loadHTMLString:[playGame getAnswerAsHtmlWithTextAs:userProfile.gameOptions.showTextAs linkDictionary:[grammarButton.titleLabel.text isEqualToString:@"D"]] baseURL:nil];
    
    // play 'ditty'
    
    // now loop through and find matches and update 'said' and bottom to green/red depending
    // on what's found or missing
    // wait for a second so user can see what they said
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC), dispatch_get_current_queue(), ^{
        // loop through words to find matches
        [self findSaidMatches];
    });
}

- (void)recognizer:(SKRecognizer *)recognizer didFinishWithError:(NSError *)error suggestion:(NSString *)suggestion {
    [recognizer autorelease];
    // Inform the user of the error and suggestion
}

- (void)recognizerDidBeginRecording:(SKRecognizer *)recognizer {
    // Update the UI to indicate the system is now recording
    statusLabel.text = @"Recording...";
}

- (void)recognizerDidFinishRecording:(SKRecognizer *)recognizer {
    // Update the UI to indicate that recording has stopped and the speech is still being processed
    statusLabel.text = @"Analyzing...";
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
	return (interfaceOrientation == UIDeviceOrientationPortrait);
}

- (void)recognition:(ISSpeechRecognition *)speechRecognition didGetRecognitionResult:(ISSpeechRecognitionResult *)result {
	NSLog(@"Method: %@", NSStringFromSelector(_cmd));
	NSLog(@"Result: %@", result.text);
    
    // process using text
    [playGame processAnswerUsingText:@"hello"];
//    [playGame processAnswerUsingText:result.text];
    
    // first update 'said' text in white
    [saidWebView loadHTMLString:[playGame getAnswerAsHtmlWithTextAs:userProfile.gameOptions.showTextAs linkDictionary:[grammarButton.titleLabel.text isEqualToString:@"D"]] baseURL:nil];
    
    // play 'ditty'

    // now loop through and find matches and update 'said' and bottom to green/red depending
    // on what's found or missing
    // wait for a second so user can see what they said
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC), dispatch_get_current_queue(), ^{
        // loop through words to find matches
        [self findSaidMatches];
    });

	// wait a little so we exit here
// now executed by findsaidmatches
//    [self performSelector:@selector(doNextAction) withObject:nil afterDelay:2];
    
    // do next action
//    [self doNextAction];
}

-(void)findSaidMatches {
    // find match, if there are more, call recursively
    BOOL matchFound;
    BOOL moreWords = [playGame findNextMatch:&matchFound];
    
    // if match was found
    if (matchFound) {
        // update said and question web views
        [saidWebView loadHTMLString:[playGame getAnswerAsHtmlWithTextAs:userProfile.gameOptions.showTextAs linkDictionary:[grammarButton.titleLabel.text isEqualToString:@"D"]] baseURL:nil];
        [dialogueWebView loadHTMLString:[playGame getCurrentLineAsHtmlWithTextAs:userProfile.gameOptions.showTextAs linkDictionary:[grammarButton.titleLabel.text isEqualToString:@"D"]] baseURL:nil];
        
        // play 'ditty'
        [matchedPlayer play];
    }

    if (moreWords) {
        // call recursively after delay
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (matchFound?1:0) * NSEC_PER_SEC), dispatch_get_current_queue(), ^{
            // find next match if more words
            if (moreWords)
                [self findSaidMatches];
        });
    }
    else {
        // wait a little then try again or move on
        [self performSelector:@selector(doNextAction) withObject:nil afterDelay:1];
    }
}

- (void)recognition:(ISSpeechRecognition *)speechRecognition didFailWithError:(NSError *)error {
	NSLog(@"Method: %@", NSStringFromSelector(_cmd));
	NSLog(@"Error: %@", error);
}

- (void)recognitionCancelledByUser:(ISSpeechRecognition *)speechRecognition {
	NSLog(@"Method: %@", NSStringFromSelector(_cmd));
}

- (void)recognitionDidBeginRecording:(ISSpeechRecognition *)speechRecognition {
	NSLog(@"Method: %@", NSStringFromSelector(_cmd));
    statusLabel.text = @"Recording...";
}

- (void)recognitionDidFinishRecording:(ISSpeechRecognition *)speechRecognition {
	NSLog(@"Method: %@", NSStringFromSelector(_cmd));
    // update label
    statusLabel.text = @"Analyzing...";
}

-(void)pauseAfterBigLabelTimerFired:(NSTimer*)inTimer {
    // clear label
    bigLabel.text = nil;
}

-(void)pauseAfterSpeakingTimerFired:(NSTimer*)inTimer {
//    printf("pauseAfterSpeakingTimerFired\r\n");
    
    // release the player and reset the variable so we know we're not playing
    // do this straight after speech finishes, otherwise pause during this timer
    // is not during speaking and next action hasn't been chosen yet
//    [speechPlayer release];
//    speechPlayer = nil;
    
    // don't look for game paused in timer routines, pause again after starting next
    //  action
    // if this is a player part, start listening after prompting
//    if ([playGame isPlayerPart])
    if ([playGame isGetUserInput]) {
        // start listening
        [self startListening];
    }
    else {
        NSLog(@"Call doNextAction for pauseAfterSpeakingTimerFired");
        // release timer and do the next action
        [self doNextAction];
    }
}

-(void)viewLongPressed:(id)sender {
    // get recognizer
    UILongPressGestureRecognizer *recognizer = sender;
    
    // state?
    switch (recognizer.state) {
        case UIGestureRecognizerStateBegan:
            // if we are listening, show thought menu with choices
            if ([playGame isGetUserInput]) {
                // hide controls if we are showing
                [self hideMenuAndControls];
                
                // show thought menu
                // if different view controller depending on number of options
                if ([playGame getAnswerChoices].count > 2)
                    thoughtMenu = [[ThoughtMenuViewController alloc] initWithNibName:@"ThoughtMenuViewController2" bundle:nil];
                else
                    thoughtMenu = [[ThoughtMenuViewController alloc] initWithNibName:@"ThoughtMenuViewController" bundle:nil];
                [thoughtMenu setMenuItemStrings:[playGame getAnswerChoices]];
                thoughtMenu.delegate = self;
                [self addChildViewController:thoughtMenu];
                [thoughtMenu didMoveToParentViewController:self];
                [self.view addSubview:thoughtMenu.view];
            }
            break;
        case UIGestureRecognizerStateChanged:
            // pass to thought menu
            if (thoughtMenu != nil)
                [thoughtMenu processLongPress:sender];
            break;
        case UIGestureRecognizerStateEnded:
            // pass to thought menu
            if (thoughtMenu != nil)
                [thoughtMenu processLongPress:sender];
            break;
        default:
            break;
    }
}

-(BOOL)automaticallyForwardAppearanceAndRotationMethodsToChildViewControllers {
    return YES;
}

-(void)didMakeThoughtSelection:(int)inSelection {
    NSLog(@"Thought selection made");
    // pause speaking and listening, so we don't fire finish speaking timer at the same time
    NSLog(@"Made thought selection");
    [myRecorder pauseRecording];
    [speechPlayer pause];
    
    // invalidate pause timer too, as it may go off and fire another donextaction
    [pauseTimer invalidate];
    self.pauseTimer = nil;
    
    // process answer
    [playGame processAnswerUsingSelection:inSelection];
    
    // do next action
    [self doNextAction];
    
    // remove and release menu
    [thoughtMenu removeFromParentViewController];
    [thoughtMenu.view removeFromSuperview];
    thoughtMenu = nil;
}

-(void)didCancelThoughtSelection {
    // remove and release menu
    [thoughtMenu removeFromParentViewController];
    [thoughtMenu.view removeFromSuperview];
    thoughtMenu = nil;
}

-(void)pauseAfterListeningTimerFired:(NSTimer*)inTimer {
    if (!gamePaused) {
        NSLog(@"Do next actiona for pauseAfterListeningTimerFired");
		// do the next action
		[self doNextAction];
	}
}

-(void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag {
//    printf("audio finished\r\n");
	// get application delegate
    McpAppDelegate *appDelegate = (McpAppDelegate*)[[UIApplication sharedApplication] delegate];
    
	// if this is the speech player and not the beep sound
	if (player == speechPlayer) {
        // try releasing the player after the pause so the forward/back still work
        // even for short bursts of text such as 'de mo'
        // ^^ no
        // have to release here, otherwise pause during wait causes issues
        
		// release the player and reset the variable so we know we're not playing
		[speechPlayer release];
		speechPlayer = nil;
        
        // if user has asked for a long pause, make it a second for them
        // to digest the information
        if ([playGame isShortPause])
            // pause for a half second, then continue
            self.pauseTimer = [NSTimer scheduledTimerWithTimeInterval:0.7f target:self selector:@selector(pauseAfterSpeakingTimerFired:) userInfo:nil repeats:NO];
        else
            // pause for a half second, then continue
            self.pauseTimer = [NSTimer scheduledTimerWithTimeInterval:0.2f target:self selector:@selector(pauseAfterSpeakingTimerFired:) userInfo:nil repeats:NO];
	}
    
    // if this is beep, start recording
    if (player == beepPlayer) {
        // if resuming, don't pause anymore
        if (gamePaused && resumeAfterUpdate) {
            onResumeAction = onResume_DoNothing;
            [self resumeGame];
        }
        
        // start recording if not paused
        if (!gamePaused) {
            /*
            // try nuance engine
            SKRecognizer *dragonRecognizer = [[SKRecognizer alloc] initWithType:SKDictationRecognizerType detection:SKLongEndOfSpeechDetection language:@"ja_JP" delegate:self];
            return;*/
            
            // check here to see if the recording rate has changed
            if ([userProfile getSampleRateForAudioDevice] != [myRecorder getSampleRate]) {
                [myRecorder release];
                myRecorder = [[AudioRecorder alloc] initWithSampleRate:[userProfile getSampleRateForAudioDevice] startMuted:appDelegate.localOptions.isMicMuted];
            }
            
            // start recording
            [myRecorder startRecording];
        }
        
        // ok to interrupt (skip forward/back) now
        okToInterrupt = YES;
    }
}

-(BOOL)isMuted {
    return myRecorder.isMuted;
}

-(IBAction)menuButtonPressed:(id)sender {
    // hide menu and controls
    [self hideMenuAndControls];
    
    // pause game
    [self pauseGame];
    
    // just use alert view
    UIActionSheet *actionMenu = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Resume" destructiveButtonTitle:@"Exit to Menu" otherButtonTitles:@"Change Game", @"Change Deck", @"Section Picker", @"Options", @"Progress Report", nil];
    [actionMenu showInView:self.view];
    [actionMenu release];
    /*
    UIAlertView *alertMenu = [[UIAlertView alloc] initWithTitle:@"Game Menu" message:nil delegate:self cancelButtonTitle:@"Resume" otherButtonTitles:@"Change Game", @"Change Deck", @"Section Picker", @"Options", @"Progress Report", @"Exit to Menu", nil];
    alertMenu.delegate = self;
    [alertMenu show];
    [alertMenu release];
*/
    
    /*
    // if we are in calibration mode, this will cancel it
    if (calibrationMode) {
        // confirm cancellation
        [self pauseGame];
        
        // confirm first, then skip to next conversation
        UIAlertView *myAlert = [[UIAlertView alloc] initWithTitle:@"Menu" message:@"This will cancel calibration, are you sure?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
        [myAlert show];
        [myAlert release];
    }
    else
        [self showMenu];
    */
}

-(void)showMenu {
	// pause game
	[self pauseGame];
	
	// bring up menu controller
	GameMenuViewController *myMenuController = [[GameMenuViewController alloc] initWithNibName:@"GameMenuViewController" bundle:nil];
	self.gameMenuController = myMenuController;
    self.gameMenuController.view.frame = self.view.frame;
	[self.view addSubview:myMenuController.view];
	[myMenuController release];
    
    /*
     // change calibrate description depending on state
     if (userProfile.gameOptions.calibrationMode)
     [self.gameMenuController.calibrateButton setTitle:@"Cancel Recalibration" forState:UIControlStateNormal];
     else
     [self.gameMenuController.calibrateButton setTitle:@"Recalibrate" forState:UIControlStateNormal];
     */
    
	// get notify of mouse up
	[myMenuController.selectConversationsButton addTarget:self action:@selector(selectConversationsButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
	[myMenuController.optionsButton addTarget:self action:@selector(optionsButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
	[myMenuController.changeBookButton addTarget:self action:@selector(changeBookButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
	[myMenuController.resumeButton addTarget:self action:@selector(resumeButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
	[myMenuController.quitButton addTarget:self action:@selector(quitButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
	[myMenuController.calibrateButton addTarget:self action:@selector(calibrateButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
}

-(IBAction)backButtonPressed:(id)sender {
    // reset hide controls timer
    [self resetHideControlsTimer];
    
    // firstly, pause the game
    [self pauseGame];
    
    // confirm first, then skip to next conversation
    UIAlertView *myAlert = [[UIAlertView alloc] initWithTitle:@"Previous Game" message:@"Change game type, are you sure?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    [myAlert show];
    [myAlert release];
}

-(IBAction)rewindButtonPressed:(id)sender {
    printf("rewind pressed\r\n");
    
    // reset hide controls timer
    [self resetHideControlsTimer];
    
	// ok to interrupt?
	if (okToInterrupt) {
		// can't interrupt anymore until listening again
		okToInterrupt = NO;
		
        // pause and restart after update
        if (!gamePaused) {
            [self pauseGame];
            resumeAfterUpdate = YES;
        }
        else
            resumeAfterUpdate = NO; // don't resume if already paused
        
		// if we are actively listening, stop
		if (myRecorder.recordState.state == AUDIORECORDER_STATE_LISTENING) {
            [myRecorder pauseRecording];
		}
		
        // if we are speaking, and it's been move than 1/2 second since rewind last pushed
        // just start same action again
        float elapsedTime = CFAbsoluteTimeGetCurrent()-rewindLastPushedTime;
        // if we're waiting for input, do previous sentence too
//        if (![playGame isGetUserInput] && elapsedTime > 1.0f) {
        if (elapsedTime > 1.0f) {
            // play this part again
            [self doCurrentAction];
        }
        else {
            // tell game to rewind, or do current bit again if we've hit the wall
            if ([playGame rewind]) {
                // just update webview
                [self updateWebview];
                
                // now do current action
                [self doCurrentAction];
            }
            else {
                // play current part again
                [self doCurrentAction];
            }
        }
        
        // save last pushed time
        rewindLastPushedTime = CFAbsoluteTimeGetCurrent();
	}
}

-(IBAction)pauseButtonPressed:(id)sender {
    // reset hide controls timer
    [self resetHideControlsTimer];
    
    // pause or play
    if (gamePaused)
        [self resumeGame];
    else {
        // pause and set resume off
        [self pauseGame];
        resumeAfterUpdate = NO;
    }
}

-(IBAction)forwardButtonPressed:(id)sender {
    // reset hide controls timer
    [self resetHideControlsTimer];
    
    // firstly, pause the game
    [self pauseGame];
    
    // confirm first, then skip to next conversation
    UIAlertView *myAlert = [[UIAlertView alloc] initWithTitle:@"Next Game" message:@"Change game type, are you sure?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    [myAlert show];
    [myAlert release];
}

-(void)changeGamePressed:(id)sender {
    // display menu asking which game they want
    UIAlertView *alertMenu = [[UIAlertView alloc] initWithTitle:@"Change Game" message:nil delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Guess Japanese", @"Guess English", @"Guess Missing Word", nil];
    alertMenu.delegate = self;
    [alertMenu show];
    [alertMenu release];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // call super first
    [super prepareForSegue:segue sender:sender];
    
    // is this is the quick help, pause the game
    if ([[segue identifier] isEqualToString:@"QuickHelpSegue"]) {
        HelpViewController *helpController = [segue destinationViewController];
        helpController.delegate = self;
        [self pauseGame];
    }
    
    // is this is the play controller
    if ([[segue identifier] isEqualToString:@"ShowProgress"]) {
        // update stats
        ProgressViewController *progressController = [segue destinationViewController];
        progressController.delegate = self;
        [progressController setGame:[playGame getFlashGame]];
    }
}

-(void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex {
    // which menu option?
    
    // resume
    if ([[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString:@"Resume"]) {
        [self resumeGame];
    }
    
    // change game
    if ([[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString:@"Change Game"]) {
        [self changeGamePressed:self];
    }
    
    // change deck
    if ([[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString:@"Change Deck"]) {
        // show deck picker
        [self showDeckPicker];
    }
    
    // section picker
    if ([[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString:@"Section Picker"]) {
        [self showSectionPicker];
    }
    
    // options
    if ([[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString:@"Options"]) {
        [self optionsButtonPressed:actionSheet];
        /*
         // do current action again
         [self lineChanged];
         [self updateWebview];
         
         // change to do current action on resume from closing menu
         onResumeAction = onResume_DoCurrentAction;
         [self resumeGame];
         */
        
    }
    
    // progress report
    if ([[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString:@"Progress Report"]) {
        [self performSegueWithIdentifier:@"ShowProgress" sender:self];
    }
    
    // exit to menu
    if ([[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString:@"Exit to Menu"]) {
        [self quitButtonPressed:actionSheet];
    }
    
    /*
    // game menu?
    if ([alertView.title isEqualToString:@"Game Menu"]) {
        // what did we do?
        switch (buttonIndex) {
            case 0: // resume
                [self resumeGame];
                break;
            case 1: // change game
                [self changeGamePressed:self];
                
                break;
            case 2: // change deck
                // show deck picker
                [self showDeckPicker];
                break;
            case 3: // section picker
                [self showSectionPicker];
                break;
            case 4: // options
                [self optionsButtonPressed:alertView];
     
                break;
            case 5: // progress report
                [self performSegueWithIdentifier:@"ShowProgress" sender:self];
                break;
            case 6: // exit to menu
                [self quitButtonPressed:alertView];
                break;
            default:
                break;
        }
    }
    */
}

-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    // get app delegate
    McpAppDelegate *appDelegate = (McpAppDelegate*)[[UIApplication sharedApplication] delegate];
    
    // make well known
    if ([alertView.title isEqualToString:@"Mark Well Known"]) {
        if (buttonIndex == 1) {
            // make well known
            [playGame makeWellKnown];
            
            // get the name of the word
            FlashCard *currentCard = [playGame getCurrentFlashCard];
            
            // re-do current action to show new re-routed block
            resumeAfterUpdate = YES;
            [self updateWebview];
            [self doCurrentAction];
        }
        else
            // resume
            [self resumeGame];
    }
    
    // skip forward?
    if ([alertView.title isEqualToString:@"Change Game"]) {
        if (buttonIndex != alertView.cancelButtonIndex) {
            // change the game
            switch (buttonIndex) {
                case 1:
                    [playGame changeGameTo:gameType_SayJapaneseFromEnglish];
                    break;
                case 2:
                    [playGame changeGameTo:gameType_SayEnglishFromJapanese];
                    break;
                case 3:
                    [playGame changeGameTo:gameType_SayBlankWord];
                    break;
                case 4:
                    [playGame changeGameTo:gameType_SayBlankParticle];
                    break;
                default:
                    [playGame changeGameTo:gameType_SayJapaneseFromEnglish];
                    break;
            }
            
            // do current action again
            [self lineChanged];
            [self updateWebview];
            
            // change to do current action on resume from closing menu
            onResumeAction = onResume_DoCurrentAction;
        }
        
        // resume game
        [self resumeGame];
    }
    
    if ([alertView.title isEqualToString:@"Start Calibration"]) {
        if (buttonIndex ==  1) {
            [self startCalibration];
        }
    }
    
    if ([alertView.title isEqualToString:@"Menu"]) {
        if (buttonIndex ==  1) {
            // cancel calibration
            [self cancelCalibration];
            
            // show menu now, but override prior pause state to resume
            [self showMenu];
        }
        else
            [self resumeGame];
    }
    
    if ([alertView.title isEqualToString:@"Audio Device Changed"]) {
        // cancel calibration
        [self cancelCalibration];
        
        // resume game
        resumeAfterUpdate = YES;
    }
    
    if ([alertView.title isEqualToString:@"Calibrate"]) {
        if (buttonIndex ==  1) {
            [self startCalibration];
        }
        else
            [self resumeGame];
    }
    
    /*
    if ([alertView.title isEqualToString:@"Calibration Complete"]) {
        // restore previous game state and save profile with new network
        [userProfile getNetForAudioDevice].trained = YES;
        calibrationMode = NO;
        [userProfile saveAs:userProfile.name];
        [cpuProfile saveAs:cpuProfile.name];
        
        // release backup network
        [backupNet release];
        
        // change book back to user's selected book
        [self restartGameWithBookNamed:userProfile.gameData.selectedBookName];
    }*/
}

-(void)showSectionPicker {
    // get application delegate
    McpAppDelegate *appDelegate = (McpAppDelegate*)[[UIApplication sharedApplication] delegate];
    
    // show section picker
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Storyboard" bundle:nil];
    SectionPickerViewController *mySectionPicker = [storyboard instantiateViewControllerWithIdentifier:@"SectionPicker"];;
    [mySectionPicker setDeck:[playGame getDeck] gameType:[playGame getGameType] localOptions:appDelegate.localOptions appOptions:appOptions];
    mySectionPicker.delegate = self;
    [self.navigationController pushViewController:mySectionPicker animated:YES];
    
    // show nagivation bar again
    self.navigationController.navigationBarHidden = NO;
}

-(void)didPickSections {
    // restart the game
    [self restartGameWithBookNamed:[playGame getDeck].deckName completion:^(BOOL success) {
        // hide nav bar again
        self.navigationController.navigationBarHidden = YES;
        
        // if success
        if (success) {
            // start timer again because it turns off (think it's when another form comes on top)
            self.labelTimer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(labelTimerFired:) userInfo:nil repeats:YES];
            
            // resume game
            [self resumeGame];
        }
        else {
            // show alert
            UIAlertView *myAlert = [[UIAlertView alloc] initWithTitle:@"Can't Start Game" message:@"There was a problem starting this game.  Check to make sure you have a valid deck and sections selected." delegate:self cancelButtonTitle:@"Understood" otherButtonTitles:nil];
            [myAlert show];
            [myAlert release];
        }
    }];
    
}

-(void)didCancelPickSections {
    // hide nav bar again
    self.navigationController.navigationBarHidden = YES;
    
	// start timer again because it turns off (think it's when another form comes on top)
	self.labelTimer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(labelTimerFired:) userInfo:nil repeats:YES];
    
    // change to do current action on resume from closing menu
    // why?
    //    onResumeAction = onResume_DoCurrentAction;
    [self resumeGame];
}

-(void)showDeckPicker {
	// create fade view
    sectionPickerFader = [[UIView alloc] initWithFrame:self.view.frame];
	sectionPickerFader.backgroundColor = [UIColor clearColor];
	sectionPickerFader.opaque = NO;
	[[self.view superview] addSubview:sectionPickerFader];
    
    // get application delegate
    McpAppDelegate *appDelegate = (McpAppDelegate*)[[UIApplication sharedApplication] delegate];
    
	// bring up deck picker controller
	deckPicker = [[DeckPickerView alloc]  initWithDeckType:@"All" parentViewController:self usingLocalOptions:appDelegate.localOptions];
	deckPicker.delegate = self;
	CGRect myFrame = deckPicker.frame;
	myFrame.origin.y = myFrame.size.height;
	deckPicker.frame = myFrame;
	[[self.view superview] addSubview:deckPicker];
	
	// animate bringing picker up from bottom and fade
	[UIView beginAnimations:@"frame" context:nil];
	[UIView setAnimationDuration:0.5];
	[UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
	
	// change origin of picker to real origin
	myFrame.origin.y = 0;
	deckPicker.frame = myFrame;
	
	// fade view
	sectionPickerFader.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
    
	// animate!
	[UIView commitAnimations];
}

-(void)didPickDeckWithName:(NSString*)inDeckName {
    // get application delegate
    McpAppDelegate *appDelegate = (McpAppDelegate*)[[UIApplication sharedApplication] delegate];
    
    // save last played deck in options
    [appDelegate.localOptions setLastPlayedDeckName:inDeckName];
    
    // last played game type for this deck now becomes THIS game type
    [appDelegate.localOptions setLastPlayedGameType:[playGame getGameType] forDeck:inDeckName];
    [appDelegate.localOptions save];
    
    // restart the game
    [self restartGameWithBookNamed:inDeckName completion:^(BOOL success) {
        // hide picker
        [self hideDeckPicker];
        
        // if success
        if (success) {
            // resume game
            [self resumeGame];
        }
        else {
            // show alert
            UIAlertView *myAlert = [[UIAlertView alloc] initWithTitle:@"Can't Start Game" message:@"There was a problem starting this game.  Check to make sure you have a valid deck and sections selected." delegate:self cancelButtonTitle:@"Understood" otherButtonTitles:nil];
            [myAlert show];
            [myAlert release];
        }
    }];
    
}

-(void)didCancelPick {
    // hide picker
    [self hideDeckPicker];
    
    // change to do current action on resume from closing menu
    // why?
    //    onResumeAction = onResume_DoCurrentAction;
    [self resumeGame];
}

-(void)hideDeckPicker {
	// animate bringing picker up from bottom and fade
	[UIView beginAnimations:@"frame" context:nil];
	[UIView setAnimationDuration:0.5];
	[UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
	[UIView setAnimationDelegate:self];
	[UIView setAnimationDidStopSelector:@selector(hidePickerStopped:finished:context:)];
	
	// change origin of picker to real origin
	CGRect myFrame = deckPicker.frame;
	myFrame.origin.y = 480;
	deckPicker.frame = myFrame;
	
	// fade view
	sectionPickerFader.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.0];
	
	// animate!
	[UIView commitAnimations];
}

-(void)hidePickerStopped:(id)animationID finished:(BOOL)finished context:(id)context {
	// remove picker and fader
	[deckPicker removeFromSuperview];
	[sectionPickerFader removeFromSuperview];
	
	// re-enable navigation item
//	self.navigationItem.rightBarButtonItem.enabled = YES;
//	self.navigationItem.leftBarButtonItem.enabled = YES;
}

-(void)restartGameWithBookNamed:(NSString*)inBookName completion:(void(^)(BOOL success)) block {
    // get app delegate
    McpAppDelegate *appDelegate = (McpAppDelegate*)[[UIApplication sharedApplication] delegate];
    
    // get last played deck name
    NSString *lastPlayedName = [[appDelegate localOptions] getLastPlayedDeckName];
    
    /*
    // create conv game test!
    playGame = [[InteractiveConvGame alloc] initWithBookName:lastPlayedName viewController:self completion:^(BOOL success) {
        if (success) {
            // inform game and line has changed
            [self gameChanged];
            [self lineChanged];
            
            // call completion block
            block(YES);
        }
        else {
            playGame = nil;
            block(NO);
        }
    }];*/
    
    // create new game
    playGame = [[FlashConvGame alloc] initWithBookName:lastPlayedName giveClassIntro:NO viewController:self completion:^(BOOL success) {
        if (success) {
            // inform game and line has changed
            [self gameChanged];
            [self lineChanged];
            
            // call completion block
            block(YES);
        }
        else {
            playGame = nil;
            block(NO);
        }
    }];
}

-(IBAction)fastForwardButtonPressed:(id)sender {
    // reset hide controls timer
    [self resetHideControlsTimer];
    
	// ok to interrupt?
	if (okToInterrupt) {
		// can't interrupt anymore until listening or speaking again
		okToInterrupt = NO;
		
        // pause and resume after change
        if (!gamePaused) {
            [self pauseGame];
            resumeAfterUpdate = YES;
        }
        else
            resumeAfterUpdate = NO; // don't resume if already paused
        
		// if we are actively listening, stop
		if (myRecorder.recordState.state == AUDIORECORDER_STATE_LISTENING) {
            [myRecorder pauseRecording];
		}
		
        // don't use do next action because we don't want to go to next conversation
        // firstly, let's try and just go to next segment
        if ([playGame fastForward]) {
            // just update webview
            [self updateWebview];
            
            // now do current action
            [self doCurrentAction];
        }
	}
}

-(IBAction)grammarButtonPressed:(id)sender {
    // toggle title
    if ([grammarButton.titleLabel.text isEqualToString:@"G"])
        [grammarButton setTitle:@"D" forState:UIControlStateNormal];
    else
        [grammarButton setTitle:@"G" forState:UIControlStateNormal];
    
    // update web view to change links
    [self updateWebview];
}

-(IBAction)correctButtonPressed:(id)sender {
    // get application delegate
    McpAppDelegate *appDelegate = (McpAppDelegate*)[[UIApplication sharedApplication] delegate];
    
    // pause game
    [self pauseGame];
    
    // show correction view controller
    CorrectionViewController *correctionController = [[CorrectionViewController alloc]  initWithNibName:@"CorrectionViewController" bundle:nil sampleSentence:[playGame getCurrentSampleSentence]];
    correctionController.delegate = self;
    [self.navigationController pushViewController:correctionController animated:YES];
    [correctionController release];
    
    // show nagivation bar again
    self.navigationController.navigationBarHidden = NO;
}

-(IBAction)knownButtonPressed:(id)sender {
    // pause game
    [self pauseGame];
    
    // hide menu and controls again
    [self hideMenuAndControls];
    
    // get the name of the word
    FlashCard *currentCard = [playGame getCurrentFlashCard];
    
    // confirm want to make well known!
    UIAlertView *myAlert = [[UIAlertView alloc] initWithTitle:@"Mark Well Known" message:[NSString stringWithFormat:@"Are you sure you want to mark the word %@ (%@) as well known?", currentCard.kanji, currentCard.kana] delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    [myAlert show];
    [myAlert release];
}

-(void)didMakeCorrection {
    // hide nav bar again
    self.navigationController.navigationBarHidden = YES;
    
	// start timer again because it turns off (think it's when another form comes on top)
	self.labelTimer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(labelTimerFired:) userInfo:nil repeats:YES];
    
    // resume game
    [self resumeGame];
}

-(void)didCancelCorrection {
    // hide nav bar again
    self.navigationController.navigationBarHidden = YES;
    
	// start timer again because it turns off (think it's when another form comes on top)
	self.labelTimer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(labelTimerFired:) userInfo:nil repeats:YES];
    
    // resume game
    [self resumeGame];
}

// this is actually skip button!
-(IBAction)dialoguePressed:(id)sender {
	// ok to interrupt?
	if (okToInterrupt) {
		// can't interrupt anymore until listening again
		okToInterrupt = NO;
		
		// if we are actively listening, stop
		if (myRecorder.recordState.state == AUDIORECORDER_STATE_LISTENING) {
            [myRecorder pauseRecording];
		}
		
		// move to next line
		[self doNextAction];
	}
}

/*
+(void)writeBuffer:(float*)inBuffer ofLength:(int)inLength ToWavFile:(NSString *)inFilename withSampleRate:(int)inSampleRate {
    return;
	// get output filenames
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *filename1 = [documentsDirectory stringByAppendingFormat:@"/%@", inFilename];
	NSString *oldFilename1 = [documentsDirectory stringByAppendingFormat:@"/old_%@", inFilename];
	
	// if file exists, save old copy
	if ([[NSFileManager defaultManager] fileExistsAtPath:filename1]) {
		NSError *myError = [NSError alloc];
		[[NSFileManager defaultManager] removeItemAtPath:oldFilename1 error:&myError];
		[[NSFileManager defaultManager] copyItemAtPath:filename1 toPath:oldFilename1 error:&myError];
	}
	
	// set up format
	AudioStreamBasicDescription dataFormat;
	dataFormat.mSampleRate = inSampleRate;
	dataFormat.mFormatID = kAudioFormatLinearPCM;
	dataFormat.mFramesPerPacket = 1;
	dataFormat.mChannelsPerFrame = 1;
	dataFormat.mBytesPerFrame = 4;
	dataFormat.mBytesPerPacket = 4;
	dataFormat.mBitsPerChannel = 32;
	dataFormat.mReserved = 0;
	dataFormat.mFormatFlags = kLinearPCMFormatFlagIsFloat|kLinearPCMFormatFlagIsPacked;
	
	// get c path names
	char *path1 = malloc(1024);
	[filename1 getCString:path1 maxLength:1024 encoding:NSUTF8StringEncoding];
	
	// write first audio file
	AudioFileID fileId1;
	CFURLRef fileURL1 = CFURLCreateFromFileSystemRepresentation(NULL, (UInt8*)path1, strlen(path1), false);
	UInt32 sizeWritten = inLength;
	AudioFileCreateWithURL(fileURL1, kAudioFileWAVEType, &dataFormat, kAudioFileFlags_EraseFile, &fileId1);
	AudioFileWriteBytes(fileId1, false, 0, &sizeWritten, inBuffer);
	AudioFileClose(fileId1);
	
	// free buffers
	free(path1);
}
*/

-(IBAction)statusPressed:(id)sender {
    
}

-(void)labelTimerFired:(NSTimer*)inTimer {
    // game paused overrides all
    if (gamePaused) {
        statusLabel.text = @"Paused";
        return;
    }
    
	// update label
    switch (myRecorder.recordState.state) {
        case AUDIORECORDER_STATE_ANALYZING:
            // don't analyze if the game is paused
            if (!gamePaused) {
                // save number of samples here because this one will get added
                NewTrainingData *trainingData = [userProfile getTrainingDataForAudioDevice];
                int numSamples = trainingData.trainingSamples.count;
                
                // get grade
                statusLabel.text = @"Analyzing...";
                NSDate *startTime = [NSDate date];
                [playGame processAnswerUsingBuffer:myRecorder.recordState.recordBuffer ofSize:myRecorder.recordState.recordBufferPos sampleRate:[myRecorder getSampleRate]];
                NSTimeInterval interval = [startTime timeIntervalSinceNow];
                printf("compare took %f seconds\r\n", interval);
                
                // no longer analyzing
                [myRecorder pauseRecording];
                
                // if we are in calibrate mode, then calibrate
                if (calibrationMode) {
                    // if we already have samples (have trained), make sure it gets
                    // at least 40%
//                    if (numSamples > 0 && grade < 0.0f) {
                    /*
                    if (numSamples > 0 && grade < 0.0f) {
                        // remove this sample from list of training samples
                        NSString *testText = [playGame getCurrentSegmentAsText];
                        testText = [testText stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@".,!?？！。、ょゃュ"]];
                        [[userProfile getTrainingDataForAudioDevice].trainingSamples removeObjectForKey:testText];
                        
                        // do animation to show and fade fail score
                        [self showFailAnimation:grade];
                    }
                    else {
                        // if this is the first time, reset weights
                        if (numSamples == 0)
                            [[userProfile getNetForAudioDevice] resetWeights];
                        
                        // re-calibrate
                        // we're just collecting samples now
//                        [self doNextAction];
                        [self calibrate];
                    }*/
                    // do next action
                    [self doNextAction];
                }
                
                // proceed
                [self doNextAction];
                
                /*
                else {
                    // if we passed
                    if (grade > userProfile.gameOptions.passAccuracy ||
                        calibrationMode) {
                        // register pass
                        [playGame registerRightAnswer];
                        
                        // do animation to show pass score and add to total
                        [self showPassAnimation:grade];
                        
                        // do next action
                        [self doNextAction];
                    }
                    else {
                        // do animation to show and fade fail score
                        [self showFailAnimation:grade];
                    }
                }
                 */
            }
            break;
        case AUDIORECORDER_STATE_RECORDING:
			statusLabel.text = @"Recording...";
            break;
        case AUDIORECORDER_STATE_LISTENING:
            if (myRecorder.isMuted)
                statusLabel.text = @"Waiting for Input...";
            else
                statusLabel.text = @"Listening...";
            break;
        case AUDIORECORDER_STATE_PAUSED:
        default:
            if (speechPlayer.isPlaying)
                statusLabel.text = @"Speaking...";
            else
                statusLabel.text = @"Paused...";
            break;
    }
}

-(void)startCalibration {
    // take a backup of existing network
    backupNet = [[userProfile getNetForAudioDevice] copy];
    
    // clear existing samples
    [[userProfile getTrainingDataForAudioDevice].trainingSamples removeAllObjects];
    
    // turn on calibration, set mode to auto
    calibrationMode = YES;
    userProfile.gameOptions.playPart = 0;
    userProfile.gameOptions.promptMyPart = YES;
    userProfile.gameOptions.pauseAfterActor = NO;
    
    // create new game now we changed the book
//    [self restartGameWithBookNamed:@"Calibration"];
}

-(void)cancelCalibration {
    // cancel calibration
    calibrationMode = NO;
    
    // restore and release network
//    [userProfile replaceNetForAudioDeviceWith:backupNet];
    [backupNet release];
    
    // change book back to user's selected book
//    [self restartGameWithBookNamed:userProfile.gameData.selectedBookName];
    resumeAfterUpdate = NO;
}

-(void)calibrate {
    // show calibration popup dialog
	calibrateController = [[CalibratePopupController alloc] initWithNibName:@"CalibratePopupController" bundle:nil];
    calibrateController.view.frame = self.view.frame;
    calibrateController.popupDelegate = self;
	[self.view addSubview:calibrateController.view];
    
    // save copy of old network in case of abort
    oldNet = [[userProfile getNetForAudioDevice] copy];
    
    // pause game
    [self pauseGame];
    
    // start calibration (this will happen on a seperate thread
    [calibrateController startCalibration];
}

-(void)calibrationFinished:(BOOL)didCancel {
    // if we cancelled
    if (didCancel) {
        // remove this sample from list of training samples
        [[userProfile getTrainingDataForAudioDevice].trainingSamples removeObjectForKey:[playGame getCurrentSegmentForSpeech:NO]];
        
        // and re-instate old net
        [userProfile replaceNetForAudioDeviceWith:oldNet];
    }
    
    // free backed-up network
    [oldNet release];
    
    // close and release controller
    [calibrateController.view removeFromSuperview];
    [calibrateController release];
    
    // resume game
    resumeAfterUpdate = YES;
    if (didCancel)
        [self doCurrentAction];
    else
        [self doNextAction];
}

-(void)showPassAnimation:(float)inScore {
	// add to score
//	[playGame addPercentageToScore:inScore];
	
	// get center of segment text as start position
	CGPoint segmentCenter = [self getCurrentSegmentCenter];
	
	// show pass score and wait for 1/2 second
	lastScoreLabel.alpha = 1.0;
	lastScoreLabel.text = [NSString stringWithFormat:@"%1.0f%%", inScore*100.0f];
	lastScoreLabel.textColor = [UIColor greenColor];
	lastScoreLabel.transform = CGAffineTransformIdentity;
	lastScoreLabel.frame = CGRectMake(segmentCenter.x-(lastScoreFrame.size.width/2), segmentCenter.y-(lastScoreFrame.size.height/2), lastScoreFrame.size.width, lastScoreFrame.size.height);
	lastScoreLabel.hidden = NO;
	self.passFailWaitTimer = [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(passWaitTimerFired:) userInfo:nil repeats:NO];
}

-(void)passWaitTimerFired:(NSTimer*)inTimer {
	// animate moving of last score from current position to scoreboard
	[UIView beginAnimations:@"pass" context:nil];
	[UIView setAnimationDuration:0.5];
	[UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
	[UIView setAnimationDelegate:self];
	[UIView setAnimationDidStopSelector:@selector(passAnimationStopped:finished:context:)];
	lastScoreLabel.transform = CGAffineTransformMakeScale(0.5, 0.5);
	lastScoreLabel.frame = CGRectMake(runningScoreLabel.frame.origin.x, runningScoreLabel.frame.origin.y, lastScoreLabel.frame.size.width, lastScoreLabel.frame.size.height);
//	float scale = runningScoreLabel.frame.size.width/lastScoreLabel.frame.size.width;
//	lastScoreLabel.transform = CGAffineTransformMakeScale(scale, scale);
	[UIView commitAnimations];
}

-(void)passAnimationStopped:(id)animationID finished:(BOOL)finished context:(id)context {
	// hide last score again, update running total
	lastScoreLabel.hidden = YES;
//	runningScoreLabel.text = [NSString stringWithFormat:@"%1.0f%%", [playGame getRunningScorePercentage]*100.0f];
}

-(void)showFailAnimation:(float)inScore {
	// get center of segment text as start position
	CGPoint segmentCenter = [self getCurrentSegmentCenter];
	
	// show pass score and wait for 1/2 second
	lastScoreLabel.alpha = 1.0;
	lastScoreLabel.text = [NSString stringWithFormat:@"%1.0f%%", inScore*100.0f];
	lastScoreLabel.textColor = [UIColor redColor];
	lastScoreLabel.frame = lastScoreFrame;
	lastScoreLabel.transform = CGAffineTransformIdentity;
	lastScoreLabel.frame = CGRectMake(segmentCenter.x-(lastScoreFrame.size.width/2), segmentCenter.y-(lastScoreFrame.size.height/2), lastScoreFrame.size.width, lastScoreFrame.size.height);
	lastScoreLabel.hidden = NO;
	self.passFailWaitTimer = [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(failWaitTimerFired:) userInfo:nil repeats:NO];
	
	// sound failure sound
    [failPlayer play];

    // play the file
}

-(void)failWaitTimerFired:(NSTimer*)inTimer {
	// animate moving of last score from current position to scoreboard
	[UIView beginAnimations:@"pass" context:nil];
	[UIView setAnimationDuration:0.5];
	[UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
	[UIView setAnimationDelegate:self];
	[UIView setAnimationDidStopSelector:@selector(failAnimationStopped:finished:context:)];
	lastScoreLabel.alpha = 0.0f;
	[UIView commitAnimations];
}

-(void)failAnimationStopped:(id)animationID finished:(BOOL)finished context:(id)context {
	// hide last score again
	lastScoreLabel.hidden = YES;
	
    // never retry
    /*
    // register wrong answer
    BOOL retry = [playGame retryAfterWrongAnswer];
    
    // if we are retrying
    if (retry)
        // listen again
        self.pauseTimer = [[NSTimer scheduledTimerWithTimeInterval:0.0 target:self selector:@selector(pauseAfterActorChangeTimerFired:) userInfo:nil repeats:NO] retain];
    else {
        [self doNextAction];
    }*/
    
    // proceed
    [self doNextAction];
}

-(CGPoint)getCurrentSegmentCenter {
	// get bounds of current segment in uiview
	
	// first create uiimage
	UIGraphicsBeginImageContext(dialogueWebView.bounds.size);
	[dialogueWebView.layer renderInContext:UIGraphicsGetCurrentContext()];
	UIImage *webImage = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	
	// next, put the image into a data buffer
//	int width = dialogueWebView.bounds.size.width;
//	int height = dialogueWebView.bounds.size.height;
	int width =  CGImageGetWidth(webImage.CGImage);
	int height =  CGImageGetHeight(webImage.CGImage);
	CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
	unsigned char *rawData = malloc(height * width * 4);
	int bytesPerPixel = 4;
	int bytesPerRow = bytesPerPixel * width;
	int bitsPerComponent = 8;
	CGContextRef context = CGBitmapContextCreate(rawData, width, height, bitsPerComponent, bytesPerRow, colorSpace, kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);
	CGColorSpaceRelease(colorSpace);
	CGContextClearRect(context, CGRectMake(0, 0, width, height));
	CGContextDrawImage(context, CGRectMake(0, 0, width, height), webImage.CGImage);
//	[dialogueWebView.layer renderInContext:context];
	CGContextRelease(context);
	
//	unsigned char* data = CGBitmapContextGetData (context);
	CGPoint topLeft = CGPointMake(9999.0f, 9999.0f);
	CGPoint bottomRight = CGPointMake(0.0f, 0.0f);
	topLeft.x = width;
	topLeft.y = height;
	for (int y=0; y < height; y++) {
		for (int x=0; x < width; x++) {
			int byteIndex = (bytesPerRow * y) + x * bytesPerPixel;
			unsigned char red = rawData[byteIndex];
			unsigned char green = rawData[byteIndex + 1];
			unsigned char blue = rawData[byteIndex + 2];
//			unsigned char alpha = rawData[byteIndex + 3];
			
			if (red == 255 && green == 255 && blue == 255) {
				if (x < topLeft.x)
					topLeft.x = x;
				if (y < topLeft.y)
					topLeft.y = y;
				if (x > bottomRight.x)
					bottomRight.x = x;
				if (y > bottomRight.y)
					bottomRight.y = y;
			}
		}
	}
	
    free(rawData);
	CGPoint screenPos;
	screenPos.x = dialogueWebView.frame.origin.x + topLeft.x + ((bottomRight.x-topLeft.x)/2);
	screenPos.y = dialogueWebView.frame.origin.y + topLeft.y + ((bottomRight.y-topLeft.y)/2);
	return screenPos;
}

-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
	// get url as string
	NSString *myUrl = [[request URL] absoluteString];
	
	// show grammar tip popup
	if ([myUrl hasPrefix:@"file://"]) {
		// get tip name from url
		NSString *tipName = [myUrl stringByReplacingOccurrencesOfString:@"file://" withString:@""];
		tipName = [tipName stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
		
		// pause game
		[self pauseGame];
		
		// show tip
		GrammarTipViewController *myTipController = [[GrammarTipViewController alloc] initWithNibName:@"GrammarTipViewController" bundle:nil];
		self.tipController = myTipController;
        self.tipController.view.frame = self.view.frame;
		[self.view addSubview:myTipController.view];
		[myTipController release];
		
		// load grammar construction from dictionary, either grammar or true dictionary
        // depending on button setting
		NSString *tipText = @"";
        if ([grammarButton.titleLabel.text isEqualToString:@"G"]) {
            GrammarNote *note = [self getGrammarNoteFromTitle:tipName];
            if (note != nil) {
                tipText = [NSString stringWithFormat:@"<p><h1>%@</h1></p><p>%@</p>", note.title, note.text];
            }
            else {
                // grammar not found error
                tipText = [NSString stringWithFormat:@"<p><h1>Grammar tip '%@' not found</h1></p>", tipName];
            }
        }
		else {
            // get application delegate
            McpAppDelegate *appDelegate = (McpAppDelegate*)[[UIApplication sharedApplication] delegate];
            tipText = [appDelegate getDictionaryHtmlForWord:tipName];
        }
        
        // ipad?
        BOOL isIpad = NO;
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 30200
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
            isIpad = YES;
#endif
        
        // smaller font for iphone
        int fontSize = 17;
        int headerFontSize = 24;
        if (!isIpad) {
            fontSize = 10;
            headerFontSize = 14;
        }
        
		// create tip html text
		NSMutableString *htmlString = [NSMutableString stringWithFormat:@"<html><style>body {color:white;text-shadow:1px 1px 0px #000;font-family='helvetica';font-size:%dpx;font-weight:bold;}</style>", fontSize];
		[htmlString appendFormat:@"<html><style>blockquote {color:white;text-shadow:1px 1px 0px #000;font-family='helvetica';font-size:%dpx;font-weight:bold;}</style>", fontSize];
		[htmlString appendFormat:@"<html><style>ul {color:white;text-shadow:1px 1px 0px #000;font-family='helvetica';font-size:%dpx;font-weight:bold;}</style>", fontSize];
		[htmlString appendFormat:@"<style>table {color:white;text-shadow:1px 1px 0px #000;font-family='helvetica';font-size:%dpx;font-weight:bold;}</style>", fontSize];
		[htmlString appendFormat:@"<body>%@</body></html>", tipText];
        
        // convert kanji to users requested format
        NSString *htmlString2 = [HtmlFunctions convertHtml:htmlString toHtmlAs:userProfile.gameOptions.showTextAs];
        
        // assign tip text to web view
		[tipController.tipWebView loadHTMLString:htmlString2 baseURL:nil];
		
		// get notify of mouse up
		[tipController.closeButton addTarget:self action:@selector(closeTipButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
	}
	
	return YES;
}

-(GrammarNote*)getGrammarNoteFromTitle:(NSString*)inTitle {
    // loop through out grammar notes and find title
    return [grammarNotes objectForKey:inTitle];
}

-(void)closeTipButtonPressed:(id)sender {
	// hide controller
	[self.tipController.view removeFromSuperview];
    
    // resume game
    [self resumeGame];
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

-(void)viewDidDisappear:(BOOL)animated {
	// invalidate timer because it holds a reference to this object!
	[self.labelTimer invalidate];
}

-(void)viewWillUnload {
    // allow phone to turn off again
    [[UIApplication sharedApplication] setIdleTimerDisabled:NO];
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillDisappear:(BOOL)animated {
	// invalidate timer here, because it contains a reference to us!
	[labelTimer invalidate];
}

- (void)dealloc {
    // stop listening for route changes, otherwise it's left with old 'conversecontroller'
    AudioSessionRemovePropertyListenerWithUserData(kAudioSessionProperty_AudioRouteChange, routeChangeListener, self);
    
    // release ispeech
    [isRecognition release];
    
	// free buffers and input queue
    [myRecorder release];
	
	// dispose of fail sound
	AudioServicesDisposeSystemSoundID(failSoundId);
	AudioServicesDisposeSystemSoundID(beepSoundId);
    
	// free objc objects
    [grammarNotes release];
    [lipSyncArray release];
    [userProfile release];
    [cpuProfile release];
	[backgroundImageView release];
	[menuButton release];
	[grammarButton release];
    [pauseButton release];
    [sourceButton release];
	[actor1 release];
	[actor2 release];
	[actor3 release];
	[actor4 release];
	[dialogueWebView release];
	[statusLabel release];
	[playGame release];
	[labelTimer release];
	[pauseTimer release];
    [blinkTimer release];
	[passFailWaitTimer release];
	[runningScoreLabel release];
	[lastScoreLabel release];
	[faderView release];
	[showScoreController release];
	[gameMenuController release];
	[tipController release];
    [currentAudioRoute release];
    
    // call super
	[super dealloc];
}


@end
