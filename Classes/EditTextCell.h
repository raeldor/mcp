//
//  NameCell.h
//  Mcp
//
//  Created by Ray on 10/19/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface EditTextCell : UITableViewCell {
	IBOutlet UITextField *myTextField;
}

@property (nonatomic, retain) UITextField *myTextField;

@end
