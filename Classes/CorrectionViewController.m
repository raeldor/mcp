//
//  CorrectionViewController.m
//  Mcp
//
//  Created by Ray Price on 1/15/13.
//
//

#import "CorrectionViewController.h"
#import "SampleEntry.h"
#import "SampleEntryWord.h"
#import "JapaneseDictionary.h"
#import "CorrectionSelection.h"
#import "CorrectionWordActionSheet.h"
#import "SampleSentences.h"

@interface CorrectionViewController ()

@end

@implementation CorrectionViewController

@synthesize delegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil sampleSentence:(SampleEntry*)inSampleSentence
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // save a copy of sentence and original
        originalSentence = [inSampleSentence retain];
        sampleSentence = [inSampleSentence copy];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // show cancel and submit buttons
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(didPressCancel:)];
    self.navigationItem.leftBarButtonItem = cancelButton;
    [cancelButton release];
    UIBarButtonItem *selectButton = [[UIBarButtonItem alloc] initWithTitle:@"Submit" style:UIBarButtonItemStylePlain target:self action:@selector(didPressSubmit:)];
    self.navigationItem.rightBarButtonItem = selectButton;
    [selectButton release];
    
    // no word selected by default
    selectedWordIndex = -1;
    
    // populate html using sample sentence id
    [self updateHtml];
    
    // set title
    self.navigationItem.title = @"Correction";
}


-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
	// get url as string
	NSString *myUrl = [[request URL] absoluteString];
	
	// show grammar tip popup
	if ([myUrl hasPrefix:@"word://"]) {
		// get word index from url
		NSString *indexString = [myUrl stringByReplacingOccurrencesOfString:@"word://" withString:@""];
        int wordIndex = [indexString intValue];
        
        // set selected word index and update html
        selectedWordIndex = wordIndex;
        [self updateHtml];
    }
    else {
        // is this a sense link?
        if ([myUrl hasPrefix:@"sense://"]) {
            // get word index from url
            NSString *indexString = [myUrl stringByReplacingOccurrencesOfString:@"sense://" withString:@""];
            int newSenseIndex = [indexString intValue];
        
            // update selected sense for the current word
            SampleEntryWord *thisWord = [sampleSentence.words objectAtIndex:selectedWordIndex];
            thisWord.senseIndex = newSenseIndex;
            
            // update html to show selection
            [self updateHtml];
        }
        else {
            if ([myUrl hasPrefix:@"kanji://"]) {
                 // show action sheet
                SampleEntryWord *thisWord = [sampleSentence.words objectAtIndex:selectedWordIndex];
                 wordActionSheet = [[CorrectionWordActionSheet alloc] initWithTitle:@"Correct" delegate:self andSampleWord:thisWord];
                 
                 // show in this view
                 [wordActionSheet showInView:self.view];
                 [wordActionSheet setBounds:CGRectMake(0,0,320, 464)];
                 
                 // release
                 [wordActionSheet release];
            }
        }
    }
    
	return YES;
}

-(void)updateHtml {
	// define styles
    int fontSize = 24;
	NSMutableString *htmlString = [NSMutableString stringWithFormat:@"<style>span.first {color:black;font-family='helvetica';font-size:%dpx;font-weight:bold;}</style>", fontSize];
	[htmlString appendFormat:@"<style>span.second {color:black;background-color:yellow;font-family='helvetica';font-size:%dpx;font-weight:bold;}</style>", fontSize];
	[htmlString appendFormat:@"<style>span.third {color:black;font-family='helvetica';font-weight:bold;}</style>"];
	[htmlString appendFormat:@"<style>span.fourth {color:black;font-family='helvetica';font-weight:italic;}</style>"];
	[htmlString appendFormat:@"<style>a {color:inherit}</style>"];
	[htmlString appendString:@"<body><p>"];
    
    //    segmentText = [HtmlFunctions convertHtml:segmentText toHtmlAs:showTextAs];
    
    // open span
    [htmlString appendFormat:@"<span class='%@'>", @"first"];
    
    // get sentence kanji with ruby
    NSString *sentenceKanji = [sampleSentence getKanjiWithRuby:YES includeLinks:YES];
    [htmlString appendString:sentenceKanji];
    
    // close span
    [htmlString appendFormat:@"</span>"];
    
    /*
    // loop through words
    for (int w=0; w < sampleSentence.words.count; w++) {
        // get this word
        SampleEntryWord *thisWord = [sampleSentence.words objectAtIndex:w];
        
        // open span
        htmlString = [htmlString stringByAppendingFormat:@"<span class='%@'>", @"first"];
        
        // create ruby
        NSString *rubyString;
        if ([thisWord.kanji isEqualToString:thisWord.kana])
            rubyString = thisWord.kana;
        else
            rubyString = [NSString stringWithFormat:@"<ruby>%@<rt>%@</rt></ruby>", thisWord.kanji, thisWord.kana];
        
        // add word to html
        if (thisWord.dictionaryId != -1)
            htmlString = [htmlString stringByAppendingFormat:@"<a href=\"file://%d\">%@</a> ", w, rubyString];
        else
            htmlString = [htmlString stringByAppendingFormat:@"%@ ", rubyString];
        
        // close span
        htmlString = [htmlString stringByAppendingFormat:@"</span>"];
    }*/
    
    // show translation in new body
	[htmlString appendFormat:@"</p><p>%@</p>", sampleSentence.meaning];
    
    // if a word is currently selected
    if (selectedWordIndex != -1) {
        // get the word
        SampleEntryWord *thisWord = [sampleSentence.words objectAtIndex:selectedWordIndex];
        
        // get kanji of word from dictionary entry
        JapaneseWord *thisWordEntry = [[JapaneseDictionary getSingleton] getWordFromDictionaryId:thisWord.dictionaryId];
        
        // add dictionary definition to html
        // have meanings selectable (to make correction)
        // LATER, have pronunciations selectable (to make correction)
        [htmlString appendFormat:@"<p><span class='first'><a href=\"kanji://1\">%@</a></span><br><span class='third'>%@</span><br>%@/%@ (%@)<br><br>", thisWordEntry.kanji, thisWordEntry.kana, thisWordEntry.wordType, thisWordEntry.wordSubType, thisWordEntry.customCategory];
        
        // make correction
        CorrectionSelection *correction = [[CorrectionSelection alloc] initWithDictionaryEntry:thisWordEntry];
        
        // add senses
        for (int s=0; s < correction.senses.count; s++) {
            // add to html
            if ((s == thisWord.senseIndex-1) || (thisWord.senseIndex == 0 && s == 0))
                [htmlString appendFormat:@"%d. <span class='second'><a href=\"test\">%@</a></span>; ", s+1, [correction.senses objectAtIndex:s]];
            else
                [htmlString appendFormat:@"%d. <a href=\"sense://%d\">%@</a>; ", s+1, s+1, [correction.senses objectAtIndex:s]];
        }
        
        // release correction
        [correction release];
        
        // close paragraph
        [htmlString appendString:@"</p>"];
    }
    
	// end html body
	[htmlString appendString:@"</body></html>"];
    
    // update web view
	[sentenceWebView loadHTMLString:htmlString baseURL:nil];
}

-(void)didPressCancel:(id)sender {
    // tell delegate
    [delegate didCancelCorrection];
    
    // pop
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)didPressSubmit:(id)sender {
    // correct sample sentence
    [[SampleSentences getSingleton] correctSentenceUsingEntry:sampleSentence originalEntry:originalSentence];
    
    // update sentence in cache, so we don't get it again
    for (int i=0; i < sampleSentence.words.count; i++) {
        // replace word with new word
        [originalSentence.words setObject:[sampleSentence.words objectAtIndex:i] atIndexedSubscript:i];
    }
    
    // tell user thank you!
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Submitted" message:@"Thank you for submitting this correction.  It will be available after the next release." delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
    alert.delegate = self;
    [alert show];
    [alert release];
}

-(void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex {
    // get correct action sheet
    CorrectionWordActionSheet *thisSheet = (CorrectionWordActionSheet*)actionSheet;
    
    // if user selected
    if (buttonIndex == 1) {
        // update the dictionary id and sense
        SampleEntryWord *thisWord = [sampleSentence.words objectAtIndex:selectedWordIndex];
        thisWord.dictionaryId = thisSheet.selectedWord.dictionaryId;
        thisWord.senseIndex = 1;
        
        // refresh the html
        [self updateHtml];
    }
}

-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    // if we cancelled, pop controler
    if ([alertView.title isEqualToString:@"Submitted"]) {
        // tell delegate we made correction
        [delegate didMakeCorrection];
        
        // pop controller
        [self.navigationController popViewControllerAnimated:YES];
    }
}

-(void)dealloc {
    // release retains
    [sampleSentence release];
    [originalSentence release];
    
    // super
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
