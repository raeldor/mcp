//
//  GameData.m
//  Mcp
//
//  Created by Ray on 3/10/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "GameData.h"


@implementation GameData

@synthesize selectedPractice;
@synthesize selectedBookName;

-(id)init {
	if ((self = [super init])) {
		// create empty list
		self.selectedPractice = [NSMutableArray arrayWithCapacity:10];
#ifdef TARGET_FREE      
        self.selectedBookName = @"Genki I Ch.1-3";
#else
        self.selectedBookName = @"Genki I";
#endif
	}
	return self;
}

-(id)initWithCoder:(NSCoder*)coder {
	if ((self = [super init])) {
        self.selectedPractice = [coder decodeObjectForKey:@"SelectedPractice"];
        if (self.selectedPractice == nil)
            self.selectedPractice = [NSMutableArray arrayWithCapacity:10];
        self.selectedBookName = [coder decodeObjectForKey:@"SelectedBookName"];
        if (self.selectedBookName == nil)
#ifdef TARGET_FREE
            self.selectedBookName = @"Genki I Ch.1-3";
#else
            self.selectedBookName = @"Genki I";
#endif
	}
	return self;
}

-(BOOL)containsPracticeKey:(NSString*)inPracticeKey {
    // loop through selected practice
    for (int i=0; i < selectedPractice.count; i++) {
        NSString *myKey = [selectedPractice objectAtIndex:i];
        if ([myKey isEqualToString:inPracticeKey])
            return TRUE;
    }
    return FALSE;
}

-(void)removePracticeForBookUniqueKey:(NSString*)inBookUniqueKey {
    // remove conversations for this name
    int i=0;
    while (i < selectedPractice.count) {
        // find book name
        NSString *myKey = [selectedPractice objectAtIndex:i];
        NSString *bookKey = (NSString*)[[myKey componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString: @"\n"]] objectAtIndex:0];
        
        // remove or move on
        if ([bookKey isEqualToString:inBookUniqueKey])
            [selectedPractice removeObjectAtIndex:i];
        else
            i++;
    }
}

-(void)encodeWithCoder:(NSCoder*)coder {
    [coder encodeObject:selectedPractice forKey:@"SelectedPractice"];
    [coder encodeObject:selectedBookName forKey:@"SelectedBookName"];
}

-(void)dealloc {
	[selectedPractice release];
	[selectedBookName release];
	[super dealloc];
}

@end
