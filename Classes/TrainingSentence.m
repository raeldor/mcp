//
//  TrainingSentence.m
//  Mcp
//
//  Created by Ray on 3/3/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "TrainingSentence.h"

@implementation TrainingSentence

@synthesize sentence;
@synthesize syllableNames;

-(id)initWithDictionary:(NSDictionary*)inDictionary {
	if (self = [super init]) {
		// initialize arrays
		self.sentence = [inDictionary objectForKey:@"Sentence"];
		self.syllableNames = [inDictionary objectForKey:@"SyllableNames"];
	}
	return self;
}

- (void)dealloc {
	[syllableNames release];
	[sentence release];
	[super dealloc];
}

@end
