    //
//  ShowScoreViewController.m
//  Mcp
//
//  Created by Ray on 11/12/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "ShowScoreViewController.h"


@implementation ShowScoreViewController

@synthesize continueButton;
@synthesize scoreLabel;

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
	[continueButton release];
	[scoreLabel release];
    [super dealloc];
}


@end
