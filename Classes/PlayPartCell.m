//
//  PlayPartCell.m
//  Mcp
//
//  Created by Ray on 5/15/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "PlayPartCell.h"


@implementation PlayPartCell

@synthesize playSegment;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if ((self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (void)dealloc {
	[playSegment release];
    [super dealloc];
}


@end
