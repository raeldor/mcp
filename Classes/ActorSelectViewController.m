//
//  ActorSelectViewController.m
//  Mcp
//
//  Created by Ray on 11/1/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//


#import "ActorSelectViewController.h"
#import "Actor.h"
#import "ActorListCell.h"
#import "McpAppDelegate.h"

@implementation ActorSelectViewController

@synthesize actorList;
@synthesize selectDelegate;

#pragma mark -
#pragma mark View lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
	
    /*
	// set title
	self.title = @"Actors";
	
	// create cancel button
	UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(cancelButtonClick:)];
	self.navigationItem.leftBarButtonItem = cancelButton;
	[cancelButton release];
	
	// create select button
	UIBarButtonItem *selectButton = [[UIBarButtonItem alloc] initWithTitle:@"Select" style:UIBarButtonItemStyleBordered target:self action:@selector(selectButtonClick:)];
	self.navigationItem.rightBarButtonItem = selectButton;
	[selectButton release];
	*/
    
	// search for deck archive files
	[self populateActorList];
}

-(void)populateActorList {
	// create new list of actors
	self.actorList = [Actor getListOfFilesWithExtension:@"actor"];
}

#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return actorList.count;
}

/*
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	return 141;
}*/

// Customize the appearance of table view cells.
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	// get application delegate
    McpAppDelegate *appDelegate = (McpAppDelegate*)[[UIApplication sharedApplication] delegate];
    
	// get regular table cell
    static NSString *CellIdentifier = @"ActorListCell";
    ActorListCell *cell = (ActorListCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    /*
    if (cell == nil) {
		// load cell layout from nib
		NSArray *nib = [[NSBundle mainBundle] loadNibNamed:CellIdentifier owner:self options:nil];
		cell = [nib objectAtIndex:0];
    }*/
	
	// Set up the cell...
	NSUInteger row = [indexPath row];
	cell.nameLabel.text = [actorList objectAtIndex:row];
    Actor *actor = [appDelegate getActorUsingName:[actorList objectAtIndex:row]];
    cell.actorImageView.image = [actor.animationFrames objectAtIndex:0];
    
    /*
    int restFrame = [actor getActorFrameForName:@"rest"];
    NSString *filename = [NSString stringWithFormat:@"%@%05d.jpg", actor.animeCharacter, restFrame+1];
    NSString *filePath = [[NSBundle mainBundle] pathForResource:filename ofType:nil];
    cell.actorImageView.image = [UIImage imageWithContentsOfFile:filePath];
	cell.editingAccessoryType = UITableViewCellAccessoryDisclosureIndicator;
	cell.selectionStyle = UITableViewCellSelectionStyleBlue;*/
	
    return cell;
}

-(IBAction)selectButtonClick:(id)sender {
	// hide keyboard and pop controller
	[self.navigationController popViewControllerAnimated:YES];
	
	// call delegate to notify about selection
	[selectDelegate didMakeBrowserSelection:[actorList objectAtIndex:[lastIndexPath row]] fromController:self];
}

-(IBAction)cancelButtonClick:(id)sender {	
	// pop controler
	[self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -
#pragma mark Table view delegate

-(void)tableView:(UITableView*)tableView didSelectRowAtIndexPath:(NSIndexPath*) indexPath {
	// save last index
    [lastIndexPath release];
	lastIndexPath = [indexPath retain];
}

#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    // Relinquish ownership of anything that can be recreated in viewDidLoad or on demand.
    // For example: self.myOutlet = nil;
}


- (void)dealloc {
	[actorList release];
	[lastIndexPath release];
    [super dealloc];
}

@end


