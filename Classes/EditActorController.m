//
//  EditActorController.m
//  Mcp
//
//  Created by Ray on 10/21/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "EditActorController.h"
#import "NameCell.h"
#import "ActorPhotoCell.h"
#import "ActorVoiceCell.h"
#import "ActorPitchCell.h"
#include "AudioToolbox/AudioQueue.h"
#include "AudioToolbox/AudioToolbox.h"
#import <AVFoundation/AVFoundation.h>
#include "vt_jpn_show.h"
#include "vt_jpn_misaki.h"
#import "EditPhotoViewController.h"
#import "ActorAnimeCell.h"
#import "McpAppDelegate.h"

@implementation EditActorController

@synthesize actor;
@synthesize editableListDelegate;
@synthesize popOver;

#pragma mark -
#pragma mark View lifecycle


-(void)setEditableObject:(id)editableObject isNew:(BOOL)isNew {
	// allow selecting during editor
	self.tableView.allowsSelectionDuringEditing = TRUE;
	
	// save new flag
	newMode = isNew;
	
	// save object
	self.actor = editableObject;
	
	// this table is always in edit mode
	// we are not adding delete rows, so we don't need edit mode
	//	[self setEditing:YES animated:NO];
	
	// create cancel button
	UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(cancelButtonClick:)];
	self.navigationItem.leftBarButtonItem = cancelButton;
	[cancelButton release];
	
	// create save button
	UIBarButtonItem *saveButton = [[UIBarButtonItem alloc] initWithTitle:@"Save" style:UIBarButtonItemStyleBordered target:self action:@selector(saveButtonClick:)];
	self.navigationItem.rightBarButtonItem = saveButton;
	[saveButton release];
}

-(void)saveButtonClick:(id)sender {
	// check first for errors
	if ([self isOkToSave])
	{
		// notify parent of change
		if (newMode)
			[editableListDelegate didAddNewObject:actor];
		else
			[editableListDelegate didUpdateObject:actor];
		
		// pop controller
		if (newMode) {
			[self dismissModalViewControllerAnimated:YES];
		}
		else {
			[self.navigationController popViewControllerAnimated:TRUE];
		}
	}
}

-(void)cancelButtonClick:(id)sender {
	// pop controller
	if (newMode) {
		[self dismissModalViewControllerAnimated:YES];
	}
	else {
		[self.navigationController popViewControllerAnimated:TRUE];
	}
}

-(BOOL)isOkToSave {
	BOOL stillOk = YES;
	NSString *msg;
	
	// check fields are filled
    if (actor.actorName == nil || [actor.actorName isEqualToString:@""]) {
        msg = @"Actor name must be completed.";
        stillOk = NO;
    }
	
	// display error if not ok
	if (!stillOk) {
		UIAlertView *myAlert = [[UIAlertView alloc] initWithTitle:@"Oops" message:msg delegate:nil cancelButtonTitle:@"Understood" otherButtonTitles:nil];
		[myAlert show];
		[myAlert release];
	}
	
	return stillOk;
}

#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return 2;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	switch (([indexPath section]*2)+[indexPath row]) {
		case 1: // anime picture
//			return 240;
            return 364;
			break;
		case 2: // voice
			return 120;
			break;
        case 3: // pitch
            return 81;
            break;
		default:
			break;
	}
	return 44;
}

-(NSString*)tableView:(UITableView*)tableView
titleForHeaderInSection:(NSInteger)section {
	switch (section) {
		case 0:
			return @"General";
		case 1:
			return @"Voice";
		default:
			break;
	}
	return @"Unknown";
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	// get application delegate
    McpAppDelegate *appDelegate = (McpAppDelegate*)[[UIApplication sharedApplication] delegate];
    
	// get identifier and get cell
    BOOL cellLoaded = NO;
    NSString *CellIdentifier = @"Cell";
	NSArray *identifiers = [NSArray arrayWithObjects:@"NameCell", @"ActorAnimeCell", @"ActorVoiceCell", @"ActorPitchCell", nil];
	CellIdentifier = [identifiers objectAtIndex:([indexPath section]*2)+[indexPath row]];
	UITableViewCell *cell = (NameCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	if (cell == nil) {
		// load cell layout from nib
		NSArray *nib = [[NSBundle mainBundle] loadNibNamed:CellIdentifier owner:self options:nil];
		cell = [nib objectAtIndex:0];
        cellLoaded = YES;
	}
	cell.accessoryType = UITableViewCellAccessoryNone;
	cell.selectionStyle = UITableViewCellSelectionStyleNone;
	
	// now configure new cell
	NameCell *myNameCell;
	ActorAnimeCell *myAnimeCell;
	ActorVoiceCell *myVoiceCell;
	ActorPitchCell *myPitchCell;
	switch (([indexPath section]*2)+[indexPath row]) {
		case 0: // name
			myNameCell = (NameCell*)cell;
			[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textFieldDidChange:) name:@"UITextFieldTextDidChangeNotification" object:myNameCell.nameTextField];
			myNameCell.nameTextField.text = actor.actorName;
			break;
		case 1: // anime cell
			myAnimeCell = (ActorAnimeCell*)cell;
            if (actor.animeCharacter == nil)
                myAnimeCell.nameLabel.text = [appDelegate.availableAnimeNames objectAtIndex:0];
            else
                myAnimeCell.nameLabel.text = actor.animeCharacter;

            // update picture and name etc.
            [self updateAnimeCellContent:myAnimeCell.contentView];
            
            // setup slider value changed to update picture and name
			[myAnimeCell.animeSlider addTarget:self action:@selector(animeSliderChanged:) forControlEvents:UIControlEventValueChanged];
            
//			[myVoiceCell.actorButton addTarget:self action:@selector(actorButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
//			myPhotoCell.actorImageView.image = actor.image;
//			myPhotoCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
//			myPhotoCell.selectionStyle = UITableViewCellSelectionStyleBlue;
			break;
		case 2: // voice
			myVoiceCell = (ActorVoiceCell*)cell;
			[myVoiceCell loadVoiceList];
			[myVoiceCell setVoiceAs:actor.speechVoice];
			[myVoiceCell.testButton addTarget:self action:@selector(playButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
			[myVoiceCell.voiceNameSegmentControl addTarget:self action:@selector(voiceSegmentChanged:) forControlEvents:UIControlEventValueChanged];
			break;
		case 3: // pitch
			myPitchCell = (ActorPitchCell*)cell;
			myPitchCell.pitchSlider.value = actor.speechPitch;
            myPitchCell.percentLabel.text = [NSString stringWithFormat:@"%d", actor.speechPitch];
			[myPitchCell.pitchSlider addTarget:self action:@selector(pitchChanged:) forControlEvents:UIControlEventValueChanged];
			break;
		default:
			break;
	}
    
    return cell;
}

-(IBAction)animeSliderChanged:(id)sender {
    UISlider *mySlider = (UISlider*)sender;
    
	// get application delegate
    McpAppDelegate *appDelegate = (McpAppDelegate*)[[UIApplication sharedApplication] delegate];
    
    // round slider to number of available anime characters
    int selectedCharacterIndex = round(mySlider.value*(float)(appDelegate.availableAnimeNames.count-1));
    mySlider.value = (float)selectedCharacterIndex/(float)(appDelegate.availableAnimeNames.count-1);
    
    // update name based on slider position
    actor.animeCharacter = [appDelegate.availableAnimeNames objectAtIndex:selectedCharacterIndex];
    
    // get cell and update name, picture and position etc.
    [self updateAnimeCellContent:mySlider.superview];
}

-(void)updateAnimeCellContent:(UIView*)inContentView {
	// get application delegate
    McpAppDelegate *appDelegate = (McpAppDelegate*)[[UIApplication sharedApplication] delegate];
    
    // get cell and update name, picture and slider position etc.
	ActorAnimeCell *myAnimeCell = (ActorAnimeCell*)inContentView.superview;
    
    // update name and image
    myAnimeCell.nameLabel.text = actor.animeCharacter;
    int restFrame = [actor getActorFrameForName:@"rest"];
    NSString *filename = [NSString stringWithFormat:@"%@%05d.jpg", actor.animeCharacter, restFrame+1];
    NSString *filePath = [[NSBundle mainBundle] pathForResource:filename ofType:nil];
    myAnimeCell.animeImage.image = [UIImage imageWithContentsOfFile:filePath];
    
    // find index of name and update slider position
    int selectedCharacterIndex = -1;
    for (int i=0; i < appDelegate.availableAnimeNames.count; i++)
        if ([(NSString*)[appDelegate.availableAnimeNames objectAtIndex:i] isEqualToString:actor.animeCharacter]) {
            selectedCharacterIndex = i;
            break;
        }
    myAnimeCell.animeSlider.value = (float)selectedCharacterIndex/(float)(appDelegate.availableAnimeNames.count-1);
}

-(void)playButtonPressed:(id)sender {
	// get documents directory
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	
	// create sample audio file
	NSString *testText = @"konnichiwa.  yoroshikuonegaishimasu,";
	int speechVoiceIndex;
	if ([actor.speechVoice isEqualToString:@"Misaki"])
		speechVoiceIndex = 2;
	else
		speechVoiceIndex = 1;
	//	const char* cc_testText = [testText cStringUsingEncoding:-2147481087];
	const char* cc_testText = [testText cStringUsingEncoding:NSShiftJISStringEncoding];
	NSString *filename = @"/test000.wav";
	NSString *file_path = [documentsDirectory stringByAppendingString:filename];
	const char* cc_file_path=[file_path UTF8String];
	int pitch = actor.speechPitch;
	int result;
	if (speechVoiceIndex == 1)
		result = VT_TextToFile_JPN_Show(VT_FILE_API_FMT_S16PCM_WAVE, (char*)cc_testText, (char*)cc_file_path, -1, pitch, 100, 100, 0, -1, -1);
	else
		result = VT_TextToFile_JPN_Misaki(VT_FILE_API_FMT_S16PCM_WAVE, (char*)cc_testText, (char*)cc_file_path, -1, pitch, 100, 100, 0, -1, -1);
	if (result != 1) {
		NSException *exception = [NSException exceptionWithName:@"DatabaseError" reason:@"Cannot open speech database" userInfo:nil];
		@throw exception;
	}
	
	// play the file
	NSURL *fileURL = [[NSURL alloc] initFileURLWithPath:file_path];
	AVAudioPlayer *newPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:fileURL error:nil];
	[fileURL release];
	[newPlayer prepareToPlay];
	//	[newPlayer setDelegate:self];
	[newPlayer play];
    [newPlayer autorelease];
}

-(void)voiceSegmentChanged:(id)sender {
	UISegmentedControl *voiceSegment = (UISegmentedControl*)sender;
	actor.speechVoice = [voiceSegment titleForSegmentAtIndex:voiceSegment.selectedSegmentIndex];
}

-(void)pitchChanged:(id)sender {
	UISlider *pitchSlider = (UISlider*)sender;
	actor.speechPitch = pitchSlider.value;
}

-(UITableViewCellEditingStyle)tableView:(UITableView*)tableView editingStyleForRowAtIndexPath:(NSIndexPath*)indexPath {
	return UITableViewCellEditingStyleNone;
}

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */


/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
 
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }   
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }   
 }
 */


/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
 }
 */


/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */


#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	// save last index path for de-highlighting and updating
    [lastIndexPath release];
	lastIndexPath = [indexPath retain];
	
	switch (([indexPath section]*2)+[indexPath row]) {
		case 1: // picture
		{
			// show action sheet
			UIActionSheet *action = [[UIActionSheet alloc] init];
			if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary])
				[action addButtonWithTitle:@"Choose Existing Photo"];
			if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
				[action addButtonWithTitle:@"Take Photo"];
//			if (actor.image != nil)
//				[action addButtonWithTitle:@"Edit Photo"];
//			if (actor.image != nil)
//				[action addButtonWithTitle:@"Delete Photo"];
			[action addButtonWithTitle:@"Cancel"];
			action.cancelButtonIndex = action.numberOfButtons-1;
			action.delegate = self;
			[action showInView:self.view];
			[action release];
		}
			break;
		default:
			break;
	}
}

-(void)textFieldDidChange:(NSNotification*)note {
	actor.actorName = [note.object performSelector:@selector(text)];
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
	// use button title
	NSString *buttonTitle = [actionSheet buttonTitleAtIndex:buttonIndex];
	
	// ipad?
	BOOL isIpad = NO;
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 30200
	if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
		isIpad = YES;
#endif
	
	// if we're using picker
	if ([buttonTitle isEqualToString:@"Choose Existing Photo"] || [buttonTitle isEqualToString:@"Take Photo"]) {
		// use image picker
		UIImagePickerController	*picker = [[UIImagePickerController alloc] init];
		picker.delegate = self;
		picker.sourceType = [buttonTitle isEqualToString:@"Choose Existing Photo"] ? UIImagePickerControllerSourceTypePhotoLibrary:UIImagePickerControllerSourceTypeCamera;
		
		// embed in popover for iPad
		if (isIpad) {
			UIPopoverController *myPopOver = [[NSClassFromString(@"UIPopoverController") alloc] initWithContentViewController:picker];
			[myPopOver presentPopoverFromRect:self.view.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
			self.popOver = myPopOver;
			[myPopOver release];
		}
		else {
			[self presentModalViewController:picker animated:YES];
		}
		// release picker
		[picker release];
	}
    /*
	if ([buttonTitle isEqualToString:@"Delete Photo"]) {
		actor.image = nil;
		[self.tableView reloadData];
	}
	if ([buttonTitle isEqualToString:@"Edit Photo"]) {
		// load edit photo view controller
		EditPhotoViewController *editController = [[EditPhotoViewController alloc] initWithNibName:@"EditPhotoViewController" bundle:nil];
		[editController setPhoto:actor.image];
		[self.navigationController pushViewController:editController animated:YES];
		[editController release];
	}
	if ([buttonTitle isEqualToString:@"Delete Photo"] || [buttonTitle isEqualToString:@"Cancel"]) {
		// deselect any currently selected row
		[self.tableView deselectRowAtIndexPath:lastIndexPath animated:YES];
	}
     */
}

-(void)imagePickerController:(UIImagePickerController*)picker didFinishPickingImage:(UIImage*)image editingInfo:(NSDictionary*)editingInfo {
	// ipad?
	BOOL isIpad = NO;
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 30200
	if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
		isIpad = YES;
#endif
	
	// save image, but resize to 320 x 396
	CGSize newSize = CGSizeMake(320, 396);
	UIGraphicsBeginImageContext(newSize	);
	[image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
	UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
//	actor.image = newImage;
	
	// close picker and refresh view
	if (isIpad) {
		//		[popOver release];
	}
	else {
		[picker dismissModalViewControllerAnimated:YES];
	}
	[self.tableView reloadData];
}

#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    // Relinquish ownership of anything that can be recreated in viewDidLoad or on demand.
    // For example: self.myOutlet = nil;
}


- (void)dealloc {
    [lastIndexPath release];
	[actor release];
	[popOver release];
    [super dealloc];
}


@end

