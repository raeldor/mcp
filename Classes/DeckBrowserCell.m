//
//  DeckBrowserCell.m
//  HF Sensei
//
//  Created by Ray Price on 2/22/13.
//
//

#import "DeckBrowserCell.h"

@implementation DeckBrowserCell

@synthesize deckImage;
@synthesize deckName;
@synthesize readingNotStudiedLabel;
@synthesize readingStudiedLabel;
@synthesize readingStudyingLabel;
@synthesize writingNotStudiedLabel;
@synthesize writingStudiedLabel;
@synthesize writingStudyingLabel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
