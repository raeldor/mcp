//
//  GenderCell.m
//  Mcp
//
//  Created by Ray on 4/10/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "GenderCell.h"

@implementation GenderCell

@synthesize genderSegmentControl;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if ((self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])) {
        // Initialization code
    }
    return self;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

-(void)setGenderAs:(int)inGenderIndex {
    genderSegmentControl.selectedSegmentIndex = inGenderIndex;
}

- (void)dealloc {
	[genderSegmentControl release];
    [super dealloc];
}

@end
