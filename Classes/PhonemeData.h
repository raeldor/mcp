//
//  PhonemeData.h
//  Mcp
//
//  Created by Ray on 1/24/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PhonemeData : NSObject {
	NSMutableArray *phonemeSamples;
}

-(id)init;
-(void)dealloc;

@property (nonatomic, retain) NSMutableArray *phonemeSamples;

@end
