//
//  Book.h
//  Mcp
//
//  Created by Ray on 10/23/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DataFile.h"
#import "Chapter.h"

@interface Book : DataFile <DataFileDelegate> {
    NSString *uniqueKey;
	NSString *bookName;
	UIImage *image;
	NSMutableArray *chapters;
	BOOL allowDelete;
}

-(id)init;
-(id)initFromArchive:(NSString*)inName;
-(void)archiveFieldsUsingArchiver:(NSKeyedArchiver*)inArchiver;
-(void)unarchiveFieldsUsingUnarchiver:(NSKeyedUnarchiver*)inUnarchiver;

@property (nonatomic, retain) NSString *uniqueKey;
@property (nonatomic, retain) NSString* bookName;
@property (nonatomic, retain) UIImage *image;
@property (nonatomic, retain) NSMutableArray *chapters;

@end
