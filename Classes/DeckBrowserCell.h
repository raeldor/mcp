//
//  DeckBrowserCell.h
//  HF Sensei
//
//  Created by Ray Price on 2/22/13.
//
//

#import <UIKit/UIKit.h>

@interface DeckBrowserCell : UITableViewCell {
}

@property (nonatomic, assign) IBOutlet UIImageView *deckImage;
@property (nonatomic, assign) IBOutlet UILabel *deckName;
@property (nonatomic, assign) IBOutlet UILabel *readingStudiedLabel;
@property (nonatomic, assign) IBOutlet UILabel *readingStudyingLabel;
@property (nonatomic, assign) IBOutlet UILabel *readingNotStudiedLabel;
@property (nonatomic, assign) IBOutlet UILabel *writingStudiedLabel;
@property (nonatomic, assign) IBOutlet UILabel *writingStudyingLabel;
@property (nonatomic, assign) IBOutlet UILabel *writingNotStudiedLabel;

@end
