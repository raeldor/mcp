//
//  Character.h
//  Mcp
//
//  Created by Ray on 10/23/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface Character : NSObject <NSCoding, NSCopying> {
	NSString *characterName;
	NSString *actorName;
}

@property (nonatomic, retain) NSString *characterName;
@property (nonatomic, retain) NSString *actorName;

@end
