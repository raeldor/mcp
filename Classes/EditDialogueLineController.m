//
//  EditDialogueLineController.m
//  Mcp
//
//  Created by Ray on 1/18/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "EditDialogueLineController.h"
#import "EditHtmlCell.h"
#import "McpAppDelegate.h"
#import "GrammarPickerViewController.h"
#import "EditDialogueLineCell.h"

@implementation EditDialogueLineController

@synthesize thisLine;
@synthesize editDialogueLineDelegate;

#pragma mark -
#pragma mark Initialization

/*
- (id)initWithStyle:(UITableViewStyle)style {
    // Override initWithStyle: if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization.
    }
    return self;
}
*/


#pragma mark -
#pragma mark View lifecycle

-(void)setEditableObject:(DialogueLine*)inLine isNew:(BOOL)isNew {
	// allow selecting during editor
	self.tableView.allowsSelectionDuringEditing = TRUE;
	
	// save new flag
	newMode = isNew;
	
	// make a copy of this object
	thisLine = [inLine copy];
	
	// set title
	self.title = @"Edit Line";
	
	// this table is always in edit mode
	// we are not adding delete rows, so we don't need edit mode
	//	[self setEditing:YES animated:NO];
	
	// create cancel button
	UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(cancelButtonClick:)];
	self.navigationItem.leftBarButtonItem = cancelButton;
	[cancelButton release];
	
	// create save button
	UIBarButtonItem *saveButton = [[UIBarButtonItem alloc] initWithTitle:@"Save" style:UIBarButtonItemStyleBordered target:self action:@selector(saveButtonClick:)];
	self.navigationItem.rightBarButtonItem = saveButton;
	[saveButton release];
        
	// change edit menu to add link grammar
    UIMenuItem *createMenuItem = [[UIMenuItem alloc] initWithTitle:@"Create Link" action:@selector(linkGrammar:)];
	NSArray *newItems = [NSArray arrayWithObject:createMenuItem];
    [createMenuItem release];
	[UIMenuController sharedMenuController].menuItems = newItems;
}

-(void)link {
    [webViewBeingEdited stringByEvaluatingJavaScriptFromString:@"document.execCommand(\"CreateLink\", false, \"http://www.gartner.com\")"];
}

-(void)unlink {
    [webViewBeingEdited stringByEvaluatingJavaScriptFromString:@"document.execCommand(\"Unlink\")"];
}

-(BOOL)isOkToSave {
	BOOL stillOk = YES;
	NSString *msg;
	
	// check fields are filled
    if (thisLine.line == nil || [thisLine.line isEqualToString:@""]) {
        msg = @"Dialogue line text must be completed.";
        stillOk = NO;
    }
	
	// display error if not ok
	if (!stillOk) {
		UIAlertView *myAlert = [[UIAlertView alloc] initWithTitle:@"Oops" message:msg delegate:nil cancelButtonTitle:@"Understood" otherButtonTitles:nil];
		[myAlert show];
		[myAlert release];
	}
	
	return stillOk;
}

-(void)saveButtonClick:(id)sender {
	// check first for errors
	if ([self isOkToSave])
	{
		// notify parent of change
		if (newMode)
			[editDialogueLineDelegate didAddDialogueLine:thisLine];
		else
			[editDialogueLineDelegate didUpdateDialogueLine:thisLine];
		
		// pop controller
		if (newMode) {
			[self dismissModalViewControllerAnimated:YES];
		}
		else {
			[self.navigationController popViewControllerAnimated:TRUE];
		}
	}
}

-(void)cancelButtonClick:(id)sender {
	// pop controller
	if (newMode) {
		[self dismissModalViewControllerAnimated:YES];
	}
	else {
		[self.navigationController popViewControllerAnimated:TRUE];
	}
}

#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return 1;
}



-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	switch ([indexPath row]) {
		case 0: // edit line cell
            return 100;
//			return self.view.frame.size.height/2;
			break;
		default:
			break;
	}
	return 44;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *CellIdentifier = @"Cell";
	
	// get identifier and get cell
	NSArray *identifiers = [NSArray arrayWithObjects:@"EditDialogueLineCell", nil];
	CellIdentifier = [identifiers objectAtIndex:[indexPath row]];
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	if (cell == nil) {
		// load cell layout from nib
		NSArray *nib = [[NSBundle mainBundle] loadNibNamed:CellIdentifier owner:self options:nil];
		cell = [nib objectAtIndex:0];
	}
	cell.accessoryType = UITableViewCellAccessoryNone;
	cell.selectionStyle = UITableViewCellSelectionStyleNone;
	
	// now configure new cell
	EditDialogueLineCell *myEditCell;
    NSString *htmlString;
	switch ([indexPath row]) {
		case 0: // edit/preview text
            htmlString = [NSString stringWithFormat:@"<html><body><div contenteditable=\"true\">%@</div></body></html>", thisLine.line];
			myEditCell = (EditDialogueLineCell*)cell;
            [myEditCell.webView loadHTMLString:htmlString baseURL:nil];
            webViewBeingEdited = myEditCell.webView;
			break;
		default:
			break;
	}
    
    return cell;
}

-(void)textViewDidChange:(NSNotification*)note {
	// update line
	UITextView *thisTextView = (UITextView*)note.object;
	thisLine.line = [NSString stringWithString:thisTextView.text];
}

-(void)textViewDidBeginEditing:(NSNotification*)note {
	UITextView *thisTextView = (UITextView*)note.object;
	textViewBeingEdited = thisTextView;
}

-(void)textViewDidEndEditing:(NSNotification*)note {
	textViewBeingEdited = nil;
}

-(BOOL)canPerformAction:(SEL)action withSender:(id)sender {
    if (action == @selector(linkGrammar:)) {
		if (webViewBeingEdited != nil)
            return YES;
		else
			return NO;
    }
	
    return [super canPerformAction:action withSender:sender];
}

-(void)linkGrammar:(UIMenuController*)sender {
    // use action sheet so webview keeps focus
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil 
                                                             delegate:nil
                                                    cancelButtonTitle:nil
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:nil];
    [actionSheet setActionSheetStyle:UIActionSheetStyleBlackTranslucent];

    // our grammar picker
    GrammarPickerViewController *myPicker = [[GrammarPickerViewController alloc] initWithNibName:@"GrammarPickerViewController" bundle:nil];
    [myPicker setActionSheet:actionSheet];
    
    // add our view and show action sheet, then set bounds
    [actionSheet addSubview:myPicker.view];
    [actionSheet showInView:self.view];
    [actionSheet setBounds:CGRectMake(0, 0, 320, 485)];
    
    // release picker
//    [myPicker release];
    [actionSheet release];
}

-(void)didMakeSelection:(int)inSelection {
	// find name of grammar note
    NSString *noteTitle = @"";
	
    // link the grammar
	NSString *href = [noteTitle stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *command = [NSString stringWithFormat:@"document.execCommand(\"CreateLink\", false, \"file://%@\")", href];
    [webViewBeingEdited stringByEvaluatingJavaScriptFromString:command];
	
	// update to data object too
	thisLine.line = [NSString stringWithString:linkTextView.text];
}

-(void)didCancelSelection {
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/


/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source.
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
    }   
}
*/


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/


/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // Navigation logic may go here. Create and push another view controller.
    /*
    <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
    [self.navigationController pushViewController:detailViewController animated:YES];
    [detailViewController release];
    */
}


#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    // Relinquish ownership of anything that can be recreated in viewDidLoad or on demand.
    // For example: self.myOutlet = nil;
}


- (void)dealloc {
	[thisLine release];
    [super dealloc];
}


@end

