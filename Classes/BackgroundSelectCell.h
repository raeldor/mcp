//
//  BackgroundSelectCell.h
//  Mcp
//
//  Created by Ray on 10/30/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface BackgroundSelectCell : UITableViewCell {
	IBOutlet UIImageView *backgroundImageView;
	IBOutlet UILabel *nameLabel;
}

@property (nonatomic, retain) UIImageView *backgroundImageView;
@property (nonatomic, retain) UILabel *nameLabel;

@end
