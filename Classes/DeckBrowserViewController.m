//
//  DeckBrowserViewController.m
//  HF Sensei
//
//  Created by Ray Price on 2/22/13.
//
//

#import "DeckBrowserViewController.h"
#import "DeckBrowserCell.h"
#import "FlashDeck.h"
#import "SectionBrowserViewController.h"
#import "EditDeckViewController.h"
#import "DataFile2.h"

@interface DeckBrowserViewController ()

@end

@implementation DeckBrowserViewController

@synthesize sectionBrowserDelegate;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // clear table selection again
    self.clearsSelectionOnViewWillAppear = YES;

	// add edit button
	self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    // populate deck list
    // done by view will appear
//    [self refreshDeckList];
}

-(void)viewWillAppear:(BOOL)animated {
    // call super
    [super viewWillAppear:animated];
    
    // if appearing again, it may have changed in the meantime, so refresh list
    [self refreshDeckList];
}

-(void)refreshDeckList {
    // refresh list
    cloudQuery = [[DataFile2 getListOfArchivesWithExtension:@"flashDeck" completion:^(NSArray *archiveList) {
        // release existing list
        [deckList release];

        // populate deck list
        deckList = [[NSMutableArray arrayWithArray:archiveList] retain];
        
        // refresh table
        [self.tableView reloadData];
        
        // release query
        [cloudQuery release];
        
        /*
        // only show decks that have kanji sections
        deckList = [[NSMutableArray alloc] initWithCapacity:10];
        for (int i=0; i < archiveList.count; i++) {
            NSString *deckName = [archiveList objectAtIndex:i];
            FlashDeck *thisDeck = [[FlashDeck alloc] initWithName:deckName];
            NSLog(@"Open deck %@ to check for section types", deckName);
            [thisDeck openWithCompletionHandler:^(BOOL success) {
                // check sections
                BOOL hasRequiredSections = NO;
                for (int s=0; s < thisDeck.deckSections.count; s++) {
                    DeckSection *thisSection = [thisDeck.deckSections objectAtIndex:s];
                    if ([thisSection.sectionType isEqualToString:@"Kanji"]) {
                        hasRequiredSections = YES;
                        break;
                    }
                }
                
                // if we have required sections, add deck
                if (hasRequiredSections) {
                    NSLog(@"Added deck %@ to deck list", deckName);
                    // add and refresh table
                    [deckList addObject:deckName];
                    [self.tableView reloadData];
                }
            }];
            
            // release deck
            [thisDeck release];
        }*/
    }] retain];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if (self.tableView.editing)
        return deckList.count+1;
    else
        return (deckList.count==0?1:deckList.count);
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([(NSObject*)sectionBrowserDelegate respondsToSelector:@selector(didUpdateReadingStudySwitchForSectionNamed:inDeckNamed:toValue:)])
        return 122;
    else
        return 92;
    /*
    if (indexPath.row < deckList.count)
        return 92;
    else
        return 0;*/
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // add new row, or deck row
    if (indexPath.row >= deckList.count) {
        UITableViewCell *thisCell;
        if (deckList.count == 0 && !self.tableView.isEditing)
            thisCell = [tableView dequeueReusableCellWithIdentifier:@"PressAddCell"];
// not available on ios5
//        thisCell = [tableView dequeueReusableCellWithIdentifier:@"PressAddCell" forIndexPath:indexPath];
        else
            thisCell = [tableView dequeueReusableCellWithIdentifier:@"AddCell"];
// not available on ios5
//            thisCell = [tableView dequeueReusableCellWithIdentifier:@"AddCell" forIndexPath:indexPath];
        return thisCell;
    }
    else {
        // Configure the cell...
        NSString *deckName = [deckList objectAtIndex:indexPath.row];
        DeckBrowserCell *thisCell;
        if ([(NSObject*)sectionBrowserDelegate respondsToSelector:@selector(didUpdateReadingStudySwitchForSectionNamed:inDeckNamed:toValue:)])
            thisCell = [tableView dequeueReusableCellWithIdentifier:@"RenshuuDeckCell"]       ;
        else
            thisCell = [tableView dequeueReusableCellWithIdentifier:@"DeckBrowserCell"];

        // not on ios5
//        DeckBrowserCell *thisCell = [tableView dequeueReusableCellWithIdentifier:@"DeckBrowserCell" forIndexPath:indexPath];
        thisCell.deckName.text = deckName;
        
        // load the deck
        FlashDeck *deck = [[FlashDeck alloc] initWithName:[deckList objectAtIndex:indexPath.row]];
        [deck openWithCompletionHandler:^(BOOL success) {
            // set image and close and release
            thisCell.deckImage.image = deck.image;
            
            // calculate and show sections marked as studied etc.
            if ([(NSObject*)sectionBrowserDelegate respondsToSelector:@selector(didUpdateReadingStudySwitchForSectionNamed:inDeckNamed:toValue:)]) {
                int kanjiTypeCount = 0;
                int readingStudiedCount = 0;
                int readingStudyingCount = 0;
                int readingNotStudiedCount = 0;
                int writingStudiedCount = 0;
                int writingStudyingCount = 0;
                int writingNotStudiedCount = 0;
                for (int i=0; i < deck.deckSections.count; i++) {
                    // marked?
                    DeckSection *thisSection = [deck.deckSections objectAtIndex:i];
                    if ([thisSection.sectionType isEqualToString:@"Kanji"])
                        kanjiTypeCount++;
                    int readSetting = [sectionBrowserDelegate getReadingStudySwitchValueForSectionNamed:thisSection.sectionName inDeckNamed:deck.deckName];
                    int writeSetting = [sectionBrowserDelegate getWritingStudySwitchValueForSectionNamed:thisSection.sectionName inDeckNamed:deck.deckName];
                    if (readSetting == 0)
                        readingStudiedCount++;
                    else {
                        if (readSetting == 1)
                            readingStudyingCount++;
                        else
                            readingNotStudiedCount++;
                    }
                    if (writeSetting == 0)
                        writingStudiedCount++;
                    else {
                        if (writeSetting == 1)
                            writingStudyingCount++;
                        else
                            writingNotStudiedCount++;
                    }
                }
                
                // populate appropriate label
                thisCell.readingStudyingLabel.text = [NSString stringWithFormat:@"%3.0f%%", ((float)readingStudyingCount/(float)deck.deckSections.count)*100.0f];
                thisCell.readingStudiedLabel.text = [NSString stringWithFormat:@"%3.0f%%", ((float)readingStudiedCount/(float)deck.deckSections.count)*100.0f];
                thisCell.readingNotStudiedLabel.text = [NSString stringWithFormat:@"%3.0f%%", ((float)readingNotStudiedCount/(float)deck.deckSections.count)*100.0f];
                thisCell.writingStudyingLabel.text = [NSString stringWithFormat:@"%3.0f%%", ((float)writingStudyingCount/(float)deck.deckSections.count)*100.0f];
                thisCell.writingStudiedLabel.text = [NSString stringWithFormat:@"%3.0f%%", ((float)writingStudiedCount/(float)deck.deckSections.count)*100.0f];
                thisCell.writingNotStudiedLabel.text = [NSString stringWithFormat:@"%3.0f%%", ((float)writingNotStudiedCount/(float)deck.deckSections.count)*100.0f];
            }
            
            // have to close this, otherwise it doesn't get released
            [deck closeWithCompletionHandler:^(BOOL success) {
            }];
        }];
        [deck release];
        
        return thisCell;
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // if editing a deck, pass deck reference
    if ([segue.identifier isEqualToString:@"EditDeckSegue"]) {
        EditDeckViewController *editController = (EditDeckViewController *)segue.destinationViewController;
        [editController setDeckTo:sender];
        editController.delegate = self;
    }
    // if browsing sections
    if ([segue.identifier isEqualToString:@"SectionBrowserSegue"]) {
        SectionBrowserViewController *browseController = (SectionBrowserViewController *)segue.destinationViewController;
        browseController.sectionBrowserDelegate = self.sectionBrowserDelegate;
        [browseController setDeckTo:sender];
    }
}

-(void)tableView:(UITableView*)tableView didSelectRowAtIndexPath:(NSIndexPath*) indexPath {
	// find row
	NSUInteger row = [indexPath row];
	
	// editing mode?
	if (self.editing) {
		// add new?
		if (row >= [deckList count]) {
			[self tableView:tableView commitEditingStyle:UITableViewCellEditingStyleInsert forRowAtIndexPath:indexPath];
		}
		else {
            // don't allow user to select again yet
            self.tableView.allowsSelection = NO;
            
            // perform segue to show deck editor browser
			FlashDeck *thisDeck = [[FlashDeck alloc] initWithName:[deckList objectAtIndex:row]];
            [thisDeck openWithCompletionHandler:^(BOOL success) {
                // edit this deck once open
                [self performSegueWithIdentifier:@"EditDeckSegue" sender:thisDeck];
                [thisDeck release];
                
                // allow selection again
                self.tableView.allowsSelection = YES;
            }];
		}
	}
	else {
		if (deckList.count > 0)
		{
            // don't allow user to select again yet
            self.tableView.allowsSelection = NO;
            
            // perform segue to show section browser
			FlashDeck *thisDeck = [[FlashDeck alloc] initWithName:[deckList objectAtIndex:row]];
            [thisDeck openWithCompletionHandler:^(BOOL success) {
                // browse sections, once open
                [self performSegueWithIdentifier:@"SectionBrowserSegue" sender:thisDeck];
                
                // just browsing, so don't keep open
                [thisDeck closeWithCompletionHandler:^(BOOL success) {
                }];
                
                // allow selection again
                self.tableView.allowsSelection = YES;
            }];
            [thisDeck release];
		}
	}
}

- (void)setEditing:(BOOL)editing animated:(BOOL)animated {
	// add/remove placeholder items
	NSMutableArray *indexPaths = [NSMutableArray array];
	[indexPaths addObject:[NSIndexPath indexPathForRow:deckList.count inSection:0]];
	[self.tableView beginUpdates];
	[super setEditing:editing animated:animated];
	[self.tableView setEditing:editing animated:YES];
	if (editing) {
		// delete 'press edit' message
		if (deckList.count == 0)
			[self.tableView deleteRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationTop];
		
		// Show the placeholder rows and hide back button
		[self.tableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationTop];
		self.navigationItem.hidesBackButton = YES;
	}
	else
	{
		// Hide the placeholder rows and show back button again
		[self.tableView deleteRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationTop];
		self.navigationItem.hidesBackButton = NO;
		
		// show 'press edit' message
		if (deckList.count == 0)
			[self.tableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationTop];
	}
	[self.tableView endUpdates];
}

-(UITableViewCellEditingStyle)tableView:(UITableView*)tableView editingStyleForRowAtIndexPath:(NSIndexPath*)indexPath {
	// if we are in edit mode
	if (self.editing) {
		// show delete style and insert for 'add new'
		if (indexPath.row != deckList.count)
			return UITableViewCellEditingStyleDelete;
		else
			return UITableViewCellEditingStyleInsert;
	}
	return UITableViewCellEditingStyleNone;
}

// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    // if we are deleting rows
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
		[FlashDeck deleteDeck:[deckList objectAtIndex:[indexPath row]]];
		[deckList removeObjectAtIndex:[indexPath row]];
        [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:YES];
    }
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // perform segue to edit deck, passing nil as deck
        [self performSegueWithIdentifier:@"EditDeckSegue" sender:nil];
	}
}

-(void)didUpdateObject:(id)updatedObject key:oldKey {
    // refresh list
    // remove old name
    for (int i=0; i < deckList.count; i++) {
        NSString *deckName = [deckList objectAtIndex:i];
        if ([deckName isEqualToString:oldKey]) {
            [deckList removeObjectAtIndex:i];
        }
    }
    // manually update, because there seems to be some delay!
    FlashDeck *newDeck = updatedObject;
    [deckList addObject:newDeck.deckName];
    [self.tableView reloadData];
//    [self refreshDeckList];
}

-(void)didAddNewObject:(id)newObject {
    // refresh list
    // insert new object
    FlashDeck *newDeck = newObject;
    [deckList addObject:newDeck.deckName];
    [self.tableView reloadData];
//    [self refreshDeckList];
}

-(void)dealloc {
    // release retains
    [deckList release];
    
    // call super
    [super dealloc];
}

#pragma mark - Table view delegate


@end
