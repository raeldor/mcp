/*
 *  EditDialogueLineDelegate.h
 *  Mcp
 *
 *  Created by Ray on 1/18/11.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */

#import "DialogueLine.h"

@protocol EditDialogueLineDelegate

-(void)didUpdateDialogueLine:(DialogueLine*)inLine;
-(void)didAddDialogueLine:(DialogueLine*)inLine;

@end