//
//  NotesCell.h
//  Mcp
//
//  Created by Ray on 10/29/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NotesCell : UITableViewCell {
	IBOutlet UILabel *notesLabel;
	IBOutlet UITextView *notesTextView;
}

@property (nonatomic, retain) UILabel *notesLabel;
@property (nonatomic, retain) UITextView *notesTextView;

@end
