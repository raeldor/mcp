//
//  EditHtmlViewController.h
//  Mcp
//
//  Created by Ray on 1/18/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface EditHtmlViewController : UIViewController {
	IBOutlet UISegmentedControl *editPreviewControl;
	IBOutlet UITextView *editTextView;
	IBOutlet UIWebView *previewWebView;
}

@property (nonatomic, retain) UISegmentedControl *editPreviewControl;
@property (nonatomic, retain) UITextView *editTextView;
@property (nonatomic, retain) UIWebView *previewWebView;

-(void)editPreviewChanged:(id)sender;

@end
