//
//  GameMenuViewController.m
//  Mcp
//
//  Created by Ray on 12/22/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "GameMenuViewController.h"


@implementation GameMenuViewController

@synthesize selectConversationsButton;
@synthesize optionsButton;
@synthesize resumeButton;
@synthesize changeBookButton;
@synthesize quitButton;
@synthesize calibrateButton;

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
	[selectConversationsButton release];
	[optionsButton release];
    [changeBookButton release];
	[resumeButton release];
	[quitButton release];
	[calibrateButton release];
    [super dealloc];
}


@end
