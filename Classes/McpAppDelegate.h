//
//  McpAppDelegate.h
//  Mcp
//
//  Created by Ray on 10/14/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Accelerate/Accelerate.h"
#import "TrainingData.h"
#import "UserProfile.h"
#import "McpLocalOptions.h"
#import "McpAppOptions.h"
#import "mecab.h"
#import "DataFileCache.h"
#import "Actor.h"
#import "Background.h"
#import "RomajiPath.h"
#import "sqlite3.h"
#import "FlashGameAppDelegateProtocol.h"
#import "McpAppOptions.h"
#import "McpLocalOptions.h"

@interface McpAppDelegate : NSObject <UIApplicationDelegate, FlashGameAppDelegateProtocol> {
    McpLocalOptions *localOptions;
    McpAppOptions *appOptions;
    UIWindow *window;
    UINavigationController *navigationController;
	TrainingData *trainingData;
	UserProfile *currentUserProfile;
	UserProfile *currentCpuProfile;
    DataFileCache *fileCache;
//	Mecab *mecab;
    RomajiPath *romajiPath;
	sqlite3 *jmwDb;
    NSArray *availableAnimeNames;
    
    NSMutableDictionary *phonemes;
    NSMutableDictionary *trainingWords;
    
    float *melMatrix8k;
    float *melMatrix16k;
    float *melWindow8k;
    float *melWindow16k;
    FFTSetup setupReal;
}

+(NSString *)getUuid;
-(Actor*)getActorUsingName:(NSString*)inName;
-(Background*)getBackgroundUsingName:(NSString*)inName;
-(NSString*)getDictionaryHtmlForWord:(NSString*)inWord;
-(float*)getMelMatrixForSampleRate:(int)inSampleRate;
-(float)freq2mel:(float)inFreq;
-(float)mel2freq:(float)inFreq;
-(float*)buildFftMatrixWithSamplingRate:(int)fs windowLength:(int)N channels:(int)nofChannels;
-(float*)buildFftWindowSizeArrayWithSamplingRate:(int)fs windowLength:(int)N channels:(int)nofChannels;
-(float*)getMelWindowForSampleRate:(int)inSampleRate;

-(FFTSetup)getFftSetup;
-(void)copyBundledDocuments;
-(void)copyBundledDocumentsOfType:(NSString*)inType;
-(void)copyBundledDocumentOfName:(NSString*)inName;
//-(int)getActorFrameForName:(NSString*)inName;
//-(NSArray*)getSyncListForPhoneme:(NSString*)inPhoneme; // moved to actor
-(void)localOptionsLoaded:(NSNotification *) notification;
-(void)appOptionsLoaded:(NSNotification *) notification;
-(void)oldAppOptionsLoaded:(NSNotification *) notification;
-(BOOL)isBundledDocumentWithName:(NSString*)inName andType:(NSString*)inType;
-(void)mergeOldOptions;

@property (strong, nonatomic) UIWindow *window;

@property (nonatomic, retain) McpLocalOptions *localOptions;
@property (nonatomic, retain) McpAppOptions *appOptions;
@property (nonatomic, retain) McpAppOptions *oldAppOptions;
//@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet UINavigationController *navigationController;
@property (nonatomic, retain) TrainingData *trainingData;
@property (nonatomic, retain) UserProfile *currentUserProfile;
@property (nonatomic, retain) UserProfile *currentCpuProfile;
//@property (nonatomic, retain) NSMutableArray *mecabExceptions;
//@property (nonatomic, retain) Mecab *mecab;
@property (nonatomic, retain) RomajiPath *romajiPath;
@property (nonatomic, retain) NSArray *availableAnimeNames;
@property (nonatomic, retain) NSMutableDictionary *phonemes;
@property (nonatomic, retain) NSMutableDictionary *trainingWords;
@property (nonatomic, assign) BOOL isOptionsLoaded;
@property (nonatomic, assign) BOOL isOldOptionsLoaded;
@property (nonatomic, assign) BOOL isLocalOptionsLoaded;

@end

