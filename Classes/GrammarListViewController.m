//
//  GrammarListViewController.m
//  Mcp
//
//  Created by Ray on 1/7/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "GrammarListViewController.h"
#import "EditGrammarController.h"
#import "GrammarNote.h"
#import "McpAppDelegate.h"
#import "GrammarViewController.h"
#import "TextFunctions.h"

@implementation GrammarListViewController

/*
@synthesize browseTableView;
@synthesize searchBar;
@synthesize searchController;
@synthesize keyboardHideButton;
@synthesize searchToolbar;
@synthesize romajiSwitch;
@synthesize ftsSwitch;
 */
@synthesize book;
@synthesize chapter;

#pragma mark -
#pragma mark View lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
	
	// set title
	self.title = @"Grammar";
	    
	// allow selecting during editor
	self.tableView.allowsSelection = YES;
	self.tableView.allowsSelectionDuringEditing = YES;
	
	// add edit button
	self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

-(void)setEditableObject:(Chapter *)inChapter inBook:(Book*)inBook {
	self.chapter = inChapter;
	self.book = inBook;
}

-(void)keyboardHideButtonClicked:(id)sender {
	// resign responder for keyboard
//	[searchBar resignFirstResponder];
}

#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // if not search tableview, return all grammar
    if (tableView == self.tableView) {
        // how many grammar notes?
        int noteCount = chapter.grammarNotes.count;
        if (self.editing)
            return noteCount+1;
        else
        {
            if (noteCount == 0)
                return 1;
            else
                return noteCount;
        }
    }
    
    // otherwise, return search results
    return searchResults.count;
}

// Customize the appearance of table view cells.
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	// get regular table cell
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
		cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
	
	// default cell config
	cell.textLabel.text = NULL;
	cell.imageView.image = NULL;
	cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
	cell.editingAccessoryType = UITableViewCellAccessoryDisclosureIndicator;
	cell.selectionStyle = UITableViewCellSelectionStyleNone;
	cell.textLabel.textColor = [UIColor blackColor];
    
	// if no cells, display 'press to add' message
	if (chapter.grammarNotes.count == 0 && !self.editing) {
		cell.textLabel.text = @"Press Edit Button To Create New Grammar Point";
		cell.textLabel.textColor = [UIColor lightGrayColor];
		cell.textLabel.adjustsFontSizeToFitWidth = YES;
	}
	else {
		// Set up the cell...
		NSUInteger row = [indexPath row];
		if (row < chapter.grammarNotes.count) {
            if (tableView == self.tableView)
                cell.textLabel.text = [[chapter.grammarNotes objectAtIndex:row] title];
            else
                cell.textLabel.text = [[searchResults objectAtIndex:row] title];
//			cell.editingAccessoryType = UITableViewCellAccessoryDisclosureIndicator;
			cell.selectionStyle = UITableViewCellSelectionStyleBlue;
		}
		else {
			cell.textLabel.text = @"Add New";
			cell.textLabel.textColor = [UIColor lightGrayColor];
			cell.selectionStyle = UITableViewCellSelectionStyleBlue;
		}
	}
	
    return cell;
}

//editing mode changed
- (void)setEditing:(BOOL)editing animated:(BOOL)animated {
	// add/remove placeholder items
	NSMutableArray *indexPaths = [NSMutableArray array];
	[indexPaths addObject:[NSIndexPath indexPathForRow:chapter.grammarNotes.count inSection:0]];
	[self.tableView beginUpdates];
	[super setEditing:editing animated:animated];
	[self.tableView setEditing:editing animated:YES];
	if (editing) {
		// delete 'press edit' message
		if (chapter.grammarNotes.count == 0)
			[self.tableView deleteRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationTop];
		
		// Show the placeholder rows and hide back button
		[self.tableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationTop];
		self.navigationItem.hidesBackButton = YES;
	}
	else
	{
		// Hide the placeholder rows and show back button again
		[self.tableView deleteRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationTop];
		self.navigationItem.hidesBackButton = NO;
		
		// show 'press edit' message
		if (chapter.grammarNotes.count == 0)
			[self.tableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationTop];
	}
	[self.tableView endUpdates];
}

-(UITableViewCellEditingStyle)tableView:(UITableView*)tableView editingStyleForRowAtIndexPath:(NSIndexPath*)indexPath {
	// which row are we getting
	NSUInteger row = [indexPath row];
	
	// if we are in edit mode
	if (self.editing) {
		// show delete style and insert for 'add new'
		if (row != chapter.grammarNotes.count)
			return UITableViewCellEditingStyleDelete;
		else
			return UITableViewCellEditingStyleInsert;
	}
	return UITableViewCellEditingStyleNone;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    // if we are deleting rows
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the dictionary and save
		[chapter.grammarNotes removeObjectAtIndex:[indexPath row]];
		[book saveAs:book.bookName];
		
		// delete from view too
        [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:YES];
    }   
    else
		if (editingStyle == UITableViewCellEditingStyleInsert) {
			// edit grammar using edit controller
			EditGrammarController *editGrammarController = [[EditGrammarController alloc] initWithNibName:@"EditGrammarController" bundle:nil];
			GrammarNote *newGrammar = [[GrammarNote alloc] init];
			[editGrammarController setEditableObject:newGrammar isNew:YES];
			editGrammarController.editableListDelegate = self;
			UINavigationController *secondNavigationController = [[UINavigationController alloc] initWithRootViewController:editGrammarController];
			[self.navigationController presentModalViewController:secondNavigationController animated:YES];
			[newGrammar release];
			[editGrammarController release];
			[secondNavigationController release];
		}
}

-(void)didUpdateObject:(id)updatedObject {
	// update dictionary to disk
	[book saveAs:book.bookName];
	
	// refresh table
	[self.tableView reloadData];
}

-(void)didAddNewObject:(id)newObject {
	// save new grammar note
	GrammarNote *newGrammar = (GrammarNote*)newObject;
	[chapter.grammarNotes addObject:newGrammar];
	[book saveAs:book.bookName];
	
	// reload table
	[self.tableView reloadData];
}

#pragma mark -
#pragma mark Table view delegate

-(void)tableView:(UITableView*)tableView didSelectRowAtIndexPath:(NSIndexPath*) indexPath {
	// find row
	NSUInteger row = [indexPath row];
	
	// save index path
    [lastIndexPath release];
	lastIndexPath = [indexPath retain];
    
    // which grammar note?
    GrammarNote *selectedNote = nil;
    if (tableView == self.tableView) {
        if (row < chapter.grammarNotes.count)
            selectedNote = [chapter.grammarNotes objectAtIndex:row];
    }
    else {
        if (row < searchResults.count)
            selectedNote = [searchResults objectAtIndex:row];
    }
    
	// editing mode?
	if (self.editing) {
		// add new?
		if (row >= [chapter.grammarNotes count]) {
			[self tableView:tableView commitEditingStyle:UITableViewCellEditingStyleInsert forRowAtIndexPath:indexPath];
		}
		else {
			// edit grammar using edit controller
			EditGrammarController *editGrammarController = [[EditGrammarController alloc] initWithNibName:@"EditGrammarController" bundle:nil];
			[editGrammarController setEditableObject:selectedNote isNew:NO];
			editGrammarController.editableListDelegate = self;
			[self.navigationController pushViewController:editGrammarController animated:YES];
			[editGrammarController release];
		}
	}
    else {
        // show view controller to view grammar
        GrammarViewController *viewController = [[GrammarViewController alloc] initWithNibName:@"GrammarViewController" bundle:nil];
        [viewController setViewNote:selectedNote allowSelect:NO];
        [self.navigationController pushViewController:viewController animated:YES];
        [viewController release];
    }
}

-(BOOL)tableView:(UITableView*)tableView canMoveRowAtIndexPath:(NSIndexPath*)indexPath {
	// can't re-order 'add new'
	if ([indexPath row] < chapter.grammarNotes.count)
		return YES;
	else
		return NO;
}

-(void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
	// get from and to row
	int fromRow = [fromIndexPath row];
	int toRow = [toIndexPath row];
	
	// swap these rows in the array
	GrammarNote *moveNote = [[chapter.grammarNotes objectAtIndex:fromRow] retain];
	[chapter.grammarNotes removeObjectAtIndex:fromRow];
	[chapter.grammarNotes insertObject:moveNote atIndex:toRow];
	[moveNote release];
	
	// save dictionary
	[book saveAs:book.bookName];
}

#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    // Relinquish ownership of anything that can be recreated in viewDidLoad or on demand.
    // For example: self.myOutlet = nil;
}


- (void)dealloc {
    [lastIndexPath release];
    /*
    [searchBar release];
    [searchController release];
	[keyboardHideButton release];
	[ftsSwitch release];
	[romajiSwitch release];
	[searchToolbar release];
     */
    [book release];
    [chapter release];
    [super dealloc];
}


@end

