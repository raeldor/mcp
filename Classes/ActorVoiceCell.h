//
//  ActorVoiceCell.h
//  Mcp
//
//  Created by Ray on 10/21/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface ActorVoiceCell : UITableViewCell {
	IBOutlet UISegmentedControl *voiceNameSegmentControl;
	IBOutlet UIButton *testButton;
}

@property (nonatomic, retain) UISegmentedControl *voiceNameSegmentControl;
@property (nonatomic, retain) UIButton *testButton;

-(void)loadVoiceList;
-(BOOL)setVoiceAs:(NSString*)inVoiceName;

@end
