//
//  ConversationListCell.h
//  Mcp
//
//  Created by Ray on 1/29/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface ConversationListCell : UITableViewCell {
	IBOutlet UILabel *grammarLabel;
//	IBOutlet UIWebView *dialogueWebView;
	IBOutlet UILabel *actorLabel1;
	IBOutlet UILabel *actorLabel2;
	IBOutlet UILabel *actorLabel3;
	IBOutlet UILabel *actorLabel4;
	IBOutlet UILabel *dialogueLabel1;
	IBOutlet UILabel *dialogueLabel2;
	IBOutlet UILabel *dialogueLabel3;
	IBOutlet UILabel *dialogueLabel4;
}

@property (nonatomic, retain) UILabel *grammarLabel;
@property (nonatomic, retain) UILabel *actorLabel1;
@property (nonatomic, retain) UILabel *actorLabel2;
@property (nonatomic, retain) UILabel *actorLabel3;
@property (nonatomic, retain) UILabel *actorLabel4;
@property (nonatomic, retain) UILabel *dialogueLabel1;
@property (nonatomic, retain) UILabel *dialogueLabel2;
@property (nonatomic, retain) UILabel *dialogueLabel3;
@property (nonatomic, retain) UILabel *dialogueLabel4;
//@property (nonatomic, retain) UIWebView *dialogueWebView;

@end
