//
//  TrainingWord.h
//  Mcp
//
//  Created by Ray Price on 11/20/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TrainingWord : NSObject {
	NSString *wordName;
	NSString *wordKana;
    NSMutableArray *phonemes;
}

-(id)initWithName:(NSString*)inName andKana:(NSString*)inKana;
-(id)initWithDictionaryEntry:(NSDictionary*)inDict;
-(int)getFeatureIndexAtPercentPos:(float)inPercentPos;
-(void)dealloc;

@property (nonatomic, retain) NSString *wordName;
@property (nonatomic, retain) NSString *wordKana;
@property (nonatomic, retain) NSMutableArray *phonemes;

@end
