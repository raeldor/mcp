//
//  TapToCloseDelegate.h
//  HF Sensei
//
//  Created by Ray Price on 3/12/13.
//
//

#ifndef HF_Sensei_TapToCloseDelegate_h
#define HF_Sensei_TapToCloseDelegate_h

@protocol TapToCloseDelegate

-(void)didTapToClose;

@end

#endif
