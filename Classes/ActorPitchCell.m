//
//  ActorPitchCell.m
//  Mcp
//
//  Created by Ray on 10/21/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "ActorPitchCell.h"


@implementation ActorPitchCell

@synthesize pitchSlider;
@synthesize textLabel;
@synthesize percentLabel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if ((self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])) {
        // Initialization code
    }
    return self;
}

-(IBAction)pitchChanged:(id)sender {
    // set text
    UISlider *mySlider = (UISlider*)sender;
    percentLabel.text = [NSString stringWithFormat:@"%3.0f", mySlider.value];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)dealloc {
	[pitchSlider release];
    [textLabel release];
    [percentLabel release];
    [super dealloc];
}


@end
