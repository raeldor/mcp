//
//  PercentageCell.m
//  Mcp
//
//  Created by Ray on 4/22/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "PercentageCell.h"

@implementation PercentageCell

@synthesize textLabel;
@synthesize percentSlider;
@synthesize percentLabel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if ((self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])) {
        // Initialization code
    }
    return self;
}

-(IBAction)sliderChanged:(id)sender {
    // set text
    UISlider *mySlider = (UISlider*)sender;
    percentLabel.text = [NSString stringWithFormat:@"%3.0f%%", mySlider.value*100.0f];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (void)dealloc {
	[percentSlider release];
    [textLabel release];
    [percentLabel release];
    [super dealloc];
}


@end
