//
//  GameData.h
//  Mcp
//
//  Created by Ray on 3/10/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Book.h"

@interface GameData : NSObject <NSCoding> {
    NSMutableArray *selectedPractice; // string in the format <book key>/n<chapter key>/n<grammar name>
    NSString *selectedBookName;
}

@property (nonatomic, retain) NSMutableArray *selectedPractice;
@property (nonatomic, retain) NSString *selectedBookName;

-(void)removePracticeForBookUniqueKey:(NSString*)bookUniqueKey;
-(BOOL)containsPracticeKey:(NSString*)inPracticeKey;

@end
