//
//  SwitchCell.m
//  Mcp
//
//  Created by Ray on 4/22/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "SwitchCell.h"


@implementation SwitchCell

@synthesize textLabel;
@synthesize mySwitch;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if ((self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (void)dealloc {
	[mySwitch release];
    [textLabel release];
    [super dealloc];
}


@end
