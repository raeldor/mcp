//
//  EditPhotoViewController.h
//  Mcp
//
//  Created by Ray on 10/22/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface EditPhotoViewController : UIViewController {
	IBOutlet UIImageView *photoImageView;
	
	UIImage *originalPhoto;
}

@property (nonatomic, retain) UIImage *originalPhoto;
@property (nonatomic, retain) UIImageView *photoImageView;

-(void)cancelButtonClick:(id)sender;
-(void)saveButtonClick:(id)sender;
-(void)setPhoto:(UIImage*)inPhoto;

@end
