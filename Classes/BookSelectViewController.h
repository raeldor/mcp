//
//  BookSelectViewController.h
//  Mcp
//
//  Created by Ray Price on 8/31/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SelectViewControllerDelegate.h"

@interface BookSelectViewController : UITableViewController <UITableViewDelegate, UITableViewDataSource> {
	// delegate
	id<SelectViewControllerDelegate> selectDelegate;
	
	NSMutableArray *bookList; // list of NSString*
	NSIndexPath *lastIndexPath;
}

@property (nonatomic, retain) NSMutableArray *bookList;
@property (nonatomic, assign) id<SelectViewControllerDelegate> selectDelegate;

-(void)populateBookList;
-(IBAction)selectButtonClick:(id)sender;
-(IBAction)cancelButtonClick:(id)sender;

@end