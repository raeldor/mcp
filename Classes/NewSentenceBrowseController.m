//
//  NewSentenceBrowseController.m
//  HF Sensei
//
//  Created by Ray on 4/5/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import "NewSentenceBrowseController.h"
#import "DictionaryListCell.h"
#import "SampleSentences.h"
#import "FlashCard.h"

@implementation NewSentenceBrowseController

@synthesize searchController;

-(void)viewDidLoad {
	[super viewDidLoad];
    
    // save sample sentences and dictionary reference for speed
    sampleSentences = [SampleSentences getSingleton];
    japaneseDictionary = [JapaneseDictionary getSingleton];
    
    // create arrays for search results and selected sentences
    searchResults = [[NSMutableArray alloc] initWithCapacity:10];
    
    // create search controller
    searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    searchController.searchResultsUpdater = self;
    searchController.obscuresBackgroundDuringPresentation = false;
    searchController.searchBar.autocapitalizationType = UITextAutocapitalizationTypeNone;
    searchController.searchBar.placeholder = @"Search";
    searchController.searchBar.scopeButtonTitles = [NSArray arrayWithObjects:@"Meaning",@"Romaji",@"Reading",@"Kanji", nil];
    self.navigationItem.searchController = searchController;
    self.definesPresentationContext = true;
    
    // base height on autolayout content
    self.tableView.estimatedRowHeight = 89.0f;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
//    self.searchDisplayController.searchResultsTableView.estimatedRowHeight = 89.0f;
//    self.searchDisplayController.searchResultsTableView.rowHeight = UITableViewAutomaticDimension;
    
    /*
    // create activity indicator for tableview
    activityIndicator = [[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(0.0f, 0.0f, 32.0f, 32.0f)];
    [activityIndicator setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleGray];
    [self.tableView addSubview:activityIndicator];
    
    // and again for search view
    activityIndicator2 = [[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(0.0f, 0.0f, 32.0f, 32.0f)];
    [activityIndicator2 setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleGray];
    [self.searchDisplayController.searchResultsTableView addSubview:activityIndicator2];
    
    // no keyboard by default
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name: UIKeyboardWillShowNotification object:nil];
    keyboardHeight = 0;
     */
}

-(void)updateSearchResultsForSearchController:(UISearchController *)searchController {
    // cancel any existing timer here because we still need to cancel even if search text is zero
    if (searchTimer) {
        [searchTimer invalidate];
        [searchTimer release];
        searchTimer = nil;
    }

    // get search results
    if ([searchController.searchBar.text length] != 0) {
        // set timer to do search so we're not constantly searching which is slow
        searchTimer = [[NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(searchTimerFired:) userInfo:searchController repeats:FALSE] retain];
    }
    else {
        // clear results
        [searchResults release];
        searchResults = [[NSMutableArray alloc] init];
        
        // reload tableview
        [self.tableView reloadData];
    }
    
    // loaded
//    isSearchResultsLoaded = YES;
    
    // reload tableview
//    [self.searchDisplayController.searchResultsTableView reloadData];
//    [self.tableView reloadData];
    
    // end animation
//    [activityIndicator stopAnimating];
//    [activityIndicator2 stopAnimating];
    NSLog(@"stop animating\r\n");
}

-(void)searchTimerFired:(NSTimer*)inTimer {
    // performing search
    performingSearch = YES;
    
    // release existing results
    [searchResults release];
    
    // perform search
    searchResults = [[sampleSentences getIndexListFromSearchString:searchController.searchBar.text usingSearchScope:searchController.searchBar.selectedScopeButtonIndex] retain];
    
    // finished search
    performingSearch = NO;
    
    // reload tableview
    [self.tableView reloadData];
}

// TEMPORARY FIX FOR BAD CELL AUTO LAYOUT ISSUES
/*
-(void)viewDidAppear:(BOOL)animated {
    // reload
    [self.tableView reloadData];
    
    // call super
    [super viewDidAppear:animated];
}*/

/*
-(void)viewWillAppear:(BOOL)animated {
    // reload
    [self.tableView reloadData];
    
    // call super
    [super viewWillAppear:animated];
}*/

/*
-(void)keyboardWillShow:(NSNotification *)inNotification {
	// save keyboard height
	CGRect keyboardRect;
	[[[inNotification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] getValue:&keyboardRect];
    keyboardHeight = keyboardRect.size.height;
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    // reload table view
    self.searchDisplayController.searchBar.text = @"";
    [self.tableView reloadData];
}

-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    // show scope bar after first time
    self.searchDisplayController.searchBar.showsScopeBar = YES;
}

-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchScope:(NSInteger)searchOption {
    // not loaded yet
    isSearchResultsLoaded = NO;
    
    // bottom overlap
    int overlap = (self.navigationController.navigationBar.frame.size.height * 2) + 20;
    int overlap2 = self.tabBarController.tabBar.frame.size.height+20;
    if (!self.navigationController.navigationBarHidden)
        overlap2 += self.navigationController.navigationBar.frame.size.height * 2;
    if ([self.searchDisplayController.searchBar isFirstResponder])
        overlap2 += keyboardHeight;
    
    // show activity indicator
    [activityIndicator setCenter:CGPointMake(self.tableView.frame.size.width/2.0f, (self.tableView.frame.size.height-overlap)/2.0f)];
    [activityIndicator2 setCenter:CGPointMake(self.tableView.frame.size.width/2.0f, (self.tableView.frame.size.height-overlap2)/2.0f)];
    [activityIndicator startAnimating];
    [activityIndicator2 startAnimating];
    NSLog(@"start animating\r\n");
    
    // update search results
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(updateSearchResultsUsingSearchController:) object:controller];
    [self performSelector:@selector(updateSearchResultsUsingSearchController:) withObject:controller afterDelay:0.0];
    
    return NO;
}

-(BOOL)searchDisplayController:(UISearchDisplayController *)controller
shouldReloadTableForSearchString:(NSString *)searchString
{
    // not loaded yet
    isSearchResultsLoaded = NO;
    
    // bottom overlap
    int overlap = (self.navigationController.navigationBar.frame.size.height * 2) + 20;
    int overlap2 = self.tabBarController.tabBar.frame.size.height+20;
    if (!self.navigationController.navigationBarHidden)
        overlap2 += self.navigationController.navigationBar.frame.size.height * 2;
    if ([self.searchDisplayController.searchBar isFirstResponder])
        overlap2 += keyboardHeight;
    
    // show activity indicator
    [activityIndicator setCenter:CGPointMake(self.tableView.frame.size.width/2.0f, (self.tableView.frame.size.height-overlap)/2.0f)];
    [activityIndicator2 setCenter:CGPointMake(self.tableView.frame.size.width/2.0f, (self.tableView.frame.size.height-overlap2)/2.0f)];
    [activityIndicator startAnimating];
    [activityIndicator2 startAnimating];
    NSLog(@"start animating\r\n");
    
    // call, but cancel previous
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(updateSearchResultsUsingSearchController:) object:controller];
    [self performSelector:@selector(updateSearchResultsUsingSearchController:) withObject:controller afterDelay:0.5];
    
    return NO;
}
*/

-(void)viewDidLayoutSubviews {
    // call super
    [super viewDidLayoutSubviews];
}

/*
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    // set preferred width here?
    DictionaryListCell *thisCell = (DictionaryListCell*)cell;
    thisCell.topRightLabel.preferredMaxLayoutWidth = cell.frame.size.width;
    [thisCell.contentView setNeedsLayout];
    [thisCell.contentView layoutIfNeeded];
}*/

/*
-(void)updateSearchResultsUsingSearchController:(UISearchDisplayController*)controller {
	// get search results
	if ([controller.searchBar.text length] != 0) {
        // release existing results
        [searchResults release];
        
        // perform search
        searchResults = [[sampleSentences getIndexListFromSearchString:controller.searchBar.text usingSearchScope:controller.searchBar.selectedScopeButtonIndex] retain];
    }
    else {
        // clear results
        [searchResults release];
        searchResults = [[NSMutableArray alloc] init];
    }
    
    // loaded
    isSearchResultsLoaded = YES;
    
    // reload tableview
    // reload both in case we're showing search results in regular table view
    [controller.searchResultsTableView reloadData];
    [self.tableView reloadData];
    
    // stop animating
    [activityIndicator stopAnimating];
    [activityIndicator2 stopAnimating];
    NSLog(@"stop animating\r\n");
}
*/

-(NSUInteger)numberOfEntriesInGroup:(NSUInteger)groupIndex {
    NSUInteger groupCount = [sampleSentences getGroupCount];
    return groupCount > 0 ? [sampleSentences getEntryCountUsingGroupIndex:groupIndex] : 0;
}

/*
-(SampleEntry*)entryAtGroupIndex:(int)groupIndex entryIndex:(int)entryIndex {
    return [sampleSentences getEntryUsingGroupIndex:groupIndex entryIndex:entryIndex];
}*/

-(NSInteger)numberOfSectionsInTableView:(UITableView*)tableView {
    // if we have search text, but haven't finished loading, don't show anything
//    if (self.searchDisplayController.searchBar.text.length > 0 && !isSearchResultsLoaded)
//        return 0;
    
    // if search controller
//    if (tableView == self.searchDisplayController.searchResultsTableView || (self.searchDisplayController.searchBar.text.length > 0 && isSearchResultsLoaded))
    if ((searchController.isActive || filterIsActive) && ![searchController.searchBar.text isEqualToString:@""])
		return 1;
	else {
		int groupCount = [sampleSentences getGroupCount];
		return groupCount > 0 ? groupCount : 1;
	}
}

-(NSString*)tableView:(UITableView*)tableView
titleForHeaderInSection:(NSInteger)section {
//    if (tableView == self.searchDisplayController.searchResultsTableView || (self.searchDisplayController.searchBar.text.length > 0 && isSearchResultsLoaded))
    if ((searchController.isActive || filterIsActive) && ![searchController.searchBar.text isEqualToString:@""])
        return [super tableView:tableView titleForHeaderInSection:section];
	else {
		int groupCount = [sampleSentences getGroupCount];
		return groupCount > 0 ? [sampleSentences getGroupNameUsingIndex:section] : @"No Examples";
	}
}

-(NSArray*)sectionIndexTitlesForTableView:(UITableView*)tableView {
//    if (tableView == self.searchDisplayController.searchResultsTableView || (self.searchDisplayController.searchBar.text.length > 0 && isSearchResultsLoaded))
    if ((searchController.isActive || filterIsActive) && ![searchController.searchBar.text isEqualToString:@""])
		return nil;
	else
		return [sampleSentences getGroupList];
}

-(NSInteger)tableView:(UITableView*)tableView
numberOfRowsInSection:(NSInteger)section {
//    if (tableView == self.searchDisplayController.searchResultsTableView || (self.searchDisplayController.searchBar.text.length > 0 && isSearchResultsLoaded))
    if ((searchController.isActive || filterIsActive) && ![searchController.searchBar.text isEqualToString:@""])
        return searchResults.count;
    else
        return [self numberOfEntriesInGroup:section];
}

/*
-(CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 84.0f;
}*/

/*
-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}*/

-(UITableViewCell*)tableView:(UITableView*)tableView
	   cellForRowAtIndexPath:(NSIndexPath*)indexPath {
	// get cell
	static NSString *identifier = @"DictionaryListCell";
    DictionaryListCell *cell;
//    if (tableView == self.searchDisplayController.searchResultsTableView)
//        cell = [self.tableView dequeueReusableCellWithIdentifier:identifier];
//    else
        cell = [self.tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
    
    /*
	static NSString *identifier = @"DictionaryListCell";
	DictionaryListCell *cell = (DictionaryListCell*)[tableView dequeueReusableCellWithIdentifier:identifier];
	if (cell == nil) {
		// load cell layout from nib
		NSArray *nib;
		NSString *nibName = @"SentenceListCell";
		nib = [[NSBundle mainBundle] loadNibNamed:nibName owner:self options:nil];
		cell = [nib objectAtIndex:0];
	}*/
	
	// which row are we getting
	NSUInteger section = [indexPath section];
	NSUInteger row = [indexPath row];
	
    // get sentence entry
	SampleEntry *sentence;
    // why are we loading search results into non-search table!?
//    if (tableView == self.searchDisplayController.searchResultsTableView || (self.searchDisplayController.searchBar.text.length > 0 && isSearchResultsLoaded))
    
    // well, now we know the answer.. when we are coming in with a search string
    // we display search results in normal table view.  use a new flag
    // displaySearchResultsInRegularTableView for this
//    if (tableView == self.searchDisplayController.searchResultsTableView ||
//        (displaySearchResultsInRegularTableView && isSearchResultsLoaded))
    if ((searchController.isActive || filterIsActive) && ![searchController.searchBar.text isEqualToString:@""])
        sentence = [sampleSentences getEntryUsingGroupIndex:0 entryIndex:[[searchResults objectAtIndex:row] intValue]];
    else
        sentence = [sampleSentences getEntryUsingGroupIndex:section entryIndex:row];
    
    // convert from placeholder string to markup
    NSString *markup = [sentence getMarkup];
    
	// get application delegate that has our object
    cell.topRightLabel.markupText = markup;
	cell.topLeftLabel.text = sentence.kana;
	cell.grayLabel.text = sentence.meaning;
    
    if ([cell.topRightLabel.attributedString.string hasPrefix:@"彼女は、ノーベル"]) {
        int j=0;
    }
    
    // have a go at setting the preferred width, as first time counts!
    if (searchController.isActive && ![searchController.searchBar.text isEqualToString:@""])
//    if (tableView == self.searchDisplayController.searchResultsTableView ||
//        (displaySearchResultsInRegularTableView && isSearchResultsLoaded))
        cell.topRightLabel.preferredMaxLayoutWidth = CGRectGetWidth(tableView.bounds)-16.0f;
    else
        cell.topRightLabel.preferredMaxLayoutWidth = CGRectGetWidth(tableView.bounds)-36.0f;
        
    return cell;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning]; // Releases the view if it doesn't have a superview
    // Release anything that's not essential, such as cached data
}

- (void)dealloc {
    [searchResults release];
    [searchController release];
//    [activityIndicator release];
    [super dealloc];
}


@end
