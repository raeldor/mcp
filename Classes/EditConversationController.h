//
//  EditConversationController.h
//  Mcp
//
//  Created by Ray on 10/26/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Conversation.h"
#import "EditableListDelegate.h"
#import "SelectViewControllerDelegate.h"
#import "SelectControllerDelegate.h"
#import "EditDialogueLineDelegate.h"
#import "TouchyTableView.h"
#import "Book.h"
#import "Chapter.h"

@interface EditConversationController : UIViewController <UIWebViewDelegate, UITextFieldDelegate, UINavigationControllerDelegate, UIActionSheetDelegate, UIImagePickerControllerDelegate, UITableViewDelegate, SelectViewControllerDelegate, SelectControllerDelegate, EditDialogueLineDelegate, UIAlertViewDelegate> {
    IBOutlet TouchyTableView *myTableView;
	BOOL newMode;
	Conversation *conversation;
	id<EditableListDelegate> editableListDelegate;
	NSIndexPath *lastIndexPath;
    
    int foundSize;
    NSString *testHtmlString;
	
	UIPopoverController *popOver; // for ipad compatibility
	UITextView *textViewBeingEdited;
	UITextView *linkTextView;
	NSRange linkRange;
    Book *book;
	Chapter *chapter;
}

@property (nonatomic, retain) TouchyTableView *myTableView;
@property (nonatomic, retain) Conversation *conversation;
@property (nonatomic, assign) id<EditableListDelegate> editableListDelegate;
@property (nonatomic, retain) UIPopoverController *popOver;

-(void)findHtmlHeight:(id)inString;
-(void)setEditableObject:(Conversation*)thisConversation inBook:(Book*)inBook andChapter:(Chapter*)inChapter isNew:(BOOL)isNew;
-(BOOL)isOkToSave;
-(void)cancelButtonClick:(id)sender;
-(void)saveButtonClick:(id)sender;
-(void)textFieldDidChange:(NSNotification*)note;
-(void)characterNameDidChange:(NSNotification*)note;
-(void)dialogueLineDidChange:(NSNotification*)note;
-(void)dialogueLineBeginEdit:(NSNotification*)note;
-(void)dialogueLineEndEdit:(NSNotification*)note;
-(void)actorSegmentChanged:(id)sender;
-(void)characterButtonPressed:(id)sender;
-(void)didMakeBrowserSelection:(NSString*)selection fromController:(UIViewController*)selectController;
-(void)didUpdateDialogueLine:(DialogueLine*)inLine;
-(void)didAddDialogueLine:(DialogueLine*)inLine;

@end
