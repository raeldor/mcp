//
//  Grammar.h
//  Mcp
//
//  Created by Ray on 1/7/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GrammarNote : NSObject <NSCoding> {
	NSString *title;
	NSString *text;
}

-(id)init;

@property (nonatomic, retain) NSString* title;
@property (nonatomic, retain) NSString *text;

@end
