//
//  AboutController.h
//  HF Sensei
//
//  Created by Ray on 8/9/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface AboutViewController : UIViewController <UIWebViewDelegate> {
	IBOutlet UIWebView *ackWebView;
	IBOutlet UILabel *versionLabel;
	IBOutlet UILabel *productLabel;
}

@property (nonatomic, retain) IBOutlet UIWebView *ackWebView;
@property (nonatomic, retain) IBOutlet UILabel *versionLabel;
@property (nonatomic, retain) IBOutlet UILabel *productLabel;

@end
