//
//  NewKanjiBrowseController.m
//  HFSensei
//
//  Created By Ray on 3/15/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import "NewKanjiBrowseController.h"
#import "KanjiListCell.h"
#import "KanjiDictionary.h"
#import "FlashCard.h"
#import "MultiSelectController.h"
#import "ShowKanjiController.h"

@implementation NewKanjiBrowseController

@synthesize searchController;

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
	[super viewDidLoad];
	
    // create arrays for search results and selected kanji
    searchResults = [[NSMutableArray alloc] initWithCapacity:10];
    
    // save kanji dictionary for quick reference
    kanjiDictionary = [KanjiDictionary getSingleton];
    
    // create search controller
    searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    searchController.searchResultsUpdater = self;
    searchController.obscuresBackgroundDuringPresentation = false;
    searchController.searchBar.autocapitalizationType = UITextAutocapitalizationTypeNone;
    searchController.searchBar.placeholder = @"Search";
    searchController.searchBar.scopeButtonTitles = [NSArray arrayWithObjects:@"Meaning",@"Romaji",@"Reading",@"Kanji", nil];
    self.navigationItem.searchController = searchController;
    self.definesPresentationContext = true;

    // base height on autolayout content
    self.tableView.estimatedRowHeight = 89.0f;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
//    self.searchDisplayController.searchResultsTableView.estimatedRowHeight = 89.0f;
//    self.searchDisplayController.searchResultsTableView.rowHeight = UITableViewAutomaticDimension;
    
    /*
    // create activity indicator for tableview
    activityIndicator = [[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(0.0f, 0.0f, 32.0f, 32.0f)];
    [activityIndicator setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleGray];
    [self.tableView addSubview:activityIndicator];
    
    // and again for search view
    activityIndicator2 = [[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(0.0f, 0.0f, 32.0f, 32.0f)];
    [activityIndicator2 setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleGray];
    [self.searchDisplayController.searchResultsTableView addSubview:activityIndicator2];
    
    // no keyboard by default
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name: UIKeyboardWillShowNotification object:nil];
    keyboardHeight = 0;
     */
}

-(void)updateSearchResultsForSearchController:(UISearchController *)searchController {
    // cancel any existing timer here because we still need to cancel even if search text is zero
    if (searchTimer) {
        [searchTimer invalidate];
        [searchTimer release];
        searchTimer = nil;
    }

    // get search results
    if ([searchController.searchBar.text length] != 0) {
        // set timer to do search so we're not constantly searching which is slow
        searchTimer = [[NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(searchTimerFired:) userInfo:searchController repeats:FALSE] retain];
    }
    else {
        // clear results
        [searchResults release];
        searchResults = [[NSMutableArray alloc] init];
        
        // reload tableview
        [self.tableView reloadData];
    }
}

-(void)searchTimerFired:(NSTimer*)inTimer {
    // performing search
    performingSearch = YES;
    
    // release existing results
    [searchResults release];
    
    // perform search
    searchResults = [[kanjiDictionary getIndexListFromSearchString:searchController.searchBar.text usingSearchScope:searchController.searchBar.selectedScopeButtonIndex] retain];

    // finished search
    performingSearch = NO;
    
    // reload tableview
    [self.tableView reloadData];
}


/*
-(void)keyboardWillShow:(NSNotification *)inNotification {
	// save keyboard height
	CGRect keyboardRect;
	[[[inNotification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] getValue:&keyboardRect];
    keyboardHeight = keyboardRect.size.height;
}

-(void)viewWillAppear:(BOOL)animated {
    // call super first
    [super viewWillAppear:animated];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    // reload table view
    self.searchDisplayController.searchBar.text = @"";
    [self.tableView reloadData];
}

-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    // show scope bar after first time
    self.searchDisplayController.searchBar.showsScopeBar = YES;
}

-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchScope:(NSInteger)searchOption {
    // not loaded yet
    isSearchResultsLoaded = NO;
    
    // bottom overlap
    int overlap = (self.navigationController.navigationBar.frame.size.height * 2) + 20;
    int overlap2 = self.tabBarController.tabBar.frame.size.height+20;
    if (!self.navigationController.navigationBarHidden)
        overlap2 += self.navigationController.navigationBar.frame.size.height * 2;
    if ([self.searchDisplayController.searchBar isFirstResponder])
        overlap2 += keyboardHeight;
    
    // show activity indicator
    [activityIndicator setCenter:CGPointMake(self.tableView.frame.size.width/2.0f, (self.tableView.frame.size.height-overlap)/2.0f)];
    [activityIndicator2 setCenter:CGPointMake(self.tableView.frame.size.width/2.0f, (self.tableView.frame.size.height-overlap2)/2.0f)];
    [activityIndicator startAnimating];
    [activityIndicator2 startAnimating];
    NSLog(@"start animating\r\n");

    // update search results
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(updateSearchResultsUsingSearchController:) object:controller];
    [self performSelector:@selector(updateSearchResultsUsingSearchController:) withObject:controller afterDelay:0.0];
    
    return YES;
}

-(BOOL)searchDisplayController:(UISearchDisplayController *)controller
shouldReloadTableForSearchString:(NSString *)searchString
{
    // not loaded yet
    isSearchResultsLoaded = NO;
    
    // bottom overlap
    int overlap = (self.navigationController.navigationBar.frame.size.height * 2) + 20;
    int overlap2 = self.tabBarController.tabBar.frame.size.height+20;
    if (!self.navigationController.navigationBarHidden)
        overlap2 += self.navigationController.navigationBar.frame.size.height * 2;
    if ([self.searchDisplayController.searchBar isFirstResponder])
        overlap2 += keyboardHeight;
    
    // show activity indicator
    [activityIndicator setCenter:CGPointMake(self.tableView.frame.size.width/2.0f, (self.tableView.frame.size.height-overlap)/2.0f)];
    [activityIndicator2 setCenter:CGPointMake(self.tableView.frame.size.width/2.0f, (self.tableView.frame.size.height-overlap2)/2.0f)];
    [activityIndicator startAnimating];
    [activityIndicator2 startAnimating];
    NSLog(@"start animating\r\n");

    // update search results
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(updateSearchResultsUsingSearchController:) object:controller];
    [self performSelector:@selector(updateSearchResultsUsingSearchController:) withObject:controller afterDelay:0.5];
    
    return NO;
}

-(void)updateSearchResultsUsingSearchController:(UISearchDisplayController*)controller {
	// get search results
	if ([controller.searchBar.text length] != 0) {
        // release existing results
        [searchResults release];
        
        // get search list based on scope
        searchResults = [[kanjiDictionary getIndexListFromSearchString:controller.searchBar.text usingSearchScope:controller.searchBar.selectedScopeButtonIndex] retain];
	}
    else {
        // clear results
        [searchResults release];
        searchResults = [[NSMutableArray alloc] init];
    }
    
    // loaded now
    isSearchResultsLoaded = YES;
    
    // reload tableviews
    // reload both in case we're showing search results in regular table view
    [controller.searchResultsTableView reloadData];
    [self.tableView reloadData];
    
    // end animation
    [activityIndicator stopAnimating];
    [activityIndicator2 stopAnimating];
    NSLog(@"stop animating\r\n");
}
*/

-(NSUInteger)numberOfEntriesInGroup:(NSUInteger)groupIndex {
    // return group count from dictionary
    NSUInteger groupCount = [kanjiDictionary getGroupCount];
    return groupCount > 0 ? [kanjiDictionary getEntryCountUsingGroupIndex:groupIndex] : 0;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView*)tableView {
    // if we have search text, but haven't finished loading, don't show anything
//    if (self.searchDisplayController.searchBar.text.length > 0 && !isSearchResultsLoaded)
//        return 0;
    
    // if search controller
//    if (tableView == self.searchDisplayController.searchResultsTableView || (self.searchDisplayController.searchBar.text.length > 0 && isSearchResultsLoaded))
    if ((searchController.isActive || filterIsActive) && ![searchController.searchBar.text isEqualToString:@""])
        return 1;
    else {
        // get group count from dictionary
        int groupCount = [kanjiDictionary getGroupCount];
        return groupCount > 0 ? groupCount : 1;
    }
}

-(NSString*)tableView:(UITableView*)tableView
titleForHeaderInSection:(NSInteger)section {
//    if (tableView == self.searchDisplayController.searchResultsTableView || (self.searchDisplayController.searchBar.text.length > 0 && isSearchResultsLoaded))
    if ((searchController.isActive || filterIsActive) && ![searchController.searchBar.text isEqualToString:@""])
        return [super tableView:tableView titleForHeaderInSection:section];
	else {
		int groupCount = [kanjiDictionary getGroupCount];
		return groupCount > 0 ? [kanjiDictionary getGroupNameUsingIndex:section] : @"No kanjis";
	}
}

-(NSArray*)sectionIndexTitlesForTableView:(UITableView*)tableView {
//    if (tableView == self.searchDisplayController.searchResultsTableView || (self.searchDisplayController.searchBar.text.length > 0 && isSearchResultsLoaded))
    if ((searchController.isActive || filterIsActive) && ![searchController.searchBar.text isEqualToString:@""])
		return nil;
	else
		return [kanjiDictionary getCustomCategoryList];
}

-(NSInteger)tableView:(UITableView*)tableView
numberOfRowsInSection:(NSInteger)section {
//    if (tableView == self.searchDisplayController.searchResultsTableView || (self.searchDisplayController.searchBar.text.length > 0 && isSearchResultsLoaded))
    if ((searchController.isActive || filterIsActive) && ![searchController.searchBar.text isEqualToString:@""])
        return searchResults.count;
    else
        return [self numberOfEntriesInGroup:section];
}

/*
-(CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 68.0f;
}*/

-(UITableViewCell*)tableView:(UITableView*)tableView
	   cellForRowAtIndexPath:(NSIndexPath*)indexPath {
	// get cell
	static NSString *identifier = @"KanjiListCell";
    KanjiListCell *cell;
//    if (tableView == self.searchDisplayController.searchResultsTableView)
//        cell = [self.tableView dequeueReusableCellWithIdentifier:identifier];
//    else
        cell = [self.tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
    
    
    /*
	if (cell == nil) {
		// load cell layout from nib
		NSArray *nib;
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 30200
		if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
			nib = [[NSBundle mainBundle] loadNibNamed:@"KanjiListCell-iPad" owner:self options:nil];
		else {
			nib = [[NSBundle mainBundle] loadNibNamed:@"KanjiListCell" owner:self options:nil];
		}
#else
		nib = [[NSBundle mainBundle] loadNibNamed:@"KanjiListCell" owner:self options:nil];
#endif
		cell = [nib objectAtIndex:0];
	}*/
	
	// which row are we getting
	NSUInteger row = [indexPath row];
	NSUInteger section = [indexPath section];
	
	// configure cell
	KanjiEntry *kanjiEntry;
//    if (tableView == self.searchDisplayController.searchResultsTableView || (self.searchDisplayController.searchBar.text.length > 0 && isSearchResultsLoaded))
    if ((searchController.isActive || filterIsActive) && ![searchController.searchBar.text isEqualToString:@""])
        kanjiEntry = [kanjiDictionary getEntryUsingGroupIndex:0 entryIndex:[[searchResults objectAtIndex:row] intValue]];
    else
        kanjiEntry = [kanjiDictionary getEntryUsingGroupIndex:section entryIndex:row];
	cell.kanjiLabel.text = [kanjiEntry kanji];
	cell.meaningLabel.text = [kanjiEntry getAllMeaningsUsingSeperator:@", "];
	cell.readingLabel.text = [kanjiEntry getAllReadingsUsingSeperator:@", "];
	
	return cell;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning]; // Releases the view if it doesn't have a superview
    // Release anything that's not essential, such as cached data
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
	[self.tableView reloadData];
}

- (void)dealloc {
	[searchResults release];
    [searchController release];
//    [activityIndicator release];
    [super dealloc];
}

@end
