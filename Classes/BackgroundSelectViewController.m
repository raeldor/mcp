//
//  BackgroundSelectViewController.m
//  Mcp
//
//  Created by Ray on 10/30/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//


#import "BackgroundSelectViewController.h"
#import "Background.h"
#import "BackgroundListCell.h"
#import "McpAppDelegate.h"

@implementation BackgroundSelectViewController

@synthesize backgroundList;
@synthesize selectDelegate;

#pragma mark -
#pragma mark View lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
	
	// set title
	self.title = @"Backgrounds";
	
	// create cancel button
	UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(cancelButtonClick:)];
	self.navigationItem.leftBarButtonItem = cancelButton;
	[cancelButton release];
	
	// create select button
	UIBarButtonItem *selectButton = [[UIBarButtonItem alloc] initWithTitle:@"Select" style:UIBarButtonItemStyleBordered target:self action:@selector(selectButtonClick:)];
	self.navigationItem.rightBarButtonItem = selectButton;
	[selectButton release];
	
	// search for deck archive files
	[self populateBackgroundList];
}

-(void)populateBackgroundList {
	// create new list of backgrounds
	self.backgroundList = [Background getListOfFilesWithExtension:@"background"];
}

#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return backgroundList.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	return 120;
}

// Customize the appearance of table view cells.
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	// get application delegate
    McpAppDelegate *appDelegate = (McpAppDelegate*)[[UIApplication sharedApplication] delegate];
    
	// get regular table cell
    static NSString *CellIdentifier = @"BackgroundListCell";
    BackgroundListCell *cell = (BackgroundListCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
		// load cell layout from nib
		NSArray *nib = [[NSBundle mainBundle] loadNibNamed:CellIdentifier owner:self options:nil];
		cell = [nib objectAtIndex:0];
    }
	
	// Set up the cell...
	NSUInteger row = [indexPath row];
	cell.nameLabel.text = [backgroundList objectAtIndex:row];
//	Background *background = [[Background alloc] initFromArchive:[backgroundList objectAtIndex:row]];
    Background *background = [appDelegate getBackgroundUsingName:[backgroundList objectAtIndex:row]];
	cell.backgroundImageView.image = background.image;
	cell.editingAccessoryType = UITableViewCellAccessoryDisclosureIndicator;
	cell.selectionStyle = UITableViewCellSelectionStyleBlue;
	
    return cell;
}

-(IBAction)selectButtonClick:(id)sender {
	// hide keyboard and pop controller
	[self.navigationController popViewControllerAnimated:YES];

	// call delegate to notify about selection
	[selectDelegate didMakeBrowserSelection:[backgroundList objectAtIndex:[lastIndexPath row]] fromController:self];
}

-(IBAction)cancelButtonClick:(id)sender {	
	// pop controler
	[self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -
#pragma mark Table view delegate

-(void)tableView:(UITableView*)tableView didSelectRowAtIndexPath:(NSIndexPath*) indexPath {
	// save last index
    [lastIndexPath release];
	lastIndexPath = [indexPath retain];
}

#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    // Relinquish ownership of anything that can be recreated in viewDidLoad or on demand.
    // For example: self.myOutlet = nil;
}


- (void)dealloc {
	[backgroundList release];
	[lastIndexPath release];
    [super dealloc];
}

@end


