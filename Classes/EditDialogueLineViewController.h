//
//  EditDialogueLineViewController.h
//  Mcp
//
//  Created by Ray Price on 10/20/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DialogueLine.h"
#import "EditDialogueLineDelegate.h"
#import "SelectControllerDelegate.h"
#import "Chapter.h"

@interface EditDialogueLineViewController : UIViewController <SelectControllerDelegate> {
    IBOutlet UIWebView *webView;
    
	BOOL newMode;
	DialogueLine *thisLine;
	id<EditDialogueLineDelegate> editDialogueLineDelegate;
    Chapter *chapter;
}

@property (nonatomic, retain) UIWebView *webView;
@property (nonatomic, retain) DialogueLine *thisLine;
@property (nonatomic, assign) id<EditDialogueLineDelegate> editDialogueLineDelegate;

-(void)setEditableObject:(DialogueLine*)thisLine isNew:(BOOL)isNew ;
-(void)setChapterAs:(Chapter*)inChapter;
-(BOOL)isOkToSave;
-(void)cancelButtonClick:(id)sender;
-(void)saveButtonClick:(id)sender;
-(void)linkGrammar:(UIMenuController*)sender;
-(void)unlinkGrammar:(UIMenuController*)sender;

@end
