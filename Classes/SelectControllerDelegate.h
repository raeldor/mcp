//
//  SelectControllerDelegate.h
//  Mcp
//
//  Created by Ray on 1/13/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SelectControllerDelegate

@optional

-(void)didMakeSelection:(int)inSelection;
-(void)didCancelSelection;

@end
