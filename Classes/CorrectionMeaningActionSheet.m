//
//  CorrectionMeaningActionSheet.m
//  Mcp
//
//  Created by Ray Price on 1/17/13.
//
//


#import "CorrectionMeaningActionSheet.h"
#import "CorrectionSelection.h"

@implementation CorrectionMeaningActionSheet

-(id)initWithTitle:(NSString*)inTitle delegate:(id<UIActionSheetDelegate>)inDelegate andWord:(JapaneseWord*)inWord {
    self = [super initWithTitle:inTitle delegate:inDelegate cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:nil, nil];
    if (self) {
        // save word entry
        wordEntry = inWord;
        
        // create the picker
        UIPickerView *wordPicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0.0, 44.0, 0.0, 0.0)];
        wordPicker.showsSelectionIndicator = YES;
        wordPicker.delegate = self;
        wordPicker.dataSource = self;
        
        // create toolbar for cancel, done at top
        UIToolbar *pickerToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
        pickerToolbar.barStyle = UIBarStyleBlackOpaque;
        [pickerToolbar sizeToFit];
        
        // create bar buttons for bar
        NSMutableArray *barItems = [[NSMutableArray alloc] init];
        UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(didPressPickerCancel:)];
        [barItems addObject:cancelButton];
        UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
        [barItems addObject:flexSpace];
        UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(didPressPickerDone:)];
        [barItems addObject:doneButton];
        [cancelButton release];
        [flexSpace release];
        [doneButton release];
        
        // add buttons to toolbar
        [pickerToolbar setItems:barItems animated:YES];
        [barItems release];
        
        // add toolbar and picker to action sheet
        [self addSubview:pickerToolbar];
        [self addSubview:wordPicker];
        
        // release objects
        [pickerToolbar release];
        [wordPicker release];
        
        // get a list of words that have same reading
        NSArray *words = [[JapaneseDictionary getSingleton] getIndexListFromSearchString:wordEntry.kanji usingSearchScope:2];
        
        // create new array of correction selections
        correctionSelections = [[NSMutableArray alloc] initWithCapacity:10];
        
        // selection correction (later will be input)
        selectedCorrection = [[CorrectionSelection alloc] initWithDictionaryEntry:inWord];
    }
    
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void)didPressPickerCancel:(id)sender {
    // get picker
    [self dismissWithClickedButtonIndex:0 animated:YES];
}

-(void)didPressPickerDone:(id)sender {
    // get picker
    [self dismissWithClickedButtonIndex:0 animated:YES];
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView*)pickerView {
	return 1;
}

-(NSInteger)pickerView:(UIPickerView*)pickerView numberOfRowsInComponent:(NSInteger)component {
    // number of senses
    return selectedCorrection.senses.count;
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return @"Not implemented";
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
}

-(UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view {
    // if we don't have a view already
    UILabel *thisLabel;
	if (view == nil) {
        // create a new one
		UILabel *newLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width-40, 30)];
		newLabel.font = [UIFont boldSystemFontOfSize:12];
		newLabel.lineBreakMode = UILineBreakModeWordWrap;
		newLabel.numberOfLines = 3;
		newLabel.backgroundColor = [UIColor clearColor];
		newLabel.opaque = NO;
		newLabel.textAlignment = UITextAlignmentLeft;
		
        // use this label
        thisLabel = newLabel;
		[newLabel autorelease];
	}
	else {
        // change text on existing label
		thisLabel = (UILabel*)view;
	}
    
    // return sense
    NSString *thisSense = [selectedCorrection.senses objectAtIndex:row];
    thisLabel.text = [NSString stringWithFormat:@"%d. %@", row+1, thisSense];
    
    return thisLabel;
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */

@end

