//
//  ShowTextAsCell.h
//  Mcp
//
//  Created by Ray on 4/23/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShowTextAsCell : UITableViewCell {
    IBOutlet UISegmentedControl *asSegment;
}

@property (nonatomic, retain) UISegmentedControl *asSegment;

@end
