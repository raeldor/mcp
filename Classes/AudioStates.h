/*
 *  AudioStates.h
 *  Mcp
 *
 *  Created by Ray on 11/8/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#include "AudioToolbox/AudioQueue.h"
#include "AudioToolbox/AudioToolbox.h"

#ifndef AudioStates_h
#define AudioStates_h

#define NUM_BUFFERS 6
//#define BUFFER_SIZE 8000

enum AUDIORECORDER_STATE {AUDIORECORDER_STATE_STOPPED, AUDIORECORDER_STATE_LISTENING, AUDIORECORDER_STATE_RECORDING, AUDIORECORDER_STATE_ANALYZING, AUDIORECORDER_STATE_PAUSED};
enum AUDIOPLAYER_STATE {AUDIOPLAYER_STATE_STOPPED, AUDIOPLAYER_STATE_PLAYING, AUDIOPLAYER_STATE_PAUSED};

typedef struct _RecordState
{
	AudioStreamBasicDescription dataFormat;
	AudioQueueRef queue;
	AudioQueueBufferRef buffers[NUM_BUFFERS];
    enum AUDIORECORDER_STATE state;
    void* recordBuffer;
    int recordBufferSize;
    int recordBufferPos;
    BOOL isMuted;
    
    // to monitor background noise
    BOOL waitingForFirstBuffer;
    float avgBackgroundNoise;
} RecordState;

typedef struct _PlayState
{
	AudioStreamBasicDescription dataFormat;
	AudioQueueRef queue;
	AudioQueueBufferRef buffers[NUM_BUFFERS];
    enum AUDIOPLAYER_STATE state;
    void *playBuffer;
    int playBufferSize;
    int playBufferPos;
} PlayState;

#endif