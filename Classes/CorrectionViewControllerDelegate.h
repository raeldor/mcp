//
//  CorrectionViewControllerDelegate.h
//  Mcp
//
//  Created by Ray Price on 1/15/13.
//
//

#ifndef Mcp_CorrectionViewControllerDelegate_h
#define Mcp_CorrectionViewControllerDelegate_h

@protocol CorrectionViewControllerDelegate

-(void)didMakeCorrection;
-(void)didCancelCorrection;

@end


#endif
