//
//  RootViewController.h
//  Mcp
//
//  Created by Ray on 10/14/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RootViewController : UIViewController <UIAlertViewDelegate> {
    IBOutlet UIButton *playButton;
    IBOutlet UILabel *playLabel;
    
    int optionsCheckCount;
    NSTimer *checkTimer;
}

-(IBAction)showFlashDecks:(id)sender;

-(void)checkOptionsTimerFired:(NSTimer*)sender;

@end
