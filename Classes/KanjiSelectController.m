//
//  KanjiSelectController.m
//  HF Sensei
//
//  Created by Ray Price on 2/25/13.
//
//

#import "KanjiSelectController.h"
#import "KanjiEntry.h"
#import "FlashCard.h"
#import "MultiSelectController.h"
#import "KanjiDictionary.h"

@interface KanjiSelectController ()

@end

@implementation KanjiSelectController

@synthesize selectDelegate;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // create select list
    selectedKanjis = [[NSMutableArray alloc] initWithCapacity:10];
    
    // add long press recognizer to tableviews
    [self.tableView addGestureRecognizer:longPressRecognizer];
}

-(IBAction)selectButtonClick:(id)sender {
	// hide keyboard and pop controller
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
//    [self.navigationController dismissModalViewControllerAnimated:YES];
	
	// call delegate to notify about selection
	[selectDelegate didMakeBrowserSelection:selectedKanjis];
}

-(IBAction)cancelButtonClick:(id)sender {
    // if we have cards, confirm first
    if (selectedKanjis.count > 0) {
		UIAlertView *myAlert = [[UIAlertView alloc] initWithTitle:@"Cancel" message:@"Cancel adding these cards, are you sure?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
		[myAlert show];
		[myAlert release];
    }
    else {
        // hide keyboard and pop controler
        [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
//        [self.navigationController dismissModalViewControllerAnimated:YES];
    }
}

-(IBAction)handleLongPressGesture:(UIGestureRecognizer *)gestureRecognizer {
    // gesture begining?
    if (gestureRecognizer.state == UIGestureRecognizerStateBegan) {
        
        // did we long press on a row?
        UITableView *thisTableView = (UITableView*)gestureRecognizer.view;
        
        /*
        // select all search results
        if (thisTableView == self.searchDisplayController.searchResultsTableView) {
            for (int i=0; i < searchResults.count; i++) {
                // get entry
                KanjiEntry *kanjiEntry = [[KanjiDictionary getSingleton] getEntryUsingGroupIndex:0 entryIndex:[[searchResults objectAtIndex:i] intValue]];
                
                // add new card to selection
                FlashCard *newCard = [[FlashCard alloc] initWithKanji:[kanjiEntry kanji] kana:nil meanings:[NSArray arrayWithObject:[kanjiEntry.meanings objectAtIndex:0]] notes:nil];
                [selectedKanjis addObject:newCard];
                [newCard release];
                
            }
            
            // refresh table to display check mark
            [thisTableView reloadData];
            
            return;
        }*/
        
        CGPoint p = [gestureRecognizer locationInView:thisTableView];
        NSIndexPath *indexPath = [thisTableView indexPathForRowAtPoint:p];
        if (indexPath != nil) {
            [self didSelectRowAtIndexPath:indexPath forTableView:thisTableView wasLongPress:YES];
        }
    }
}

-(int)getSelectedIndexForKanji:(NSString*)inKanji {
    // already in selected list?
    int selectedIndex = -1;
    for (int i=0; i < selectedKanjis.count; i++)
        if ([[[selectedKanjis objectAtIndex:i] kanji] isEqualToString:inKanji]) {
            selectedIndex = i;
            break;
        }
    return selectedIndex;
}

-(void)didSelectRowAtIndexPath:(NSIndexPath*)indexPath forTableView:(UITableView*)tableView wasLongPress:(BOOL)wasLongPress {
	// which row are we getting
	NSUInteger row = [indexPath row];
	NSUInteger section = [indexPath section];
    lastSelectedTableView = tableView;
	lastSelectedSection = section;
	lastSelectedRow = row;
	
	// find kanji entry we are picking
	KanjiEntry *kanjiEntry;
    if ((self.searchController.isActive || filterIsActive) && ![self.searchController.searchBar.text isEqualToString:@""])
        kanjiEntry = [[KanjiDictionary getSingleton] getEntryUsingGroupIndex:0 entryIndex:[[searchResults objectAtIndex:row] intValue]];
    else
        kanjiEntry = [[KanjiDictionary getSingleton] getEntryUsingGroupIndex:section entryIndex:row];
	
    // already in selected list?
    int selectedIndex = [self getSelectedIndexForKanji:[kanjiEntry kanji]];
    
    // select or deselect
    if (selectedIndex == -1) {
        // don't show meanings selector is there is only one, or user didn't want to multi-select
        if ([[kanjiEntry meanings] count] == 1 || !wasLongPress) {
            // add new card to selection
            FlashCard *newCard = [[FlashCard alloc] initWithKanji:[kanjiEntry kanji] kana:nil meanings:[kanjiEntry meanings] notes:nil];
            [selectedKanjis addObject:newCard];
            [newCard release];
            
            // refresh table to display check mark
            [tableView reloadData];
        }
        else {
            // go to a screen to select the wanted meanings
            MultiSelectController *selector = [[MultiSelectController alloc] initWithSelectedItems:nil title:@"Meanings" listSource:[kanjiEntry meanings]];
            selector.delegate = self;
            UINavigationController *secondNavigationController = [[UINavigationController alloc] initWithRootViewController:selector];
            [self.navigationController presentViewController:secondNavigationController animated:YES completion:nil];
//            [self.navigationController presentModalViewController:secondNavigationController animated:YES];
            [secondNavigationController release];
            [selector release];
        }
    }
    else {
        [selectedKanjis removeObjectAtIndex:selectedIndex];
    }
    [tableView reloadData];
}

-(void)tableView:(UITableView*)tableView
didSelectRowAtIndexPath:(NSIndexPath*) indexPath {
    // call without long press
    [self didSelectRowAtIndexPath:indexPath forTableView:tableView wasLongPress:NO];
}

// called by multi selector of meanings
-(void)didMakeSelection:(NSArray*)selection notes:(NSArray*)inNotes {
	// find the selected word again
	KanjiEntry *kanjiEntry;
    if ((self.searchController.isActive || filterIsActive) && ![self.searchController.searchBar.text isEqualToString:@""])
        kanjiEntry = [kanjiDictionary getEntryUsingGroupIndex:0 entryIndex:[[searchResults objectAtIndex:lastSelectedRow] intValue]];
    else
        kanjiEntry = [kanjiDictionary getEntryUsingGroupIndex:lastSelectedSection entryIndex:lastSelectedRow];
    
	// add new card to selection
	FlashCard *newCard = [[FlashCard alloc] initWithKanji:[kanjiEntry kanji] kana:nil meanings:selection notes:nil];
	[selectedKanjis addObject:newCard];
	[newCard release];
	
	// refresh table to display check mark
    [lastSelectedTableView reloadData];
}

-(void)didCancelSelection {
}

/*
-(void)updateSearchResultsUsingSearchController:(UISearchDisplayController*)controller {
    // call super
    [super updateSearchResultsUsingSearchController:controller];
    
    // not sure why we need to remove and re-add, but we do
	if ([controller.searchBar.text length] != 0) {
        // add long press to select individual definitions
        [controller.searchResultsTableView removeGestureRecognizer:longPressRecognizer];
        [controller.searchResultsTableView addGestureRecognizer:longPressRecognizer];
    }
    else {
        // disable long press
        [controller.searchResultsTableView removeGestureRecognizer:longPressRecognizer];
        [self.tableView removeGestureRecognizer:longPressRecognizer];
        [self.tableView addGestureRecognizer:longPressRecognizer];
    }
}
*/

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

-(UITableViewCellAccessoryType)tableView:(UITableView*)tableView
		accessoryTypeForRowWithIndexPath:(NSIndexPath*)indexPath {
    // which row are we getting
    NSUInteger row = [indexPath row];
    NSUInteger section = [indexPath section];
    
    // find kanji entry we are picking
    KanjiEntry *kanjiEntry;
//    if (tableView == self.searchDisplayController.searchResultsTableView)
    if ((self.searchController.isActive || filterIsActive) && ![self.searchController.searchBar.text isEqualToString:@""])
        kanjiEntry = [kanjiDictionary getEntryUsingGroupIndex:0 entryIndex:[[searchResults objectAtIndex:row] intValue]];
    else
        kanjiEntry = [kanjiDictionary getEntryUsingGroupIndex:section entryIndex:row];
    
    // already in selected list?
    int selectedIndex = [self getSelectedIndexForKanji:[kanjiEntry kanji]];
    
    // if this is already in selected list return checkmark
    if (selectedIndex == -1)
        return UITableViewCellAccessoryNone;
    else
        return UITableViewCellAccessoryCheckmark;
}

#pragma mark - Table view delegate

- (void)dealloc {
    [selectedKanjis release];
    [super dealloc];
}


@end
