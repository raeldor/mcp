//
//  NewTrainingData.m
//  Mcp
//
//  Created by Ray Price on 9/17/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "NewTrainingData.h"


@implementation NewTrainingData

@synthesize trainingSamples;
@synthesize phonemeTrainingData;

-(id)init {
    if ((self = [super init])) {
        // save training sample info
        trainingSamples = [[NSMutableDictionary alloc] init];
        phonemeTrainingData = [[NSMutableDictionary alloc] init];
    }
    return self;
}

-(id)initWithCoder:(NSCoder*)coder {
	if ((self = [super init])) {
        self.trainingSamples = [coder decodeObjectForKey:@"trainingSamples"];
        self.phonemeTrainingData = [coder decodeObjectForKey:@"phonemeTrainingData"];
        
        // phoneme was added later, so create empty if not loaded
        if (phonemeTrainingData == nil)
            phonemeTrainingData = [[NSMutableDictionary alloc] init];
	}
	return self;
}

-(void)encodeWithCoder:(NSCoder*)coder {
    [coder encodeObject:trainingSamples forKey:@"trainingSamples"];
    [coder encodeObject:phonemeTrainingData forKey:@"phonemeTrainingData"];
}

-(void)dealloc {
    [trainingSamples release];
    [phonemeTrainingData release];
	[super dealloc];
}

@end
