//
//  GrammarTipViewController.h
//  Mcp
//
//  Created by Ray on 1/4/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface GrammarTipViewController : UIViewController {
	IBOutlet UIWebView *tipWebView;
	IBOutlet UIButton *closeButton;
}

@property (nonatomic, retain) UIWebView *tipWebView;
@property (nonatomic, retain) UIButton *closeButton;

@end
