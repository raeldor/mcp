//
//  AudioRecorder.h
//  Mcp
//
//  Created by Ray on 4/16/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#include "AudioToolbox/AudioQueue.h"
#include "AudioToolbox/AudioToolbox.h"
#include "AudioStates.h"

@interface AudioRecorder : NSObject {
    RecordState recordState;
}

-(id)initWithSampleRate:(int)inSampleRate startMuted:(BOOL)inStartMuted;
-(void)dealloc;
-(int)getRecordBufferPos;
-(void*)getRecordBuffer;
-(BOOL)startRecording;
-(BOOL)pauseRecording;
-(BOOL)resumeRecording;
-(int)getSampleRate;
-(void)mute;
-(void)unMute;

@property (nonatomic, assign) RecordState recordState;
@property (nonatomic, readonly) BOOL isMuted;

@end

void AudioInputCallback5(void *inUserData, AudioQueueRef inAQ, AudioQueueBufferRef inBuffer, const AudioTimeStamp *inStartTime, UInt32 inNumberPacketDescriptions, const AudioStreamPacketDescription *inPacketDescs);

