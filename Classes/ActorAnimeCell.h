//
//  ActorAnimeCell.h
//  Mcp
//
//  Created by Ray Price on 10/8/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface ActorAnimeCell : UITableViewCell {
    IBOutlet UISlider *animeSlider;
    IBOutlet UILabel *nameLabel;
    IBOutlet UIImageView *animeImage;
}

@property (nonatomic, assign) UISlider *animeSlider;
@property (nonatomic, assign) UILabel *nameLabel;
@property (nonatomic, assign) UIImageView *animeImage;

@end
