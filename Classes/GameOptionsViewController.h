//
//  GameOptionsViewController.h
//  Mcp
//
//  Created by Ray on 4/22/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GameOptionsViewControllerDelegate.h"
#import "McpLocalOptions.h"
#import "SelectViewControllerDelegate.h"

@interface GameOptionsViewController : UITableViewController <UITextFieldDelegate, UINavigationControllerDelegate,  UITableViewDelegate, UIAlertViewDelegate, SelectViewControllerDelegate> {
	id<GameOptionsViewControllerDelegate> delegate;
    
    IBOutlet UISegmentedControl *jlptLevelSegmentedControl;
    IBOutlet UISegmentedControl *pickSentenceSegmentedControl;
    IBOutlet UISlider *englishSpeedSlider;
    IBOutlet UILabel *englishSpeedLabel;
    IBOutlet UISlider *japaneseSpeedSlider;
    IBOutlet UILabel *japaneseSpeedLabel;
    IBOutlet UISlider *recognitionAccuracySlider;
    IBOutlet UILabel *recognitionAccuracyLabel;
    IBOutlet UISlider *batchSizeSlider;
    IBOutlet UILabel *batchSizeLabel;
    IBOutlet UIImageView *senseiImageView;
    IBOutlet UILabel *senseiNameLabel;
    IBOutlet UISwitch *showSubtitlesSwitch;
    IBOutlet UISegmentedControl *sentenceRepeatsSegmentedControl;
}

@property (nonatomic, retain) McpLocalOptions *myOptions;
@property (nonatomic, assign) id<GameOptionsViewControllerDelegate> delegate;

-(BOOL)isOkToSave;
-(IBAction)cancelButtonPressed:(id)sender;
-(IBAction)saveButtonPressed:(id)sender;

-(IBAction)jlptLevelChanged:(id)sender;
-(IBAction)pickSentenceChanged:(id)sender;
-(IBAction)batchSizeChanged:(id)sender;
-(IBAction)englishSpeedChanged:(id)sender;
-(IBAction)japaneseSpeedChanged:(id)sender;
-(IBAction)accuracyChanged:(id)sender;
-(IBAction)showSubtitlesChanged:(id)sender;
-(IBAction)sentenceRepeatsChanged:(id)sender;

@end
