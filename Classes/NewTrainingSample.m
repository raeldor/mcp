//
//  NewTrainingSample.m
//  Mcp
//
//  Created by Ray Price on 9/17/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "NewTrainingSample.h"
#import "VoiceSample.h"

@implementation NewTrainingSample

@synthesize text;
@synthesize featureCount;
@synthesize size;
@synthesize userFeatures;
@synthesize cpuFeatures;

-(id)initWithText:(NSString*)inText featureCount:(int)inFeatureCount size:(int)inSize userFeatures:(float*)inUserFeatures cpuFeatures:(float*)inCpuFeatures {
    if ((self = [super init])) {
        // save training sample info
        self.text = inText;
        featureCount = inFeatureCount;
        size = inSize;
        
        // copy features
        userFeatures = malloc(sizeof(float)*inSize*inFeatureCount);
        cpuFeatures = malloc(sizeof(float)*inSize*inFeatureCount);
        memcpy(userFeatures, inUserFeatures, sizeof(float)*inSize*inFeatureCount);
        memcpy(cpuFeatures, inCpuFeatures, sizeof(float)*inSize*inFeatureCount);
    }
    return self;
}

-(id)initWithCoder:(NSCoder*)coder {
	if ((self = [super init])) {
        self.text = [coder decodeObjectForKey:@"text"];
        featureCount = [coder decodeInt32ForKey:@"featureCount"];
        size = [coder decodeInt32ForKey:@"size"];
        NSUInteger decodeLength;
        float *myUserFeatures = (float*)[coder decodeBytesForKey:@"userFeatures" returnedLength:&decodeLength];
        userFeatures = malloc(decodeLength);
        memcpy(userFeatures, myUserFeatures, decodeLength);
        float *myCpuFeatures = (float*)[coder decodeBytesForKey:@"cpuFeatures" returnedLength:&decodeLength];
        cpuFeatures = malloc(decodeLength);
        memcpy(cpuFeatures, myCpuFeatures, decodeLength);
	}
	return self;
}

-(void)encodeWithCoder:(NSCoder*)coder {
    [coder encodeObject:text forKey:@"text"];
    [coder encodeInt32:featureCount forKey:@"featureCount"];
    [coder encodeInt32:size forKey:@"size"];
    [coder encodeBytes:(void*)userFeatures length:sizeof(float)*size*featureCount forKey:@"userFeatures"];
    [coder encodeBytes:(void*)cpuFeatures length:sizeof(float)*size*featureCount forKey:@"cpuFeatures"];
}

-(void)dealloc {
    free(userFeatures);
    free(cpuFeatures);
    [text release];
	[super dealloc];
}

@end
