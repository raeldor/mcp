//
//  ConvGame.h
//  Mcp
//
//  Created by Ray on 11/8/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Book.h"
#import "Conversation.h"
#import "CepstralHistory.h"
#import "Actor.h"
#import "ConvGameProtocol.h"

@interface GrammarConvGame : NSObject<ConvGameProtocol> {
	Book *book;
	Conversation *currentConversation;
    NSMutableArray *currentActors;
	NSMutableArray *conversationPlayList;
    int currentPlayPart; // -1 = all, -2 = none, other = index of part
	int currentConversationIndex;
	int currentLineIndex;
	int currentSegmentIndex;
	float totalScorePercentage;
	int scoreSegmentCount;
	CepstralHistory *userCepstralHistory;
	CepstralHistory *cpuCepstralHistory;
}

// REMOVE THIS EVENTUALLY
-(void)normalizeFloatArray:(float*)inArray ofSize:(int)inSize;
-(UInt32)getSampleCountForFile:(NSString*)inFilePath;
-(void)getDataIntoFloatBuffer:(float*)inBuffer ofLength:(int)inLength fromFile:(NSString*)inFilePath;
-(void)getDataIntoFloatBuffer2:(float*)inBuffer ofLength:(int)inLength fromFile:(NSString*)inFilePath;
-(Conversation*)getConversationFromKey:(NSString*)inKey;

@property (nonatomic, retain) Book *book;
@property (nonatomic, retain) Conversation *currentConversation;
@property (nonatomic, retain) NSMutableArray *conversationPlayList;
@property (nonatomic, assign) int currentSegmentIndex;

@end

