//
//  ChapterListViewController.h
//  Mcp
//
//  Created by Ray on 12/21/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "EditableListDelegate.h"
#import "Book.h"

@interface ChapterListViewController : UITableViewController <UITableViewDelegate, UITableViewDataSource, EditableListDelegate> {
	Book *book;
    int finalEditType;
}

@property (nonatomic, retain) Book *book;

-(void)setFinalEditTypeAs:(int)inEditType;
-(void)setEditableObject:(Book *)inBook;

@end
