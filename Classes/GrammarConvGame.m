//
//  ConvGame.m
//  Mcp
//
//  Created by Ray on 11/8/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "GrammarConvGame.h"
#import "Book.h"
#import "DialogueLine.h"
#import "Actor.h"
#import "Background.h"
#import "Character.h"
#import "AudioToolbox/AudioFile.h"
#import "VoiceSample.h"
#include "vt_jpn_show.h"
#include "vt_jpn_misaki.h"
#include "math.h"
#include "limits.h"
#import "McpAppDelegate.h"
#import "Node.h"
#import "TextFunctions.h"
#import "UserProfile.h"
#import "ConverseViewController.h"
#import "NewTrainingData.h"
#import "NewTrainingSample.h"
#import "VoiceFunctions.h"
#import "FloatAudioBuffer.h"
#import "HtmlFunctions.h"

@implementation GrammarConvGame

@synthesize currentConversation;
@synthesize conversationPlayList;
@synthesize book;
@synthesize currentSegmentIndex;

-(id)initWithBookName:(NSString*)inBookName andSectionName:(NSString*)inSectionName {
	// get application delegate
    McpAppDelegate *appDelegate = (McpAppDelegate*)[[UIApplication sharedApplication] delegate];
    
	if ((self = [super init])) {
		// create cepstral history for cms
		userCepstralHistory = [[CepstralHistory alloc] initWithMfccCount:[VoiceSample getMfccCount]];
		cpuCepstralHistory = [[CepstralHistory alloc] initWithMfccCount:[VoiceSample getMfccCount]];
		
		// set line and segment index to zero
		currentConversationIndex = 0;
		currentLineIndex = 0;
		currentSegmentIndex = 0;
        currentActors = [[NSMutableArray alloc] init];
		
        // load users selected book first if we have it
        NSMutableArray *bookList = [Book getListOfFilesWithExtension:@"book"];
        BOOL bookFound = NO;
        for (int i=0; i < bookList.count; i++) {
            if ([[bookList objectAtIndex:i] isEqualToString:inBookName]) {
                book = [[Book alloc] initFromArchive:[bookList objectAtIndex:i]];
                bookFound = YES;
                break;
            }
        }
        
        // if not found, load first book
        if (!bookFound) {
            // tell user we're loading first book
            UIAlertView *myAlert = [[UIAlertView alloc] initWithTitle:@"Oops" message:[NSString stringWithFormat:@"Couldn't find the currently selected book '%@', will try and load first book.", inBookName] delegate:nil cancelButtonTitle:@"Understood" otherButtonTitles:nil];
            [myAlert show];
            [myAlert release];
            
            // load first book
            book = [[Book alloc] initFromArchive:[bookList objectAtIndex:0]];
        }
        
		// list of conversations to play
		self.conversationPlayList = [GrammarConvGame getConversationPlayListFromGrammarList:appDelegate.currentUserProfile.gameData.selectedPractice inBook:book];
        
        // if no conversations are selected
        if (self.conversationPlayList.count == 0) {
            // select all of first chapter's grammar
            Chapter *myChapter = [book.chapters objectAtIndex:0];
            for (int i=0; i < myChapter.conversations.count; i++) {
                Conversation *thisConversation = [myChapter.conversations objectAtIndex:i];
                NSArray *grammarList = [thisConversation getGrammarListAsArray];
                for (int g=0; g < grammarList.count; g++) {
                    // if this grammar key exists in our selected list
                    NSString *grammarKey = [NSString stringWithFormat:@"%@\n%@\n%@", book.uniqueKey, myChapter.uniqueKey, [grammarList objectAtIndex:g]];
                    if (![appDelegate.currentUserProfile.gameData containsPracticeKey:grammarKey])
                        [appDelegate.currentUserProfile.gameData.selectedPractice addObject:grammarKey];
                }
            }
            
            // save updated selections
            [appDelegate.currentUserProfile saveAs:appDelegate.currentUserProfile.name];
            
            // try get conversation list again
            self.conversationPlayList = [GrammarConvGame getConversationPlayListFromGrammarList:appDelegate.currentUserProfile.gameData.selectedPractice inBook:book];
        }
        
        // shouldn't have nill conversation so abort
        if (conversationPlayList.count == 0) {
            // display error to user and return nil object
            UIAlertView *myAlert = [[UIAlertView alloc] initWithTitle:@"Oops" message:@"No individual conversations found for the selected grammar.  Select all grammars for this chapter to practice." delegate:nil cancelButtonTitle:@"Understood" otherButtonTitles:nil];
            [myAlert show];
            [myAlert release];
        }
        else {
            // load conversation
            self.currentConversation = [self getConversationFromKey:[self.conversationPlayList objectAtIndex:currentConversationIndex]];
            
            // load actors into current actors array
            [self loadActors];
        
            // get current play part
            [self updateCurrentPlayPart];
        }
	}
	return self;
}

-(FlashDeck*)getDeck {
    return nil;
}

-(gameTypeEnum)getGameType {
    return nil;
}


-(int)getConversationCount {
    return conversationPlayList.count;
}

-(Book*)getBook {
    return book;
}

-(void)loadActors {
    // load actors for current conversation into actors array
    [currentActors removeAllObjects];
    for (int i=0; i < currentConversation.characters.count; i++) {
        Character *myChar = [currentConversation.characters objectAtIndex:i];
        Actor *myActor = [[Actor alloc] initFromArchive:myChar.actorName];
        [currentActors addObject:myActor];
        [myActor release];
    }
}

+(NSMutableArray*)getConversationPlayListFromGrammarList:(NSArray*)selectedGrammarList inBook:(Book*)inBook {
    // build list of conversations to play
    NSMutableArray *conversationPlayList = [NSMutableArray arrayWithCapacity:5];
    
    // load selected conversations from users profile list of chapters+grammar
    for (int c=0; c < inBook.chapters.count; c++) {
        Chapter *myChapter = [inBook.chapters objectAtIndex:c];
        for (int i=0; i < myChapter.conversations.count; i++) {
            // check each grammar for this conversation
            // conversation must contain all selected grammars for the chapter to be eligible?
            Conversation *thisConversation = [myChapter.conversations objectAtIndex:i];
            NSArray *grammarList = [thisConversation getGrammarListAsArray];
            BOOL containsAll = YES;
            for (int g=0; g < grammarList.count; g++) {
                // if this grammar key exists in our selected list
                NSString *grammarKey = [NSString stringWithFormat:@"%@\n%@\n%@", inBook.uniqueKey, myChapter.uniqueKey, [grammarList objectAtIndex:g]];
                BOOL found = NO;
                
                // see if this grammar is in our selected list
                for (int s=0; s < selectedGrammarList.count; s++) {
                    if ([[selectedGrammarList objectAtIndex:s] isEqualToString:grammarKey]) {
                        found = YES;
                        break;
                    }
                }
                if (!found) {
                    containsAll = NO;
                    break;
                }
            }
            // if contains all grammar, ok
            if (containsAll && grammarList.count != 0) {
                // add this conversation to play list
                NSString *conversationKey = [NSString stringWithFormat:@"%@\n%@\n%@", inBook.uniqueKey, myChapter.uniqueKey, thisConversation.uniqueKey];
                [conversationPlayList addObject:conversationKey];
            }
        }
    }
    
    // return list of conversations
    return conversationPlayList;
}

-(void)updateCurrentPlayPart {
	// get application delegate
    McpAppDelegate *appDelegate = (McpAppDelegate*)[[UIApplication sharedApplication] delegate];
    
    // get user profile
    UserProfile *userProfile = appDelegate.currentUserProfile;
    
    // convert selected play part (0=auto, 1=all, 2=none)
    // to current play part (-1 = all, -2 = none, other = index of part)
    switch (userProfile.gameOptions.playPart) {
        case 0: // auto
            // find part that has the most of the selected grammar
            currentPlayPart = [currentConversation getBestCharacterIndexFromSelectedPractice:userProfile.gameData.selectedPractice];
            break;
        case 1: // all
            currentPlayPart = -1;
            break;
        case 2: // none
            currentPlayPart = -2;
            break;
        default: // default to all
            currentPlayPart = -1;
            break;
    }
}

-(Conversation*)getConversationFromKey:(NSString*)inKey {
	// extract chapter and conversation name
//	NSString *bookName = (NSString*)[[inKey componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString: @"\n"]] objectAtIndex:0];
	NSString *chapterName = (NSString*)[[inKey componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString: @"\n"]] objectAtIndex:1];
	NSString *conversationName = (NSString*)[[inKey componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString: @"\n"]] objectAtIndex:2];
	
	// find chapter and conversation
	Conversation *foundConversation = nil;
	for (int c=0; c < book.chapters.count; c++) {
		if ([[[book.chapters objectAtIndex:c] uniqueKey] isEqualToString:chapterName]) {
			Chapter *thisChapter = [book.chapters objectAtIndex:c];
			for (int i=0; i < thisChapter.conversations.count; i++) {
				if ([[[thisChapter.conversations objectAtIndex:i] uniqueKey] isEqualToString:conversationName]) {
					foundConversation = [thisChapter.conversations objectAtIndex:i];
				}
			}
		}
	}
	
	return foundConversation;
}

-(NSString*)getCurrentLineAsHtmlWithTextAs:(int)showTextAs linkDictionary:(BOOL)inLinkDictionary {
    // ipad?
    BOOL isIpad = NO;
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 30200
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        isIpad = YES;
#endif
    
    // make furigana larger
    // make highlighted text larger
    int highlightedFontSize = 18;
    int fontSize = 18;
    if (isIpad) {
        fontSize = 24;
        highlightedFontSize = 32;
    }
    
	// define styles
	NSString *htmlString = @"<html>";
	htmlString = [htmlString stringByAppendingFormat:@"<style>span.first {color:white;text-shadow:1px 1px 0px #000;font-family='helvetica';font-size:%dpx;font-weight:bold;}</style><style>span.second {color:gray;text-shadow:1px 1px 0px #000;font-family='helvetica';font-size:17px;font-weight:bold;}</style>", highlightedFontSize];
	htmlString = [htmlString stringByAppendingFormat:@"<style>span.second {color:gray;text-shadow:1px 1px 0px #000;font-family='helvetica';font-size:%dpx;font-weight:bold;}</style>", fontSize];
//	htmlString = [htmlString stringByAppendingFormat:@"<style>a {color:inherit;text-shadow:1px 1px 0px #000;font-family='helvetica';font-size:%dpx;font-weight:bold;}</style>", highlightedFontSize];
	htmlString = [htmlString stringByAppendingFormat:@"<style>a {color:inherit;text-shadow:1px 1px 0px #000;font-family='helvetica';font-weight:bold;}</style>"];
	htmlString = [htmlString stringByAppendingString:@"<body><p>"];
	
	// loop through segments
	int thisSegmentIndex = 0;
	NSString *segmentText;
	while ((segmentText = [self getSegmentAtIndex:thisSegmentIndex forSpeech:NO]) != nil) {
		// if this is the current segment, display in white
        NSString *spanClass;
		if (thisSegmentIndex == currentSegmentIndex)
            spanClass = @"first";
        else
            spanClass = @"second";
        
        // add span
        htmlString = [htmlString stringByAppendingFormat:@"<span class='%@'>", spanClass];

        // take segment and convert to new format
        if (inLinkDictionary)
            segmentText = [HtmlFunctions convertHtml2:segmentText toHtmlAs:showTextAs];
        else
            segmentText = [HtmlFunctions convertHtml:segmentText toHtmlAs:showTextAs];
        
        // now add this new segment to the html
        htmlString = [htmlString stringByAppendingFormat:@"%@", segmentText];
        
        // close span
        htmlString = [htmlString stringByAppendingFormat:@"</span>"];
        
		// go to next segment
		thisSegmentIndex++;
	}
	
	// end html body
	htmlString = [htmlString stringByAppendingString:@"</p></body></html>"];

	return htmlString;
}

-(NSString*)getCurrentSegmentAsTextForSpeech:(BOOL)inForSpeech {
    return [self getSegmentAsTextAtIndex:currentSegmentIndex forSpeech:inForSpeech];
}

-(NSString*)getSegmentAsTextAtIndex:(int)inIndex forSpeech:(BOOL)inForSpeech {
	// first get whole string
	NSString *segment = [self getSegmentAtIndex:inIndex forSpeech:inForSpeech];
	
    // return as text
    return [HtmlFunctions stripHtmlTagsFromHtml:segment];
}

-(float)getRunningScorePercentage {
	// total score
	return totalScorePercentage/scoreSegmentCount;
}

-(void)addPercentageToScore:(float)inPercentage {
	// increment score and segment counter
	totalScorePercentage += inPercentage;
	scoreSegmentCount++;
}

-(BOOL)retryAfterWrongAnswer {
    return YES;
}

-(void)registerRightAnswer {
}

-(float)gradeCurrentSegmentUsingBuffer:(void*)inBuffer ofSize:(int)inBufferSize calibrationMode:(BOOL)inCalibrationMode sampleRate:(int)inSampleRate {
    // return grade
    return [VoiceFunctions gradeBuffer:inBuffer ofSize:inBufferSize againstText:[self getCurrentSegmentAsTextForSpeech:NO] calibrationMode:inCalibrationMode sampleRate:inSampleRate];
    
    // get application delegate
    McpAppDelegate *appDelegate = (McpAppDelegate*)[[UIApplication sharedApplication] delegate];
    
	// get documents directory
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	
	// first convert text to speech file
	// eventually just output to buffer
	NSString *testText = [self getCurrentSegmentAsTextForSpeech:YES];
	const char* cc_testText = [testText cStringUsingEncoding:-2147481087];
	NSString *filename = @"/test000.wav";
	NSString *file1Path = [documentsDirectory stringByAppendingString:filename];
	const char* cc_file_path=[file1Path UTF8String];
    
    // strip punctuation from text to get only sounds and use this for quantize count
    testText = [testText stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@".,!?？！。、ょゃュ"]];
    int quantizeTestCount = testText.length*12;
	
	// male or female voice?
	int result;
    int format = VT_FILE_API_FMT_S16PCM_WAVE;
	if (appDelegate.currentUserProfile.gender == 0)
		result = VT_TextToFile_JPN_Show(format, (char*)cc_testText, (char*)cc_file_path, -1, appDelegate.currentUserProfile.voicePitch, 100, 100, 0, -1, -1);
	else
		result = VT_TextToFile_JPN_Misaki(format, (char*)cc_testText, (char*)cc_file_path, -1, appDelegate.currentUserProfile.voicePitch, 100, 100, 0, -1, -1);
	if (result != 1) {
		NSException *exception = [NSException exceptionWithName:@"DatabaseError" reason:@"Cannot open speech database" userInfo:nil];
		@throw exception;
	}
	
    // RUN THROUGH APPLE AUPEAKLIMITER TO REMOVE CLIPPING!
    /* no peak limiter in IOS
    // Error checking result
	OSStatus res = noErr;
    AUGraph myGraph;
    res = NewAUGraph(&myGraph);
    AUNode inNode;
    AUNode outNode;
    
    // create au peak limiter node
    AudioComponentDescription plDescription;
    plDescription.componentType = kAudioUnitType_Effect;
	plDescription.componentSubType = kAudioUnitSubType_AU3DMixerEmbedded;
	plDescription.componentFlags = 0;
	plDescription.componentFlagsMask = 0;
	plDescription.componentManufacturer = kAudioUnitManufacturer_Apple;
    
    
    DisposeAUGraph(myGraph);
     */
    
//#define TEST_CASE
    
    // test using pre-set samples
    file1Path = [[NSBundle mainBundle] pathForResource:@"ray_su_mi_ma_se_n_aud.wav" ofType:@""];
//    NSString *file2Path = [[NSBundle mainBundle] pathForResource:@"yumi_aiueo.wav" ofType:@""];
    NSString *file2Path = [[NSBundle mainBundle] pathForResource:@"ray_sumimasen_aud.wav" ofType:@""];
	
	// get data for speech file
	int file1SampleCount = [self getSampleCountForFile:file1Path];
	float *file1Data = malloc(sizeof(float)*file1SampleCount);
	[self getDataIntoFloatBuffer:file1Data ofLength:file1SampleCount fromFile:file1Path];
    
	// get data for speech file 2
	int file2SampleCount = [self getSampleCountForFile:file2Path];
	float *file2Data = malloc(sizeof(float)*file2SampleCount);
	[self getDataIntoFloatBuffer:file2Data ofLength:file2SampleCount fromFile:file2Path];
    
    // if we are in calibration mode, add to training data
    if (inCalibrationMode) {
        // get correct training data depending on audio device
        NewTrainingData *myTrainingData = [appDelegate.currentUserProfile getTrainingDataForAudioDevice];
        
        // add time matched data to history
        NSString *sampleKey = [self getCurrentSegmentAsTextForSpeech:NO];
        // find the name of the phoneme
        sampleKey = [TextFunctions stripPunctuationFromString:sampleKey];
        
        NewTrainingSample *newSample = [[NewTrainingSample alloc] initWithText:sampleKey featureCount:1 size:inBufferSize/sizeof(float) userFeatures:inBuffer cpuFeatures:inBuffer];
        
        [myTrainingData.trainingSamples setObject:newSample forKey:sampleKey];
        [newSample release];
        
        // find total number of samples
        NSEnumerator *enumerator = [myTrainingData.trainingSamples keyEnumerator];
        NSString *nextKey;
        int totalSampleCount = 0;
        while ((nextKey = [enumerator nextObject])) {
            NewTrainingSample *thisSample = [myTrainingData.trainingSamples objectForKey:nextKey];
            totalSampleCount += thisSample.size;
        }
    }    
    
    // if not calibration mode, build words from user samples
    if (!inCalibrationMode) {
        // get correct training data depending on audio device
        NewTrainingData *myTrainingData = [appDelegate.currentUserProfile getTrainingDataForAudioDevice];
        
        // get sentence as kana
        NSString *kana = [HtmlFunctions convertHtml:[self getCurrentSegmentAsTextForSpeech:NO] toHtmlAs:2];
        kana = [TextFunctions katakanaToHiragana:kana];
        NSString *thisSentence = [TextFunctions stripPunctuationFromString:kana];
        
        // first find total length
        int buildLength = 0;
        for (int i=0; i < thisSentence.length; i++) {
            // get next phoneme
            NSString *thisPhoneme = [thisSentence substringWithRange:NSMakeRange(i, 1)];
            if (i < thisSentence.length-1) {
                // test next character for compound
                NSString *nextPhoneme = [thisSentence substringWithRange:NSMakeRange(i+1, 1)];
                NSRange found = [@"ゃゅょ" rangeOfString:nextPhoneme];
                if (found.location != NSNotFound)
                    thisPhoneme = [thisPhoneme stringByAppendingString:nextPhoneme];
            }
            
            // find training sample and add to length
            NewTrainingSample *thisSample = [myTrainingData.trainingSamples objectForKey:thisPhoneme];
            buildLength += thisSample.size;
        }
        
        // create new sample space and copy stuff across
        int featureCount = [VoiceSample getFeatureCount];
        float *buildSample = malloc(sizeof(float)*1*buildLength);
        
        // build our own sample
        // first find total length
        int buildPos = 0;
        for (int i=0; i < thisSentence.length; i++) {
            // find phoneme
            NSString *thisPhoneme = [thisSentence substringWithRange:NSMakeRange(i, 1)];
            if (i < thisSentence.length-1) {
                // test next character for compound
                NSString *nextPhoneme = [thisSentence substringWithRange:NSMakeRange(i+1, 1)];
                NSRange found = [@"ゃゅょ" rangeOfString:nextPhoneme];
                if (found.location != NSNotFound)
                    thisPhoneme = [thisPhoneme stringByAppendingString:nextPhoneme];
            }
            
            // get sample for phoneme
            NewTrainingSample *thisSample = [myTrainingData.trainingSamples objectForKey:thisPhoneme];
            
            if (thisSample == 0) {
                int w=0;
            }
            else {
                // find trim... do this during recording later!
                float lead = 0.0f;
                float tail = 1.0f;
                [VoiceSample getLead:&lead andTail:&tail fromBuffer:thisSample.userFeatures ofLength:thisSample.size overrideNoiseLevel:0.1f];
                
                // add this phoneme to build sample
                int start = (int)((float)thisSample.size*lead);
                int end = (int)((float)thisSample.size*tail);
                memcpy(&buildSample[buildPos], &thisSample.userFeatures[start], (end-start)*sizeof(float));
                buildPos += end-start;
            }
        }
        
        // replace existing data with ours
        free(file1Data);
        file1Data = buildSample;
        buildSample = 0;
        file1SampleCount = buildPos;
    }
    
    
    
    
    // don't need to down convert anymore because it was recorded at this bitrate
    /*
    // if our sample came in at 8k convert tts to 8k, 8-bit
    if (inSampleRate == 8000) {
        for (int i=0; i < file1SampleCount/2; i++)
            file1Data[i] = roundf(file1Data[i*2]*255.0f)/255.0f;
        file1SampleCount /= 2;
    }
     */
    
    // write raw (without high pass) of compare sample
    [ConverseViewController writeBuffer:file1Data ofLength:sizeof(float)*file1SampleCount ToWavFile:@"compare1_nocms_raw.wav" withSampleRate:inSampleRate];
	
	// normalize buffers
//	[self normalizeFloatArray:inBuffer ofSize:inBufferSize/sizeof(float)];
	
	// normalize buffers
	[self normalizeFloatArray:file1Data ofSize:file1SampleCount];
	
	// calculate quantize size using fft window size with a little overlap
	// then round up to make divisible my compare segment size
	int fftWindowSize = [VoiceSample getN];
	fftWindowSize = (float)fftWindowSize*0.66f; // overlap by 2/3
//	fftWindowSize = (float)fftWindowSize*0.66f; // overlap by 1/3
	int quantizeCount = round((float)file1SampleCount/(float)fftWindowSize);
    quantizeCount = quantizeTestCount;
	printf("quantize count is %d\r\n", quantizeCount);

#ifdef TEST_CASE
    // do voice sample first so we can equalize bands the same
	VoiceSample *noCmsSample2 = [[VoiceSample alloc] initWithBuffer:file2Data ofLength:file2SampleCount quantizeTo:quantizeCount fromUser:appDelegate.currentUserProfile noiseLevel:0.1f cepstralHistory:userCepstralHistory matchSpectrum:nil sampleRate:inSampleRate];
	VoiceSample *noCmsSample = [[VoiceSample alloc] initWithBuffer:file1Data ofLength:file1SampleCount quantizeTo:quantizeCount fromUser:appDelegate.currentCpuProfile noiseLevel:0.0f cepstralHistory:cpuCepstralHistory matchSpectrum:[noCmsSample2 getSpectrumBuffer] sampleRate:inSampleRate];
#else
    // do voice sample first so we can equalize bands the same
	VoiceSample *noCmsSample2 = [[VoiceSample alloc] initWithBuffer:inBuffer ofLength:inBufferSize/sizeof(float) quantizeTo:quantizeCount fromUser:appDelegate.currentUserProfile noiseLevel:0.1f cepstralHistory:userCepstralHistory matchSpectrum:nil sampleRate:inSampleRate];
	VoiceSample *noCmsSample = [[VoiceSample alloc] initWithBuffer:file1Data ofLength:file1SampleCount quantizeTo:quantizeCount fromUser:appDelegate.currentCpuProfile noiseLevel:0.0f cepstralHistory:cpuCepstralHistory matchSpectrum:[noCmsSample2 getSpectrumBuffer] sampleRate:inSampleRate];
#endif
    
    // compare samples
	float noCmsScore = [noCmsSample compareWithVoiceSample:noCmsSample2 filenameSuffix:@"_nocms" forText:[self getCurrentSegmentAsTextForSpeech:NO] calibrationMode:inCalibrationMode writeDebug:YES];
	printf("score=%f\r\n", noCmsScore);
	[noCmsSample release];
	[noCmsSample2 release];
	
	// free file buffers
	free(file1Data);
	
	return noCmsScore;
}

-(BOOL)isGetUserInput {
    return [self isPlayerPart];
}

-(void) normalizeFloatArray:(float*)inArray ofSize:(int)inSize {
	float max = 0;
	float min = INT_MAX;
	for (int i=0; i < inSize; i++) {
		if (fabs(inArray[i]) > max)
			max = fabs(inArray[i]);
		if (fabs(inArray[i]) < min)
			min = fabs(inArray[i]);
	}
	for (int i=0; i < inSize; i++) {
		if (inArray[i] > 0)
			inArray[i] = fabs(inArray[i]) / max;
		else 
			inArray[i] = 0 - (fabs(inArray[i]) / max);
	}
}

-(UInt32)getSampleCountForFile:(NSString*)inFilePath {
	// open file
	AudioFileID fileId;
	CFURLRef fileUrl = CFURLCreateWithFileSystemPath(nil, (CFStringRef)inFilePath, kCFURLPOSIXPathStyle, NO);
	OSStatus status = AudioFileOpenURL(fileUrl, kAudioFileReadPermission, 0, &fileId);
	
	// get wave file data format and size
	AudioStreamBasicDescription dataFormat;
	UInt32 dataSize = sizeof(dataFormat);
	status = AudioFileGetProperty(fileId, kAudioFilePropertyDataFormat, &dataSize, &dataFormat);
	UInt64 byteCount = 0;
	UInt32 dataSize2 = sizeof(byteCount);
	status = AudioFileGetProperty(fileId, kAudioFilePropertyAudioDataByteCount, &dataSize2, &byteCount);
	
	// close file again
	AudioFileClose(fileId);
	
	// return data size
	return (UInt32)byteCount/dataFormat.mBytesPerPacket;
}

-(void)getDataIntoFloatBuffer:(float*)inBuffer ofLength:(int)inLength fromFile:(NSString*)inFilePath {
	// open file
	AudioFileID fileId;
	CFURLRef fileUrl = CFURLCreateWithFileSystemPath(nil, (CFStringRef)inFilePath, kCFURLPOSIXPathStyle, NO);
	OSStatus status = AudioFileOpenURL(fileUrl, kAudioFileReadPermission, 0, &fileId);
	
	// allocate audio buffer
    //	float *audioBuffer = malloc(inLength*sizeof(float));
	short *audioBuffer = malloc(inLength*sizeof(short));
	
	// read bytes from file
	UInt32 byteCount2 = inLength*sizeof(short);
    //	UInt32 byteCount2 = inLength*sizeof(float);
	status = AudioFileReadBytes(fileId, NO, 0, &byteCount2, audioBuffer);
	for (int i=0; i < inLength; i++)
		inBuffer[i] = audioBuffer[i];
	
	// free audio buffer
	free(audioBuffer);
	
	// normalize float data
	[self normalizeFloatArray:inBuffer ofSize:inLength];
	
	// close file again
	AudioFileClose(fileId);
}

-(void)getDataIntoFloatBuffer2:(float*)inBuffer ofLength:(int)inLength fromFile:(NSString*)inFilePath {
	// open file
	AudioFileID fileId;
	CFURLRef fileUrl = CFURLCreateWithFileSystemPath(nil, (CFStringRef)inFilePath, kCFURLPOSIXPathStyle, NO);
	OSStatus status = AudioFileOpenURL(fileUrl, kAudioFileReadPermission, 0, &fileId);
	
	// allocate audio buffer
    float *audioBuffer = malloc(inLength*sizeof(float));
	
	// read bytes from file
    UInt32 byteCount2 = inLength*sizeof(float);
	status = AudioFileReadBytes(fileId, NO, 0, &byteCount2, audioBuffer);
	for (int i=0; i < inLength; i++)
		inBuffer[i] = audioBuffer[i];
	
	// free audio buffer
	free(audioBuffer);
	
	// normalize float data
	[self normalizeFloatArray:inBuffer ofSize:inLength];
	
	// close file again
	AudioFileClose(fileId);
}


-(int)getSegmentCount {
	// get this line
	DialogueLine *thisLine = [currentConversation.dialogueLines objectAtIndex:currentLineIndex];
	
	// loop through each character
	int thisSegmentIndex = 0;
	bool insideBrackets = NO;
	for (int i=0; i < [thisLine.line length]; i++) {
		// check for being inside bracket
		if ([@"<" rangeOfString:[thisLine.line substringWithRange:NSMakeRange(i, 1)]].location != NSNotFound) {
			// set flag
			insideBrackets = YES;
		}
		else {
			if ([@">" rangeOfString:[thisLine.line substringWithRange:NSMakeRange(i, 1)]].location != NSNotFound) {
				// set flag
				insideBrackets = NO;
			}
		}
		
		// if we meet seperator, increment index
		if (!insideBrackets && [@"/,.:;!?。、／；：！？" rangeOfString:[thisLine.line substringWithRange:NSMakeRange(i, 1)]].location != NSNotFound) {
			thisSegmentIndex++;
		}
	}
    
    return thisSegmentIndex;
}

-(NSString*)getSegmentAtIndex:(int)inIndex forSpeech:(BOOL)inForSpeech {
	// get this line
	DialogueLine *thisLine = [currentConversation.dialogueLines objectAtIndex:currentLineIndex];
	NSString *thisSegment = @"";
	
	// loop through each character
	int thisSegmentIndex = 0;
	bool insideBrackets = NO;
	for (int i=0; i < [thisLine.line length]; i++) {
		// check for being inside bracket
		if ([@"<" rangeOfString:[thisLine.line substringWithRange:NSMakeRange(i, 1)]].location != NSNotFound) {
			// set flag
			insideBrackets = YES;
		}
		else {
			if ([@">" rangeOfString:[thisLine.line substringWithRange:NSMakeRange(i, 1)]].location != NSNotFound) {
				// set flag
				insideBrackets = NO;
			}
		}
		
		// if this segment is the one we're looking for then add to string
		if (thisSegmentIndex == inIndex) {
			// if not pause marker, add to string
			if (insideBrackets)
				thisSegment = [thisSegment stringByAppendingString:[thisLine.line substringWithRange:NSMakeRange(i, 1)]];
			else {
				if ([@"/／" rangeOfString:[thisLine.line substringWithRange:NSMakeRange(i, 1)]].location == NSNotFound)
					thisSegment = [thisSegment stringByAppendingString:[thisLine.line substringWithRange:NSMakeRange(i, 1)]];
			}
		}
		
		// if we meet seperator, increment index
		if (!insideBrackets && [@"/,.:;!?。、／；：！？" rangeOfString:[thisLine.line substringWithRange:NSMakeRange(i, 1)]].location != NSNotFound) {
			thisSegmentIndex++;
		}
	}

	if ([[thisSegment stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""])
		return nil;
	else
		return thisSegment;
}

-(int)getCurrentCharacterIndex {
	DialogueLine *thisLine = [currentConversation.dialogueLines objectAtIndex:currentLineIndex];
	return thisLine.characterIndex;
}

-(Actor*)getCurrentActor {
	DialogueLine *thisLine = [currentConversation.dialogueLines objectAtIndex:currentLineIndex];
    return [currentActors objectAtIndex:thisLine.characterIndex];
}

-(Actor*)getTalkingToActor {
    if (currentActors.count == 0)
        return nil;
    else
        return [currentActors objectAtIndex:[self getTalkingToCharacterIndex]];
}

// get index of the character we're talking to
-(int)getTalkingToCharacterIndex {
    // if this is not us, just return the actor who is speaking
    DialogueLine *thisLine;
    if (![self isPlayerPart])
        thisLine = [currentConversation.dialogueLines objectAtIndex:currentLineIndex];
    else
        thisLine = [currentConversation.dialogueLines objectAtIndex:currentLineIndex==0?1:currentLineIndex-1];
	return thisLine.characterIndex;
}

-(NSString*)getCurrentCharacterVoiceName {
	Character *thisChar = [currentConversation.characters objectAtIndex:[self getCurrentCharacterIndex]];
	Actor *myActor = [[Actor alloc] initFromArchive:thisChar.actorName];
	NSString *voiceName = [myActor.speechVoice retain];
	[myActor release];
	
	return [voiceName autorelease];
}

-(int)getCurrentCharacterVoicePitch {
	Character *thisChar = [currentConversation.characters objectAtIndex:[self getCurrentCharacterIndex]];
	Actor *myActor = [[Actor alloc] initFromArchive:thisChar.actorName];
	int voicePitch = myActor.speechPitch;
	[myActor release];
	
	return voicePitch;
}

-(BOOL)isPlayerPart {
    // get current actor index
    DialogueLine *thisLine = [currentConversation.dialogueLines objectAtIndex:currentLineIndex];
    int thisActorIndex = thisLine.characterIndex;

    // which part are we currently playing
    switch (currentPlayPart) {
        case -1: // all
            return YES;
            break;
        case -2: // none
            return NO;
            break;
        default: // default is the actual index
            if (currentPlayPart == thisActorIndex)
                return YES;
            else
                return NO;
            break;
    }
    
    // just in case it falls through
    return NO;
}

-(UIImage*)getBackgroundImage {
	// get background image
	Background *myBackground = [[Background alloc] initFromArchive:currentConversation.backgroundName];
	UIImage *myImage = [myBackground.image retain];
	[myBackground release];
	
	return [myImage autorelease];
}

-(int)getCharacterCount {
	return currentConversation.characters.count;
}

-(UIImage*)getCharacterImageAtIndex:(int)inIndex {
	// get image
	Character *myChar = [currentConversation.characters objectAtIndex:inIndex];
	Actor *myActor = [[Actor alloc] initFromArchive:myChar.actorName];
//	UIImage *charImage = [myActor.image retain];
	UIImage *charImage = [[myActor.animationFrames objectAtIndex:10] retain];
	[myActor release];
	
	return [charImage autorelease];
}

-(BOOL)goToNextSegment {
	// increment segment index
	if ([self getSegmentAtIndex:currentSegmentIndex+1 forSpeech:NO] != nil) {
		currentSegmentIndex++;
		return YES;
	}
	return NO;
}

-(BOOL)goToNextLine {
	// increment line index
	if (currentLineIndex+1 < currentConversation.dialogueLines.count) {
		currentLineIndex++;
		currentSegmentIndex = 0;
		return YES;
	}
	else {
		return NO;
	}
}

-(BOOL)goToNextConversation {
	// go to next conversation
	if (currentConversationIndex+1 < conversationPlayList.count)
		currentConversationIndex++;
	else
		currentConversationIndex = 0;
	self.currentConversation = [self getConversationFromKey:[conversationPlayList objectAtIndex:currentConversationIndex]];
    
    // load actors
    [self loadActors];
    
	// just reset line and segment index for now
	currentLineIndex = 0;
	currentSegmentIndex = 0;
	
	// reset score
	totalScorePercentage = 0;
	scoreSegmentCount = 0;
    
    // get current play part
    [self updateCurrentPlayPart];
	
	return YES;
}

-(BOOL)goToPreviousSegment {
	// decrement segment index
    if (currentSegmentIndex > 0) {
		currentSegmentIndex--;
		return YES;
	}
	return NO;
}

-(BOOL)goToPreviousLine {
	// decrement line index
    if (currentLineIndex > 0) {
		currentLineIndex--;
		currentSegmentIndex = [self getSegmentCount]-1;
		return YES;
	}
	else {
		return NO;
	}
}

-(BOOL)goToPreviousConversation {
	// go to previous conversation
    if (currentConversationIndex > 0)
        currentConversationIndex--;
    else
        currentConversationIndex = conversationPlayList.count-1;
	self.currentConversation = [self getConversationFromKey:[conversationPlayList objectAtIndex:currentConversationIndex]];
    
    // load the actors
    [self loadActors];
    
	// just reset line and segment index for now
	currentLineIndex = 0;
	currentSegmentIndex = 0;
	
	// reset score
	totalScorePercentage = 0;
	scoreSegmentCount = 0;
    
    // get current play part
    [self updateCurrentPlayPart];
	
	return YES;
}

-(void)dealloc {
    [currentActors release];
	[currentConversation release];
	[conversationPlayList release];
	[book release];
    [super dealloc];
}

@end
