//
//  AudioPlayer.m
//  Mcp
//
//  Created by Ray on 4/18/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "AudioPlayer.h"


@implementation AudioPlayer

@synthesize playState;

-(id)init {
    if (self = [super init]) {
        // setup play state information
        int sampleRate = 16000;
        playState.dataFormat.mSampleRate = sampleRate;
        playState.dataFormat.mFormatID = kAudioFormatLinearPCM;
        playState.dataFormat.mFramesPerPacket = 1;
        playState.dataFormat.mChannelsPerFrame = 1;
        playState.dataFormat.mBytesPerFrame = 4;
        playState.dataFormat.mBytesPerPacket = 4;
        playState.dataFormat.mBitsPerChannel = 32;
        playState.dataFormat.mReserved = 0;
        playState.dataFormat.mFormatFlags = kLinearPCMFormatFlagIsFloat|kLinearPCMFormatFlagIsPacked;
        
        // create output queue
        OSStatus status = AudioQueueNewOutput(&playState.dataFormat, AudioOutputCallback, &playState, CFRunLoopGetCurrent(), kCFRunLoopCommonModes, 0, &playState.queue);
        
        // allocate buffers, but don't queue yet
        for (int i=0; i < NUM_BUFFERS; i++)
        {
            int BUFFER_SIZE = sampleRate;
            status = AudioQueueAllocateBuffer(playState.queue, BUFFER_SIZE*4, &playState.buffers[i]);
        }	
        playState.playBuffer = 0;
        playState.playBufferSize = 0;
        playState.playBufferPos = 0;
        
        // playing stopped to start
        playState.state = AUDIOPLAYER_STATE_STOPPED;
    }
    return self;
}

-(int)getPlayBufferPos {
    return playState.playBufferPos;
}

-(void*)getPlayBuffer {
    return playState.playBuffer;
}

-(BOOL)startPlayingBuffer:(void*)inBuffer ofSize:(int)inBufferSize {
    // free existing buffer space if we have it
    if (playState.playBuffer != 0)
        free(playState.playBuffer);
    
    // copy buffer to our own  buffer
    playState.playBuffer = malloc(sizeof(float)*inBufferSize);
    playState.playBufferSize = inBufferSize;
    memcpy(playState.playBuffer, inBuffer, inBufferSize);
    
    
    // fill the first three buffers and queue
    int BUFFER_SIZE = playState.dataFormat.mSampleRate;
    playState.playBufferPos = 0;
    for (int i=0; i < NUM_BUFFERS; i++)
    {
        if (playState.playBufferPos < playState.playBufferSize)
        {
            memcpy(playState.buffers[i]->mAudioData, playState.playBuffer+playState.playBufferPos, BUFFER_SIZE*4);
            playState.buffers[i]->mAudioDataByteSize = BUFFER_SIZE*4;
            playState.playBufferPos += BUFFER_SIZE*4;
            OSStatus status2=AudioQueueEnqueueBuffer(playState.queue, playState.buffers[i], 0, NULL);
            NSLog(@"Play queue buffer result code %ld", status2);
        }
    }
    
    // start audio playing
    playState.state = AUDIOPLAYER_STATE_PLAYING;
    AudioQueueStart(playState.queue, NULL);
    
    return YES;
}

-(void)dealloc {
	AudioQueueDispose(playState.queue, TRUE);
	free(playState.playBuffer);
	[super dealloc];
}

@end

void AudioOutputCallback(void *inUserData, AudioQueueRef inAQ, AudioQueueBufferRef inBuffer)
{
    // get user data (play state)
	PlayState *myPlayState = (PlayState*)inUserData;
    
	// fill data and requeue buffer
    int BUFFER_SIZE = myPlayState->dataFormat.mSampleRate;
	if (myPlayState->playBufferPos < myPlayState->playBufferSize)
	{
        NSLog(@"Queue next playback buffer from position %d", myPlayState->playBufferPos);
		
		memcpy(inBuffer->mAudioData, myPlayState->playBuffer+myPlayState->playBufferPos, BUFFER_SIZE*4);
		myPlayState->playBufferPos += BUFFER_SIZE*4;
		AudioQueueEnqueueBuffer(myPlayState->queue, inBuffer, 0, NULL);
	}
	else {
		if (myPlayState->state == AUDIOPLAYER_STATE_PLAYING)
		{
			// stop playback, but not until finished
            NSLog(@"Stop audio playback, but wait until finished");
			myPlayState->state = AUDIOPLAYER_STATE_STOPPED;
			AudioQueueStop(myPlayState->queue, false);
		}
/*		if (myPlayState->state == AUDIOPLAYER_STATE_PLAYING)
		{
            //			[[NSString stringWithFormat:@"Played all of recording buffer playposition now %d\n", playBufferPos] writeToFile:@"/dev/stdout" atomically:NO];
			
			AudioTimeStamp myTimeStamp;
			Boolean discontinuity;
			AudioQueueGetCurrentTime(inAQ, NULL, &myTimeStamp, &discontinuity);
			
			// wait for queue to stop, then call my proc
            //			OSStatus status = AudioQueueAddPropertyListener(playState->queue, kAudioQueueProperty_IsRunning, PlaybackFinishedCallback, NULL);
			
		}*/
	}
}

/*
 
 void PlaybackFinishedCallback(void *inUserData, AudioQueueRef inAQ, AudioQueuePropertyID inID)
 {
 [[NSString stringWithFormat:@"Inside playback finished\n"] writeToFile:@"/dev/stdout" atomically:NO];
 // if playback has finished
 
 
 UInt32 isRunning;
 UInt32 isRunningSize=sizeof(isRunning);
 OSStatus status = AudioQueueGetProperty(inAQ, kAudioQueueProperty_IsRunning, &isRunning, &isRunningSize);
 [[NSString stringWithFormat:@"Isrunning returned as %d\n", isRunning] writeToFile:@"/dev/stdout" atomically:NO];
 if (isRunning == 0)
 {
 // start recording again
 AudioQueueStop(recordState.queue, false);
 isPlaying = NO;
 
 }
 }
*/

