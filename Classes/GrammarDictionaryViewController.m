//
//  GrammarListViewController.m
//  Mcp
//
//  Created by Ray on 1/7/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "GrammarDictionaryViewController.h"
#import "EditGrammarController.h"
#import "GrammarNote.h"
#import "McpAppDelegate.h"
#import "GrammarViewController.h"
#import "TextFunctions.h"
#import "Book.h"
#import "Chapter.h"
#import "RomajiPath.h"

@implementation GrammarDictionaryViewController

/*
@synthesize browseTableView;
@synthesize mySearchBar;
@synthesize searchController;
@synthesize keyboardHideButton;
@synthesize searchToolbar;
@synthesize romajiSwitch;
@synthesize ftsSwitch;
*/

#pragma mark -
#pragma mark View lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
	
	// set title
	self.title = @"Grammar";
    
	// allow selecting during editor
	self.tableView.allowsSelection = YES;
	
    // create grammar notes array from all books
    grammarNotes = [[NSMutableArray alloc] init];
    // loop through books
    NSArray *bookNames = [Book getListOfFilesWithExtension:@"book"];
    for (int i=0; i < bookNames.count; i++) {
        // open book
        NSString *thisBookName = [bookNames objectAtIndex:i];
        Book *thisBook = [[Book alloc] initFromArchive:thisBookName];
        // loop through chapters
        for (int c=0; c < thisBook.chapters.count; c++) {
            // add grammar notes
            Chapter *thisChapter = [thisBook.chapters objectAtIndex:c];
            [grammarNotes addObjectsFromArray:thisChapter.grammarNotes];
        }
        // release book
        [thisBook release];
    }
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    // try searching as kana too
    NSString *kana = [[RomajiPath getSingleton] convertToHiragana:searchBar.text];
    
    // update search results
    [searchResults release];
    searchResults = [[NSMutableArray alloc] initWithCapacity:10];
    for (int i=0; i < grammarNotes.count; i++) {
        GrammarNote *thisNote = [grammarNotes objectAtIndex:i];
        NSString *grammarText = thisNote.text;
        //        NSString *grammarText = [TextFunctions convertHtml:thisNote.text toHtmlAs:1]; // furigana
        BOOL found = NO;
        if ([thisNote.title rangeOfString:searchBar.text options:NSCaseInsensitiveSearch].location != NSNotFound ||
            [grammarText rangeOfString:searchBar.text options:NSCaseInsensitiveSearch].location != NSNotFound)
            found = YES;
        if (![kana isEqualToString:@""]) {
            if ([thisNote.title rangeOfString:kana options:NSCaseInsensitiveSearch].location != NSNotFound ||
                [grammarText rangeOfString:kana options:NSCaseInsensitiveSearch].location != NSNotFound)
                found = YES;
        }
        if (found)
            [searchResults addObject:thisNote];
    }
}

-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {	
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {	
}

#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // if not search tableview, return all grammar
    if (tableView == self.tableView) {
        // how many grammar notes?
        int noteCount = grammarNotes.count;
        if (self.editing)
            return noteCount+1;
        else
        {
            if (noteCount == 0)
                return 1;
            else
                return noteCount;
        }
    }
    
    // otherwise, return search results
    return searchResults.count;
}

// Customize the appearance of table view cells.
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	// get regular table cell
    static NSString *CellIdentifier = @"GrammarCell";
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	
	// default cell config
	cell.textLabel.text = NULL;
	cell.imageView.image = NULL;
	cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
	cell.editingAccessoryType = UITableViewCellAccessoryDisclosureIndicator;
	cell.selectionStyle = UITableViewCellSelectionStyleNone;
	cell.textLabel.textColor = [UIColor blackColor];
    
	// if no cells, display 'press to add' message
	if (grammarNotes.count == 0 && !self.editing) {
		cell.textLabel.text = @"Press Edit Button To Create New Grammar Point";
		cell.textLabel.textColor = [UIColor lightGrayColor];
		cell.textLabel.adjustsFontSizeToFitWidth = YES;
	}
	else {
		// Set up the cell...
		NSUInteger row = [indexPath row];
		if (row < grammarNotes.count) {
            if (tableView == self.tableView)
                cell.textLabel.text = [[grammarNotes objectAtIndex:row] title];
            else
                cell.textLabel.text = [[searchResults objectAtIndex:row] title];
            //			cell.editingAccessoryType = UITableViewCellAccessoryDisclosureIndicator;
			cell.selectionStyle = UITableViewCellSelectionStyleBlue;
		}
		else {
			cell.textLabel.text = @"Add New";
			cell.textLabel.textColor = [UIColor lightGrayColor];
			cell.selectionStyle = UITableViewCellSelectionStyleBlue;
		}
	}
	
    return cell;
}

#pragma mark -
#pragma mark Table view delegate

/*
-(void)tableView:(UITableView*)tableView didSelectRowAtIndexPath:(NSIndexPath*) indexPath {
	// find row
	NSUInteger row = [indexPath row];
	
	// save index path
    [lastIndexPath release];
	lastIndexPath = [indexPath retain];
 
     // which grammar note?
     GrammarNote *selectedNote;
     if (tableView == self.tableView) {
     if (row < grammarNotes.count)
     selectedNote = [grammarNotes objectAtIndex:row];
     }
     else {
     if (row < searchResults.count)
     selectedNote = [searchResults objectAtIndex:row];
     }
     
	// editing mode?
	if (!self.editing) {
        // show view controller to view grammar
        GrammarViewController *viewController = [[GrammarViewController alloc] initWithNibName:@"GrammarViewController" bundle:nil];
        [viewController setViewNote:selectedNote allowSelect:NO];
        [self.navigationController pushViewController:viewController animated:YES];
        [viewController release];
    }
}
*/

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // call super first
    [super prepareForSegue:segue sender:sender];
    
    // sender should be cell
    UITableViewCell *thisCell = (UITableViewCell*)sender;
    
    // which grammar note?
    GrammarNote *selectedNote;
    if ([self.tableView indexPathForCell:thisCell] != nil) {
        int row = [self.tableView indexPathForCell:thisCell].row;
        selectedNote = [grammarNotes objectAtIndex:row];
    }
    else {
        int row = [self.searchDisplayController.searchResultsTableView indexPathForCell:thisCell].row;
        selectedNote = [searchResults objectAtIndex:row];
    }
    
    // set note
    GrammarViewController *grammarViewController = [segue destinationViewController];
    [grammarViewController setGrammarNote:selectedNote];
}

#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    // Relinquish ownership of anything that can be recreated in viewDidLoad or on demand.
    // For example: self.myOutlet = nil;
}


- (void)dealloc {
    [lastIndexPath release];
    /*
    [browseTableView release];
    [mySearchBar release];
    [searchController release];
	[keyboardHideButton release];
	[ftsSwitch release];
	[romajiSwitch release];
	[searchToolbar release];
     */
    [grammarNotes release];
    [super dealloc];
}


@end

