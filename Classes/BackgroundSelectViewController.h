//
//  BackgroundSelectViewController.h
//  Mcp
//
//  Created by Ray on 10/30/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SelectViewControllerDelegate.h"

@interface BackgroundSelectViewController : UITableViewController <UITableViewDelegate, UITableViewDataSource> {
	// delegate
	id<SelectViewControllerDelegate> selectDelegate;
	
	NSMutableArray *backgroundList; // list of NSString*
	NSIndexPath *lastIndexPath;
}

@property (nonatomic, retain) NSMutableArray *backgroundList;
@property (nonatomic, assign) id<SelectViewControllerDelegate> selectDelegate;

-(void)populateBackgroundList;
-(IBAction)selectButtonClick:(id)sender;
-(IBAction)cancelButtonClick:(id)sender;

@end