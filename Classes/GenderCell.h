//
//  GenderCell.h
//  Mcp
//
//  Created by Ray on 4/10/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface GenderCell : UITableViewCell {
	IBOutlet UISegmentedControl *genderSegmentControl;
}

@property (nonatomic, retain) UISegmentedControl *genderSegmentControl;

-(void)setGenderAs:(int)inGenderIndex;

@end
