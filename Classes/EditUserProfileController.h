//
//  EditUserProfileController.h
//  Mcp
//
//  Created by Ray on 4/9/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserProfile.h"
#import "EditableListDelegate.h"

@interface EditUserProfileController : UITableViewController <UITextFieldDelegate, UINavigationControllerDelegate,  UITableViewDelegate> {
	BOOL newMode;
	UserProfile *profile;
	id<EditableListDelegate> editableListDelegate;
}

@property (nonatomic, retain) UserProfile *profile;
@property (nonatomic, assign) id<EditableListDelegate> editableListDelegate;

-(void)setEditableObject:(UserProfile*)thisProfile isNew:(BOOL)isNew ;
-(BOOL)isOkToSave;
-(void)cancelButtonClick:(id)sender;
-(void)saveButtonClick:(id)sender;

-(void)textFieldDidChange:(NSNotification*)note;
-(void)pitchChanged:(id)sender;
-(void)genderSegmentChanged:(id)sender;

@end
