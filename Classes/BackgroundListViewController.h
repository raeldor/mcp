//
//  BackgroundListViewController.h
//  Mcp
//
//  Created by Ray on 10/19/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EditableListDelegate.h"

@interface BackgroundListViewController : UITableViewController <UITableViewDelegate, UITableViewDataSource, EditableListDelegate> {
	NSMutableArray *backgroundList; // list of NSString*
	NSIndexPath *lastIndexPath;
	NSString *oldBackgroundName;
}

@property (nonatomic, retain) NSMutableArray *backgroundList;

-(void)populateBackgroundList;

@end
