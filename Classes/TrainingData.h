//
//  TrainingData.h
//  Mcp
//
//  Created by Ray on 1/24/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Syllable.h"

@interface TrainingData : NSObject {
	NSMutableArray *trainingSentences;
	NSMutableArray *trainingSamples;
	NSMutableDictionary *phonemeIndexes;
	NSMutableArray *syllables;
	
	NSMutableArray *phonemeList;
	NSMutableDictionary *phonemeData;
}

-(id)init;
-(void)dealloc;
-(Syllable*)findSyllableFromKana:(NSString*)inKana;

@property (nonatomic, retain) NSMutableArray *trainingSamples;
@property (nonatomic, retain) NSMutableArray *trainingSentences;
@property (nonatomic, retain) NSMutableDictionary *phonemeIndexes;
@property (nonatomic, retain) NSMutableArray *syllables;

@property (nonatomic, retain) NSMutableDictionary *phonemeData;
@property (nonatomic, retain) NSMutableArray *phonemeList;

@end
