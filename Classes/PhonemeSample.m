//
//  PhonemeSample.m
//  Mcp
//
//  Created by Ray on 1/24/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "PhonemeSample.h"


@implementation PhonemeSample;

@synthesize sampleBuffer;
@synthesize sampleSize;

-(id)initWithBuffer:(float*)inBuffer ofSize:(int)inSize {
	if (self = [super init]) {
		sampleBuffer = inBuffer;
		sampleSize = inSize;
	}
	return self;
}

- (void)dealloc {
	[super dealloc];
}

@end
