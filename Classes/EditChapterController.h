//
//  EditChapterController.h
//  Mcp
//
//  Created by Ray on 12/21/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Book.h"
#import "EditableListDelegate.h"


@interface EditChapterController : UITableViewController <UITextFieldDelegate, UINavigationControllerDelegate, UIActionSheetDelegate, UIImagePickerControllerDelegate, UITableViewDelegate> {
	BOOL newMode;
	Chapter *chapter;
	id<EditableListDelegate> editableListDelegate;
	NSIndexPath *lastIndexPath;	
}

@property (nonatomic, retain) Chapter *chapter;
@property (nonatomic, assign) id<EditableListDelegate> editableListDelegate;

-(void)setEditableObject:(Chapter*)thisChapter isNew:(BOOL)isNew ;
-(BOOL)isOkToSave;
-(void)cancelButtonClick:(id)sender;
-(void)saveButtonClick:(id)sender;
-(void)textFieldDidChange:(NSNotification*)note;

@end

