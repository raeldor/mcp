//
//  DataPoint.m
//  Mcp
//
//  Created by Ray on 3/20/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "DataPoint.h"

@implementation DataPoint

@synthesize dataCount;
@synthesize visited;
@synthesize noise;
@synthesize data;
@synthesize clusterIndex;
@synthesize dataSourceIndex;

-(id)initWithData:(float*)inData count:(int)inCount dataSourceIndex:(int)inDataSourceIndex {
	if (self = [super init]) {
		// setup parameters
		dataCount = inCount;
		visited = NO;
		noise = NO;
		clusterIndex = -1;
		dataSourceIndex = inDataSourceIndex;
		
		// copy data
		data = malloc(sizeof(float)*inCount);
		memcpy(data, inData, sizeof(float)*inCount);
	}
	return self;
}

-(float)distanceFromDataPoint:(DataPoint*)inDataPoint {
	// use euclidean distance
	float total = 0.0f;
	for (int i=0; i < dataCount; i++) {
		total += pow(inDataPoint.data[i]-data[i], 2.0f);
	}
	total = sqrt(total);
	return total;
/*	
	// distance of each data point value divided by number of values
	float total = 0.0f;
	float maxDiff = 0.0f;
	for (int i=0; i < dataCount; i++) {
		float diff = 0.0f;
		if (fabs(data[i]) > fabs(inDataPoint.data[i]))
			diff = fabs(data[i]) - fabs(inDataPoint.data[i]);
		else
			diff = fabs(inDataPoint.data[i]) - fabs(data[i]);
		total += diff;
		if (diff > maxDiff)
			maxDiff = diff;
	}
	return maxDiff;*/
}

-(void)dealloc {
	free(data);
	[super dealloc];
}

@end
