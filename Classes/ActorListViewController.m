//
//  ActorListViewController.m
//  Mcp
//
//  Created by Ray on 10/21/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "ActorListViewController.h"
#import "EditActorController.h"
#import "Actor.h"
#import "ActorListCell.h"
#import "McpAppDelegate.h"

@implementation ActorListViewController

@synthesize actorList;

#pragma mark -
#pragma mark View lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
	
	// set title
	self.title = @"Actors";
	
	// allow selecting during editor
	self.tableView.allowsSelection = FALSE;
	self.tableView.allowsSelectionDuringEditing = TRUE;
	
	// add edit button
	self.navigationItem.rightBarButtonItem = self.editButtonItem;
	
	// search for deck archive files
	[self populateActorList];
}

-(void)populateActorList {
	// create new list of actors
	self.actorList = [Actor getListOfFilesWithExtension:@"actor"];
}

#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	if (self.editing)
		return actorList.count+1;
	else
	{
		if (actorList.count == 0)
			return 1;
		else
			return actorList.count;
	}
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	return 141;
}

// Customize the appearance of table view cells.
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	// get application delegate
    McpAppDelegate *appDelegate = (McpAppDelegate*)[[UIApplication sharedApplication] delegate];
    
	// get regular table cell
    static NSString *CellIdentifier = @"ActorListCell";
    ActorListCell *cell = (ActorListCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
		// load cell layout from nib
		NSArray *nib = [[NSBundle mainBundle] loadNibNamed:CellIdentifier owner:self options:nil];
		cell = [nib objectAtIndex:0];
    }
	
	// default cell config
	cell.textLabel.text = NULL;
	cell.imageView.image = NULL;
	cell.accessoryType = UITableViewCellAccessoryNone;
	cell.editingAccessoryType = UITableViewCellAccessoryNone;
	cell.selectionStyle = UITableViewCellSelectionStyleNone;
	cell.textLabel.textColor = [UIColor blackColor];
    
	// if no cells, display 'press to add' message
	if (actorList.count == 0 && !self.editing) {
		cell.textLabel.text = @"Press Edit Button To Create New Actor";
		cell.textLabel.textColor = [UIColor lightGrayColor];
		cell.textLabel.adjustsFontSizeToFitWidth = YES;
	}
	else {
		// Set up the cell...
		NSUInteger row = [indexPath row];
		if (row < actorList.count) {
			cell.nameLabel.text = [actorList objectAtIndex:row];
            Actor *actor = [appDelegate getActorUsingName:[actorList objectAtIndex:row]];
            int restFrame = [actor getActorFrameForName:@"rest"];
            NSString *filename = [NSString stringWithFormat:@"%@%05d.jpg", actor.animeCharacter, restFrame+1];
            NSString *filePath = [[NSBundle mainBundle] pathForResource:filename ofType:nil];
            cell.actorImageView.image = [UIImage imageWithContentsOfFile:filePath];
			cell.editingAccessoryType = UITableViewCellAccessoryDisclosureIndicator;
			cell.selectionStyle = UITableViewCellSelectionStyleBlue;
		}
		else {
			cell.textLabel.text = @"Add New";
			cell.textLabel.textColor = [UIColor lightGrayColor];
			cell.selectionStyle = UITableViewCellSelectionStyleBlue;
		}
	}
	
    return cell;
}

//editing mode changed
- (void)setEditing:(BOOL)editing animated:(BOOL)animated {
	// add/remove placeholder items
	NSMutableArray *indexPaths = [NSMutableArray array];
	[indexPaths addObject:[NSIndexPath indexPathForRow:actorList.count inSection:0]];
	[self.tableView beginUpdates];
	[super setEditing:editing animated:animated];
	[self.tableView setEditing:editing animated:YES];
	if (editing) {
		// delete 'press edit' message
		if (actorList.count == 0)
			[self.tableView deleteRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationTop];
		
		// Show the placeholder rows and hide back button
		[self.tableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationTop];
		self.navigationItem.hidesBackButton = YES;
	}
	else
	{
		// Hide the placeholder rows and show back button again
		[self.tableView deleteRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationTop];
		self.navigationItem.hidesBackButton = NO;
		
		// show 'press edit' message
		if (actorList.count == 0)
			[self.tableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationTop];
	}
	[self.tableView endUpdates];
}

-(UITableViewCellEditingStyle)tableView:(UITableView*)tableView editingStyleForRowAtIndexPath:(NSIndexPath*)indexPath {
	// which row are we getting
	NSUInteger row = [indexPath row];
	
	// if we are in edit mode
	if (self.editing) {
		// show delete style and insert for 'add new'
		if (row != actorList.count)
			return UITableViewCellEditingStyleDelete;
		else
			return UITableViewCellEditingStyleInsert;
	}
	return UITableViewCellEditingStyleNone;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    // if we are deleting rows
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
		[Actor	delete:[actorList objectAtIndex:[indexPath row]] extension:@".actor"];
		[actorList removeObjectAtIndex:[indexPath row]];
        [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:YES];
    }   
    else
		if (editingStyle == UITableViewCellEditingStyleInsert) {
			// edit actor using edit controller
			oldActorName = nil;
			EditActorController *editActorController = [[EditActorController alloc] initWithNibName:@"EditActorController" bundle:nil];
			Actor *newActor = [[Actor alloc] init];
			[editActorController setEditableObject:newActor isNew:YES];
			editActorController.editableListDelegate = self;
			UINavigationController *secondNavigationController = [[UINavigationController alloc] initWithRootViewController:editActorController];
			[self.navigationController presentModalViewController:secondNavigationController animated:YES];
			[newActor release];
			[editActorController release];
			[secondNavigationController release];
		}
}

-(void)didUpdateObject:(id)updatedObject {
	// if renamed, then must rename file too
	Actor *updatedActor = (Actor*)updatedObject;
	if (![oldActorName isEqualToString:[updatedActor actorName]])
		[updatedActor renameFrom:oldActorName to:[updatedActor actorName]];
	
	// update class to disk and refresh table
	[updatedActor saveAs:[updatedActor actorName]];
	[self populateActorList];
	[self.tableView reloadData];
}

-(void)didAddNewObject:(id)newObject {
	// save new class
	Actor *newActor = (Actor*)newObject;
	[newObject saveAs:[newActor actorName]];
	[self populateActorList];
	[self.tableView reloadData];
}

#pragma mark -
#pragma mark Table view delegate

-(void)tableView:(UITableView*)tableView didSelectRowAtIndexPath:(NSIndexPath*) indexPath {
	// get application delegate
    McpAppDelegate *appDelegate = (McpAppDelegate*)[[UIApplication sharedApplication] delegate];
    
	// find row
	NSUInteger row = [indexPath row];
	
	// save index path
    [lastIndexPath release];
	lastIndexPath = [indexPath retain];
	
	// editing mode?
	if (self.editing) {
		// add new?
		if (row >= [actorList count]) {
			[self tableView:tableView commitEditingStyle:UITableViewCellEditingStyleInsert forRowAtIndexPath:indexPath];
		}
		else {
			// edit actor using edit controller
			oldActorName = [actorList objectAtIndex:row];
			EditActorController *editActorController = [[EditActorController alloc] initWithNibName:@"EditActorController" bundle:nil];
//			Actor *thisActor = [[Actor alloc] initFromArchive:[actorList objectAtIndex:row]];
            Actor *thisActor = [appDelegate getActorUsingName:[actorList objectAtIndex:row]];
			[editActorController setEditableObject:thisActor isNew:NO];
			editActorController.editableListDelegate = self;
			[self.navigationController pushViewController:editActorController animated:YES];
//			[thisActor release];
			[editActorController release];
		}
	}
	else {
	}
}

#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    // Relinquish ownership of anything that can be recreated in viewDidLoad or on demand.
    // For example: self.myOutlet = nil;
}


- (void)dealloc {
    [lastIndexPath release];
	[actorList release];
    [super dealloc];
}


@end

