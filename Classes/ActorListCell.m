//
//  ActorListCell.m
//  Mcp
//
//  Created by Ray on 10/21/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "ActorListCell.h"


@implementation ActorListCell

@synthesize actorImageView;
@synthesize nameLabel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if ((self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])) {
        // Initialization code
    }
    return self;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
	
    [super setSelected:selected animated:animated];
	
    // Configure the view for the selected state
}


- (void)dealloc {
	[actorImageView release];
	[nameLabel release];
    [super dealloc];
}


@end
