/*
 *  SelectViewControllerDelegate.h
 *  Mcp
 *
 *  Created by Ray on 10/30/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */


@protocol SelectViewControllerDelegate

-(void)didMakeBrowserSelection:(NSString*)selection fromController:(UIViewController*)selectController;
-(void)didCancelBrowserSelection;

@end
