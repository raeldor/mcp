//
//  ActorListCell.h
//  Mcp
//
//  Created by Ray on 10/21/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface ActorListCell : UITableViewCell {
	IBOutlet UIImageView *actorImageView;
	IBOutlet UILabel *nameLabel;
}

@property (nonatomic, retain) UIImageView *actorImageView;
@property (nonatomic, retain) UILabel *nameLabel;

@end
