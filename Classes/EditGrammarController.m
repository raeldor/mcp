//
//  EditGrammarController.m
//  Mcp
//
//  Created by Ray on 1/7/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "EditGrammarController.h"
#import "EditHtmlCell.h"
#import "NameCell.h"
#import "Book.h"
#import "Chapter.h"
#import "Conversation.h"
#import "DialogueLine.h"

@implementation EditGrammarController

@synthesize grammar;
@synthesize editableListDelegate;

#pragma mark -
#pragma mark View lifecycle

-(void)setEditableObject:(GrammarNote*)editableObject isNew:(BOOL)isNew {
	// allow selecting during editor
	self.tableView.allowsSelectionDuringEditing = TRUE;
	
	// save new flag
	newMode = isNew;
	
	// save object
	self.grammar = editableObject;
    
    // save old title to go through and replace grammar links
    oldTitle = [grammar.title copy];
	
	// this table is always in edit mode
	// we are not adding delete rows, so we don't need edit mode
	//	[self setEditing:YES animated:NO];
	
	// create cancel button
	UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(cancelButtonClick:)];
	self.navigationItem.leftBarButtonItem = cancelButton;
	[cancelButton release];
	
	// create save button
	UIBarButtonItem *saveButton = [[UIBarButtonItem alloc] initWithTitle:@"Save" style:UIBarButtonItemStyleBordered target:self action:@selector(saveButtonClick:)];
	self.navigationItem.rightBarButtonItem = saveButton;
	[saveButton release];
	
	// notify keyboard events
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
}

-(void)saveButtonClick:(id)sender {
	// check first for errors
	if ([self isOkToSave])
	{
        // if we changed the title
        if (![grammar.title isEqualToString:oldTitle]) {
            // loop through books
            // create new list of books
            NSMutableArray *bookList = [Book getListOfFilesWithExtension:@"book"];
            for (int b=0; b < bookList.count; b++) {
                // open book
                Book *thisBook = [[Book alloc] initFromArchive:[bookList objectAtIndex:b]];
                
                // loop through chapters
                for (int ch=0; ch < thisBook.chapters.count; ch++) {
                    // loop through conversations
                    Chapter *thisChapter = [thisBook.chapters objectAtIndex:ch];
                    for (int co=0; co < thisChapter.conversations.count; co++) {
                        // loop through dialog lines
                        Conversation *thisConv = [thisChapter.conversations objectAtIndex:co];
                        for (int d=0; d < thisConv.dialogueLines.count; d++) {
                            // replace hyperlink with new link
                            DialogueLine *thisLine = [thisConv.dialogueLines objectAtIndex:d];
                            NSString *searchString = [NSString stringWithFormat:@"<a href=""file://%@"">", [oldTitle stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
                            NSString *replaceString = [NSString stringWithFormat:@"<a href=""file://%@"">", [grammar.title stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
                            thisLine.line = [thisLine.line stringByReplacingOccurrencesOfString:searchString withString:replaceString];
                        }
                    }
                }
                
                // save and release book
                [thisBook saveAs:thisBook.bookName];
                [thisBook release];
            }
        }
        
		// notify parent of change
		if (newMode)
			[editableListDelegate didAddNewObject:grammar];
		else
			[editableListDelegate didUpdateObject:grammar];
		
		// pop controller
		if (newMode) {
			[self dismissModalViewControllerAnimated:YES];
		}
		else {
			[self.navigationController popViewControllerAnimated:TRUE];
		}
	}
}

-(void)cancelButtonClick:(id)sender {
	// pop controller
	if (newMode) {
		[self dismissModalViewControllerAnimated:YES];
	}
	else {
		[self.navigationController popViewControllerAnimated:TRUE];
	}
}

-(BOOL)isOkToSave {
	BOOL stillOk = YES;
	NSString *msg;
	
	// check fields are filled
    if (grammar.title == nil || [grammar.title isEqualToString:@""]) {
        msg = @"Title must be completed.";
        stillOk = NO;
    }
    else
        if (grammar.text == nil || [grammar.text isEqualToString:@""]) {
            msg = @"Grammar text must be completed.";
            stillOk = NO;
        }
	
	// display error if not ok
	if (!stillOk) {
		UIAlertView *myAlert = [[UIAlertView alloc] initWithTitle:@"Oops" message:msg delegate:nil cancelButtonTitle:@"Understood" otherButtonTitles:nil];
		[myAlert show];
		[myAlert release];
	}
	
	return stillOk;
}

#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return 2;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	switch ([indexPath row]) {
		case 1: // edit text cell
            // make larger for ipad
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 30200
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
                return 300;
#endif
            return 200;
			break;
		default:
			break;
	}
	return 44;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *CellIdentifier = @"Cell";
	
	// get identifier and get cell
	NSArray *identifiers = [NSArray arrayWithObjects:@"NameCell", @"EditHtmlCell", nil];
	CellIdentifier = [identifiers objectAtIndex:[indexPath row]];
	UITableViewCell *cell = (NameCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	if (cell == nil) {
		// load cell layout from nib
		NSArray *nib = [[NSBundle mainBundle] loadNibNamed:CellIdentifier owner:self options:nil];
		cell = [nib objectAtIndex:0];
	}
	cell.accessoryType = UITableViewCellAccessoryNone;
	cell.selectionStyle = UITableViewCellSelectionStyleNone;
	
	// now configure new cell
	NameCell *myNameCell;
	EditHtmlCell *myEditHtmlCell;
	switch ([indexPath row]) {
		case 0: // name
			myNameCell = (NameCell*)cell;
			[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textFieldDidChange:) name:@"UITextFieldTextDidChangeNotification" object:myNameCell.nameTextField];
			myNameCell.nameLabel.text = @"Title:";
			myNameCell.nameTextField.text = grammar.title;
			break;
		case 1: // edit/preview text
			myEditHtmlCell = (EditHtmlCell*)cell;
			myEditHtmlCell.editTextView.text = grammar.text;
			[myEditHtmlCell.previewWebView loadHTMLString:grammar.text baseURL:nil];
			[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textViewDidChange:) name:@"UITextViewTextDidChangeNotification" object:myEditHtmlCell.editTextView];
			[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textViewDidBeginEditing:) name:@"UITextViewTextDidBeginEditingNotification" object:myEditHtmlCell.editTextView];
			break;
		default:
			break;
	}
    
    return cell;
}

-(void)textViewDidChange:(NSNotification*)note {
	UITextView *thisTextView = (UITextView*)note.object;
	grammar.text = [NSString stringWithString:thisTextView.text];
}

-(void)textViewDidBeginEditing:(NSNotification*)note {
	// scroll to show editable text
	UITextView *thisTextView = (UITextView*)note.object;
	UITableViewCell *cell = (UITableViewCell*)[[thisTextView superview] superview];
	[self.tableView scrollToRowAtIndexPath:[self.tableView indexPathForCell:cell] atScrollPosition:UITableViewScrollPositionTop animated:YES];
}

-(void)keyboardWillShow:(NSNotification*)note {
	// don't do any scrolling
	self.tableView.scrollEnabled = NO;
	
	// reload data to re-size cell when keyboard is present!
//	keyboardPresent = YES;
//	[self.tableView reloadData];
}

-(UITableViewCellEditingStyle)tableView:(UITableView*)tableView editingStyleForRowAtIndexPath:(NSIndexPath*)indexPath {
	return UITableViewCellEditingStyleNone;
}

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */


/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
 
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }   
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }   
 }
 */


/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
 }
 */


/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */


#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	// save last index path for de-highlighting and updating
    [lastIndexPath release];
	lastIndexPath = [indexPath retain];
}

-(void)textFieldDidChange:(NSNotification*)note {
	grammar.title = [note.object performSelector:@selector(text)];
}

#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    // Relinquish ownership of anything that can be recreated in viewDidLoad or on demand.
    // For example: self.myOutlet = nil;
}


- (void)dealloc {
    [lastIndexPath release];
	[grammar release];
    [super dealloc];
}


@end
