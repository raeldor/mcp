//
//  ActorPhotoCell.h
//  Mcp
//
//  Created by Ray on 10/21/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface ActorPhotoCell : UITableViewCell {
	IBOutlet UIImageView *actorImageView;
}

@property (nonatomic, retain) UIImageView *actorImageView;

@end
