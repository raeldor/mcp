//
//  Syllable.h
//  Mcp
//
//  Created by Ray on 3/3/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface Syllable : NSObject {
	NSString *syllableName;
	NSString *syllableKana;
	NSMutableArray *phonemes;
}

-(id)initWithDictionary:(NSDictionary*)inDictionary;
-(void)dealloc;

@property (nonatomic, retain) NSString *syllableName;
@property (nonatomic, retain) NSString *syllableKana;
@property (nonatomic, retain) NSMutableArray *phonemes;

@end
