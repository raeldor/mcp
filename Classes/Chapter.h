//
//  Chapter.h
//  Mcp
//
//  Created by Ray on 10/25/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface Chapter : NSObject <NSCoding> {
    NSString *uniqueKey;
	NSString *chapterName;
	NSMutableArray *conversations;
	NSMutableArray *grammarNotes;
}

@property (nonatomic, retain) NSString *uniqueKey;
@property (nonatomic, retain) NSString *chapterName;
@property (nonatomic, retain) NSMutableArray *conversations;
@property (nonatomic, retain) NSMutableArray *grammarNotes;

@end
