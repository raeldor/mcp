//
//  VoiceSample.m
//  Mcp
//
//  Created by Ray on 11/15/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#include "AudioToolbox/AudioQueue.h"
#include "AudioToolbox/AudioToolbox.h"
#import "VoiceSample.h"
#import "Accelerate/Accelerate.h"
#import "McpAppDelegate.h"
#import "NeuralNet.h"
#import "FloatIndex.h"
#import "eq.h"
#import "NewTrainingData.h"
#import "NewTrainingSample.h"
#import "TrainingWord.h"
#import "BufferPhoneme.h"

@implementation StretchDetail

@synthesize score;
@synthesize stretchPercent;
@synthesize processedSize;

-(id)initWithScore:(float)inScore andStretch:(float)inStretch andProcessedSize:(int)inProcessedSize {
 	if ((self = [super init])) {
        score = inScore;
        stretchPercent = inStretch;
        processedSize = inProcessedSize;
    }
    return self;
}

@end

@implementation VoiceSample

+(int)getMelChannelCountForSampleRate:(int)inSampleRate {
//    return 16;
    return 40;
    if (inSampleRate == 8000)
        return 32;
    else
        return 40;
}

+(int)getMfccCount {
	return 256;
}

+(int)getFeatureCount {
    return 6;
}

+(int)getCompareFeatureCount {
	return 6;
}

+(int)getOutputFeatureCount {
	return 6;
}

// n is effectively the window size
+(int)getN {
	UInt32 log2n = [VoiceSample getLog2N];
	return 1<<log2n;
}

+(int)getLog2N {
	return 9;
}

+(int)getCompareSegmentSize {
	return 32;
}

-(id)initWithBuffer:(float*)inBuffer ofLength:(int)inLength quantizeTo:(int)inQuantizeCount fromUser:(UserProfile*)inUserProfile noiseLevel:(float)inNoiseLevel cepstralHistory:(CepstralHistory*)inCepstralHistory phonemeLengthArray:(NSArray*)inPhonemeLengthArray sampleRate:(int)inSampleRate {
	if ((self = [super init])) {
        // get application delegate
        McpAppDelegate *appDelegate = (McpAppDelegate*)[[UIApplication sharedApplication] delegate];
        
		// take buffer and saved analyzed version of the data
		printf("buffer size is %d, quantize count %d\r\n", inLength, inQuantizeCount);
        
        // save length array for compare
        phonemeLengthArray = inPhonemeLengthArray;

		// save quantize count and mfcc parameters
		quantizeCount = inQuantizeCount;
		mfccCount = [VoiceSample getMfccCount];
//		int usedMfccCount = [VoiceSample getMfccCount];
        melCount = [VoiceSample getMelChannelCountForSampleRate:inSampleRate];

        // for debugging, just use same as mfcc
        featureCount = [VoiceSample getFeatureCount];
		
		// set up fft parameters
		UInt32 log2n = [VoiceSample getLog2N];
		UInt32 n = [VoiceSample getN];
		UInt32 halfN = n/2;
		UInt32 stride = 1;
        FFTSetup setupReal = [appDelegate getFftSetup];

        // use fft to determine what silence to remove from beginning and end
        
        // this will be done using getlead stuff later
        /*
		// remove any silence greater than window size
		int copyPos = 0;
		int silenceCount = 0;
		int newLength = inLength;
		for (int i=0; i < inLength; i++) {
			if (inBuffer[i] == 0.0f)
				silenceCount++;
			else {
				if (silenceCount > halfN) {
					copyPos -= silenceCount;
					newLength -= silenceCount;
				}
				silenceCount = 0;
			}
			inBuffer[copyPos++] = inBuffer[i];
		}
		inLength = newLength;
         */
        
        // TAKE OFF HIGH PASS TO TEST VOLUME
		// high pass filter
		float *hpBuffer = malloc(inLength*sizeof(float));
		float prevValue = 0.0f;
		for (int s=0; s < inLength; s++) {
			float thisValue = inBuffer[s];
//			if (s == 0)
				hpBuffer[s] = thisValue;
//			else {
//                hpBuffer[s] = thisValue-0.95f*prevValue;
//            }
			prevValue = thisValue;
		}
        
        // normalize again now
		[self normalizeBufferAtZero:hpBuffer ofSize:inLength];
        
        // use fft to find lead and tail
		int leadLength = 0;
		int tailLength = 0;
        NSArray *silenceList;
        
        float leadPercent = 0.0f;
        float tailPercent = 0.0f;
        float overrideNoiseLevel = -1.0f;
        if ([inUserProfile.name isEqualToString:@"Cpu"]) {
            overrideNoiseLevel = 0.1f;
        }
        else
            overrideNoiseLevel = 0.1f;
        silenceList = [self getLead:&leadPercent andTail:&tailPercent fromBuffer:inBuffer ofLength:inLength overrideNoiseLevel:overrideNoiseLevel fromUser:inUserProfile];
        leadLength = (inLength-1) * leadPercent;
        tailLength = (inLength-1)  - ((inLength-1) * tailPercent);
        
        // WE NEED TO REMOVE SILENCE BECAUSE THE USER MAY 'SPEAK THROUGH' WORDS
        // THAT THE ORIGINAL SPEAKER SEPARATES
        
        // DON'T REMOVE SILENCE FOR CPU VERSION BECAUSE WE NEED TO KEEP THE PHONEME POSITIONS
        if ([inUserProfile.name isEqualToString:@"Cpu"]) {
            silenceList = [NSArray array];
            leadLength = 0;
            tailLength = 0;
        }
        
        
//		printf("lead is %d\n\r", leadLength);
//		printf("tail is %d\n\r", tailLength);
        
		// now we have lead and tail, copy to our own buffer for debugging
		sampleBufferSize = (inLength-leadLength-tailLength) * sizeof(float);
		sampleBuffer = malloc(sampleBufferSize);
		int copyOffset = leadLength;
		int i=0;
        int silencePos = 0;
        int waitCounter = 0;
		while (copyOffset < inLength-tailLength) {
            // if we are looking for silence, check to see if we've hit it
            if (waitCounter == 0 && silencePos < silenceList.count) {
                // find next silence position
                NSNumber *startPercent = [silenceList objectAtIndex:silencePos];
                NSNumber *silenceLength = [silenceList objectAtIndex:silencePos+1];
                
                // if we're past the start position
                if ((float)copyOffset/(float)inLength > [startPercent floatValue]) {
                    // start wait counter
                    waitCounter = [silenceLength floatValue]*(float)inLength;
                    
                    // increment silence position
                    silencePos += 2;
                }
            }
            
            // copy if we are not waiting to pass silence
            if (waitCounter > 0) {
                waitCounter--;
                copyOffset++;
            }
            else
                sampleBuffer[i++] = hpBuffer[copyOffset++];
        }
        sampleBufferSize = i*sizeof(float);
        int sampleBufferSamples = i;
        /*
        // try making quieter
        if (![inUserProfile.name isEqualToString:@"Cpu"]) {
            for (int s=0; s < sampleBufferSamples/5; s++) {
                sampleBuffer[s] *= 0.3f;
            }
        }
        */
        
        // copy sample buffer to hpBuffer
        // are you an idiot?  have you been copying hp to sample all this time!?
        memcpy(hpBuffer, sampleBuffer, sampleBufferSamples*sizeof(float));
//        memcpy(sampleBuffer, hpBuffer, sampleBufferSamples*sizeof(float));
//        for (int i=0; i < sampleBufferSamples; i++)
//            hpBuffer[i+leadLength] = sampleBuffer[i];
        int hpBufferSize = inLength-leadLength-tailLength;
        hpBufferSize = sampleBufferSamples;

        // fft parameters
		int window = n;
		int stepSize = (hpBufferSize-window) / quantizeCount;

        
        // calculate volume from raw samples, because it seems more reliable
        int volumeWindow = n/4;
        float *volumeBuffer = malloc(sizeof(float)*quantizeCount);
        int windowPos = 0;
        for (int i=0; i < inQuantizeCount; i++) {
            float total = 0.0f;
            float max = 0.0f;
            float min = 1.0f;
            for (int p=windowPos; p < windowPos+volumeWindow; p++) {
                total += hpBuffer[p];
                if (hpBuffer[p] > max)
                    max = hpBuffer[p];
                if (hpBuffer[p] < min)
                    min = hpBuffer[p];
            }
            volumeBuffer[i] = max-min;
            // show as increment based on volume
//            if (i > 0)
//                volumeBuffer[i-1] = volumeBuffer[i]-volumeBuffer[i-1];
            windowPos += stepSize;
        }
//        [VoiceSample normalizePositiveBuffer:volumeBuffer ofSize:quantizeCount];
        
        [self normalizeAndScaleBuffer:volumeBuffer ofSize:quantizeCount];
        for (int i=0; i < 4; i++)
            [self blurBuffer:volumeBuffer ofWidth:quantizeCount andHeight:1];
        [self normalizeAndScaleBuffer:volumeBuffer ofSize:quantizeCount];
        [VoiceSample applyContrast:volumeBuffer ofWidth:quantizeCount andHeight:1 notUsedPercentage:0.0f];
        
        
        // high pass filter
        prevValue = 0.0f;
        for (int s=0; s < hpBufferSize; s++) {
            float thisValue = hpBuffer[s];
            if (s == 0)
                hpBuffer[s] = thisValue;
            else {
                hpBuffer[s] = thisValue-0.95f*prevValue;
            }
            prevValue = thisValue;
        }
        // normalize again now
        [self normalizeBufferAtZero:hpBuffer ofSize:hpBufferSize];
        
//		printf("length is %d\n\r", inLength-leadLength-tailLength);
//		printf("window is %d\n\r", window);
//		printf("step size is %d\r\n", stepSize);
		
		// allocate memory for complex array
		COMPLEX_SPLIT complexArray;
		complexArray.realp = (float*)malloc(halfN*sizeof(float));
		complexArray.imagp = (float*)malloc(halfN*sizeof(float));
		
		// allocate buffers for final data
		dataLength = quantizeCount;

		// buffer for results of fft
		float *resultBuffer = malloc(sizeof(float)*halfN);
		float *resultDbBuffer = malloc(sizeof(float)*halfN);
		
		// image buffer (12 mfcc's)
		featureBuffer = malloc(sizeof(float)*featureCount*quantizeCount);
		
		// mfcc and delta buffer (number of mfcc's)
		melBuffer = malloc(sizeof(float)*melCount*quantizeCount);
		mfccBuffer = malloc(sizeof(float)*mfccCount*quantizeCount);
		deltaBuffer = malloc(sizeof(float)*mfccCount*quantizeCount);
		
		// energy buffers
		energy = malloc(sizeof(float)*quantizeCount);
		energyDelta = malloc(sizeof(float)*quantizeCount);
		
		// covariance buffer
		covBuffer = malloc(sizeof(float)*quantizeCount);
		
		// spectrum buffer
		spectrumBuffer = malloc(sizeof(float)*halfN*quantizeCount);
		
		// get pre-calcd mel matrix
        int melChannels = [VoiceSample getMelChannelCountForSampleRate:inSampleRate];
        float *fftMatrix = [appDelegate getMelMatrixForSampleRate:inSampleRate];
		
        // find min and max db across entire sample
        float maxDb = -9999.0f;
        float minDb = 9999.0f;
		
		// create data point for each quantize segment
		float *harmonicStrength = malloc(sizeof(float)*quantizeCount);
        float *powerBuffer = malloc(sizeof(float)*quantizeCount);
        float *maxPowerBuffer = malloc(sizeof(float)*quantizeCount);
		float TWOPI = 2.0f * M_PI;
		for (int s=0; s < quantizeCount; s++) {
			// copy buffer data into a seperate array and apply hamming window
            // don't use leadlength because we copied to beginning of buffer
			int offset = (int)(s * stepSize);
			float *hamBuffer = malloc(n*sizeof(float));
			for (int i=0; i < n; i++)
				hamBuffer[i] = hpBuffer[offset+i] * ((1.0f-0.46f) - 0.46f*cos(TWOPI*i/((float)n-1.0f)));
			
			// configure float array into acceptable input array format (interleaved)
			vDSP_ctoz((COMPLEX*)hamBuffer, 2, &complexArray, 1, halfN);
			
			// free ham buffer
			free(hamBuffer);

			// run FFT
			vDSP_fft_zrip(setupReal, &complexArray, stride, log2n, FFT_FORWARD);
            
            // Absolute square (equivalent to mag^2)
            complexArray.imagp[0] = 0.0f;
            vDSP_zvmags(&complexArray, 1, complexArray.realp, 1, halfN);
            bzero(complexArray.imagp, (halfN) * sizeof(float));
            
            // scale
            float scale = 1.0f / (2.0f*(float)n);
            vDSP_vsmul(complexArray.realp, 1, &scale, complexArray.realp, 1, halfN);
            
            // get log of absolute values for passing to inverse FFT for cepstrum
            float *logmag = malloc(sizeof(float)*halfN);
            for (int i=0; i < halfN; i++)
                complexArray.realp[i] = logf(sqrtf(complexArray.realp[i]));
//            logmag[i] = logf(sqrtf(complexArray.realp[i]));
            
            // print magnitude to debug window
//            for (int i=0; i < halfN; i++)
//                printf("spectrum %f\r\n", complexArray.realp[i]);
//            for (int i=0; i < halfN; i++)
//                printf("log spectrum %f\r\n", logmag[i]);
            
			// configure float array into acceptable input array format (interleaved)
//			vDSP_ctoz((COMPLEX*)logmag, 2, &complexArray, 1, halfN/2);
            
            // create cepstrum
            vDSP_fft_zrip(setupReal, &complexArray, stride, log2n, FFT_INVERSE);
            complexArray.realp[0] = complexArray.imagp[0];
            
            /*
            // Absolute square (equivalent to mag^2)
            complexArray.imagp[0] = 0.0f;
            vDSP_zvmags(&complexArray, 1, complexArray.realp, 1, halfN/2);
            bzero(complexArray.imagp, (halfN) * sizeof(float));
            
            // scale again
            scale = (float) 1.0 / (2 * n);
            vDSP_vsmul(complexArray.realp, 1, &scale, complexArray.realp, 1, halfN);
            
            // zero out dc component
            complexArray.realp[0] = 0.0f;
             */
            
            // try multiplying by the volume at this position
//            for (int i=0; i < halfN; i++)
//                complexArray.realp[i] *= volumeBuffer[s];
            
            //convert interleaved to real
            float *displayData = malloc(sizeof(float)*n);
            vDSP_ztoc(&complexArray, 1, (COMPLEX*)displayData, 2, halfN);
            
            
            
            // print magnitude to debug window
//            for (int i=0; i < halfN; i++)
//                printf("cepstrum %f\r\n", complexArray.realp[i]);
            
            /*
            for (int i=16; i < halfN-16; i++) {
                complexArray.realp[i] = 0;
                complexArray.imagp[i] = 0;
            }
            
            // now run this through the fft again
            vDSP_fft_zrip(setupReal, &complexArray, stride, log2n-2, FFT_FORWARD);
  
            // convert to magnitude
            complexArray.imagp[0] = 0.0f;
            complexArray.realp[0] = 0.0f;
            vDSP_zvmags(&complexArray, 1, complexArray.realp, 1, halfN/4);
            bzero(complexArray.imagp, (halfN/2) * sizeof(float));
            
            // scale again
            scale = (float) 1.0 / (2 * n);
            vDSP_vsmul(complexArray.realp, 1, &scale, complexArray.realp, 1, halfN);
            vDSP_vsmul(complexArray.imagp, 1, &scale, complexArray.imagp, 1, halfN);
            
            // print magnitude to debug window
            for (int i=0; i < halfN; i++)
                printf("mag fft %f\r\n", complexArray.realp[i]);
            
            // take log
            for (int i=1; i < halfN/4; i++)
                complexArray.realp[i] = log10f(complexArray.realp[i]);
            
            
            // print magnitude to debug window
            for (int i=0; i < halfN; i++)
                printf("log fft %f\r\n", complexArray.realp[i]);
            */
            
            
			// convert complex data into an array of real floats
			// find max and total amplitude at the same time
			float totalDb = 0;
			float totalMag = 0;
			float totalAmp = 0;
			float maxAmp = 0;
			float minAmp = 99999.0f;
            powerBuffer[s] = 0.0f;
            maxPowerBuffer[s] = 0.0f;
			for (int i=0; i < halfN; i++) {
				// get the complex number and convert
				float complex1 = complexArray.realp[i];
				float complex2 = complexArray.imagp[i];
				float sample = sqrt(powf(complex1, 2) + powf(complex2, 2));
				
                sample = displayData[i];
//                sample = complexArray.realp[i];
                
				// calc total magnitude before convert to power
				totalMag += sample;
                
                // save spectrum for debugging
//                spectrumBuffer[s*halfN+i] = log10f(sample);
//                spectrumBuffer[s*halfN+i] = powf(fabs(sample)/n, 2.0f);
                
//				sample = powf(sample, 2.0f);
//                sample = 10 * log10f(sample);
                
                
                spectrumBuffer[s*halfN+i] = sample;
                powerBuffer[s] += sample;
                if (sample > maxPowerBuffer[s])
                    maxPowerBuffer[s] = sample;
                
				// convert magnitude to power spectrum
				sample = powf(sample, 2.0f);
//				sample = 10.0f * log10f(sample);
				
				// convert to db
				float db = 10.0f * log10(sample);
				resultDbBuffer[i] = db;
				
				if (db > maxDb)
					maxDb = db;
				if (db < minDb)
					minDb = db;
                
				resultBuffer[i] = sample;
				totalAmp += sample;
				totalDb += db;
				if (sample > maxAmp)
					maxAmp = sample;
				if (sample < minAmp)
					minAmp = sample;
			}
            
			// inverse dct
/*			for (int m=0; m < mfccCount; m++) {
				invDct[m] = 0.5f*mfcc[0];
				for (int k=1; k < melCount; k++) {
					invDct[m] += cos(k*(m+0.5f)*(M_PI/(float)melCount))*mfcc[k];
				}
				invDct[m] *= 2.0f/melCount;
			}*/
			
            /*
			// debug print sphinx dct's
			printf("Our DCT's for %d:",s);
			for (int i=0; i < mfccCount; i++)
				printf("%8.4f ", mfcc[i]);
			printf("\r\n");
*/
		}
        
        // for each of the spectrum bands, match total power against the total power in
        // the spectrum band of the user sample.  this is because the user sample may be
        // inadequte in certain spectrum bands

        /*
        // add spectrum data to ongoing history buffer
        [inUserProfile.bufferHistory addNewData:spectrumBuffer ofLength:quantizeCount];

        // if this sample is from the CPU
        if ([inUserProfile.name isEqualToString:@"Cpu"]) {
            // find mismatch between frequency response in generated voice versus
            // the microphone
            float *userBands = [appDelegate.currentUserProfile.bufferHistory getRowTotals];
            float *cpuBands = [appDelegate.currentCpuProfile.bufferHistory getRowTotals];
            for (int i=0; i < halfN; i++) {
                // compare 2 to get boost
                float boost = userBands[i] / cpuBands[i];
                
                // now boost this spectrum buffer
                for (int s=0; s < quantizeCount; s++) {
                    spectrumBuffer[s*halfN+i] *= boost;
                }
            }
        }
        */
        
        /*
         // normalize the spectrum buffer again
         [self normalizeBufferAtZero:spectrumBuffer ofSize:halfN*quantizeCount];
         
        // hp filter the spectrum buffer
        for (int i=0; i < halfN; i++) {
            for (int s=0; s < quantizeCount; s++) {
                float thisValue = spectrumBuffer[s*halfN+i];
                if (s == 0)
                    spectrumBuffer[s*halfN+i] = thisValue;
                else {
                    spectrumBuffer[s*halfN+i] = thisValue-0.95f*prevValue;
                    //                hpBuffer[s] = thisValue - prevValue + 0.95f * hpBuffer[s-1];
                }
                prevValue = thisValue;
            }
        }
         */

        /*
        // normalize the spectrum buffer again
        // DON'T SCALE BECAUSE INVERSE FFT OF LOG GIVES QUEFRENCY, BUT INVERSE FTT
        // OF LINEAR CONVERTS BACK TO TIME
//        [self normalizeBufferAtZero:spectrumBuffer ofSize:halfN*quantizeCount];
        
        // run fft on the fft and output to spectrum buffer
        int firstHalfN = [VoiceSample getN]/2;
        for (int s=0; s < quantizeCount; s++) {
            int log2n = 8;
            int n = 1<<log2n;
            int halfN = n/2;
            FFTSetup mySetupReal = vDSP_create_fftsetup(log2n, FFT_RADIX2);
            vDSP_ctoz((COMPLEX*)&spectrumBuffer[s*firstHalfN+0], 2, &complexArray, 1, halfN);
            vDSP_fft_zrip(mySetupReal, &complexArray, stride, log2n, FFT_INVERSE);
            for (int i=0; i < firstHalfN; i++)
                spectrumBuffer[s*firstHalfN+i] = 0.0f;
            float total = 0.0f;
            for (int i=0; i < halfN; i++) {
                // get the complex number and convert
                float complex1 = complexArray.realp[i];
                float complex2 = complexArray.imagp[i];
                float sample = sqrt(powf(complex1, 2) + powf(complex2, 2));
                spectrumBuffer[s*firstHalfN+i] = sample;
                total += sample;
            }
            vDSP_destroy_fftsetup(mySetupReal);
        }
        */
        /*
        // lifter to remove the pitch estimation
        for (int s=0; s < quantizeCount; s++) {
            for (int i=15; i < halfN; i++) {
                spectrumBuffer[s*halfN+i] = 0.0f;
            }
        }
         */
        
        /*
        // now run dft again to pull the formant info
        for (int s=0; s < quantizeCount; s++) {
            int log2n = [VoiceSample getLog2N]-1;
            int n = 1<<log2n;
            int firstHalfN = halfN;
            int halfN = n/2;
            FFTSetup mySetupReal = vDSP_create_fftsetup(log2n, FFT_RADIX2);
            vDSP_ctoz((COMPLEX*)&spectrumBuffer[s*firstHalfN+0], 2, &complexArray, 1, halfN);
            vDSP_fft_zrip(mySetupReal, &complexArray, stride, log2n, FFT_FORWARD);
            for (int i=0; i < firstHalfN; i++)
                spectrumBuffer[s*firstHalfN+i] = 0.0f;
            float total = 0.0f;
            for (int i=0; i < halfN; i++) {
                // get the complex number and convert
                float complex1 = complexArray.realp[i];
                float complex2 = complexArray.imagp[i];
                float sample = sqrt(powf(complex1, 2) + powf(complex2, 2));
                spectrumBuffer[s*firstHalfN+i] = sample;
                total += sample;
            }
            vDSP_destroy_fftsetup(mySetupReal);
        }
        
        // normalize the spectrum buffer again
        [self normalizeAndScaleBuffer:spectrumBuffer ofSize:halfN*quantizeCount];
        for (int s=0; s < quantizeCount; s++) {
            for (int i=0; i < halfN/4; i++) {
                spectrumBuffer[s*halfN+i] = 0.0f;
            }
        }
        */
        
//        [self normalizeAndScaleBuffer:spectrumBuffer ofSize:halfN*quantizeCount];
        
        /*
        // try and match spectrum of incoming sample
        if (NO) {
//        if (matchSpectrumBuffer != nil) {
            for (int i=0; i < halfN; i++) {
                // loop through quantize and find total power for both
                float thisPower = 0.0f;
                float inPower = 0.0f;
                for (int s=0; s < quantizeCount; s++) {
                    thisPower += spectrumBuffer[s*halfN+i];
                    inPower += matchSpectrumBuffer[s*halfN+i];
                }
                
                // find amount to boost/lower this by
                float boost = inPower / thisPower;
                
                // now boost this spectrum buffer
                for (int s=0; s < quantizeCount; s++) {
                    spectrumBuffer[s*halfN+i] *= 2.0f;
                }
            }
        }
        */
        
        // normalize the spectrum buffer first so we get power values
//        [self normalizeAndScaleBuffer:spectrumBuffer ofSize:halfN*quantizeCount];
        
        /* debugging
        // try a test to remove some from spectrum buffer
        float total = 0.0f;
        float specmin = 9999.0f;
        float specmax = 0.0f;
        for (int s=0; s < quantizeCount; s++) {
            for (int i=0; i < halfN; i++) {
                total += log10f(spectrumBuffer[s*halfN+i]);
                if (log10f(spectrumBuffer[s*halfN+i]) < specmin)
                    specmin = log10f(spectrumBuffer[s*halfN+i]);
                if (log10f(spectrumBuffer[s*halfN+i]) > specmax)
                    specmax = log10f(spectrumBuffer[s*halfN+i]);
            }
        }
        printf("spectrum min is %f, max is %f\r\n", specmin, specmax);
         */
        
        /*
//        if (![inUserProfile.name isEqualToString:@"Cpu"]) {
            float mean = total / (float)(quantizeCount*halfN);
            for (int s=0; s < quantizeCount; s++) {
                for (int i=0; i < halfN; i++) {
                    float sample = spectrumBuffer[s*halfN+i];
                    sample = log10f(sample);
//                    sample += mean;
                    sample = powf(10.0f, sample);
                    if (i == 0 || i > halfN/5)
                        sample *= 0.01f; // tone down anything over around 1500khz
                    spectrumBuffer[s*halfN+i] = sample;
                }
            }
//        }
        */
        
//        for (int c=0; c < 4; c++)
//            [self blurBuffer:spectrumBuffer ofWidth:quantizeCount andHeight:halfN];
        
        /*
        // run a blur on the spectral buffer
        for (int c=0; c < 4; c++) {
            for (int s=1; s < quantizeCount-1; s++) {
                for (int i=0; i < halfN-1; i++) {
                    spectrumBuffer[s*halfN+i] = (spectrumBuffer[s*halfN+i]+spectrumBuffer[(s+1)*halfN+i]+spectrumBuffer[(s+1)*halfN+i]+spectrumBuffer[s*halfN+i+1])/4.0f;
                }
            }
            for (int s=quantizeCount-1; s > 0; s--) {
                for (int i=halfN-1; i > 0; i--) {
                    spectrumBuffer[s*halfN+i] = (spectrumBuffer[s*halfN+i]+spectrumBuffer[s*halfN+i-1]+spectrumBuffer[(s-1)*halfN+i]+spectrumBuffer[(s-1)*halfN+i-1])/4.0f;
                }
            }
        }*/
    
        
        /*
        // apply constrast to spectrum
        [self normalizeAndScaleBuffer:spectrumBuffer ofSize:halfN*quantizeCount];
        [VoiceSample applyContrast:spectrumBuffer ofWidth:quantizeCount andHeight:halfN notUsedPercentage:0.0f];
        [self normalizeAndScaleBuffer:spectrumBuffer ofSize:halfN*quantizeCount];
*/
        
        // post process the spectrum buffer into mel logs
        float totalPower = 0.0f;
        float minMel = 9999.0f;
        float maxMel = 0.0f;
        float maxMelLog = 0.0f;
        for (int s=0; s < quantizeCount; s++) {
            float spectrumPower = 0.0f;
            float minSample = 9999.0f;
            float maxSample = 0.0f;
            for (int i=0; i < halfN; i++) {
                float sample = spectrumBuffer[s*halfN+i];
                
                sample = powf(fabs(sample), 2.0f);
                
                // convert to db
                float db = 10.0f * log10(db);
                resultBuffer[i] = sample;
                spectrumPower += sample;
                if (sample > maxSample)
                    maxSample = sample;
                if (sample < minSample)
                    minSample = sample;
            }
            
            
            // calculate powers for each of the mel channels
            float melPower[256]; // max 40 channels
            float *melWindow = [appDelegate getMelWindowForSampleRate:16000];
            for (int c=0; c < melChannels; c++) {
                melPower[c] = 0.0f;
                for (int i=0; i < halfN; i++) {
                    melPower[c] += resultBuffer[i] * fftMatrix[c*halfN+i];
                }
                
                // do test here, to make value smaller depending on window size
//                float windowRatio = (melWindow[melChannels-1]/melWindow[c]);
//                melPower[c] *= windowRatio;
                
                
                if (melPower[c] > maxMel)
                    maxMel = melPower[c];
                if (melPower[c] < minMel)
                    minMel = melPower[c];
                totalPower += melPower[c];
                
            }
            
            // compress using logarithms
            float melLogs[256];
            for (int i=0; i < melChannels; i++) {
                melLogs[i] = log10f(melPower[i]);
                if (melLogs[i] > maxMelLog)
                    maxMelLog = melLogs[i];
            }

            // instead of using dct, use fft again
            /*
            int log2n = 8;
            int n = 1<<log2n;
//            int n = melChannels;
            int halfN = n/2;
//            int log2n = log2(n);
            FFTSetup mySetupReal = vDSP_create_fftsetup(log2n, FFT_RADIX2);
			vDSP_ctoz((COMPLEX*)melLogs, 2, &complexArray, 1, halfN);
			vDSP_fft_zrip(mySetupReal, &complexArray, stride, log2n, FFT_FORWARD);
            float newFft[256];
            for (int i=0; i < mfccCount; i++)
                newFft[i] = 0.0f;
			for (int i=0; i < halfN; i++) {
				// get the complex number and convert
				float complex1 = complexArray.realp[i];
				float complex2 = complexArray.imagp[i];
				float sample = sqrt(powf(complex1, 2) + powf(complex2, 2));
                newFft[i] = sample;
            }
            vDSP_destroy_fftsetup(mySetupReal);
            */

            // update mel buffer
            for (int i=0; i < melCount; i++) {
                melBuffer[s*melCount+i] = melLogs[i];
//                melBuffer[s*melCount+i] = melPower[i];
                
//                if (i == melCount-1)
//                    melBuffer[s*melCount+i] = 0.0f;
//                melBuffer[s*melCount+i] = melLogs[i];
            }
        }
        
        
        /*
        // define bark scale
//        int barkCount = melChannels;
        const float barkEdges[] = {20,100,200,300,400,510,630,770,920,1080,1270,1480,1720,2000,2320,2700,3150,3700,4400,5300,6400,7700,9500,12000,15500};
//        const float barkCenters[] = {50,150,250,350,450,570,700,840,1000,1170,1370,1600,1850,2150,2500,2900,3400,4000,4800,5800,7000,8500,10500,13500};
        
        // convert to bark
        for (int s=0; s < quantizeCount; s++) {
            for (int w=0; w < melChannels; w++) {
                // determine start pos and end pos into spectrum buffer
                float bandwidthPerArrayElement = 8000.0f/(float)halfN;
                int windowStart = roundf(barkEdges[w]/bandwidthPerArrayElement);
                int windowEnd = roundf(barkEdges[w+1]/bandwidthPerArrayElement);
                int windowSize = windowEnd-windowStart;
                
                // create triangular window
                float *windowMatrix = malloc(windowSize*sizeof(float));
                for (int i=0; i < windowSize; i++)
                    windowMatrix[i] = (2.0f/((float)windowSize-1.0f))*((((float)windowSize-1.0f)/2.0f)-fabs((float)i-(((float)windowSize-1.0f)/2.0f)));
                
                // assign to mel buffer
                melBuffer[s*melCount+w] = 0.0f;
                for (int i=0; i < windowSize; i++) {
                    float sample = spectrumBuffer[s*halfN+windowStart+i];
                    sample = powf(fabs(sample), 2.0f);
                    melBuffer[s*melCount+w] += sample * windowMatrix[i];
                }
                melBuffer[s*melCount+w] /= (float)windowSize;
//                melBuffer[s*melCount+w] = log10f(melBuffer[s*melCount+w]);
            }
        }
        */
        
        /*
        // the first two formants are always below 3000hz
        // try doing a straight linear mapping of the spectrum up to 3000hz into
        // the mel buffer using overlapping triangular windows
        // assume an 8000hz range at this point
        int startFreq = 0;
        int endFreq = 3000;
        if (![inUserProfile.name isEqualToString:@"Cpu"]) {
            startFreq = 0;
            endFreq = 2500;
        }
        int maxN = (float)halfN*((float)endFreq/8000.0f);
        float overlap = 0.5f;
        int windowSize = (float)(maxN/melChannels)*(1.0f+overlap);
        int overlapSize = (float)(maxN/melChannels)*overlap;
        int windowStepSize = windowSize-overlapSize;
        int windowCount = maxN/windowStepSize;
        // pre-compute window
        float *windowMatrix = malloc(windowSize*sizeof(float));
        for (int i=0; i < windowSize; i++)
            windowMatrix[i] = (2.0f/(windowSize-1.0f))*(((windowSize-1.0f)/2.0f)-fabs((float)i-((windowSize-1.0f)/2.0f)));
        for (int s=0; s < quantizeCount; s++) {
            int windowStartPos = startFreq;
            for (int w=0; w < windowCount; w++) {
                melBuffer[s*melCount+w] = 0.0f;
                for (int i=0; i < windowSize; i++) {
                    float sample = spectrumBuffer[s*halfN+windowStartPos+i];
                    sample = powf(fabs(sample), 2.0f);
                    melBuffer[s*melCount+w] += sample * windowMatrix[i];
                }
                melBuffer[s*melCount+w] = log10f(melBuffer[s*melCount+w]);
                windowStartPos += windowStepSize;
            }
        }        
        */
        
        
//        [self normalizeAndScaleBuffer:melBuffer ofSize:melCount*quantizeCount];

        // normalize mel buffer
        // have to normalize completely, not using max because volume should not
        // be a factor
//        [self normalizeBufferAtZero:melBuffer ofSize:quantizeCount*melCount];
//        [self normalizeBufferAtZero:melBuffer ofSize:quantizeCount*melCount];
            
        /*
        // TAKE DCT OF ORIGINAL SPECTROGRAM
        // compute mel cosine filter bank
        float *melcosine = malloc(sizeof(float)*mfccCount*halfN);
        float period = (float) 2.0f * (float)halfN;
        for (int i = 0; i < mfccCount; i++) {
            float frequency = 2.0f * M_PI * i / period;
            for (int j = 0; j < halfN; j++) {
                melcosine[i*halfN+j] = cosf(frequency * ((float)j + 0.5));
            }
        }
        
        // compute mfcc from mel logs
        // create the cepstrum
        float *cepstrum = malloc(sizeof(float)*mfccCount);
        period = (float) halfN;
        float beta = 0.5f;
        // apply the melcosine filter
        for (int s=0; s < quantizeCount; s++) {
            for (int i = 0; i < mfccCount; i++) {
                if (halfN > 0) {
                    cepstrum[i] = 0.0f;
                    float *melcosine_i = &melcosine[i*halfN];
                    int j = 0;
                    cepstrum[i] += (beta * log10f(powf(spectrumBuffer[s*halfN+j], 2.0f)) * melcosine_i[j]);
                    for (j = 1; j < halfN; j++) {
                        cepstrum[i] += (log10f(powf(spectrumBuffer[s*halfN+j], 2.0f)) * melcosine_i[j]);
                    }
                    cepstrum[i] /= period;
                    mfccBuffer[s*mfccCount+i] = cepstrum[i];
                }
            }
        }        
        */
        
        // TAKE DCT OF ORIGINAL SPECTROGRAM
        // do fft instead
        float *myLogs = malloc(sizeof(float)*halfN);
        for (int s=0; s < quantizeCount; s++) {
            // convert to logs
            for (int i=0; i < halfN; i++)
                myLogs[i] = log10f(spectrumBuffer[s*halfN+i]);
            int myn = halfN;
            int myhalfN = myn/2;
            int mylog2n = log2(myn);
            // reflect around 0.5;
            vDSP_ctoz((COMPLEX*)&myLogs[0], 2, &complexArray, 1, myhalfN);
            vDSP_fft_zrip(setupReal, &complexArray, stride, mylog2n, FFT_INVERSE);
            for (int i=0; i < myhalfN; i++) {
                // get the complex number and convert
                float complex1 = complexArray.realp[i];
                float complex2 = complexArray.imagp[i];
                float sample = sqrt(powf(complex1, 2.0f) + powf(complex2, 2.0f));
                if (i < mfccCount) {
                    mfccBuffer[s*mfccCount+i] = sample;
                }
            }
//            mfccBuffer[s*mfccCount+0] = 0.0f;
        }
        free(myLogs);
        
        // apply lifter, make easy for now (simple cutoff)
        for (int s = 0; s < quantizeCount; s++) {
            for (int i=0; i < halfN/2; i++) {
//                if (i > 32)
//                    mfccBuffer[s*mfccCount+i] = 0.0f;
            }
        }
        
        // now run fft on liftered lower cepstral values to get formants
        int smoothN = halfN/2;
        int smoothHalfN = smoothN/2;
//        float *mySmooth = malloc(sizeof(float)*smoothHalfN);
        for (int s=0; s < quantizeCount; s++) {
            // convert to logs
            for (int i=0; i < smoothN; i++)
                myLogs[i] = mfccBuffer[s*mfccCount+i];
            int mylog2n = log2(smoothN);
            // reflect around 0.5;
            vDSP_ctoz((COMPLEX*)&myLogs[0], 2, &complexArray, 1, smoothHalfN);
            vDSP_fft_zrip(setupReal, &complexArray, stride, mylog2n, FFT_FORWARD);
//            for (int i=0; i < mfccCount; i++)
//                mfccBuffer[s*mfccCount+i] = 0.0f;
            for (int i=0; i < smoothHalfN; i++) {
                // get the complex number and convert
                float complex1 = complexArray.realp[i];
                float complex2 = complexArray.imagp[i];
                float sample = sqrt(powf(complex1, 2.0f) + powf(complex2, 2.0f));
                if (i < mfccCount) {
//                    mfccBuffer[s*mfccCount+i] = log10f(sample);
                    if (sample > 9999.0f) {
                        printf("s=%d,i=%d,sample=%f\r\n", s, i, sample);
                        for (int j=0; j < melChannels*4; j++)
                            printf("j=%d,mylog=%f\r\n", j, myLogs[j]);
                    }
                }
            }
        }
        [self normalizeAndScaleBuffer:mfccBuffer ofSize:quantizeCount*mfccCount];
        
        /*
        // now roll our own dct
        for (int s=0; s < quantizeCount; s++) {
            float mfcc[256];
            float total = 0.0f;
            for (int m=0; m < mfccCount; m++) {
                mfcc[m] = 0.0f;
                for (int k=0; k < melCount; k++) {
                    mfcc[m] += cos(m*(k+0.5f)*M_PI/(float)melCount)*melBuffer[s*melCount+k];
                }
                total += mfcc[m];
                mfccBuffer[s*mfccCount+m] = mfcc[m];
            }
        }        
        */
        
        /*
        // SPHINX DCT
        // TAKE DCT OF MEL LOGS
        // compute mel cosine filter bank
        float *melcosine = malloc(sizeof(float)*mfccCount*melCount);
        float period = (float) 2.0f * (float)melCount;
        for (int i = 0; i < mfccCount; i++) {
            float frequency = 2.0f * M_PI * i / period;
            for (int j = 0; j < melCount; j++) {
                melcosine[i*melCount+j] = cosf(frequency * ((float)j + 0.5));
            }
        }
        
        // compute mfcc from mel logs
        // create the cepstrum
        float *cepstrum = malloc(sizeof(float)*mfccCount);
        period = (float) melCount;
        float beta = 0.5;
        // apply the melcosine filter
        for (int s=0; s < quantizeCount; s++) {
            for (int i = 0; i < mfccCount; i++) {
                if (melCount > 0) {
                    cepstrum[i] = 0.0f;
                    float *melcosine_i = &melcosine[i*melCount];
                    int j = 0;
                    cepstrum[i] += (beta * melBuffer[s*melCount+j] * melcosine_i[j]);
                    for (j = 1; j < melCount; j++) {
                        cepstrum[i] += (melBuffer[s*melCount+j] * melcosine_i[j]);
                    }
                    cepstrum[i] /= period;
                    mfccBuffer[s*mfccCount+i] = cepstrum[i];
                }
            }
        }
         */
        
        
//        for (int i=0; i < mfccCount*quantizeCount; i++)
//            mfccBuffer[i] = powf(fabs(mfccBuffer[i]),2.0f);
        
//        for (int i=0; i < quantizeCount; i++)
//            mfccBuffer[i*mfccCount+0] = 0.0f;
//        [self normalizeBufferAtZero:mfccBuffer ofSize:mfccCount*quantizeCount];
        
//        [self normalizePositiveBuffer:mfccBuffer ofSize:mfccCount*quantizeCount usingMax:0.2f];
        
//        [self normalizeAndScaleBuffer:mfccBuffer ofSize:mfccCount*quantizeCount];
//        [self applyContrast:mfccBuffer ofWidth:quantizeCount andHeight:mfccCount notUsedPercentage:0.0f];
        
        /*
        // use mfcc space to compress mel down to 1/4
        for (int s=0; s < quantizeCount; s++) {
            int melPos = 0;
            for (int i=0; i < mfccCount; i++) {
                // compress
                mfccBuffer[s*mfccCount+i] = 0.0f;
                float total = 0.0f;
                for (int m=0; m < 4; m++) {
                    total += melBuffer[s*melCount+melPos];
//                    if (melBuffer[s*melCount+melPos] > mfccBuffer[s*mfccCount+i])
//                        mfccBuffer[s*mfccCount+i] = melBuffer[s*melCount+melPos];
                    melPos++;
                }
                mfccBuffer[s*mfccCount+i] = total / 4.0f;
            }
        }
        */
        
        
//        [self normalizeAndScaleBuffer:mfccBuffer ofSize:mfccCount*quantizeCount];
        
//        [self applyContrast:mfccBuffer ofWidth:quantizeCount andHeight:mfccCount notUsedPercentage:0.0f];
//        [self normalizePositiveBuffer:mfccBuffer ofSize:mfccCount*quantizeCount];
        
        /*
        // compile mfcc into one line to hopefully show strength of harmonics
		for (int s=0; s < quantizeCount; s++) {
            mfccBuffer[s*mfccCount+0] = 0;
            for (int i=mfccCount/4; i < mfccCount-(mfccCount/4); i++) {
                if (mfccBuffer[s*mfccCount+i] > mfccBuffer[s*mfccCount+0])
                    mfccBuffer[s*mfccCount+0] = mfccBuffer[s*mfccCount+i];
            }
        }
		for (int s=0; s < quantizeCount; s++) {
            for (int i=1; i < mfccCount; i++) {
                mfccBuffer[s*mfccCount+i] = 0.0f;
            }
        }
         */
//        [VoiceSample normalizePositiveBuffer:mfccBuffer ofSize:mfccCount*quantizeCount];
//        [self increaseContrastOfBuffer:mfccBuffer andSize:mfccCount*quantizeCount byPercent:5.0f usingCutoff:0.2f];
//        [self normalizePositiveBuffer:mfccBuffer ofSize:mfccCount*quantizeCount];
        
        
        
        if ([inUserProfile.name isEqualToString:@"Cpu"])
            [VoiceSample writeToImageWithFilename:[NSString stringWithFormat:@"compare1_nocms_mfcc.png"] alternateBuffer:mfccBuffer alternateFeatureCount:mfccCount alternateQuantizeCount:quantizeCount];
        else
            [VoiceSample writeToImageWithFilename:[NSString stringWithFormat:@"compare2_nocms_mfcc.png"] alternateBuffer:mfccBuffer alternateFeatureCount:mfccCount alternateQuantizeCount:quantizeCount];
        
        // END SPHINX DCT
        
        /*
        // try a small test here... apply contrast in small sections (same size as compare sections)
        [self normalizeAndScaleBuffer:melBuffer ofSize:melCount*quantizeCount];
        for (int s=0; s < quantizeCount; s += 12) {
            [self applyContrast:&melBuffer[s*melCount] ofWidth:quantizeCount-s<12?quantizeCount-s:12 andHeight:melCount notUsedPercentage:0.0f];
        }
        [self normalizeAndScaleBuffer:melBuffer ofSize:melCount*quantizeCount];
        */
        
        // apply contrast to mel buffer
        [self normalizeAndScaleBuffer:melBuffer ofSize:melCount*quantizeCount];
//        [VoiceSample applyContrast:melBuffer ofWidth:quantizeCount andHeight:melCount notUsedPercentage:[inUserProfile.name isEqualToString:@"Cpu"]?0.0f:1.0f];
//        [self normalizeAndScaleBuffer:melBuffer ofSize:melCount*quantizeCount];

        
        // apply constrast to spectrrum
        [self normalizeAndScaleBuffer:spectrumBuffer ofSize:halfN*quantizeCount];
        for (int i=0; i < 2; i++)
            [self blurBuffer:spectrumBuffer ofWidth:quantizeCount andHeight:halfN];
        [self normalizeAndScaleBuffer:spectrumBuffer ofSize:halfN*quantizeCount];
        
//        [VoiceSample applyContrast:spectrumBuffer ofWidth:quantizeCount andHeight:halfN notUsedPercentage:0.0f];
        
        // add to ongoing cepstral history
//        [inCepstralHistory addNewMfccData:spectrumBuffer ofLength:quantizeCount];
//        float *cepstralMeans = [inCepstralHistory getCepstralMeanArray];
        
        // try a quick CMN cepstral mean normalization
//        for (i=0; i < halfN; i++) {
//            for (int s=0; s < quantizeCount; s++) {
//                spectrumBuffer[s*halfN+i] -= cepstralMeans[i];
//            }
//        }
//        free(cepstralMeans);
        
//        [self normalizeAndScaleBuffer:spectrumBuffer ofSize:halfN*quantizeCount];
//        [self normalizeBufferAroundZeroToPoint5:spectrumBuffer ofSize:halfN*quantizeCount];
        
        // write out spectrum buffer
        if ([inUserProfile.name isEqualToString:@"Cpu"])
            [VoiceSample writeToImageWithFilename:[NSString stringWithFormat:@"compare1_nocms_spectrum.png"] alternateBuffer:spectrumBuffer alternateFeatureCount:[VoiceSample getN]/2 alternateQuantizeCount:quantizeCount];
        else
            [VoiceSample writeToImageWithFilename:[NSString stringWithFormat:@"compare2_nocms_spectrum.png"] alternateBuffer:spectrumBuffer alternateFeatureCount:[VoiceSample getN]/2 alternateQuantizeCount:quantizeCount];
        
        // write out mel buffer
        if ([inUserProfile.name isEqualToString:@"Cpu"])
            [VoiceSample writeToImageWithFilename:[NSString stringWithFormat:@"compare1_nocms_mel.png"] alternateBuffer:melBuffer alternateFeatureCount:melCount alternateQuantizeCount:quantizeCount];
        else
            [VoiceSample writeToImageWithFilename:[NSString stringWithFormat:@"compare2_nocms_mel.png"] alternateBuffer:melBuffer alternateFeatureCount:melCount alternateQuantizeCount:quantizeCount];
        
        
         /*
        // now roll dct on mel log buffer
        for (int s=0; s < quantizeCount; s++) {
            float mfcc[256];
            float total = 0.0f;
            for (int m=0; m < mfccCount; m++) {
                mfcc[m] = 0.0f;
                for (int k=0; k < melCount; k++) {
                    mfcc[m] += cos(m*(k+0.5f)*M_PI/(float)melCount)*melBuffer[s*melCount+k];
                }
                total += mfcc[m];
                mfccBuffer[s*mfccCount+m] = mfcc[m];
            }
        }        
         */

        /*
        // do fft instead
        float *myLogs = malloc(sizeof(float)*melChannels);
        for (int s=0; s < quantizeCount; s++) {
//        int log2n = 8;
//        int n = 1<<log2n;
            int myn = melChannels;
            int myhalfN = myn;
            int mylog2n = log2(myn);
            FFTSetup mySetupReal = vDSP_create_fftsetup(mylog2n, FFT_RADIX2);
            // reflect around 0.5;
            vDSP_ctoz((COMPLEX*)&melBuffer[s*melCount], 2, &complexArray, 1, myhalfN);
            vDSP_fft_zrip(mySetupReal, &complexArray, stride, mylog2n, FFT_INVERSE);
            float newFft[256];
            for (int i=0; i < mfccCount; i++)
                newFft[i] = 0.0f;
            for (int i=0; i < myn; i++) {
                // get the complex number and convert
                float complex1 = complexArray.realp[i];
                float complex2 = complexArray.imagp[i];
                float sample = sqrt(powf(complex1, 2.0f) + powf(complex2, 2.0f));
                newFft[i] = sample;
                if (i < mfccCount) {
                    mfccBuffer[s*mfccCount+i] = sample;
                    if (sample > 9999.0f) {
                        printf("s=%d,i=%d,sample=%f\r\n", s, i, sample);
                        for (int j=0; j < melChannels*4; j++)
                            printf("j=%d,mylog=%f\r\n", j, myLogs[j]);
                    }
                }
            }
            mfccBuffer[s*mfccCount+0] = 0.0f;
            vDSP_destroy_fftsetup(mySetupReal);
        }
        free(myLogs);
*/
    
        /* debugging
        // find min and max after invert
        float min = 9999.0f;
        float max = 0.0f;
        for (int s=0; s < quantizeCount; s++) {
            for (int i=0; i < mfccCount; i++) {
                if (mfccBuffer[s*mfccCount+i] > max)
                    max = mfccBuffer[s*mfccCount+i];
                if (mfccBuffer[s*mfccCount+i] < min)
                    min = mfccBuffer[s*mfccCount+i];
            }
        }
        printf("after mfcc, min is %f, max is %f\r\n", min, max);
        */

/*
		// print to debug window
		printf("Result of DCT:\n\r");
		for (int q=0; q < quantizeCount; q++) {
			for (i=0; i < mfccCount; i++) {
				printf("%4.4f ", imageBuffer[q*mfccCount+i]);
			}
			printf("\r\n");
		}
 */
		
/*		
		// use AVERAGE OVER UTTERANCES cepstral mean subtraction on each cepstrum
		// use the cepstral means passed in, and return new ones
		for (int m=0; m < mfccCount; m++) {
			float newCepstralMean = 0.0f;
			for (int s=0; s < quantizeCount; s++)
				newCepstralMean += imageBuffer[s*mfccCount+m];
			newCepstralMean /= quantizeCount;
			[newCepstralMeans addObject:[NSNumber numberWithFloat:newCepstralMean]];
			printf("new cepstral mean for mfcc %d is %f\r\n", m, newCepstralMean);
			NSNumber *cepstralMean = [useCepstralMeans objectAtIndex:m];
			printf("use cepstral mean for mfcc %d is %f\r\n", m, [cepstralMean floatValue]);
			for (int s=0; s < quantizeCount; s++) {
				imageBuffer[s*mfccCount+m] -= [cepstralMean floatValue];
			}
		}
*/
		
		// use cepstral mean subtraction on each cepstrum
		// use the cepstral means passed in, and return new ones
/*		if (useCms) {
			for (int m=0; m < mfccCount; m++) {
				float newCepstralMean = 0.0f;
				for (int s=0; s < quantizeCount; s++)
					newCepstralMean += mfccBuffer[s*mfccCount+m];
				newCepstralMean /= quantizeCount;
				printf("C-mean for mfcc %d is %f\r\n", m, newCepstralMean);
				for (int s=0; s < quantizeCount; s++) {
					mfccBuffer[s*mfccCount+m] -= newCepstralMean;
				}
			}
		}*/
        
        // normalize bands

        /*
		// high pass filter on cepstral values
		for (int m=0; m < mfccCount; m++) {
			float prevValue = 0.0f;
			for (int s=0; s < quantizeCount; s++) {
				float thisValue = mfccBuffer[s*mfccCount+m];
				if (s == 0)
					mfccBuffer[s*mfccCount+m] = 0.0f;
				else
                    mfccBuffer[s*mfccCount+m] = thisValue-prevValue + 0.97f * mfccBuffer[(s-1)*mfccCount+m];
//					mfccBuffer[s*mfccCount+m] = thisValue-0.95f*prevValue;
				prevValue = thisValue;
			}
		}
        */
        
		/*		
		// print to debug window
		printf("Result of RASTA:\n\r");
		for (int q=0; q < quantizeCount; q++) {
			for (i=0; i < mfccCount; i++) {
				printf("%4.4f ", imageBuffer[q*mfccCount+i]);
			}
			printf("\r\n");
		}
*/		
        
        /*
		// run ceptral mean subtraction
		if (inCepstralHistory != nil) {
			// add this cepstral information to history and get mean
			[inCepstralHistory addNewMfccData:mfccBuffer ofLength:quantizeCount];
			float *cepstralMeans = [inCepstralHistory getCepstralMeanArray];
			
			// subtract cepstral mean
			for (int m=0; m < mfccCount; m++) {
				float newCepstralMean = 0.0f;
				for (int s=0; s < quantizeCount; s++)
					newCepstralMean += mfccBuffer[s*mfccCount+m];
				newCepstralMean /= quantizeCount;
//				printf("Cepstral mean for mfcc %d is %f\r\n", m, newCepstralMean);
//				printf("Historical cepstral mean for mfcc %d is %f\r\n", m, cepstralMeans[m]);
				for (int s=0; s < quantizeCount; s++) {
                    if (![inUserProfile.name isEqualToString:@"Cpu"])
                    mfccBuffer[s*mfccCount+m] -= cepstralMeans[m];
				}
			}
			
			// free cepstral mean array
			free(cepstralMeans);
		}
*/
        
        
        /* debugging
        min = 9999.0f;
        max = 0.0f;
        for (int s=0; s < quantizeCount; s++) {
            for (int i=0; i < mfccCount; i++) {
                if (mfccBuffer[s*mfccCount+i] > max)
                    max = mfccBuffer[s*mfccCount+i];
                if (mfccBuffer[s*mfccCount+i] < min)
                    min = mfccBuffer[s*mfccCount+i];
            }
        }
        printf("after cepstral normalization, min is %f, max is %f\r\n", min, max);
        
        min = 9999.0f;
        max = 0.0f;
        for (int s=0; s < quantizeCount; s++) {
            for (int i=0; i < melCount; i++) {
                if (melBuffer[s*melCount+i] > max)
                    max = melBuffer[s*melCount+i];
                if (melBuffer[s*melCount+i] < min)
                    min = melBuffer[s*melCount+i];
            }
        }
        printf("before inv dct mel logs, min is %f, max is %f\r\n", min, max);
         */
        
        /*
        float invDct[256];
		for (int s=0; s < quantizeCount; s++) {
			for (int k=0; k < melCount; k++) {
				invDct[k] = 0.5f*mfccBuffer[s*mfccCount+0];
				for (int n=1; n < mfccCount; n++) {
					invDct[k] += cos(n*(k+0.5f)*(M_PI/(float)mfccCount))*mfccBuffer[s*mfccCount+n];
				}
			}
			for (int m=0; m < melCount; m++)
				melBuffer[s*melCount+m] = invDct[m];
		}
*/
        
        /* debugging
        min = 9999.0f;
        max = 0.0f;
        for (int s=0; s < quantizeCount; s++) {
            for (int i=0; i < melCount; i++) {
                if (melBuffer[s*melCount+i] > max)
                    max = melBuffer[s*melCount+i];
                if (melBuffer[s*melCount+i] < min)
                    min = melBuffer[s*melCount+i];
            }
        }
        printf("after inv dct mel logs, min is %f, max is %f\r\n", min, max);
        [self normalizeBufferAtZero:melBuffer ofSize:quantizeCount*melCount usingMax:4.0f];
        */
        
        printf("max mel log is %f, max power is %f, min power is %f\r\n", maxMelLog, maxMel, minMel);
        
//        [self normalizeBuffer:melBuffer ofSize:melCount*quantizeCount usingMax:maxMel];   
//        [self normalizePositiveBuffer:melBuffer ofSize:quantizeCount*melCount usingMax:maxMel];
//        [self normalizeBufferAroundZeroToPoint5:melBuffer ofSize:quantizeCount*melCount usingMax:5.0f];
//        [self applyContrast:melBuffer ofWidth:quantizeCount andHeight:melCount notUsedPercentage:0.0f];
//        [self normalizeAndScaleBuffer:melBuffer ofSize:quantizeCount*melCount];
         
        /*
        // normalize the buffer
		[self normalizeAndScaleBuffer:mfccBuffer ofSize:mfccCount*quantizeCount];
        
        // try test to match spectrum buffer 
        if (matchSpectrumBuffer != nil) {
            for (int i=0; i < featureCount; i++) {
                // loop through quantize and find total power for both
                float thisPower = 0.0f;
                float inPower = 0.0f;
                for (int s=0; s < quantizeCount; s++) {
                    thisPower += mfccBuffer[s*featureCount+i];
                    inPower += matchSpectrumBuffer[s*featureCount+i];
                }
                
                // find amount to boost/lower this by
                float boost = inPower / thisPower;
                
                // now boost this spectrum buffer
                for (int s=0; s < quantizeCount; s++) {
                    mfccBuffer[s*featureCount+i] *= boost;
                }
            }
        }
        */
        
        /*
        // find min and max after invert
        float min = 9999.0f;
        float max = 0.0f;
		for (int s=0; s < quantizeCount; s++) {
			for (int i=9; i < usedMfccCount; i++) {
                if (mfccBuffer[s*featureCount+i] > max)
                    max = mfccBuffer[s*featureCount+i];
                if (mfccBuffer[s*featureCount+i] < min)
                    min = mfccBuffer[s*featureCount+i];
            }
        }
        printf("after inverse dct, min is %f, max is %f\r\n", min, max);
        */
        
        /*
        for (int i=0; i < quantizeCount; i++) {
            mfccBuffer[i*mfccCount+0] = 0.0f;
            for (int s=8; s < mfccCount; s++)
                mfccBuffer[i*mfccCount+s] = 0.0f;
        }
        */
        
        /*
        // normalize the mfcc buffer
//        [self normalizeBufferAroundZeroToPoint5:mfccBuffer ofSize:mfccCount*quantizeCount usingMax:3.0f];
//        [self normalizeBufferAroundZeroToPoint5:mfccBuffer ofSize:mfccCount*quantizeCount];
        for (int i=0; i < quantizeCount; i++)
            for (int m=0; m < mfccCount; m++) {
                if (m == 0 || m > 20)
                    mfccBuffer[i*mfccCount+m] = 0.0f;
            }
        
        float min = 9999.0f;
        float max = 0.0f;
        for (int s=0; s < quantizeCount; s++) {
            for (int i=0; i < mfccCount; i++) {
                if (mfccBuffer[s*mfccCount+i] > max)
                    max = mfccBuffer[s*mfccCount+i];
                if (mfccBuffer[s*mfccCount+i] < min)
                    min = mfccBuffer[s*mfccCount+i];
            }
        }
        printf("mfcc before normalize, min is %f, max is %f\r\n", min, max);
        [VoiceSample normalizeBufferAroundZeroToPoint5:mfccBuffer ofSize:mfccCount*quantizeCount usingMax:0.5f];
//        [self normalizeAndScaleBuffer:mfccBuffer ofSize:mfccCount*quantizeCount];

        // calculate moving average and then overwrite to mfccbuffer
        float *mfccMa = malloc(sizeof(float)*quantizeCount*mfccCount);
        for (int s=0; s < quantizeCount; s++) {
            for (int i=0; i < mfccCount; i++) {
                float p1 = 0.0f;
//                float p2 = 0.0f;
                for (int m=-2; m < 2; m++)
                    p1 += mfccBuffer[MIN(MAX(0,s+m),quantizeCount-1)*mfccCount+i];
                mfccMa[s*mfccCount+i] = p1/4.0f;
            }
        }
        for (int s=0; s < quantizeCount; s++) {
            for (int i=0; i < mfccCount; i++) {
                mfccBuffer[s*mfccCount+i] = mfccMa[s*mfccCount+i];
            }
        }
        free(mfccMa);
        
        // calculate velocity using mfcc
        float *mfccVelocityBuffer = malloc(sizeof(float)*quantizeCount*mfccCount);
        for (int s=-0; s < quantizeCount-0; s++) {
            for (int i=0; i < mfccCount; i++) {
                float p1 = 0.0f;
                float p2 = 0.0f;
                for (int m=-2; m < 2; m++) {
                    p1 += (float)m * (mfccBuffer[MIN(MAX(0,s+m),quantizeCount-1)*mfccCount+i]-mfccBuffer[MIN(MAX(0,s-m),quantizeCount-1)*mfccCount+i]);
                    p2 += powf((float)m, 2.0f);
                }
                mfccVelocityBuffer[(s+0)*mfccCount+i] = p1/(2*p2);
//                mfccVelocityBuffer[(s+0)*mfccCount+i] = s<0?0.5f:mfccBuffer[MIN(MAX(0,s),quantizeCount-1)*mfccCount+i];
            }
        }
        
        // calculate velocity of prior n samples
        float *mfccVelocityBuffer2 = malloc(sizeof(float)*quantizeCount*mfccCount);
        for (int s=-4; s < quantizeCount-4; s++) {
            for (int i=0; i < mfccCount; i++) {
                float p1 = 0.0f;
                float p2 = 0.0f;
                for (int m=-2; m < 2; m++) {
                    p1 += (float)m * (mfccBuffer[MIN(MAX(0,s+m),quantizeCount-1)*mfccCount+i]-mfccBuffer[MIN(MAX(0,s-m),quantizeCount-1)*mfccCount+i]);
                    p2 += powf((float)m, 2.0f);
                }
                mfccVelocityBuffer2[(s+4)*mfccCount+i] = p1/(2*p2);
//                mfccVelocityBuffer2[(s+4)*mfccCount+i] = s<0?0.5f:mfccBuffer[MIN(MAX(0,s),quantizeCount-1)*mfccCount+i];
            }
        }
        float *mfccVelocityBuffer3 = malloc(sizeof(float)*quantizeCount*mfccCount);
        for (int s=-8; s < quantizeCount-8; s++) {
            for (int i=0; i < mfccCount; i++) {
                float p1 = 0.0f;
                float p2 = 0.0f;
                for (int m=-2; m < 2; m++) {
                    p1 += (float)m * (mfccBuffer[MIN(MAX(0,s+m),quantizeCount-1)*mfccCount+i]-mfccBuffer[MIN(MAX(0,s-m),quantizeCount-1)*mfccCount+i]);
                    p2 += powf((float)m, 2.0f);
                }
                mfccVelocityBuffer3[(s+8)*mfccCount+i] = p1/(2*p2);
//                mfccVelocityBuffer3[(s+8)*mfccCount+i] = s<0?0.5f:mfccBuffer[MIN(MAX(0,s),quantizeCount-1)*mfccCount+i];
            }
        }
        
        // calculate velocity using mel
        float *melVelocityBuffer = malloc(sizeof(float)*quantizeCount*melCount);
        for (int s=0; s < quantizeCount; s++) {
            for (int i=0; i < melCount; i++) {
                float p1 = 0.0f;
                float p2 = 0.0f;
//                for (int m=-5; m < 5; m++) {
                for (int m=-10; m < 0; m++) {
                    p1 += (float)m * (melBuffer[MIN(MAX(0,s+m),quantizeCount-1)*melCount+i]-melBuffer[MIN(MAX(0,s-m),quantizeCount-1)*melCount+i]);
                    p2 += powf((float)m, 2.0f);
                }
                melVelocityBuffer[s*melCount+i] = p1/(2.0f*p2);
            }
        }
         */
        
        /*
        float myMin = 9999.0f;
        float myMax = 0.0f;
        for (int i=0; i < quantizeCount*melCount; i++) {
            if (melVelocityBuffer[i] > myMax)
                myMax = melVelocityBuffer[i];
            if (melVelocityBuffer[i] < myMin)
                myMin = melVelocityBuffer[i];
        }
        for (int i=0; i < quantizeCount*mfccCount; i++) {
            if (mfccVelocityBuffer[i] > myMax)
                myMax = mfccVelocityBuffer[i];
            if (mfccVelocityBuffer[i] < myMin)
                myMin = mfccVelocityBuffer[i];
        }
        */
        
        // normalize velocity buffers
        /*
        [VoiceSample normalizeBufferAroundZeroToPoint5:mfccVelocityBuffer ofSize:mfccCount*quantizeCount usingMax:0.15f];
        [VoiceSample normalizeBufferAroundZeroToPoint5:mfccVelocityBuffer2 ofSize:mfccCount*quantizeCount usingMax:0.15f];
        [VoiceSample normalizeBufferAroundZeroToPoint5:mfccVelocityBuffer3 ofSize:mfccCount*quantizeCount usingMax:0.15f];
//        [self normalizeAndScaleBuffer:mfccVelocityBuffer ofSize:mfccCount*quantizeCount];
        [VoiceSample normalizeBufferAroundZeroToPoint5:melVelocityBuffer ofSize:melCount*quantizeCount usingMax:0.15f];
        */
        
        /*
        // calculate distance... used to calculate phoneme boundaries
        // must be done after normalization of mfcc
        float *distanceBuffer = malloc(sizeof(float)*quantizeCount);
        int j=1;
        for (int s=0; s < quantizeCount; s++) {
            float sum = 0.0f;
            for (int i=0; i < 6; i++) {
                if (!(s-j < 0 || s > quantizeCount-1))
                    sum += mfccBuffer[(s-j)*mfccCount+i]-mfccBuffer[(s)*mfccCount+i];
            }
            distanceBuffer[s] = fabs(sum);
        }
        [VoiceSample normalizePositiveBuffer:distanceBuffer ofSize:quantizeCount];
        */
        
        /*
        // quantize mfcc buffer per segment
        for (int s=0; s < quantizeCount; s++) {
            float max = 0.0f;
            for (int i=1; i < mfccCount; i++) {
                if (mfccBuffer[s*mfccCount+i] > max)
                    max = mfccBuffer[s*mfccCount+i];
            }
            for (int i=1; i < mfccCount; i++)
                mfccBuffer[s*mfccCount+i] = mfccBuffer[s*mfccCount+i] / max;
            
        }
        */
        
        /*
        // calculate moving for power buffer and then overwrite to mfccbuffer
        float *powerMa = malloc(sizeof(float)*quantizeCount);
        for (int s=0; s < quantizeCount; s++) {
            float p1 = 0.0f;
            for (int m=-2; m < 2; m++)
                p1 += powerBuffer[MIN(MAX(0,s+m),quantizeCount-1)];
            powerMa[s] = p1/4.0f;
        }
        for (int s=0; s < quantizeCount; s++) {
            powerBuffer[s] = powerMa[s];
        }
        for (int s=0; s < quantizeCount; s++) {
            float p1 = 0.0f;
            for (int m=-2; m < 2; m++)
                p1 += maxPowerBuffer[MIN(MAX(0,s+m),quantizeCount-1)];
            powerMa[s] = p1/4.0f;
        }
        for (int s=0; s < quantizeCount; s++) {
            maxPowerBuffer[s] = powerMa[s];
        }
        free(powerMa);
        */
        
        /*
        // convert power buffer to angle
        [self normalizePositiveBuffer:powerBuffer ofSize:quantizeCount];
		for (int s=0; s < quantizeCount; s++) {
            float ax = 1.0f;
            float ay = 0.0f;
            float bx = 1.0f;
            float by = powerBuffer[s]-(s-1<0?0.0f:powerBuffer[s-1]);
            float alength = sqrtf(powf(ax, 2.0f)+powf(ay, 2.0f));
            float blength = sqrtf(powf(bx, 2.0f)+powf(by, 2.0f));
            ax /= alength;
            ay /= alength;
            bx /= blength;
            by /= blength;
            float angle = acosf(ax*bx+ay*by);
            if (by < 0.0f)
                angle = -angle;
            maxPowerBuffer[s] = (alength==0.0f||blength==0.0f)?0.0f:angle;
        }
        for (int s=0; s < quantizeCount; s++)
            powerBuffer[s] = maxPowerBuffer[s];
         [self normalizeBufferAroundZeroToPoint5:powerBuffer ofSize:quantizeCount];
        */
        
        // normalize the power buffer here, after we calculate moving average!
//        [VoiceSample normalizePositiveBuffer:powerBuffer ofSize:quantizeCount];
//        [VoiceSample normalizePositiveBuffer:maxPowerBuffer ofSize:quantizeCount];
        
        
        // create a harmonic buffer
        float *harmonicBuffer = malloc(sizeof(float)*quantizeCount);
        for (int s=0; s < quantizeCount; s++) {
            float total = 0.0f;
            float max = 0.0f;
            for (int i=mfccCount/4; i < mfccCount-(mfccCount/4); i++) {
                total += mfccBuffer[s*mfccCount+i];
                if (mfccBuffer[s*mfccCount+i] > max)
                    max = mfccBuffer[s*mfccCount+i];
            }
            harmonicBuffer[s] = max;
        }
        [VoiceSample normalizePositiveBuffer:harmonicBuffer ofSize:quantizeCount];
        /*
        for (i=0; i < 5; i++) {
            [self blurBuffer:harmonicBuffer ofWidth:quantizeCount andHeight:1];
        }
        [VoiceSample normalizePositiveBuffer:harmonicBuffer ofSize:quantizeCount];
        [VoiceSample normalizePositiveBuffer:volumeBuffer ofSize:quantizeCount];
        */
        
        //
		// copy stuff to feature buffer
		//
		
		// copy mfccs and deltas to feature buffer and free
		for (int s=0; s < quantizeCount; s++) {
            // copy mel buffer
            int featurePos = 0;
            /*
            for (int i=0; i < featureCount-20; i++) {
                featureBuffer[s*featureCount+i] = melBuffer[s*melCount+i];
                featurePos++;
            }*/

            for (int i=0; i < 6; i++) {
                featureBuffer[s*featureCount+featurePos] = spectrumBuffer[s*halfN+i+1];
                featurePos++;
            }
//            for (int i=0; i < 4; i++) {
//                featureBuffer[s*featureCount+featurePos] = volumeBuffer[s];
//                featurePos++;
//            }
            
            /*
            for (int i=0; i < 20; i++) {
                featureBuffer[s*featureCount+featurePos] = harmonicBuffer[s];
                featurePos++;
            }
            for (int i=0; i < 20; i++) {
                featureBuffer[s*featureCount+featurePos] = volumeBuffer[s];
                featurePos++;
            }
            */
            
//            for (int i=0; i < melCount-10; i++)
//                featureBuffer[s*featureCount+i] = melBuffer[s*melCount+(i+10)];
            featurePos += melCount;
            
            // copy line 2 from mfcc (which represents harmonic strength)
//            featureBuffer[s*featureCount+featurePos] = mfccBuffer[s*mfccCount+1];
//            featurePos++;
            
            // copy previous 'n' amplitude values to get last 'n' seconds ADSR window for non-
            // harmonic envelope (try 6 either side for starters)
            /*
            for (int i=-6; i < 6; i++) {
//                featureBuffer[s*featureCount+featurePos] = powerBuffer[s];
                featureBuffer[s*featureCount+featurePos] = (s+i<0||s+i>quantizeCount?0.0f:powerBuffer[s+i]);
                featurePos++;
            }
            */

            // copy power to position 7
//            featureBuffer[s*featureCount+featurePos++] = maxPowerBuffer[s];
            
            /*
            // copy max power to position 8
            featureBuffer[s*featureCount+featurePos++] = powerBuffer[s];
            
            // now historically show last 6 powers
            for (int i=1; i < 5; i++) {
                featureBuffer[s*featureCount+featurePos++] = s-i<0?0.0f:powerBuffer[s-i];
            }
            */
            
            /*
            for (int i=0; i < compareFeatureCount; i++)
//                    featureBuffer[s*featureCount+i] = melVelocityBuffer[s*melCount+i-(featureCount/2)];
                    featureBuffer[s*featureCount+featurePos++] = mfccVelocityBuffer[s*mfccCount+i+1];
            for (int i=0; i < compareFeatureCount; i++)
                featureBuffer[s*featureCount+featurePos++] = mfccVelocityBuffer2[s*mfccCount+i+1];
            for (int i=0; i < compareFeatureCount; i++)
                featureBuffer[s*featureCount+featurePos++] = mfccVelocityBuffer3[s*mfccCount+i+1];
             */
		}
        
		// free the fft result buffer
		free(resultBuffer);
		free(harmonicStrength);
        /*
        free(mfccVelocityBuffer);
        free(mfccVelocityBuffer2);
        free(mfccVelocityBuffer3);
        free(melVelocityBuffer);
         */
		
		// free complex data
		free(complexArray.realp);
		free(complexArray.imagp);
		
		// free high pass buffer 
		free(hpBuffer);
	}
	return self;
}

/*
-(float*)getFeatureBufferFromSpectrogram:(float*)inSpectrumBuffer ofWidth:(int)inWidth andHeight:(int)inHeight {
    // get application delegate
    McpAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    
    // get fft matrix
    float *melMatrix = [appDelegate getMelMatrix];
    
    // create memory for feature buffer
    featureCount = [VoiceSample getMfccCount];
    int melChannels = featureCount;
    float *myFeatureBuffer = malloc(sizeof(float)*inWidth*featureCount);
    
    // post process the spectrum buffer into mfcc
    float *resultBuffer = malloc(sizeof(float)*inHeight);
    for (int s=0; s < inWidth; s++) {
        for (int i=0; i < inHeight; i++) {
            float sample = inSpectrumBuffer[s*inHeight+i];
            
            // convert to db
            sample = powf(sample, 2.0f);
            resultBuffer[i] = sample;
        }
        
        // calculate powers for each of the mel channels
        float melPower[128]; // max 40 channels
        for (int c=0; c < melChannels; c++) {
            melPower[c] = 0.0f;
            for (int i=0; i < inHeight; i++) {
                melPower[c] += resultBuffer[i] * melMatrix[c*inHeight+i];
            }
        }
        
        // compress using logarithms
        float melLogs[128];
        for (int i=0; i < melChannels; i++) {
            melLogs[i] = log10(melPower[i]);
        }
                
        // update output buffer
        for (int i=0; i < inHeight; i++)
            myFeatureBuffer[s*inHeight+i] = 10 * melLogs[i];
    } 
    free(resultBuffer);
    
    // normalize the buffer
    [self normalizeAndScaleBuffer:myFeatureBuffer ofSize:inWidth*featureCount];
    
    // try and apply contrast
    [self applyContrast:myFeatureBuffer ofWidth:inWidth andHeight:featureCount notUsedPercentage:0.0f];
    
    // return feature buffer
    return myFeatureBuffer;
}
*/

-(float*)getSpectrogramFromBuffer:(float*)hpBuffer ofLength:(int)inLength quantizeTo:(int)inQuantizeCount {
    // get application delegate
    McpAppDelegate *appDelegate = (McpAppDelegate*)[[UIApplication sharedApplication] delegate];
    
    // set up fft parameters
    UInt32 log2n = [VoiceSample getLog2N];
    UInt32 n = [VoiceSample getN];
    UInt32 halfN = n/2;
    UInt32 stride = 1;
    FFTSetup setupReal = [appDelegate getFftSetup];
    int window = n;
    int stepSize = (inLength-window) / inQuantizeCount;
    
    // allocate memory for complex array
    COMPLEX_SPLIT complexArray;
    complexArray.realp = (float*)malloc(halfN*sizeof(float));
    complexArray.imagp = (float*)malloc(halfN*sizeof(float));
    
    // first run fft on buffer
    // create data point for each quantize segment
    float *mySpectrumBuffer = malloc(sizeof(float)*halfN*inQuantizeCount);
    float TWOPI = 2.0f * M_PI;
    for (int s=0; s < inQuantizeCount; s++) {
        // copy buffer data into a seperate array and apply hamming window
        int offset = (int)(s * stepSize);
        float *hamBuffer = malloc(n*sizeof(float));
        for (int i=0; i < n; i++)
            hamBuffer[i] = hpBuffer[offset+i] * ((1.0f-0.46f) - 0.46f*cos(TWOPI*i/((float)n-1.0f)));
        
        // configure float array into acceptable input array format (interleaved)
        vDSP_ctoz((COMPLEX*)hamBuffer, 2, &complexArray, 1, halfN);
        
        // free ham buffer
        free(hamBuffer);
        
        // run FFT
        vDSP_fft_zrip(setupReal, &complexArray, stride, log2n, FFT_FORWARD);
        
        // convert complex data into an array of real floats
        // find max and total amplitude at the same time
        for (int i=0; i < halfN; i++) {
            // get the complex number and convert
            float complex1 = complexArray.realp[i];
            float complex2 = complexArray.imagp[i];
            float sample = sqrt(powf(complex1, 2) + powf(complex2, 2));
            
            // save spectrum for debugging
            mySpectrumBuffer[s*halfN+i] = sample;
        }
    }
    
    // quantize
    [self normalizeBufferAtZero:mySpectrumBuffer ofSize:inQuantizeCount*halfN];
    
    return mySpectrumBuffer;
}

+(void)getLead:(float*)leadPercent andTail:(float*)tailPercent fromBuffer:(float*)hpBuffer ofLength:(int)inLength overrideNoiseLevel:(float)inNoiseLevel {
    // set up fft parameters
    int fftWindowSize = [VoiceSample getN];
    fftWindowSize = fftWindowSize/3;
    int uncutQuantizeCount = round((float)inLength/(float)fftWindowSize);
    UInt32 n = [VoiceSample getN];
//    FFTSetup setupReal = [appDelegate getFftSetup];
    int window = n;
    int stepSize = (inLength-window) / uncutQuantizeCount;
    
    // calculate volume from raw samples, because it seems more reliable
    float *mySpectrumAvgBuffer = malloc(sizeof(float)*uncutQuantizeCount);
    int windowPos = 0;
    for (int i=0; i < uncutQuantizeCount; i++) {
        windowPos += stepSize;
        float total = 0.0f;
        float max = 0.0f;
        for (int p=windowPos; p < windowPos+fftWindowSize; p++) {
            total += hpBuffer[p];
            if (hpBuffer[p] > max)
                max = hpBuffer[p];
        }
        mySpectrumAvgBuffer[i] = max;
    }
    [self normalizePositiveBuffer:mySpectrumAvgBuffer ofSize:uncutQuantizeCount];
    
    
    // take the average of the first and last 2 samples as noise level
    float startNoiseLevel = (mySpectrumAvgBuffer[0]+mySpectrumAvgBuffer[1])/2.0f;
    float endNoiseLevel = (mySpectrumAvgBuffer[uncutQuantizeCount-1]+mySpectrumAvgBuffer[uncutQuantizeCount-2])/2.0f;
    float noiseLevel = MAX(startNoiseLevel, endNoiseLevel);
    printf("start noise level is %f, end noise level is %f\r\n", startNoiseLevel, endNoiseLevel);
    
    // now find lead
    int leadPos = 0;
    for (int i=0; i < uncutQuantizeCount; i++) {
        if (mySpectrumAvgBuffer[i] > (inNoiseLevel>0.0f?inNoiseLevel:MAX(0.1f,noiseLevel*4.0f))) {
            leadPos = i;
            break;
        }
    }
    // now find tail
    int tailPos = 0;
    for (int i=uncutQuantizeCount-1; i >= 0; i--) {
        if (mySpectrumAvgBuffer[i] > (inNoiseLevel>0.0f?inNoiseLevel:MAX(0.1f,noiseLevel*4.0f))) {
            tailPos = i;
            break;
        }
    }
    
    // set variables as percentage
    *leadPercent = (float)leadPos / (float)uncutQuantizeCount;
    *tailPercent = (float)tailPos / (float)uncutQuantizeCount;
}

// use fft spectrogram to find lead and fail
// and return array of silence
-(NSArray*)getLead:(float*)leadPercent andTail:(float*)tailPercent fromBuffer:(float*)hpBuffer ofLength:(int)inLength overrideNoiseLevel:(float)inNoiseLevel fromUser:(UserProfile*)inUserProfile {
    // get application delegate
//    McpAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    
    
    // set up fft parameters
    int fftWindowSize = [VoiceSample getN];
    fftWindowSize = fftWindowSize/3;
    int uncutQuantizeCount = round((float)inLength/(float)fftWindowSize);
//    UInt32 log2n = [VoiceSample getLog2N];
    UInt32 n = [VoiceSample getN];
//    UInt32 halfN = n/2;
//    UInt32 stride = 1;
//    FFTSetup setupReal = [appDelegate getFftSetup];
    int window = n;
    int stepSize = (inLength-window) / uncutQuantizeCount;
    
    // without FFT is more reliable
    /*
    // allocate memory for complex array
    COMPLEX_SPLIT complexArray;
    complexArray.realp = (float*)malloc(halfN*sizeof(float));
    complexArray.imagp = (float*)malloc(halfN*sizeof(float));
    
    // first run fft on buffer
    // create data point for each quantize segment
    float *mySpectrumBuffer = malloc(sizeof(float)*uncutQuantizeCount*halfN);
    float TWOPI = 2.0f * M_PI;
    for (int s=0; s < uncutQuantizeCount; s++) {
        // copy buffer data into a seperate array and apply hamming window
        int offset = (int)(s * stepSize);
        float *hamBuffer = malloc(n*sizeof(float));
        for (int i=0; i < n; i++)
            hamBuffer[i] = hpBuffer[offset+i] * ((1.0f-0.46f) - 0.46f*cos(TWOPI*i/((float)n-1.0f)));
        
        // configure float array into acceptable input array format (interleaved)
        vDSP_ctoz((COMPLEX*)hamBuffer, 2, &complexArray, 1, halfN);
        
        // free ham buffer
        free(hamBuffer);
        
        // run FFT
        vDSP_fft_zrip(setupReal, &complexArray, stride, log2n, FFT_FORWARD);
        
        // convert complex data into an array of real floats
        // find max and total amplitude at the same time
        for (int i=0; i < halfN; i++) {
            // get the complex number and convert
            float complex1 = complexArray.realp[i];
            float complex2 = complexArray.imagp[i];
            float sample = sqrt(powf(complex1, 2) + powf(complex2, 2));
            
            // save spectrum
//            sample = powf(sample, 2.0f);
            mySpectrumBuffer[s*halfN+i] = sample;
        }
    }
    
     */
    
    // add raw, untrimmed spectrum data to ongoing history buffer
//    [inUserProfile.bufferHistory addNewData:mySpectrumBuffer ofLength:uncutQuantizeCount];
    
    // don't do this mismatch stuff for now
    /*
    // if this sample is from the CPU
    if (inNoiseLevel > 0.0f) {
        // find mismatch between frequency response in generated voice versus
        // the microphone
        float *userBands = [appDelegate.currentUserProfile.bufferHistory getRowTotals];
        float *cpuBands = [appDelegate.currentCpuProfile.bufferHistory getRowTotals];
        for (int i=0; i < halfN; i++) {
            // compare 2 to get boost
            float boost = userBands[i] / cpuBands[i];
            
            // now boost this spectrum buffer
            for (int s=0; s < uncutQuantizeCount; s++) {
                mySpectrumBuffer[s*halfN+i] *= boost;
            }
        }
    }
    */
    
    /*
    // get max and average of spectrum buffer
    float *mySpectrumAvgBuffer = malloc(sizeof(float)*uncutQuantizeCount);
    float *mySpectrumMaxBuffer = malloc(sizeof(float)*uncutQuantizeCount);
    float *mySpectrumTotBuffer = malloc(sizeof(float)*uncutQuantizeCount);
    for (int s=0; s < uncutQuantizeCount; s++) {
        mySpectrumMaxBuffer[s] = 0.0f;
        mySpectrumAvgBuffer[s] = 0.0f;
        for (int i=0; i < halfN; i++) {
            // save spectrum for debugging
            float sample = mySpectrumBuffer[s*halfN+i];
            mySpectrumAvgBuffer[s] += sample;
            mySpectrumTotBuffer[s] += sample;
            if (sample > mySpectrumMaxBuffer[s])
                mySpectrumMaxBuffer[s] = sample;
        }
        mySpectrumAvgBuffer[s] /= halfN;
    }
        
    // quantize
    [self normalizePositiveBuffer:mySpectrumMaxBuffer ofSize:uncutQuantizeCount];
    [self normalizePositiveBuffer:mySpectrumAvgBuffer ofSize:uncutQuantizeCount];
    
    [self normalizeAndScaleBuffer:mySpectrumBuffer ofSize:uncutQuantizeCount*halfN];
    [VoiceSample applyContrast:mySpectrumBuffer ofWidth:uncutQuantizeCount andHeight:halfN notUsedPercentage:0.0f];
    [self normalizeAndScaleBuffer:mySpectrumBuffer ofSize:uncutQuantizeCount*halfN];
    */
    
    // calculate volume from raw samples, because it seems more reliable
    float *mySpectrumAvgBuffer = malloc(sizeof(float)*uncutQuantizeCount);
    int windowPos = 0;
    for (int i=0; i < uncutQuantizeCount; i++) {
        windowPos += stepSize;
        float total = 0.0f;
        float max = 0.0f;
        for (int p=windowPos; p < windowPos+fftWindowSize; p++) {
            total += hpBuffer[p];
            if (hpBuffer[p] > max)
                max = hpBuffer[p];
        }
        mySpectrumAvgBuffer[i] = max;
    }
    [VoiceSample normalizePositiveBuffer:mySpectrumAvgBuffer ofSize:uncutQuantizeCount];
    
    
    /*
    // write for debugging
    if ([inUserProfile.name isEqualToString:@"Cpu"])
        [VoiceSample writeToImageWithFilename:[NSString stringWithFormat:@"compare1_nocms_pretrim.png"] alternateBuffer:mySpectrumBuffer alternateFeatureCount:halfN alternateQuantizeCount:uncutQuantizeCount];
    else
        [VoiceSample writeToImageWithFilename:[NSString stringWithFormat:@"compare2_nocms_pretrim.png"] alternateBuffer:mySpectrumBuffer alternateFeatureCount:halfN alternateQuantizeCount:uncutQuantizeCount];
    
    for (int i=0; i < uncutQuantizeCount; i++)
        printf("%d=%f\r\n", i, mySpectrumAvgBuffer[i]);
    */
    
    // try and apply contrast
//    [self applyContrast:mySpectrumAvgBuffer ofWidth:uncutQuantizeCount andHeight:1];
//    [self applyContrast:mySpectrumMaxBuffer ofWidth:uncutQuantizeCount andHeight:1];

//    for (int i=0; i < uncutQuantizeCount; i++)
//        printf("spectrum channel %d max is %f, avg is %f\r\n", i, mySpectrumMaxBuffer[i], mySpectrumAvgBuffer[i]);
    
    // take the average of the first and last 2 samples as noise level
    float startNoiseLevel = (mySpectrumAvgBuffer[0]+mySpectrumAvgBuffer[1])/2.0f;
    float endNoiseLevel = (mySpectrumAvgBuffer[uncutQuantizeCount-1]+mySpectrumAvgBuffer[uncutQuantizeCount-2])/2.0f;
    float noiseLevel = MAX(startNoiseLevel, endNoiseLevel);
    printf("start noise level is %f, end noise level is %f\r\n", startNoiseLevel, endNoiseLevel);
    
    // now find lead
    int leadPos = 0;
    for (int i=0; i < uncutQuantizeCount; i++) {
        if (mySpectrumAvgBuffer[i] > (inNoiseLevel>0.0f?inNoiseLevel:MAX(0.1f,noiseLevel*4.0f))) {
            leadPos = i;
            break;
        }
    }
    // now find tail
    int tailPos = 0;
    for (int i=uncutQuantizeCount-1; i >= 0; i--) {
        if (mySpectrumAvgBuffer[i] > (inNoiseLevel>0.0f?inNoiseLevel:MAX(0.1f,noiseLevel*4.0f))) {
            tailPos = i;
            break;
        }
    }
//    leadPos = 0;
//    tailPos = uncutQuantizeCount-1;
    
    // also build a list of contigous segments of silence to remove or shorten
    // because these make a huge difference in the compare calc and silence duration
    // really shouldn't impact the result that much
    NSMutableArray *silenceList = [NSMutableArray arrayWithCapacity:10];
    int contiguous = 0;
    for (int i=0; i < uncutQuantizeCount; i++) {
        if (mySpectrumAvgBuffer[i] < (inNoiseLevel>0.0f?inNoiseLevel:MAX(0.1f,noiseLevel*3.0f))) {
            contiguous++;
        }
        else {
            if (contiguous > 0) {
                // ignore is contiguous is less than 2 or we're at the start
//                printf("contiguous silence of %d, ends at position %d\r\n", contiguous, i);
                if (contiguous > 1 && i-contiguous > 0) {
                    // start position and length
                    NSNumber *silenceStart = [NSNumber numberWithFloat:(float)(i-contiguous)/(float)uncutQuantizeCount];
                    NSNumber *silenceLength = [NSNumber numberWithFloat:(float)contiguous/(float)uncutQuantizeCount];
                    [silenceList addObject:silenceStart];
                    [silenceList addObject:silenceLength];
                    printf("silence length %f%%, found at %f%%\r\n", [silenceLength floatValue], [silenceStart floatValue]);
                }
                
                // reset contigous
                contiguous = 0;
            }
        }
    }
    
    // free spectrum buffer
    free(mySpectrumAvgBuffer);
    /*
    free(mySpectrumMaxBuffer);
    free(mySpectrumTotBuffer);
    free(mySpectrumBuffer);
    
    // free complex data
    free(complexArray.realp);
    free(complexArray.imagp);
    */
    
    // set variables as percentage
    *leadPercent = (float)leadPos / (float)uncutQuantizeCount;
    *tailPercent = (float)tailPos / (float)uncutQuantizeCount;
    
    return silenceList;
}

-(int*)getPhonemeBreakPositionsForCount:(int)phonemeCount {
	// find the (phonemecount-1) largest delta's
	// first calculate overall delta value
	NSMutableArray *deltaArray = [NSMutableArray arrayWithCapacity:10];
	for (int s=0; s < quantizeCount; s++) {
		float total = 0.0f;
		for (int i=0; i < mfccCount; i++) {
			if (i != 0) {
				total += fabs(deltaBuffer[s*mfccCount+i]);
				if (deltaBuffer[s*mfccCount+i] > 0.5f)
					total += deltaBuffer[s*mfccCount+i] - 0.5f;
				else
					total += 0.5f - deltaBuffer[s*mfccCount+i];
			}
		}
		total /= (mfccCount-1);
		FloatIndex *newIndex = [[FloatIndex alloc] initWithFloatValue:total indexValue:s];
		[deltaArray addObject:newIndex];
		[newIndex release];
	}
	
	// find largest values that are at least 1 pixel apart
	// sort by size into array of indexes
	[deltaArray sortUsingSelector:@selector(compareFloat:)];
	
	// loop through index array
	int pos=0;
	NSMutableArray *foundArray = [NSMutableArray arrayWithCapacity:10];
	while(foundArray.count < phonemeCount-1) {
		// if next index is too close, ignore this one and move on
		FloatIndex *thisIndex = [deltaArray objectAtIndex:pos];
		FloatIndex *nextIndex = [deltaArray objectAtIndex:pos+1];
		if ([thisIndex indexValue] == [nextIndex indexValue]+1 || [thisIndex indexValue] == [nextIndex indexValue]-1) {
			// ignore
		}
		else {
			// add to array
			[foundArray addObject:thisIndex];
		}
		pos++;
	}
	
	// sort found indexes by index and put into return array
	[foundArray sortUsingSelector:@selector(compareIndex:)];
	int *returnArray = malloc(sizeof(int)*phonemeCount-1);
	for (int i=0; i < foundArray.count; i++) {
		FloatIndex *thisIndex = [foundArray objectAtIndex:i];
		returnArray[i] = [thisIndex indexValue];
	}
	
	return returnArray;
}

-(void)cropAtStartPercent:(float)startPercent endPercent:(float)endPercent quantizeCount:(int)newQuantizeCount {
	// if it's the full length, do nothing
	if (startPercent == 0.0f && endPercent == 1.0f)
		return;
	
	// calculate start position and quantize count
	int startPos = round((float)quantizeCount*startPercent);
	int endPos = round((float)quantizeCount*endPercent);
	if (startPercent == 0.0f)
		endPos = startPos + newQuantizeCount;
	else
		startPos = endPos - newQuantizeCount;
	
	// copy into new buffer
	float *newFeatureBuffer = malloc(sizeof(float)*newQuantizeCount*featureCount);
	int newPos=0;
	for (int s=startPos; s < endPos; s++) {
		for (int i=0; i < featureCount; i++) {
			newFeatureBuffer[newPos*featureCount+i] = featureBuffer[s*featureCount+i];
		}
		newPos++;
	}
	
	// free old buffer and replace with new one
	free(featureBuffer);
	featureBuffer = newFeatureBuffer;
	
	// update quantize count
	quantizeCount = newQuantizeCount;
}

+(void)matchBrightnessOfBuffer:(float*)buffer1 toBuffer:(float*)buffer2 ofWidth:(int)inWidth andHeight:(int)inHeight {
    // first find brightness
    float avgBrightness1 = 0.0f;
    float avgBrightness2 = 0.0f;
    for (int i=0; i < inWidth*inHeight; i++) {
        avgBrightness1 += buffer1[i];
        avgBrightness2 += buffer2[i];
    }
    avgBrightness1 /= (float)(inWidth*inHeight);
    avgBrightness2 /= (float)(inWidth*inHeight);
    
    // change brightness
    for (int i=0; i < inWidth*inHeight; i++)
        buffer1[i] -= avgBrightness1-avgBrightness2;
}

+(void)applyContrast:(float*)inBuffer ofWidth:(int)inWidth andHeight:(int)inHeight notUsedPercentage:(float)inNotUsedPercentage {
	float currentBrightness = 0.0f;
	float currentContrast = 0.0f;
	float whiteDistribution = 0.0f;
	float blackDistribution = 0.0f;
	float minBlackPercent = 0.5f * (1.0f+inNotUsedPercentage);
	float minWhitePercent = 0.20f * (1.0f-inNotUsedPercentage);
//	float minBlackPercent = 0.25f;
//	float minWhitePercent = 0.10f;
	
    // image equalization (en.wikipedia.org/wiki/Histogram_equalization
    
    // first, convert to byte buffer
	unsigned char *charBuffer = malloc(sizeof(unsigned char)*inHeight*inWidth);
    memset(charBuffer, 0, sizeof(unsigned char)*inHeight*inWidth);
    for (int i=0; i < inHeight*inWidth; i++)
        charBuffer[i] = roundf(inBuffer[i]*255.0f);
    
    // now build histogram
    int *histogram = calloc(256, sizeof(int));
    for (int i=0; i < inHeight*inWidth; i++)
        histogram[charBuffer[i]] += 1;
    
    // convert to cummulative distribution (cdf)
    for (int i=1; i < 256; i++)
        histogram[i] = histogram[i-1] + histogram[i];
    
    // find min cdf
    unsigned char minCdf = 0;
    for (int i=0; i < 256; i++)
        if (histogram[i] > 0) {
            minCdf = histogram[i];
            break;
        }
    
    // equalize histogram
    for (int i=0; i < 256; i++)
        histogram[i] = roundf(((float)(histogram[i]-minCdf)/(float)(inWidth*inHeight))*255.0f);
    
    // update byte buffer from equalized histogram
    for (int i=0; i < inWidth*inHeight; i++)
        charBuffer[i] = histogram[charBuffer[i]];
    
    // copy new byte buffer to input image array
    for (int i=0; i < inWidth*inHeight; i++)
        inBuffer[i] = (float)charBuffer[i] / 255.0f;
    
    // release histogram and buffer
    free(histogram);
    free(charBuffer);
    
    return;
    
	// hold result
    float *tempBuffer = malloc(sizeof(float)*inHeight*inWidth);
	
    // apply contrast
    [VoiceSample changeBrightnessTo:-0.2f contrastTo:0.4f forBuffer:inBuffer toBuffer:tempBuffer ofWidth:inWidth andHeight:inHeight blackDistribution:&blackDistribution whiteDistribution:&whiteDistribution];
    
    /*
    // first get overall brightness level
    float myBrightness = 0.0f;
    for (int i=0; i < inWidth*inHeight; i++)
        myBrightness += inBuffer[i];
    myBrightness /= (float)inWidth*(float)inHeight;
    
    // now try and aim for 50% brightness across image, so add/remove difference from
    // each pixel
    float myDifference = 0.5f - myBrightness;
    for (int i=0; i < inWidth*inHeight; i++)
        tempBuffer[i] = inBuffer[i] + myDifference;
    
	// decrease brightness until you have 15-25% in the black range
    int count=0;
    int iteration=0;
	do
	{
        // increase contrast and brightness until we achieve white percent
        while (whiteDistribution < minWhitePercent) {
            currentContrast += 0.1f;
            currentBrightness += 0.1f;
            [self changeBrightnessTo:currentBrightness contrastTo:currentContrast forBuffer:inBuffer toBuffer:tempBuffer ofWidth:inWidth andHeight:inHeight blackDistribution:&blackDistribution whiteDistribution:&whiteDistribution];
            count++;
            printf("white distribution is %f\r\n", whiteDistribution);
        }
        
        // decrease brightness until we achieve black percent
        while (blackDistribution < minBlackPercent) {
            currentBrightness -= 0.1f;
            [self changeBrightnessTo:currentBrightness contrastTo:currentContrast forBuffer:inBuffer toBuffer:tempBuffer ofWidth:inWidth andHeight:inHeight blackDistribution:&blackDistribution whiteDistribution:&whiteDistribution];
            count++;
            printf("black distribution is %f\r\n", blackDistribution);
        }
        
            
        // count iterations for debugging
        iteration++;
} while (blackDistribution < minBlackPercent || whiteDistribution < minWhitePercent);
    
    printf("**contrast applied in %d iterations\r\n", count);
    
     */
    
	// copy temp buffer to real buffer
	for (int i=0; i < inHeight*inWidth; i++)
		inBuffer[i] = tempBuffer[i];
	
	// free memory
	free(tempBuffer);
}

-(void)increaseContrastOfBuffer:(float*)inBuffer andSize:(int)inSize byPercent:(float)inPercent usingCutoff:(float)inCutoff {
    for (int i=0; i < inSize; i++) {
        // get pixel as normalized value
        float shade = inBuffer[i];
        
        // increase contrast
        shade -= inCutoff;
        shade *= (1.0f + inPercent);
        shade += inCutoff;
        
        // put back in buffer
        inBuffer[i] = shade;
    }
}

+(void)changeBrightnessTo:(float)inBrightnessPercentChange contrastTo:(float)inContrastPercentChange forBuffer:(float*)inBuffer toBuffer:(float*)outBuffer ofWidth:(int)inWidth andHeight:(int)inHeight blackDistribution:(float*)blackDistribution whiteDistribution:(float*)whiteDistribution {
	// distrubution
	float blackWidth = 0.1f;
	float whiteWidth = 0.1f;
	int blackCount = 0;
	int whiteCount = 0;
	
	// loop through image
	for (int x = 0; x < inWidth; x++)
	{
		for (int y = 0; y < inHeight; y++)
		{
			// get pixel as normalized value
			float shade = inBuffer[(x*inHeight)+y];
			
			// increase contrast
			shade -= 0.5f;
			shade *= (1.0f + inContrastPercentChange);
			shade += 0.5f;
			
			// change brightness
			shade += inBrightnessPercentChange;
			
			// write pixel back
			if (shade > 1.0f)
				shade = 1.0f;
			if (shade < 0.0f)
				shade = 0.0f;
			if (shade < blackWidth)
				blackCount++;
			if (shade > 1.0f - whiteWidth)
				whiteCount++;
			outBuffer[(x*inHeight)+y] = shade;
		}
	}
	
	// calculate distribution
	*whiteDistribution = whiteCount / (float)(inWidth * inHeight);
	*blackDistribution = blackCount / (float)(inWidth * inHeight);
}

-(void)blurBuffer:(float*)inBuffer ofWidth:(int)inWidth andHeight:(int)inHeight {
	// hold result
	float *tempBuffer = malloc(sizeof(float)*inHeight*inWidth);
	memset(tempBuffer, 0, sizeof(float)*inHeight*inWidth);
    
	int radius = 1;
	float filter[] = {
		0.0f, 0.2f, 0.0f,
		0.2f, 0.2f, 0.2f,
		0.0f, 0.2f, 0.0f
	};
	
	// copy result to input buffer
	float total = 0.0f;
	for (int y=0; y < inHeight; y++) {
		for (int x=0; x < inWidth; x++) {
			total = 0.0f;
			for (int ky = 0; ky <= 0; ky++) {
//                for (int ky = -radius; ky <= radius; ky++) {
				for (int kx = -radius; kx <= radius; kx++) {
					if (x+kx >= 0 && x+kx < inWidth && y+ky >= 0 && y+ky < inHeight) {
//						float test1 = inBuffer[((x+kx)*inHeight)+y+ky];
//						float test2 = filter[((ky+radius)*3)+(kx+radius)];
						total += inBuffer[((x+kx)*inHeight)+y+ky] * filter[((ky+radius)*3)+(kx+radius)];
					}
				}
			}
			tempBuffer[(x*inHeight)+y] = total;
		}
	}
	
	// copy temp buffer to real buffer
	for (int i=0; i < inHeight*inWidth; i++)
		inBuffer[i] = tempBuffer[i];
	
	// free memory
	free(tempBuffer);
}

-(void)image:(UIImage*)image didFinishSavingWithError:(NSError*)error contextInfo:(void*)contextInfo {
	if (error != nil) {
		assert("test");
	}
}

CGContextRef MyCreateBitmapContext (int pixelsWide,
									int pixelsHigh)
{
    CGContextRef    context = NULL;
    CGColorSpaceRef colorSpace;
    void *          bitmapData;
    int             bitmapByteCount;
    int             bitmapBytesPerRow;
	
    bitmapBytesPerRow   = (pixelsWide * 4);// 1
    bitmapByteCount     = (bitmapBytesPerRow * pixelsHigh);
	
//    colorSpace = CGColorSpaceCreateWithName(kCGColorSpaceGenericRGB);// 2
	colorSpace = CGColorSpaceCreateDeviceRGB();
    bitmapData = malloc( bitmapByteCount );// 3
    if (bitmapData == NULL)
    {
        CGColorSpaceRelease(colorSpace);
        fprintf (stderr, "Memory not allocated!");
        return NULL;
    }
    context = CGBitmapContextCreate (bitmapData,// 4
									 pixelsWide,
									 pixelsHigh,
									 8,      // bits per component
									 bitmapBytesPerRow,
									 colorSpace,
									 kCGImageAlphaPremultipliedLast);
    if (context== NULL)
    {
        CGColorSpaceRelease( colorSpace );// 6
        CGContextRelease(context);
        free (bitmapData);// 5
        fprintf (stderr, "Context not created!");
        return NULL;
    }
    CGColorSpaceRelease( colorSpace );// 6
    free(bitmapData);
	
    return context;// 7
}

-(void)normalizeBufferAtZero:(float*)inBuffer ofSize:(int)inSize {
	// normalize buffer between -1.0 and 1.0, but keep it centered around 0
	float max = 0;
	for (int i=0; i < inSize; i++) {
		if (fabs(inBuffer[i]) > max)
			max = fabs(inBuffer[i]);
	}
	for (int i=0; i < inSize; i++) {
		if (inBuffer[i] > 0)
			inBuffer[i] = fabs(inBuffer[i]) / max;
		else 
			inBuffer[i] = 0 - (fabs(inBuffer[i]) / max);
	}
}

-(void)normalizeBufferAtZero:(float*)inBuffer ofSize:(int)inSize usingMax:(float)inMax {
	// normalize buffer between -1.0 and 1.0, but keep it centered around 0
	float max = inMax;
	for (int i=0; i < inSize; i++) {
		if (fabs(inBuffer[i]) > max)
			max = fabs(inBuffer[i]);
	}
	for (int i=0; i < inSize; i++) {
		if (inBuffer[i] > 0)
			inBuffer[i] = fabs(inBuffer[i]) / max;
		else 
			inBuffer[i] = 0 - (fabs(inBuffer[i]) / max);
	}
}

+(void)normalizeBufferAroundZeroToPoint5:(float*)inBuffer ofSize:(int)inSize usingMax:(float)inMax {
	// normalize buffer between -1.0 and 1.0, but keep it centered around 0
	// then convert to between 0.0 and 1.0
	for (int i=0; i < inSize; i++) {
		if (inBuffer[i] > 0)
			inBuffer[i] = fabs(inBuffer[i]) / inMax;
		else 
			inBuffer[i] = 0 - (fabs(inBuffer[i]) / inMax);
		inBuffer[i] = MIN(1,MAX(0,(inBuffer[i]+1.0f)/2.0f));
	}
}

-(void)normalizeBufferAroundZeroToPoint5:(float*)inBuffer ofSize:(int)inSize {
	// normalize buffer between -1.0 and 1.0, but keep it centered around 0
	// then convert to between 0.0 and 1.0
	float max = 0.0f;
	for (int i=0; i < inSize; i++) {
		if (fabs(inBuffer[i]) > max)
			max = fabs(inBuffer[i]);
	}
	for (int i=0; i < inSize; i++) {
		if (inBuffer[i] > 0)
			inBuffer[i] = fabs(inBuffer[i]) / max;
		else 
			inBuffer[i] = 0 - (fabs(inBuffer[i]) / max);
		inBuffer[i] = MIN(1,MAX(0,(inBuffer[i]+1.0f)/2.0f));
	}
}

-(void)normalizePositiveBuffer:(float*)inBuffer ofSize:(int)inSize usingMax:(float)max {
	// normalize buffer between -1.0 and 1.0, but keep it centered around 0
	// then convert to between 0.0 and 1.0
	for (int i=0; i < inSize; i++) {
		if (inBuffer[i] >= 0.0f)
			inBuffer[i] = fabs(inBuffer[i]) / max;
        else
            assert("oh no!");
        //		else 
        //			inBuffer[i] = 0 - (fabs(inBuffer[i]) / max);
        //		inBuffer[i] = (inBuffer[i]+1.0f)/2.0f;
	}
}

+(void)normalizePositiveBuffer:(float*)inBuffer ofSize:(int)inSize {
	// normalize buffer between -1.0 and 1.0, but keep it centered around 0
	// then convert to between 0.0 and 1.0
    float max = 0.0f;
	for (int i=0; i < inSize; i++)
        if (inBuffer[i] > max)
            max = inBuffer[i];
	for (int i=0; i < inSize; i++) {
		if (inBuffer[i] >= 0.0f)
			inBuffer[i] = fabs(inBuffer[i]) / max;
        else
            assert("oh no!");
        //		else 
        //			inBuffer[i] = 0 - (fabs(inBuffer[i]) / max);
        //		inBuffer[i] = (inBuffer[i]+1.0f)/2.0f;
	}
}



-(void)normalizeBuffer:(float*)inBuffer ofSize:(int)inSize usingMax:(float)max {
	for (int i=0; i < inSize; i++) {
		if (inBuffer[i] > 0)
			inBuffer[i] = fabs(inBuffer[i]) / max;
		else 
			inBuffer[i] = 0 - (fabs(inBuffer[i]) / max);
		inBuffer[i] = (inBuffer[i]+1.0f)/2.0f;
		if (inBuffer[i] > 1.0f)
			inBuffer[i] = 1.0f;
		if (inBuffer[i] < 0.0f)
			inBuffer[i] = 0.0f;
	}
}

-(void)normalizeAndScaleBuffer:(float*)inBuffer ofSize:(int)inSize {
	// normalize buffer between 0.0 and 1.0
	float max = 0;
	float min = INT_MAX;
	for (int i=0; i < inSize; i++) {
		if (inBuffer[i] > max)
			max = inBuffer[i];
		if (inBuffer[i] < min)
			min = inBuffer[i];
	}
	for (int i=0; i < inSize; i++) {
		inBuffer[i] = (inBuffer[i]-min) / (max-min);
	}
	return;
/*	
	float max = 0;
	float min = INT_MAX;
	for (int i=0; i < inSize; i++) {
		if (fabs(inBuffer[i]) > max)
			max = fabs(inBuffer[i]);
		if (fabs(inBuffer[i]) < min)
			min = fabs(inBuffer[i]);
	}
	for (int i=0; i < inSize; i++) {
		if (inBuffer[i] > 0)
			inBuffer[i] = (fabs(inBuffer[i])-min) / (max-min);
		else 
			inBuffer[i] = 0 - ((fabs(inBuffer[i])-min) / (max-min));
	}*/
}

-(void)createSsmaWithBuffer:(float*)inBuffer ofSize:(int)inSize usingPeriod:(int)inPeriod outputTo:(float*)outBuffer {
	// create simple smoothed moving average
	float total = 0.0f;
	for (int i=0; i < inSize; i++) {
		float dataPoint = inBuffer[i];
		
		// calculate average
		if (i < inPeriod) {
			// sma
			outBuffer[i] = total / (i+1);
			total += dataPoint;
		}
		else {
			//ssma
			total = (total - outBuffer[i-1] + dataPoint);
			outBuffer[i] = total / (float)inPeriod;
		}
	}
}

-(float)compareBuffer:(float*)inBuffer ofSize:(int)inSize withBuffer:(float*)inBuffer2 {
	// find difference as a percentage
	float totDifference = 0;
	for (int i=0; i < inSize; i++) {
		float difference = 0;
		if (inBuffer[i] > inBuffer2[i])
			difference = inBuffer[i]-inBuffer2[i];
		else
			difference = inBuffer2[i]-inBuffer[i];
		
		// make larger difference more exagerated (exponential)
		difference = powf(difference, 4) * 30.0f;
		
		// add to total
		totDifference += difference;
	}
	
	// return average
	float avgDifference = 1.0f - (totDifference / (float)inSize);
	return avgDifference;
}

-(void)writeToCsvFileWithFilename:(NSString*)inFilename {
	// get output filenames
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *filename1 = [documentsDirectory stringByAppendingFormat:@"/%@", inFilename];
	NSString *oldFilename1 = [documentsDirectory stringByAppendingFormat:@"/old_%@", inFilename];
	
	// if file exists, save old copy
	if ([[NSFileManager defaultManager] fileExistsAtPath:filename1]) {
		NSError *myError = [NSError alloc];
		[[NSFileManager defaultManager] removeItemAtPath:oldFilename1 error:&myError];
		[[NSFileManager defaultManager] copyItemAtPath:filename1 toPath:oldFilename1 error:&myError];
	}
	
	// write out new file
	[[NSFileManager defaultManager] createFileAtPath:filename1 contents:nil attributes:nil];
	
	// open file
	NSFileHandle *pfile = [NSFileHandle fileHandleForWritingAtPath:filename1];
	
	// loop through data and write in csv format
	for (int i=0; i < featureCount; i++) {
		for (int s=0; s < quantizeCount; s++) {
			[pfile writeData:[[NSString stringWithFormat:@"%f,", featureBuffer[s*featureCount+i]] dataUsingEncoding:NSUTF8StringEncoding]];
		}
		[pfile writeData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
	}
	
	// close file
	[pfile closeFile];
}

-(void)writeToWavFile:(NSString *)inFilename {
    return;
	// get output filenames
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *filename1 = [documentsDirectory stringByAppendingFormat:@"/%@", inFilename];

	// set up format
	AudioStreamBasicDescription dataFormat;
	dataFormat.mSampleRate = 16000;
	dataFormat.mFormatID = kAudioFormatLinearPCM;
	dataFormat.mFramesPerPacket = 1;
	dataFormat.mChannelsPerFrame = 1;
	dataFormat.mBytesPerFrame = 4;
	dataFormat.mBytesPerPacket = 4;
	dataFormat.mBitsPerChannel = 32;
	dataFormat.mReserved = 0;
	dataFormat.mFormatFlags = kLinearPCMFormatFlagIsFloat|kLinearPCMFormatFlagIsPacked;
	
	// get c path names
	char *path1 = malloc(1024);
	[filename1 getCString:path1 maxLength:1024 encoding:NSUTF8StringEncoding];
	
	// write first audio file
	AudioFileID fileId1;
	CFURLRef fileURL1 = CFURLCreateFromFileSystemRepresentation(NULL, (UInt8*)path1, strlen(path1), false);
	UInt32 sizeWritten = sampleBufferSize;
	AudioFileCreateWithURL(fileURL1, kAudioFileWAVEType, &dataFormat, kAudioFileFlags_EraseFile, &fileId1);
	AudioFileWriteBytes(fileId1, false, 0, &sizeWritten, sampleBuffer);
	AudioFileClose(fileId1);
	
	// free buffers
	free(path1);
}

+(void)writeToImageWithFilename:(NSString*)inFilename alternateBuffer:(float*)inAltBuffer alternateFeatureCount:(int)inAltFeatureCount alternateQuantizeCount:(int)inAltQuantizeCount {
    // buffers
//    float *myFeatureBuffer = featureBuffer;
//    int myFeatureCount = featureCount;
//    int myQuantizeCount = quantizeCount;
//    if (inAltBuffer != nil) {
        float *myFeatureBuffer = inAltBuffer;
        int myFeatureCount = inAltFeatureCount;
        int myQuantizeCount = inAltQuantizeCount;
//    }
    
	// create bitmap
	CGContextRef myContext = MyCreateBitmapContext(myQuantizeCount, myFeatureCount);
	
	for (int x=0; x < myQuantizeCount; x++) {
		for (int y=0; y < myFeatureCount; y++) {
			float shade = myFeatureBuffer[(x*myFeatureCount)+y];
			if (inAltBuffer != nil)
				shade = inAltBuffer[(x*myFeatureCount)+y];
			CGContextSetRGBFillColor(myContext, shade, shade, shade, 1.0f);
			CGContextFillRect(myContext, CGRectMake(x, myFeatureCount-1-y, 1, 1));
		}
	}
	
	// get filename
	CGImageRef myCGImage = CGBitmapContextCreateImage(myContext);
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *imagePath = [documentsDirectory stringByAppendingFormat:@"/%@", inFilename];
	UIImage *myImage = [UIImage imageWithCGImage:myCGImage];
	
	// write file
	[UIImagePNGRepresentation(myImage) writeToFile:imagePath atomically:YES];
	void *data = CGBitmapContextGetData(myContext);
	free(data);
	CGImageRelease(myCGImage);
	CGContextRelease(myContext);
}

-(int)getSyllableCount {
	// loop through sample
	int syllableCount = 0;
	int pos = 0;
	while (pos < quantizeCount) {
		int startPos = pos;
		if ([self getNextSyllableEndPositionFromPosition:&pos]) {
			printf("*** found syllable from position %d to position %d\r\n", startPos, pos);
		}
//		char type = [self getNextContigousNoiseTypeFromPosition:&pos];
	}
	
	return syllableCount;
}

-(bool)getNextSyllableEndPositionFromPosition:(int*)endPos {
	// parameters
	int minSyllableLength = 5;
	
	// check first type
	int startPos = *endPos;
	int pos = *endPos;
	char type = [self getNextContigousNoiseTypeFromPosition:&pos];
	if (type == 'n') {
		*endPos = pos;
		return false;
	}
	else {
		if (type == 'c') {
			// check next type
			type = [self getNextContigousNoiseTypeFromPosition:&pos];
			if (type == 'n') {
				*endPos = pos;
				return false;
			}
			else {
				// must be vowel
				*endPos = pos;
				if (pos-startPos > minSyllableLength)
					return true;
				else
					return false;
			}
		}
		else {
			// must be vowel
			int previousPos = pos;
			type = [self getNextContigousNoiseTypeFromPosition:&pos];
			if (type == 'n') {
				*endPos = previousPos;
				if (previousPos-startPos > minSyllableLength)
					return true;
				else
					return false;
			}
			else {
				// must be consonant
				// japanese words don't end in consonants, so return true, but return previous position
				*endPos = previousPos;
				if (previousPos-startPos > minSyllableLength)
					return true;
				else
					return false;
			}

		}
	}
	return false;
}

-(char)getNextContigousNoiseTypeFromPosition:(int*)inPos {
	// parameters
	int pos = *inPos;
	char thisType = [self getNoiseTypeForPosition:pos++];
	while (pos < quantizeCount && ([self getNoiseTypeForPosition:pos] == thisType || (pos+1 < quantizeCount && [self getNoiseTypeForPosition:pos+1] == thisType))) {
		pos++;
	}
	printf("found sound %c from position %d to position %d\r\n", thisType, *inPos, pos);
	*inPos = pos;
	return thisType;
}

-(char)getNoiseTypeForPosition:(int)inPos {
	// paramsx`
	float vowelPeriodicityThreshold = 0.75f;
	float vowelEnergyThreshold = 0.75f;
	float noiseEnergyThreshold = 0.25f;
	
	// get values
	float energyValue = featureBuffer[inPos*featureCount+featureCount-3];
	float periodicity = featureBuffer[inPos*featureCount+featureCount-1];
	
	// calculate type, v=vowel, n=noise, c=consonant
	if (energyValue > vowelEnergyThreshold && periodicity > vowelPeriodicityThreshold)
		return 'v';
	else {
		if (energyValue < noiseEnergyThreshold)
			return 'n';
		else
			return 'c';
	}
}

-(float)compareWithVoiceSample:(VoiceSample*)inSample filenameSuffix:(NSString*)filenameSuffix forText:(NSString*)inText calibrationMode:(BOOL)inCalibrationMode writeDebug:(BOOL)inWriteDebug {
    
    /*
    // just compare length
    if ((float)sampleBufferSize < (float)[inSample getSampleBufferSize]*0.8f-16000.0f ||
        (float)sampleBufferSize > (float)[inSample getSampleBufferSize]*1.2f+16000.0f)
        return 0.0f;
    else
        return 1.0f;
    */
    
	// write sound samples out to documents directory for debugging
	[self writeToWavFile:[NSString stringWithFormat:@"compare1%@.wav", filenameSuffix]];
	[inSample writeToWavFile:[NSString stringWithFormat:@"compare2%@.wav", filenameSuffix]];
    
    // write out mfcc buffer
    /*
	[VoiceSample writeToImageWithFilename:[NSString stringWithFormat:@"compare1_nocms_mfcc.png", filenameSuffix] alternateBuffer:mfccBuffer alternateFeatureCount:melCount alternateQuantizeCount:quantizeCount];
	[VoiceSample writeToImageWithFilename:[NSString stringWithFormat:@"compare2_nocms_mfcc.png", filenameSuffix] alternateBuffer:[inSample getMfccBuffer] alternateFeatureCount:[VoiceSample getN]/2 alternateQuantizeCount:quantizeCount];
    */
    
    // don't do grading until we've perfected it
//    return 1.0f;
	
    // compare mel buffer straight
//	float returnScore = [VoiceSample compareImage3:melBuffer ofWidth:quantizeCount andHeight:melCount withImage:[inSample getMelBuffer] forText:inText dtwImage:melBuffer withDtwImage:[inSample getMelBuffer] withHeight:melCount calibrationMode:inCalibrationMode writeDebug:inWriteDebug compareWithCheckImage:NO];
    
    // compare feature buffer straight
//	float returnScore = [VoiceSample compareImage3:featureBuffer ofWidth:quantizeCount andHeight:featureCount withImage:[inSample getFeatureBuffer] forText:inText dtwImage:featureBuffer withDtwImage:[inSample getFeatureBuffer] withHeight:featureCount calibrationMode:inCalibrationMode writeDebug:inWriteDebug compareWithCheckImage:NO];
    // compare feature buffer straight
	float returnScore = [VoiceSample compareImage3:featureBuffer ofWidth:quantizeCount andHeight:featureCount withImage:[inSample getFeatureBuffer] forText:inText dtwImage:melBuffer withDtwImage:[inSample getMelBuffer] withHeight:melCount calibrationMode:inCalibrationMode writeDebug:inWriteDebug compareWithCheckImage:NO phonemeLengthArray:phonemeLengthArray];
    printf("score is %f\r\n", returnScore);
    
    
	// do comparison - use mel buffer for dtw, because it's generally closer
//	float returnScore = [VoiceSample compareImage3:featureBuffer ofWidth:quantizeCount andHeight:featureCount withImage:[inSample getFeatureBuffer] forText:inText dtwImage:melBuffer withDtwImage:[inSample getMelBuffer] withHeight:melCount calibrationMode:inCalibrationMode writeDebug:inWriteDebug compareWithCheckImage:NO];
//    printf("score is %f\r\n", returnScore);
	
	// translate good and bad range to score between 0.0f and 1.0f
    float score = [VoiceSample convertScoreToPercent:returnScore];
    
	return score;
}

-(int)getSampleBufferSize {
	return sampleBufferSize;
}

-(float*)getSampleBuffer {
	return sampleBuffer;
}

-(float*)getSpectrumBuffer {
	return spectrumBuffer;
}

-(float)compareImage2:(float*)inImage1 ofWidth:(int)inWidth andHeight:(int)inHeight withImage:(float*)inImage2 {
	// create weights for mfcc
	float mfccWeights[12];
	mfccWeights[0] = 1.0f;
	mfccWeights[1] = 1.0f;
	mfccWeights[2] = 1.0f;
	mfccWeights[3] = 1.0f;
	mfccWeights[4] = 1.0f;
	mfccWeights[5] = 1.0f;
	mfccWeights[6] = 1.0f;
	mfccWeights[7] = 1.0f;
	mfccWeights[8] = 1.0f;
	mfccWeights[9] = 1.0f;
	mfccWeights[10] = 1.0f;
	mfccWeights[11] = 1.0f;
	
	// do DTW for each mfcc sequence
	float totalScore = 0.0f;
	float totalCount = 0.0f;
	for (int m=0; m < featureCount; m++) {
		float *DTW = malloc(sizeof(float)*(inWidth+1)*(inWidth+1));
		float cost = 0.0f;
        
        // zero memory
        memset(DTW, 0, sizeof(float)*(inWidth+1)*(inWidth+1));
		
		// init
		for (int i=0; i < ((inWidth+1)*(inWidth+1)); i++)
			DTW[i] = 99999.0f;
		for (int i=1; i < inWidth+1; i++)
			DTW[0*(inWidth+1)+i] = 99999.0f;
		for (int i=1; i < inWidth+1; i++)
			DTW[i*(inWidth+1)+0] = 99999.0f;
		DTW[0] = 0;
		
		// use dtw algorithm
		int w=10;
		for (int i=1; i < inWidth+1; i++) {
			for (int j=fmax(1, i-w); j < fmin(inWidth, i+w)+1; j++) {
//			for (int j=1; j < inWidth+1; j++) {
				// test... if the energy in the source and destination frames are both below 0.01f, no cost!
				if (inImage1[(i-1)*inHeight+0] < 0.01f && inImage2[(j-1)*inHeight+0] < 0.01f)
					cost += 0.0f;
				else {
					if (inImage1[(i-1)*inHeight+m] > inImage2[(j-1)*inHeight+m]) {
						cost = inImage1[(i-1)*inHeight+m] - inImage2[(j-1)*inHeight+m];
					}
					else {
						cost = inImage2[(j-1)*inHeight+m] - inImage1[(i-1)*inHeight+m];
					}
				}
				DTW[i*(inWidth+1)+j] = cost + fmin(fmin(DTW[(i-1)*(inWidth+1)+j], DTW[i*(inWidth+1)+j-1]), DTW[(i-1)*(inWidth+1)+j-1]);
			}
		}
		float result = DTW[(inWidth)*(inWidth+1)+inWidth];
        free(DTW);
		printf("dtw result for mfc %d is %f\r\n", m, result);
		totalScore += (result * mfccWeights[m]);
		totalCount += mfccWeights[m];
	}
	float newScore= totalScore / totalCount;
	newScore = newScore / (float)inWidth;
	printf("newScore=%f (divided by %d)\r\n",newScore, inWidth);
	return newScore;
	// answer is coming in here between 5 and 10 (right and wrong)
	newScore -= .10f;
	newScore *= (1.0f/0.05f);
	newScore = 1.0f - newScore;
	return newScore;
}

+(float)compareImage3_1:(float*)inImage1 ofWidth:(int)inWidth withImage:(float*)inImage2 withLeeway:(int)inLeeway stretchImage2:(BOOL)inStretch {	
	// do DTW for each mfcc sequence
	float *DTW = malloc(sizeof(float)*(inWidth+1)*(inWidth+1));
	memset(DTW, 0, sizeof(float)*(inWidth+1)*(inWidth+1));
    
	// init
	for (int i=0; i < ((inWidth+1)*(inWidth+1)); i++)
		DTW[i] = 99999.0f;
	DTW[0] = 0;
	
	// use dtw algorithm
	int w=inLeeway;
	for (int i=1; i < inWidth+1; i++) {
		for (int j=fmax(1, i-w); j < fmin(inWidth, i+w)+1; j++) {
			// calculate cost as euclidean distance
			float cost;
			float value1 = inImage1[(i-1)];
			float value2 = inImage2[(j-1)];
			if (value1 > value2)
				cost = value1 - value2;
			else
				cost = value2 - value1;
			
			DTW[i*(inWidth+1)+j] = cost + fmin(fmin(DTW[(i-1)*(inWidth+1)+j], DTW[i*(inWidth+1)+j-1]), DTW[(i-1)*(inWidth+1)+j-1]);
			
		}
		
	}
    
    
    // return stretched image?
    if (inStretch) {
        int inHeight = 1;
        float *newImage = malloc(sizeof(float)*inWidth*inHeight);
        memset(newImage, 0, sizeof(float)*inWidth*inHeight);
        int currentX = inWidth;
        int currentY = inWidth;
        while (currentX > 0 && currentY > 0) {
            // copy data
            if (currentX-1 < inWidth && currentY-1 < inWidth) {
                for (int m=0; m < inHeight; m++) {
                    newImage[(currentY-1)*inHeight+m] = inImage2[(currentX-1)*inHeight+m];
                }
            }
            else
                assert("eh!?");
            
            // see where to move next
            float value1 = DTW[currentY*(inWidth+1)+currentX-1]; // left
            float value2 = DTW[(currentY-1)*(inWidth+1)+currentX-1]; // diag
            float value3 = DTW[(currentY-1)*(inWidth+1)+currentX]; // above
            
            // left
            if (value1 < value2 && value1 < value3) {
                currentX--;
            }
            else {
                // diag
                if (value2 < value1 && value2 < value3) {
                    currentX--;
                    currentY--;
                }
                else {
                    // above
                    currentY--;
                }
            }
        }
        
        // copy and free
        for (int i=0; i < inWidth; i++)
            inImage2[i] = newImage[i];
        free(newImage);
    }

    float retVal = DTW[(inWidth)*(inWidth+1)+inWidth];
    free(DTW);
	return retVal;
}

-(double)sigmoidWithInput:(double)inInput response:(double)inResponse {
    return 1.0f / (1 + pow(2.7183, -inInput/inResponse));
}

+(float)convertScoreToPercent:(float)inScore {
    float good = 0.08f;
    float bad = 0.15f;
    if (inScore < good)
        inScore = 1.0f;
    else {
        if (inScore > bad)
            inScore = 0.0f;
        else {
            inScore -= good;
            inScore *= (1.0f/(bad-good));
            inScore = 1.0f - inScore;
        }
    }
    return inScore;
}

+(float)TukeyFuncWithWidth:(int)inWidth andPosition:(int)i {
    float a = 0.5f;
    float tukeyFunc;
    if ((float)i < a*((float)inWidth-1.0f)/2.0f)
        tukeyFunc = 0.5f*(1.0f+cosf(M_PI*((2.0f*(float)i)/(a*((float)inWidth-1.0f))-1)));
    else {
        if ((float)i < ((float)inWidth-1.0f)*(1.0f-a/2.0f))
            tukeyFunc = 1.0f;
        else
            tukeyFunc = 0.5f*(1.0f+cosf(M_PI*((2.0f*(float)i)/(a*((float)inWidth-(2.0f/a)+1.0f))-1)));
    }
    return tukeyFunc;
}

+(float)compareImage3_horizontal:(float*)inImage1 ofWidth:(int)inWidth andHeight:(int)inHeight withImage:(float*)inImage2 withLeeway:(int)inLeeway stretchImage2:(BOOL)inStretch {	
	// initialize dtw array
	float *DTW = malloc(sizeof(float)*(inHeight+1)*(inHeight+1));
    memset(DTW, 0, sizeof(float)*(inHeight+1)*(inHeight+1));
	for (int i=0; i < ((inHeight+1)*(inHeight+1)); i++)
		DTW[i] = 99999.0f;
	DTW[0] = 0;

    // do compare
    int w = inLeeway;
	for (int i=1; i < inHeight+1; i++) {
		for (int j=fmax(1, i-w); j < fmin(inHeight, i+w)+1; j++) {
			// find cost horizontally with no leeway
            float cost = 0.0f;
            for (int c=0; c < inWidth; c++) {
                float value1 = inImage1[c*inHeight+(i-1)];
                float value2 = inImage2[c*inHeight+(j-1)];
                if (value1 > value2)
                    cost += value1 - value2;
                else
                    cost += value2 - value1;
            }
			cost = cost / inWidth;
            
			DTW[i*(inHeight+1)+j] = cost + fmin(fmin(DTW[(i-1)*(inHeight+1)+j], DTW[i*(inHeight+1)+j-1]), DTW[(i-1)*(inHeight+1)+j-1]);
		}
	}
    
    // stretch if requested
    if (inStretch) {
        float *newImage = malloc(sizeof(float)*inWidth*inHeight);
        memset(newImage, 0, sizeof(float)*inWidth*inHeight);
        int currentX = inHeight;
        int currentY = inHeight;
        while (currentX > 0 && currentY > 0) {
            // copy data
            if (currentX-1 < inHeight && currentY-1 < inHeight) {
                for (int m=0; m < inWidth; m++) {
                    newImage[m*inHeight+(currentY-1)] = inImage2[m*inHeight+(currentX-1)];
                }
            }
            else
                assert("eh!?");
            
            // see where to move next
            float value1 = DTW[currentY*(inHeight+1)+currentX-1]; // left
            float value2 = DTW[(currentY-1)*(inHeight+1)+currentX-1]; // diag
            float value3 = DTW[(currentY-1)*(inHeight+1)+currentX]; // above
            
            // left
            if (value1 < value2 && value1 < value3) {
                currentX--;
            }
            else {
                // diag
                if (value2 < value1 && value2 < value3) {
                    currentX--;
                    currentY--;
                }
                else {
                    // above
                    currentY--;
                }
            }
        }
        
        // copy and free
        for (int i=0; i < inWidth*inHeight; i++)
            inImage2[i] = newImage[i];
        free(newImage);
        
    }
    
	float result = DTW[(inHeight)*(inHeight+1)+inHeight];
    free(DTW);
    return result;
}

+(float)compareImage3:(float*)inImage1 ofWidth:(int)inWidth andHeight:(int)inHeight withImage:(float*)inImage2 forText:(NSString*)inText dtwImage:(float*)inDtwImage1 withDtwImage:(float*)inDtwImage2 withHeight:(int)inDtwHeight calibrationMode:(BOOL)inCalibrationMode writeDebug:(BOOL)inWriteDebug compareWithCheckImage:(BOOL)inCompareWithCheck phonemeLengthArray:(NSArray *)inPhonemeLengthArray {
    
    // change brightness of second image to match first
    [VoiceSample matchBrightnessOfBuffer:inImage2 toBuffer:inImage1 ofWidth:inWidth andHeight:inHeight];
    
    // apply auto brightness/contrast to each column
    int startPos = 0;
    /*
    for (int i=0; i < inWidth; i++) {
        int thisLength = 1;
        [VoiceSample applyContrast:&inImage1[startPos*inHeight] ofWidth:thisLength andHeight:inHeight notUsedPercentage:0.0f];
        [VoiceSample applyContrast:&inImage2[startPos*inHeight] ofWidth:thisLength andHeight:inHeight notUsedPercentage:0.0f];
        startPos++;
    }
    */
    
    /*
    // try apply constrast over all
    [VoiceSample applyContrast:inImage1 ofWidth:inWidth andHeight:inHeight notUsedPercentage:0.0f];
    [VoiceSample applyContrast:inImage2 ofWidth:inWidth andHeight:inHeight notUsedPercentage:0.0f];
    */
    
    /*
    // apply severe contrast to get main features
    float blackDistribution = 0.0f;
    float whiteDistribution = 0.0f;
    [VoiceSample changeBrightnessTo:-0.2f contrastTo:0.6f forBuffer:inImage1 toBuffer:inImage1 ofWidth:inWidth andHeight:inHeight blackDistribution:&blackDistribution whiteDistribution:&whiteDistribution];
    [VoiceSample changeBrightnessTo:-0.2f contrastTo:0.6f forBuffer:inImage2 toBuffer:inImage2 ofWidth:inWidth andHeight:inHeight blackDistribution:&blackDistribution whiteDistribution:&whiteDistribution];
    */
    
	// write image out as bitmap
    if (inWriteDebug) {
        [VoiceSample writeToImageWithFilename:@"compare1_nocms_prestretch.png" alternateBuffer:inImage1 alternateFeatureCount:inHeight alternateQuantizeCount:inWidth];
        [VoiceSample writeToImageWithFilename:@"compare2_nocms_prestretch.png" alternateBuffer:inImage2 alternateFeatureCount:inHeight alternateQuantizeCount:inWidth];
    }
    
    
    // get compare feature count
//    int compareFeatureCount = [self getCompareFeatureCount];
    int compareFeatureCount = [self getMfccCount];
    compareFeatureCount = inHeight;
    int outputFeatureCount = [self getOutputFeatureCount];

	// do DTW for each dtw sequence
	float *DTW = malloc(sizeof(float)*(inWidth+1)*(inWidth+1));
	for (int i=0; i < ((inWidth+1)*(inWidth+1)); i++)
		DTW[i] = 99999.0f;
	DTW[0] = 0;
    
    // write out dtw buffers
    if (inWriteDebug) {
        [self writeToImageWithFilename:@"compare1_nocms_dtw.png" alternateBuffer:inDtwImage1 alternateFeatureCount:inDtwHeight alternateQuantizeCount:inWidth];
        [self writeToImageWithFilename:@"compare2_nocms_dtw.png" alternateBuffer:inDtwImage2 alternateFeatureCount:inDtwHeight alternateQuantizeCount:inWidth];
    }
    
    /*
    // boost frequencies my maximum of 30% to try and match source signal
    for (int h=0; h < inHeight; h++) {
        float total1 = 0.0f;
        float total2 = 0.0f;
        for (int i=0; i < inWidth; i++) {
            total1 += inImage1[i*inHeight+h];
            total2 += inImage2[i*inHeight+h];
        }
        float diff = total1/total2;
        
        if (diff > 1.5f)
            diff = 1.5f;
        else
            if (diff < 0.5f)
                diff = 0.5f;
        
        // reduce/enhance image2
        for (int i=0; i < inWidth; i++) {
            inImage2[i*inHeight+h] *= diff;
        }
    }
    */
    
    // don't do DTW here, it doesn't make sense since it doesn't use the CPU sample
    // do neural net tranform first, THEN DTW
    // SCRUB THAT... WE NEED TO DO IT HERE BECAUSE OF TRAINING DATA
    
    // bad match for stockholm ha, try mfcc
	// use dtw algorithm, match on mel spectrum buffer, not mfcc
//    float TWOPI = 2.0f * M_PI;
	int maxW = MAX(5,(int)roundf(inWidth*0.2f)); // stay closer to home for smaller samples
	for (int i=1; i < inWidth+1; i++) {
        // try a test, adjust w for position in sample based on curve so we can't
        // just chop off the end
        float tukeyFunc = [self TukeyFuncWithWidth:inWidth andPosition:i];
        
//        float hamFunc = ((1.0f-0.46f) - 0.46f*cos(TWOPI*i/((float)inWidth-1.0f)));
//        int w = MAX(5,(int)((float)maxW*hamFunc));
        int w=tukeyFunc*(float)maxW+3;
        w=maxW;
        // add an extra five because the beginning and end truncation isn't perfect
//        w+=5;
//        printf("at position %d, w is %d\r\n", i, w);
		for (int j=fmax(1, i-w); j < fmin(inWidth, i+w)+1; j++) {
            
            /*
            // match brightness to calculate cost
            float *test = malloc(sizeof(float)*inHeight);

            // first get overall brightness level
            float image1Brightness = 0.0f;
            float image2Brightness = 0.0f;
            for (int w=0; w < inHeight; w++) {
                image1Brightness += inImage1[(i-1)*inHeight+w];
                image2Brightness += inImage2[(j-1)*inHeight+w];
            }
            image1Brightness /= (float)inHeight;
            image2Brightness /= (float)inHeight;
            float myDifference = image1Brightness/image2Brightness;
            
            // limit to 30%
            if (myDifference > 1.3f)
                myDifference = 1.3f;
            else {
                if (myDifference < 0.7f)
                    myDifference = 0.7f;
            }
            
            // now try and increase brightness
            for (int w=0; w < inHeight; w++)
            test[w] = inImage2[(j-1)*inHeight+w] * myDifference;
            
            // first get overall brightness level
            float newBrightness = 0.0f;
            for (int w=0; w < inHeight; w++)
                newBrightness += test[w];
            newBrightness /= (float)inHeight;
            */
            
			// use another dtw to find cost in frequency axis
            // stretch image1 instead
			float cost = [VoiceSample compareImage3_1:&inImage1[(i-1)*inHeight] ofWidth:compareFeatureCount withImage:&inImage2[(j-1)*inHeight] withLeeway:0 stretchImage2:NO];
            
//			float cost = [VoiceSample compareImage3_1:&inImage2[(i-1)*inHeight] ofWidth:compareFeatureCount withImage:&inImage1[(j-1)*inHeight] withLeeway:2];
			cost = cost / inDtwHeight;
            
			DTW[i*(inWidth+1)+j] = cost + fmin(fmin(DTW[(i-1)*(inWidth+1)+j], DTW[i*(inWidth+1)+j-1]), DTW[(i-1)*(inWidth+1)+j-1]);
            
//            free(test);
		}
	}
	float result = DTW[(inWidth)*(inWidth+1)+inWidth];
//	float newScore = result / (float)inWidth;
    printf("DTW result is %f, maxw is %d\r\n", result, maxW);
    
    // find path through the time warp and create a buffer of the stretched sample image
    // for debugging
    float *newImage = malloc(sizeof(float)*inWidth*inHeight);
    int currentX = inWidth;
    int currentY = inWidth;
    int xCount = 0;
    int yCount = 0;
    while (currentX > 0 && currentY > 0) {
        // copy data
        if (currentX-1 < inWidth && currentY-1 < inWidth) {
            
            /*
            // copy, but adjust brightness to same as image1
            // first get overall brightness level
            float image1Brightness = 0.0f;
            float image2Brightness = 0.0f;
            for (int i=0; i < inHeight; i++) {
                image1Brightness += inImage1[(currentY-1)*inHeight+i];
                image2Brightness += inImage2[(currentX-1)*inHeight+i];
            }
            image1Brightness /= (float)inHeight;
            image2Brightness /= (float)inHeight;
            float myDifference = image1Brightness/image2Brightness;
            
            // limit to 30%
            if (myDifference > 1.3f)
                myDifference = 1.3f;
            else {
                if (myDifference < 0.7f)
                    myDifference = 0.7f;
            }
             */
            
            for (int m=0; m < inHeight; m++) {
  
                // straight copy
                // stretchimage1 instead
                newImage[(currentY-1)*inHeight+m] = inImage2[(currentX-1)*inHeight+m];
//                newImage[(currentY-1)*inHeight+m] = inImage1[(currentX-1)*inHeight+m];
                
                /*
                // change contrast
                float newValue = inImage2[(currentX-1)*inHeight+m];
                newValue *= myDifference;
                newImage[(currentY-1)*inHeight+m] = newValue;
                 */
                
                // now try and increase brightness
//                newImage[(currentY-1)*inHeight+m] = inImage2[(currentX-1)*inHeight+m]*myDifference;
            }
        }
        else
            assert("eh!?");
        
        // see where to move next
        float value1 = DTW[currentY*(inWidth+1)+currentX-1]; // left
        float value2 = DTW[(currentY-1)*(inWidth+1)+currentX-1]; // diag
        float value3 = DTW[(currentY-1)*(inWidth+1)+currentX]; // above
        
        // don't allow to jump by more than 2 spaces at a time
        /*
        if (xCount > 2 || yCount > 2) {
            currentX--;
            currentY--;
            xCount = 0;
            yCount = 0;
        }
        else {*/
        {
            // left
            if (value1 < value2 && value1 < value3) {
                currentX--;
                xCount++;
                yCount = 0;
            }
            else {
                // diag
                if (value2 < value1 && value2 < value3) {
                    currentX--;
                    currentY--;
                    xCount = 0;
                    yCount = 0;
                }
                else {
                    // above
                    currentY--;
                    yCount++;
                    xCount = 0;
                }
            }
        }
    }
    free(DTW);
    
    // do a little test to apply constrast to each 24-length segment
    /*
    for (int s=0; s< inWidth; s+= 24) {
        [VoiceSample applyContrast:&inImage2[s*inHeight] ofWidth:inWidth-s < 24?inWidth-s:24 andHeight:inHeight notUsedPercentage:0.0f];
        [VoiceSample applyContrast:&newImage[s*inHeight] ofWidth:inWidth-s < 24?inWidth-s:24 andHeight:inHeight notUsedPercentage:0.0f];
    }*/
    
    
    // try another little test here, find cost of each vowel, but dynamic time warp
    // vertically, so each line cannot be off-kilter
    startPos = 0;
    float verticalCost = 0.0f;
    for (int i=0; i < inPhonemeLengthArray.count; i++) {
        int thisLength = (int)([[inPhonemeLengthArray objectAtIndex:i] floatValue]*(float)inWidth);
        float cost = 0.0f;
        // get overall cost for phoneme
//        float cost = [VoiceSample compareImage3_horizontal:&inImage1[startPos*inHeight] ofWidth:thisLength andHeight:inHeight withImage:&newImage[startPos*inHeight] withLeeway:3 stretchImage2:YES];
        
        // break into quarters and compare each
        int length1 = thisLength/2;
        int length2 = thisLength-length1;
        int newHeight = inHeight/2;
        
        // create buffers
        // 1 3   5 7
        // 2 4   6 8
        float *buffer1 = malloc(sizeof(float)*length1*newHeight);
        float *buffer2 = malloc(sizeof(float)*length1*newHeight);
        float *buffer3 = malloc(sizeof(float)*length2*newHeight);
        float *buffer4 = malloc(sizeof(float)*length2*newHeight);
        float *buffer5 = malloc(sizeof(float)*length1*newHeight);
        float *buffer6 = malloc(sizeof(float)*length1*newHeight);
        float *buffer7 = malloc(sizeof(float)*length2*newHeight);
        float *buffer8 = malloc(sizeof(float)*length2*newHeight);
        
        // copy data
        for (int r=0; r < inHeight; r++) {
            for (int c=0; c < length1; c++) {
                if (r < newHeight) {
                    buffer1[c*newHeight+r] = inImage1[(c+startPos)*inHeight+r];
                    buffer5[c*newHeight+r] = newImage[(c+startPos)*inHeight+r];
                }
                else {
                    buffer2[c*newHeight+r-newHeight] = inImage1[(c+startPos)*inHeight+r];
                    buffer6[c*newHeight+r-newHeight] = newImage[(c+startPos)*inHeight+r];
                }
            }
        }
        for (int r=0; r < inHeight; r++) {
            for (int c=0; c < length2; c++) {
                if (r < newHeight) {
                    buffer3[c*newHeight+r] = inImage1[(c+startPos+length1)*inHeight+r];
                    buffer7[c*newHeight+r] = newImage[(c+startPos+length1)*inHeight+r];
                }
                else {
                    buffer4[c*newHeight+r-newHeight] = inImage1[(c+startPos+length1)*inHeight+r];
                    buffer8[c*newHeight+r-newHeight] = newImage[(c+startPos+length1)*inHeight+r];
                }
            }
        }
        
        
        float cost1 = [VoiceSample compareImage3_horizontal:buffer1 ofWidth:length1 andHeight:newHeight withImage:buffer5 withLeeway:0 stretchImage2:NO] / (float)newHeight;
        float cost2 = [VoiceSample compareImage3_horizontal:buffer2 ofWidth:length1 andHeight:newHeight withImage:buffer6 withLeeway:0 stretchImage2:NO] / (float)newHeight;
        float cost3 = [VoiceSample compareImage3_horizontal:buffer3 ofWidth:length2 andHeight:newHeight withImage:buffer7 withLeeway:0 stretchImage2:NO] / (float)newHeight;
        float cost4 = [VoiceSample compareImage3_horizontal:buffer4 ofWidth:length2 andHeight:newHeight withImage:buffer8 withLeeway:0 stretchImage2:NO] / (float)newHeight;
        
        printf("cost 1 is %f, cost2 is %f, cost3 is %f, cost4 is %f, original cost is %f\r\n", cost1, cost2, cost3, cost4, cost/(float)inHeight);
        
        cost = MAX(cost1, MAX(cost2, MAX(cost3, cost4)));
               
        if (cost > verticalCost)
            verticalCost = cost;
        printf("vertical dtw cost of phoneme %d is %f\r\n", i, cost);
        
        startPos += thisLength;
    }    
    
    // use the phoneme lengths to apply constrast to individual phonemes to see if we get
    // a better result
    /*
    startPos = 0;
    for (int i=0; i < inPhonemeLengthArray.count; i++) {
        int thisLength = (int)([[inPhonemeLengthArray objectAtIndex:i] floatValue]*(float)inWidth);
        [VoiceSample applyContrast:&inImage1[startPos*inHeight] ofWidth:thisLength andHeight:inHeight notUsedPercentage:0.0f];
        [VoiceSample applyContrast:&newImage[startPos*inHeight] ofWidth:thisLength andHeight:inHeight notUsedPercentage:0.0f];
        startPos += thisLength;
    }
     */
    
    /*
    // TEST to make newimage have the same magnitude as inimage1
    for (int s=0; s < inWidth; s++) {
        // calculate totals for both
        float total1 = 0.0f;
        float total2 = 0.0f;
        for (int i=0; i < inHeight; i++) {
            total1 += inImage1[s*inHeight+i];
            total2 += newImage[s*inHeight+i];
        }
        
        // find ratio of difference
        float ratio = total1/total2;
        
        if (ratio > 1.2f)
            ratio = 1.2;
        if (ratio < 0.8f)
            ratio = 0.8f;
        
        // now reduce brightness and apply contrast?
        float blackDistribution = 0.0f;
        float whiteDistribution = 0.0f;
        [VoiceSample changeBrightnessTo:-(1.0f-ratio)*2.0f contrastTo:(1.0f-ratio)*2.0f forBuffer:&newImage[s*inHeight] toBuffer:&newImage[s*inHeight] ofWidth:1 andHeight:inHeight blackDistribution:&blackDistribution whiteDistribution:&whiteDistribution];
        
    }
    */
    
    /*
     no more velocity
    
    // calculate moving average on stretched image
    float *movingAvg = malloc(sizeof(float)*inWidth*compareFeatureCount);
    for (int s=0; s < inWidth; s++) {
        for (int i=0; i < compareFeatureCount; i++) {
            float p1 = 0.0f;
            for (int m=-2; m < 2; m++) {
                p1 += newImage[MIN(MAX(0,s+m),inWidth-1)*inHeight+i];
            }
            movingAvg[s*compareFeatureCount+i] = p1/4.0f;
        }
    }
    
    // recalculate velocity on stretched image
    float *mfccVelocityBuffer = malloc(sizeof(float)*inWidth*compareFeatureCount);
    for (int s=-0; s < inWidth-0; s++) {
        for (int i=0; i < compareFeatureCount; i++) {
            float p1 = 0.0f;
            float p2 = 0.0f;
            for (int m=-2; m < 2; m++) {
                p1 += (float)m * (newImage[MIN(MAX(0,s+m),inWidth-1)*inHeight+i]-newImage[MIN(MAX(0,s-m),inWidth-1)*inHeight+i]);
                p2 += powf((float)m, 2.0f);
            }
            mfccVelocityBuffer[(s+0)*compareFeatureCount+i] = p1/(2*p2);
//            mfccVelocityBuffer[(s+0)*compareFeatureCount+i] = s<0?0.5f:newImage[MIN(MAX(0,s),inWidth-1)*inHeight+i];
        }
    }
    
    // recalculate velocity on stretched image
    float *mfccVelocityBuffer2 = malloc(sizeof(float)*inWidth*compareFeatureCount);
    for (int s=-4; s < inWidth-4; s++) {
        for (int i=0; i < compareFeatureCount; i++) {
            float p1 = 0.0f;
            float p2 = 0.0f;
            for (int m=-2; m < 2; m++) {
                p1 += (float)m * (newImage[MIN(MAX(0,s+m),inWidth-1)*inHeight+i]-newImage[MIN(MAX(0,s-m),inWidth-1)*inHeight+i]);
                p2 += powf((float)m, 2.0f);
            }
            mfccVelocityBuffer2[(s+4)*compareFeatureCount+i] = p1/(2*p2);
//            mfccVelocityBuffer2[(s+4)*compareFeatureCount+i] = s<0?0.5f:newImage[MIN(MAX(0,s),inWidth-1)*inHeight+i];
        }
    }
    
    // recalculate velocity on stretched image
    float *mfccVelocityBuffer3 = malloc(sizeof(float)*inWidth*compareFeatureCount);
    for (int s=-8; s < inWidth-8; s++) {
        for (int i=0; i < compareFeatureCount; i++) {
            float p1 = 0.0f;
            float p2 = 0.0f;
            for (int m=-2; m < 2; m++) {
                p1 += (float)m * (newImage[MIN(MAX(0,s+m),inWidth-1)*inHeight+i]-newImage[MIN(MAX(0,s-m),inWidth-1)*inHeight+i]);
                p2 += powf((float)m, 2.0f);
            }
            mfccVelocityBuffer3[(s+8)*compareFeatureCount+i] = p1/(2*p2);
//            mfccVelocityBuffer3[(s+8)*compareFeatureCount+i] = s<0?0.5f:newImage[MIN(MAX(0,s),inWidth-1)*inHeight+i];
        }
    }
    
    // normalize buffers
    [VoiceSample normalizeBufferAroundZeroToPoint5:mfccVelocityBuffer ofSize:inWidth*compareFeatureCount usingMax:0.15f];
    [VoiceSample normalizeBufferAroundZeroToPoint5:mfccVelocityBuffer2 ofSize:inWidth*compareFeatureCount usingMax:0.15f];
    [VoiceSample normalizeBufferAroundZeroToPoint5:mfccVelocityBuffer3 ofSize:inWidth*compareFeatureCount usingMax:0.15f];
    */
    
    // copy moving average and velocitys to stretched image
    /*
   for (int s=0; s < inWidth; s++) {
        int featurePos = 0;
        for (int i=0; i < compareFeatureCount; i++)
            newImage[s*inHeight+featurePos++] = newImage[s*inHeight+i];
//        newImage[s*inHeight+featurePos++] = movingAvg[s*inHeight/4+i];
        for (int i=0; i < compareFeatureCount; i++)
            newImage[s*inHeight+featurePos++] = mfccVelocityBuffer[s*compareFeatureCount+i];
        for (int i=0; i < compareFeatureCount; i++)
            newImage[s*inHeight+featurePos++] = mfccVelocityBuffer2[s*compareFeatureCount+i];
        for (int i=0; i < compareFeatureCount; i++)
            newImage[s*inHeight+featurePos++] = mfccVelocityBuffer3[s*compareFeatureCount+i];
    }
    */
    
    /*
    // CALCULATE HISTORY FOR FEATURES GOING BACK 'N' STEPS AT A TIME
    int stepSize = 1;
    for (int s=0; s < inWidth; s++) {
        int featurePos = 0;
        int wOffset = -1;
        wOffset = 0-(([self getOutputFeatureCount] / [self getCompareFeatureCount]-1)/2);
        for (int i=compareFeatureCount; i < inHeight; i++) {
            // copy data going backwards
            newImage[s*inHeight+i] = s-wOffset<0?0.5f:newImage[MIN(MAX(0,s-wOffset),inWidth-1)*inHeight+featurePos];
            inImage1[s*inHeight+i] = s-wOffset<0?0.5f:inImage1[MIN(MAX(0,s-wOffset),inWidth-1)*inHeight+featurePos];
            
            // go to next feature, reset feature pointer if we're larger than compare count
            featurePos++;
            if (featurePos >= compareFeatureCount) {
                featurePos = 0;
                wOffset += stepSize;
                // don't include same as original!
                if (wOffset == 0)
                    wOffset += stepSize;
            }
        }
    }
    */
    
    /*
    // go through and stretch downwards so we can see what it looks like
    for (int i=0; i < inWidth; i++) {
        float thisCost = [self compareImage3_1:&inImage1[i*inHeight] ofWidth:inHeight withImage:&newImage[i*inHeight] withLeeway:1 stretchImage2:YES];
    
        // try applying constrast again too
//        [VoiceSample applyContrast:&newImage[i*inHeight] ofWidth:1 andHeight:inHeight notUsedPercentage:0.0f];
    }
     */
    
    /*
    // apply auto brightness/contrast to each column
    int startPos = 0;
    for (int i=0; i < inWidth; i++) {
        int thisLength = 1;
        [VoiceSample applyContrast:&inImage1[startPos*inHeight] ofWidth:thisLength andHeight:inHeight notUsedPercentage:0.0f];
        [VoiceSample applyContrast:&newImage[startPos*inHeight] ofWidth:thisLength andHeight:inHeight notUsedPercentage:0.0f];
        startPos++;
    }
    */
    
	// write image out as bitmap
    if (inWriteDebug) {
        [VoiceSample writeToImageWithFilename:@"compare1_nocms.png" alternateBuffer:inImage1 alternateFeatureCount:inHeight alternateQuantizeCount:inWidth];
        [VoiceSample writeToImageWithFilename:@"compare2_nocms.png" alternateBuffer:newImage alternateFeatureCount:inHeight alternateQuantizeCount:inWidth];
    }
    
    // free buffers
    /*
    free(movingAvg);
    free(mfccVelocityBuffer);
    free(mfccVelocityBuffer2);
    free(mfccVelocityBuffer3);
     */
    
//    if (inWriteDebug)
//        [VoiceSample writeToImageWithFilename:@"compare2_nocms_stretch.png" alternateBuffer:newImage alternateFeatureCount:inHeight alternateQuantizeCount:inWidth];

    // get application delegate
    McpAppDelegate *appDelegate = (McpAppDelegate*)[[UIApplication sharedApplication] delegate];
    NeuralNet *myNet = [appDelegate.currentUserProfile getNetForAudioDevice];
    NeuralNet *cpuNet = [appDelegate.currentCpuProfile getNetForAudioDevice];
    
    /*
    ** COMMENT OUT ADD TO TRAINING SAMPLES FOR NOW, AS WE WILL ADD FULL AUDIO
    ** TO TRAINING SAMPLES FOR TESTING
    // if we are in calibration mode, add to training data
    if (inCalibrationMode) {
        // get correct training data depending on audio device
        NewTrainingData *myTrainingData = [appDelegate.currentUserProfile getTrainingDataForAudioDevice];
        
        // add time matched data to history
        NSString *sampleKey = inText;
        // find the name of the phoneme
        sampleKey = [sampleKey stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@".,!?？！。、"]];
//        sampleKey = [[NSNumber numberWithInt:myTrainingData.trainingSamples.count+1] stringValue];
        
        // TEST!! SET THIS BACK TO USE NEWIMAGE FOR USER FEATURES
//        NewTrainingSample *newSample = [[NewTrainingSample alloc] initWithText:inText featureCount:inHeight size:inWidth userFeatures:newImage cpuFeatures:inImage1];
        
        NewTrainingSample *newSample = [[NewTrainingSample alloc] initWithText:inText featureCount:inHeight size:inWidth userFeatures:newImage cpuFeatures:inImage1];
        
        [myTrainingData.trainingSamples setObject:newSample forKey:sampleKey];
        [newSample release];
        
        // only keep a maximum of 50 samples
        if (myTrainingData.trainingSamples.count > 50) {
            NSEnumerator *enumerator = [myTrainingData.trainingSamples keyEnumerator];
            NSString *nextKey = [enumerator nextObject];
            [myTrainingData.trainingSamples removeObjectForKey:nextKey];
        }
        
        // find total number of samples
        NSEnumerator *enumerator = [myTrainingData.trainingSamples keyEnumerator];
        NSString *nextKey;
        int totalSampleCount = 0;
        while ((nextKey = [enumerator nextObject])) {
            NewTrainingSample *thisSample = [myTrainingData.trainingSamples objectForKey:nextKey];
            totalSampleCount += thisSample.size;
        }
        */
    
        /*
        
        int inPos = 0;
        int outPos = 0;
        float *inputData = malloc(sizeof(float)*totalSampleCount*inHeight);
        float *outputData = malloc(sizeof(float)*totalSampleCount*inHeight);
        enumerator = [myTrainingData.trainingSamples keyEnumerator];
        while ((nextKey = [enumerator nextObject])) {
            NewTrainingSample *thisSample = [myTrainingData.trainingSamples objectForKey:nextKey];
            for (int s=0; s < thisSample.size; s++) {
                for (int i=0; i < inHeight; i++) {
                    inputData[inPos++] = thisSample.userFeatures[s*inHeight+i];
                    outputData[outPos++] = thisSample.cpuFeatures[s*inHeight+i];
                }
            }
        }
        
//        [VoiceSample writeToImageWithFilename:@"compare2_nocms_history.png" alternateBuffer:inputData alternateFeatureCount:inHeight alternateQuantizeCount:totalSampleCount];
        free(inputData);
        free(outputData);
         */
//    }
    
    // try a test here... concatenante phonemes from training samples and replace
    // inimage1
    
    /*
    // data to pass to neural network for translation must be double not float
    // pass in both features and try and calc difference
    double *thisData = malloc(sizeof(double)*inHeight*inWidth);
    double *cpuData = malloc(sizeof(double)*inHeight*inWidth);
    int inPos = 0;
    int inPos1 = 0;
    for (int i=0; i < inWidth; i++) {
        for (int m=0; m < inHeight; m++)
            thisData[inPos++] = newImage[i*inHeight+m];
        for (int m=0; m < inHeight; m++)
            cpuData[inPos1++] = inImage1[i*inHeight+m];
//        for (int m=0; m < inHeight; m++)
//            thisData[inPos++] = inImage1[i*inHeight+m];
    }
    
    // now translate user data
    float *transImage = malloc(sizeof(float)*inWidth*outputFeatureCount);
    float *transCpuImage = malloc(sizeof(float)*inWidth*outputFeatureCount);
    for (int i=0; i < inWidth; i++) {
        double *trans = [myNet updateWithInputs:&thisData[i*inHeight]];
        double *cpuTrans = [cpuNet updateWithInputs:&cpuData[i*inHeight]];
        for (int s=0; s < outputFeatureCount; s++) {
            if (s < outputFeatureCount) {
                transImage[i*(outputFeatureCount)+s] = trans[s];
                transCpuImage[i*(outputFeatureCount)+s] = cpuTrans[s];
            }
            else {
                transImage[i*(outputFeatureCount)+s] = newImage[i*(inHeight)+s];
                transCpuImage[i*(outputFeatureCount)+s] = inImage1[i*(inHeight)+s];
            }
        }
        free(trans);
        free(cpuTrans);
    }
    free(thisData);
    free(cpuData);
    
    // WE CAN WRITE OUT DEBUG INFORMATION HERE IF WE WANT TO SEE HOW CLOSE
    // THE TRANSLATED NETWORK IS GETTING DURING TRAINING IN ORDER TO SET THE GOOD/BAD
    // BOUNDARIES
    // debugging, write out translated sample
//    if (inWriteDebug) {
        [VoiceSample writeToImageWithFilename:@"compare2_nocms_trans.png" alternateBuffer:transImage alternateFeatureCount:outputFeatureCount alternateQuantizeCount:inWidth];
        [VoiceSample writeToImageWithFilename:@"compare1_nocms_trans.png" alternateBuffer:transCpuImage alternateFeatureCount:outputFeatureCount alternateQuantizeCount:inWidth];
//    }
     */
    
    /*
    // this next block is for trying to link sound to actual phoneme
    // don't use if we're just converting to translated image
    // create check image for testing if we're in calibration mode
    int compareWidth = 0;
    int checkPos = 0;
    int againstPos = 0;
    float *checkImage = malloc(sizeof(float)*inWidth*outputFeatureCount);
    float *againstImage = malloc(sizeof(float)*inWidth*outputFeatureCount);
    int compareLength = 0; // may not be same as width if there's ignored features
    if (inCompareWithCheck) {
        // find text of word and remove punctuation for processing
        NSString *wordName = [inText stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@".,!?？！。、"]];
        
        // find our training word
        TrainingWord *word = [appDelegate.trainingWords objectForKey:wordName];
        
        // assert if word not found
        if (word == nil)
            NSAssert(0, @"Training word not found");
        
        // find real feature index array for phonemes
        for (int s=0; s < inWidth; s++) {
            // get feature index at this position
            int featureIndex = [word getFeatureIndexAtPercentPos:(float)s/(float)inWidth];
            
            // assert if we didn't find feature index
            // we're now using this for ignored features
//            if (featureIndex == -1)
//                NSAssert(0, @"Feature index not found");
            if (featureIndex > outputFeatureCount-1)
                NSAssert(0, @"Feature index out of bounds");
            if (featureIndex != -1) {
                for (int i=0; i < outputFeatureCount; i++) {
                    if (featureIndex == i)
                        checkImage[compareWidth*outputFeatureCount+i] = 1.0f;
                    else
                        checkImage[compareWidth*outputFeatureCount+i] = 0.0f;
                    againstImage[compareWidth*outputFeatureCount+i] = transCpuImage[s*outputFeatureCount+i];
                }
                compareWidth++;
            }
        }    
    }
    else
        compareWidth = inWidth;
    
    int j=0;
    if (compareWidth != inWidth) {
        j++;
    }
    
    [VoiceSample writeToImageWithFilename:@"compare2_nocms_check.png" alternateBuffer:checkImage alternateFeatureCount:outputFeatureCount alternateQuantizeCount:inWidth];
    [VoiceSample writeToImageWithFilename:@"compare1_nocms_check.png" alternateBuffer:checkImage alternateFeatureCount:outputFeatureCount alternateQuantizeCount:inWidth];
    */
    
    // test each phoneme independently
    startPos = 0;
    float newCost = 0.0f;
    float newWorstSegment = 0.0f;
    for (int p=0; p < inPhonemeLengthArray.count; p++) {
//        BufferPhoneme *thisPhoneme = [inPhonemeLengthArray objectAtIndex:p];
//        int thisLength = (int)(thisPhoneme.length*(float)inWidth);
        int thisLength = (int)([[inPhonemeLengthArray objectAtIndex:p] floatValue]*(float)inWidth);
        for (int i=startPos; i < startPos+thisLength; i++) {
            int startY = 0;
            int endY = compareFeatureCount-startY-1;
            
            // allow more leeway as you get closer to the center, which is further from
            // the potential distortion by neighboring phonemes
            float tukey = [self TukeyFuncWithWidth:thisLength andPosition:i-startPos];
            int leeway = 2-(int)(2.0f*tukey)+2;
            float leewayPercent = 1.0f-0.2f*tukey;
            float thisCost = [self compareImage3_1:&inImage1[i*inHeight+startY] ofWidth:endY-startY withImage:&newImage[i*inHeight+startY] withLeeway:0 stretchImage2:NO];
            newCost += thisCost;
//            printf("column %d cost is %f, leeway is %d\r\n", i, thisCost, leeway);
        }
        newCost = newCost / (float)(compareFeatureCount*thisLength);
        printf("new segment cost is %f, processed %d samplesr\n", newCost, thisLength);
        if (newCost > newWorstSegment)
            newWorstSegment = newCost;
        newCost = 0.0f;
        startPos += thisLength;
    }
    
    // now we have warped image, we can try and compare segments to find worst
    // test score against warped image
    int compareWidth = inWidth;
    int segmentSize = compareWidth < 48 ? 12 : 24;
    int processedCount = 0;
    float worstSegment = 0.0f;
    float cost=0;
//    float processSegmentBand[256];
//    int processSegmentBandCount = 0;
//    int midBandCount = 0;
//    int startBand = -1;
//    int endBand = -1;
    for (int i=0; i < compareWidth; i++) {
        // calculate cost
        // calcualate cost against real feature indexes
        
        // TEST USING CPU FEATURES!!!
//        cost += [self compareImage3_1:&checkImage[i*inHeight+0] ofWidth:outputFeatureCount withImage:&transCpuImage[i*inHeight]];
/*
        // if we're calibrating use real expected value, not cpu one
        if (inCompareWithCheck)
            cost += [self compareImage3_1:&checkImage[i*outputFeatureCount+0] ofWidth:outputFeatureCount withImage:&againstImage[i*outputFeatureCount] withLeeway:0];
        else
            cost += [self compareImage3_1:&transCpuImage[i*outputFeatureCount+0] ofWidth:outputFeatureCount withImage:&againstImage[i*outputFeatureCount] withLeeway:0];
*/
        
        // do straight compare of input images
        int startY = 0;
        int endY = compareFeatureCount;
        float thisCost = [self compareImage3_1:&inImage1[i*inHeight+startY] ofWidth:endY-startY withImage:&newImage[i*inHeight+startY] withLeeway:0 stretchImage2:NO];
        cost += thisCost;
//        cost += [self compareImage3_1:&inImage1[i*inHeight+startY] ofWidth:endY-startY withImage:&transImage[i*inHeight] withLeeway:2];
        
        /*
        float columnTotal = 0.0f;
        printf("\r\n");
        for (int m=0; m < inHeight; m++) {
            if (processSegmentBand[m]) {
                float thisCost;
                float value1 = inImage1[i*inHeight+m];
                float value2 = transImage[i*inHeight+m];
                if (value1 > value2)
                    thisCost = value1 - value2;
                else
                    thisCost = value2 - value1;
                cost += thisCost;
                columnTotal += thisCost;
//                printf("%f-%f=%f,", value1, value2,thisCost);
            }
        }
//        printf("\r\ncolumn %d total is %f\r\n", i, columnTotal);
*/
        
        processedCount++;
        if (processedCount == segmentSize && compareWidth-i > segmentSize) {
            cost = cost / (float)(compareFeatureCount*processedCount);
//            cost = cost / (float)(compareFeatureCount*processedCount);
            printf("segment cost is %f, processed %d samplesr\n", cost, processedCount);
            if (cost > worstSegment)
                worstSegment = cost;
            cost = 0.0f;
            processedCount = 0;
        }
    }
    if (processedCount > 0) {
        printf("final cost before divide is %f\r\n", cost);
        cost = cost / (float)(compareFeatureCount*processedCount);
        printf("final segment cost is %f, processed %d samples\r\n", cost, processedCount);
        if (cost > worstSegment)
            worstSegment = cost;
    }
    printf("worst segment is %f\r\n", worstSegment);
    
    // free image
    free(newImage);
    /*
    free(transImage);
    free(transCpuImage);
    free(checkImage);
    free(againstImage);
    */
    
    // return basic score because trainer will work out spread
    return worstSegment;
    return newWorstSegment;
    return verticalCost;
}

-(float)covarianceOf:(float*)inArray1 with:(float*)inArray2 length:(int)inLength {
	// find autocovariance for this frame
	float totalX = 0.0f;
	float totalY = 0.0f;
	float totalXY = 0.0f;
	for (int j=0; j < inLength; j++) {
		totalX += inArray1[j]+1.0f;
		totalY += inArray2[j]+1.0f;
		totalXY += ((inArray1[j]+1.0f)*(inArray2[j]+1.0f));
	}
	float meanX = totalX / inLength;
	float meanY = totalY / inLength;
	float meanXY = totalXY / inLength;
	float diff = meanX * meanY;
	
	return meanXY - diff;
}

-(float)correlationOf:(float*)inArray1 with:(float*)inArray2 length:(int)inLength {
	float sumXy = 0.0f, sumXx = 0.0f, sumYy = 0.0f, sumX = 0.0f, sumY = 0.0f;
	int N = 0;
	for (int i = 0; i < inLength; i++)
	{
		// get source and destination values
		float x = inArray1[i];
		float y = inArray2[i];
		
		// calculate totals
		sumXy += x*y;
		sumXx += x*x;
		sumYy += y*y;
		sumX += x;
		sumY += y;
		N++;
	}
	
	// use weighting function for importance of mfcc's
	
	// calculate correlation for this mfcc
	float corr = 1.0f;
	if (sqrt((N*sumXx - sumX*sumX) * (N*sumYy - sumY*sumY)) != 0.0f)
		corr = (N*sumXy - sumX*sumY) / sqrt((N*sumXx - sumX*sumX) * (N*sumYy - sumY*sumY));
//	if (corr < 0.0f)
//		corr = 0.0f;
	return corr;
}

// split method
-(float)compareImage4:(float*)inImage1 ofWidth:(int)inWidth andHeight:(int)inHeight withImage:(float*)inImage2 {
    // only process lines (bands) where the average power is above a certain threshold, otherwise we get false positives for all the black
    int processCount = 0;
    BOOL processBand[256];
    for (int m=0; m < inHeight; m++) {
        float bandAvg1 = 0.0f;
        float bandMax1 = 0.0f;
        float bandAvg2 = 0.0f;
        float bandMax2 = 0.0f;
        for (int i=0; i < inWidth; i++) {
            bandAvg1 += inImage1[i*inHeight+m];
            if (inImage1[i*inHeight+m] > bandMax1)
                bandMax1 = inImage1[i*inHeight+m];
            bandAvg2 += inImage1[i*inHeight+m];
            if (inImage2[i*inHeight+m] > bandMax2)
                bandMax2 = inImage2[i*inHeight+m];
        }
        bandAvg1 /= (float)inWidth;
        bandAvg2 /= (float)inWidth;
        if (bandMax1 > 0.1f || bandMax2 > 0.1f) {
            processBand[m] = YES;
            processCount++;
        }
        else {
            //            printf("don't process band %d\r\n", m);
            processBand[m] = NO;
        }
    }    
  
    // recursively split in half and test 20% in either direction, best score
    // of either side wins
    return [self compareImageSplit:inImage1 ofWidth:inWidth andHeight:inHeight startPosition:0 withImage:inImage2 segmentSize:inWidth processBands:processBand];
}

-(float)compareImageSplit:(float*)inImage1 ofWidth:(int)inWidth andHeight:(int)inHeight startPosition:(int)inStartPosition withImage:(float*)inImage2 segmentSize:(int)inSegmentSize processBands:(BOOL*)processBand {
    // recursively split in half and test 20% in either direction, best score
    // of either side wins, then we do the same with each half until we are smaller
    // than segment size
    int segmentSize = 16;
    
    // find midpoint
    int midPoint = inSegmentSize/2;
    
    // loop through tests
    float minSegmentDiff = 9999.0f;
    float otherSegmentDiff = 0.0f;
    float minStretchPercent = 0.0f;
    for (float i=0.5; i < 1.5; i += 0.05f) {
        // get score for left side
        float leftDiff = [self compareSegmentOfImage:inImage1 withImage:inImage2 ofWidth:inWidth andHeight:inHeight image1StartPosition:inStartPosition image2StartPosition:inStartPosition segmentSize:midPoint stretchPercentage:i processBands:processBand boost:NO];
        
        // get score for right side
        float rightDiff = [self compareSegmentOfImage:inImage1 withImage:inImage2 ofWidth:inWidth andHeight:inHeight image1StartPosition:inStartPosition+midPoint image2StartPosition:inStartPosition+(int)roundf((float)midPoint*i) segmentSize:midPoint stretchPercentage:2.0f-i processBands:processBand boost:NO];
        
        // take best score of either side
        if (leftDiff < minSegmentDiff)
        {
            minSegmentDiff = leftDiff;
            otherSegmentDiff = rightDiff;
            minStretchPercent = i;
        }
        if (rightDiff < minSegmentDiff)
        {
            minSegmentDiff = rightDiff;
            otherSegmentDiff = leftDiff;
            minStretchPercent = i;
        }
    }
    
    // get score for left side
    float boostedLeftDiff = [self compareSegmentOfImage:inImage1 withImage:inImage2 ofWidth:inWidth andHeight:inHeight image1StartPosition:inStartPosition image2StartPosition:inStartPosition segmentSize:midPoint stretchPercentage:minStretchPercent processBands:processBand boost:YES];
    
    // get score for right side
    float boostedRightDiff = [self compareSegmentOfImage:inImage1 withImage:inImage2 ofWidth:inWidth andHeight:inHeight image1StartPosition:inStartPosition+midPoint image2StartPosition:inStartPosition+(int)roundf((float)midPoint*minStretchPercent) segmentSize:midPoint stretchPercentage:2.0f-minStretchPercent processBands:processBand boost:YES];
    
    // if we split again and it's smaller than segment size, return answer here
    // which is the worst of either side
    if (midPoint/2 < segmentSize)
        return MAX(boostedLeftDiff,boostedRightDiff);
    
    // recursively call this proc again for each side and add the difference
    float leftDiff = [self compareImageSplit:inImage1 ofWidth:inWidth andHeight:inHeight startPosition:inStartPosition withImage:inImage2 segmentSize:(int)((float)midPoint*minStretchPercent) processBands:processBand]; 
    float rightDiff = [self compareImageSplit:inImage1 ofWidth:inWidth andHeight:inHeight startPosition:inStartPosition+(int)roundf((float)midPoint*minStretchPercent) withImage:inImage2 segmentSize:(int)roundf((float)midPoint*(2.0f-minStretchPercent)) processBands:processBand];
    
    // return worst score
    return MAX(leftDiff, rightDiff);
}

-(float)compareImage:(float*)inImage1 ofWidth:(int)inWidth andHeight:(int)inHeight withImage:(float*)inImage2 {
    // only process lines (bands) where the average power is above a certain threshold, otherwise we get false positives for all the black
    int processCount = 0;
    BOOL processBand[256];
    for (int m=0; m < inHeight; m++) {
        float bandAvg = 0.0f;
        float bandMax = 0.0f;
        for (int i=1; i < inWidth; i++) {
            bandAvg += inImage1[i*inHeight+m];
            if (inImage1[i*inHeight+m] > bandMax)
                bandMax = inImage1[i*inHeight+m];
        }
        bandAvg /= (float)inWidth;
        if (bandMax > 0.05f) {
            processBand[m] = YES;
            processCount++;
        }
        else
            processBand[m] = NO;
    }
    
    // hardcode to miss low and high end
    /*
    for (int i=0; i < 24; i++)
        processBand[i]=YES;
    processBand[0] = NO;
    processBand[23] = NO;
    processBand[22] = NO;
    processBand[21] = NO;
    processBand[20] = NO;
    processBand[19] = NO;
    processBand[18] = NO;
     */
    
    // reverse images for backwards compare
    float *newImage1 = malloc(sizeof(float)*inWidth*inHeight);
    float *newImage2 = malloc(sizeof(float)*inWidth*inHeight);
    for (int i=0; i < quantizeCount; i++) {
        for (int s=0; s < featureCount; s++) {
            newImage1[i*featureCount+s] = inImage1[(inWidth-1-i)*inHeight+s];
            newImage2[i*featureCount+s] = inImage2[(inWidth-1-i)*inHeight+s];
        }
    }
    
    // because the final stretch is forced, find segment scores going forward
    // then going backwards and use the first half of forwards and the last half
    // of backwards
    NSArray *forwards = [self getSegmentScores:inImage1 ofWidth:inWidth andHeight:inHeight withImage:inImage2 processBands:processBand test:NO];
//    NSArray *backwards = [self getSegmentScores:newImage1 ofWidth:inWidth andHeight:inHeight withImage:newImage2 processBands:processBand];
    
    // now find mid-point and use first half forward scores, and back-half backwards
    // scores to find worth
//    int halfPos = ceil(((float)forwards.count+(float)backwards.count)/4.0f);
    float worstScore1 = 9999.0f;
    float worstScore2 = 9999.0f;
    float worstScore3 = 9999.0f;
    /*
    for (int i=0; i < halfPos; i++) {
        NSNumber *number1 =[forwards objectAtIndex:i];
        NSNumber *number2 =[backwards objectAtIndex:i];
        if ([number1 floatValue] > worstScore1)
            worstScore1 = [number1 floatValue];
        if ([number2 floatValue] > worstScore1)
            worstScore1 = [number2 floatValue];
    }*/
    for (int i=0; i < forwards.count; i++) {
        StretchDetail *detail =[forwards objectAtIndex:i];
        if (detail.score > worstScore2)
            worstScore2 = detail.score;
    }
    /*
    for (int i=0; i < backwards.count; i++) {
        StretchDetail *detail =[backwards objectAtIndex:i];
        if (detail.score > worstScore3)
            worstScore3 = detail.score;
    }
    */
    
    // convert lists to image and save for debugging
    float *newImage = [self buildBufferUsingStretchList:forwards andImage:inImage2 ofWidth:inWidth andHeight:inHeight];
    [VoiceSample writeToImageWithFilename:@"compare2_nocms_stretch.png" alternateBuffer:newImage alternateFeatureCount:featureCount alternateQuantizeCount:quantizeCount];
    
    /*
    // now, offset by half segment size and try again
    NSArray *forwards2 = [self getSegmentScores:inImage1 ofWidth:inWidth andHeight:inHeight withImage:newImage processBands:processBand test:YES];
    float *newImage_1 = [self buildBufferUsingStretchList:forwards2 andImage:newImage ofWidth:inWidth andHeight:inHeight];
    [self writeToImageWithFilename:@"compare2_nocms_stretch_1.png" alternateBuffer:newImage_1 alternateFeatureCount:featureCount alternateQuantizeCount:quantizeCount];
*/
    
    /*
    // take this new image and reverse it, then run it through again
    // reverse images for backwards compare
    float *newImage2_1 = malloc(sizeof(float)*inWidth*inHeight);
    for (int i=0; i < quantizeCount; i++) {
        for (int s=0; s < featureCount; s++) {
            newImage2_1[i*featureCount+s] = newImage[(inWidth-1-i)*inHeight+s];
        }
    }
    NSArray *backwards = [self getSegmentScores:newImage1 ofWidth:inWidth andHeight:inHeight withImage:newImage2_1 processBands:processBand];
    float *newImage_1 = [self buildBufferUsingStretchList:backwards andImage:newImage2_1 ofWidth:inWidth andHeight:inHeight];
    [self writeToImageWithFilename:@"compare2_nocms_stretch_1.png" alternateBuffer:newImage_1 alternateFeatureCount:featureCount alternateQuantizeCount:quantizeCount];
    */
    
    free(newImage);
    free(newImage1);
    free(newImage2);
    
	// return the best of the worst
	return MIN(MIN(worstScore1,worstScore2),worstScore3);
}

-(float*)buildBufferUsingStretchList:(NSArray*)inStretchList andImage:(float*)inImage ofWidth:(int)inWidth andHeight:(int)inHeight {
    // create buffer
    float *newImage = malloc(sizeof(float)*inHeight*inWidth);
    
    // loop through stretch list
    int imagePos = 0;
    int newPos = 0;
    for (int s=0; s < inStretchList.count; s++) {
        // get stretch percent
        StretchDetail *detail = [inStretchList objectAtIndex:s];
        float stretchPercentage = detail.stretchPercent;
        
        // find new width
        int newWidth = (int)round((float)detail.processedSize * stretchPercentage);
        
        // loop through to populate new image
        for (int w = 0; w < detail.processedSize; w++)
        {
            // find image position
            float perc = (float)w/(float)detail.processedSize;
            int thisPos = imagePos + (int)roundf(newWidth*perc);
            
            for (int i=0; i < featureCount; i++)
                newImage[newPos*inHeight+i] = inImage[thisPos*inHeight+i];
            
            // increment position
            newPos++;
        }
        
        // draw a white vertical line
        for (int i=0; i < featureCount; i++)
            newImage[(newPos-1)*inHeight+i] = 1.0f;
        
        // increment position
        imagePos += newWidth;
    }
    
    return newImage;
}

-(NSArray*)getSegmentScores:(float*)inImage1 ofWidth:(int)inWidth andHeight:(int)inHeight withImage:(float*)inImage2 processBands:(BOOL*)processBand test:(BOOL)inTest {
	// work from left to right... find best match for each line within 20% of current position
	// work from current source position
    NSMutableArray *segmentScores = [NSMutableArray arrayWithCapacity:5];
	int destPos = 0;
    int sourcePos = 0;
	float totalDiff = 0.0f;
	float biggestDiff = 0.0f;
	int segmentSize = [VoiceSample getCompareSegmentSize];
    segmentSize = 16;
    while (sourcePos < inWidth) {
        // if this is the last segment, the stretch is determined by the
        // space left in the destination
        float minSegmentDiff = 99999999.0f;
        float minStretchPercent = 0.0f;
        int processedSize = 0;
        if (inWidth - sourcePos < segmentSize*2) // we can't grade part of a segment, it's too small
        {
            float stretch = (float)(inWidth-destPos)/(float)(inWidth-sourcePos);
            printf("force stretch of %f, destpos %d, sourcepos %d\r\n", stretch, destPos, sourcePos);
            float segmentDiff = [self compareSegmentOfImage:inImage1 withImage:inImage2 ofWidth:inWidth andHeight:inHeight image1StartPosition:sourcePos image2StartPosition:destPos segmentSize:inWidth-sourcePos stretchPercentage:stretch processBands:processBand boost:NO];
            minSegmentDiff = segmentDiff;
            minStretchPercent = stretch;
            processedSize = inWidth-sourcePos;
        }
        else {
            // try a segment at a time (16 pixels?) warping the image +/- 50% 
            // to find the best match
//            for (float i = 1.0f; i <= 1.0f; i += 0.05f)
                for (float i = 0.5f; i <= 1.5f; i += 0.05f)
            {
                float segmentDiff = [self compareSegmentOfImage:inImage1 withImage:inImage2 ofWidth:inWidth andHeight:inHeight image1StartPosition:sourcePos image2StartPosition:destPos segmentSize:segmentSize stretchPercentage:i processBands:processBand boost:NO];
                if (segmentDiff < minSegmentDiff)
                {
                    minSegmentDiff = segmentDiff;
                    minStretchPercent = i;
                }
            }
            processedSize = segmentSize;
        }
        printf("Best segment difference is %f, stretch of %f\r\n", minSegmentDiff, minStretchPercent);
        
        // test check, try again with best stretch, but this time boost amplification
        // at each position to match
        float testSegmentDiff = [self compareSegmentOfImage:inImage1 withImage:inImage2 ofWidth:inWidth andHeight:inHeight image1StartPosition:sourcePos image2StartPosition:destPos segmentSize:processedSize stretchPercentage:minStretchPercent processBands:processBand boost:YES];
        float testSegmentDiff1 = [self compareSegmentOfImage:inImage1 withImage:inImage2 ofWidth:inWidth andHeight:inHeight image1StartPosition:sourcePos image2StartPosition:destPos segmentSize:(int)(processedSize/2.0f) stretchPercentage:minStretchPercent processBands:processBand boost:YES];
        float testSegmentDiff2 = [self compareSegmentOfImage:inImage1 withImage:inImage2 ofWidth:inWidth andHeight:inHeight image1StartPosition:sourcePos+(processedSize/2) image2StartPosition:destPos+(int)(((float)processedSize/2.0f)*minStretchPercent) segmentSize:(int)(processedSize/2.0f) stretchPercentage:minStretchPercent processBands:processBand boost:YES];
        minSegmentDiff = MAX(testSegmentDiff1, testSegmentDiff2);
        
        printf("Retest difference is %f, %f, %f, stretch of %f\r\n", testSegmentDiff, testSegmentDiff1, testSegmentDiff2, minStretchPercent);

        // add thi segment score to list
        StretchDetail *detail = [[StretchDetail alloc] initWithScore:minSegmentDiff andStretch:minStretchPercent andProcessedSize:processedSize];
        [segmentScores addObject:detail];
        [detail release];
		
		// save the worst segment score
        if (minSegmentDiff > biggestDiff)
            biggestDiff = minSegmentDiff;
        
		// find the new destination position based on the stretch %
		destPos += (int)((float)segmentSize * minStretchPercent);
        
        // source increments by the same amount
        sourcePos += processedSize;
        // keep track of total diff for fun
		totalDiff += minSegmentDiff;
    }
    
    return segmentScores;
}

-(float)compareSegmentOfImage:(float*)inImage1 withImage:(float*)inImage2 ofWidth:(int)inWidth andHeight:(int)inHeight image1StartPosition:(int)inImage1StartPosition image2StartPosition:(int)inImage2StartPosition segmentSize:(int)inSegmentSize stretchPercentage:(float)inStretchPercentage processBands:(BOOL*)inProcessBand boost:(BOOL)inBoostFlag {
	// create array for mfcc weights
//	float mfccWeight[12];
    
//    printf("stretch %f\r\n", inStretchPercentage);
	
    // calculate HERE which bands to process for just the segments we are comparing!
    
    
    // new width
    int newWidth = (int)((float)inSegmentSize * inStretchPercentage);
    
    int processCount = 0;
    BOOL processBand[256];
    memset(processBand, 0, sizeof(BOOL)*256);
    for (int m=0; m < inHeight; m++) {
        float bandAvg1 = 0.0f;
        float bandMax1 = 0.0f;
        float bandAvg2 = 0.0f;
        float bandMax2 = 0.0f;
        for (int i=0; i < newWidth; i++) {
            // destination position will be actual true pixel
            int destPos = inImage2StartPosition + i;
            
            // source position will be stretched
            int sourcePos = inImage1StartPosition + (int)((float)inSegmentSize * ((float)i / (float)newWidth));
            
            // if it does go off the end, return high score so it gets ignored
            if (sourcePos > inWidth-1 || destPos > inWidth-1)
                return 9999.0f;

            bandAvg1 += inImage1[(sourcePos*inHeight)+m];
            bandAvg2 += inImage2[(destPos*inHeight)+m];
            if (inImage1[(sourcePos*inHeight)+m] > bandMax1)
                bandMax1 = inImage1[(sourcePos*inHeight)+m];
            if (inImage2[(destPos*inHeight)+m] > bandMax2)
                bandMax2 = inImage2[(destPos*inHeight)+m];
        }
        bandAvg1 /= (float)inWidth;
        bandAvg2 /= (float)inWidth;
        if (bandMax1 > 0.1f || bandMax2 > 0.1f) {
            processBand[m] = YES;
            processCount++;
        }
        else {
//            printf("don't process band %d\r\n", m);
            processBand[m] = NO;
        }
    }    
    
	// loop through mfcc's
	float totalDiff = 0.0f;
	int totalCount = 0;

    // loop through width
    for (int w = 0; w < newWidth; w++)
    {
        // destination position will be actual true pixel
        int destPos = inImage2StartPosition + w;
        
        // source position will be stretched
        int sourcePos = inImage1StartPosition + (int)((float)inSegmentSize * ((float)w / (float)newWidth));
        
        // if it does go off the end, return high score so it gets ignored
        if (sourcePos > inWidth-1 || destPos > inWidth-1)
            return 9999.0f;
        /*
        if (sourcePos > inWidth-1)
            sourcePos = inWidth-1;
        if (destPos > inWidth-1)
            destPos = inWidth-1;
        */
        /*
        // use dtw for features
        float cost = [self compareImage3_1:&inImage1[(sourcePos*inHeight)] ofWidth:featureCount withImage:&inImage2[(destPos*inHeight)]];
        totalDiff += cost;
        totalCount += featureCount;
*/

        // try and make the same amplitude
        float newImage1[64];
        float newImage2[64];
        float image1Total = 0.0f;
        float image2Total = 0.0f;
        for (int i=0; i < featureCount; i++) {
            image1Total += inImage1[(sourcePos*inHeight)+i];
            image2Total += inImage2[(destPos*inHeight)+i];
        }
        float boost = 0.0f;
        if (image2Total > 0.0f)
            boost = image1Total/image2Total;
        else
            boost = 1.0f;
        if (boost < 0.2f)
            boost = 0.2f;
        if (boost > 1.8f)
            boost = 1.8f;
        for (int i=0; i < featureCount; i++) {
            newImage1[i] = inImage1[(sourcePos*inHeight)+i];
            newImage2[i] = inImage2[(destPos*inHeight)+i] * boost;
        }
/*        
        // make dimmest one brighter to get best contrast
        float boost = 0;
        if (image1Total > image2Total)
            boost = image1Total/image2Total;
        else
            boost = image2Total/image1Total;
        if (boost < 0.5f)
            boost = 0.5f;
        if (boost > 1.5f)
            boost = 1.5f;
        for (int i=0; i < featureCount; i++) {
            if (image1Total > image2Total) {
                newImage1[i] = inImage1[(sourcePos*inHeight)+i];
                newImage2[i] = inImage2[(destPos*inHeight)+i] * boost;
            }
            else {
                newImage1[i] = inImage1[(sourcePos*inHeight)+i] * boost;
                newImage2[i] = inImage2[(destPos*inHeight)+i];
            }
        }
        */
        float thisDiff = 0.0f;
        for (int i=0; i < featureCount; i++) {
            // process this band?
            if (processBand[i]) {
                // compare this cell
                // get source and destination values
                float x = inImage1[(sourcePos*inHeight)+i];
                float y = inImage2[(destPos*inHeight)+i];
                if (inBoostFlag) {
                    x = newImage1[i];
                    y = newImage2[i];
                }
                
                // find difference
                float difference = 0.0f;
                if (x > y)
                    difference = x - y;
                else
                    difference = y - x;
                
//                printf("%f,", difference);
                
                // compare this column
                totalDiff += difference;
                thisDiff += difference;
                totalCount++;
            }
		}
//        printf("\r\n");
        
//        float cov = [self covarianceOf:&inImage1[(sourcePos*inHeight)] with:&inImage2[(destPos*inHeight)] length:featureCount];
        /*
        cov = 2.0f-(cov+1.0f);
        totalDiff += cov;
        totalCount++;
        */
         
        // large numbers = same
        // small numbers = different
        
//        printf("column %d difference is %f, cov is %f\r\n", x, thisDiff, cov);
	}
    
	return totalDiff/(float)totalCount;
}

// use correlation formula
-(float)compareSegmentOfImage2:(float*)inImage1 withImage:(float*)inImage2 ofWidth:(int)inWidth andHeight:(int)inHeight image1StartPosition:(int)inImage1StartPosition image2StartPosition:(int)inImage2StartPosition segmentSize:(int)inSegmentSize stretchPercentage:(float)inStretchPercentage {
	/*
	// calculate cepstral mean
	for (int i=0; i < mfccCount; i++) {
		float cepstralMean = 0;
		for (int s=0; s < quantizeCount; s++)
			cepstralMean += imageBuffer[(s*mfccCount)+i];
		cepstralMean /= quantizeCount;
		for (int s=0; s < quantizeCount; s++)
			imageBuffer[(s*mfccCount)+i] -= cepstralMean;
	}
	*/
	
	// create weights for importants of mfcc's
	float mfccWeights[12];
	mfccWeights[0] = 0.8f;
	mfccWeights[1] = 1.0f;
	mfccWeights[2] = 1.0f;
	mfccWeights[3] = 1.0f;
	mfccWeights[4] = 0.6f;
	mfccWeights[5] = 0.6f;
	
	// loop through width
	float totalCorr = 0.0f;
//	float sumXy = 0.0f, sumXx = 0.0f, sumYy = 0.0f, sumX = 0.0f, sumY = 0.0f;
//	int N = 0;
	for (int h=0; h < featureCount; h++) {
		int newWidth = (int)((float)inSegmentSize * inStretchPercentage);
		float totalDiff = 0.0f;
		float sumXy = 0.0f, sumXx = 0.0f, sumYy = 0.0f, sumX = 0.0f, sumY = 0.0f;
		int N = 0;
		for (int x = 0; x < newWidth; x++)
		{
			// destination position will be actual true pixel
			int destPos = inImage2StartPosition + x;
			
			// source position will be stretched
			int sourcePos = inImage1StartPosition + (int)((float)inSegmentSize * ((float)x / (float)newWidth));
			
			// get source and destination values
			float x = inImage1[(sourcePos*inHeight)+h];
			float y = inImage2[(destPos*inHeight)+h];
//			float x = inImage1[(sourcePos*inHeight)+h] - cepstralMeanSource;
//			float y = inImage2[(destPos*inHeight)+h] - cepstralMeanDest;
			
			// calculate totals
			sumXy += x*y;
			sumXx += x*x;
			sumYy += y*y;
			sumX += x;
			sumY += y;
			
			// compare this column
			totalDiff += [self compareColumn:sourcePos OfImage:inImage1 ofWidth:inWidth andHeight:inHeight withImage:inImage2 column:destPos];
			N++;
		}
		
		// use weighting function for importance of mfcc's
		
		// calculate correlation for this mfcc
		float corr = (N*sumXy - sumX*sumY) / sqrt((N*sumXx - sumX*sumX) * (N*sumYy - sumY*sumY));
		if (corr < 0.0f)
			corr = 0.0f;
//		corr *= (1.0f + (1.0f-mfccWeights[h]));
		totalCorr += corr;
	}
	// convert to between 0.0f and 1.0f, then change from likeness to difference
	float result = totalCorr/(float)featureCount;
//	float result = corr;
	result = result < 0.0f ? 0.0f : result;
	result = 1.0f - result;
	return result;
//	return totalDiff/(float)totalCount;
}

// old formula using columns
-(float)compareSegmentOfImage3:(float*)inImage1 withImage:(float*)inImage2 ofWidth:(int)inWidth andHeight:(int)inHeight image1StartPosition:(int)inImage1StartPosition image2StartPosition:(int)inImage2StartPosition segmentSize:(int)inSegmentSize stretchPercentage:(float)inStretchPercentage {
	float totalDiff = 0.0f;
	float totalCount = 0.0f;
	int newWidth = (int)((float)inSegmentSize * inStretchPercentage);
	for (int x = 0; x < newWidth; x++)
	{
		// destination position will be actual true pixel
		int destPos = inImage2StartPosition + x;
		
		// source position will be stretched
		int sourcePos = inImage1StartPosition + (int)((float)inSegmentSize * ((float)x / (float)newWidth));
				
		// compare this column
		totalDiff += [self compareColumn:sourcePos OfImage:inImage1 ofWidth:inWidth andHeight:inHeight withImage:inImage2 column:destPos];
		totalCount += 1.0f;
	}
	return totalDiff/(float)totalCount;
}

-(float)compareColumn:(int)inImage1Column OfImage:(float*)inImage1 ofWidth:(int)inWidth andHeight:(int)inHeight withImage:(float*)inImage2 column:(int)inImage2Column {
	// loop through column pixels
	float columnDiff = 0.0f;
//	for (int y = 1; y < inHeight; y++)
	for (int y = 0; y < featureCount; y++)
	{
		// only check where there is data
		float threshold = 0.0f;
		float pixel1;
		float pixel2;
		// if we go off edge, use last pixel
		if (inImage1Column < inWidth)
			pixel1 = inImage1[(inImage1Column*inHeight)+y];
		else
			pixel1 = inImage1[((inWidth-1)*inHeight)+y];
		if (inImage2Column < inWidth)
			pixel2 = inImage2[(inImage2Column*inHeight)+y];
		else
			pixel2 = inImage2[((inWidth-1)*inHeight)+y];
 		if (inImage1[(inImage1Column*inHeight)+y] > threshold || inImage2[(inImage2Column*inHeight)+y] > threshold)
		{
			// calculate difference ^2
			float difference = 0.0f;
			if (pixel1 > pixel2)
				difference = pixel1 - pixel2;
			else
				difference = pixel2 - pixel1;
//			difference = (float)powf(difference * 100.0f, 2.0f);
			columnDiff += difference;
		}
	}
	return columnDiff;
}
/*
-(void)stretchAndCropImage:(float*)fromImage toImage:(float*)toImage fromOldHeight:(int)oldHeight toNewHeight:(int)newHeight {
	// stretch
	for (int x=0; x < 128; x++) {
		for (int y=0; y < oldHeight; y++) {
			float ratio = (float)oldHeight/(float)newHeight;
			int newY = (int)((float)y*ratio);
			toImage[(x*oldHeight)+y] = fromImage[(x*oldHeight)+newY];
		}
	}
}
*/

-(float*)getFeatureBuffer {
	return featureBuffer;
}

-(float*)getMelBuffer {
	return melBuffer;
}

-(float*)getMfccBuffer {
	return mfccBuffer;
}

-(int)getDataLength {
	return dataLength;
}

-(void)dealloc {
	// free feature buffers
    free(melBuffer);
	free(mfccBuffer);
	free(deltaBuffer);
	free(energy);
	free(energyDelta);
	free(covBuffer);
	free(spectrumBuffer);
	
	// free data buffers
	free(featureBuffer);
	free(sampleBuffer);
	
	[super dealloc];
}
@end
