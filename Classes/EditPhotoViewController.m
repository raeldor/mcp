    //
//  EditPhotoViewController.m
//  Mcp
//
//  Created by Ray on 10/22/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "EditPhotoViewController.h"


@implementation EditPhotoViewController

@synthesize originalPhoto;
@synthesize photoImageView;

-(void)setPhoto:(UIImage*)inPhoto {
	// save original photo and set as photo in ui control
	self.originalPhoto = inPhoto;
}

-(void)touchesEnded:(NSSet*)touches withEvent:(UIEvent*)event {
    /*
	// find when touch up took place
	UITouch *myTouch = [touches anyObject];
	CGPoint touchPoint = [myTouch locationInView:photoImageView];
	
	// did it take place in image view?
	CGSize frameSize = photoImageView.frame.size;
	CGSize imageSize = photoImageView.image.size;
	if (touchPoint.x >=0 && touchPoint.y >=0 && touchPoint.x < frameSize.width && touchPoint.y < frameSize.height) {
		// translate the touch point to a pixel point on the image
		CGPoint pixelPoint = CGPointMake(touchPoint.x*(frameSize.width/imageSize.width), touchPoint.y*(frameSize.height/imageSize.height));
		
		// loop through image and remove all pixels this color
		for (int y=0; y < originalPhoto.size.height; y++) {
			for (int x=0; x < originalPhoto.size.width; x++) {
			}
		}
	}
    */
}

/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
        // Custom initialization
    }
    return self;
}
*/

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
}
*/

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	
	// set title
	self.title = @"Edit Photo";
	
	// create cancel button
	UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(cancelButtonClick:)];
	self.navigationItem.leftBarButtonItem = cancelButton;
	[cancelButton release];
	
	// create save button
	UIBarButtonItem *saveButton = [[UIBarButtonItem alloc] initWithTitle:@"Save" style:UIBarButtonItemStyleBordered target:self action:@selector(saveButtonClick:)];
	self.navigationItem.rightBarButtonItem = saveButton;
	[saveButton release];
	
	// set control values here
	photoImageView.image = originalPhoto;
}

-(void)cancelButtonClick:(id)sender {
	// pop controller
	UINavigationController *parent = (UINavigationController*)self.parentViewController;
	[parent popViewControllerAnimated:TRUE];
}

-(void)saveButtonClick:(id)sender {
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
	[originalPhoto release];
	[photoImageView release];
    [super dealloc];
}


@end
