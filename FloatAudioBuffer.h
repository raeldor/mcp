//
//  FloatAudioBuffer.h
//  Mcp
//
//  Created by Ray Price on 3/24/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FloatAudioBuffer : NSObject <NSCopying, NSCoding> {
    float *buffer;
    int bufferSize;
    int sampleCount;
    int sampleRate;
}

-(id)initWithSampleCount:(int)inSampleCount andSampleRate:(int)inSampleRate;
-(id)initFromFile:(NSString*)inFilePath;
-(void)writeToWavFile:(NSString*)inFileNameOnly;
-(void)normalize;
-(void)truncateToSampleCount:(int)inSampleCount;
-(void)trimSilenceUsingOverrideNoiseLevel:(float)inOverrideNoiseLevel doTrimMiddle:(BOOL)inTrimMiddle;
-(NSArray*)getSilenceArrayUsingOverrideNoiseLevel:(float)inNoiseLevel andLeadInto:(float*)outLead andTailInto:(float*)outTail;
-(void)runHighPassFilter;
-(void)downsample;

+(void)normalizeBufferAtZero:(float*)inArray ofSampleCount:(int)inSampleCount;
+(void)normalizePositiveBuffer:(float*)inBuffer ofSize:(int)inSize;

@property (nonatomic, readonly) float *buffer;
@property (nonatomic, readonly) int bufferSize;
@property (nonatomic, readonly) int sampleCount;
@property (nonatomic, readonly) int sampleRate;

@end
