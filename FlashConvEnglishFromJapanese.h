//
//  FlashConvEnglishFromJapanese.h
//  Mcp
//
//  Created by Ray Price on 1/27/13.
//
//

#import <Foundation/Foundation.h>
#import "FlashConvProtocol.h"

@interface FlashConvEnglishFromJapanese : NSObject <FlashConvProtocol>

-(FlashConvBlock*)getClassIntroConversationBlockFor:(FlashGame*)inGame;
-(FlashConvBlock*)getGameIntroConversationBlockFor:(FlashGame*)inGame;
-(FlashConvBlock*)getQuestionConversationBlockFor:(FlashGame*)inGame;
-(FlashConvBlock*)getAnswerConversationBlockFor:(FlashGame*)inGame;
-(FlashConvBlock*)getCongratsConversationBlockFor:(FlashGame*)inGame;
-(FlashConvBlock*)getRegretsConversationBlockFor:(FlashGame*)inGame;

@end
