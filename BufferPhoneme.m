//
//  BufferPhoneme.m
//  Mcp
//
//  Created by Ray Price on 4/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BufferPhoneme.h"

@implementation BufferPhoneme

@synthesize length;
@synthesize phoneme;

-(id)initWithLength:(int)inLength andPhoneme:(NSString*)inPhoneme {
    // init and assign
    if ((self = [super init])) {
        length = inLength;
        self.phoneme = inPhoneme;
    }
    return self;
}

-(void)dealloc {
    // release memory
    [phoneme release];
    [super dealloc];
}

@end
