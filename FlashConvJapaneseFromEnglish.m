//
//  FlashConvJapaneseFromEnglish.m
//  Mcp
//
//  Created by Ray Price on 1/27/13.
//
//

#import "FlashConvJapaneseFromEnglish.h"
#import "HtmlFunctions.h"
#import "McpAppDelegate.h"
#import "FlashConvAnswer.h"
#import "FlashConvLine.h"

@implementation FlashConvJapaneseFromEnglish

-(FlashConvBlock*)getBlockForName:(NSString*)inBlockName usingGame:(FlashGame*)inGame isMuted:(BOOL)inIsMuted {
    // return correct block
    if ([inBlockName isEqualToString:@"ClassIntro"])
        return [self getClassIntroConversationBlockFor:inGame];
    if ([inBlockName isEqualToString:@"GameIntro"])
        return [self getGameIntroConversationBlockFor:inGame];
    if ([inBlockName isEqualToString:@"Question"])
        return [self getQuestionConversationBlockFor:inGame];
    if ([inBlockName isEqualToString:@"Answer"])
        return [self getAnswerConversationBlockFor:inGame];
    if ([inBlockName isEqualToString:@"Congrats"])
        return [self getCongratsConversationBlockFor:inGame];
    if ([inBlockName isEqualToString:@"Regrets"])
        return [self getRegretsConversationBlockFor:inGame];
    
    // no block found
    return nil;
}

-(FlashConvBlock*)getClassIntroConversationBlockFor:(FlashGame*)inGame {
    FlashConvBlock *returnBlock = [[FlashConvBlock alloc] initWithBlockName:@"ClassIntro" nextBlockName:@"GameIntro" goToNextCard:NO];
    
    // format a date string
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"yyyy年MM月dd日"];
    NSDate *now = [[NSDate alloc] init];
    NSString *dateString = [format stringFromDate:now];
    [now release];
    [format release];
    
    // create first line
    FlashConvLine *firstLine = [[FlashConvLine alloc] init];
    [firstLine.segmentArray addObject:CONVSTRING_WELCOME];
    [returnBlock.lines addObject:firstLine];
    [firstLine release];
    
    return [returnBlock autorelease];
}

-(FlashConvBlock*)getGameIntroConversationBlockFor:(FlashGame*)inGame {
    FlashConvBlock *returnBlock = [[FlashConvBlock alloc] initWithBlockName:@"GameIntro" nextBlockName:@"Question" goToNextCard:NO];
    
    // create first line
    FlashConvLine *firstLine = [[FlashConvLine alloc] init];
    [firstLine.segmentArray addObject:CONVSTRING_GUESSJAPANESE];
    [returnBlock.lines addObject:firstLine];
    [firstLine release];
    
    // say % completion
    FlashConvLine *secondLine = [[FlashConvLine alloc] init];
    [secondLine.segmentArray addObject:[NSString stringWithFormat:@"今まで、%3.0f%%完成しました。", [inGame getPercentComplete]]];
    
    // let's go!
    [secondLine.segmentArray addObject:CONVSTRING_LETSGO];
    [returnBlock.lines addObject:secondLine];
    [secondLine release];
    
    /*
    // let's go!
    FlashConvLine *secondLine = [[FlashConvLine alloc] init];
    [secondLine.segmentArray addObject:CONVSTRING_LETSGO];
    [returnBlock.lines addObject:secondLine];
    [secondLine release];*/
    
    return [returnBlock autorelease];
}

-(FlashConvBlock*)getQuestionConversationBlockFor:(FlashGame*)inGame {
    FlashConvBlock *returnBlock = [[FlashConvBlock alloc] initWithBlockName:@"Question" nextBlockName:nil goToNextCard:NO];
    
    // add continue button
    [returnBlock.answers addObject:[[[FlashConvAnswer alloc] initWithButtonText:@"Continue" jumpToBlockName:@"Answer" isRightAnswer:NO isWrongAnswer:NO] autorelease]];
    
	// get application delegate
    McpAppDelegate *appDelegate = (McpAppDelegate*)[[UIApplication sharedApplication] delegate];
    
    // create first line
    FlashConvLine *firstLine = [[FlashConvLine alloc] init];
    
    // say word in english
    [firstLine.segmentArray addObject:[NSString stringWithFormat:@"「%@」。", [inGame getQuestionUsingSeparator:@", "]]];
    
    // say word type
    [firstLine.segmentArray addObject:[NSString stringWithFormat:@"%@。", [inGame getWordType]]];
    
    // add notes
    if (![[inGame getNotes] isEqualToString:@""] && ![[inGame getNotes] isEqualToString:@"word usually written using kana alone"])
        [firstLine.segmentArray addObject:[NSString stringWithFormat:@"%@。", [inGame getNotes]]];
    
    // say sample sentence
    SampleEntry *thisSample = [inGame getSampleEntryForJlptLevel:appDelegate.localOptions.jlptLevel pickType:appDelegate.localOptions.pickSentenceType];
    if (thisSample == nil) {
        [firstLine.segmentArray addObject:@"No example found。"];
    }
    else
        [firstLine.segmentArray addObject:[NSString stringWithFormat:@"For example \"%@\"。", thisSample.meaning] ];
    
    // ask question
    [firstLine.segmentArray addObject:@"日本語で何と言いますか。"];
    
    // add first line to block
    [returnBlock.lines addObject:firstLine];
    [firstLine release];
    
    return [returnBlock autorelease];
}

-(FlashConvBlock*)getAnswerConversationBlockFor:(FlashGame*)inGame {
    FlashConvBlock *returnBlock = [[FlashConvBlock alloc] initWithBlockName:@"Answer" nextBlockName:nil goToNextCard:NO];
    
    // add answers
    [returnBlock.answers addObject:[[[FlashConvAnswer alloc] initWithButtonText:@"はい" jumpToBlockName:@"Congrats" isRightAnswer:YES isWrongAnswer:NO] autorelease]];
    [returnBlock.answers addObject:[[[FlashConvAnswer alloc] initWithButtonText:@"いいえ" jumpToBlockName:@"Regrets" isRightAnswer:NO isWrongAnswer:YES] autorelease]];
    
	// get application delegate
    McpAppDelegate *appDelegate = (McpAppDelegate*)[[UIApplication sharedApplication] delegate];
    
    // create first line
    FlashConvLine *firstLine = [[FlashConvLine alloc] init];
    
    // say question again for confirmation
    [firstLine.segmentArray addObject:[NSString stringWithFormat:@"「%@」", [inGame getQuestionUsingSeparator:@", "]]];
    
    // say answer
    if ([[inGame getNotes] isEqualToString:@"word usually written using kana alone"])
        [firstLine.segmentArray addObject:[NSString stringWithFormat:@"日本語では「%@」です。", [inGame getKanaUsingSeparator:@", "]]];
    else {
        NSString *rubyText = [HtmlFunctions getRubyForKanji:[inGame getKanjiUsingSeparator:@", "] andKana:[inGame getKanaUsingSeparator:@", "]];
        [firstLine.segmentArray addObject:[NSString stringWithFormat:@"日本語では「<vtml_sub alias=\"%@\">%@</vtml_sub>」です。", [inGame getKanaUsingSeparator:@", "], rubyText]];
    }

    // add first line to block
    [returnBlock.lines addObject:firstLine];
    [firstLine release];
        
    // add did u get it right?
    FlashConvLine *fifthLine = [[FlashConvLine alloc] init];
    [fifthLine.segmentArray addObject:CONVSTRING_WEREYOURIGHT];
    [returnBlock.lines addObject:fifthLine];
    [fifthLine release];
    
    return [returnBlock autorelease];
}

-(FlashConvBlock*)getCongratsConversationBlockFor:(FlashGame*)inGame {
    FlashConvBlock *returnBlock = [[FlashConvBlock alloc] initWithBlockName:@"Congrats" nextBlockName:@"Question" goToNextCard:YES];
    
    // create first line
    FlashConvLine *firstLine = [[FlashConvLine alloc] init];
    [firstLine.segmentArray addObject:CONVSTRING_CONGRATULATIONS];
    [returnBlock.lines addObject:firstLine];
    [firstLine release];
    
    return [returnBlock autorelease];
}

-(FlashConvBlock*)getRegretsConversationBlockFor:(FlashGame*)inGame {
    FlashConvBlock *returnBlock = [[FlashConvBlock alloc] initWithBlockName:@"Regrets" nextBlockName:@"Question" goToNextCard:YES];
    
    // create first line
    FlashConvLine *firstLine = [[FlashConvLine alloc] init];
    
    // say it's ok
    [firstLine.segmentArray addObject:CONVSTRING_ITSOK];
    [firstLine.segmentArray addObject:@"よく覚えてください。"];
    
    // add first line
    [returnBlock.lines addObject:firstLine];
    [firstLine release];
    
    // create second line
    FlashConvLine *secondLine = [[FlashConvLine alloc] init];
    
    // say answer
    if ([[inGame getNotes] isEqualToString:@"word usually written using kana alone"]) {
        [secondLine.segmentArray addObject:[NSString stringWithFormat:@"「%@」。", [inGame getKanaUsingSeparator:@", "]]];
        [secondLine.segmentArray addObject:[NSString stringWithFormat:@"「%@」", [inGame getQuestionUsingSeparator:@", "]]];
        [secondLine.segmentArray addObject:[NSString stringWithFormat:@"「%@」。", [inGame getKanaUsingSeparator:@", "]]];
    }
    else {
        NSString *rubyText = [HtmlFunctions getRubyForKanji:[inGame getKanjiUsingSeparator:@", "] andKana:[inGame getKanaUsingSeparator:@", "]];
        [secondLine.segmentArray addObject:[NSString stringWithFormat:@"「<vtml_sub alias=\"%@\">%@</vtml_sub>」。", [inGame getKanaUsingSeparator:@", "], rubyText]];
        [secondLine.segmentArray addObject:[NSString stringWithFormat:@"「%@」", [inGame getQuestionUsingSeparator:@", "]]];
        [secondLine.segmentArray addObject:[NSString stringWithFormat:@"「<vtml_sub alias=\"%@\">%@</vtml_sub>」。", [inGame getKanaUsingSeparator:@", "], rubyText]];
    }
    [secondLine.segmentArray addObject:@"例えば。"];
    secondLine.pauseAfterEachSegment = YES;
    
    // add first line
    [returnBlock.lines addObject:secondLine];
    [secondLine release];
    
	// get application delegate
    McpAppDelegate *appDelegate = (McpAppDelegate*)[[UIApplication sharedApplication] delegate];

    // create second line
    FlashConvLine *thirdLine = [[FlashConvLine alloc] init];
    
    // show sample sentence with ruby
    SampleEntry *thisSample = [inGame getSampleEntryForJlptLevel:appDelegate.localOptions.jlptLevel pickType:appDelegate.localOptions.pickSentenceType];
    if (thisSample != nil) {
        // say as segments in brackets
        NSMutableArray *segments = [NSMutableArray arrayWithArray:[thisSample getKanjiArrayWithRuby:YES breakUp:YES includeLinks:NO blankWordIndex:-1]];
        [segments setObject:[NSString stringWithFormat:@"「%@", [segments objectAtIndex:0]] atIndexedSubscript:0];
        [segments setObject:[NSString stringWithFormat:@"%@」", [segments objectAtIndex:segments.count-1]] atIndexedSubscript:segments.count-1];
        [thirdLine.segmentArray addObjectsFromArray:segments];
    }
    thirdLine.pauseAfterEachSegment = YES;
    
    // add second line
    [returnBlock.lines addObject:thirdLine];
    
    // add second line again
    [returnBlock.lines addObject:thirdLine];
    [thirdLine release];
    
    return [returnBlock autorelease];
}

@end
