//
//  FlashConvAutopilot.m
//  Mcp
//
//  Created by Ray Price on 1/28/13.
//
//

#import "FlashConvAutopilot.h"
#import "HtmlFunctions.h"
#import "McpAppDelegate.h"
#import "FlashConvLine.h"

@implementation FlashConvAutopilot

-(FlashConvBlock*)getBlockForName:(NSString*)inBlockName usingGame:(FlashGame*)inGame isMuted:(BOOL)inIsMuted {
    // return correct block
    if ([inBlockName isEqualToString:@"ClassIntro"])
        return [self getClassIntroConversationBlockFor:inGame];
    if ([inBlockName isEqualToString:@"GameIntro"])
        return [self getGameIntroConversationBlockFor:inGame];
    if ([inBlockName isEqualToString:@"Question"])
        return [self getQuestionConversationBlockFor:inGame];
    if ([inBlockName isEqualToString:@"Answer"])
        return [self getAnswerConversationBlockFor:inGame];
    
    // no block found
    return nil;
}

-(FlashConvBlock*)getClassIntroConversationBlockFor:(FlashGame*)inGame {
    FlashConvBlock *returnBlock = [[FlashConvBlock alloc] initWithBlockName:@"ClassIntro" nextBlockName:@"GameIntro" goToNextCard:NO];
    
    // format a date string
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"yyyy年MM月dd日"];
    NSDate *now = [[NSDate alloc] init];
    NSString *dateString = [format stringFromDate:now];
    [now release];
    [format release];
    
    // create first line
    FlashConvLine *firstLine = [[FlashConvLine alloc] init];
    [firstLine.segmentArray addObject:@"クラスにようこそ。"];
    [returnBlock.lines addObject:firstLine];
    [firstLine release];
    
    return [returnBlock autorelease];
}

-(FlashConvBlock*)getGameIntroConversationBlockFor:(FlashGame*)inGame {
    FlashConvBlock *returnBlock = [[FlashConvBlock alloc] initWithBlockName:@"GameIntro" nextBlockName:@"Question" goToNextCard:NO];
    
    // create first line
    FlashConvLine *firstLine = [[FlashConvLine alloc] init];
    [firstLine.segmentArray addObject:@"このゲームで、聞いてだけください。"];
    [returnBlock.lines addObject:firstLine];
    [firstLine release];
    
    // let's go!
    FlashConvLine *secondLine = [[FlashConvLine alloc] init];
    [secondLine.segmentArray addObject:@"行きましょう！"];
    [returnBlock.lines addObject:secondLine];
    [secondLine release];
    
    return [returnBlock autorelease];
}

-(FlashConvBlock*)getQuestionConversationBlockFor:(FlashGame*)inGame {
    FlashConvBlock *returnBlock = [[FlashConvBlock alloc] initWithBlockName:@"Question" nextBlockName:@"Answer" goToNextCard:NO];
    
	// get application delegate
    McpAppDelegate *appDelegate = (McpAppDelegate*)[[UIApplication sharedApplication] delegate];
    
    // create first line
    FlashConvLine *firstLine = [[FlashConvLine alloc] init];
    
    // say word in english
    [firstLine.segmentArray addObject:[NSString stringWithFormat:@"「%@」。", [inGame getQuestionUsingSeparator:@", "]]];
    
    // say word type
    [firstLine.segmentArray addObject:[NSString stringWithFormat:@"%@。", [inGame getWordType]]];
    
    // add notes
    if (![[inGame getNotes] isEqualToString:@""] && ![[inGame getNotes] isEqualToString:@"word usually written using kana alone"])
        [firstLine.segmentArray addObject:[NSString stringWithFormat:@"%@。", [inGame getNotes]]];
    
    // say sample sentence
    SampleEntry *thisSample = [inGame getSampleEntryForJlptLevel:appDelegate.localOptions.jlptLevel pickType:appDelegate.localOptions.pickSentenceType];
    if (thisSample == nil) {
        [firstLine.segmentArray addObject:@"No example found。"];
    }
    else
        [firstLine.segmentArray addObject:[NSString stringWithFormat:@"For example \"%@\"。", thisSample.meaning] ];
    
    // add first line to block
    [returnBlock.lines addObject:firstLine];
    [firstLine release];
    
    // create second line
    FlashConvLine *secondLine = [[FlashConvLine alloc] init];
    
    // say answer
    if ([[inGame getNotes] isEqualToString:@"word usually written using kana alone"])
        [secondLine.segmentArray addObject:[NSString stringWithFormat:@"日本語では「%@」です。", [inGame getKanaUsingSeparator:@", "]]];
    else {
        NSString *rubyText = [HtmlFunctions getRubyForKanji:[inGame getKanjiUsingSeparator:@", "] andKana:[inGame getKanaUsingSeparator:@", "]];
        [secondLine.segmentArray addObject:[NSString stringWithFormat:@"日本語では「<vtml_sub alias=\"%@\">%@</vtml_sub>」です。", [inGame getKanaUsingSeparator:@", "], rubyText]];
    }
    
    // add first line to block
    [returnBlock.lines addObject:secondLine];
    [secondLine release];
    
    return [returnBlock autorelease];
}

-(FlashConvBlock*)getAnswerConversationBlockFor:(FlashGame*)inGame {
    FlashConvBlock *returnBlock = [[FlashConvBlock alloc] initWithBlockName:@"Answer" nextBlockName:@"Question" goToNextCard:YES];
    
	// get application delegate
    McpAppDelegate *appDelegate = (McpAppDelegate*)[[UIApplication sharedApplication] delegate];
    
    // create second line
    FlashConvLine *secondLine = [[FlashConvLine alloc] init];
    
    // add example sentence pronunciation
    SampleEntry *thisSample = [inGame getSampleEntryForJlptLevel:appDelegate.localOptions.jlptLevel pickType:appDelegate.localOptions.pickSentenceType];
    if (thisSample == nil) {
        // say couldn't find line
        [secondLine.segmentArray addObject:@"例文を見つけませんでした。"];
        [returnBlock.lines addObject:secondLine];
        [secondLine release];
    }
    else {
        // show sample sentence with ruby
        [secondLine.segmentArray addObject:[NSString stringWithFormat:@"「%@」", [thisSample getKanjiWithRuby:YES includeLinks:NO]]];
        [returnBlock.lines addObject:secondLine];
        [secondLine release];
        
        // add sentence meaning
        FlashConvLine *thirdLine = [[FlashConvLine alloc] init];
        [thirdLine.segmentArray addObject:[NSString stringWithFormat:@"「%@」", thisSample.meaning]];
        [returnBlock.lines addObject:thirdLine];
        [thirdLine release];
        
        // show sample sentence with ruby again
        FlashConvLine *fourthLine = [[FlashConvLine alloc] init];
        [fourthLine.segmentArray addObject:[NSString stringWithFormat:@"「%@」", [thisSample getKanjiWithRuby:YES includeLinks:NO]]];
        [returnBlock.lines addObject:fourthLine];
        [fourthLine release];
    }
    
    return [returnBlock autorelease];
}

@end
