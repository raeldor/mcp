//
//  FlashConvGame.h
//  Mcp - Wrapper for flashgame that turns it into a conversation
//
//  Created by Ray Price on 3/24/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ConvGameProtocol.h"
#import "FlashGame.h"
#import "Actor.h"
#import "SampleEntry.h"
#import "FlashConvProtocol.h"

// forward declaration
@class ConverseViewController;

@interface FlashConvGame : NSObject<ConvGameProtocol,FlashConvProtocol> {
    FlashGame *myFlashGame;
    Actor *myActor;
    NSString *currentBlockName;
    int currentLineIndex;
    int currentSegmentIndex;
    int lastSelectedAnswerIndex;
    bool changeGameRequested;
    bool oneMoreTimeRequested;
    SampleEntry *currentSample;
    id<FlashConvProtocol> conversation;
    ConverseViewController *converseController;
}

@property (nonatomic, assign) int currentSegmentIndex;

-(int)getCommandIndexUsingBuffer:(void*)inBuffer ofSize:(int)inBufferSize andSampleRate:(int)inSampleRate fromCommandList:(NSArray*)inCommandList;

-(FlashConvBlock*)getConversationBlock;

@end

