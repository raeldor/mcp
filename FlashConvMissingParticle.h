//
//  FlashConvMissingWord.h
//  Mcp
//
//  Created by Ray Price on 1/27/13.
//
//

#import <Foundation/Foundation.h>
#import "FlashConvProtocol.h"

@interface FlashConvMissingParticle : NSObject <FlashConvProtocol> {
    int missingParticleIndex;
}

-(FlashConvBlock*)getClassIntroConversationBlockFor:(FlashGame*)inGame;
-(FlashConvBlock*)getGameIntroConversationBlockFor:(FlashGame*)inGame;
-(FlashConvBlock*)getQuestionConversationBlockFor:(FlashGame*)inGame isMuted:(BOOL)inIsMuted;
-(FlashConvBlock*)getAnswerConversationBlockFor:(FlashGame*)inGame;
-(FlashConvBlock*)getCongratsConversationBlockFor:(FlashGame*)inGame;
-(FlashConvBlock*)getRegretsConversationBlockFor:(FlashGame*)inGame isMuted:(BOOL)inIsMuted;
-(FlashConvBlock*)getRepeat1ConversationBlockFor:(FlashGame*)inGame isMuted:(BOOL)inIsMuted;
-(FlashConvBlock*)getRepeat2ConversationBlockFor:(FlashGame*)inGame isMuted:(BOOL)inIsMuted;
-(FlashConvBlock*)getRepeat3ConversationBlockFor:(FlashGame*)inGame isMuted:(BOOL)inIsMuted;

@end
