//
//  GrammarPickerViewController.m
//  Mcp
//
//  Created by Ray Price on 10/19/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "GrammarPickerViewController.h"
#import "McpAppDelegate.h"

@implementation GrammarPickerViewController

@synthesize gpToolbar;
@synthesize gpPicker;
@synthesize selectDelegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return [[chapter.grammarNotes objectAtIndex:row] title];;
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return chapter.grammarNotes.count;
}

-(void)setActionSheet:(UIActionSheet*)inActionSheet {
    actionSheet = [inActionSheet retain];
}

-(void)setChapterAs:(Chapter*)inChapter {
    chapter = [inChapter retain];
}

-(IBAction)cancelButtonPressed:(id)sender {
    // call cancel delegate
    [selectDelegate didCancelSelection];
    
    // dismiss action sheet
    [actionSheet dismissWithClickedButtonIndex:0 animated:YES];
}

-(IBAction)selectButtonPressed:(id)sender {
    // call select delegate
    [selectDelegate didMakeSelection:[gpPicker selectedRowInComponent:0]];
    
    // dismiss action sheet
    [actionSheet dismissWithClickedButtonIndex:0 animated:YES];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)dealloc {
    self.selectDelegate = nil;

    [chapter release];
	[actionSheet release];
    [super dealloc];
}

@end
