//
//  McpAppOptions.m
//  Mcp
//
//  Created by Ray Price on 12/4/12.
//
//

#import "McpAppOptions.h"

@implementation McpAppOptions

-(id)initWithFilename:(NSString*)inFilename andLocalOptions:(LocalOptions*)inLocalOptions {
	if ((self = [super initWithFilename:inFilename andLocalOptions:inLocalOptions])) {
    }
    return self;
}

-(void)dealloc {
    // call super
    [super dealloc];
}

@end
