//
//  FlashConvAutopilot.h
//  Mcp
//
//  Created by Ray Price on 1/28/13.
//
//

#import <Foundation/Foundation.h>
#import "FlashConvProtocol.h"

@interface FlashConvAutopilot : NSObject <FlashConvProtocol>

-(FlashConvBlock*)getClassIntroConversationBlockFor:(FlashGame*)inGame;
-(FlashConvBlock*)getGameIntroConversationBlockFor:(FlashGame*)inGame;
-(FlashConvBlock*)getQuestionConversationBlockFor:(FlashGame*)inGame;
-(FlashConvBlock*)getAnswerConversationBlockFor:(FlashGame*)inGame;

@end
