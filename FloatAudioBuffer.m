//
//  FloatAudioBuffer.m
//  Mcp
//
//  Created by Ray Price on 3/24/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "FloatAudioBuffer.h"
#import "AudioToolbox/AudioFile.h"

@implementation FloatAudioBuffer

@synthesize buffer;
@synthesize bufferSize;
@synthesize sampleCount;
@synthesize sampleRate;

-(id)initWithSampleCount:(int)inSampleCount andSampleRate:(int)inSampleRate {
    // init super first
    if ((self = [super init])) {
        // save size and sample rate
        bufferSize = inSampleCount*sizeof(float);
        sampleCount = inSampleCount;
        sampleRate = inSampleRate;
        
        // allocate buffer space
        buffer = malloc(bufferSize);
    }
    return self;
}

-(id)initFromFile:(NSString*)inFilePath {
    // init super first
    if ((self = [super init])) {
        // open file
        AudioFileID fileId;
        CFURLRef fileUrl = CFURLCreateWithFileSystemPath(nil, (CFStringRef)inFilePath, kCFURLPOSIXPathStyle, NO);
        OSStatus status = AudioFileOpenURL(fileUrl, kAudioFileReadPermission, 0, &fileId);
        
        // get wave file data format and size
        AudioStreamBasicDescription dataFormat;
        UInt32 dataSize = sizeof(dataFormat);
        status = AudioFileGetProperty(fileId, kAudioFilePropertyDataFormat, &dataSize, &dataFormat);
        UInt64 byteCount = 0;
        UInt32 dataSize2 = sizeof(byteCount);
        status = AudioFileGetProperty(fileId, kAudioFilePropertyAudioDataByteCount, &dataSize2, &byteCount);
        
        // save buffer size and byte count
        sampleCount = (UInt32)byteCount/dataFormat.mBytesPerPacket;
        bufferSize = sampleCount*sizeof(float);
        sampleRate = dataFormat.mSampleRate;
        
        // create audio buffer for reading
        buffer = malloc(sampleCount*sizeof(float));
        
        // read bytes from file
        // if int, convert to float here
        UInt32 byteCount2 = byteCount;
        if (dataFormat.mBytesPerPacket == 2) {
            // allocate audio buffer
            short *audioBuffer = malloc(bufferSize);
            
            // read and convert
            status = AudioFileReadBytes(fileId, NO, 0, &byteCount2, audioBuffer);
            for (int i=0; i < sampleCount; i++)
                buffer[i] = audioBuffer[i];
            
            // free audio buffer
            free(audioBuffer);
        }
        else {
            status = AudioFileReadBytes(fileId, NO, 0, &byteCount2, buffer);
        }
        
        // normalize float data
        [FloatAudioBuffer normalizeBufferAtZero:buffer ofSampleCount:sampleCount];
        
        // close file again
        AudioFileClose(fileId);
        CFRelease(fileUrl);
    }
    return self;
}

-(void)writeToWavFile:(NSString*)inFileNameOnly {
    return;
	// get output filenames
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *filename1 = [documentsDirectory stringByAppendingFormat:@"/%@", inFileNameOnly];
	NSString *oldFilename1 = [documentsDirectory stringByAppendingFormat:@"/old_%@", inFileNameOnly];
	
	// if file exists, save backup copy
    /*
	if ([[NSFileManager defaultManager] fileExistsAtPath:filename1]) {
		NSError *myError = [NSError alloc];
		[[NSFileManager defaultManager] removeItemAtPath:oldFilename1 error:&myError];
		[[NSFileManager defaultManager] copyItemAtPath:filename1 toPath:oldFilename1 error:&myError];
	}*/
	
	// set up format
	AudioStreamBasicDescription dataFormat;
	dataFormat.mSampleRate = sampleRate;
	dataFormat.mFormatID = kAudioFormatLinearPCM;
	dataFormat.mFramesPerPacket = 1;
	dataFormat.mChannelsPerFrame = 1;
	dataFormat.mBytesPerFrame = 4;
	dataFormat.mBytesPerPacket = 4;
	dataFormat.mBitsPerChannel = 32;
	dataFormat.mReserved = 0;
	dataFormat.mFormatFlags = kLinearPCMFormatFlagIsFloat|kLinearPCMFormatFlagIsPacked;
	
	// get c path names
	char *path1 = malloc(1024);
	[filename1 getCString:path1 maxLength:1024 encoding:NSUTF8StringEncoding];
	
	// write first audio file
	AudioFileID fileId1;
	CFURLRef fileURL1 = CFURLCreateFromFileSystemRepresentation(NULL, (UInt8*)path1, strlen(path1), false);
	UInt32 sizeWritten = bufferSize;
	AudioFileCreateWithURL(fileURL1, kAudioFileWAVEType, &dataFormat, kAudioFileFlags_EraseFile, &fileId1);
	AudioFileWriteBytes(fileId1, false, 0, &sizeWritten, buffer);
	AudioFileClose(fileId1);
	
	// free buffers
	free(path1);
}

-(void)downsample {
     // if our sample came in at 8k convert tts to 8k, 8-bit
     if (sampleRate == 16000) {
         // downsample
         for (int i=0; i < sampleCount/2; i++)
             buffer[i] = roundf(buffer[i*2]*255.0f)/255.0f;
         
         // set new rates and sizes
         sampleRate = 8000;
         sampleCount /= 2;
         bufferSize /= 2;
     }
}

-(void)truncateToSampleCount:(int)inSampleCount {
    // just change sample count for now
    sampleCount = inSampleCount;
    bufferSize = sampleCount*sizeof(float);
}

-(void)normalize {
    [FloatAudioBuffer normalizeBufferAtZero:buffer ofSampleCount:sampleCount];
}

+(void)normalizeBufferAtZero:(float*)inBuffer ofSampleCount:(int)inSampleCount {
	float max = 0;
	for (int i=0; i < inSampleCount; i++) {
		if (fabs(inBuffer[i]) > max)
			max = fabs(inBuffer[i]);
	}
	for (int i=0; i < inSampleCount; i++) {
		if (inBuffer[i] > 0)
			inBuffer[i] = fabs(inBuffer[i]) / max;
		else 
			inBuffer[i] = 0 - (fabs(inBuffer[i]) / max);
	}
}

+(void)normalizePositiveBuffer:(float*)inBuffer ofSize:(int)inSize {
	// normalize buffer between -1.0 and 1.0, but keep it centered around 0
	// then convert to between 0.0 and 1.0
    float max = 0.0f;
	for (int i=0; i < inSize; i++)
        if (inBuffer[i] > max)
            max = inBuffer[i];
	for (int i=0; i < inSize; i++) {
		if (inBuffer[i] >= 0.0f)
			inBuffer[i] = fabs(inBuffer[i]) / max;
        else
            assert("oh no!");
	}
}

-(void)trimSilenceUsingOverrideNoiseLevel:(float)inOverrideNoiseLevel doTrimMiddle:(BOOL)inTrimMiddle {
    // get lead, tail and silence list
    float leadPercent = 0.0f;
    float tailPercent = 0.0f;
    NSArray *silenceList = [self getSilenceArrayUsingOverrideNoiseLevel:inOverrideNoiseLevel andLeadInto:&leadPercent andTailInto:&tailPercent];
    
    // calculate lead and tail in samples
    int leadLength = (sampleCount-1) * leadPercent;
    int tailLength = (sampleCount-1)  - ((sampleCount-1) * tailPercent);
    
    // now we have lead and tail, copy to our own buffer for debugging
    int sampleBufferSize = (sampleCount-leadLength-tailLength) * sizeof(float);
    float *sampleBuffer = malloc(sampleBufferSize);
    int copyOffset = leadLength;
    int newPos = 0;
    int silencePos = 0;
    int waitCounter = 0;
    while (copyOffset < sampleCount-tailLength) {
        // if we are looking for silence, check to see if we've hit it
        if (waitCounter == 0 && silencePos < silenceList.count) {
            // find next silence position
            NSNumber *startPercent = [silenceList objectAtIndex:silencePos];
            NSNumber *silenceLength = [silenceList objectAtIndex:silencePos+1];
            
            // if we're past the start position
            if ((float)copyOffset/(float)sampleCount > [startPercent floatValue]) {
                // start wait counter
                waitCounter = [silenceLength floatValue]*(float)sampleCount;
                
                // increment silence position
                silencePos += 2;
            }
        }
        
        // copy if we are not waiting to pass silence
        if (waitCounter > 0) {
            waitCounter--;
            copyOffset++;
        }
        else
            sampleBuffer[newPos++] = buffer[copyOffset++];
    }
    
    // replace our existing buffer with this new one
    free(buffer);
    buffer = sampleBuffer;
    bufferSize = newPos*sizeof(float);
    sampleCount = newPos;
}

// use raw wave lead and tail, and return array of silence
-(NSArray*)getSilenceArrayUsingOverrideNoiseLevel:(float)inNoiseLevel andLeadInto:(float*)outLead andTailInto:(float*)outTail {
    // set up fft parameters
    int windowSize = 256;
    int quantizeCount = round((float)sampleCount/(float)windowSize);
    int stepSize = (sampleCount-windowSize) / quantizeCount;

    // calculate volume from raw samples, because it seems more reliable that fft
    float *volumeBuffer = malloc(sizeof(float)*quantizeCount);
    int windowPos = 0;
    for (int i=0; i < quantizeCount; i++) {
        windowPos += stepSize;
        float total = 0.0f;
        float max = 0.0f;
        for (int p=windowPos; p < windowPos+windowSize; p++) {
            total += buffer[p];
            if (buffer[p] > max)
                max = buffer[p];
        }
        volumeBuffer[i] = max;
    }
    
    // normalize volumebuffer
    [FloatAudioBuffer normalizePositiveBuffer:volumeBuffer ofSize:quantizeCount];
    
    
    // attempt to calculate noise level
    float startNoiseLevel = (volumeBuffer[0]+volumeBuffer[1])/2.0f;
    float endNoiseLevel = (volumeBuffer[quantizeCount-1]+volumeBuffer[quantizeCount-2])/2.0f;
    float noiseLevel = MAX(startNoiseLevel, endNoiseLevel);
    
    // now find lead
    int leadPos = 0;
    for (int i=0; i < quantizeCount; i++) {
        if (volumeBuffer[i] > (inNoiseLevel>0.0f?inNoiseLevel:MAX(0.1f,noiseLevel*4.0f))) {
            leadPos = i;
            break;
        }
    }
    
    // now find tail
    int tailPos = 0;
    for (int i=quantizeCount-1; i >= 0; i--) {
        if (volumeBuffer[i] > (inNoiseLevel>0.0f?inNoiseLevel:MAX(0.1f,noiseLevel*4.0f))) {
            tailPos = i;
            break;
        }
    }
    
    // also build a list of contigous segments of silence to remove or shorten
    // because these make a huge difference in the compare calc and silence duration
    // really shouldn't impact the result that much
    NSMutableArray *silenceList = [NSMutableArray arrayWithCapacity:10];
    int contiguous = 0;
    for (int i=0; i < quantizeCount; i++) {
        if (volumeBuffer[i] < (inNoiseLevel>0.0f?inNoiseLevel:MAX(0.1f,noiseLevel*3.0f))) {
            contiguous++;
        }
        else {
            if (contiguous > 0) {
                // ignore is contiguous is less than 2 or we're at the start
                //                printf("contiguous silence of %d, ends at position %d\r\n", contiguous, i);
                if (contiguous > 1 && i-contiguous > 0) {
                    // start position and length
                    NSNumber *silenceStart = [NSNumber numberWithFloat:(float)(i-contiguous)/(float)quantizeCount];
                    NSNumber *silenceLength = [NSNumber numberWithFloat:(float)contiguous/(float)quantizeCount];
                    [silenceList addObject:silenceStart];
                    [silenceList addObject:silenceLength];
                    printf("silence length %f%%, found at %f%%\r\n", [silenceLength floatValue], [silenceStart floatValue]);
                }
                
                // reset contigous
                contiguous = 0;
            }
        }
    }
    
    // free spectrum buffer
    free(volumeBuffer);
    
    // set variables as percentage
    *outLead = (float)leadPos / (float)quantizeCount;
    *outTail = (float)tailPos / (float)quantizeCount;
    
    return silenceList;
}

-(void)runHighPassFilter {
    // high pass filter
    float prevValue = 0.0f;
    for (int s=0; s < sampleCount; s++) {
        float thisValue = buffer[s];
        if (s == 0)
            buffer[s] = thisValue;
        else {
            buffer[s] = thisValue-0.95f*prevValue;
        }
        prevValue = thisValue;
    }
    
    // normalize again now
    [self normalize];
}

-(id)copyWithZone:(NSZone*)zone
{
	// make a new copy to pass back
    FloatAudioBuffer *newBuffer = [[FloatAudioBuffer allocWithZone:zone] initWithSampleCount:sampleCount andSampleRate:sampleRate];
    memcpy(newBuffer.buffer, buffer, bufferSize);
	return newBuffer;
}

-(id)initWithCoder:(NSCoder*)coder {
	if ((self = [super init])) {
        bufferSize = [coder decodeInt32ForKey:@"bufferSize"];
        sampleCount = [coder decodeInt32ForKey:@"sampleCount"];
        sampleRate = [coder decodeInt32ForKey:@"sampleRate"];
        NSUInteger decodeLength;
        float *myBuffer = (float*)[coder decodeBytesForKey:@"buffer" returnedLength:&decodeLength];
        buffer = malloc(decodeLength);
        memcpy(buffer, myBuffer, decodeLength);
	}
	return self;
}

-(void)encodeWithCoder:(NSCoder*)coder {
    [coder encodeInt32:bufferSize forKey:@"bufferSize"];
    [coder encodeInt32:sampleCount forKey:@"sampleCount"];
    [coder encodeInt32:sampleRate forKey:@"sampleRate"];
    [coder encodeBytes:(void*)buffer length:sizeof(float)*sampleCount forKey:@"buffer"];
}

-(void)dealloc {
    // free buffer
    free(buffer);
    [super dealloc];
}

@end
