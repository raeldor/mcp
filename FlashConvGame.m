//
//  FlashConvGame.m
//  Mcp
//
//  Created by Ray Price on 3/24/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "FlashConvGame.h"
#import "Actor.h"
#import "TextFunctions.h"
#import "VoiceFunctions.h"
#import "FloatAudioBuffer.h"
#import "HtmlFunctions.h"
#import "SampleEntry.h"
#import "McpAppDelegate.h"
#import "FlashConvEnglishFromJapanese.h"
#import "FlashConvJapaneseFromEnglish.h"
#import "FlashConvAutopilot.h"
#import "FlashConvMissingWord.h"
#import "FlashConvMissingParticle.h"
#import "FlashConvAnswer.h"
#import "ConverseViewController.h"
#import "FlashConvLine.h"

@implementation FlashConvGame

@synthesize currentSegmentIndex;

//-(id)initWithBookName:(NSString*)inBookName {
-(id)initWithBookName:(NSString*)inBookName giveClassIntro:(BOOL)inGiveClassIntro viewController:(ConverseViewController*)inController completion:(void(^)(BOOL success)) block {
    // save view controller reference to pick up game state information
    converseController = inController;
    
    // gets stripped, so put this in for now
    [DeckSection class];
    [FlashCard class];
    
    // initialize
	if ((self = [super init])) {
        // get app delegate
        McpAppDelegate *appDelegate = (McpAppDelegate*)[[UIApplication sharedApplication] delegate];
        
        // get last played game type
        gameTypeEnum lastPlayedGameType = [[appDelegate localOptions] getLastPlayedGameTypeForDeck:inBookName];
        if (lastPlayedGameType != gameType_SayEnglishFromJapanese &&
            lastPlayedGameType != gameType_SayJapaneseFromEnglish &&
            lastPlayedGameType != gameType_SayBlankWord &&
            lastPlayedGameType != gameType_SayBlankParticle)
            lastPlayedGameType = gameType_SayBlankWord;
        
        // open flash deck
        FlashDeck *myDeck = [[FlashDeck alloc] initWithName:inBookName];
        [myDeck openWithCompletionHandler:^(BOOL success) {
            // close so we don't edit
            [myDeck closeWithCompletionHandler:^(BOOL success) {
                // do nothing after close
            }];
            
            // see if we are trying to create before having loaded app options
            if (!appDelegate.isOptionsLoaded) {
                NSLog(@"Trying to start game before options are loaded.");
                
                /*
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"DEBUG:Trying to start game before options are loaded." delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
                alert.delegate = self;
                [alert show];
                [alert release];*/
            }
            
            // create flash type game using this deck
            myFlashGame = [[FlashGame alloc] initWithDeck:myDeck andGameType:lastPlayedGameType forAppDelegate:appDelegate];
            
            // did we fail?
            if (myFlashGame == nil) {
                // execute block as failed
                [self release];
                block(NO);
            }
            else {
                // create conversation formatter based on game type
                switch (myFlashGame.gameType) {
                    case gameType_SayEnglishFromJapanese:
                        conversation = [[FlashConvEnglishFromJapanese alloc] init];
                        break;
                    case gameType_SayJapaneseFromEnglish:
                        conversation = [[FlashConvJapaneseFromEnglish alloc] init];
                        break;
                    case gameType_SayBlankWord:
                        conversation = [[FlashConvMissingWord alloc] init];
                        break;
                    case gameType_SayBlankParticle:
                        conversation = [[FlashConvMissingParticle alloc] init];
                        break;
                    default:
                        break;
                }
                
                // set current block to class intro
                currentBlockName = @"ClassIntro";
                
                // create an actor for the conversation
                myActor = [[appDelegate getActorUsingName:appDelegate.localOptions.senseiName] retain];
                
                // now we're all done, execute completion block
                block(YES);
            }
        }];
        
        // release the deck
        [myDeck release];
    }
    
    return self;
}

-(FlashDeck*)getDeck {
    return myFlashGame.playDeck;
}

-(gameTypeEnum)getGameType {
    return myFlashGame.gameType;
}

-(FlashConvBlock*)getConversationBlock {
    // get depending on block index
    return [conversation getBlockForName:currentBlockName usingGame:myFlashGame isMuted:converseController.isMuted];
}

-(NSString*)getSegmentAtIndex:(int)inIndex forSpeech:(BOOL)inForSpeech {
    // get application delegate
    McpAppDelegate *appDelegate = (McpAppDelegate*)[[UIApplication sharedApplication] delegate];
    
    // get conversation block
    FlashConvBlock *convBlock = [self getConversationBlock];
    
    // which line?
    FlashConvLine *thisLine = [convBlock.lines objectAtIndex:currentLineIndex];
    NSArray *lineBlock = thisLine.segmentArray;
    
    // which segment?
    NSString *thisSegment = [lineBlock objectAtIndex:inIndex];
    
    // if for speech, strip ruby
    if (inForSpeech && thisSegment != nil) {
        // remove ruby tags
        thisSegment = [thisSegment stringByReplacingOccurrencesOfString:@"<ruby>" withString:@""];
        thisSegment = [thisSegment stringByReplacingOccurrencesOfString:@"</ruby>" withString:@""];
        
        // strip rt
        while ([thisSegment rangeOfString:@"<rt>"].location != NSNotFound) {
            // find range
            int startPos = [thisSegment rangeOfString:@"<rt>"].location;
            int endPos = [thisSegment rangeOfString:@"</rt>"].location + 4;
            NSString *removeString = [thisSegment substringWithRange:NSMakeRange(startPos, endPos-startPos+1)];
            
            // strip out
            thisSegment = [thisSegment stringByReplacingOccurrencesOfString:removeString withString:@""];
        }
    }
    
    // assert
    if (thisSegment == nil) {
        int i=0;
    }
    
    // return segment
    return thisSegment;
}

-(NSString*)getAnswerAsHtmlWithTextAs:(int)showTextAs linkDictionary:(BOOL)inLinkDictionary{
    return @"<html/>";
}

-(NSString*)getCurrentLineAsHtmlWithTextAs:(int)showTextAs linkDictionary:(BOOL)inLinkDictionary{
    // ipad?
    BOOL isIpad = NO;
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 30200
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        isIpad = YES;
#endif
    
    // make furigana larger
    // make highlighted text larger
    int highlightedFontSize = 18;
    int fontSize = 18;
    if (isIpad) {
        fontSize = 24;
        highlightedFontSize = 32;
    }
        
	// define styles
	NSMutableString *htmlString = [NSMutableString stringWithString:@"<html><style>#footer{text-align:center;position:absolute;left:0px;width:100%;bottom:-10px;}</style>"];
	[htmlString appendFormat:@"<style>span.first {color:white;text-shadow: -1px 0 black, 0 1px black, 1px 0 black, 0 -1px black, black 0.1em 0.1em 0.2em #000;font-family='helvetica';font-size:%dpx;font-weight:bold;}</style><style>span.second {color:lightgray;text-shadow: -1px 0 black, 0 1px black, 1px 0 black, 0 -1px black, black 0.1em 0.1em 0.2em #000;font-family='helvetica';font-size:%dpx;font-weight:bold;}</style>", highlightedFontSize, fontSize];
	[htmlString appendFormat:@"<style>span.third {color:white;text-shadow: -1px 0 black, 0 1px black, 1px 0 black, 0 -1px black, black 0.1em 0.1em 0.2em #000;font-family='helvetica';font-size:%dpx;font-weight:bold;}</style><style>span.fourth {color:lightgray;text-shadow: -1px 0 black, 0 1px black, 1px 0 black, 0 -1px black, black 0.1em 0.1em 0.2em #000;font-family='helvetica';font-size:%dpx;font-weight:bold;}</style>", (int)((float)highlightedFontSize*2.0f), (int)((float)fontSize*2.0f)];
	[htmlString appendFormat:@"<style>a {color:inherit;text-shadow:1px 1px 0px #000;font-family='helvetica';font-weight:bold;}</style>"];
	[htmlString appendString:@"<body><p id=\"footer\">"];
	
    // keep track if we are still inside brackets
    BOOL insideBrackets = NO;
    
    // loop through all segments to display html
    for (int s=0; s < [self getSegmentCount]; s++) {
        // if this is the current segment, display in white
        NSString *spanClass = @"second";
        NSString *spanClass2 = @"fourth";
        if (s == currentSegmentIndex) {
            spanClass = @"first";
            spanClass2 = @"third";
        }
        [htmlString appendFormat:@"<span class='%@'>", spanClass];
        
        // override furigana for question part using flash card kaba in case there is
        // two pronunciations for the same kanji (such as 'kaze')
        
        // get text for this part
        NSString *segmentText = [self getSegmentAtIndex:s forSpeech:NO];
        
        // check brackets here before they disappear
        BOOL hasOpeningBrackets = ([segmentText rangeOfString:@"「"].location != NSNotFound);
        BOOL hasClosingBrackets = ([segmentText rangeOfString:@"」"].location != NSNotFound);
        
        // if we are inside brackets, make larger
        if (insideBrackets) {
            segmentText = [NSString stringWithFormat:@"</span><span class='%@'>%@", spanClass2, segmentText];
        }
        
        // make quoted text larger
        segmentText = [segmentText stringByReplacingOccurrencesOfString:@"「" withString:[NSString stringWithFormat:@"「</span><span class='%@'>", spanClass2]];
        
        // make quoted text larger
        segmentText = [segmentText stringByReplacingOccurrencesOfString:@"」" withString:[NSString stringWithFormat:@"</span><span class='%@'>」", spanClass]];

        // are we still inside brackets?
        if (hasOpeningBrackets) {
            if (!hasClosingBrackets)
                insideBrackets = YES;
        }
        if (!hasOpeningBrackets) {
            if (hasClosingBrackets)
                insideBrackets = NO;
        }
        
        // now add this new segment to the html
        [htmlString appendFormat:@"%@", segmentText];
        
        // close span
        [htmlString appendFormat:@"</span>"];
	}
    
	// end html body
	[htmlString appendString:@"</p></body></html>"];
    
	return htmlString;
}

-(NSString*)getCurrentSegmentAsTextForSpeech:(BOOL)inForSpeech {
    return [self getSegmentAtIndex:currentSegmentIndex forSpeech:inForSpeech];
}
    
-(BOOL)isSpeakThisSegment {
    return YES;
}

-(BOOL)isGetUserInput {
    // user input
    FlashConvBlock *convBlock = [self getConversationBlock];
    
    // if we require answer
    if (convBlock.answers.count > 0) {
        if (currentLineIndex == convBlock.lines.count-1) {
            FlashConvLine *thisLine = [convBlock.lines objectAtIndex:currentLineIndex];
            if (currentSegmentIndex == thisLine.segmentArray.count-1 || thisLine.answerAfterEachSegment)
                return YES;
            else
                return NO;
        }
        else
            return NO;
    }
    else
        return NO;
}

-(BOOL)isShortPause {
    // user input
    FlashConvBlock *convBlock = [self getConversationBlock];
    FlashConvLine *thisLine = [convBlock.lines objectAtIndex:currentLineIndex];
    return thisLine.pauseAfterEachSegment;
}

-(NSUInteger)getActorCount {
    return 1;  // always sensei
}

-(int)getCurrentCharacterIndex {
    return 0;
}

-(Actor*)getActorAtIndex:(int)inIndex {
    // get app delegate
    McpAppDelegate *appDelegate = (McpAppDelegate*)[[UIApplication sharedApplication] delegate];
    
    // if actor has changed, load new one
    if (![myActor.actorName isEqualToString:appDelegate.localOptions.senseiName]) {
        // load new actor
        [myActor release];
        myActor = [[appDelegate getActorUsingName:appDelegate.localOptions.senseiName] retain];
    }
    
    return myActor;
}

// for potential correction
-(SampleEntry*)getCurrentSampleSentence {
	// get application delegate
    McpAppDelegate *appDelegate = (McpAppDelegate*)[[UIApplication sharedApplication] delegate];
    
    // return current sample sentence
    SampleEntry *thisSample = [myFlashGame getSampleEntryForJlptLevel:appDelegate.localOptions.jlptLevel pickType:appDelegate.localOptions.pickSentenceType];
    
    return thisSample;
}

-(NSString*)getCurrentSegmentForSpeech:(BOOL)inForSpeech {
    return [self getSegmentAtIndex:currentSegmentIndex forSpeech:inForSpeech];
}

-(int)getSegmentCount {
    // get line
    FlashConvBlock *convBlock = [self getConversationBlock];
    FlashConvLine *thisLine = [convBlock.lines objectAtIndex:currentLineIndex];
    return thisLine.segmentArray.count;
}

-(NSString*)getCurrentSegmentLanguageName {
    // for now do simple check for kanji
    NSString *thisSegment = [self getCurrentSegmentForSpeech:NO];
    if ([TextFunctions containsKanji:thisSegment] || [TextFunctions containsKana:thisSegment])
        return @"Japanese";
    else
        return @"English";
}

-(BOOL)canFastForward {
    return YES;
}

-(BOOL)canRewind {
    return YES;
}

-(BOOL)canSkipForward {
    return YES;
}

-(BOOL)canSkipBackward {
    return YES;
}

-(void)makeWellKnown {
    // make current card well known
    [myFlashGame makeWellKnown];
    
    // make block congrats block ready for docurrentaction again
    currentBlockName = @"Congrats";
    currentLineIndex = 0;
    currentSegmentIndex = 0;
}

-(FlashCard*)getCurrentFlashCard {
    return [myFlashGame getCurrentCard];
}

-(BOOL)playNext {
    NSLog(@"playNext called");
    
    // get block
    FlashConvBlock *convBlock = [self getConversationBlock];
    
    // try and go forward first
    if ([self fastForward])
        return YES;
    else {
        // can't fast forward, so must be moving block
        
        // need to move forward a block
        NSString *nextBlockName = convBlock.nextBlockName;
        for (int i=0; i < convBlock.answers.count; i++) {
            // process answer if selected!
            if (lastSelectedAnswerIndex == i) {
                // increment right/wrong if needed
                FlashConvAnswer *thisAnswer = [convBlock.answers objectAtIndex:i];
                if (thisAnswer.isRightAnswer) {
                    [myFlashGame markAnswerRight];
                }
                if (thisAnswer.isWrongAnswer) {
                    [myFlashGame markAnswerWrong];
                }
                
                // jump to the block pointed to by the answer
                nextBlockName = thisAnswer.jumpToBlockName;
            }
        }
        currentBlockName = nextBlockName;
        currentLineIndex = 0;
        currentSegmentIndex = 0;
        
        // go to next card?
        if (convBlock.goToNextCard)
            [myFlashGame nextCard];
        
        return YES;
    }
}

-(BOOL)fastForward {
    // get line
    FlashConvBlock *convBlock = [self getConversationBlock];
    FlashConvLine *thisLine = [convBlock.lines objectAtIndex:currentLineIndex];
    
    // try and go to next segment first
    if (currentSegmentIndex+1 < thisLine.segmentArray.count) {
        currentSegmentIndex++;
        return YES;
    }
    else {
        // try and go to next line
        if (currentLineIndex+1 < convBlock.lines.count) {
            currentLineIndex++;
            currentSegmentIndex = 0;
            return YES;
        }
        else {
            // if no answer is required, we can continue
            if (convBlock.answers.count == 0) {
                // before changing blocks, do we need to go to next card?
                if (convBlock.goToNextCard)
                    [myFlashGame nextCard];
                
                // change to next block
                currentBlockName = convBlock.nextBlockName;
                currentLineIndex = 0;
                currentSegmentIndex = 0;
                return YES;
            }
            else
                return NO;
        }
    }
}

-(FlashGame*)getFlashGame {
    return myFlashGame;
}

-(BOOL)changeGameTo:(gameTypeEnum)inGameType {
    // get app delegate
    McpAppDelegate *appDelegate = (McpAppDelegate*)[[UIApplication sharedApplication] delegate];
    
    // change game
    myFlashGame.gameType = inGameType;
    
    // change conversation formatter
    [conversation release];
    switch (myFlashGame.gameType) {
        case gameType_SayEnglishFromJapanese:
            conversation = [[FlashConvEnglishFromJapanese alloc] init];
            break;
        case gameType_SayJapaneseFromEnglish:
            conversation = [[FlashConvJapaneseFromEnglish alloc] init];
            break;
        case gameType_SayBlankWord:
            conversation = [[FlashConvMissingWord alloc] init];
            break;
        case gameType_SayBlankParticle:
            conversation = [[FlashConvMissingParticle alloc] init];
            break;
        default:
            break;
    }

    // have to shuffle after game change
    [myFlashGame shuffleDeck];
    
    // set next stage to game change
    currentBlockName = @"GameIntro";
    currentSegmentIndex = 0;
    currentLineIndex = 0;
    
    // save change of game type to options
    [[appDelegate localOptions] setLastPlayedGameType:myFlashGame.gameType forDeck:myFlashGame.playDeck.deckName];
    [[appDelegate localOptions] save];
    
    // success
    return YES;
}

-(BOOL)skipForward {
    return NO;
}

-(BOOL)rewind {
    // try and go back a segment first
    if (currentSegmentIndex > 0) {
        currentSegmentIndex--;
        return YES;
    }
    else {
        // try and go back a line
        if (currentLineIndex > 0) {
            currentLineIndex--;
            
            // set to last segment
            FlashConvBlock *convBlock = [self getConversationBlock];
            FlashConvLine *thisLine = [convBlock.lines objectAtIndex:currentLineIndex];
            currentSegmentIndex = thisLine.segmentArray.count-1;
            
            return YES;
        }
        else
            return NO;
    }
}

-(BOOL)skipBackward {
    return YES;
}

-(int)getCommandIndexUsingBuffer:(void*)inBuffer ofSize:(int)inBufferSize andSampleRate:(int)inSampleRate fromCommandList:(NSArray*)inCommandList {
    // try sample against each command and return best
    float bestScore = -1.0f;
    int indexMatch = -1;
    for (int i=0; i < inCommandList.count; i++) {
        // format of this string can be multiple choices separated by '/' and optional
        // pronunciation separated by '/' inside '{}'
        NSString *thisCommandString = [inCommandList objectAtIndex:i];
        NSArray *pronuns;
        
        // get string in brackets
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"\\{.*?\\}" options:NSRegularExpressionCaseInsensitive error:NULL];
        NSTextCheckingResult *result1 = [regex firstMatchInString:thisCommandString options:0 range:NSMakeRange(0, [thisCommandString length])];
        if (result1 != nil && result1.range.location != NSNotFound) {
            // found brackets
            NSString *inBrackets = [thisCommandString substringWithRange:NSMakeRange(result1.range.location+1, result1.range.length-2)];
            pronuns = [inBrackets componentsSeparatedByString:@"/"];
        }
        else {
            // didn't find brackets, so get pronunciations from command string direct
            pronuns = [thisCommandString componentsSeparatedByString:@"/"];
        }
        
        // check all pronunciations for this command
        for (int p=0; p < pronuns.count; p++) {
            float thisScore = [VoiceFunctions gradeBuffer:inBuffer ofSize:inBufferSize againstText:[pronuns objectAtIndex:p] calibrationMode:NO sampleRate:inSampleRate];
            if (thisScore > bestScore) {
                bestScore = thisScore;
                indexMatch = i;
            }
        }
    }
    return indexMatch;
}

-(NSArray*)getAnswerChoices {
    // get conversation block
    FlashConvBlock *convBlock = [self getConversationBlock];
    
    // create array of answer strings
    NSMutableArray *answerStrings = [NSMutableArray arrayWithCapacity:2];
    for (int i=0; i < convBlock.answers.count; i++) {
        FlashConvAnswer *thisAnswer = [convBlock.answers objectAtIndex:i];
        [answerStrings addObject:thisAnswer.buttonText];
    }
    return answerStrings;
}

-(BOOL)processAnswerUsingSelection:(int)inSelectionIndex {
    // get conversation block
    FlashConvBlock *convBlock = [self getConversationBlock];
    
    // get selected answer
    FlashConvAnswer *selectedAnswer = [convBlock.answers objectAtIndex:inSelectionIndex];

    // which answer?
    for (int i=0; i < convBlock.answers.count; i++) {
        FlashConvAnswer *thisAnswer = [convBlock.answers objectAtIndex:i];
        if ([thisAnswer.buttonText isEqualToString:selectedAnswer.buttonText]) {
            // save index
            lastSelectedAnswerIndex = i;
            
            break;
        }
    }
    
    return YES;
}

-(BOOL)processAnswerUsingBuffer:(void*)inBuffer ofSize:(int)inBufferSize sampleRate:(int)inSampleRate {
    // get command index match
    NSArray *commands = [self getAnswerChoices];
    int index = [self getCommandIndexUsingBuffer:inBuffer ofSize:inBufferSize andSampleRate:inSampleRate fromCommandList:commands];
    
    // process answer
    return [self processAnswerUsingSelection:index];
}

-(void)dealloc {
    // release game
    [myFlashGame release];
    [myActor release];
    [conversation release];
    [super dealloc];
}

@end
